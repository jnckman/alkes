-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2017 at 07:49 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_wellhos`
--

-- --------------------------------------------------------

--
-- Table structure for table `alamats`
--

CREATE TABLE `alamats` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alamat` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alamats`
--

INSERT INTO `alamats` (`id`, `deleted_at`, `created_at`, `updated_at`, `alamat`, `customers_id`) VALUES
(1, NULL, '2017-09-20 00:52:15', NULL, 'alamat', 1),
(2, NULL, NULL, NULL, 'jl adi suciptio 56 , Yogyakarta, Indonesia', 3),
(3, '2017-09-20 14:06:13', NULL, NULL, 'jl adi suciptio 56 , Yogyakarta, Indonesia', 3),
(4, NULL, NULL, NULL, 'Jl Jendral Sudirman 32, Jl Ahman Yani 32', 3),
(5, NULL, NULL, NULL, 'jl adi suciptio 56 , Yogyakarta, Indonesia', 2),
(6, NULL, NULL, NULL, 'aioei', 2),
(7, NULL, NULL, NULL, 'dsss', 2),
(8, NULL, NULL, NULL, 'wadawd', 2),
(9, '2017-09-20 14:11:19', NULL, NULL, 'aaa', 2),
(10, '2017-09-20 14:11:15', NULL, NULL, 'coks', 2);

-- --------------------------------------------------------

--
-- Table structure for table `assign_users`
--

CREATE TABLE `assign_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `table_id` int(10) UNSIGNED DEFAULT NULL,
  `table_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assign_users`
--

INSERT INTO `assign_users` (`id`, `deleted_at`, `created_at`, `updated_at`, `user_id`, `table_id`, `table_name`) VALUES
(1, NULL, NULL, NULL, 1, 3, 'wellhos_subdepts'),
(2, NULL, NULL, NULL, 2, 1, 'wellhos_subdepts');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL,
  `norek` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atasnama` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `customers_id`, `norek`, `bank`, `atasnama`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'lol', 'lol', 'lol', '2017-09-20 20:54:25', NULL, NULL),
(2, 1, '2', '2', '2', '2017-09-20 20:47:58', NULL, NULL),
(3, 3, '02222395847', 'BNI', 'Daniel Roy', '2017-09-20 21:05:03', NULL, NULL),
(4, 1, 'tes', 'tes', 'tes', '2017-09-20 20:54:31', NULL, NULL),
(5, 1, 'tes', 'tes', 'tes2', '2017-09-20 20:54:35', NULL, NULL),
(6, 1, 'tes', 'tes', 'tes3', '2017-09-20 20:54:39', NULL, NULL),
(7, 1, 'tes', 'tes', 'tes3', '2017-09-20 20:55:49', NULL, NULL),
(8, 1, 'tes', 'tes', 'tes3', '2017-09-20 20:56:03', NULL, NULL),
(9, 1, 'tes', 'tes', 'tes3', '2017-09-20 20:56:15', NULL, NULL),
(10, 2, 'tes', 'tes', 'tt', '2017-09-20 20:57:44', NULL, NULL),
(11, 1, 'dwxx', 'dw', 'd12', '2017-09-20 20:58:20', '2017-09-20 13:58:20', NULL),
(12, 2, 'tas', 'tes', 'tos', '2017-09-20 21:07:12', NULL, NULL),
(13, 2, 'tes', 'tis', 'eo', '2017-09-20 21:07:19', NULL, NULL),
(14, 2, 'ooo', 'ooo', 'ooo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barangdatangs`
--

CREATE TABLE `barangdatangs` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tanggaldatang` date DEFAULT NULL,
  `nospbm` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penerima` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suppliers_id` int(10) UNSIGNED DEFAULT NULL,
  `gudangs_id` int(10) UNSIGNED DEFAULT NULL,
  `purchaseorders_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukuran` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avgprice` int(10) UNSIGNED DEFAULT '0',
  `min` int(10) UNSIGNED DEFAULT '0',
  `max` int(10) UNSIGNED DEFAULT '0',
  `akumstok` int(10) UNSIGNED DEFAULT '0',
  `ppn` int(10) UNSIGNED DEFAULT '0',
  `harga_beli_akhir` int(10) UNSIGNED DEFAULT '0',
  `harga_jual` int(10) UNSIGNED DEFAULT '0',
  `kode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci,
  `satuans_id` int(10) UNSIGNED DEFAULT NULL,
  `kategoris_id` int(10) UNSIGNED DEFAULT NULL,
  `cara_pakai` text COLLATE utf8mb4_unicode_ci,
  `spesifikasi` text COLLATE utf8mb4_unicode_ci,
  `video` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`, `jenis`, `tipe`, `ukuran`, `status`, `avgprice`, `min`, `max`, `akumstok`, `ppn`, `harga_beli_akhir`, `harga_jual`, `kode`, `gambar`, `satuans_id`, `kategoris_id`, `cara_pakai`, `spesifikasi`, `video`) VALUES
(1, NULL, '2017-09-12 11:57:41', '2017-10-08 17:25:50', 'Sikat Serat Rami', 'SIKAT', 'Finished', '50x50', 'OK', 20000, 10, 100, 50, 10, 21000, 21000, 'SK001', 'uploads/1/2017-10/1.PNG', 2, 1, NULL, NULL, NULL),
(2, NULL, '2017-09-17 16:33:52', '2017-10-08 17:20:01', 'Sikat Pembersih Piring', 'Sikat', 'Finished', '20x20', 'OK', 10000, 100, 1000, 200, 25, 12000, 20000, 'SK003', 'uploads/1/2017-10/3.PNG', 2, 1, NULL, NULL, NULL),
(3, NULL, '2017-09-12 11:57:41', '2017-10-08 17:25:01', 'Eco Bottle Brush', 'Sikat', 'Finished', '20x80', 'OK', 10000, 25, 250, 100, 10, 15000, 25000, 'SK005', 'uploads/1/2017-10/5.PNG', 2, 1, NULL, NULL, NULL),
(4, NULL, '2017-09-21 07:21:38', '2017-10-08 17:21:55', 'Sikat Elastis Stainless', 'Sikat', 'Finished', '30x30', 'OK', 12000, 100, 1000, 500, 10, 13000, 20000, 'SK004', 'uploads/1/2017-10/4.PNG', 2, 1, NULL, NULL, NULL),
(5, NULL, '2017-09-24 20:45:48', '2017-10-08 17:25:19', 'Sikat Cuci Pakaian', 'Sikat', 'Finished', '10x10', 'OK', 10000, 100, 1000, 500, 10, 10000, 15000, 'SK002', 'uploads/1/2017-10/2.PNG', 2, 1, NULL, NULL, NULL),
(6, NULL, '2017-09-12 11:57:41', '2017-10-08 17:31:22', 'Super Broom Bolde', 'Sapu', 'Finished', '50x50', 'OK', 50000, 10, 100, 30, 10, 70000, 150000, 'SP001', 'uploads/1/2017-10/6.PNG', 2, 2, NULL, NULL, NULL),
(7, NULL, '2017-09-17 16:33:52', '2017-10-08 17:35:40', 'Sapu Gelagah', 'Sapu', 'Finished', '60x60', 'OK', 30000, 10, 100, 50, 0, 32000, 50000, 'SP003', 'uploads/1/2017-10/8.PNG', 2, 2, NULL, NULL, NULL),
(8, NULL, '2017-09-12 11:57:41', '2017-10-08 17:38:51', 'Sapu ijuk Dragon', 'Sapu', 'Finished', '20x120', 'OK', 20000, 25, 250, 100, 10, 30000, 65000, 'SP005', 'uploads/1/2017-10/10.PNG', 2, 2, NULL, NULL, NULL),
(9, NULL, '2017-09-21 07:21:38', '2017-10-08 17:39:05', 'SAPU NAGOYA MACAN', 'Sapu', 'Finished', '30x30', 'OK', 35000, 100, 1000, 500, 10, 60000, 80000, 'SP004', 'uploads/1/2017-10/9.PNG', 2, 2, NULL, NULL, NULL),
(10, NULL, '2017-09-24 20:45:48', '2017-10-08 17:33:55', 'Sapu Lidi', 'Sapu', 'Finished', '10x70', 'OK', 10000, 100, 1000, 500, 10, 10000, 20000, 'SP002', 'uploads/1/2017-10/7.PNG', 2, 2, NULL, NULL, NULL),
(11, NULL, '2017-09-24 20:45:48', '2017-10-08 17:41:35', 'Power BROOM', 'Sapu', 'Finished', '10x90', 'OK', 100000, 100, 1000, 500, 10, 120000, 150000, 'SP006', 'uploads/1/2017-10/11.PNG', 2, 2, NULL, NULL, NULL),
(12, NULL, '2017-09-12 11:57:41', '2017-10-08 17:49:24', 'All Natural Pure Baking Soda by Bob\'s Red Mill', 'Baking Soda', 'Finished', '400', 'OK', 30000, 25, 250, 100, 10, 35000, 65000, 'BK003', 'uploads/1/2017-10/14.PNG', 4, 3, NULL, NULL, NULL),
(13, NULL, '2017-09-21 07:21:38', '2017-10-08 17:48:14', 'Health Paradise - Natural Baking Soda', 'Baking Soda', 'Finished', '500', 'OK', 33000, 100, 1000, 500, 10, 40000, 50000, 'BK002', 'uploads/1/2017-10/13.PNG', 4, 3, NULL, NULL, NULL),
(14, NULL, '2017-09-24 20:45:48', '2017-10-08 17:46:38', 'Baking Soda \"Arm & Hammer\" US', 'Baking Soda', 'Finished', '1', 'OK', 10000, 100, 1000, 500, 10, 20000, 44000, 'BK001', 'uploads/1/2017-10/12.PNG', 1, 3, NULL, NULL, NULL),
(15, NULL, '2017-09-24 20:45:48', '2017-10-08 17:50:26', 'Pure Baking Soda Arm & Hammer Pharmacy Grade', 'Baking Soda', 'Finished', '500', 'OK', 100000, 100, 1000, 500, 10, 120000, 150000, 'BK004', 'uploads/1/2017-10/15.PNG', 4, 3, NULL, NULL, NULL),
(16, NULL, '2017-10-22 22:04:27', NULL, 'Barang0001', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0001', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(17, NULL, '2017-10-22 22:16:06', NULL, 'barang0001', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0001', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(18, NULL, '2017-10-22 22:16:06', NULL, 'barang0002', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0002', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(19, NULL, '2017-10-22 22:16:06', NULL, 'barang0003', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0003', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(20, NULL, '2017-10-22 22:16:06', NULL, 'barang0004', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0004', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(21, NULL, '2017-10-22 22:16:06', NULL, 'barang0005', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0005', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(22, NULL, '2017-10-22 22:16:07', NULL, 'barang0006', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0006', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(23, NULL, '2017-10-22 22:16:07', NULL, 'barang0007', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0007', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(24, NULL, '2017-10-22 22:16:07', NULL, 'barang0008', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0008', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(25, NULL, '2017-10-22 22:16:07', NULL, 'barang0009', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0009', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(26, NULL, '2017-10-22 22:16:07', NULL, 'barang0010', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0010', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(27, NULL, '2017-10-22 22:16:07', NULL, 'barang0011', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0011', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(28, NULL, '2017-10-22 22:16:07', NULL, 'barang0012', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0012', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(29, NULL, '2017-10-22 22:16:07', NULL, 'barang0013', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0013', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(30, NULL, '2017-10-22 22:16:07', NULL, 'barang0014', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0014', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(31, NULL, '2017-10-22 22:16:07', NULL, 'barang0015', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0015', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(32, NULL, '2017-10-22 22:16:07', NULL, 'barang0016', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0016', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(33, NULL, '2017-10-22 22:16:07', NULL, 'barang0017', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0017', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(34, NULL, '2017-10-22 22:16:07', NULL, 'barang0018', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0018', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(35, NULL, '2017-10-22 22:16:07', NULL, 'barang0019', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0019', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(36, NULL, '2017-10-22 22:16:07', NULL, 'barang0020', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0020', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(37, NULL, '2017-10-22 22:16:07', NULL, 'barang0021', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0021', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(38, NULL, '2017-10-22 22:16:07', NULL, 'barang0022', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0022', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(39, NULL, '2017-10-22 22:16:07', NULL, 'barang0023', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0023', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(40, NULL, '2017-10-22 22:16:07', NULL, 'barang0024', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0024', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(41, NULL, '2017-10-22 22:16:07', NULL, 'barang0025', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0025', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(42, NULL, '2017-10-22 22:16:07', NULL, 'barang0026', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0026', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(43, NULL, '2017-10-22 22:16:07', NULL, 'barang0027', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0027', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(44, NULL, '2017-10-22 22:16:08', NULL, 'barang0028', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0028', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(45, NULL, '2017-10-22 22:16:08', NULL, 'barang0029', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0029', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(46, NULL, '2017-10-22 22:16:08', NULL, 'barang0030', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0030', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(47, NULL, '2017-10-22 22:16:08', NULL, 'barang0031', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0031', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(48, NULL, '2017-10-22 22:16:08', NULL, 'barang0032', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0032', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(49, NULL, '2017-10-22 22:16:08', NULL, 'barang0033', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0033', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(50, NULL, '2017-10-22 22:16:08', NULL, 'barang0034', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0034', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(51, NULL, '2017-10-22 22:16:08', NULL, 'barang0035', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0035', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(52, NULL, '2017-10-22 22:16:08', NULL, 'barang0036', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0036', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(53, NULL, '2017-10-22 22:16:08', NULL, 'barang0037', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0037', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(54, NULL, '2017-10-22 22:16:08', NULL, 'barang0038', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0038', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(55, NULL, '2017-10-22 22:16:08', NULL, 'barang0039', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0039', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(56, NULL, '2017-10-22 22:16:08', NULL, 'barang0040', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0040', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(57, NULL, '2017-10-22 22:16:08', NULL, 'barang0041', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0041', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(58, NULL, '2017-10-22 22:16:08', NULL, 'barang0042', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0042', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(59, NULL, '2017-10-22 22:16:08', NULL, 'barang0043', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0043', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(60, NULL, '2017-10-22 22:16:08', NULL, 'barang0044', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0044', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(61, NULL, '2017-10-22 22:16:08', NULL, 'barang0045', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0045', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(62, NULL, '2017-10-22 22:16:08', NULL, 'barang0046', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0046', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(63, NULL, '2017-10-22 22:16:08', NULL, 'barang0047', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0047', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(64, NULL, '2017-10-22 22:16:08', NULL, 'barang0048', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0048', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(65, NULL, '2017-10-22 22:16:08', NULL, 'barang0049', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0049', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(66, NULL, '2017-10-22 22:16:08', NULL, 'barang0050', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0050', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(67, NULL, '2017-10-22 22:16:08', NULL, 'barang0051', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0051', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(68, NULL, '2017-10-22 22:16:08', NULL, 'barang0052', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0052', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(69, NULL, '2017-10-22 22:16:08', NULL, 'barang0053', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0053', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(70, NULL, '2017-10-22 22:16:08', NULL, 'barang0054', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0054', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(71, NULL, '2017-10-22 22:16:08', NULL, 'barang0055', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0055', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(72, NULL, '2017-10-22 22:16:08', NULL, 'barang0056', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0056', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(73, NULL, '2017-10-22 22:16:08', NULL, 'barang0057', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0057', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(74, NULL, '2017-10-22 22:16:08', NULL, 'barang0058', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0058', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(75, NULL, '2017-10-22 22:16:09', NULL, 'barang0059', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0059', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(76, NULL, '2017-10-22 22:16:09', NULL, 'barang0060', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0060', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(77, NULL, '2017-10-22 22:16:09', NULL, 'barang0061', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0061', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(78, NULL, '2017-10-22 22:16:09', NULL, 'barang0062', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0062', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(79, NULL, '2017-10-22 22:16:09', NULL, 'barang0063', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0063', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(80, NULL, '2017-10-22 22:16:09', NULL, 'barang0064', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0064', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(81, NULL, '2017-10-22 22:16:09', NULL, 'barang0065', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0065', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(82, NULL, '2017-10-22 22:16:09', NULL, 'barang0066', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0066', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(83, NULL, '2017-10-22 22:16:09', NULL, 'barang0067', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0067', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(84, NULL, '2017-10-22 22:16:09', NULL, 'barang0068', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0068', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(85, NULL, '2017-10-22 22:16:09', NULL, 'barang0069', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0069', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(86, NULL, '2017-10-22 22:16:09', NULL, 'barang0070', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0070', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(87, NULL, '2017-10-22 22:16:09', NULL, 'barang0071', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0071', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(88, NULL, '2017-10-22 22:16:09', NULL, 'barang0072', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0072', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(89, NULL, '2017-10-22 22:16:09', NULL, 'barang0073', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0073', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(90, NULL, '2017-10-22 22:16:09', NULL, 'barang0074', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0074', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(91, NULL, '2017-10-22 22:16:09', NULL, 'barang0075', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0075', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(92, NULL, '2017-10-22 22:16:09', NULL, 'barang0076', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0076', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(93, NULL, '2017-10-22 22:16:09', NULL, 'barang0077', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0077', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(94, NULL, '2017-10-22 22:16:09', NULL, 'barang0078', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0078', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(95, NULL, '2017-10-22 22:16:09', NULL, 'barang0079', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0079', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(96, NULL, '2017-10-22 22:16:09', NULL, 'barang0080', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0080', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(97, NULL, '2017-10-22 22:16:09', NULL, 'barang0081', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0081', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(98, NULL, '2017-10-22 22:16:09', NULL, 'barang0082', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0082', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(99, NULL, '2017-10-22 22:16:09', NULL, 'barang0083', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0083', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(100, NULL, '2017-10-22 22:16:09', NULL, 'barang0084', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0084', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(101, NULL, '2017-10-22 22:16:09', NULL, 'barang0085', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0085', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(102, NULL, '2017-10-22 22:16:09', NULL, 'barang0086', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0086', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(103, NULL, '2017-10-22 22:16:10', NULL, 'barang0087', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0087', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(104, NULL, '2017-10-22 22:16:10', NULL, 'barang0088', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0088', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(105, NULL, '2017-10-22 22:16:10', NULL, 'barang0089', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0089', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(106, NULL, '2017-10-22 22:16:10', NULL, 'barang0090', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0090', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(107, NULL, '2017-10-22 22:16:10', NULL, 'barang0091', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0091', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(108, NULL, '2017-10-22 22:16:10', NULL, 'barang0092', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0092', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(109, NULL, '2017-10-22 22:16:10', NULL, 'barang0093', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0093', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(110, NULL, '2017-10-22 22:16:10', NULL, 'barang0094', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0094', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(111, NULL, '2017-10-22 22:16:10', NULL, 'barang0095', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0095', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(112, NULL, '2017-10-22 22:16:10', NULL, 'barang0096', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0096', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(113, NULL, '2017-10-22 22:16:10', NULL, 'barang0097', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0097', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(114, NULL, '2017-10-22 22:16:10', NULL, 'barang0098', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0098', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(115, NULL, '2017-10-22 22:16:10', NULL, 'barang0099', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0099', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(116, NULL, '2017-10-22 22:16:10', NULL, 'barang0100', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0100', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(117, NULL, '2017-10-22 22:16:10', NULL, 'barang0101', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0101', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(118, NULL, '2017-10-22 22:16:10', NULL, 'barang0102', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0102', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(119, NULL, '2017-10-22 22:16:10', NULL, 'barang0103', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0103', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(120, NULL, '2017-10-22 22:16:10', NULL, 'barang0104', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0104', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(121, NULL, '2017-10-22 22:16:10', NULL, 'barang0105', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0105', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(122, NULL, '2017-10-22 22:16:10', NULL, 'barang0106', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0106', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(123, NULL, '2017-10-22 22:16:10', NULL, 'barang0107', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0107', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(124, NULL, '2017-10-22 22:16:10', NULL, 'barang0108', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0108', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(125, NULL, '2017-10-22 22:16:10', NULL, 'barang0109', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0109', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(126, NULL, '2017-10-22 22:16:10', NULL, 'barang0110', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0110', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(127, NULL, '2017-10-22 22:16:10', NULL, 'barang0111', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0111', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(128, NULL, '2017-10-22 22:16:10', NULL, 'barang0112', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0112', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(129, NULL, '2017-10-22 22:16:11', NULL, 'barang0113', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0113', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(130, NULL, '2017-10-22 22:16:11', NULL, 'barang0114', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0114', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(131, NULL, '2017-10-22 22:16:11', NULL, 'barang0115', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0115', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(132, NULL, '2017-10-22 22:16:11', NULL, 'barang0116', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0116', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(133, NULL, '2017-10-22 22:16:11', NULL, 'barang0117', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0117', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(134, NULL, '2017-10-22 22:16:11', NULL, 'barang0118', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0118', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(135, NULL, '2017-10-22 22:16:11', NULL, 'barang0119', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0119', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(136, NULL, '2017-10-22 22:16:11', NULL, 'barang0120', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0120', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(137, NULL, '2017-10-22 22:16:11', NULL, 'barang0121', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0121', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(138, NULL, '2017-10-22 22:16:11', NULL, 'barang0122', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0122', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(139, NULL, '2017-10-22 22:16:11', NULL, 'barang0123', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0123', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(140, NULL, '2017-10-22 22:16:11', NULL, 'barang0124', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0124', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(141, NULL, '2017-10-22 22:16:11', NULL, 'barang0125', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0125', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(142, NULL, '2017-10-22 22:16:11', NULL, 'barang0126', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0126', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(143, NULL, '2017-10-22 22:16:11', NULL, 'barang0127', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0127', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(144, NULL, '2017-10-22 22:16:11', NULL, 'barang0128', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0128', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(145, NULL, '2017-10-22 22:16:11', NULL, 'barang0129', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0129', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(146, NULL, '2017-10-22 22:16:11', NULL, 'barang0130', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0130', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(147, NULL, '2017-10-22 22:16:11', NULL, 'barang0131', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0131', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(148, NULL, '2017-10-22 22:16:11', NULL, 'barang0132', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0132', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(149, NULL, '2017-10-22 22:16:11', NULL, 'barang0133', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0133', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(150, NULL, '2017-10-22 22:16:11', NULL, 'barang0134', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0134', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(151, NULL, '2017-10-22 22:16:12', NULL, 'barang0135', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0135', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(152, NULL, '2017-10-22 22:16:12', NULL, 'barang0136', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0136', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(153, NULL, '2017-10-22 22:16:12', NULL, 'barang0137', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0137', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(154, NULL, '2017-10-22 22:16:12', NULL, 'barang0138', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0138', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(155, NULL, '2017-10-22 22:16:12', NULL, 'barang0139', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0139', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(156, NULL, '2017-10-22 22:16:12', NULL, 'barang0140', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0140', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(157, NULL, '2017-10-22 22:16:12', NULL, 'barang0141', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0141', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(158, NULL, '2017-10-22 22:16:12', NULL, 'barang0142', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0142', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(159, NULL, '2017-10-22 22:16:12', NULL, 'barang0143', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0143', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(160, NULL, '2017-10-22 22:16:12', NULL, 'barang0144', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0144', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(161, NULL, '2017-10-22 22:16:12', NULL, 'barang0145', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0145', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(162, NULL, '2017-10-22 22:16:12', NULL, 'barang0146', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0146', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(163, NULL, '2017-10-22 22:16:12', NULL, 'barang0147', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0147', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(164, NULL, '2017-10-22 22:16:12', NULL, 'barang0148', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0148', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(165, NULL, '2017-10-22 22:16:12', NULL, 'barang0149', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0149', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(166, NULL, '2017-10-22 22:16:12', NULL, 'barang0150', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0150', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(167, NULL, '2017-10-22 22:16:12', NULL, 'barang0151', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0151', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(168, NULL, '2017-10-22 22:16:12', NULL, 'barang0152', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0152', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(169, NULL, '2017-10-22 22:16:12', NULL, 'barang0153', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0153', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(170, NULL, '2017-10-22 22:16:12', NULL, 'barang0154', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0154', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(171, NULL, '2017-10-22 22:16:12', NULL, 'barang0155', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0155', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(172, NULL, '2017-10-22 22:16:12', NULL, 'barang0156', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0156', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(173, NULL, '2017-10-22 22:16:13', NULL, 'barang0157', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0157', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(174, NULL, '2017-10-22 22:16:13', NULL, 'barang0158', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0158', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(175, NULL, '2017-10-22 22:16:13', NULL, 'barang0159', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0159', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(176, NULL, '2017-10-22 22:16:13', NULL, 'barang0160', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0160', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(177, NULL, '2017-10-22 22:16:13', NULL, 'barang0161', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0161', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(178, NULL, '2017-10-22 22:16:13', NULL, 'barang0162', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0162', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(179, NULL, '2017-10-22 22:16:13', NULL, 'barang0163', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0163', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(180, NULL, '2017-10-22 22:16:13', NULL, 'barang0164', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0164', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(181, NULL, '2017-10-22 22:16:13', NULL, 'barang0165', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0165', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(182, NULL, '2017-10-22 22:16:13', NULL, 'barang0166', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0166', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(183, NULL, '2017-10-22 22:16:13', NULL, 'barang0167', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0167', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(184, NULL, '2017-10-22 22:16:13', NULL, 'barang0168', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0168', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(185, NULL, '2017-10-22 22:16:13', NULL, 'barang0169', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0169', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(186, NULL, '2017-10-22 22:16:13', NULL, 'barang0170', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0170', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(187, NULL, '2017-10-22 22:16:13', NULL, 'barang0171', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0171', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(188, NULL, '2017-10-22 22:16:13', NULL, 'barang0172', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0172', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(189, NULL, '2017-10-22 22:16:13', NULL, 'barang0173', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0173', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(190, NULL, '2017-10-22 22:16:13', NULL, 'barang0174', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0174', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(191, NULL, '2017-10-22 22:16:13', NULL, 'barang0175', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0175', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(192, NULL, '2017-10-22 22:16:13', NULL, 'barang0176', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0176', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(193, NULL, '2017-10-22 22:16:14', NULL, 'barang0177', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0177', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(194, NULL, '2017-10-22 22:16:14', NULL, 'barang0178', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0178', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(195, NULL, '2017-10-22 22:16:14', NULL, 'barang0179', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0179', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(196, NULL, '2017-10-22 22:16:14', NULL, 'barang0180', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0180', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(197, NULL, '2017-10-22 22:16:14', NULL, 'barang0181', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0181', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(198, NULL, '2017-10-22 22:16:14', NULL, 'barang0182', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0182', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(199, NULL, '2017-10-22 22:16:14', NULL, 'barang0183', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0183', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(200, NULL, '2017-10-22 22:16:14', NULL, 'barang0184', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0184', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(201, NULL, '2017-10-22 22:16:14', NULL, 'barang0185', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0185', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(202, NULL, '2017-10-22 22:16:14', NULL, 'barang0186', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0186', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(203, NULL, '2017-10-22 22:16:14', NULL, 'barang0187', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0187', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(204, NULL, '2017-10-22 22:16:14', NULL, 'barang0188', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0188', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(205, NULL, '2017-10-22 22:16:14', NULL, 'barang0189', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0189', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(206, NULL, '2017-10-22 22:16:14', NULL, 'barang0190', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0190', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(207, NULL, '2017-10-22 22:16:14', NULL, 'barang0191', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0191', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(208, NULL, '2017-10-22 22:16:14', NULL, 'barang0192', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0192', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(209, NULL, '2017-10-22 22:16:14', NULL, 'barang0193', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0193', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(210, NULL, '2017-10-22 22:16:14', NULL, 'barang0194', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0194', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(211, NULL, '2017-10-22 22:16:14', NULL, 'barang0195', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0195', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(212, NULL, '2017-10-22 22:16:14', NULL, 'barang0196', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0196', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(213, NULL, '2017-10-22 22:16:14', NULL, 'barang0197', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0197', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(214, NULL, '2017-10-22 22:16:14', NULL, 'barang0198', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0198', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(215, NULL, '2017-10-22 22:16:14', NULL, 'barang0199', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0199', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(216, NULL, '2017-10-22 22:16:14', NULL, 'barang0200', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0200', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(217, NULL, '2017-10-22 22:16:15', NULL, 'barang0201', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0201', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(218, NULL, '2017-10-22 22:16:15', NULL, 'barang0202', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0202', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(219, NULL, '2017-10-22 22:16:15', NULL, 'barang0203', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0203', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(220, NULL, '2017-10-22 22:16:15', NULL, 'barang0204', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0204', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(221, NULL, '2017-10-22 22:16:15', NULL, 'barang0205', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0205', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(222, NULL, '2017-10-22 22:16:15', NULL, 'barang0206', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0206', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(223, NULL, '2017-10-22 22:16:15', NULL, 'barang0207', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0207', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(224, NULL, '2017-10-22 22:16:15', NULL, 'barang0208', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0208', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(225, NULL, '2017-10-22 22:16:15', NULL, 'barang0209', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0209', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(226, NULL, '2017-10-22 22:16:15', NULL, 'barang0210', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0210', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL);
INSERT INTO `barangs` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`, `jenis`, `tipe`, `ukuran`, `status`, `avgprice`, `min`, `max`, `akumstok`, `ppn`, `harga_beli_akhir`, `harga_jual`, `kode`, `gambar`, `satuans_id`, `kategoris_id`, `cara_pakai`, `spesifikasi`, `video`) VALUES
(227, NULL, '2017-10-22 22:16:15', NULL, 'barang0211', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0211', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(228, NULL, '2017-10-22 22:16:15', NULL, 'barang0212', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0212', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(229, NULL, '2017-10-22 22:16:15', NULL, 'barang0213', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0213', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(230, NULL, '2017-10-22 22:16:15', NULL, 'barang0214', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0214', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(231, NULL, '2017-10-22 22:16:15', NULL, 'barang0215', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0215', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL),
(232, NULL, '2017-10-22 22:16:15', NULL, 'barang0216', 'CobaBarang', 'CobaBarang', '0x0', 'finished', 1000, 5, 200, 50, 10, 500, 2000, 'b0216', 'uploads/1/2017-10/default_placeholder_750x415.png', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_gudangs`
--

CREATE TABLE `barang_gudangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `gudangs_id` int(10) UNSIGNED DEFAULT NULL,
  `stok` int(10) UNSIGNED DEFAULT '0',
  `min` int(10) UNSIGNED DEFAULT NULL,
  `max` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `responses` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2017-09-12 06:57:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'http://localhost/wellhos/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2017-11-23 05:33:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(8, 'Wellhos Department', 'Route', 'AdminWellhosDepartmentsControllerGetIndex', 'normal', 'fa fa-building-o', 27, 1, 0, 1, 1, '2017-09-12 11:45:15', '2017-09-27 04:35:36'),
(10, 'Kategori', 'Route', 'AdminKategorisControllerGetIndex', 'normal', 'fa fa-gear', 21, 1, 0, 1, 3, '2017-09-12 11:50:04', '2017-09-27 04:35:00'),
(11, 'Satuan', 'Route', 'AdminSatuansControllerGetIndex', 'normal', 'fa fa-glass', 21, 1, 0, 1, 4, '2017-09-12 11:50:54', '2017-09-27 04:35:06'),
(12, 'Barang', 'Route', 'AdminBarangsControllerGetIndex', 'normal', 'fa fa-group', 21, 1, 0, 1, 5, '2017-09-12 11:51:29', '2017-09-27 04:35:12'),
(13, 'Supplier', 'Route', 'AdminSuppliersControllerGetIndex', 'normal', 'fa fa-database', 21, 1, 0, 1, 2, '2017-09-12 11:58:24', '2017-09-27 04:34:54'),
(14, 'Pricelist', 'Route', 'AdminPricelistsControllerGetIndex', 'normal', 'fa fa-money', 21, 1, 0, 1, 7, '2017-09-12 11:59:03', '2017-09-27 04:35:19'),
(15, 'Gudang', 'Route', 'AdminGudangsControllerGetIndex', 'normal', 'fa fa-simplybuilt', 21, 1, 0, 1, 1, '2017-09-12 12:02:07', '2017-09-27 04:34:48'),
(16, 'Resep', 'Route', 'AdminResepsControllerGetIndex', 'normal', 'fa fa-book', 22, 1, 0, 1, 3, '2017-09-12 12:04:20', '2017-09-27 04:34:27'),
(17, 'List Bahan', 'Route', 'AdminListbahansControllerGetIndex', 'normal', 'fa fa-bookmark', 22, 1, 0, 1, 4, '2017-09-12 12:07:27', '2017-09-27 04:34:35'),
(19, 'Barang Gudang', 'Route', 'AdminBarangGudangs30ControllerGetIndex', 'normal', 'fa fa-home', 22, 1, 0, 1, 1, '2017-09-12 12:15:20', '2017-09-27 04:34:13'),
(20, 'Transferlog', 'Route', 'AdminTransferlogsControllerGetIndex', 'normal', 'fa fa-exchange', 22, 1, 0, 1, 2, '2017-09-12 12:17:34', '2017-09-27 04:34:21'),
(21, 'Master Data', 'URL', '#', 'normal', 'fa fa-th', 0, 1, 0, 1, 5, '2017-09-12 16:53:10', '2017-09-27 04:34:41'),
(22, 'Stock', 'URL', '#', 'normal', 'fa fa-gift', 0, 1, 0, 1, 4, '2017-09-12 16:55:29', '2017-09-27 04:33:51'),
(23, 'Master Project', 'Route', 'AdminMasterProjectsControllerGetIndex', 'normal', 'fa fa-file-o', 27, 1, 0, 1, 2, '2017-09-15 22:08:25', '2017-09-27 04:35:44'),
(24, 'Project', 'Route', 'AdminProjectsControllerGetIndex', 'normal', 'fa fa-book', 27, 1, 0, 1, 3, '2017-09-15 22:09:41', '2017-09-27 04:35:51'),
(27, 'Company Data', 'URL', '#', 'normal', 'fa fa-times-circle-o', 0, 1, 0, 1, 6, '2017-09-16 07:11:06', '2017-09-27 04:35:25'),
(28, 'Material Request', 'URL', '#', 'normal', 'fa fa-video-camera', 0, 1, 0, 1, 8, '2017-09-16 07:14:26', '2017-09-27 04:36:32'),
(29, 'List All MR', 'Route', 'AdminMrSubdepts36ControllerGetIndex', 'normal', 'fa fa-th-list', 28, 1, 0, 1, 6, '2017-09-16 13:23:13', '2017-10-15 19:12:09'),
(38, 'Manage Shop', 'URL', 'manageshop', 'normal', 'fa fa-shopping-bag', 0, 1, 0, 1, 2, '2017-09-16 14:40:26', '2017-09-27 04:33:44'),
(42, 'Customer Data', 'URL', '#', 'normal', 'fa fa-user', 0, 1, 0, 1, 7, '2017-09-19 22:54:36', '2017-09-27 04:36:07'),
(51, 'Customer', 'Route', 'AdminCustomers56ControllerGetIndex', 'normal', 'fa fa-glass', 42, 1, 0, 1, 1, '2017-09-20 00:49:56', '2017-09-27 04:35:59'),
(54, 'Bank', 'Route', 'AdminBanks59ControllerGetIndex', 'normal', 'fa fa-money', 42, 1, 0, 1, 2, '2017-09-20 05:31:24', '2017-09-27 04:36:15'),
(55, 'Alamat', 'Route', 'AdminAlamatsControllerGetIndex', 'normal', 'fa fa-road', 42, 1, 0, 1, 3, '2017-09-20 14:13:07', '2017-09-27 04:36:23'),
(56, 'Material Request by User', 'Route', 'subdept.mr.mainpage', 'normal', 'fa fa-calendar-o', 0, 1, 0, 1, 1, '2017-09-20 14:51:12', '2017-10-01 17:54:04'),
(57, 'Periode MR', 'Route', 'AdminPeriodeMrsControllerGetIndex', 'normal', 'fa fa-calendar-o', 28, 1, 0, 1, 2, '2017-09-21 07:31:31', '2017-09-28 07:13:45'),
(66, 'Purchase Order MR', 'Route', 'AdminPurchaseordersControllerGetIndex', 'normal', 'fa fa-lock', 76, 1, 0, 1, 1, '2017-09-29 13:52:20', '2017-10-20 21:57:44'),
(67, 'Group Barang', 'Module', 'groups', 'normal', 'fa fa-object-group', 21, 1, 0, 1, 6, '2017-10-03 19:50:04', '2017-10-03 19:54:30'),
(68, 'Group Barang Isi', 'Module', 'groupbys', 'normal', 'fa fa-object-group', 0, 0, 0, 1, 4, '2017-10-03 19:55:21', '2017-10-03 19:57:35'),
(70, 'Pengiriman Barang', 'Route', 'index.pengiriman.barang', 'normal', 'fa fa-truck', 79, 1, 0, 1, 1, '2017-10-07 09:14:31', '2017-10-22 05:45:02'),
(72, 'List Request Edit PO', 'URL', 'listrequesteditpo', 'normal', 'fa fa-th-list', 28, 1, 0, 1, 5, '2017-10-12 22:26:05', '2017-10-12 22:27:34'),
(73, 'Request Barangs', 'Route', 'AdminRequestBarangsControllerGetIndex', 'normal', 'fa fa-glass', 28, 1, 0, 1, 3, '2017-10-13 19:59:48', NULL),
(74, 'Retur Barang', 'Route', 'AdminReturbarangsControllerGetIndex', 'normal', 'fa fa-step-backward', 28, 1, 0, 1, 7, '2017-10-15 03:15:42', '2017-10-21 21:57:33'),
(75, 'Purchase Order Pembelian', 'Route', 'purchaseorderspembelian.index', 'normal', 'fa fa-calendar-o', 76, 1, 0, 1, 2, '2017-10-16 23:43:52', '2017-10-20 16:41:45'),
(76, 'Purchase Order', 'URL', '#', 'normal', 'fa fa-phone-square', 0, 1, 0, 1, 9, '2017-10-20 21:48:34', NULL),
(77, 'List Sales Order', 'Route', 'AdminSalesOrdersControllerGetIndex', 'normal', 'fa fa-list', 81, 1, 0, 1, 1, '2017-10-22 20:47:51', '2017-10-25 05:05:36'),
(78, 'List Pengiriman', 'Route', 'index.list.pengiriman.barang', 'normal', 'fa fa-list-alt', 79, 1, 0, 1, 2, '2017-10-23 14:23:05', '2017-10-23 14:35:36'),
(79, 'Pengiriman', 'URL', '#', 'normal', 'fa fa-truck', 0, 1, 0, 1, 11, '2017-10-23 14:23:50', NULL),
(80, 'Retur Sales Order', 'Route', 'AdminRetursoControllerGetIndex', NULL, 'fa fa-step-backward', 81, 1, 0, 1, 2, '2017-10-25 04:22:42', NULL),
(81, 'Sales Order', 'URL', '#', 'normal', 'fa fa-qrcode', 0, 1, 0, 1, 10, '2017-10-25 05:04:48', NULL),
(84, 'Tanda Terima', 'Route', 'AdminTandaTerimasControllerGetIndex', NULL, 'fa fa-check-square', 0, 0, 0, 1, 2, '2017-10-29 14:52:24', NULL),
(85, 'Surat Jalan', 'Route', 'AdminSuratJalansControllerGetIndex', NULL, 'fa fa-paper-plane', 0, 0, 0, 1, 3, '2017-10-29 14:54:23', NULL),
(86, 'laporan MR', 'Route', 'laporanmr', 'normal', 'fa fa-sticky-note', 28, 1, 0, 1, 1, '2017-11-14 00:57:53', NULL),
(87, 'Purchase Order Laporan', 'Route', 'po.getLaporan', 'normal', 'fa fa-file-text-o', 0, 0, 0, 1, 1, '2017-11-14 11:04:09', '2017-11-14 11:15:44'),
(88, 'Pending Approve PR', 'Route', 'pr.getBelumApproved', 'normal', 'fa fa-th-list', 28, 1, 0, 1, 4, '2017-11-15 11:30:51', NULL),
(89, 'Revisi PO', 'Route', 'AdminRevisiPosControllerGetIndex', NULL, 'fa fa-file-text-o', 76, 1, 0, 1, 3, '2017-11-20 09:57:20', NULL),
(90, 'Laporan', 'URL', '#', 'normal', 'fa fa-book', 0, 1, 0, 1, 3, '2017-11-21 07:23:11', '2017-11-21 07:25:03'),
(91, 'Laporan MR', 'Route', 'laporan.mr.index', 'normal', 'fa fa-file-text-o', 90, 1, 0, 1, 1, '2017-11-21 07:24:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 25, 1),
(26, 26, 1),
(27, 27, 1),
(28, 28, 1),
(30, 30, 1),
(31, 31, 1),
(32, 32, 1),
(33, 33, 1),
(34, 34, 1),
(35, 35, 1),
(36, 36, 1),
(37, 37, 1),
(38, 38, 1),
(39, 39, 1),
(40, 40, 1),
(41, 41, 1),
(42, 42, 1),
(43, 43, 1),
(44, 44, 1),
(45, 45, 1),
(46, 46, 1),
(47, 47, 1),
(48, 48, 1),
(49, 49, 1),
(50, 50, 1),
(51, 51, 1),
(52, 52, 1),
(53, 53, 1),
(55, 55, 1),
(56, 54, 1),
(60, 58, 1),
(61, 59, 1),
(62, 60, 1),
(64, 62, 1),
(65, 61, 1),
(67, 63, 1),
(68, 64, 1),
(69, 65, 1),
(70, 57, 1),
(76, 56, 1),
(77, 67, 1),
(78, 68, 1),
(79, 69, 1),
(81, 71, 1),
(82, 72, 1),
(83, 73, 1),
(84, 29, 1),
(85, 75, 1),
(86, 76, 1),
(87, 66, 1),
(88, 74, 1),
(89, 70, 1),
(92, 79, 1),
(93, 78, 1),
(94, 80, 1),
(95, 81, 1),
(96, 77, 1),
(99, 84, 1),
(100, 85, 1),
(101, 86, 1),
(102, 87, 1),
(103, 88, 1),
(104, 89, 1),
(106, 91, 1),
(107, 90, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2017-09-12 06:57:35', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2017-09-12 06:57:35', NULL, NULL),
(12, 'gudang', 'fa fa-building-o', 'gudangs', 'gudangs', 'AdminGudangsController', 0, 0, '2017-09-12 07:44:22', NULL, '2017-09-12 08:28:34'),
(13, 'Kategori', 'fa fa-gear', 'kategoris', 'kategoris', 'AdminKategorisController', 0, 0, '2017-09-12 07:51:35', NULL, '2017-09-12 08:28:39'),
(14, 'Satuan', 'fa fa-glass', 'satuans', 'satuans', 'AdminSatuansController', 0, 0, '2017-09-12 07:53:29', NULL, '2017-09-12 08:28:46'),
(15, 'Supplier', 'fa fa-group', 'suppliers', 'suppliers', 'AdminSuppliersController', 0, 0, '2017-09-12 08:03:02', NULL, '2017-09-12 08:28:49'),
(16, 'Barang', 'fa fa-database', 'barangs', 'barangs', 'AdminBarangsController', 0, 0, '2017-09-12 08:04:48', NULL, '2017-09-12 08:28:29'),
(17, 'Pricelist', 'fa fa-glass', 'pricelists', 'pricelists', 'AdminPricelistsController', 0, 0, '2017-09-12 08:24:11', NULL, '2017-09-12 08:28:43'),
(18, 'Price', 'fa fa-money', 'pricelists18', 'pricelists', 'AdminPricelists18Controller', 0, 0, '2017-09-12 08:26:34', NULL, '2017-09-12 08:28:37'),
(19, 'Wellhos Department', 'fa fa-building-o', 'wellhos_departments', 'wellhos_departments', 'AdminWellhosDepartmentsController', 0, 0, '2017-09-12 11:45:15', NULL, NULL),
(20, 'Wellhos Subdepartment', 'fa fa-building-o', 'wellhos_subdepts', 'wellhos_subdepts', 'AdminWellhosSubdeptsController', 0, 0, '2017-09-12 11:46:23', NULL, NULL),
(21, 'Kategori', 'fa fa-gear', 'kategoris', 'kategoris', 'AdminKategorisController', 0, 0, '2017-09-12 11:50:04', NULL, NULL),
(22, 'Satuan', 'fa fa-glass', 'satuans', 'satuans', 'AdminSatuansController', 0, 0, '2017-09-12 11:50:54', NULL, NULL),
(23, 'Barang', 'fa fa-database', 'barangs', 'barangs', 'AdminBarangsController', 0, 0, '2017-09-12 11:51:29', NULL, NULL),
(24, 'Supplier', 'fa fa-group', 'suppliers', 'suppliers', 'AdminSuppliersController', 0, 0, '2017-09-12 11:58:24', NULL, NULL),
(25, 'Pricelist', 'fa fa-money', 'pricelists', 'pricelists', 'AdminPricelistsController', 0, 0, '2017-09-12 11:59:03', NULL, NULL),
(26, 'Gudang', 'fa fa-simplybuilt', 'gudangs', 'gudangs', 'AdminGudangsController', 0, 0, '2017-09-12 12:02:07', NULL, NULL),
(27, 'Resep', 'fa fa-book', 'reseps', 'reseps', 'AdminResepsController', 0, 0, '2017-09-12 12:04:20', NULL, NULL),
(28, 'List Bahan', 'fa fa-home', 'listbahans', 'listbahans', 'AdminListbahansController', 0, 0, '2017-09-12 12:07:27', NULL, NULL),
(29, 'Barang Gudang', 'fa fa-home', 'barang_gudangs', 'barang_gudangs', 'AdminBarangGudangs29Controller', 0, 0, '2017-09-12 12:09:08', NULL, '2017-09-12 12:14:59'),
(30, 'Barang Gudang', 'fa fa-home', 'barang_gudangs30', 'barang_gudangs', 'AdminBarangGudangs30Controller', 0, 0, '2017-09-12 12:15:20', NULL, NULL),
(31, 'Transferlog', 'fa fa-exchange', 'transferlogs', 'transferlogs', 'AdminTransferlogsController', 0, 0, '2017-09-12 12:17:34', NULL, NULL),
(32, 'Master Project', 'fa fa-file-o', 'master_projects', 'master_projects', 'AdminMasterProjectsController', 0, 0, '2017-09-15 22:08:25', NULL, NULL),
(33, 'Project', 'fa fa-book', 'projects', 'projects', 'AdminProjectsController', 0, 0, '2017-09-15 22:09:41', NULL, NULL),
(34, 'mr_subdepts', 'fa fa-glass', 'mr_subdepts', 'mr_subdepts', 'AdminMrSubdeptsController', 0, 0, '2017-09-16 01:50:06', NULL, '2017-09-16 13:23:03'),
(35, 'isi_mr_subdepts', 'fa fa-glass', 'isi_mr_subdepts', 'isi_mr_subdepts', 'AdminIsiMrSubdeptsController', 0, 0, '2017-09-16 01:57:09', NULL, '2017-09-16 13:30:54'),
(36, 'MR Subdept', 'fa fa-glass', 'mr_subdepts36', 'mr_subdepts', 'AdminMrSubdepts36Controller', 0, 0, '2017-09-16 13:23:13', NULL, NULL),
(37, 'isi mr subdept', 'fa fa-glass', 'isi_mr_subdepts37', 'isi_mr_subdepts', 'AdminIsiMrSubdepts37Controller', 0, 0, '2017-09-16 13:31:06', NULL, '2017-09-16 13:34:31'),
(38, 'Isi MR Subdept', 'fa fa-glass', 'isi_mr_subdepts38', 'isi_mr_subdepts', 'AdminIsiMrSubdepts38Controller', 0, 0, '2017-09-16 13:37:27', NULL, NULL),
(39, 'resep', 'fa fa-glass', 'reseps39', 'reseps', 'AdminReseps39Controller', 0, 0, '2017-09-16 13:48:13', NULL, '2017-09-16 13:53:42'),
(40, 'listbahan', 'fa fa-glass', 'listbahans40', 'listbahans', 'AdminListbahans40Controller', 0, 0, '2017-09-16 13:48:33', NULL, '2017-09-16 13:53:57'),
(41, 'resep', 'fa fa-glass', 'reseps41', 'reseps', 'AdminReseps41Controller', 0, 0, '2017-09-16 13:49:11', NULL, '2017-09-16 13:53:35'),
(42, 'Rekap MR', 'fa fa-glass', 'rekap_mrs', 'rekap_mrs', 'AdminRekapMrsController', 0, 0, '2017-09-16 13:54:28', NULL, NULL),
(43, 'isi rekap', 'fa fa-glass', 'isi_rekaps', 'isi_rekaps', 'AdminIsiRekapsController', 0, 0, '2017-09-16 13:57:45', NULL, '2017-09-16 13:58:46'),
(44, 'Isi Rekap', 'fa fa-glass', 'isi_rekaps44', 'isi_rekaps', 'AdminIsiRekaps44Controller', 0, 0, '2017-09-16 13:58:58', NULL, NULL),
(45, 'Customers', 'fa fa-group', 'customers', 'customers', 'AdminCustomersController', 0, 0, '2017-09-19 22:47:57', NULL, '2017-09-19 23:42:46'),
(46, 'alamat_customers', 'fa fa-search', 'alamatcustomers', 'alamatcustomers', 'AdminAlamatcustomersController', 0, 0, '2017-09-19 22:53:12', NULL, '2017-09-19 23:58:40'),
(47, 'bank', 'fa fa-money', 'banks', 'banks', 'AdminBanksController', 0, 0, '2017-09-19 22:53:55', NULL, '2017-09-20 00:02:52'),
(48, 'customers', 'fa fa-glass', 'customers', 'customers', 'AdminCustomersController', 0, 0, '2017-09-19 23:43:24', NULL, '2017-09-19 23:45:17'),
(49, 'customers', 'fa fa-glass', 'customers49', 'customers', 'AdminCustomers49Controller', 0, 0, '2017-09-19 23:47:58', NULL, '2017-09-20 00:36:46'),
(50, 'alamat_customers', 'fa fa-glass', 'alamat_customers', 'alamat_customers', 'AdminAlamatCustomersController', 0, 0, '2017-09-20 00:01:22', NULL, '2017-09-20 00:02:45'),
(51, 'alamat_customers', 'fa fa-glass', 'alamat_customers51', 'alamat_customers', 'AdminAlamatCustomers51Controller', 0, 0, '2017-09-20 00:04:30', NULL, '2017-09-20 00:07:14'),
(52, 'alamat', 'fa fa-glass', 'alamat', 'alamat', 'AdminAlamatController', 0, 0, '2017-09-20 00:06:22', NULL, '2017-09-20 00:07:10'),
(53, 'alamat', 'fa fa-search', 'alamat53', 'alamat', 'AdminAlamat53Controller', 0, 0, '2017-09-20 00:09:25', NULL, '2017-09-20 00:25:51'),
(54, 'alamatcustomers', 'fa fa-glass', 'alamatcustomers54', 'alamatcustomers', 'AdminAlamatcustomers54Controller', 0, 0, '2017-09-20 00:31:21', NULL, '2017-09-20 00:34:32'),
(55, 'alamatcustomers', 'fa fa-glass', 'alamatcustomers55', 'alamatcustomers', 'AdminAlamatcustomers55Controller', 0, 0, '2017-09-20 00:34:43', NULL, '2017-09-20 00:36:42'),
(56, 'Customer', 'fa fa-glass', 'customers56', 'customers', 'AdminCustomers56Controller', 0, 0, '2017-09-20 00:49:56', NULL, NULL),
(57, 'Alamat', 'fa fa-glass', 'alamats', 'alamats', 'AdminAlamatsController', 0, 0, '2017-09-20 00:51:58', NULL, NULL),
(58, 'banks', 'fa fa-glass', 'banks58', 'banks', 'AdminBanks58Controller', 0, 0, '2017-09-20 04:36:34', NULL, '2017-09-20 05:31:15'),
(59, 'Bank', 'fa fa-glass', 'banks59', 'banks', 'AdminBanks59Controller', 0, 0, '2017-09-20 05:31:24', NULL, NULL),
(60, 'Periode MR', 'fa fa-calendar-o', 'periode_mrs', 'periode_mrs', 'AdminPeriodeMrsController', 0, 0, '2017-09-21 07:31:31', NULL, NULL),
(61, 'MR Departments', 'fa fa-glass', 'mr_departments', 'mr_departments', 'AdminMrDepartmentsController', 0, 0, '2017-09-23 23:23:18', NULL, '2017-09-23 23:26:18'),
(62, 'mr departments', 'fa fa-glass', 'mr_departments62', 'mr_departments', 'AdminMrDepartments62Controller', 0, 0, '2017-09-23 23:26:34', NULL, '2017-09-23 23:27:04'),
(63, 'MR Departments', 'fa fa-glass', 'mr_departments63', 'mr_departments', 'AdminMrDepartments63Controller', 0, 0, '2017-09-23 23:27:40', NULL, NULL),
(64, 'Isi MR Department', 'fa fa-glass', 'isi_mr_departments', 'isi_mr_departments', 'AdminIsiMrDepartmentsController', 0, 0, '2017-09-23 23:53:31', NULL, NULL),
(65, 'Lock Rekaps', 'fa fa-glass', 'lock_rekaps', 'lock_rekaps', 'AdminLockRekapsController', 0, 0, '2017-09-24 18:11:04', NULL, '2017-09-24 19:33:25'),
(66, 'lock rekaps', 'fa fa-glass', 'lock_rekaps66', 'lock_rekaps', 'AdminLockRekaps66Controller', 0, 0, '2017-09-24 19:33:47', NULL, '2017-09-25 02:30:52'),
(67, 'Lock Rekaps', 'fa fa-glass', 'lock_rekaps', 'lock_rekaps', 'AdminLockRekapsController', 0, 0, '2017-09-25 02:31:10', NULL, NULL),
(68, 'Purchase Order', 'fa fa-lock', 'purchaseorders', 'purchaseorders', 'AdminPurchaseordersController', 0, 0, '2017-09-29 13:52:20', NULL, NULL),
(69, 'Isi Purchase Order', 'fa fa-unlock', 'isi_pos', 'isi_pos', 'AdminIsiPosController', 0, 0, '2017-09-29 13:56:00', NULL, NULL),
(70, 'Surat Terima Barang', 'fa fa-truck', 'barangdatangs', 'barangdatangs', 'AdminBarangdatangsController', 0, 0, '2017-10-01 08:25:45', NULL, NULL),
(71, 'Isi SPBM', 'fa fa-list', 'isispbms', 'isispbms', 'AdminIsispbmsController', 0, 0, '2017-10-01 08:43:32', NULL, NULL),
(72, 'Groups', 'fa fa-glass', 'groups', 'groups', 'AdminGroupsController', 0, 0, '2017-10-03 19:50:04', NULL, NULL),
(73, 'Groupbys', 'fa fa-lock', 'groupbys', 'groupbys', 'AdminGroupbysController', 0, 0, '2017-10-03 19:55:21', NULL, NULL),
(74, 'pengiriman', 'fa fa-truck', 'pengiriman_barangs', 'pengiriman_barangs', 'AdminPengirimansController', 0, 0, '2017-10-07 09:10:49', NULL, '2017-10-18 07:42:30'),
(75, 'request_barangs', 'fa fa-glass', 'request_barangs', 'request_barangs', 'AdminRequestBarangsController', 0, 0, '2017-10-15 03:15:42', NULL, NULL),
(76, 'returbarang', 'fa fa-step-backward', 'Returbarangs', 'Returbarangs', 'AdminReturbarangsController', 0, 0, '2017-10-13 19:59:48', NULL, NULL),
(77, 'Pengiriman Barang', 'fa fa-truck', 'pengiriman_barangs77', 'pengiriman_barangs', 'AdminPengirimanBarangs77Controller', 0, 0, '2017-10-18 07:41:22', NULL, NULL),
(78, 'Sales Order', 'fa fa-qrcode', 'sales_orders', 'sales_orders', 'AdminSalesOrdersController', 0, 0, '2017-10-22 20:47:51', NULL, NULL),
(79, 'Isi Sales Orders', 'fa fa-glass', 'isi_sales_orders', 'isi_sales_orders', 'AdminIsiSalesOrdersController', 0, 0, '2017-10-22 20:59:00', NULL, NULL),
(80, 'Retur Sales Order', 'fa fa-step-backward', 'returso', 'returso', 'AdminRetursoController', 0, 0, '2017-10-25 04:22:42', NULL, NULL),
(81, 'Isi Retur Sales Order', 'fa fa-glass', 'isi_returso', 'isi_returso', 'AdminIsiRetursoController', 0, 0, '2017-10-25 04:23:16', NULL, NULL),
(82, 'Tanda Terima', 'fa fa-check-square', 'tanda_terimas', 'tanda_terimas', 'AdminTandaTerimasController', 0, 0, '2017-10-29 14:52:24', NULL, NULL),
(83, 'Surat Jalan', 'fa fa-paper-plane', 'surat_jalans', 'surat_jalans', 'AdminSuratJalansController', 0, 0, '2017-10-29 14:54:23', NULL, NULL),
(84, 'Revisi PO', 'fa fa-file-text-o', 'revisi_pos', 'revisi_pos', 'AdminRevisiPosController', 0, 0, '2017-11-20 09:57:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2017-09-12 06:57:35', NULL),
(2, 'Master', 0, 'skin-purple', NULL, NULL),
(3, 'Kepala_Sub_Department', 0, 'skin-green', NULL, NULL),
(4, 'Kepala_Department', 0, 'skin-yellow', NULL, NULL),
(5, 'Client', 0, 'skin-blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2017-09-12 06:57:35', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2017-09-12 06:57:35', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2017-09-12 06:57:35', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2017-09-12 06:57:35', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2017-09-12 06:57:35', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2017-09-12 06:57:35', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2017-09-12 06:57:35', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2017-09-12 06:57:35', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2017-09-12 06:57:35', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2017-09-12 06:57:35', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2017-09-12 06:57:35', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(21, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(22, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(23, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(24, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(25, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(26, 1, 1, 1, 1, 1, 1, 26, NULL, NULL),
(27, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(28, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(29, 1, 1, 1, 1, 1, 1, 29, NULL, NULL),
(30, 1, 1, 1, 1, 1, 1, 30, NULL, NULL),
(31, 1, 1, 1, 1, 1, 1, 31, NULL, NULL),
(32, 1, 1, 1, 1, 1, 1, 32, NULL, NULL),
(33, 1, 1, 1, 1, 1, 1, 33, NULL, NULL),
(34, 1, 1, 1, 1, 1, 1, 34, NULL, NULL),
(35, 1, 1, 1, 1, 1, 1, 35, NULL, NULL),
(36, 1, 1, 1, 1, 1, 1, 36, NULL, NULL),
(37, 1, 1, 1, 1, 1, 1, 37, NULL, NULL),
(38, 1, 1, 1, 1, 1, 1, 38, NULL, NULL),
(39, 1, 1, 1, 1, 1, 1, 39, NULL, NULL),
(40, 1, 1, 1, 1, 1, 1, 40, NULL, NULL),
(41, 1, 1, 1, 1, 1, 1, 41, NULL, NULL),
(42, 1, 1, 1, 1, 1, 1, 42, NULL, NULL),
(43, 1, 1, 1, 1, 1, 1, 43, NULL, NULL),
(44, 1, 1, 1, 1, 1, 1, 44, NULL, NULL),
(45, 1, 1, 1, 1, 1, 2, 23, NULL, NULL),
(46, 1, 1, 1, 1, 1, 2, 26, NULL, NULL),
(47, 1, 1, 1, 1, 1, 2, 21, NULL, NULL),
(48, 1, 1, 1, 1, 1, 2, 25, NULL, NULL),
(49, 1, 1, 1, 1, 1, 2, 22, NULL, NULL),
(50, 1, 1, 1, 1, 1, 2, 24, NULL, NULL),
(51, 1, 0, 0, 0, 0, 3, 23, NULL, NULL),
(52, 1, 0, 0, 0, 0, 3, 30, NULL, NULL),
(53, 1, 1, 0, 1, 0, 3, 38, NULL, NULL),
(54, 1, 0, 0, 0, 0, 3, 21, NULL, NULL),
(55, 1, 0, 0, 0, 0, 3, 28, NULL, NULL),
(56, 1, 1, 0, 1, 0, 3, 36, NULL, NULL),
(57, 1, 0, 0, 0, 0, 3, 25, NULL, NULL),
(58, 1, 0, 0, 0, 0, 3, 33, NULL, NULL),
(59, 1, 0, 0, 0, 0, 3, 22, NULL, NULL),
(60, 1, 0, 0, 0, 0, 3, 24, NULL, NULL),
(61, 1, 0, 0, 0, 0, 3, 19, NULL, NULL),
(62, 1, 0, 0, 0, 0, 3, 20, NULL, NULL),
(63, 1, 0, 1, 0, 0, 4, 23, NULL, NULL),
(64, 1, 0, 1, 0, 0, 4, 30, NULL, NULL),
(65, 1, 0, 1, 0, 0, 4, 26, NULL, NULL),
(66, 1, 0, 1, 1, 0, 4, 38, NULL, NULL),
(67, 1, 1, 1, 1, 0, 4, 44, NULL, NULL),
(68, 1, 0, 1, 0, 0, 4, 21, NULL, NULL),
(69, 1, 0, 1, 0, 0, 4, 28, NULL, NULL),
(70, 1, 0, 1, 1, 0, 4, 36, NULL, NULL),
(71, 1, 0, 1, 0, 0, 4, 25, NULL, NULL),
(72, 1, 1, 1, 1, 0, 4, 42, NULL, NULL),
(73, 1, 0, 1, 0, 0, 4, 27, NULL, NULL),
(74, 1, 0, 1, 0, 0, 4, 22, NULL, NULL),
(75, 1, 0, 1, 0, 0, 4, 24, NULL, NULL),
(76, 1, 1, 1, 1, 0, 4, 31, NULL, NULL),
(77, 1, 0, 1, 0, 0, 4, 19, NULL, NULL),
(78, 1, 0, 0, 0, 0, 5, 23, NULL, NULL),
(79, 1, 0, 0, 0, 0, 5, 21, NULL, NULL),
(80, 1, 0, 0, 0, 0, 5, 22, NULL, NULL),
(81, 1, 0, 0, 0, 0, 3, 26, NULL, NULL),
(82, 1, 1, 1, 1, 1, 3, 27, NULL, NULL),
(83, 1, 0, 0, 0, 0, 3, 31, NULL, NULL),
(84, 1, 0, 1, 0, 0, 4, 33, NULL, NULL),
(85, 1, 0, 1, 0, 0, 4, 20, NULL, NULL),
(86, 1, 1, 1, 1, 1, 2, 30, NULL, NULL),
(87, 1, 0, 1, 0, 1, 2, 38, NULL, NULL),
(88, 1, 0, 1, 0, 1, 2, 44, NULL, NULL),
(89, 1, 1, 1, 1, 1, 2, 28, NULL, NULL),
(90, 1, 1, 1, 1, 1, 2, 32, NULL, NULL),
(91, 1, 0, 1, 0, 1, 2, 36, NULL, NULL),
(92, 1, 1, 1, 1, 1, 2, 33, NULL, NULL),
(93, 1, 0, 1, 0, 1, 2, 42, NULL, NULL),
(94, 1, 1, 1, 1, 1, 2, 27, NULL, NULL),
(95, 1, 0, 1, 1, 1, 2, 31, NULL, NULL),
(96, 1, 1, 1, 1, 1, 2, 19, NULL, NULL),
(97, 1, 1, 1, 1, 1, 2, 20, NULL, NULL),
(98, 1, 1, 1, 1, 1, 1, 77, NULL, NULL),
(99, 1, 1, 1, 1, 1, 1, 78, NULL, NULL),
(100, 1, 1, 1, 1, 1, 1, 79, NULL, NULL),
(101, 1, 1, 1, 1, 1, 1, 80, NULL, NULL),
(102, 1, 1, 1, 1, 1, 1, 81, NULL, NULL),
(103, 1, 1, 1, 1, 1, 1, 82, NULL, NULL),
(104, 1, 1, 1, 1, 1, 1, 83, NULL, NULL),
(105, 1, 1, 1, 1, 1, 1, 84, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2017-11-01 03:51:46', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2017-11-01 03:51:46', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2017-11-01 03:51:46', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2017-11-01 03:51:46', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Wellhos', 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2017-11-01 03:51:46', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', NULL, 'upload_image', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', NULL, 'upload_image', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2017-11-01 03:51:46', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$ebzeWmn9Yn3ym0LjrO7WketkAhimWuSLDwDw.Q8UiPmYVNv7q6tY.', 1, '2017-09-12 06:57:35', NULL, 'Active'),
(2, 'UserCoba', 'uploads/1/2017-10/battlechart.png', 'user@user.user', '$2y$10$XRB7V6ccxWG50F2WOT6BJO7Vzf60fdmtxNQWAr8z4Igeat/euO8hO', 5, '2017-10-20 04:02:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nocustomer` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat invoice` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notelp` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namapemilik` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamatpemilik` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noktp` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `deleted_at`, `created_at`, `updated_at`, `nocustomer`, `nama`, `alamat invoice`, `npwp`, `notelp`, `jenis`, `namapemilik`, `alamatpemilik`, `noktp`, `top`, `user_id`) VALUES
(1, NULL, '2017-09-20 00:51:12', '2017-09-20 00:51:24', '1', 'lol', '', '1', '1', '1', '1', '1', '1', 0, 0),
(2, NULL, '2017-09-20 09:16:06', NULL, 'c2', 'c22', '', 'c2', 'c2', 'c2', 'c2', 'c2', 'c2', 0, 0),
(3, NULL, '2017-09-20 09:16:21', NULL, 'c3', 'c33', '', 'c3', 'c3', 'c3', 'c3', 'c3', 'c3', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengambilan_barangs`
--

CREATE TABLE `detail_pengambilan_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pengambilan_barang_id` int(10) UNSIGNED DEFAULT NULL,
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengiriman_barangs`
--

CREATE TABLE `detail_pengiriman_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pengiriman_barang_id` int(10) UNSIGNED DEFAULT NULL,
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diskons`
--

CREATE TABLE `diskons` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `persen` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `harga` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groupbys`
--

CREATE TABLE `groupbys` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groupbys`
--

INSERT INTO `groupbys` (`id`, `created_at`, `updated_at`, `deleted_at`, `barang_id`, `group_id`) VALUES
(1, '2017-10-16 10:09:02', NULL, NULL, 5, 1),
(2, '2017-10-16 10:09:08', NULL, NULL, 4, 1),
(3, '2017-10-16 10:09:49', NULL, NULL, 7, 2),
(4, '2017-10-16 10:09:53', NULL, NULL, 8, 2),
(5, '2017-10-16 10:09:57', NULL, NULL, 10, 2),
(6, '2017-10-16 10:10:41', NULL, NULL, 5, 3),
(7, '2017-10-16 10:10:48', NULL, NULL, 9, 3),
(8, '2017-10-16 10:11:40', NULL, NULL, 12, 4),
(9, '2017-10-16 10:11:47', NULL, NULL, 15, 4),
(10, '2017-10-16 10:11:58', NULL, NULL, 5, 5),
(11, '2017-10-16 10:12:01', NULL, NULL, 10, 5),
(12, '2017-10-16 10:12:05', NULL, NULL, 14, 5),
(13, '2017-10-16 10:12:12', NULL, NULL, 14, 5),
(14, '2017-10-23 03:00:00', NULL, NULL, 16, 6),
(15, '2017-10-23 03:00:06', NULL, NULL, 18, 6),
(16, '2017-10-23 03:00:11', NULL, NULL, 19, 6),
(17, '2017-10-23 03:00:15', NULL, NULL, 20, 6),
(18, '2017-10-23 03:00:18', NULL, NULL, 21, 6),
(19, '2017-10-23 03:00:26', NULL, NULL, 22, 6),
(20, '2017-10-23 03:00:34', NULL, NULL, 23, 6),
(21, '2017-10-23 03:00:42', NULL, NULL, 24, 6),
(22, '2017-10-23 03:00:47', NULL, NULL, 25, 6),
(23, '2017-10-23 03:00:54', NULL, NULL, 26, 6);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `counter` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `created_at`, `updated_at`, `deleted_at`, `nama`, `counter`) VALUES
(1, '2017-10-16 10:08:53', '2017-10-23 01:32:45', NULL, 'Sikat1', 1),
(2, '2017-10-16 10:09:37', '2017-10-16 10:10:33', NULL, 'SAPU3', 0),
(3, '2017-10-16 10:10:22', '2017-10-22 22:17:24', NULL, 'Sikat dan Sapu 01', 2),
(4, '2017-10-16 10:11:20', '2017-10-22 22:17:37', NULL, 'Soda', 1),
(5, '2017-10-16 10:11:29', NULL, NULL, 'Campur01', 0),
(6, '2017-10-23 02:59:53', NULL, NULL, 'Groupb0001-b0010', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gudangs`
--

CREATE TABLE `gudangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notelp` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gudangs`
--

INSERT INTO `gudangs` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`, `kode`, `notelp`, `email`, `pic`) VALUES
(1, NULL, '2017-09-12 12:03:03', NULL, 'gudang 1', 'g01', '00000', 'gudang1@gmail.com', 'mr g01'),
(2, NULL, '2017-09-16 07:08:35', NULL, 'gudang 2', 'g2', '000000', 'gudang2@gmail.com', 'bp gudang 2'),
(3, NULL, '2017-09-16 07:08:51', NULL, 'gudang 3', 'g3', '000000', 'gudang3@gmail.com', 'bp gudang 3'),
(4, NULL, '2017-09-24 22:20:27', NULL, 'gudang 4', 'g4', '0298 33321', 'gudang4@gmail.com', 'bp budi'),
(5, NULL, '2017-11-05 01:59:53', NULL, 'gudang_cacat', 'g-5', '0000000000', 'gudang@cacat.cacat', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `harga_juals`
--

CREATE TABLE `harga_juals` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `harga_jual` int(10) UNSIGNED DEFAULT '0',
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `ket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_diskon` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `harga_juals`
--

INSERT INTO `harga_juals` (`id`, `created_at`, `updated_at`, `deleted_at`, `harga_jual`, `barang_id`, `ket`, `harga_diskon`) VALUES
(1, NULL, NULL, NULL, 5000, 10, NULL, 2000),
(2, NULL, NULL, NULL, 1000, 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `isispbms`
--

CREATE TABLE `isispbms` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `barangdatangs_id` int(10) UNSIGNED DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `kondisi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_mr_departments`
--

CREATE TABLE `isi_mr_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `mr_departments_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_mr_subdepts`
--

CREATE TABLE `isi_mr_subdepts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `mr_subdepts_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `total` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_pos`
--

CREATE TABLE `isi_pos` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `purchaseorders_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `harga` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_rekaps`
--

CREATE TABLE `isi_rekaps` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `keterangan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_returso`
--

CREATE TABLE `isi_returso` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `returso_id` int(10) UNSIGNED DEFAULT NULL,
  `isi_sales_order_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `isi_sales_orders`
--

CREATE TABLE `isi_sales_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sales_orders_id` int(10) UNSIGNED DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT NULL,
  `total` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategoris`
--

CREATE TABLE `kategoris` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategoris`
--

INSERT INTO `kategoris` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`, `kode`) VALUES
(1, NULL, '2017-09-12 11:54:06', '2017-10-08 17:14:30', 'Sikat', 'k1'),
(2, NULL, '2017-09-12 11:54:12', '2017-10-08 17:30:05', 'Sapu', 'k2'),
(3, NULL, '2017-09-16 07:09:44', '2017-10-08 17:44:35', 'Sabun', 'k3'),
(4, NULL, '2017-09-16 07:09:49', NULL, 'kategori 4', 'k4');

-- --------------------------------------------------------

--
-- Table structure for table `listbahans`
--

CREATE TABLE `listbahans` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barang_gudangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `reseps_id` int(10) UNSIGNED DEFAULT NULL,
  `display` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `listrequesteditpos`
--

CREATE TABLE `listrequesteditpos` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `catatan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `users_id` int(10) UNSIGNED DEFAULT NULL,
  `purchaseorders_id` int(10) UNSIGNED DEFAULT '0',
  `counter` int(10) UNSIGNED DEFAULT '0',
  `status` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lock_rekaps`
--

CREATE TABLE `lock_rekaps` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0',
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `pricelists_id` int(10) UNSIGNED DEFAULT NULL,
  `periode_mrs_id` int(10) UNSIGNED DEFAULT NULL,
  `approve` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logpurchaserequests`
--

CREATE TABLE `logpurchaserequests` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lock_rekaps_id` int(10) UNSIGNED DEFAULT NULL,
  `action` int(10) UNSIGNED DEFAULT NULL,
  `act` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logspbms`
--

CREATE TABLE `logspbms` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spbms_id` int(10) UNSIGNED DEFAULT NULL,
  `barangdatangs_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_mrs`
--

CREATE TABLE `log_mrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_action` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` int(10) UNSIGNED DEFAULT NULL,
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `mr_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_pengirimans`
--

CREATE TABLE `log_pengirimans` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `pengiriman_brg_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_projects`
--

CREATE TABLE `master_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no telp` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_10_18_050615_create_alamats_table', 1),
(2, '2017_10_18_050615_create_assign_users_table', 1),
(3, '2017_10_18_050615_create_banks_table', 1),
(4, '2017_10_18_050615_create_barang_gudangs_table', 1),
(5, '2017_10_18_050615_create_barangdatangs_table', 1),
(6, '2017_10_18_050615_create_barangs_table', 1),
(7, '2017_10_18_050615_create_cms_apicustom_table', 1),
(8, '2017_10_18_050615_create_cms_apikey_table', 1),
(9, '2017_10_18_050615_create_cms_dashboard_table', 1),
(10, '2017_10_18_050615_create_cms_email_queues_table', 1),
(11, '2017_10_18_050615_create_cms_email_templates_table', 1),
(12, '2017_10_18_050615_create_cms_logs_table', 1),
(13, '2017_10_18_050615_create_cms_menus_privileges_table', 1),
(14, '2017_10_18_050615_create_cms_menus_table', 1),
(15, '2017_10_18_050615_create_cms_moduls_table', 1),
(16, '2017_10_18_050615_create_cms_notifications_table', 1),
(17, '2017_10_18_050615_create_cms_privileges_roles_table', 1),
(18, '2017_10_18_050615_create_cms_privileges_table', 1),
(19, '2017_10_18_050615_create_cms_settings_table', 1),
(20, '2017_10_18_050615_create_cms_statistic_components_table', 1),
(21, '2017_10_18_050615_create_cms_statistics_table', 1),
(22, '2017_10_18_050615_create_cms_users_table', 1),
(23, '2017_10_18_050615_create_customers_table', 1),
(24, '2017_10_18_050615_create_groupbys_table', 1),
(25, '2017_10_18_050615_create_groups_table', 1),
(26, '2017_10_18_050615_create_gudangs_table', 1),
(27, '2017_10_18_050615_create_harga_juals_table', 1),
(28, '2017_10_18_050615_create_isi_mr_departments_table', 1),
(29, '2017_10_18_050615_create_isi_mr_subdepts_table', 1),
(30, '2017_10_18_050615_create_isi_pos_table', 1),
(31, '2017_10_18_050615_create_isi_rekaps_table', 1),
(32, '2017_10_18_050615_create_isispbms_table', 1),
(33, '2017_10_18_050615_create_kategoris_table', 1),
(34, '2017_10_18_050615_create_listbahans_table', 1),
(35, '2017_10_18_050615_create_listrequesteditpos_table', 1),
(36, '2017_10_18_050615_create_lock_rekaps_table', 1),
(37, '2017_10_18_050615_create_log_mrs_table', 1),
(38, '2017_10_18_050615_create_logpurchaserequests_table', 1),
(39, '2017_10_18_050615_create_logspbms_table', 1),
(40, '2017_10_18_050615_create_master_projects_table', 1),
(41, '2017_10_18_050615_create_mr_departments_table', 1),
(42, '2017_10_18_050615_create_mr_subdepts_table', 1),
(43, '2017_10_18_050615_create_pengambilanbarangs_table', 1),
(44, '2017_10_18_050615_create_periode_mrs_table', 1),
(45, '2017_10_18_050615_create_pricelists_table', 1),
(46, '2017_10_18_050615_create_projects_table', 1),
(47, '2017_10_18_050615_create_purchaseorders_table', 1),
(48, '2017_10_18_050615_create_rekap_mrs_table', 1),
(49, '2017_10_18_050615_create_request_barangs_table', 1),
(50, '2017_10_18_050615_create_reseps_table', 1),
(51, '2017_10_18_050615_create_returbarangs_table', 1),
(52, '2017_10_18_050615_create_satuans_table', 1),
(53, '2017_10_18_050615_create_split_logs_table', 1),
(54, '2017_10_18_050615_create_suppliers_table', 1),
(55, '2017_10_18_050615_create_transferlogs_table', 1),
(56, '2017_10_18_050615_create_wellhos_departments_table', 1),
(57, '2017_10_18_050615_create_wellhos_subdepts_table', 1),
(58, '2017_10_18_051121_create_pengiriman_barangs', 1),
(59, '2017_10_18_051446_create_detail_pengiriman_barangs', 1),
(60, '2017_10_18_051538_create_pengambilan_barangs', 1),
(61, '2017_10_18_051603_create_detail_pengambilan_barangs', 1),
(62, '2017_10_18_121608_add_column_gudang_id', 1),
(63, '2017_10_19_115309_tambah_field_gambar_request_barangs', 1),
(64, '2017_10_21_073313_create_table_sales_orders', 1),
(65, '2017_10_25_040847_add_status_table_pengiriman', 1),
(66, '2017_10_29_200008_add_field_pengiriman_id', 1),
(67, '2017_10_29_203450_table_surat_jalan', 1),
(68, '2017_10_29_203506_table_tanda_terima', 1),
(69, '2017_10_31_063611_perbaiki_kesalahan_rase', 1),
(70, '2017_10_31_101957_bikin_isi_returso', 1),
(71, '2017_10_31_102549_create_returso_123', 1),
(72, '2017_11_01_102153_edit_pengambilan_pengiriman_barangs', 1),
(73, '2017_11_01_112519_log_pengirimans', 1),
(74, '2017_11_05_072310_update_isi_returso', 1),
(75, '2017_11_05_074531_add_kode_kirim_surat_jalans', 1),
(76, '2017_11_05_084133_edit_tanda_terimas', 1),
(77, '2017_11_07_224612_create_addcolumnperiodemrs_tables', 1),
(78, '2017_11_10_131504_update_periodemrs_status', 1),
(79, '2017_11_10_141146_update_table_isi_so_tambah_harga', 1),
(80, '2017_11_10_141800_update_table_customers_x_suppliers_tambah_TOP', 1),
(81, '2017_11_10_162140_create_table_diskon', 1),
(82, '2017_11_11_103204_update_customer_field_user_id', 1),
(83, '2017_11_14_091534_create_addtotalmr_tables', 1),
(84, '2017_11_14_113649_update_field_mr_harga', 1),
(85, '2017_11_17_171834_create_table_porevisi', 1),
(86, '2017_11_20_223758_update_table_revisi_po_field_pengaju', 1),
(87, '2017_11_22_220604_update_table_mr_subdept', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mr_departments`
--

CREATE TABLE `mr_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nomr` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periode_mrs_id` int(10) UNSIGNED DEFAULT NULL,
  `wellhos_departments_id` int(10) UNSIGNED DEFAULT NULL,
  `approval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mr_subdepts`
--

CREATE TABLE `mr_subdepts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `projects_id` int(10) UNSIGNED DEFAULT NULL,
  `wellhos_subdepts_id` int(10) UNSIGNED DEFAULT NULL,
  `periode_mrs_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nomr` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gudang` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengambilanbarangs`
--

CREATE TABLE `pengambilanbarangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isipengirimans_id` int(10) UNSIGNED DEFAULT NULL,
  `barang_gudangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengambilan_barangs`
--

CREATE TABLE `pengambilan_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tgl_ambil` date DEFAULT NULL,
  `user` int(10) UNSIGNED DEFAULT NULL,
  `gudang_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_barangs`
--

CREATE TABLE `pengiriman_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tgl_kirim` date DEFAULT NULL,
  `kode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `global_id` int(10) UNSIGNED DEFAULT NULL,
  `user` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengambilan_brg_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `periode_mrs`
--

CREATE TABLE `periode_mrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `judul` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tglmulai` date DEFAULT NULL,
  `tglselesai` date DEFAULT NULL,
  `catatan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selesai` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tahun` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pricelists`
--

CREATE TABLE `pricelists` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` int(10) UNSIGNED DEFAULT '0',
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `suppliers_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pricelists`
--

INSERT INTO `pricelists` (`id`, `deleted_at`, `created_at`, `updated_at`, `kode`, `nama`, `harga`, `barangs_id`, `suppliers_id`) VALUES
(1, NULL, '2017-09-12 12:00:57', NULL, 'pricelist001', 'pricelist001', 5000, 1, 1),
(2, NULL, '2017-09-25 11:21:06', NULL, 'pricelist002', 'pricelist002', 50000, 1, 3),
(3, NULL, '2017-09-25 11:21:21', NULL, 'pricelist003', 'pricelist003', 6000, 2, 2),
(4, NULL, '2017-09-25 11:21:35', NULL, 'pricelist004', 'pricelist005', 7000, 4, 2),
(5, NULL, '2017-09-25 11:21:54', NULL, 'pricelist005', 'pricelist005', 30000, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `master_projects_id` int(10) UNSIGNED DEFAULT NULL,
  `PIC` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saldo` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorders`
--

CREATE TABLE `purchaseorders` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nopo` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tglpo` date DEFAULT NULL,
  `kodesp` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alteralamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL,
  `jenispo` int(10) UNSIGNED DEFAULT '0',
  `ongkir` int(10) UNSIGNED DEFAULT '0',
  `ppn` int(10) UNSIGNED DEFAULT '0',
  `periode_mrs_id` int(10) UNSIGNED DEFAULT NULL,
  `tempo` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rekap_mrs`
--

CREATE TABLE `rekap_mrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `norekap` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_barangs`
--

CREATE TABLE `request_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reseps`
--

CREATE TABLE `reseps` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya_proses` int(10) UNSIGNED DEFAULT '0',
  `barangs_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `returbarangs`
--

CREATE TABLE `returbarangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `noretur` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalretur` int(10) UNSIGNED DEFAULT NULL,
  `barangdatangs_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `returso`
--

CREATE TABLE `returso` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sales_order_id` int(10) UNSIGNED DEFAULT NULL,
  `nomor` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `returso`
--

INSERT INTO `returso` (`id`, `deleted_at`, `created_at`, `updated_at`, `sales_order_id`, `nomor`, `status`, `keterangan`) VALUES
(1, NULL, '2017-10-30 17:00:00', NULL, 1, 'no123', 'apalah', 'buang panas');

-- --------------------------------------------------------

--
-- Table structure for table `revisi_pos`
--

CREATE TABLE `revisi_pos` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isi_pos_id` int(10) UNSIGNED DEFAULT NULL,
  `harga_po` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `harga_baru` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tgl_revisi` date DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `status` enum('new','approve','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_orders`
--

CREATE TABLE `sales_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nomor` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `total` int(10) UNSIGNED DEFAULT NULL,
  `metode_bayar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `satuans`
--

CREATE TABLE `satuans` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `satuans`
--

INSERT INTO `satuans` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`) VALUES
(1, NULL, '2017-09-12 11:54:21', NULL, 'kilogram'),
(2, NULL, '2017-09-12 11:54:27', NULL, 'centimeter'),
(3, NULL, '2017-09-12 11:54:31', NULL, 'liter'),
(4, NULL, '2017-10-08 17:47:16', NULL, 'gram');

-- --------------------------------------------------------

--
-- Table structure for table `split_logs`
--

CREATE TABLE `split_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barangs_id` int(10) UNSIGNED DEFAULT NULL,
  `barang_gudangs_id_1` int(10) UNSIGNED DEFAULT NULL,
  `barang_gudangs_id_2` int(10) UNSIGNED DEFAULT NULL,
  `gudangs_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notelp` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `deleted_at`, `created_at`, `updated_at`, `nama`, `email`, `notelp`, `alamat`, `pic`, `top`) VALUES
(1, NULL, '2017-09-12 12:00:37', NULL, 'supplier1', 'supplier1@gmail.com', '0000', '0000', 'mr sup1', 0),
(2, NULL, '2017-09-16 07:09:35', NULL, 'supplier2', 'supplier2@gmail.com', '00000', 'jl supplier 2', 'bp supplier2', 0),
(3, NULL, '2017-09-24 22:22:42', NULL, 'supplier D', 'supplierD@gmail.com', '0333123', 'Jl Jendral Sudirman 32, Jl Ahman Yani 32', 'bp budi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalans`
--

CREATE TABLE `surat_jalans` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tgl_cetak` date DEFAULT NULL,
  `user` int(10) UNSIGNED DEFAULT NULL,
  `pengiriman_kode` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tanda_terimas`
--

CREATE TABLE `tanda_terimas` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tgl_cetak` date DEFAULT NULL,
  `tgl_terima` date DEFAULT NULL,
  `user` int(10) UNSIGNED DEFAULT NULL,
  `diterima` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_nota` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nota_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transferlogs`
--

CREATE TABLE `transferlogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logcode` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `approve` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gudangs_id_dari` int(10) UNSIGNED DEFAULT NULL,
  `gudangs_id_ke` int(10) UNSIGNED DEFAULT NULL,
  `stok_sblm_transfer_d` int(10) UNSIGNED DEFAULT '0',
  `stok_sblm_transfer_k` int(10) UNSIGNED DEFAULT '0',
  `stok_stlh_transfer_d` int(10) UNSIGNED DEFAULT '0',
  `stok_stlh_transfer_k` int(10) UNSIGNED DEFAULT '0',
  `jml_transfer` int(10) UNSIGNED DEFAULT '0',
  `barang_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wellhos_departments`
--

CREATE TABLE `wellhos_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wellhos_subdepts`
--

CREATE TABLE `wellhos_subdepts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wellhos_departments_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alamats`
--
ALTER TABLE `alamats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_users`
--
ALTER TABLE `assign_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barangdatangs`
--
ALTER TABLE `barangdatangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_gudangs`
--
ALTER TABLE `barang_gudangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pengambilan_barangs`
--
ALTER TABLE `detail_pengambilan_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pengiriman_barangs`
--
ALTER TABLE `detail_pengiriman_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diskons`
--
ALTER TABLE `diskons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groupbys`
--
ALTER TABLE `groupbys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudangs`
--
ALTER TABLE `gudangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga_juals`
--
ALTER TABLE `harga_juals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isispbms`
--
ALTER TABLE `isispbms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_mr_departments`
--
ALTER TABLE `isi_mr_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_mr_subdepts`
--
ALTER TABLE `isi_mr_subdepts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_pos`
--
ALTER TABLE `isi_pos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_rekaps`
--
ALTER TABLE `isi_rekaps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_returso`
--
ALTER TABLE `isi_returso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi_sales_orders`
--
ALTER TABLE `isi_sales_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategoris`
--
ALTER TABLE `kategoris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listbahans`
--
ALTER TABLE `listbahans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listrequesteditpos`
--
ALTER TABLE `listrequesteditpos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lock_rekaps`
--
ALTER TABLE `lock_rekaps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logpurchaserequests`
--
ALTER TABLE `logpurchaserequests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logspbms`
--
ALTER TABLE `logspbms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_mrs`
--
ALTER TABLE `log_mrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_pengirimans`
--
ALTER TABLE `log_pengirimans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_projects`
--
ALTER TABLE `master_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mr_departments`
--
ALTER TABLE `mr_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mr_subdepts`
--
ALTER TABLE `mr_subdepts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengambilanbarangs`
--
ALTER TABLE `pengambilanbarangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengambilan_barangs`
--
ALTER TABLE `pengambilan_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman_barangs`
--
ALTER TABLE `pengiriman_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periode_mrs`
--
ALTER TABLE `periode_mrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricelists`
--
ALTER TABLE `pricelists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaseorders`
--
ALTER TABLE `purchaseorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rekap_mrs`
--
ALTER TABLE `rekap_mrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_barangs`
--
ALTER TABLE `request_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reseps`
--
ALTER TABLE `reseps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returbarangs`
--
ALTER TABLE `returbarangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returso`
--
ALTER TABLE `returso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revisi_pos`
--
ALTER TABLE `revisi_pos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_orders`
--
ALTER TABLE `sales_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuans`
--
ALTER TABLE `satuans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `split_logs`
--
ALTER TABLE `split_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_jalans`
--
ALTER TABLE `surat_jalans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tanda_terimas`
--
ALTER TABLE `tanda_terimas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transferlogs`
--
ALTER TABLE `transferlogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wellhos_departments`
--
ALTER TABLE `wellhos_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wellhos_subdepts`
--
ALTER TABLE `wellhos_subdepts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alamats`
--
ALTER TABLE `alamats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `assign_users`
--
ALTER TABLE `assign_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `barangdatangs`
--
ALTER TABLE `barangdatangs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT for table `barang_gudangs`
--
ALTER TABLE `barang_gudangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `detail_pengambilan_barangs`
--
ALTER TABLE `detail_pengambilan_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `detail_pengiriman_barangs`
--
ALTER TABLE `detail_pengiriman_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `diskons`
--
ALTER TABLE `diskons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groupbys`
--
ALTER TABLE `groupbys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gudangs`
--
ALTER TABLE `gudangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `harga_juals`
--
ALTER TABLE `harga_juals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `isispbms`
--
ALTER TABLE `isispbms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `isi_mr_departments`
--
ALTER TABLE `isi_mr_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `isi_mr_subdepts`
--
ALTER TABLE `isi_mr_subdepts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `isi_pos`
--
ALTER TABLE `isi_pos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `isi_rekaps`
--
ALTER TABLE `isi_rekaps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `isi_returso`
--
ALTER TABLE `isi_returso`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `isi_sales_orders`
--
ALTER TABLE `isi_sales_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kategoris`
--
ALTER TABLE `kategoris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `listbahans`
--
ALTER TABLE `listbahans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `listrequesteditpos`
--
ALTER TABLE `listrequesteditpos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lock_rekaps`
--
ALTER TABLE `lock_rekaps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `logpurchaserequests`
--
ALTER TABLE `logpurchaserequests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logspbms`
--
ALTER TABLE `logspbms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_mrs`
--
ALTER TABLE `log_mrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `log_pengirimans`
--
ALTER TABLE `log_pengirimans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_projects`
--
ALTER TABLE `master_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `mr_departments`
--
ALTER TABLE `mr_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mr_subdepts`
--
ALTER TABLE `mr_subdepts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pengambilanbarangs`
--
ALTER TABLE `pengambilanbarangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengambilan_barangs`
--
ALTER TABLE `pengambilan_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pengiriman_barangs`
--
ALTER TABLE `pengiriman_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `periode_mrs`
--
ALTER TABLE `periode_mrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pricelists`
--
ALTER TABLE `pricelists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;
--
-- AUTO_INCREMENT for table `purchaseorders`
--
ALTER TABLE `purchaseorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rekap_mrs`
--
ALTER TABLE `rekap_mrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_barangs`
--
ALTER TABLE `request_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reseps`
--
ALTER TABLE `reseps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `returbarangs`
--
ALTER TABLE `returbarangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `returso`
--
ALTER TABLE `returso`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `revisi_pos`
--
ALTER TABLE `revisi_pos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_orders`
--
ALTER TABLE `sales_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `satuans`
--
ALTER TABLE `satuans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `split_logs`
--
ALTER TABLE `split_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `surat_jalans`
--
ALTER TABLE `surat_jalans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tanda_terimas`
--
ALTER TABLE `tanda_terimas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transferlogs`
--
ALTER TABLE `transferlogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wellhos_departments`
--
ALTER TABLE `wellhos_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wellhos_subdepts`
--
ALTER TABLE `wellhos_subdepts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
