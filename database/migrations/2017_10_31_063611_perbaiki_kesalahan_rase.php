<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PerbaikiKesalahanRase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('harga_juals', function(Blueprint $table)
        {
            $table->integer('harga_diskon')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('harga_juals', function(Blueprint $table)
        {
            $table->dropColumn('harga_diskon');
        });
    }
}
