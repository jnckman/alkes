<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStokopnamStokKeluarMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('isi_stokopnams', function(Blueprint $table)
      {
          $table->integer('stok_keluar')->unsigned()->nullable();
          $table->integer('stok_masuk')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('isi_stokopnams', function(Blueprint $table)
      {
          $table->dropColumn('stok_keluar');
          $table->dropColumn('stok_masuk');
      });
    }
}
