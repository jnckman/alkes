<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePorevisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi_pos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('isi_pos_id')->unsigned()->nullable();
            $table->integer('harga_po')->unsigned()->default(0);
            $table->integer('harga_baru')->unsigned()->default(0);
            $table->date('tgl_revisi')->nullable();
            $table->text('keterangan', 65535)->nullable();
            $table->enum('status',['new','approve','rejected'])->default('new');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revisi_pos');
    }
}
