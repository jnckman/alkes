<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSplitLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('split_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->integer('barangs_id')->unsigned()->nullable();
			$table->integer('barang_gudangs_id_1')->unsigned()->nullable();
			$table->integer('barang_gudangs_id_2')->unsigned()->nullable();
			$table->integer('gudangs_id')->unsigned()->nullable();
			$table->integer('jumlah')->unsigned()->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('split_logs');
	}

}
