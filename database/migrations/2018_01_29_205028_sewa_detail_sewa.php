<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SewaDetailSewa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewa_barangs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->string('kepada', 256)->nullable();
            $table->integer('durasi')->unsigned()->nullable();
            $table->integer('biaya')->unsigned()->nullable();
            $table->integer('diterima')->unsigned()->nullable();
            $table->integer('kurang')->unsigned()->nullable();
            $table->date('batas_waktu')->nullable();
            $table->integer('total')->unsigned()->nullable();
            $table->date('tanggal_sewa')->nullable();
        });
        Schema::create('detail_sewa_barangs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('barangs_id')->unsigned()->nullable();
            $table->integer('jumlah')->unsigned()->nullable();
            $table->date('batas_waktu')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sewa_barangs');
        Schema::drop('detail_sewa_barangs');
    }
}
