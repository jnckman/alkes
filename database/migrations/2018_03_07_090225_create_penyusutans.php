<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyusutans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('penyusutans', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('nominal')->unsigned()->nullable();
            $table->integer('barangs_id')->unsigned()->nullable();
            //$table->integer('barangs_id')->unsigned()->nullable();
            $table->string('bulan', 256)->nullable();
            $table->string('tahun', 256)->nullable();
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penyusutans');
    }
}
