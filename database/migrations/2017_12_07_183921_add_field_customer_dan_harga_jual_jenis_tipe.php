<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCustomerDanHargaJualJenisTipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('customers', function(Blueprint $table)
      {
          $table->string('tipe',255)->nullable();
          $table->string('jenis_harga_jual')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('customers', function(Blueprint $table)
      {
          $table->dropColumn('tipe');
          $table->dropColumn('jenis_harga_jual');
      });
    }
}