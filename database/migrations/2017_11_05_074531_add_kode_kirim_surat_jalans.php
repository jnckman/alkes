<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKodeKirimSuratJalans extends Migration
{
    public function up()
    {
      Schema::table('surat_jalans', function(Blueprint $table)
		{
			$table->string('pengiriman_kode', 64)->nullable();
		});
    }

    public function down()
    {
      Schema::table('surat_jalans', function(Blueprint $table)
		{
			$table->dropColumn('pengiriman_kode');
		});
    }
}
