<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableSupplierKode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->string('kode', 25)->nullable();
            $table->dropColumn('top');
        });
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->string('top', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->dropColumn('kode');
            $table->dropColumn('top');
        });
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->integer('top')->unsigned()->default(0);
        });
    }
}
