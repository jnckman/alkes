<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsApicustomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_apicustom', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('permalink', 191)->nullable();
			$table->string('tabel', 191)->nullable();
			$table->string('aksi', 191)->nullable();
			$table->string('kolom', 191)->nullable();
			$table->string('orderby', 191)->nullable();
			$table->string('sub_query_1', 191)->nullable();
			$table->string('sql_where', 191)->nullable();
			$table->string('nama', 191)->nullable();
			$table->string('keterangan', 191)->nullable();
			$table->string('parameter', 191)->nullable();
			$table->timestamps();
			$table->string('method_type', 25)->nullable();
			$table->text('parameters', 65535)->nullable();
			$table->text('responses', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_apicustom');
	}

}
