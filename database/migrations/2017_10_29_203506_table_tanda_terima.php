<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTandaTerima extends Migration
{
    public function up()
    {
      Schema::create('tanda_terimas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
         $table->date('tgl_cetak')->nullable();
         $table->date('tgl_terima')->nullable();
         $table->integer('user')->unsigned()->nullable();
         $table->string('diterima', 256)->nullable();
		});
    }

    public function down()
    {
      Schema::drop('tanda_terimas');
    }
}
