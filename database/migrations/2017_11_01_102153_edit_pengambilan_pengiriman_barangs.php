<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPengambilanPengirimanBarangs extends Migration
{
   public function up()
   {
      Schema::table('pengiriman_barangs', function(Blueprint $table)
		{
			$table->integer('pengambilan_brg_id')->unsigned()->nullable();
		});

      Schema::table('pengambilan_barangs', function(Blueprint $table)
		{
			$table->boolean('status')->nullable()->default(0);
         $table->dropColumn('pengiriman_id');
		});
   }

   public function down()
   {
      Schema::table('pengiriman_barangs', function(Blueprint $table)
		{
			$table->dropColumn('pengambilan_brg_id')->unsigned()->nullable();
		});

      Schema::table('pengambilan_barangs', function(Blueprint $table)
		{
			$table->dropColumn('status')->nullable()->default(0);
         $table->integer('pengiriman_id')->unsigned()->nullable();
		});
   }
}
