<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPengirimanBarangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('detail_pengiriman_barangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
         $table->integer('pengiriman_barang_id')->unsigned()->nullable();
         $table->integer('barang_id')->unsigned()->nullable();
			$table->integer('jumlah')->unsigned()->nullable()->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('detail_pengiriman_barangs');
    }
}
