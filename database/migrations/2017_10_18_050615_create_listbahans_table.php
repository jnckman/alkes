<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListbahansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listbahans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->integer('barang_gudangs_id')->unsigned()->nullable();
			$table->integer('jumlah')->unsigned()->nullable()->default(0);
			$table->integer('reseps_id')->unsigned()->nullable();
			$table->string('display', 256)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listbahans');
	}

}
