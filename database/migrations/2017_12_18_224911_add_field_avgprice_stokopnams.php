<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAvgpriceStokopnams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isi_stokopnams', function(Blueprint $table)
        {
            $table->integer('avgprice')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isi_stokopnams', function(Blueprint $table)
        {
            $table->dropColumn('avgprice');
        });
    }
}
