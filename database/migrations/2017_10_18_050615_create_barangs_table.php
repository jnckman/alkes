<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('nama', 256)->nullable();
			$table->string('jenis', 256)->nullable();
			$table->string('tipe', 191)->nullable();
			$table->string('ukuran', 256)->nullable();
			$table->string('status', 191)->nullable();
			$table->integer('avgprice')->unsigned()->nullable()->default(0);
			$table->integer('min')->unsigned()->nullable()->default(0);
			$table->integer('max')->unsigned()->nullable()->default(0);
			$table->integer('akumstok')->unsigned()->nullable()->default(0);
			$table->integer('ppn')->unsigned()->nullable()->default(0);
			$table->integer('harga_beli_akhir')->unsigned()->nullable()->default(0);
			$table->integer('harga_jual')->unsigned()->nullable()->default(0);
			$table->string('kode', 256)->nullable();
			$table->text('gambar', 65535)->nullable();
			$table->integer('satuans_id')->unsigned()->nullable();
			$table->integer('kategoris_id')->unsigned()->nullable();
			$table->text('cara_pakai', 65535)->nullable();
			$table->text('spesifikasi', 65535)->nullable();
			$table->text('video', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barangs');
	}

}
