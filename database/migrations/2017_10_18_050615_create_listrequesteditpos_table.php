<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListrequesteditposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listrequesteditpos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->softDeletes();
			$table->string('catatan', 256)->nullable();
			$table->integer('users_id')->unsigned()->nullable();
			$table->integer('purchaseorders_id')->unsigned()->nullable()->default(0);
			$table->integer('counter')->unsigned()->nullable()->default(0);
			$table->integer('status')->unsigned()->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listrequesteditpos');
	}

}
