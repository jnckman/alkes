<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableMrSubdept extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periode_mrs', function(Blueprint $table)
        {
            $table->integer('tahun')->unsigned()->nullable();
        });
        Schema::table('projects', function(Blueprint $table)
        {
            $table->string('saldo', 256)->nullable();
            $table->string('total', 256)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periode_mrs', function(Blueprint $table)
        {
            $table->dropColumn('tahun');
        });
        Schema::table('projects', function(Blueprint $table)
        {
            $table->dropColumn('saldo');
            $table->dropColumn('total');
        });
    }
}
