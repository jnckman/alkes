<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKasmasukFinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kasmasuk', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            //$table->integer('barangdatangs_id')->unsigned()->nullable();
            $table->integer('nominal')->unsigned()->nullable();
            $table->integer('coa_id')->unsigned()->nullable();
            $table->integer('rekpers_id')->unsigned()->nullable();
            
            //$table->integer('beli')->unsigned()->nullable();
            //$table->integer('lunas')->unsigned()->nullable();
            //$table->integer('sisa')->unsigned()->nullable();
            //$table->date('tgllunas')->nullable();
            $table->string('catatan', 256)->nullable();
            
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kasmasuk');
    }
}
