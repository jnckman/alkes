<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMrSubdeptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mr_subdepts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->integer('projects_id')->unsigned()->nullable();
			$table->integer('wellhos_subdepts_id')->unsigned()->nullable();
			$table->integer('periode_mrs_id')->unsigned()->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->date('tanggal')->nullable();
			$table->string('nomr', 256)->nullable();
			$table->string('gudang', 256)->nullable();
			$table->string('approval', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mr_subdepts');
	}

}
