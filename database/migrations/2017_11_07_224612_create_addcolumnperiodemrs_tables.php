<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
class CreateAddcolumnperiodemrsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('periode_mrs', function(Blueprint $table)
        {
            
         $table->integer('selesai')->unsigned()->nullable();
        });
    }
    public function down()
    {
      Schema::table('periode_mrs', function(Blueprint $table)
        {
         
         $table->dropColumn('selesai');
        });
    }
}