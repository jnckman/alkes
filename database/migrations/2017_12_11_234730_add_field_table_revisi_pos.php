<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTableRevisiPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('revisi_pos', function(Blueprint $table)
      {
          $table->integer('split')->unsigned()->nullable();
          $table->integer('split_jumlah')->unsigned()->nullable();
          $table->integer('split_harga')->unsigned()->nullable();
          $table->integer('split_po_baru')->unsigned()->nullable();
          $table->integer('split_suppliers_id')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('revisi_pos', function(Blueprint $table)
      {
          $table->dropColumn('split');
          $table->dropColumn('split_harga');
          $table->dropColumn('split_jumlah');
          $table->dropColumn('split_po_baru');
          $table->dropColumn('split_suppliers_id');
      });
    }
}
