<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLockRekapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lock_rekaps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('status', 256)->nullable();
			$table->integer('jumlah')->unsigned()->nullable()->default(0);
			$table->integer('barangs_id')->unsigned()->nullable();
			$table->integer('pricelists_id')->unsigned()->nullable();
			$table->integer('periode_mrs_id')->unsigned()->nullable();
			$table->boolean('approve')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lock_rekaps');
	}

}
