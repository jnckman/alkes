<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDetailSewa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_sewa_barangs', function(Blueprint $table)
        {
            $table->integer('sewa_barangs_id')->unsigned()->nullable();
        });
        Schema::table('sewa_barangs', function(Blueprint $table)
        {
            $table->string('nomor',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_sewa_barangs', function(Blueprint $table)
        {
            $table->dropcolumn('sewa_barangs_id');
        });
        Schema::table('sewa_barangs', function(Blueprint $table)
        {
            $table->dropcolumn('nomor');
        });
    }
}
