<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturMrIsiReturnMr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_mrs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('mr_subdepts_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('nomor', 256)->nullable();
            $table->string('status',256)->nullable();
        });
        Schema::create('isi_retur_mrs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('retur_mrs_id')->unsigned()->nullable();
            $table->integer('isi_mr_subdepts_id')->unsigned()->nullable();
            $table->text('keterangan', 65535)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('retur_mrs');
        Schema::drop('isi_retur_mrs');
    }
}
