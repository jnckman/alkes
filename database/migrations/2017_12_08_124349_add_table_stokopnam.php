<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableStokopnam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stokopnams', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->string('kode', 256)->nullable();
            $table->date('tanggal')->nullable();
        });
        Schema::create('isi_stokopnams', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('stokopnams_id')->unsigned()->nullable();
            $table->integer('barangs_id')->unsigned()->nullable();
            $table->integer('gudangs_id')->unsigned()->nullable();
            $table->integer('stok')->unsigned()->nullable()->default(0);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stokopnams');
        Schema::drop('isi_stokopnams');
    }
}
