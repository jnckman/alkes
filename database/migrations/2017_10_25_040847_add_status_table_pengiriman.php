<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusTablePengiriman extends Migration
{

    public function up()
    {
      Schema::table('pengiriman_barangs', function(Blueprint $table)
		{
         $table->string('status', 128)->nullable();
		});
    }

    public function down()
    {
      Schema::table('pengiriman_barangs', function(Blueprint $table)
		{
         $table->dropColumn('status');
		});
    }
}
