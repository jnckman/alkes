<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakturspbm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isibukufakturpos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('barangdatangs_id')->unsigned()->nullable();
            //$table->integer('prosentase')->unsigned()->nullable();
            //$table->integer('barangs_id')->unsigned()->nullable();
            //$table->string('nofaktur', 256)->nullable();
            //$table->string('noefaktur', 256)->nullable();
            //$table->string('masapajak', 256)->nullable();
            //$table->date('tglfaktur')->nullable();
            //$table->date('tglterimafaktur')->nullable();
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('isibukufakturpos');
    }
}
