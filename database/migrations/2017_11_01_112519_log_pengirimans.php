<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogPengirimans extends Migration
{
    public function up()
    {
      Schema::create('log_pengirimans', function(Blueprint $table)
      {
          $table->increments('id');
          $table->softDeletes();
          $table->timestamps();
          $table->integer('user_id')->unsigned()->nullable();
          $table->integer('pengiriman_brg_id')->unsigned()->nullable();
          $table->string('status', 256)->nullable();
      });
    }

    public function down()
    {
      Schema::drop('log_pengirimans');
    }
}
