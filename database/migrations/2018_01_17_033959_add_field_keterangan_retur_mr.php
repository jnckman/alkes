<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldKeteranganReturMr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retur_mrs', function(Blueprint $table)
        {
            $table->text('keterangan', 65535)->nullable();
        });
        Schema::table('isi_retur_mrs', function(Blueprint $table)
        {
            $table->integer('jumlah')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retur_mrs', function(Blueprint $table)
        {
            $table->dropColumn('keterangan');
        });
        Schema::table('isi_retur_mrs', function(Blueprint $table)
        {
            $table->dropColumn('jumlah');
        });
    }
}
