<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangdatangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barangdatangs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->softDeletes();
			$table->date('tanggaldatang')->nullable();
			$table->string('nospbm', 128)->nullable();
			$table->string('penerima', 128)->nullable();
			$table->integer('suppliers_id')->unsigned()->nullable();
			$table->integer('gudangs_id')->unsigned()->nullable();
			$table->integer('purchaseorders_id')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barangdatangs');
	}

}
