<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSalesOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->string('nomor', 256)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('total')->unsigned()->nullable();
            $table->string('metode_bayar', 191)->nullable();
            $table->date('jatuh_tempo')->nullable();
            $table->string('status', 191)->nullable();
        });
        Schema::create('isi_sales_orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('sales_orders_id')->unsigned()->nullable();
            $table->integer('barangs_id')->unsigned()->nullable();
            $table->integer('jumlah')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_orders');
        Schema::drop('isi_sales_orders');
    }
}
