<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMrDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mr_departments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('nomr', 256)->nullable();
			$table->string('keterangan', 256)->nullable();
			$table->integer('periode_mrs_id')->unsigned()->nullable();
			$table->integer('wellhos_departments_id')->unsigned()->nullable();
			$table->string('approval', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mr_departments');
	}

}
