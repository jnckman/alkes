<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIsiReturso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isi_returso', function(Blueprint $table)
        {
            $table->renameColumn('returSO_id', 'to');
        });
        Schema::table('isi_returso', function(Blueprint $table)
        {
            $table->renameColumn('to', 'returso_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isi_returso', function(Blueprint $table)
        {
            $table->renameColumn('returso_id','to');
        });
        Schema::table('isi_returso', function(Blueprint $table)
        {
            $table->renameColumn('to','returSO_id');
        });
    }
}
