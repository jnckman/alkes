<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimanBarangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pengiriman_barangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
         $table->date('tgl_kirim')->nullable();
			$table->string('kode', 256)->nullable();
         $table->string('jenis', 128)->nullable();
         $table->integer('global_id')->unsigned()->nullable();
			$table->integer('user')->unsigned()->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('pengiriman_barangs');
	 }
}
