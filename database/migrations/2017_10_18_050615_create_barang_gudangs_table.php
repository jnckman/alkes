<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangGudangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barang_gudangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('kode', 256)->nullable();
			$table->integer('barangs_id')->unsigned()->nullable();
			$table->integer('gudangs_id')->unsigned()->nullable();
			$table->integer('stok')->unsigned()->nullable()->default(0);
			$table->integer('min')->unsigned()->nullable();
			$table->integer('max')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barang_gudangs');
	}

}
