<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('nocustomer', 256)->nullable();
			$table->string('nama', 256)->nullable();
			$table->string('alamat invoice', 256)->nullable();
			$table->string('npwp', 256)->nullable();
			$table->string('notelp', 256)->nullable();
			$table->string('jenis', 256)->nullable();
			$table->string('namapemilik', 256)->nullable();
			$table->string('alamatpemilik', 256)->nullable();
			$table->string('noktp', 256)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
