<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTandaTerimas extends Migration
{
    public function up()
    {
      Schema::table('tanda_terimas', function(Blueprint $table)
		{
			$table->string('jenis_nota', 64)->nullable();
         $table->integer('nota_id')->unsigned()->nullable();
		});
    }
    public function down()
    {
      Schema::table('tanda_terimas', function(Blueprint $table)
		{
         $table->dropColumn('jenis_nota');
         $table->dropColumn('nota_id');
		});
    }
}
