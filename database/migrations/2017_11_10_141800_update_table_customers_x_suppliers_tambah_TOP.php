<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCustomersXSuppliersTambahtop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->integer('top')->unsigned()->default(0);
        });
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->integer('top')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->dropColumn('top');
        });
        Schema::table('suppliers', function(Blueprint $table)
        {
            $table->dropColumn('top');
        });
    }
}
