<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoFaktur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isi_bayar_sos', function(Blueprint $table)
        {
            $table->integer('pengirimanbarangs_id')->unsigned()->nullable();
            $table->integer('rekpers_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('pengirimanbarangs_id','rekpers_id');
    }
}
