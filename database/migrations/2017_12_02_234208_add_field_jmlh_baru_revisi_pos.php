<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJmlhBaruRevisiPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('revisi_pos', function(Blueprint $table)
      {
          $table->integer('jumlah_lama')->unsigned()->nullable();
          $table->integer('jumlah_baru')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('revisi_pos', function(Blueprint $table)
      {
          $table->dropColumn('jumlah_lama');
          $table->dropColumn('jumlah_baru');
      });
    }
}
