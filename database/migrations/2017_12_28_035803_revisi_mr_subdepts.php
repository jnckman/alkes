<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevisiMrSubdepts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi_mr_subdepts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('isi_mr_id')->unsigned()->nullable();
            $table->integer('jumlah_lama')->unsigned()->nullable();
            $table->integer('jumlah_baru')->unsigned()->nullable();
            $table->string('keterangan_lama',255)->nullable();
            $table->string('keterangan_baru',255)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revisi_mr_subdepts');
    }
}
