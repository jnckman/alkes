<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHutang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bukuhutangs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('barangdatangs_id')->unsigned()->nullable();
            $table->integer('hutang')->unsigned()->nullable();
            $table->integer('beli')->unsigned()->nullable();
            $table->integer('lunas')->unsigned()->nullable();
            $table->integer('sisa')->unsigned()->nullable();
            $table->date('tgllunas')->nullable();
            $table->string('via', 256)->nullable();
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bukuhutang');
    }
}



