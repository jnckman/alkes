<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTanggalrekap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
      Schema::table('lock_rekaps', function(Blueprint $table)
      {
          $table->date('tanggalrekap')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('lock_rekaps', function(Blueprint $table)
      {
          $table->dropColumn('tanggalrekap');
      });
    }
}
