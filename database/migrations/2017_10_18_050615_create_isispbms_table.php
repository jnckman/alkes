<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIsispbmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('isispbms', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('barangdatangs_id')->unsigned()->nullable();
			$table->integer('barangs_id')->unsigned()->nullable();
			$table->integer('jumlah')->unsigned()->nullable()->default(0);
			$table->string('kondisi', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('isispbms');
	}

}
