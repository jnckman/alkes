<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchaseorders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->softDeletes();
			$table->string('nopo', 30)->nullable();
			$table->date('tglpo')->nullable();
			$table->string('kodesp', 30)->nullable();
			$table->string('alteralamat', 100)->nullable();
			$table->integer('customers_id')->unsigned()->nullable();
			$table->integer('jenispo')->unsigned()->nullable()->default(0);
			$table->integer('ongkir')->unsigned()->nullable()->default(0);
			$table->integer('ppn')->unsigned()->nullable()->default(0);
			$table->integer('periode_mrs_id')->unsigned()->nullable();
			$table->date('tempo')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchaseorders');
	}

}
