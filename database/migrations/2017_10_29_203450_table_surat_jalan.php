<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableSuratJalan extends Migration
{
    public function up()
    {
      Schema::create('surat_jalans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
         $table->date('tgl_cetak')->nullable();
         $table->integer('user')->unsigned()->nullable();
		});
    }

    public function down()
    {
        Schema::drop('surat_jalans');
    }
}
