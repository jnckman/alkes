<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningpers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekpers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            //$table->integer('barangdatangs_id')->unsigned()->nullable();
            $table->integer('saldo')->unsigned()->nullable();
            //$table->integer('beli')->unsigned()->nullable();
            //$table->integer('lunas')->unsigned()->nullable();
            //$table->integer('sisa')->unsigned()->nullable();
            //$table->date('tgllunas')->nullable();
            $table->string('norek', 256)->nullable();
            $table->string('atasnama', 256)->nullable();
            $table->string('bank', 256)->nullable();
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rekpers');
    }
}
