<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPengirimanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
      {
          $table->integer('pengiriman_id')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
      {
          $table->dropColumn('pengiriman_id');
      });
    }
}
