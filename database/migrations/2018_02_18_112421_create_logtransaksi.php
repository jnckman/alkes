<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogtransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logtransaksi', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            //$table->integer('barangdatangs_id')->unsigned()->nullable();
            //$table->integer('rekcompanies_id')->unsigned()->nullable();
            $table->integer('saldo')->unsigned()->nullable();
            $table->date('tgl')->nullable();
            $table->string('catatan', 256)->nullable();
            $table->string('bank', 256)->nullable();
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logtransaksi');
    }
}
