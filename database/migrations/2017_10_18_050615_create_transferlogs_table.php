<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransferlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transferlogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('logcode', 256)->nullable();
			$table->integer('user')->nullable();
			$table->date('tgl')->nullable();
			$table->text('note', 65535)->nullable();
			$table->string('approve', 191)->nullable();
			$table->integer('gudangs_id_dari')->unsigned()->nullable();
			$table->integer('gudangs_id_ke')->unsigned()->nullable();
			$table->integer('stok_sblm_transfer_d')->unsigned()->nullable()->default(0);
			$table->integer('stok_sblm_transfer_k')->unsigned()->nullable()->default(0);
			$table->integer('stok_stlh_transfer_d')->unsigned()->nullable()->default(0);
			$table->integer('stok_stlh_transfer_k')->unsigned()->nullable()->default(0);
			$table->integer('jml_transfer')->unsigned()->nullable()->default(0);
			$table->integer('barang_id')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transferlogs');
	}

}
