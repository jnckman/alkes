<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableBarangIsiPosDanPo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barangs', function(Blueprint $table)
        {
            $table->integer('harga_pr')->unsigned()->nullable();
        });
        Schema::table('isi_pos', function(Blueprint $table)
        {
            $table->integer('diskon')->unsigned()->nullable();
            $table->integer('total_akhir')->unsigned()->nullable();
        });
        Schema::table('purchaseorders', function(Blueprint $table)
        {
            $table->integer('diskon')->unsigned()->nullable();
            $table->integer('total_akhir')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangs', function(Blueprint $table)
        {
            $table->dropColumn('harga_pr');
        });
        Schema::table('isi_pos', function(Blueprint $table)
        {
            $table->dropColumn('diskon');
            $table->dropColumn('total_akhir');
        });
        Schema::table('purchaseorders', function(Blueprint $table)
        {
            $table->dropColumn('diskon');
            $table->dropColumn('total_akhir');
        });
    }
}
