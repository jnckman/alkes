<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWellhosSubdeptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wellhos_subdepts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('nama', 256)->nullable();
			$table->string('keterangan', 256)->nullable();
			$table->string('phone', 256)->nullable();
			$table->string('email', 256)->nullable();
			$table->string('pic', 256)->nullable();
			$table->integer('wellhos_departments_id')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wellhos_subdepts');
	}

}
