<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGudangId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
		{
         $table->integer('gudang_id')->unsigned()->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
		{
         $table->dropColumn('gudang_id');
		});
    }
}
