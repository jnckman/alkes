<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BikinIsiReturso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isi_returso', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('returSO_id')->unsigned()->nullable();
            $table->integer('isi_sales_order_id')->unsigned()->nullable();
            $table->integer('jumlah')->unsigned()->nullable();
            $table->string('status', 191)->nullable();
            $table->text('keterangan', 65535)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('isi_returso');
    }
}
