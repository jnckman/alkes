<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BayarPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayar_pos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('po_id')->unsigned()->nullable();
            $table->date('tanggal_bayar')->nullable();
            $table->string('status',256)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('coa_id')->unsigned()->nullable();
        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bayar_pos');
    }
}
