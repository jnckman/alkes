<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualpolockrekaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('lock_rekaps', function(Blueprint $table)
      {
          $table->integer('actualpo')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('lock_rekaps', function(Blueprint $table)
      {
          $table->dropColumn('actualpo');
      });
    }
}
