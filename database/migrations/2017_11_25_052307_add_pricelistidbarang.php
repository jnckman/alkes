<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricelistidbarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
      {
          $table->integer('priclists_id')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pengambilan_barangs', function(Blueprint $table)
      {
          $table->dropColumn('priclists_id');
      });
    }
}

