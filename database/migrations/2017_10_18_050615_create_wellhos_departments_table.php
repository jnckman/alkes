<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWellhosDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wellhos_departments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('nama', 256)->nullable();
			$table->string('phone', 256)->nullable();
			$table->string('pic', 256)->nullable();
			$table->string('keterangan', 256)->nullable();
			$table->string('email', 256)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wellhos_departments');
	}

}
