<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiskonSODanIsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('isi_sales_orders', function(Blueprint $table)
      {
          $table->integer('diskon')->unsigned()->nullable();
      });
      Schema::table('sales_orders', function(Blueprint $table)
      {
          $table->integer('diskon')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('isi_sales_orders', function(Blueprint $table)
      {
          $table->dropColumn('diskon');
      });
      Schema::table('sales_orders', function(Blueprint $table)
      {
          $table->dropColumn('diskon');
      });

    }
}
