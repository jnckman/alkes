<?php

use Illuminate\Database\Seeder;

class CmsMenusPrivilegesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cms_menus_privileges')->delete();
        
        \DB::table('cms_menus_privileges')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_cms_menus' => 1,
                'id_cms_privileges' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'id_cms_menus' => 2,
                'id_cms_privileges' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'id_cms_menus' => 3,
                'id_cms_privileges' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'id_cms_menus' => 4,
                'id_cms_privileges' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'id_cms_menus' => 5,
                'id_cms_privileges' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'id_cms_menus' => 6,
                'id_cms_privileges' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'id_cms_menus' => 7,
                'id_cms_privileges' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'id_cms_menus' => 8,
                'id_cms_privileges' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'id_cms_menus' => 9,
                'id_cms_privileges' => 1,
            ),
            9 => 
            array (
                'id' => 16,
                'id_cms_menus' => 16,
                'id_cms_privileges' => 1,
            ),
            10 => 
            array (
                'id' => 17,
                'id_cms_menus' => 17,
                'id_cms_privileges' => 1,
            ),
            11 => 
            array (
                'id' => 18,
                'id_cms_menus' => 18,
                'id_cms_privileges' => 1,
            ),
            12 => 
            array (
                'id' => 20,
                'id_cms_menus' => 20,
                'id_cms_privileges' => 1,
            ),
            13 => 
            array (
                'id' => 23,
                'id_cms_menus' => 23,
                'id_cms_privileges' => 1,
            ),
            14 => 
            array (
                'id' => 24,
                'id_cms_menus' => 24,
                'id_cms_privileges' => 1,
            ),
            15 => 
            array (
                'id' => 25,
                'id_cms_menus' => 25,
                'id_cms_privileges' => 1,
            ),
            16 => 
            array (
                'id' => 26,
                'id_cms_menus' => 26,
                'id_cms_privileges' => 1,
            ),
            17 => 
            array (
                'id' => 30,
                'id_cms_menus' => 30,
                'id_cms_privileges' => 1,
            ),
            18 => 
            array (
                'id' => 31,
                'id_cms_menus' => 31,
                'id_cms_privileges' => 1,
            ),
            19 => 
            array (
                'id' => 32,
                'id_cms_menus' => 32,
                'id_cms_privileges' => 1,
            ),
            20 => 
            array (
                'id' => 33,
                'id_cms_menus' => 33,
                'id_cms_privileges' => 1,
            ),
            21 => 
            array (
                'id' => 34,
                'id_cms_menus' => 34,
                'id_cms_privileges' => 1,
            ),
            22 => 
            array (
                'id' => 35,
                'id_cms_menus' => 35,
                'id_cms_privileges' => 1,
            ),
            23 => 
            array (
                'id' => 36,
                'id_cms_menus' => 36,
                'id_cms_privileges' => 1,
            ),
            24 => 
            array (
                'id' => 37,
                'id_cms_menus' => 37,
                'id_cms_privileges' => 1,
            ),
            25 => 
            array (
                'id' => 38,
                'id_cms_menus' => 38,
                'id_cms_privileges' => 1,
            ),
            26 => 
            array (
                'id' => 39,
                'id_cms_menus' => 39,
                'id_cms_privileges' => 1,
            ),
            27 => 
            array (
                'id' => 40,
                'id_cms_menus' => 40,
                'id_cms_privileges' => 1,
            ),
            28 => 
            array (
                'id' => 41,
                'id_cms_menus' => 41,
                'id_cms_privileges' => 1,
            ),
            29 => 
            array (
                'id' => 42,
                'id_cms_menus' => 42,
                'id_cms_privileges' => 1,
            ),
            30 => 
            array (
                'id' => 43,
                'id_cms_menus' => 43,
                'id_cms_privileges' => 1,
            ),
            31 => 
            array (
                'id' => 44,
                'id_cms_menus' => 44,
                'id_cms_privileges' => 1,
            ),
            32 => 
            array (
                'id' => 45,
                'id_cms_menus' => 45,
                'id_cms_privileges' => 1,
            ),
            33 => 
            array (
                'id' => 46,
                'id_cms_menus' => 46,
                'id_cms_privileges' => 1,
            ),
            34 => 
            array (
                'id' => 47,
                'id_cms_menus' => 47,
                'id_cms_privileges' => 1,
            ),
            35 => 
            array (
                'id' => 48,
                'id_cms_menus' => 48,
                'id_cms_privileges' => 1,
            ),
            36 => 
            array (
                'id' => 49,
                'id_cms_menus' => 49,
                'id_cms_privileges' => 1,
            ),
            37 => 
            array (
                'id' => 50,
                'id_cms_menus' => 50,
                'id_cms_privileges' => 1,
            ),
            38 => 
            array (
                'id' => 51,
                'id_cms_menus' => 51,
                'id_cms_privileges' => 1,
            ),
            39 => 
            array (
                'id' => 52,
                'id_cms_menus' => 52,
                'id_cms_privileges' => 1,
            ),
            40 => 
            array (
                'id' => 53,
                'id_cms_menus' => 53,
                'id_cms_privileges' => 1,
            ),
            41 => 
            array (
                'id' => 55,
                'id_cms_menus' => 55,
                'id_cms_privileges' => 1,
            ),
            42 => 
            array (
                'id' => 56,
                'id_cms_menus' => 54,
                'id_cms_privileges' => 1,
            ),
            43 => 
            array (
                'id' => 60,
                'id_cms_menus' => 58,
                'id_cms_privileges' => 1,
            ),
            44 => 
            array (
                'id' => 61,
                'id_cms_menus' => 59,
                'id_cms_privileges' => 1,
            ),
            45 => 
            array (
                'id' => 62,
                'id_cms_menus' => 60,
                'id_cms_privileges' => 1,
            ),
            46 => 
            array (
                'id' => 64,
                'id_cms_menus' => 62,
                'id_cms_privileges' => 1,
            ),
            47 => 
            array (
                'id' => 65,
                'id_cms_menus' => 61,
                'id_cms_privileges' => 1,
            ),
            48 => 
            array (
                'id' => 67,
                'id_cms_menus' => 63,
                'id_cms_privileges' => 1,
            ),
            49 => 
            array (
                'id' => 68,
                'id_cms_menus' => 64,
                'id_cms_privileges' => 1,
            ),
            50 => 
            array (
                'id' => 69,
                'id_cms_menus' => 65,
                'id_cms_privileges' => 1,
            ),
            51 => 
            array (
                'id' => 77,
                'id_cms_menus' => 67,
                'id_cms_privileges' => 1,
            ),
            52 => 
            array (
                'id' => 78,
                'id_cms_menus' => 68,
                'id_cms_privileges' => 1,
            ),
            53 => 
            array (
                'id' => 79,
                'id_cms_menus' => 69,
                'id_cms_privileges' => 1,
            ),
            54 => 
            array (
                'id' => 81,
                'id_cms_menus' => 71,
                'id_cms_privileges' => 1,
            ),
            55 => 
            array (
                'id' => 89,
                'id_cms_menus' => 70,
                'id_cms_privileges' => 1,
            ),
            56 => 
            array (
                'id' => 94,
                'id_cms_menus' => 80,
                'id_cms_privileges' => 1,
            ),
            57 => 
            array (
                'id' => 95,
                'id_cms_menus' => 81,
                'id_cms_privileges' => 1,
            ),
            58 => 
            array (
                'id' => 96,
                'id_cms_menus' => 77,
                'id_cms_privileges' => 1,
            ),
            59 => 
            array (
                'id' => 99,
                'id_cms_menus' => 84,
                'id_cms_privileges' => 1,
            ),
            60 => 
            array (
                'id' => 100,
                'id_cms_menus' => 85,
                'id_cms_privileges' => 1,
            ),
            61 => 
            array (
                'id' => 101,
                'id_cms_menus' => 86,
                'id_cms_privileges' => 1,
            ),
            62 => 
            array (
                'id' => 102,
                'id_cms_menus' => 87,
                'id_cms_privileges' => 1,
            ),
            63 => 
            array (
                'id' => 106,
                'id_cms_menus' => 91,
                'id_cms_privileges' => 1,
            ),
            64 => 
            array (
                'id' => 107,
                'id_cms_menus' => 90,
                'id_cms_privileges' => 1,
            ),
            65 => 
            array (
                'id' => 116,
                'id_cms_menus' => 73,
                'id_cms_privileges' => 9,
            ),
            66 => 
            array (
                'id' => 117,
                'id_cms_menus' => 73,
                'id_cms_privileges' => 1,
            ),
            67 => 
            array (
                'id' => 118,
                'id_cms_menus' => 88,
                'id_cms_privileges' => 9,
            ),
            68 => 
            array (
                'id' => 119,
                'id_cms_menus' => 88,
                'id_cms_privileges' => 1,
            ),
            69 => 
            array (
                'id' => 120,
                'id_cms_menus' => 72,
                'id_cms_privileges' => 9,
            ),
            70 => 
            array (
                'id' => 121,
                'id_cms_menus' => 72,
                'id_cms_privileges' => 1,
            ),
            71 => 
            array (
                'id' => 126,
                'id_cms_menus' => 94,
                'id_cms_privileges' => 9,
            ),
            72 => 
            array (
                'id' => 127,
                'id_cms_menus' => 94,
                'id_cms_privileges' => 1,
            ),
            73 => 
            array (
                'id' => 128,
                'id_cms_menus' => 93,
                'id_cms_privileges' => 9,
            ),
            74 => 
            array (
                'id' => 129,
                'id_cms_menus' => 93,
                'id_cms_privileges' => 1,
            ),
            75 => 
            array (
                'id' => 130,
                'id_cms_menus' => 92,
                'id_cms_privileges' => 9,
            ),
            76 => 
            array (
                'id' => 131,
                'id_cms_menus' => 92,
                'id_cms_privileges' => 1,
            ),
            77 => 
            array (
                'id' => 132,
                'id_cms_menus' => 27,
                'id_cms_privileges' => 9,
            ),
            78 => 
            array (
                'id' => 133,
                'id_cms_menus' => 27,
                'id_cms_privileges' => 1,
            ),
            79 => 
            array (
                'id' => 134,
                'id_cms_menus' => 74,
                'id_cms_privileges' => 9,
            ),
            80 => 
            array (
                'id' => 135,
                'id_cms_menus' => 74,
                'id_cms_privileges' => 1,
            ),
            81 => 
            array (
                'id' => 142,
                'id_cms_menus' => 56,
                'id_cms_privileges' => 9,
            ),
            82 => 
            array (
                'id' => 143,
                'id_cms_menus' => 56,
                'id_cms_privileges' => 1,
            ),
            83 => 
            array (
                'id' => 144,
                'id_cms_menus' => 95,
                'id_cms_privileges' => 1,
            ),
            84 => 
            array (
                'id' => 145,
                'id_cms_menus' => 96,
                'id_cms_privileges' => 13,
            ),
            85 => 
            array (
                'id' => 146,
                'id_cms_menus' => 96,
                'id_cms_privileges' => 14,
            ),
            86 => 
            array (
                'id' => 147,
                'id_cms_menus' => 96,
                'id_cms_privileges' => 1,
            ),
            87 => 
            array (
                'id' => 148,
                'id_cms_menus' => 97,
                'id_cms_privileges' => 1,
            ),
            88 => 
            array (
                'id' => 149,
                'id_cms_menus' => 98,
                'id_cms_privileges' => 1,
            ),
            89 => 
            array (
                'id' => 150,
                'id_cms_menus' => 99,
                'id_cms_privileges' => 1,
            ),
            90 => 
            array (
                'id' => 152,
                'id_cms_menus' => 101,
                'id_cms_privileges' => 1,
            ),
            91 => 
            array (
                'id' => 153,
                'id_cms_menus' => 104,
                'id_cms_privileges' => 1,
            ),
            92 => 
            array (
                'id' => 159,
                'id_cms_menus' => 107,
                'id_cms_privileges' => 1,
            ),
            93 => 
            array (
                'id' => 161,
                'id_cms_menus' => 106,
                'id_cms_privileges' => 1,
            ),
            94 => 
            array (
                'id' => 162,
                'id_cms_menus' => 108,
                'id_cms_privileges' => 1,
            ),
            95 => 
            array (
                'id' => 163,
                'id_cms_menus' => 109,
                'id_cms_privileges' => 1,
            ),
            96 => 
            array (
                'id' => 164,
                'id_cms_menus' => 100,
                'id_cms_privileges' => 11,
            ),
            97 => 
            array (
                'id' => 165,
                'id_cms_menus' => 100,
                'id_cms_privileges' => 1,
            ),
            98 => 
            array (
                'id' => 166,
                'id_cms_menus' => 19,
                'id_cms_privileges' => 11,
            ),
            99 => 
            array (
                'id' => 167,
                'id_cms_menus' => 19,
                'id_cms_privileges' => 1,
            ),
            100 => 
            array (
                'id' => 168,
                'id_cms_menus' => 22,
                'id_cms_privileges' => 11,
            ),
            101 => 
            array (
                'id' => 169,
                'id_cms_menus' => 22,
                'id_cms_privileges' => 1,
            ),
            102 => 
            array (
                'id' => 170,
                'id_cms_menus' => 21,
                'id_cms_privileges' => 9,
            ),
            103 => 
            array (
                'id' => 171,
                'id_cms_menus' => 21,
                'id_cms_privileges' => 11,
            ),
            104 => 
            array (
                'id' => 172,
                'id_cms_menus' => 21,
                'id_cms_privileges' => 1,
            ),
            105 => 
            array (
                'id' => 173,
                'id_cms_menus' => 15,
                'id_cms_privileges' => 9,
            ),
            106 => 
            array (
                'id' => 174,
                'id_cms_menus' => 15,
                'id_cms_privileges' => 11,
            ),
            107 => 
            array (
                'id' => 175,
                'id_cms_menus' => 15,
                'id_cms_privileges' => 1,
            ),
            108 => 
            array (
                'id' => 176,
                'id_cms_menus' => 13,
                'id_cms_privileges' => 11,
            ),
            109 => 
            array (
                'id' => 177,
                'id_cms_menus' => 13,
                'id_cms_privileges' => 1,
            ),
            110 => 
            array (
                'id' => 178,
                'id_cms_menus' => 10,
                'id_cms_privileges' => 11,
            ),
            111 => 
            array (
                'id' => 179,
                'id_cms_menus' => 10,
                'id_cms_privileges' => 1,
            ),
            112 => 
            array (
                'id' => 180,
                'id_cms_menus' => 11,
                'id_cms_privileges' => 11,
            ),
            113 => 
            array (
                'id' => 181,
                'id_cms_menus' => 11,
                'id_cms_privileges' => 1,
            ),
            114 => 
            array (
                'id' => 182,
                'id_cms_menus' => 12,
                'id_cms_privileges' => 9,
            ),
            115 => 
            array (
                'id' => 183,
                'id_cms_menus' => 12,
                'id_cms_privileges' => 11,
            ),
            116 => 
            array (
                'id' => 184,
                'id_cms_menus' => 12,
                'id_cms_privileges' => 1,
            ),
            117 => 
            array (
                'id' => 185,
                'id_cms_menus' => 28,
                'id_cms_privileges' => 9,
            ),
            118 => 
            array (
                'id' => 186,
                'id_cms_menus' => 28,
                'id_cms_privileges' => 11,
            ),
            119 => 
            array (
                'id' => 187,
                'id_cms_menus' => 28,
                'id_cms_privileges' => 1,
            ),
            120 => 
            array (
                'id' => 188,
                'id_cms_menus' => 57,
                'id_cms_privileges' => 9,
            ),
            121 => 
            array (
                'id' => 189,
                'id_cms_menus' => 57,
                'id_cms_privileges' => 11,
            ),
            122 => 
            array (
                'id' => 190,
                'id_cms_menus' => 57,
                'id_cms_privileges' => 1,
            ),
            123 => 
            array (
                'id' => 191,
                'id_cms_menus' => 29,
                'id_cms_privileges' => 9,
            ),
            124 => 
            array (
                'id' => 192,
                'id_cms_menus' => 29,
                'id_cms_privileges' => 11,
            ),
            125 => 
            array (
                'id' => 193,
                'id_cms_menus' => 29,
                'id_cms_privileges' => 1,
            ),
            126 => 
            array (
                'id' => 194,
                'id_cms_menus' => 79,
                'id_cms_privileges' => 11,
            ),
            127 => 
            array (
                'id' => 195,
                'id_cms_menus' => 79,
                'id_cms_privileges' => 1,
            ),
            128 => 
            array (
                'id' => 196,
                'id_cms_menus' => 78,
                'id_cms_privileges' => 11,
            ),
            129 => 
            array (
                'id' => 197,
                'id_cms_menus' => 78,
                'id_cms_privileges' => 1,
            ),
            130 => 
            array (
                'id' => 198,
                'id_cms_menus' => 76,
                'id_cms_privileges' => 11,
            ),
            131 => 
            array (
                'id' => 199,
                'id_cms_menus' => 76,
                'id_cms_privileges' => 1,
            ),
            132 => 
            array (
                'id' => 203,
                'id_cms_menus' => 75,
                'id_cms_privileges' => 11,
            ),
            133 => 
            array (
                'id' => 204,
                'id_cms_menus' => 75,
                'id_cms_privileges' => 1,
            ),
            134 => 
            array (
                'id' => 205,
                'id_cms_menus' => 89,
                'id_cms_privileges' => 11,
            ),
            135 => 
            array (
                'id' => 206,
                'id_cms_menus' => 89,
                'id_cms_privileges' => 1,
            ),
            136 => 
            array (
                'id' => 207,
                'id_cms_menus' => 66,
                'id_cms_privileges' => 11,
            ),
            137 => 
            array (
                'id' => 208,
                'id_cms_menus' => 66,
                'id_cms_privileges' => 1,
            ),
            138 => 
            array (
                'id' => 209,
                'id_cms_menus' => 102,
                'id_cms_privileges' => 11,
            ),
            139 => 
            array (
                'id' => 210,
                'id_cms_menus' => 102,
                'id_cms_privileges' => 1,
            ),
            140 => 
            array (
                'id' => 211,
                'id_cms_menus' => 103,
                'id_cms_privileges' => 11,
            ),
            141 => 
            array (
                'id' => 212,
                'id_cms_menus' => 103,
                'id_cms_privileges' => 1,
            ),
            142 => 
            array (
                'id' => 213,
                'id_cms_menus' => 110,
                'id_cms_privileges' => 1,
            ),
            143 => 
            array (
                'id' => 214,
                'id_cms_menus' => 105,
                'id_cms_privileges' => 1,
            ),
            144 => 
            array (
                'id' => 215,
                'id_cms_menus' => 14,
                'id_cms_privileges' => 11,
            ),
            145 => 
            array (
                'id' => 216,
                'id_cms_menus' => 14,
                'id_cms_privileges' => 1,
            ),
            146 => 
            array (
                'id' => 217,
                'id_cms_menus' => 111,
                'id_cms_privileges' => 11,
            ),
            147 => 
            array (
                'id' => 218,
                'id_cms_menus' => 111,
                'id_cms_privileges' => 1,
            ),
            148 => 
            array (
                'id' => 219,
                'id_cms_menus' => 112,
                'id_cms_privileges' => 11,
            ),
            149 => 
            array (
                'id' => 220,
                'id_cms_menus' => 112,
                'id_cms_privileges' => 1,
            ),
            150 => 
            array (
                'id' => 221,
                'id_cms_menus' => 113,
                'id_cms_privileges' => 1,
            ),
            151 => 
            array (
                'id' => 222,
                'id_cms_menus' => 114,
                'id_cms_privileges' => 1,
            ),
            152 => 
            array (
                'id' => 223,
                'id_cms_menus' => 115,
                'id_cms_privileges' => 1,
            ),
            153 => 
            array (
                'id' => 224,
                'id_cms_menus' => 116,
                'id_cms_privileges' => 1,
            ),
            154 => 
            array (
                'id' => 225,
                'id_cms_menus' => 117,
                'id_cms_privileges' => 1,
            ),
            155 => 
            array (
                'id' => 226,
                'id_cms_menus' => 118,
                'id_cms_privileges' => 1,
            ),
            156 => 
            array (
                'id' => 227,
                'id_cms_menus' => 119,
                'id_cms_privileges' => 1,
            ),
        ));
        
        
    }
}