<?php

use Illuminate\Database\Seeder;

class PeriodeMrsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('periode_mrs')->delete();
        
        \DB::table('periode_mrs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 06:00:33',
                'updated_at' => '2017-10-16 17:21:26',
                'judul' => 'mrgemilang010117',
                'tglmulai' => '2017-09-01',
                'tglselesai' => '2017-10-31',
                'catatan' => 'material request gemilang awal bulan',
                'status' => 'true',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-10-16 17:13:45',
                'updated_at' => NULL,
                'judul' => 'MR oktober',
                'tglmulai' => '2017-10-01',
                'tglselesai' => '2017-11-30',
                'catatan' => 'material request 2',
                'status' => 'false',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-10-16 17:14:04',
                'updated_at' => NULL,
                'judul' => 'MR november',
                'tglmulai' => '2017-11-01',
                'tglselesai' => '2017-11-30',
                'catatan' => 'mr 3',
                'status' => 'false',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-10-16 17:14:34',
                'updated_at' => NULL,
                'judul' => 'MR february 2018',
                'tglmulai' => '2018-02-01',
                'tglselesai' => '2018-02-28',
                'catatan' => 'mr february 2018',
                'status' => 'false',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-10-16 17:15:56',
                'updated_at' => NULL,
                'judul' => 'MR 2016',
                'tglmulai' => '2016-01-01',
                'tglselesai' => '2016-12-31',
                'catatan' => 'test data',
                'status' => 'false',
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-10-16 17:22:01',
                'updated_at' => NULL,
                'judul' => 'MR januari 2018',
                'tglmulai' => '2018-01-01',
                'tglselesai' => '2018-01-31',
                'catatan' => 'test data 2',
                'status' => NULL,
            ),
        ));
        
        
    }
}