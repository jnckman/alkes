<?php

use Illuminate\Database\Seeder;

class BarangGudangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('barang_gudangs')->delete();
        
        \DB::table('barang_gudangs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:05:58',
                'kode' => 'b001',
                'barangs_id' => 1,
                'gudangs_id' => 1,
                'stok' => 15,
                'min' => 0,
                'max' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:02:01',
                'kode' => 'b002',
                'barangs_id' => 2,
                'gudangs_id' => 1,
                'stok' => 100,
                'min' => 0,
                'max' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 15:58:14',
                'kode' => 'b003',
                'barangs_id' => 3,
                'gudangs_id' => 1,
                'stok' => 99,
                'min' => 0,
                'max' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:07:21',
                'kode' => 'b004',
                'barangs_id' => 4,
                'gudangs_id' => 1,
                'stok' => 8,
                'min' => 0,
                'max' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:05:58',
                'kode' => 'b001',
                'barangs_id' => 5,
                'gudangs_id' => 1,
                'stok' => 45,
                'min' => 0,
                'max' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:02:01',
                'kode' => 'b002',
                'barangs_id' => 6,
                'gudangs_id' => 1,
                'stok' => 11,
                'min' => 0,
                'max' => 0,
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 15:58:14',
                'kode' => 'b003',
                'barangs_id' => 7,
                'gudangs_id' => 1,
                'stok' => 89,
                'min' => 0,
                'max' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:07:21',
                'kode' => 'b004',
                'barangs_id' => 8,
                'gudangs_id' => 1,
                'stok' => 138,
                'min' => 0,
                'max' => 0,
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:05:58',
                'kode' => 'b001',
                'barangs_id' => 9,
                'gudangs_id' => 1,
                'stok' => 45,
                'min' => 0,
                'max' => 0,
            ),
            9 => 
            array (
                'id' => 10,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:02:01',
                'kode' => 'b002',
                'barangs_id' => 10,
                'gudangs_id' => 1,
                'stok' => 20,
                'min' => 0,
                'max' => 0,
            ),
            10 => 
            array (
                'id' => 11,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 15:58:14',
                'kode' => 'b003',
                'barangs_id' => 11,
                'gudangs_id' => 1,
                'stok' => 86,
                'min' => 0,
                'max' => 0,
            ),
            11 => 
            array (
                'id' => 12,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:07:21',
                'kode' => 'b004',
                'barangs_id' => 12,
                'gudangs_id' => 1,
                'stok' => 136,
                'min' => 0,
                'max' => 0,
            ),
            12 => 
            array (
                'id' => 13,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:05:58',
                'kode' => 'b001',
                'barangs_id' => 13,
                'gudangs_id' => 1,
                'stok' => 45,
                'min' => 0,
                'max' => 0,
            ),
            13 => 
            array (
                'id' => 14,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 16:02:01',
                'kode' => 'b002',
                'barangs_id' => 14,
                'gudangs_id' => 1,
                'stok' => 20,
                'min' => 0,
                'max' => 0,
            ),
            14 => 
            array (
                'id' => 15,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 03:51:35',
                'updated_at' => '2017-10-04 15:58:14',
                'kode' => 'b003',
                'barangs_id' => 15,
                'gudangs_id' => 1,
                'stok' => 100,
                'min' => 0,
                'max' => 0,
            ),
        ));
        
        
    }
}