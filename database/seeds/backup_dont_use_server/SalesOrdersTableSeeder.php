<?php

use Illuminate\Database\Seeder;

class SalesOrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sales_orders')->delete();
        
        \DB::table('sales_orders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:27:47',
                'updated_at' => '2017-10-31 10:27:47',
                'nomor' => 'SO-1710-1',
                'user_id' => 1,
                'total' => 275000,
                'metode_bayar' => 'Tunai',
                'jatuh_tempo' => NULL,
                'status' => 'selesai',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => '2017-10-31 10:28:06',
                'nomor' => 'SO-1710-2',
                'user_id' => 1,
                'total' => 415000,
                'metode_bayar' => 'Tunai',
                'jatuh_tempo' => NULL,
                'status' => 'new',
            ),
        ));
        
        
    }
}