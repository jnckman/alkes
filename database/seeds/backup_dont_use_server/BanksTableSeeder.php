<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('banks')->delete();

        \DB::table('banks')->insert(array (
            0 =>
            array (
                'id' => 1,
                'customers_id' => 1,
                'norek' => 'lol',
                'bank' => 'lol',
                'atasnama' => 'lol',
                'created_at' => '2017-09-21 03:54:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'customers_id' => 1,
                'norek' => '2',
                'bank' => '2',
                'atasnama' => '2',
                'created_at' => '2017-09-21 03:47:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'customers_id' => 3,
                'norek' => '02222395847',
                'bank' => 'BNI',
                'atasnama' => 'Daniel Roy',
                'created_at' => '2017-09-21 04:05:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes',
                'created_at' => '2017-09-21 03:54:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes2',
                'created_at' => '2017-09-21 03:54:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes3',
                'created_at' => '2017-09-21 03:54:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes3',
                'created_at' => '2017-09-21 03:55:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes3',
                'created_at' => '2017-09-21 03:56:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'customers_id' => 1,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tes3',
                'created_at' => '2017-09-21 03:56:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'customers_id' => 2,
                'norek' => 'tes',
                'bank' => 'tes',
                'atasnama' => 'tt',
                'created_at' => '2017-09-21 03:57:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'customers_id' => 1,
                'norek' => 'dwxx',
                'bank' => 'dw',
                'atasnama' => 'd12',
                'created_at' => '2017-09-21 03:58:20',
                'updated_at' => '2017-09-20 20:58:20',
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'customers_id' => 2,
                'norek' => 'tas',
                'bank' => 'tes',
                'atasnama' => 'tos',
                'created_at' => '2017-09-21 04:07:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'customers_id' => 2,
                'norek' => 'tes',
                'bank' => 'tis',
                'atasnama' => 'eo',
                'created_at' => '2017-09-21 04:07:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'customers_id' => 2,
                'norek' => 'ooo',
                'bank' => 'ooo',
                'atasnama' => 'ooo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
