<?php

use Illuminate\Database\Seeder;

class RetursoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('returso')->delete();
        
        \DB::table('returso')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 00:00:00',
                'updated_at' => NULL,
                'sales_order_id' => 1,
                'nomor' => 'no123',
                'status' => 'apalah',
                'keterangan' => 'buang panas',
            ),
        ));
        
        
    }
}