<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'migration' => '2017_10_18_050615_create_alamats_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'migration' => '2017_10_18_050615_create_assign_users_table',
                'batch' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'migration' => '2017_10_18_050615_create_banks_table',
                'batch' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'migration' => '2017_10_18_050615_create_barang_gudangs_table',
                'batch' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'migration' => '2017_10_18_050615_create_barangdatangs_table',
                'batch' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'migration' => '2017_10_18_050615_create_barangs_table',
                'batch' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'migration' => '2017_10_18_050615_create_cms_apicustom_table',
                'batch' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'migration' => '2017_10_18_050615_create_cms_apikey_table',
                'batch' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'migration' => '2017_10_18_050615_create_cms_dashboard_table',
                'batch' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'migration' => '2017_10_18_050615_create_cms_email_queues_table',
                'batch' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'migration' => '2017_10_18_050615_create_cms_email_templates_table',
                'batch' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'migration' => '2017_10_18_050615_create_cms_logs_table',
                'batch' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'migration' => '2017_10_18_050615_create_cms_menus_privileges_table',
                'batch' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'migration' => '2017_10_18_050615_create_cms_menus_table',
                'batch' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'migration' => '2017_10_18_050615_create_cms_moduls_table',
                'batch' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'migration' => '2017_10_18_050615_create_cms_notifications_table',
                'batch' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'migration' => '2017_10_18_050615_create_cms_privileges_roles_table',
                'batch' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'migration' => '2017_10_18_050615_create_cms_privileges_table',
                'batch' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'migration' => '2017_10_18_050615_create_cms_settings_table',
                'batch' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'migration' => '2017_10_18_050615_create_cms_statistic_components_table',
                'batch' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'migration' => '2017_10_18_050615_create_cms_statistics_table',
                'batch' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'migration' => '2017_10_18_050615_create_cms_users_table',
                'batch' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'migration' => '2017_10_18_050615_create_customers_table',
                'batch' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'migration' => '2017_10_18_050615_create_groupbys_table',
                'batch' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'migration' => '2017_10_18_050615_create_groups_table',
                'batch' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'migration' => '2017_10_18_050615_create_gudangs_table',
                'batch' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'migration' => '2017_10_18_050615_create_harga_juals_table',
                'batch' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'migration' => '2017_10_18_050615_create_isi_mr_departments_table',
                'batch' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'migration' => '2017_10_18_050615_create_isi_mr_subdepts_table',
                'batch' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'migration' => '2017_10_18_050615_create_isi_pos_table',
                'batch' => 1,
            ),
            30 => 
            array (
                'id' => 31,
                'migration' => '2017_10_18_050615_create_isi_rekaps_table',
                'batch' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'migration' => '2017_10_18_050615_create_isispbms_table',
                'batch' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'migration' => '2017_10_18_050615_create_kategoris_table',
                'batch' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'migration' => '2017_10_18_050615_create_listbahans_table',
                'batch' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'migration' => '2017_10_18_050615_create_listrequesteditpos_table',
                'batch' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'migration' => '2017_10_18_050615_create_lock_rekaps_table',
                'batch' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'migration' => '2017_10_18_050615_create_log_mrs_table',
                'batch' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'migration' => '2017_10_18_050615_create_logpurchaserequests_table',
                'batch' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'migration' => '2017_10_18_050615_create_logspbms_table',
                'batch' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'migration' => '2017_10_18_050615_create_master_projects_table',
                'batch' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'migration' => '2017_10_18_050615_create_mr_departments_table',
                'batch' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'migration' => '2017_10_18_050615_create_mr_subdepts_table',
                'batch' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'migration' => '2017_10_18_050615_create_pengambilanbarangs_table',
                'batch' => 1,
            ),
            43 => 
            array (
                'id' => 44,
                'migration' => '2017_10_18_050615_create_periode_mrs_table',
                'batch' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'migration' => '2017_10_18_050615_create_pricelists_table',
                'batch' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'migration' => '2017_10_18_050615_create_projects_table',
                'batch' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'migration' => '2017_10_18_050615_create_purchaseorders_table',
                'batch' => 1,
            ),
            47 => 
            array (
                'id' => 48,
                'migration' => '2017_10_18_050615_create_rekap_mrs_table',
                'batch' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'migration' => '2017_10_18_050615_create_request_barangs_table',
                'batch' => 1,
            ),
            49 => 
            array (
                'id' => 50,
                'migration' => '2017_10_18_050615_create_reseps_table',
                'batch' => 1,
            ),
            50 => 
            array (
                'id' => 51,
                'migration' => '2017_10_18_050615_create_returbarangs_table',
                'batch' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'migration' => '2017_10_18_050615_create_satuans_table',
                'batch' => 1,
            ),
            52 => 
            array (
                'id' => 53,
                'migration' => '2017_10_18_050615_create_split_logs_table',
                'batch' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'migration' => '2017_10_18_050615_create_suppliers_table',
                'batch' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'migration' => '2017_10_18_050615_create_transferlogs_table',
                'batch' => 1,
            ),
            55 => 
            array (
                'id' => 56,
                'migration' => '2017_10_18_050615_create_wellhos_departments_table',
                'batch' => 1,
            ),
            56 => 
            array (
                'id' => 57,
                'migration' => '2017_10_18_050615_create_wellhos_subdepts_table',
                'batch' => 1,
            ),
            57 => 
            array (
                'id' => 58,
                'migration' => '2017_10_18_051121_create_pengiriman_barangs',
                'batch' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'migration' => '2017_10_18_051446_create_detail_pengiriman_barangs',
                'batch' => 1,
            ),
            59 => 
            array (
                'id' => 60,
                'migration' => '2017_10_18_051538_create_pengambilan_barangs',
                'batch' => 1,
            ),
            60 => 
            array (
                'id' => 61,
                'migration' => '2017_10_18_051603_create_detail_pengambilan_barangs',
                'batch' => 1,
            ),
            61 => 
            array (
                'id' => 62,
                'migration' => '2017_10_18_121608_add_column_gudang_id',
                'batch' => 1,
            ),
            62 => 
            array (
                'id' => 63,
                'migration' => '2017_10_19_115309_tambah_field_gambar_request_barangs',
                'batch' => 1,
            ),
            63 => 
            array (
                'id' => 64,
                'migration' => '2017_10_21_073313_create_table_sales_orders',
                'batch' => 1,
            ),
            64 => 
            array (
                'id' => 65,
                'migration' => '2017_10_25_040847_add_status_table_pengiriman',
                'batch' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'migration' => '2017_10_29_200008_add_field_pengiriman_id',
                'batch' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'migration' => '2017_10_29_203450_table_surat_jalan',
                'batch' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'migration' => '2017_10_29_203506_table_tanda_terima',
                'batch' => 1,
            ),
            68 => 
            array (
                'id' => 69,
                'migration' => '2017_10_31_063611_perbaiki_kesalahan_rase',
                'batch' => 1,
            ),
            69 => 
            array (
                'id' => 70,
                'migration' => '2017_10_31_101957_bikin_isi_returso',
                'batch' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'migration' => '2017_10_31_102549_create_returso_123',
                'batch' => 1,
            ),
            71 => 
            array (
                'id' => 72,
                'migration' => '2017_11_01_102153_edit_pengambilan_pengiriman_barangs',
                'batch' => 2,
            ),
            72 => 
            array (
                'id' => 73,
                'migration' => '2017_11_01_112519_log_pengirimans',
                'batch' => 3,
            ),
            73 => 
            array (
                'id' => 83,
                'migration' => '2017_11_05_074531_add_kode_kirim_surat_jalans',
                'batch' => 4,
            ),
            74 => 
            array (
                'id' => 84,
                'migration' => '2017_11_05_084133_edit_tanda_terimas',
                'batch' => 5,
            ),
        ));
        
        
    }
}