<?php

use Illuminate\Database\Seeder;

class MasterProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('master_projects')->delete();
        
        \DB::table('master_projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 20:03:22',
                'updated_at' => NULL,
                'nama' => 'klub malam',
                'alamat' => 'jl klub malam',
                'no telp' => '',
                'email' => 'klubmalam@gmail.com',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:07:23',
                'updated_at' => NULL,
                'nama' => 'ukdw',
                'alamat' => 'ukdw',
                'no telp' => '',
                'email' => 'ukdw@gmail.com',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:07:39',
                'updated_at' => NULL,
                'nama' => 'uksw',
                'alamat' => 'uksw',
                'no telp' => '',
                'email' => 'uksw@gmail.com',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:07:55',
                'updated_at' => NULL,
                'nama' => 'liquid',
                'alamat' => 'liquid',
                'no telp' => '',
                'email' => 'liquid@gmail.com',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 05:19:19',
                'updated_at' => NULL,
                'nama' => 'Pacific Building',
                'alamat' => 'jl adi suciptio 56 , Yogyakarta, Indonesia',
                'no telp' => '',
                'email' => 'pb@gmail.com',
            ),
        ));
        
        
    }
}