<?php

use Illuminate\Database\Seeder;

class WellhosSubdeptsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wellhos_subdepts')->delete();
        
        \DB::table('wellhos_subdepts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-12 18:49:21',
                'updated_at' => NULL,
                'nama' => 'subdept1',
                'keterangan' => 'subdept1 is good too',
                'phone' => '0000000',
                'email' => 'subdept1@gmail.com',
                'pic' => 'mr subdept1',
                'wellhos_departments_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:15:12',
                'updated_at' => NULL,
                'nama' => 'subdept12',
                'keterangan' => 'dd',
                'phone' => '21321321321',
                'email' => 'tes@gmail.com',
                'pic' => 'tes',
                'wellhos_departments_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 05:17:25',
                'updated_at' => NULL,
                'nama' => 'ASM 1',
                'keterangan' => 'asm 1',
                'phone' => '000000',
                'email' => 'asm1@gmail.com',
                'pic' => 'mr asm',
                'wellhos_departments_id' => 4,
            ),
        ));
        
        
    }
}