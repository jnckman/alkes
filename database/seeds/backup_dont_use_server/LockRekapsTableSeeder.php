<?php

use Illuminate\Database\Seeder;

class LockRekapsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lock_rekaps')->delete();
        
        \DB::table('lock_rekaps')->insert(array (
            0 => 
            array (
                'id' => 18,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'periode_mrs_id' => 1,
                'status' => 'fresh added',
                'jumlah' => 60,
                'barangs_id' => 1,
                'pricelists_id' => 1,
            ),
            1 => 
            array (
                'id' => 19,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'periode_mrs_id' => 1,
                'status' => 'fresh added',
                'jumlah' => 233,
                'barangs_id' => 2,
                'pricelists_id' => NULL,
            ),
            2 => 
            array (
                'id' => 20,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'periode_mrs_id' => 1,
                'status' => 'fresh added',
                'jumlah' => 199,
                'barangs_id' => 3,
                'pricelists_id' => NULL,
            ),
            3 => 
            array (
                'id' => 21,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'periode_mrs_id' => 1,
                'status' => 'fresh added',
                'jumlah' => 1600,
                'barangs_id' => 4,
                'pricelists_id' => NULL,
            ),
        ));
        
        
    }
}