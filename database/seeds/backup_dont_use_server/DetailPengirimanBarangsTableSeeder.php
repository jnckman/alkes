<?php

use Illuminate\Database\Seeder;

class DetailPengirimanBarangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('detail_pengiriman_barangs')->delete();
        
        \DB::table('detail_pengiriman_barangs')->insert(array (
            0 => 
            array (
                'id' => 44,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 1,
                'barang_id' => 2,
                'jumlah' => 50,
            ),
            1 => 
            array (
                'id' => 45,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 1,
                'barang_id' => 3,
                'jumlah' => 50,
            ),
            2 => 
            array (
                'id' => 46,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 1,
                'barang_id' => 7,
                'jumlah' => 50,
            ),
            3 => 
            array (
                'id' => 47,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 1,
                'barang_id' => 8,
                'jumlah' => 50,
            ),
            4 => 
            array (
                'id' => 48,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 3,
                'jumlah' => 40,
            ),
            5 => 
            array (
                'id' => 49,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 7,
                'jumlah' => 20,
            ),
            6 => 
            array (
                'id' => 50,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 9,
                'jumlah' => 10,
            ),
            7 => 
            array (
                'id' => 51,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 10,
                'jumlah' => 10,
            ),
            8 => 
            array (
                'id' => 52,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 11,
                'jumlah' => 10,
            ),
            9 => 
            array (
                'id' => 53,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 12,
                'jumlah' => 100,
            ),
            10 => 
            array (
                'id' => 54,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 2,
                'barang_id' => 13,
                'jumlah' => 20,
            ),
            11 => 
            array (
                'id' => 55,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 3,
                'barang_id' => 7,
                'jumlah' => 10,
            ),
            12 => 
            array (
                'id' => 56,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'pengiriman_barang_id' => 3,
                'barang_id' => 8,
                'jumlah' => 50,
            ),
        ));
        
        
    }
}