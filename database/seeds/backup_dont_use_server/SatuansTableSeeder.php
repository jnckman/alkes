<?php

use Illuminate\Database\Seeder;

class SatuansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('satuans')->delete();
        
        \DB::table('satuans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'PCS',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'PACK',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'BOX',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'METER',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'LITER',
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'BOTOL',
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'KG',
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'KALENG',
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'SET',
            ),
            9 => 
            array (
                'id' => 10,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'PASANG',
            ),
            10 => 
            array (
                'id' => 11,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'KARUNG',
            ),
            11 => 
            array (
                'id' => 12,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'GALON ',
            ),
            12 => 
            array (
                'id' => 13,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'PAIL',
            ),
            13 => 
            array (
                'id' => 14,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'STEL',
            ),
            14 => 
            array (
                'id' => 15,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'SETEL',
            ),
            15 => 
            array (
                'id' => 16,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'LEMBAR',
            ),
            16 => 
            array (
                'id' => 17,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'LEMBAR A3',
            ),
            17 => 
            array (
                'id' => 18,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 13:01:36',
                'updated_at' => NULL,
                'nama' => 'RIM',
            ),
        ));
        
        
    }
}