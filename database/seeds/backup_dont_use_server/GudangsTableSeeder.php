<?php

use Illuminate\Database\Seeder;

class GudangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gudangs')->delete();
        
        \DB::table('gudangs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-12 19:03:03',
                'updated_at' => NULL,
                'nama' => 'gudang 1',
                'kode' => 'g01',
                'notelp' => '00000',
                'email' => 'gudang1@gmail.com',
                'pic' => 'mr g01',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 14:08:35',
                'updated_at' => NULL,
                'nama' => 'gudang 2',
                'kode' => 'g2',
                'notelp' => '000000',
                'email' => 'gudang2@gmail.com',
                'pic' => 'bp gudang 2',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 14:08:51',
                'updated_at' => NULL,
                'nama' => 'gudang 3',
                'kode' => 'g3',
                'notelp' => '000000',
                'email' => 'gudang3@gmail.com',
                'pic' => 'bp gudang 3',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 05:20:27',
                'updated_at' => NULL,
                'nama' => 'gudang 4',
                'kode' => 'g4',
                'notelp' => '0298 33321',
                'email' => 'gudang4@gmail.com',
                'pic' => 'bp budi',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 08:59:53',
                'updated_at' => NULL,
                'nama' => 'gudang_cacat',
                'kode' => 'g-5',
                'notelp' => '0000000000',
                'email' => 'gudang@cacat.cacat',
                'pic' => NULL,
            ),
        ));
        
        
    }
}