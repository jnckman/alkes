<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('customers')->delete();
        
        \DB::table('customers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 07:51:12',
                'updated_at' => '2017-09-20 07:51:24',
                'nocustomer' => '1',
                'nama' => 'lol',
                'alamat invoice' => '',
                'npwp' => '1',
                'notelp' => '1',
                'jenis' => '1',
                'namapemilik' => '1',
                'alamatpemilik' => '1',
                'noktp' => '1',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 16:16:06',
                'updated_at' => NULL,
                'nocustomer' => 'c2',
                'nama' => 'c22',
                'alamat invoice' => '',
                'npwp' => 'c2',
                'notelp' => 'c2',
                'jenis' => 'c2',
                'namapemilik' => 'c2',
                'alamatpemilik' => 'c2',
                'noktp' => 'c2',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 16:16:21',
                'updated_at' => NULL,
                'nocustomer' => 'c3',
                'nama' => 'c33',
                'alamat invoice' => '',
                'npwp' => 'c3',
                'notelp' => 'c3',
                'jenis' => 'c3',
                'namapemilik' => 'c3',
                'alamatpemilik' => 'c3',
                'noktp' => 'c3',
            ),
        ));
        
        
    }
}