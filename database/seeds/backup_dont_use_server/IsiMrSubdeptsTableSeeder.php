<?php

use Illuminate\Database\Seeder;

class IsiMrSubdeptsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('isi_mr_subdepts')->delete();
        
        \DB::table('isi_mr_subdepts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:39:12',
                'updated_at' => NULL,
                'barangs_id' => 1,
                'mr_subdepts_id' => 1,
                'jumlah' => 10,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:41:41',
                'updated_at' => NULL,
                'barangs_id' => 2,
                'mr_subdepts_id' => 1,
                'jumlah' => 200,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:06:43',
                'updated_at' => NULL,
                'barangs_id' => 4,
                'mr_subdepts_id' => 1,
                'jumlah' => 100,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:17',
                'updated_at' => NULL,
                'barangs_id' => 2,
                'mr_subdepts_id' => 3,
                'jumlah' => 213,
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:23',
                'updated_at' => NULL,
                'barangs_id' => 3,
                'mr_subdepts_id' => 3,
                'jumlah' => 199,
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 4,
                'mr_subdepts_id' => 2,
                'jumlah' => 1500,
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 1,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:39:12',
                'updated_at' => NULL,
                'barangs_id' => 6,
                'mr_subdepts_id' => 1,
                'jumlah' => 10,
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:41:41',
                'updated_at' => NULL,
                'barangs_id' => 7,
                'mr_subdepts_id' => 1,
                'jumlah' => 20,
            ),
            9 => 
            array (
                'id' => 10,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:06:43',
                'updated_at' => NULL,
                'barangs_id' => 8,
                'mr_subdepts_id' => 1,
                'jumlah' => 100,
            ),
            10 => 
            array (
                'id' => 11,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:39:12',
                'updated_at' => NULL,
                'barangs_id' => 11,
                'mr_subdepts_id' => 1,
                'jumlah' => 10,
            ),
            11 => 
            array (
                'id' => 17,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:06:43',
                'updated_at' => NULL,
                'barangs_id' => 3,
                'mr_subdepts_id' => 1,
                'jumlah' => 100,
            ),
            12 => 
            array (
                'id' => 18,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:41:41',
                'updated_at' => NULL,
                'barangs_id' => 5,
                'mr_subdepts_id' => 1,
                'jumlah' => 20,
            ),
            13 => 
            array (
                'id' => 19,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:39:12',
                'updated_at' => NULL,
                'barangs_id' => 9,
                'mr_subdepts_id' => 1,
                'jumlah' => 10,
            ),
            14 => 
            array (
                'id' => 20,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:39:12',
                'updated_at' => NULL,
                'barangs_id' => 10,
                'mr_subdepts_id' => 1,
                'jumlah' => 10,
            ),
            15 => 
            array (
                'id' => 21,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:06:43',
                'updated_at' => NULL,
                'barangs_id' => 12,
                'mr_subdepts_id' => 1,
                'jumlah' => 100,
            ),
            16 => 
            array (
                'id' => 22,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:41:41',
                'updated_at' => NULL,
                'barangs_id' => 13,
                'mr_subdepts_id' => 1,
                'jumlah' => 20,
            ),
            17 => 
            array (
                'id' => 23,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:17',
                'updated_at' => NULL,
                'barangs_id' => 1,
                'mr_subdepts_id' => 3,
                'jumlah' => 213,
            ),
            18 => 
            array (
                'id' => 24,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:23',
                'updated_at' => NULL,
                'barangs_id' => 4,
                'mr_subdepts_id' => 3,
                'jumlah' => 199,
            ),
            19 => 
            array (
                'id' => 25,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:17',
                'updated_at' => NULL,
                'barangs_id' => 5,
                'mr_subdepts_id' => 3,
                'jumlah' => 213,
            ),
            20 => 
            array (
                'id' => 26,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:17',
                'updated_at' => NULL,
                'barangs_id' => 6,
                'mr_subdepts_id' => 3,
                'jumlah' => 213,
            ),
            21 => 
            array (
                'id' => 27,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:23',
                'updated_at' => NULL,
                'barangs_id' => 7,
                'mr_subdepts_id' => 3,
                'jumlah' => 199,
            ),
            22 => 
            array (
                'id' => 28,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:23',
                'updated_at' => NULL,
                'barangs_id' => 8,
                'mr_subdepts_id' => 3,
                'jumlah' => 199,
            ),
            23 => 
            array (
                'id' => 29,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 2,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            24 => 
            array (
                'id' => 30,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 3,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            25 => 
            array (
                'id' => 31,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 4,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            26 => 
            array (
                'id' => 32,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 5,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            27 => 
            array (
                'id' => 33,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 6,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            28 => 
            array (
                'id' => 34,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 7,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            29 => 
            array (
                'id' => 35,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:32:44',
                'updated_at' => NULL,
                'barangs_id' => 8,
                'mr_subdepts_id' => 5,
                'jumlah' => 50,
            ),
            30 => 
            array (
                'id' => 36,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 1,
                'mr_subdepts_id' => 2,
                'jumlah' => 100,
            ),
            31 => 
            array (
                'id' => 37,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 5,
                'mr_subdepts_id' => 2,
                'jumlah' => 1500,
            ),
            32 => 
            array (
                'id' => 38,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 6,
                'mr_subdepts_id' => 2,
                'jumlah' => 50,
            ),
            33 => 
            array (
                'id' => 39,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 2,
                'mr_subdepts_id' => 2,
                'jumlah' => 100,
            ),
            34 => 
            array (
                'id' => 40,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 3,
                'mr_subdepts_id' => 2,
                'jumlah' => 100,
            ),
            35 => 
            array (
                'id' => 41,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 7,
                'mr_subdepts_id' => 2,
                'jumlah' => 1500,
            ),
            36 => 
            array (
                'id' => 42,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:40',
                'updated_at' => NULL,
                'barangs_id' => 8,
                'mr_subdepts_id' => 2,
                'jumlah' => 50,
            ),
        ));
        
        
    }
}