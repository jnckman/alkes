<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('groups')->delete();
        
        \DB::table('groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-10-16 17:08:53',
                'updated_at' => '2017-10-23 08:32:45',
                'deleted_at' => NULL,
                'nama' => 'Sikat1',
                'counter' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2017-10-16 17:09:37',
                'updated_at' => '2017-10-16 17:10:33',
                'deleted_at' => NULL,
                'nama' => 'SAPU3',
                'counter' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2017-10-16 17:10:22',
                'updated_at' => '2017-10-23 05:17:24',
                'deleted_at' => NULL,
                'nama' => 'Sikat dan Sapu 01',
                'counter' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2017-10-16 17:11:20',
                'updated_at' => '2017-10-23 05:17:37',
                'deleted_at' => NULL,
                'nama' => 'Soda',
                'counter' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2017-10-16 17:11:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'nama' => 'Campur01',
                'counter' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2017-10-23 09:59:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'nama' => 'Groupb0001-b0010',
                'counter' => 0,
            ),
        ));
        
        
    }
}