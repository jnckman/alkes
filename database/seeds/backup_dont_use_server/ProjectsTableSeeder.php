<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 20:03:45',
                'updated_at' => NULL,
                'nama' => 'boshe',
                'master_projects_id' => 1,
                'PIC' => 'mas boshe',
                'email' => 'boshe@gmail.com',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 20:04:17',
                'updated_at' => NULL,
                'nama' => 'liquid',
                'master_projects_id' => 1,
                'PIC' => 'mas liquid',
                'email' => 'liquid@gmail.com',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 20:06:53',
                'updated_at' => NULL,
                'nama' => 'terace',
                'master_projects_id' => 1,
                'PIC' => 'pak terace',
                'email' => 'terace@gmail.com',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:08:26',
                'updated_at' => NULL,
                'nama' => 'ukdw FTI',
                'master_projects_id' => 2,
                'PIC' => 'mr ukdw',
                'email' => 'mrukdw@gmail.com',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:08:47',
                'updated_at' => NULL,
                'nama' => 'ukdw BIO',
                'master_projects_id' => 2,
                'PIC' => 'mrukdwbio',
                'email' => 'mrukdwbio@gmail.com',
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:09:12',
                'updated_at' => NULL,
                'nama' => 'uksw salatiga',
                'master_projects_id' => 3,
                'PIC' => 'ukswsalatiga',
                'email' => 'uksws3@gmail.com',
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:28:54',
                'updated_at' => NULL,
                'nama' => 'project ukdw x',
                'master_projects_id' => 2,
                'PIC' => 'mr x',
                'email' => 'x@gmail.com',
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:29:19',
                'updated_at' => NULL,
                'nama' => 'project uksw x',
                'master_projects_id' => 3,
                'PIC' => 'mr x uksw',
                'email' => 'xuksw@gmail.com',
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 05:19:43',
                'updated_at' => NULL,
                'nama' => 'pacific building solo',
                'master_projects_id' => 5,
                'PIC' => 'bp agus',
                'email' => 'agus@gmail.com',
            ),
            9 => 
            array (
                'id' => 315,
                'deleted_at' => NULL,
                'created_at' => '2017-10-08 09:48:37',
                'updated_at' => '2017-10-08 09:48:37',
                'nama' => 'G13-0044 ',
                'master_projects_id' => NULL,
                'PIC' => 'IBU UMI',
                'email' => NULL,
            ),
        ));
        
        
    }
}