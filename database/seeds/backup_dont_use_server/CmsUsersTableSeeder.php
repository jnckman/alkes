<?php

use Illuminate\Database\Seeder;

class CmsUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cms_users')->delete();
        
        \DB::table('cms_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin',
                'photo' => NULL,
                'email' => 'admin@crudbooster.com',
                'password' => '$2y$10$ebzeWmn9Yn3ym0LjrO7WketkAhimWuSLDwDw.Q8UiPmYVNv7q6tY.',
                'id_cms_privileges' => 1,
                'created_at' => '2017-09-12 13:57:35',
                'updated_at' => NULL,
                'status' => 'Active',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'UserCoba',
                'photo' => 'uploads/1/2017-10/battlechart.png',
                'email' => 'user@user.user',
                'password' => '$2y$10$XRB7V6ccxWG50F2WOT6BJO7Vzf60fdmtxNQWAr8z4Igeat/euO8hO',
                'id_cms_privileges' => 5,
                'created_at' => '2017-10-20 11:02:33',
                'updated_at' => NULL,
                'status' => NULL,
            ),
        ));
        
        
    }
}