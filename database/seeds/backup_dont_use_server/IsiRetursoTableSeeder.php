<?php

use Illuminate\Database\Seeder;

class IsiRetursoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('isi_returso')->delete();
        
        \DB::table('isi_returso')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:35',
                'updated_at' => NULL,
                'returSO_id' => 1,
                'isi_sales_order_id' => 2,
                'jumlah' => 1,
                'status' => 'new',
                'keterangan' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:35',
                'updated_at' => NULL,
                'returSO_id' => 1,
                'isi_sales_order_id' => 3,
                'jumlah' => 1,
                'status' => 'new',
                'keterangan' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:35',
                'updated_at' => NULL,
                'returSO_id' => 1,
                'isi_sales_order_id' => 4,
                'jumlah' => 1,
                'status' => 'new',
                'keterangan' => NULL,
            ),
        ));
        
        
    }
}