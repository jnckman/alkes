<?php

use Illuminate\Database\Seeder;

class WellhosDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wellhos_departments')->delete();
        
        \DB::table('wellhos_departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-12 18:48:20',
                'updated_at' => NULL,
                'nama' => 'department 1',
                'phone' => '00000000',
                'pic' => 'mr dept1',
                'keterangan' => 'department 1 is good',
                'email' => 'department1@gmail.com',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-12 18:48:47',
                'updated_at' => NULL,
                'nama' => 'department 2',
                'phone' => '00000000',
                'pic' => 'mr dept 2',
                'keterangan' => 'department 1 is bad',
                'email' => 'department2@gmail.com',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 21:14:43',
                'updated_at' => NULL,
                'nama' => 'department 3',
                'phone' => '000',
                'pic' => '000',
                'keterangan' => '00',
                'email' => 'tes@gmail.com',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 05:15:58',
                'updated_at' => NULL,
                'nama' => 'OPD',
                'phone' => '000000',
                'pic' => 'Mr Budi',
                'keterangan' => 'Department OPD',
                'email' => 'opd@gmail.com',
            ),
        ));
        
        
    }
}