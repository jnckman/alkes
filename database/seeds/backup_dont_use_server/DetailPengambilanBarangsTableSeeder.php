<?php

use Illuminate\Database\Seeder;

class DetailPengambilanBarangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('detail_pengambilan_barangs')->delete();
        
        \DB::table('detail_pengambilan_barangs')->insert(array (
            0 => 
            array (
                'id' => 17,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 2,
                'jumlah' => 50,
            ),
            1 => 
            array (
                'id' => 18,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 7,
                'jumlah' => 80,
            ),
            2 => 
            array (
                'id' => 19,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 8,
                'jumlah' => 100,
            ),
            3 => 
            array (
                'id' => 20,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 11,
                'jumlah' => 10,
            ),
            4 => 
            array (
                'id' => 21,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 3,
                'jumlah' => 90,
            ),
            5 => 
            array (
                'id' => 22,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 9,
                'jumlah' => 10,
            ),
            6 => 
            array (
                'id' => 23,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 10,
                'jumlah' => 10,
            ),
            7 => 
            array (
                'id' => 24,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 12,
                'jumlah' => 100,
            ),
            8 => 
            array (
                'id' => 25,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'pengambilan_barang_id' => 1,
                'barang_id' => 13,
                'jumlah' => 20,
            ),
        ));
        
        
    }
}