<?php

use Illuminate\Database\Seeder;

class MrSubdeptsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mr_subdepts')->delete();
        
        \DB::table('mr_subdepts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-16 20:24:30',
                'updated_at' => NULL,
                'tanggal' => '2017-09-11',
                'nomr' => 'mr001',
                'gudang' => 'headquarter',
                'projects_id' => 1,
                'wellhos_subdepts_id' => 1,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'oke',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:09:41',
                'updated_at' => NULL,
                'tanggal' => '2017-09-14',
                'nomr' => 'mr002',
                'gudang' => 'headquarter',
                'projects_id' => 4,
                'wellhos_subdepts_id' => 1,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'oke',
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:10:03',
                'updated_at' => NULL,
                'tanggal' => '2017-09-21',
                'nomr' => 'mr003',
                'gudang' => 'headquarter',
                'projects_id' => 6,
                'wellhos_subdepts_id' => 1,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'oke',
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-22 09:11:36',
                'updated_at' => NULL,
                'tanggal' => '2017-09-28',
                'nomr' => 'mr004',
                'gudang' => 'headquarter',
                'projects_id' => 3,
                'wellhos_subdepts_id' => 1,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'oke',
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 23:29:53',
                'updated_at' => NULL,
                'tanggal' => '2017-09-13',
                'nomr' => 'mr005',
                'gudang' => 'headquarter',
                'projects_id' => 7,
                'wellhos_subdepts_id' => 2,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'oke',
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 06:04:06',
                'updated_at' => NULL,
                'tanggal' => '2017-08-21',
                'nomr' => 'MR-PROJECT-1709-1',
                'gudang' => 'headquarter',
                'projects_id' => 1,
                'wellhos_subdepts_id' => 3,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'none',
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 06:04:44',
                'updated_at' => NULL,
                'tanggal' => '2017-09-13',
                'nomr' => 'MR-PROJECT-1709-2',
                'gudang' => 'headquarter',
                'projects_id' => 2,
                'wellhos_subdepts_id' => 3,
                'periode_mrs_id' => 1,
                'user_id' => 0,
                'approval' => 'none',
            ),
            7 => 
            array (
                'id' => 13,
                'deleted_at' => NULL,
                'created_at' => '2017-10-08 09:49:50',
                'updated_at' => '2017-10-08 09:49:50',
                'tanggal' => '2017-10-08',
                'nomr' => 'mr013',
                'gudang' => 'headquarter',
                'projects_id' => 315,
                'wellhos_subdepts_id' => 3,
                'periode_mrs_id' => 1,
                'user_id' => 1,
                'approval' => 'belum',
            ),
        ));
        
        
    }
}