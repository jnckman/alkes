<?php

use Illuminate\Database\Seeder;

class AlamatsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('alamats')->delete();
        
        \DB::table('alamats')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-20 07:52:15',
                'updated_at' => NULL,
                'alamat' => 'alamat',
                'customers_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'jl adi suciptio 56 , Yogyakarta, Indonesia',
                'customers_id' => 3,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => '2017-09-20 21:06:13',
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'jl adi suciptio 56 , Yogyakarta, Indonesia',
                'customers_id' => 3,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'Jl Jendral Sudirman 32, Jl Ahman Yani 32',
                'customers_id' => 3,
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'jl adi suciptio 56 , Yogyakarta, Indonesia',
                'customers_id' => 2,
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'aioei',
                'customers_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'dsss',
                'customers_id' => 2,
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'wadawd',
                'customers_id' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => '2017-09-20 21:11:19',
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'aaa',
                'customers_id' => 2,
            ),
            9 => 
            array (
                'id' => 10,
                'deleted_at' => '2017-09-20 21:11:15',
                'created_at' => NULL,
                'updated_at' => NULL,
                'alamat' => 'coks',
                'customers_id' => 2,
            ),
        ));
        
        
    }
}