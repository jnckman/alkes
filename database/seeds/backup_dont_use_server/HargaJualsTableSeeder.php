<?php

use Illuminate\Database\Seeder;

class HargaJualsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('harga_juals')->delete();

        \DB::table('harga_juals')->insert(array (
           0 =>
           array (
             'id' => 1,
             'created_at' => NULL,
             'updated_at' => NULL,
             'deleted_at' => NULL,
             'harga_jual' => 5000,
             'barang_id' => 10,
             'ket' => NULL,
             'harga_diskon' => 2000,
           ),
           1 =>
           array (
             'id' => 2,
             'created_at' => NULL,
             'updated_at' => NULL,
             'deleted_at' => NULL,
             'harga_jual' => 1000,
             'barang_id' => 11,
             'ket' => NULL,
             'harga_diskon' => NULL,
           ),
        ));
    }
}
