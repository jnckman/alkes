<?php

use Illuminate\Database\Seeder;

class IsiSalesOrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('isi_sales_orders')->delete();
        
        \DB::table('isi_sales_orders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:27:47',
                'updated_at' => NULL,
                'sales_orders_id' => 1,
                'barangs_id' => 11,
                'jumlah' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:27:47',
                'updated_at' => NULL,
                'sales_orders_id' => 1,
                'barangs_id' => 10,
                'jumlah' => 3,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:27:47',
                'updated_at' => NULL,
                'sales_orders_id' => 1,
                'barangs_id' => 4,
                'jumlah' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:27:47',
                'updated_at' => NULL,
                'sales_orders_id' => 1,
                'barangs_id' => 3,
                'jumlah' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 6,
                'jumlah' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 8,
                'jumlah' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 12,
                'jumlah' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 2,
                'jumlah' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 5,
                'jumlah' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 4,
                'jumlah' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'deleted_at' => NULL,
                'created_at' => '2017-10-31 10:28:06',
                'updated_at' => NULL,
                'sales_orders_id' => 2,
                'barangs_id' => 9,
                'jumlah' => 1,
            ),
        ));
        
        
    }
}