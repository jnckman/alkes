<?php

use Illuminate\Database\Seeder;

class SplitLogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('split_logs')->delete();
        
        \DB::table('split_logs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:00:26',
                'updated_at' => '2017-09-25 04:00:26',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 1,
                'barang_gudangs_id_2' => 2,
                'gudangs_id' => 1,
                'jumlah' => 5,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:00:26',
                'updated_at' => '2017-09-25 04:00:26',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 1,
                'barang_gudangs_id_2' => 3,
                'gudangs_id' => 1,
                'jumlah' => 5,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:46:06',
                'updated_at' => '2017-09-25 04:46:06',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 3,
                'barang_gudangs_id_2' => 4,
                'gudangs_id' => 1,
                'jumlah' => 5,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:46:55',
                'updated_at' => '2017-09-25 04:46:55',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 1,
                'barang_gudangs_id_2' => 5,
                'gudangs_id' => 1,
                'jumlah' => 10,
            ),
            4 => 
            array (
                'id' => 5,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:46:55',
                'updated_at' => '2017-09-25 04:46:55',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 1,
                'barang_gudangs_id_2' => 6,
                'gudangs_id' => 1,
                'jumlah' => 10,
            ),
            5 => 
            array (
                'id' => 6,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:48:26',
                'updated_at' => '2017-09-25 04:48:26',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 6,
                'barang_gudangs_id_2' => 7,
                'gudangs_id' => 1,
                'jumlah' => 5,
            ),
            6 => 
            array (
                'id' => 7,
                'deleted_at' => NULL,
                'created_at' => '2017-09-25 04:48:26',
                'updated_at' => '2017-09-25 04:48:26',
                'barangs_id' => 1,
                'barang_gudangs_id_1' => 6,
                'barang_gudangs_id_2' => 8,
                'gudangs_id' => 1,
                'jumlah' => 5,
            ),
        ));
        
        
    }
}