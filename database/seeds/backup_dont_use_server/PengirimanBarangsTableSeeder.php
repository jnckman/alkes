<?php

use Illuminate\Database\Seeder;

class PengirimanBarangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pengiriman_barangs')->delete();
        
        \DB::table('pengiriman_barangs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'tgl_kirim' => '2017-11-05',
                'kode' => 'PB-1',
                'jenis' => 'mr_baru',
                'global_id' => 5,
                'user' => 1,
                'status' => NULL,
                'pengambilan_brg_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'tgl_kirim' => '2017-11-05',
                'kode' => 'PB-1',
                'jenis' => 'mr_baru',
                'global_id' => 1,
                'user' => 1,
                'status' => NULL,
                'pengambilan_brg_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:27',
                'updated_at' => NULL,
                'tgl_kirim' => '2017-11-05',
                'kode' => 'PB-1',
                'jenis' => 'mr_baru',
                'global_id' => 2,
                'user' => 1,
                'status' => NULL,
                'pengambilan_brg_id' => 1,
            ),
        ));
        
        
    }
}