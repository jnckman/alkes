<?php

use Illuminate\Database\Seeder;

class PengambilanBarangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pengambilan_barangs')->delete();
        
        \DB::table('pengambilan_barangs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-11-05 10:34:22',
                'updated_at' => NULL,
                'tgl_ambil' => '2017-11-05',
                'user' => 1,
                'gudang_id' => 1,
                'status' => 1,
            ),
        ));
        
        
    }
}