<?php

use Illuminate\Database\Seeder;

class AssignUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assign_users')->delete();
        
        \DB::table('assign_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 1,
                'table_id' => 3,
                'table_name' => 'wellhos_subdepts',
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'table_id' => 1,
                'table_name' => 'wellhos_subdepts',
            ),
        ));
        
        
    }
}