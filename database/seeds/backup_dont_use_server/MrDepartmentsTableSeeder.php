<?php

use Illuminate\Database\Seeder;

class MrDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mr_departments')->delete();
        
        \DB::table('mr_departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-09-24 06:28:24',
                'updated_at' => NULL,
                'nomr' => 'mr001',
                'periode_mrs_id' => 1,
                'keterangan' => 'oke',
                'wellhos_departments_id' => 1,
                'approval' => 'oke',
            ),
        ));
        
        
    }
}