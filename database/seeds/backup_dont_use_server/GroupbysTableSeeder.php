<?php

use Illuminate\Database\Seeder;

class GroupbysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('groupbys')->delete();
        
        \DB::table('groupbys')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-10-16 17:09:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 5,
                'group_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2017-10-16 17:09:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 4,
                'group_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2017-10-16 17:09:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 7,
                'group_id' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2017-10-16 17:09:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 8,
                'group_id' => 2,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2017-10-16 17:09:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 10,
                'group_id' => 2,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2017-10-16 17:10:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 5,
                'group_id' => 3,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2017-10-16 17:10:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 9,
                'group_id' => 3,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2017-10-16 17:11:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 12,
                'group_id' => 4,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2017-10-16 17:11:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 15,
                'group_id' => 4,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2017-10-16 17:11:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 5,
                'group_id' => 5,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2017-10-16 17:12:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 10,
                'group_id' => 5,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2017-10-16 17:12:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 14,
                'group_id' => 5,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2017-10-16 17:12:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 14,
                'group_id' => 5,
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2017-10-23 10:00:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 16,
                'group_id' => 6,
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2017-10-23 10:00:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 18,
                'group_id' => 6,
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2017-10-23 10:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 19,
                'group_id' => 6,
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2017-10-23 10:00:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 20,
                'group_id' => 6,
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2017-10-23 10:00:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 21,
                'group_id' => 6,
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2017-10-23 10:00:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 22,
                'group_id' => 6,
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2017-10-23 10:00:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 23,
                'group_id' => 6,
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2017-10-23 10:00:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 24,
                'group_id' => 6,
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2017-10-23 10:00:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 25,
                'group_id' => 6,
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2017-10-23 10:00:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'barang_id' => 26,
                'group_id' => 6,
            ),
        ));
        
        
    }
}