<?php

use Illuminate\Database\Seeder;

class KategorisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kategoris')->delete();
        
        \DB::table('kategoris')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 14:56:14',
                'updated_at' => NULL,
                'nama' => 'CNC',
                'kode' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 14:56:14',
                'updated_at' => NULL,
                'nama' => 'UNIFORM',
                'kode' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 14:56:14',
                'updated_at' => NULL,
                'nama' => 'PEST CONTROL',
                'kode' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'deleted_at' => NULL,
                'created_at' => '2017-11-27 14:56:14',
                'updated_at' => NULL,
                'nama' => 'ATK & FORM',
                'kode' => NULL,
            ),
        ));
        
        
    }
}