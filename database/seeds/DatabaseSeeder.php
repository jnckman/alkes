<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CmsMenusPrivilegesTableSeeder::class);
        $this->call(CmsPrivilegesTableSeeder::class);
        $this->call(CmsPrivilegesRolesTableSeeder::class);
        $this->call(CmsMenusTableSeeder::class);
        $this->call(CmsModulsTableSeeder::class);
        // $this->call(CmsUsersTableSeeder::class);
        // $this->call(CmsEmailTemplatesTableSeeder::class);
        // $this->call(CmsSettingsTableSeeder::class);
        // $this->call(MigrationsTableSeeder::class);
        /*$this->call(AlamatsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(BarangGudangsTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(GudangsTableSeeder::class);
        $this->call(IsiMrSubdeptsTableSeeder::class);
        $this->call(PeriodeMrsTableSeeder::class);
        $this->call(MrSubdeptsTableSeeder::class);
        $this->call(KategorisTableSeeder::class);
        $this->call(LockRekapsTableSeeder::class);
        $this->call(MasterProjectsTableSeeder::class);
        $this->call(MrDepartmentsTableSeeder::class);
        $this->call(PricelistsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(SatuansTableSeeder::class);
        $this->call(SplitLogsTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(WellhosDepartmentsTableSeeder::class);
        $this->call(WellhosSubdeptsTableSeeder::class);
        $this->call(BarangsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(GroupbysTableSeeder::class);
        $this->call(AssignUsersTableSeeder::class);
        $this->call(RetursoTableSeeder::class);
        $this->call(IsiRetursoTableSeeder::class);
        $this->call(SalesOrdersTableSeeder::class);
        $this->call(IsiSalesOrdersTableSeeder::class);
        $this->call(HargaJualsTableSeeder::class);
        $this->call(PengirimanBarangsTableSeeder::class);
        $this->call(DetailPengirimanBarangsTableSeeder::class);
        $this->call(PengambilanBarangsTableSeeder::class);
        $this->call(DetailPengambilanBarangsTableSeeder::class);*/
    }
}
