@extends('crudbooster::admin_template')
@section('header22')
   <i class="fa fa-bar-chart" aria-hidden="true"></i> Laporan Pengeluaran Barang
@endsection
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Pengeluaran Barang</li>
@endsection
@section('content')
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<form class="form-inline" id="form1">
						<label for="select2periode">Periode :</label> 
						<select class="form-control" id='select2periode'></select>
					<button type="submit" class="btn btn-sm btn-success">Submit</button>
				</form>
				<div class="box-tools pull-right">
                <div class="btn-group">
                  <!-- <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button> -->
                  <!-- <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button> -->
                </div>
	            </div>
			</div>
			<div class="box-body table-responsive">
	            <table id='table1' class="table table-bordered table-striped table-hover">
	               <thead>
	                  <tr>
	                  	<th></th>
	                     <th>Nama</th>
	                     <th>Jumlah</th>
	                     <th>Action</th>
	                  </tr>
	               </thead>
	               <tfoot>
	                  <tr>
	                  	<th></th>
	                     <th>Nama</th>
	                     <th>Jumlah</th>
	                     <th>Action</th>
	                  </tr>
	               </tfoot>
	            </table>
	        </div>
	        <div class="box-footer">
	        	<button type="button" class="btn btn-sm btn-primary" id="btnback" style="display: none">Back</button>
	        </div>
		</div>
	</div>
</div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	var periodes = {!!$periodes!!};
	var periode = {{$pnow}};
	// console.log(periode);
	var dept_id;
	$("#select2periode").select2({
		data: periodes,
	});
	$('#select2periode').val({{$pnow}}).trigger('change');  
	var pilihan = 'dept';
	var table = $('#table1').DataTable({
		fixedHeader: {
            header: true,
            footer: true
        },
	    'processing': true,
	    'serverside':true,
	    "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
            }
        ],
        "order": [],
        "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
        'ajax' : {
        		url : '{{route("laporan.pengeluaranbarangperproject")}}',
        		data:{
        			'periode' : periode,
        			'pilihan' : 'dept'
        			}
        		}
	});

	nomorurut();

	function nomorurut(){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   	}

   	$(document).on('submit','#form1',function(event){
		event.preventDefault();
		periode = $('#select2periode').val();
		$.ajax({
			url: '{{route("laporan.pengeluaranbarangperproject")}}',
			data:{
				'periode': periode,
				'pilihan' : pilihan
			}
		}).done(function(response){
			if(response.data.length==0) {
        		table.clear().draw();
        	}
        	else {
        		table.clear().draw();
				table.rows.add(response.data).draw();
        	}
		}).fail(function(){
			console.log('Terjadi Kesalahan')
		});
      // alert(periode);
      // inisialisasi();
	});

	$(document).on('click','.btndept',function(){
		pilihan = 'subdept';
		dept_id = table.row($(this).parent().parent()).data()[4];
		$.ajax({
			url: '{{route("laporan.pengeluaranbarangperproject")}}',
			data:{
				'periode': periode,
				'pilihan' : pilihan,
				'dept_id' : dept_id
			}
		}).done(function(response){
			$('#btnback').css("display", "block");
			if(response.data.length==0) {
        		table.clear().draw();
        	}
        	else {
        		table.clear().draw();
				table.rows.add(response.data).draw();
        	}
		}).fail(function(){
			console.log('Terjadi Kesalahan')
		});
		// table.row(buttonku.parent().parent()).data().action2 = '';
	});

	$(document).on('click','#btnback',function(){
		if(pilihan=='subdept'){
			pilihan = 'dept';
			$.ajax({
				url: '{{route("laporan.pengeluaranbarangperproject")}}',
				data:{
					'periode': periode,
					'pilihan' : pilihan,
					'back' : 'y',
				}
			}).done(function(response){
				$('#btnback').css("display","none");
				if(response.data.length==0) {
	        		table.clear().draw();
	        	}
	        	else {
	        		table.clear().draw();
					table.rows.add(response.data).draw();
	        	}
			}).fail(function(){
				console.log('Terjadi Kesalahan')
			});
		}
		else if(pilihan=='mr'){//mr
			pilihan='subdept';
			$.ajax({
				url: '{{route("laporan.pengeluaranbarangperproject")}}',
				data:{
					'periode': periode,
					'pilihan' : pilihan,
					'dept_id' : dept_id,
				}
			}).done(function(response){
				// $('#btnback').css("display","none");
				if(response.data.length==0) {
	        		table.clear().draw();
	        	}
	        	else {
	        		table.clear().draw();
					table.rows.add(response.data).draw();
	        	}
			}).fail(function(){
				console.log('Terjadi Kesalahan')
			});
		}
	});

	$(document).on('click','.btnsubdept',function(){
		pilihan = 'mr';
		subdept_id = table.row($(this).parent().parent()).data()[4];
		$.ajax({
			url: '{{route("laporan.pengeluaranbarangperproject")}}',
			data:{
				'periode': periode,
				'pilihan' : pilihan,
				'subdept_id' : subdept_id
			}
		}).done(function(response){
			$('#btnback').css("display", "block");
			if(response.data.length==0) {
        		table.clear().draw();
        	}
        	else {
        		table.clear().draw();
				table.rows.add(response.data).draw();
        	}
		}).fail(function(){
			console.log('Terjadi Kesalahan')
		});
		// table.row(buttonku.parent().parent()).data().action2 = '';
	});
});
</script>
@endpush