<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $data['page_title'] }}</title>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/datatables.min.css') }}">
    <link href="{{ asset('customs/css/pace-minimal-blue.css')}}" rel="stylesheet" />
    <style type="text/css">
        .nav>li>a:after {
          content: '';
          border-top: 1px solid #EBEBEB;
          position: absolute;
          bottom: 0; left: 30px; right: 15px; top: 0px;
        }
        .productinfo img{
            margin-top: 15px;
        }
        .productinfo h4{
            color: #FE980F;
        }
        .selected{
            border-bottom: 2px solid #7c7b7b;
        }
        .pagination li a,.pagination li span{
            border: 1px solid #a8a8a8;
        }
        .features_items{
            /*border-left: 1px solid #e3e3e3;
            border-right: 1px solid #e3e3e3;*/
        }
        #actionku{
            padding: 10px;
            border:1px solid #E6E4DF;
        }
        .multiline-strike strike {
          text-decoration: none;
          background-image: -webkit-linear-gradient(transparent 7px,#FE980F 7px,#FE980F 9px,transparent 9px);
          background-image:    -moz-linear-gradient(transparent 7px,#FE980F 7px,#FE980F 9px,transparent 9px);
          background-image:     -ms-linear-gradient(transparent 7px,#FE980F 7px,#FE980F 9px,transparent 9px);
          background-image:      -o-linear-gradient(transparent 7px,#FE980F 7px,#FE980F 9px,transparent 9px);
          background-image:         linear-gradient(transparent 7px,#FE980F 7px,#FE980F 9px,transparent 9px);
        }
        .multiline-strike h5{
            color: #FE980F;
        }
    </style>
    @stack('head')
</head>
<body style="padding-top: 80px;">
    @include('customView.partial.shop_navbar')
    <div class="container">
        <div class="container-fluid">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
        </div>
    </div>
    <div class="wrapper">
            @yield('content')
    </div>
    <br>
    <br>
    <br>
    <br>
<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
</footer><!--/Footer-->  
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script src="{{asset('mronline/js/jquery.js')}}"></script>
<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('mronline/js/price-range.js')}}"></script>
<script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('mronline/js/main.js')}}"></script>
<script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('js/datatable.min.js') }}"></script>
<script type="text/javascript">
$(function() {
    $('#formSearchBarang').submit(function(e){
        data = $('#formSearchBarang input[name="kunci"]').val();
        if(data=='')e.preventDefault();
    });

    var maprivilege = <?php echo $data['privilege'];?>;
    // console.log(maprivilege);

    $(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

    $(document).on("click",".addCart",function(e){
        id = $(this).attr('barang_id');
        satuan = $(this).attr('satuan');
        namabarang = $(this).parent().parent().find('.namabarang').html();
        harga_jual = $(this).attr('harga_jual');
        swalAddCart(id,namabarang,satuan,harga_jual);
    });

    $(document).on('click','.detailBarang',function(event){
        url = "{{route('shop.getDetailBarang')}}";
        id = $(this).attr('barang_id');
        $.ajax({
            type:"GET",
            url: url,
            data: {
                "id": id
            }
        }).done(function(response){
            // console.log(response);
            $('#modalDetailBarang').find('.modal-header').html('<h5>Kode : '+response.kode+'</h5>');
            modal_body =$ ('#modalDetailBarang').find('.modal-body');
            modal_body.html(
            '<div class="form-group">'+
                '<label>Nama : </label>'+
                '<p>'+response.nama+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Kategori : </label>'+
                '<p>'+response.kategori+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Satuan : </label>'+
                '<p>'+response.satuan+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Jenis : </label>'+
                '<p>'+response.jenis+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Ukuran : </label>'+
                '<p>'+response.ukuran+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Cara pakai : </label>'+
                '<p>'+response.cara_pakai+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Spesifikasi : </label>'+
                '<p>'+response.spesifikasi+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Link Video : </label>'+
                '<p><a href="'+response.video+'">'+response.video+'</a></p>'+
            '</div>');
            $('#modalDetailBarang').modal('show');
        }).fail(function(){
            swal('Oops','Gagal Mengambil Data','error').catch(swal.noop);
        });
    });

    function swalAddCart(id,namabarang='',satuan='',harga=0,update=0){
        harga_jual = harga;
        if((maprivilege[0].toLowerCase()=='so' && maprivilege[1]=='1')|| maprivilege[0].toLowerCase()=='super administrator'){
            // console.log('ok');
            var steps = [
            {
              type: 'info',
              html: 'Masukkan jumlah <b>('+satuan+')</b> '+namabarang+' :', 
              input: 'text',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Next &rarr;',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                  if (parseInt(value)>0) {
                    resolve()
                  } else {
                    reject('Input harus angka dan > 0 !')
                  }
                })
              }   
            },
            {
              title: 'Masukkan Harga Baru <b>(opsional)</b> - '+namabarang+' :',
              input: 'number',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Submit',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if(value){
                        if (value<0) {
                            reject('Input harus > 0 !')
                        }
                    }
                    resolve()
                })
              }   
            }
            ]
            swalku = swal.queue(steps);
        }
        else{
            swalku = swal({
              type: 'info',
              html: 'Masukkan jumlah <b>('+satuan+')</b> '+namabarang+' :', 
              input: 'text',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Submit',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                  if (parseInt(value)>0) {
                    resolve()
                  } else {
                    reject('Input harus angka dan > 0 !')
                  }
                })
              }
            });
        }

        swalku.then(function(gg){ 
            if(gg[1]){
                var hargaBaru = gg[1];
                var qnty = gg[0];
            }else {
                // console.log(gg);
                var hargaBaru = -1;
                var qnty = gg[0];
            }
            url = "{{ route('shop.addCart') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                    "qnty" : qnty,
                    "update" : update,
                    "hargaBaru" : hargaBaru,
                    'harga_jual' : harga_jual,
                },
            }).done(function(response){
                // console.log(response);
                if(response.berhasil==1){
                    swal({
                        type: 'success',
                        html: namabarang+' berhasil ditambahkan',
                        confirmButtonColor: '#FE980F',
                    });
                    $('#jumlah_chart').html(response.angkaCart);
                }
                else if(response.berhasil==2) {
                    swal({
                        type: 'success',
                        html: 'Jumlah berhasil diedit',
                        confirmButtonColor: '#FE980F',
                    });
                    suntingUpdate.html(response.jmlh);
                }
                else
                swal('Oops...', 'Terjadi Kesalahan !', 'error');    
            }).fail(function(){
                swal('Oops...', 'Terjadi Kesalahan !', 'error');
            });
        }).catch(swal.noop);
    };
});
</script>
@stack('bottom')
</body>
</html>