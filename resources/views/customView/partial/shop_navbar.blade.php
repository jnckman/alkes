<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
	<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{route('getShop')}}"><img src="{{asset('gambar/logo.png')}}" style="height: 30px"></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <div class="col-sm-4 col-md-6">
			<form class="navbar-form" role="search" id="formSearchBarang" action="{{route('shop.cariBarang')}}">
			    <div class="input-group">
			        <input type="text" class="form-control" placeholder="Cari Barang" name="kunci">
				        <div class="input-group-btn">
				            <button class="btn btn-warning" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				        </div>
			    </div>
		    </form>
	    </div>

	  @if(strtolower(CRUDBooster::myPrivilegeName())=='super administrator' )
      <ul class="nav navbar-nav navbar-right">
      	<li>
	    	<a href="{{route('getShop')}}" class="text-center">
	    		<span class="glyphicon glyphicon-home"></span>
	    		Home
	    	</a>
	    </li>
      	<li> 
	      <a href="{{route('shop.getCart')}}" class="text-center">
	        <span class="glyphicon glyphicon-shopping-cart"></span> 
	        <span id="jumlah_chart" style="font-size: 12px;position:relative;bottom: 8px;right:2px">{{count(session()->get('cartClient')->items)}}</span>Cart
	      </a> 
	    </li>
	    @if(empty(CRUDBooster::myId()))
	    <li><a href="{{route('getLogin')}}" class="text-center"><span class="glyphicon glyphicon-log-in"></span> Log in</a></li>
        @else
        <li>
	    	<a href="{{route('shop.getProfil')}}" class="text-center">
	    		<span class="glyphicon glyphicon-user"></span>
	    		Profil
	    	</a>
	    </li>
        <li><a href="{{route('getLogout')}}" class="text-center"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
        @endif
      </ul>
      @endif
    </div>
</div>
</nav>