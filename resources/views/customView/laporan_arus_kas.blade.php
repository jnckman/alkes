@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Arus Kas
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Arus Kas</li>
@endsection

@section('content')
<div class="box box-default">
   <div class="box-header with-border">
      Periode : <span id="judulPeriode">{{$periode}}</span>
      <button type="button" class="btn btn-default pull-right btn-md" data-target="#modalFilterPeriode" data-toggle="modal"><i class="fa fa-gear"></i> Filter</button>
   </div>
   <div class="box-body">
      <h4>Arus Kas Masuk</h4>
      <div id="tampil_masuk">
         @foreach($masuk as $k=>$v)
         @if(strtolower($k)=='total')
            <div class="row">
               <div class="col-sm-4">
                  <b>{{$k}} : </b>
               </div> 
               <div class="col-sm-8">
                  <b>Rp {{number_format($v)}}</b>
               </div> 
            </div>
         @else
            <div class="row">
               <div class="col-sm-4 ">
                  {{$k}} : 
               </div> 
               <div class="col-sm-8">
                  Rp {{number_format($v)}}
               </div> 
            </div>
         @endif
         @endforeach
      </div>
      <h4>Arus Kas Keluar</h4>
      <div id="tampil_keluar">
         @foreach($keluar as $k=>$v)
         @if(strtolower($k)=='total')
            <div class="row">
               <div class="col-sm-4">
                  <b>{{$k}} : </b>
               </div> 
               <div class="col-sm-8">
                  <b>Rp {{number_format($v)}}</b>
               </div> 
            </div>
         @else
            <div class="row">
               <div class="col-sm-4 ">
                  {{$k}} : 
               </div> 
               <div class="col-sm-8">
                  Rp {{number_format($v)}}
               </div> 
            </div>
         @endif
         @endforeach
      </div>
      <h4><div class="col-sm-4 ">Arus Kas Bersih : </div><div class="col-sm-8"><span id='arus_bersih'>Rp {{number_format($arus_bersih)}}</span></div></h4>
   </div>
</div>

<div class="modal fade" id="modalFilterPeriode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form id="formFilter">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Filter Berdasarkan :</h4>
         </div>
         <div class="modal-body" style="padding: 20px">
               <div class="form-group">
                  <label>Filter Berdasarkan</label>
                  <select id="select2tahunbulan" name="opsi" style="width:100%">
                     <option></option>
                     <option value="thn">Tahun</option>
                     <option value="bln">Bulan</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Start</label>
                  <input type="text" name="tglmulai" id="inputMulai" class="form-control" readonly="true" placeholder="Tanggal Mulai" />
               </div>
               <div class="form-group">
                  <label>End</label>
                  <input type="text" name="tglselesai" id="inputSelesai" class="form-control" readonly="true" placeholder="Tanggal Selesai" />
               </div>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">
$(function(){
   var opsi;
   $('#select2tahunbulan').select2({
      placeholder : 'Pilih Tahun/Bulan'
   }).on('select2:select',function(){
      $('input[name="tglmulai"]').val('').attr('readonly',false);
      $('input[name="tglselesai"]').val('').attr('readonly',false);
      // console.log($(this).val());
      ubahDatePicker($(this).val());
   });

   function ubahDatePicker(option){
      $('#inputMulai').datepicker('remove');
      $('#inputSelesai').datepicker('remove');
      opsi = option;
      if(option=='bln'){
         $('input[name="tglmulai"],input[name="tglselesai"]').datepicker( {
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months"
         });
      }
      if(option=='thn'){
         $('input[name="tglmulai"],input[name="tglselesai"]').datepicker( {
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
         });
      }
   }

   $('#formFilter').submit(function(event){
      event.preventDefault();
      url = '{{route("laporan.getaruskas")}}';
      start = $('input[name="tglmulai"]').val();
      end = $('input[name="tglselesai"').val();
      $.ajax({
         url: url,
         type:'get',
         data:$('#formFilter').serialize(),
      }).done(function(response){
         console.log(response);
         $('#tampil_masuk').html('');
         $('#tampil_keluar').html('');
         for(var k in response) {
            // console.log(k, response[k]);
            for(var j in response[k]) {
               if(j.toLowerCase()=='total'){
                  $('#tampil_'+k).append('<b><div class="row"><div class="col-sm-4"> '+j+' : </div>'+
                  '<div class="col-sm-8">Rp ' + response[k][j].toLocaleString() + '</div></div></b> ');
               }
               else{
                  $('#tampil_'+k).append('<div class="row"><div class="col-sm-4"> '+j+' : </div>'+
                     '<div class="col-sm-8">Rp ' + response[k][j].toLocaleString() + '</div></div> ');
               }
            }
         }
         if(start == end){
            if(opsi == 'bln'){
               start = moment(start, "YYYY-MM").format("MMMM-YYYY");
            }
            $('#judulPeriode').html(start);
         }
         else{
            if(opsi == 'bln'){
               start = moment(start, "YYYY-MM").format("MMMM-YYYY");
               end = moment(end, "YYYY-MM").format("MMMM-YYYY");
            }
            $('#judulPeriode').html(start+' hingga '+end);  
         }
         $('#arus_bersih').html(response.arus_bersih.toLocaleString());
         $('#modalFilterPeriode').modal('hide');
      }).fail(function(){
         swal('Ops','Terjadi Kesalahan','error');
      });
   });
})
</script>
@endpush
