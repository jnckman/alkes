@extends('crudbooster::admin_template')
@section('content')
  <p><a href="http://localhost/wellhos/public/admin/returso"><i class="fa fa-chevron-circle-left"></i> &nbsp; Back To List Data submodul</a></p>
  <div class="box box-default">
    <div class="box-body table-responsive no-padding">
      <table class="table table-bordered">
        <tbody>
          <tr class="active">
            <td colspan="2"><strong><i class="fa fa-bars"></i> Submodul</strong></td>
          </tr>
          <tr>
            <td width="25%"><strong>
            Nomor
            </strong></td><td>{{$returso->nomor}}</td>
          </tr>
          <tr>
            <td width="25%"><strong>
            Status
            </strong></td><td>{{$returso->status}}</td>
          </tr>
          <tr>
            <td width="25%"><strong>
             Keterangan
             </strong></td><td>{!! nl2br(e($returso->keterangan)) !!}</td>
          </tr>
                      
        </tbody>
      </table>    
    </div>
  </div>

  <div class="box">
    <div class="box-body">
      <form id="frm-example" action="" method="post">
        {{csrf_field()}}
      @if(strtolower($returso->status)=='new')
      <button type="submit" class="btn btn-success pull-right">Approve</button>
      @endif
      <div class="clearfix"></div>
      <br/>
      <div class="table-responsive">
      <table class="table table-striped" id='example'>
        <thead>
          <th>Barang</th>
          <th>Jumlah</th>
          <th>Status</th>
          <th>Keterangan</th>
          <th>
            @if(strtolower($returso->status)=='new')
            <input type="checkbox" name="select_all" value="1" class="example-select-all">
            @endif
          </th>
        </thead>

        <tfoot>
          <th>Barang</th>
          <th>Jumlah</th>
          <th>Status</th>
          <th>Keterangan</th>
          <th>
            @if(strtolower($returso->status)=='new')
            <input type="checkbox" name="select_all" value="1" class="example-select-all">
            @endif
          </th>
        </tfoot>
      </table>
      </div>
      @if(strtolower($returso->status)=='new')
      <button type="submit" class="btn btn-success pull-right">Approve</button>
      @endif
      </form>
    </div>
  </div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function(){
  var jumlahdata = '{{$jumlahdata}}';
  var table = $('#example').DataTable({
      'ajax': {
         'url': '{{url()->current()}}'+'/dt_isi'
      },
      fixedHeader: {
            header: true,
            footer: true
        },
      'columnDefs': [{
         'targets': 4,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
            if(data!='0')
             return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"/>';
            else
              return '';
         }
        }],
      'processing': true,
      'serverside':true,
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('.example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('.example-select-all').prop('checked', this.checked);
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;
      var konter = 0;
      e.preventDefault();
      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!(this.checked)){
          konter++;
          // console.log(jumlahdata);
         }
         if(this.checked){
          $('#frm-example').append('<input type="hidden" name="stok[]" value="'+ $(this).parent().parent().find('td:eq(1)').html()+'" />')
         }
      });
      if(konter==jumlahdata){
        swal('Oops','Pilih Data untuk di-Approve','error');
      }
      else this.submit();

   });

});
</script>
@endpush