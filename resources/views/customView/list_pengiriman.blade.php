@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> List Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">List Pengiriman</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id='table_master'>
                  <thead>
                     <tr class="success">
                        <th>Kode Pengiriman</th>
                        <th>Dibuat Oleh</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th>Jenis</th>
                        <th>Tujuan</th>
                        <th>Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')

<script type="text/javascript">
   $(function () {
      dt_master();
   });

   function dt_master() {
      $('#table_master').DataTable({
   		processing: true,
   		serverSide: true,
   		ajax: {
            url : "{{ route('dt.master.list.pengiriman') }}",
            data : {
               'tgl' : '{{$tgl}}'
            }
         },
   		columns: [
   			{data: 'kode'},
   			{data: 'name'},
            {data: 'tgl_kirim'},
            {data: 'status'},
            {data: 'jenis'},
            {data: 'tujuan'},
			   {data: 'action', orderable: false, searchable: false}
   		],
   		lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
	   });
   }
</script>
@endpush
