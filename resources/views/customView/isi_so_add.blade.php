@extends('crudbooster::admin_template')
@section('content')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
<p><a title="Return" href="{{request()->return_url}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Isi Sales Orders</a></p>
<div class="panel panel-default">
    <div class="panel-heading">
     <strong><i class="fa fa-glass"></i> Add Isi Sales Orders</strong>
    </div>
    <div class="panel-body" style="padding:20px 0px 0px 0px">
    	@if($type == 'edit')
    	<form class="form-horizontal" method="post" id="form" enctype="multipart/form-data" action="{{crudbooster::mainpath()}}/edit-save/{{$edit->id}}">
    	<!-- <form class="form-horizontal" method="post" id="form" enctype="multipart/form-data" action="http://localhost/wellhos/public/admin/isi_sales_orders/edit-save/186"> -->
    	@else
    	<form class="form-horizontal" method="post" id="form" enctype="multipart/form-data" action="http://localhost/wellhos/public/admin/isi_sales_orders/add-save">
    	@endif
   		{{csrf_field()}}
   		<div class="box-body" id="parent-form-area">
        	<div class="form-group header-group-0">
				<label class="control-label col-sm-2">Barang <span class="text-danger" title="This field is required">*</span></label>
				<div class="col-sm-10">								
					<select style="width:100%" class="form-control" id="barangs_id" required="" name="barangs_id" tabindex="-1" aria-hidden="true">
						<option value="">** Please select a Barang</option>
					</select>
				<p class="help-block"></p>
				</div>
			</div>
			<div class="form-group header-group-0">
				<label class="control-label col-sm-2">Jumlah <span class="text-danger" title="This field is required">*</span></label>
				<div class="col-sm-10">								
					<input style="width:100%" type="number" class="form-control" id="jumlah" required="" name="jumlah" tabindex="-1" aria-hidden="true" value="{{$edit->jumlah}}"/>
				<p class="help-block"></p>
				</div>
			</div>
			<div class="form-group header-group-0">
				<label class="control-label col-sm-2">Diskon <span class="text-danger" title="This field is required">*</span></label>
				<div class="col-sm-10">								
					<input style="width:100%" type="text" class="form-control" id="diskon2" required="" tabindex="-1" aria-hidden="true" value="{{$edit->diskon}}" />
					<input type="hidden" name="diskon" id="diskon" value="{{$edit->diskon}}">
					<input type="hidden" name="returnku" value="{{request()->return_url}}"/>
					<input type="hidden" name="sales_orders_id" value="{{$so_id}}"/>
				<p class="help-block"></p>
				</div>
			</div>
			<div class="form-group header-group-0">
				<label class="control-label col-sm-2">Total <span class="text-danger" title="This field is required">*</span></label>
				<div class="col-sm-10">								
					<input style="width:100%" type="number" readonly="" class="form-control" id="total" required="" name="total" tabindex="-1" aria-hidden="true" value="{{$edit->total}}" />
				<p class="help-block"></p>
				</div>
			</div>
		</div>
    </div>
    <div class="box-footer">
    	<div class="form-group">
			<label class="control-label col-sm-2"></label>
			<div class="col-sm-10">
			<a href="{{request()->return_url}}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Back</a>
			<input name="submit" value="Save" class="btn btn-success" type="submit">
			</div>
		</div>
    </div>
	</form>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	var barangs = <?php echo $barangs;?>;
	var barang_edit = 0;
	var harga=0;
	<?php 
	if(isset($edit->barangs_id)){
		?> barang_edit =  <?php echo $edit->barangs_id;
	} ?>;
	var select2barangs_id = $('#barangs_id').select2({
		data:barangs
	});

	$(document).on('change','#barangs_id',function(){
		if($(this).val()!=''){
			text = $('#barangs_id option:selected').text();
			gg = text.split("|");
			if(gg[1]>=0){
				harga = gg[1];
			}
			else{
				harga = 0;
			}
			$('#jumlah').attr('readonly',false);
			hitungtotal();
		}else{
			$('#total').val('');
			$('#jumlah').attr('readonly',true);
		}
	});
	var total = $('#total').val();
	if(barang_edit!=0){
		$('#barangs_id').val(barang_edit).trigger('change');
	}
	$(document).on("input","#diskon2", function(e){
        hitungtotal();
    });

	$(document).on('input','#jumlah',function(){
		hitungtotal();
	});

	function hitungtotal(){
		total = harga * $('#jumlah').val();
		var tampung = $('#diskon2').val();
		// console.log(tampung);
        var diskonsku= 0;
        if(tampung.indexOf('%') >= 0){
          tampungconvert = tampung.substr(0, tampung.indexOf('%')); 
          diskonsku = tampungconvert * total/100
          total2 = total -  diskonsku;
        }
        else{
          diskonsku = tampung;
          total2 = total - diskonsku;
        }
        $('#diskon').val(diskonsku);
		$('#total').val(total2);
	}

});
</script>
@endpush