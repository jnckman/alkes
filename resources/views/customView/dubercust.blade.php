@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Arus Kas
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Customer Dua Berlian</li>
@endsection

@section('content')
<div class="box box-default">
   <h5>Cust File</h5>
   <div class="box-header with-border">
   <form action="dubercustpost" method='POST'>
      <input type="date" name="date" class="form-control">
      {{csrf_field()}}
      <button type="submit" class="btn btn-info" style="margin-top: 20px;width: 100%;">Lihat</button>
   </form>
   </div>
   
</div>

<div class="box box-default">
   <h5>IMITF</h5>
   <div class="box-header with-border">
   <form action="imitmfpost" method='POST'>
      <input type="date" name="date" class="form-control">
      {{csrf_field()}}
      <button type="submit" class="btn btn-info" style="margin-top: 20px;width: 100%;">Lihat</button>
   </form>
   </div>
   
</div>

<div class="box box-default">
   <h5>RAW</h5>
   <div class="box-header with-border">
   <form action="rawpost" method='POST'>
      <input type="date" name="date" class="form-control">
      {{csrf_field()}}
      <button type="submit" class="btn btn-info" style="margin-top: 20px;width: 100%;">Lihat</button>
   </form>
   </div>
   
</div>

<!-- <div class="box box-default">
<h5>CUST DETAIL</h5>
   <div class="box-header with-border">
   <form action="custpost" method='POST'>
      <input type="date" name="date" class="form-control">
      {{csrf_field()}}
      <button type="submit" class="btn btn-info" style="margin-top: 20px;width: 100%;">Lihat</button>
   </form>
   </div>
   
</div> -->
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">

@endpush
