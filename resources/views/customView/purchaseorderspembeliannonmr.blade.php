@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endpush

@section('header22')
   <i class="fa fa-cart-plus" aria-hidden="true"></i> Susun Purchase Order MR
@endsection

@section('breadcrumb')
   
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        
        <div class="col-md-2 ">
          <button class="btn btn-info buatform">Buat Purchase Orders Pembelian NON MR</button>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                    <th>id</th>
                    <th>tipe po</th>
                     <th>nama supplier</th>
                     <th>tgl po</th>
                     <th>no po</th>

                     <th>total</th>
                     <th>tempo</th>
                     <th>status</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" style="height: 200px;">

                 <div class="col-md-12" style='margin-top: 10px;'>
                   <div class="col-md-12 form-group" style="padding-left: 0px;">
                     <label>No PO</label>
                     <input type="text" name="nopo" class="form-control" value="PO/B/">
                   </div>
                   <div class="col-md-12 totalpl">

                   </div>
                 </div>
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>

                            <th>
                                Nama Barang
                            </th>
                            <th>
                               Jumlah
                            </th>
                            <th>
                              Harga satuan
                            </th>
                            <th>
                              Total
                            </th>
                          </thead>
                          <tbody class='tabelisibarang'>

                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   <div class="modal fade" id="buatpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <form action='purchaseorderspembelian/submitpopembelian' method='POST'>
             <div class="modal-body" >

                 <div class="col-md-12" style='margin-top: 10px;''>
                   <!-- <div class="col-md-4" style="padding-left: 0px;">
                     FORM PO
                   </div>
                   <div class="col-md-8 totalpl">

                   </div> -->
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>No PO</label>
                     <input type="text" readonly="" name="nopo" class="form-control" value="PO/{{$last}}/{{date('Y-m-d')}}/" id='nopoform'>
                   </div>
                 </div>
                 
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Tempo (Hari)</label>
                     <input type="text" name="hari" class="form-control">
                   </div>
                 </div>

                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Ongkir</label>
                     <input type="text" name="ongkir" class="form-control">
                   </div>
                 </div>
                 
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>Jenis PO</label>
                     <select class="form-control">
                       <option value="0" class="form-control">Tempo</option>
                       <option value="1" class="form-control">Tunai</option>
                     </select>
                   </div>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>Tipe PO</label>
                     <select name='tipepo' class="form-control input-sm">
                       <option value="NON_MR" selected>Non MR</option>
                     </select>


                   </div>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>PPN(%)</label>
                     <input type="text" name="ppn" class="form-control">
                   </div>
                 </div>
                 <div class="col-md-12" style="margin-bottom: 30px;">
                   <div class="form-group">
                     <label>Kepada</label>
                     <select class="form-control select2-hidden-accessible" id="select2suppliers" required name="supplierid">
                     <option></option>
                     </select>
                   </div>
                 </div>
                 
                 
                 
                 <div class="col-md-12" >
                  {{csrf_field()}}
                   <button type='submit' class="btn btn-success">SUBMIT PO</button>

                 </div>
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">

                   </div>
                 </div> -->

             </div>
             </form>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
    </div>
@endsection

@push('bottom')

<script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {

          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': {
                url : 'purchaseorderspembeliandtajax',
                data : {
                  'mr' : '2'
                }
              }

          } );
          
          $.fn.modal.Constructor.prototype.enforceFocus = function() {};
          var suppliers = <?php echo $supplier; ?>;
          console.log(suppliers);
          $('#select2suppliers').select2({
             data:suppliers,
             placeholder:'Pilih Supplier'
          });
          $('body').on('click', '.buatform', function(event) {
            event.preventDefault();
            $.ajax({

                          type: 'get',
                          url: 'pogetlastid',
                          data: {},
                          dataType: 'json',
                          success: function (data) {
                            
                            $('#nopoform').val($('#nopoform').val() + data['id']);
                            $('#buatpo').modal('show');

                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
          });

          


      } );
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<style type="text/css">
 .selected{
  border-bottom: 2px solid grey;
 }
 .select2{
  width: 100% !important;
 }
</style>
@endpush
