@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Barang Keluar
@endsection

@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Barang Keluar</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <form id='formfilterlaporanpo'>
            {{csrf_field()}}
         <div class="box-header">
            <h3 class="box-title">Filter Berdasarkan :</h3>
         </div>
         <div class="box-body">
            <div class="col-sm-4">
               <select id='select2periode' name="periode"  class="form-control"></select>
            </div>
            <div class="col-sm-4">
               <select id="select2jenis" name="jenis" required class="form-control">
                  <option></option>
                  <option value="0">Semua</option>
                  <option value="1">Tempo</option>
                  <option value="2">Tunai</option>
               </select>
            </div>
            <div class="col-sm-4">
               <select id="select2tipe" name="tipepo" required class="form-control">
                  <option></option>
                  <option value="0">Semua</option>
                  <option value="1">MR</option>
                  <option value="2">Pembelian</option>
               </select>
            </div>
         </div>
         <div class="box-footer">
            <button type="submit" class="btn btn-info">Submit</button>
         </div>
         </form>
      </div>
   </div>
   <div class="row" >
      <div class="box box-info" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body no-padding table-responsive">
            <table id='table1' class="table table-bordered table-striped table-hover">
               <thead>
                  <tr>
                     <th></th>
                     <th>Kode</th>
                     <th>Kode PO/<br>Nama</th>
                     <th>Kode Supplier/<br>Unit</th>
                     <th>Supplier/<br>Cost/Unit</th>
                     <th>QTY</th>
                     <th>DPP</th>
                     <th>Disc</th>
                     <th>Vat in (PPN)</th>
                     <th>Total/<br>Sub Total</th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
               </thead>
               <tfoot>
                  <tr>
                     <th></th> 
                     <th>Kode</th>
                     <th>Nama</th>
                     <th>Unit</th>
                     <th>Cost/Unit</th>
                     <th>QTY</th>
                     <th>DPP</th>
                     <th>Disc</th>
                     <th>Vat in (PPN)</th>
                     <th>Total</th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
               </tfoot>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
   $('#select2periode').select2({
      placeholder:'Pilih periode',
      ajax: {
         url: '{{route("laporan.getperiode")}}',
         processResults: function (data) {
            return {
              results: data
            };
         }
      },
   });
   $('#select2jenis').select2({
      placeholder:'Pilih jenis PO',
   });
   $('#select2tipe').select2({
      placeholder:'Pilih PO MR/Pembelian',
   });
   var table = $('#table1').DataTable({
      fixedHeader: {
            header: true,
            footer: true
        },
      'processing': true,
      'serverside':true,
      "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
            },
            {
               "targets": [ 10,11,12,13,14 ],
               "visible": false,
               // "searchable": false
            },
        ],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      // "order": [[ 1, 'asc' ]]
      // "scrollX": true
      "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var groupid = -1;
 
            api.column(11, {page:'current'} ).data().each( function ( group, i ) {
               if ( last !== group ) {
                  groupid++;
                  // console.log($(this));
                  $(rows).eq( i ).before(
                     '<tr class="group groupso">'+
                        '<td colspan="2"></td>'+
                        '<td class="text-center"><b>'+group+'</b></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                     '</tr>'
                  );
 
                  last = group;
                }
            });

            $('tbody').find('.groupso').each(function (i,v) {
               rowku = table.row($(v).next());
               $(this).find('td:eq(0)').append(rowku.data()[10]);
               $(this).find('td:eq(2)').append(rowku.data()[12]);
               $(this).find('td:eq(3)').append(rowku.data()[13]);
               $(this).find('td:eq(8)').append(rowku.data()[14]);
            });
         },
   });

   function nomorurut(){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   }

   $('#formfilterlaporanpo').submit(function(event){
      event.preventDefault();
      $.ajax({
         url: '{{route("laporan.getpodt")}}',
         type : 'get',
         data: $(this).serialize(),
      }).done(function(response){
         data = response.data;
         // console.log(data);
         table.clear().draw();
         if(data.length>0){
            table.rows.add(data).draw();
            nomorurut();
         }
         else{
            swal('Oops','Data tidak ditemukan','info');
         }
      }).fail(function(){
         swal('Oops','Terjadi kesalahan','error');
      });
   });
});
</script>
@endpush
