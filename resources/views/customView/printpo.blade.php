<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
		@page { size: auto;  margin: 0mm; }
	</style>
</head>
<body>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<!-- <h3><b>PT. TATA KARYA GEMILANG</b></h3>
					<h5>Jl. Sukun MBS no 3 Condong Catur Yogyakarta</h5>
					<h5>P : 0274 88 99 81 e: purchasing@gemilang.co.id</h5>
					<h5>NPWP 02.398.317.4.542.000</h5> -->

				</div>
				<div class="col-md-12">
					<div class="col-md-8 col-md-offset-4" style="text-align: center;">
						<h3>Surat Pesanan</h3>
					</div>
					
				</div>
				<div class="col-md-12">
					<!-- <div class="col-md-2">
						<div class="row">
						<h3>Kode SP : </h3>
						</div>
						<div class="row">
							{{$po->suppliers_kode}}
						</div>
					</div> -->
					<div class="col-md-8" style="width: 70% !important;">
						<div class="row">
							
						</div>
						<div class="row">
							Kepada  : {{$po->suppliers_nama}}<br>{{$po->suppliers_alamat}}
						</div>
						<div class="row">
							
						</div>
					</div>
					<div class="col-md-8" >
						<div class="row">
							Tgl PO : {{date('d m Y',strtotime($po->po_tglpo))}}
						</div>
					</div>
					<div class="col-md-8" >
						<div class="row">
							Pesan Via : WA / Email / Phone / On Site
						</div>
					</div>
					
				</div>
				<div class="col-md-12">
					<table class="table-bordered table">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Satuan</th>
								<th>@</th>
								<th>QTY</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;?>
							@foreach($isipo as $key => $p)
							<tr>
								<td>{{$i}}</td>
								<td>{{$p->barang->kode}}</td>
								<td>{{$p->barang->nama}}</td>
								<td>{{$p->barang->satuan->nama}}</td>
								<td><p class="pull-left">Rp</p> <p class="pull-right"> {{number_format($p->harga)}}</p></td>
								<td><p class="text-right">{{$p->jumlah}}</p></td>
								<td><p class="pull-left">Rp</p> <p class="pull-right"> {{number_format($p->harga*$p->jumlah)}}</p></td>
								<?php $i++;?>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4"></td>
								<td colspan="2" class="text-center">SUB TOTAL</td>
								<th><p class="pull-left">Rp</p> <p class="pull-right">{{number_format($po->po_total-$p->po_diskon)}}</p> </th>
							</tr>
							<tr>
								<td colspan="4"></td>
								<td colspan="2" class="text-center">PPN</td>
								<th>
								<?php $ppn = 0;?>
								@if($po->po_ppn>0)
								<?php $ppn = $po->po_ppn * $po->po_total/100;?>
									<p class="pull-left">Rp </p><p class="pull-right"> 
										{{number_format( $ppn )}}
									</p>
								@else
									-
								@endif
								</th>
							</tr>
							<tr>
								<td colspan="4"></td>
								<td colspan="2" class="text-center">ONGKIR</td>
								<th><p class="pull-left">Rp </p><p class="pull-right"> 
									<?php $ongkir = 0;?>
									@if($po->po_ongkir>0)
									<?php $ongkir = $po->po_ongkir;?>
										{{ number_format( $po->po_ongkir ) }}
									@else
										-
									@endif
								</p>
								</th>
							</tr>
							<tr>
								<td colspan="4"></td>
								<th colspan="2" class="text-center">TOTAL</th>
								<th><p class="pull-left">Rp</p> <p class="pull-right">
									{{number_format($po->po_total+$ppn+$ongkir)}}
								</p></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="col-md-12">
					<table class="table-bordered table">
						<tr class="text-center">
							<td>Approve Acc</td>
							
						</tr>
						<tr class="text-center" style="height: 100px;">
							<td style="vertical-align:bottom">{{$approver->value}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
</body>

<script type="text/javascript">
$(function(){
	function printmz(){
		window.print();
		// window.location = "{{url('admin/notas')}}";
	}

	printmz();
});
</script>
</html>