@extends('crudbooster::admin_template')
@section('content')
@push('head')
<link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet" />
@endpush
<!-- <h2 class="text-center">Mix Items</h2> -->
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="stepwizard">
        <div class="stepwizard-row">
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',1) }}" type="button" class="btn btn-default btn-circle">1</a>
                <p>Resep</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',2) }}" type="button" class="btn btn-primary btn-circle">2</a>
                <p>Pilih Bahan</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',3) }}" type="button" class="btn btn-default btn-circle">3</a>
                <p>Finishing</p>
            </div>
        </div>
    </div>
  </div>
  <div class="panel-body">
    <form method='post' action="{{ route('reseps.aksi')}}">
    {{ csrf_field() }}
        <div class='form-group'>
          <label>Nama</label>
          <select type='text' name='barangs' required class='form-control select2barang'>
            <option></option>
          </select>
        </div>
        <div class='form-group'>
          <label>Sumber Barang</label>
          <select type='text' name='barang_gudangs' required class='form-control select2gudang'></select>
        </div>
        <div class='form-group'>
          <label>Jumlah</label>
          <input type='number' name='jumlah' required class='form-control' value="" min="0" placeholder="0" />
        </div>
        <input type="hidden" name="lanjut" value="k3"/>
        <button type="submit" class="btn btn-success pull-right">Add</button>
    </form>
    <br><br>
    <hr>
    @if( $mix_2 )
    <div class="table-responsive">
      <table id="table_dashboard" class="table table-hover table-striped table-bordered">
        <thead>
          <th>Nama</th>
          <th>Kode</th>
          <th>Jumlah</th>
          <th>Action</th>
        </thead>
        <tbody>
            @foreach( $mix_2 as $k => $m)
            <tr>
              @foreach( $m as $k2 => $m2)
                @if(($k2==='id'))
                  <td> <a class="btn btn-xs btn-warning btn-delete" title="Delete" href="javascript:;" onclick="swal({
                          title: 'Are you sure ?',
                          text: 'You will not be able to recover this record data!',
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#ff0000',
                          confirmButtonText: 'Yes!',
                          cancelButtonText: 'No',
                          closeOnConfirm: false },
                          function(){  location.href='{{ route('resep.mix.del',$m2) }}'});"><i class="fa fa-trash"></i></a> </td>
                @else
                  <td> {{ $m2 }} </td>
                @endif
              @endforeach
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
    @else
      Belum ada data
    @endif
  </div>
    <div class='panel-footer'>
      <a href="{{ route('resep.mix',1) }}" type="button" class="btn btn-primary">Prev</a>
      <a href="{{ route('resep.mix',3) }}" type='button' class='btn btn-primary pull-right'>
        Next</a>
      <br>
      <br>
    </div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
var satuans = <?php echo($satuans); ?>;
var varBarangs = <?php echo($barangs); ?>;
var selKategori = "{{ Session::get('mix_1')[0]['category'] }}";
var selSatuan = "{{ Session::get('mix_1')[0]['satuan'] }}";
var url = '';

$(function(){
  $(".select2satuans").select2({
    data:satuans
  });
  $('.select2barang').select2({
    data:varBarangs,
    placeholder: "Pilih Barang",
  }).on('select2:select',function(){
    var id = $(this).val();
    if(id){
        url = '<?php echo url('/'); ?>'+'/admin/reseps/getGudang_barang/'+id;
        $.get(url, function (gg){
          if(gg == "null"){
            gudang_barang = null;
          }
          else{
            gg = jQuery.parseJSON(gg);
            gudang_barang = gg;
          }
          $('.select2gudang').empty().select2({
            placeholder: 'Barang tidak ada di gudang',
            data: gudang_barang
          }).trigger('change');
        });
    }
    else {
      $('.select2gudang').empty().select2({
        placeholder: 'Pilih Barang Dahulu',
        data: null
      });
    }
  });

  $(".select2gudang").select2({
    placeholder: 'Pilih Barang Dahulu',
  });
  inputjumlah = $('input[name="jumlah"]');

  $(document).on('change','.select2gudang',function(e){
    inputjumlah.val('');
    stok = $(".select2gudang :selected").attr('title');
    if(stok) inputjumlah.attr('placeholder','max = '+stok).attr('max',stok);
    else inputjumlah.attr('placeholder','0').attr('max',0);
  })

  // $("#tableMix_2").DataTable();
})
</script>
@endpush
