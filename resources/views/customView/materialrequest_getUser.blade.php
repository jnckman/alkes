<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../mronline/js/html5shiv.js"></script>
    <script src="../mronline/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/datatables.min.css') }}">
    <style type="text/css">
    	.mr_clickable:hover .single-products .productinfo{
    		background-color: #FE980F;
    		cursor: pointer;
    	}
    	.mr_clickable:hover .single-products .productinfo p{
    		color : #fff;
    	}
    </style>

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					@include('customView.partial.navbar')
				</div>
			</div>
		</div><!--/header-middle-->
	
		
	</header><!--/header-->
	@if(@$alerts)
        		@foreach(@$alerts as $alert)
        			<div class='callout callout-{{$alert[type]}}'>        				
        					{!! $alert['message'] !!}
        			</div>
        		@endforeach
    @endif
	
	<section>
		<div class="container">
			<ol class="breadcrumb">
			  <li class="active">Profil</li>
			</ol>
			<a href="{{ route('subdept.mr.mainpage')}}" type="button" class="btn btn-default">Back</a>
			<h3>User : {{CRUDBooster::myName()}}</h3>
			<div class="form-group">
				<label>Pilih Periode MR</label>
				<select id="select2periode" class="form-control"><option></option></select>
			</div>
			<div class="form-group">
				<label>Pilih Department :</label>
				<select class="form-control" id="select2depts" name="dept_id"><option></option></select>
			</div>
			<div class="form-group">
				<label>Pilih Sub Department / ASM :</label>
				<select class="form-control" id="select2subdepts" name="subdepts"><option></option></select>
			</div>
			<br>
			<br>
			<div class="lds-css ng-scope" style="display: none" id="wait"><div style="width:100%;height:100%" class="lds-rolling"><div></div></div></div>
			<ul class="nav nav-tabs nav-justified">
	          <li class="active"><a data-toggle="tab" href="#tab_isi">List MR</a></li>
	          <li ><a data-toggle="tab" href="#tab_revisi">List Barang</a></li>
	        </ul>
	        <br>
	        <div class="tab-content">
			<div id="tab_isi" class="tab-pane fade in active">
			<div id="periode_MR">
				<div class="table-responsive" >
					<table class="table table-hover table-striped" id="tablelistmr" style="width: 100%">
					<thead>
						<th>No</th>
						<th>No MR</th>
						<th>Project</th>
						<th>LimitBulan</th>
						<th>Tanggal</th>
						<th>User</th>
						<th>Approval</th>
						<th>Status Kirim</th>
						<th>Total</th>
						<th>Action</th>
					</thead>
					</table>
				</div>
			</div>
			</div>
			<div id="tab_revisi" class="tab-pane fade">
			<div id="barang_MR">
				<div class="table-responsive" >
					<table class="table table-hover table-striped" id="tableListBarang" style="width: 100%">
					<thead>
						<th>Nama Barang</th>
						<th>Kode Barang</th>
						<th>Nomor MR</th>
						<th>Jumlah</th>
						<th>Total</th>
					</thead>
					<tfoot>
						<th colspan="4"><p class="text-right">Total</p></th>
						<th></th>
					</tfoot>
					</table>
				</div>
			</div>
			</div>
			</div>
		</div>
		<br/><br/><br/>
	</section>

	<div class="modal fade" id="modalBarangMr" role="dialog">
		<div class="modal-dialog" style="width: 670px;margin: auto;">
    		<div class="modal-content">
    			<div class="modal-header">
    			</div>
    			<div class="modal-body">
    				<ul class="nav nav-tabs nav-justified">
						<li class="active"><a data-toggle="tab" href="#tab_barangs">Barang</a></li>
						<li ><a data-toggle="tab" href="#tab_revisi_mr">Revisi</a></li>
					</ul>
					<div class="tab-content">
						<div id="tab_barangs" class="tab-pane active">
		    				<div class="table-responsive">
		    					<table id="barangBody" class="table table-striped" style="width: 100%">
								<thead>
									<th>No</th>
									<th>Barang</th>
									<th>Jumlah</th>
									<th>Dikirim</th>
									<th>Keterangan</th>
									<th>Action</th>
								</thead>
			    				</table>
		    				</div>
		    			</div>
		    			<div id="tab_revisi_mr" class="tab-pane">
		    				<div class="table-responsive">
		    					<table id="tablerevisimr" class="table table-striped" style="width: 100%">
								<thead>
									<th>Tanggal</th>
									<th>Barang</th>
									<th>Jumlah Lama</th>
									<th>Jumlah Baru</th>
									<th>Keterangan Lama</th>
									<th>Keterangan Baru</th>
									<th>User</th>
								</thead>
			    				</table>
		    				</div>
		    			</div>
		    		</div>
    			</div>
    			<div class="modal-footer">
    				<button class="btn btn-primary" data-dismiss='modal'>Close</button>
    			</div>
    		</div>
    	</div>
	</div>

	<div class="modal fade" id="modalUbahStatusKirim" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
    			<form id="formStatus">
    				{{ csrf_field() }}
    			<div class="modal-header">
    			</div>
    			<div class="modal-body">
    					<div class="form-group">
    						<label>Status Pengiriman Baru : </label>
	    					<input id="inputstatusbaru" class="form-control" type="text" required="true" name="status" placeholder="Masukkan status baru" />
    					</div>

    			</div>
    			<div class="modal-footer">
    				<button type="submit" class="btn btn-success">Submit</button>
    				<button class="btn btn-default" data-dismiss='modal'>Close</button>
    			</div>
    			</form>
    		</div>
    	</div>
	</div>

	<div class="modal" id="modalPengiriman" tabindex="-1" role="dialog" aria-hidden="true">
	   <div class="modal-dialog">
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	            <h4 class="modal-title">List Pengiriman</h4>
	         </div>
	         <div class="modal-body">
	         	<table class="table table-bordered" id="tablelistkirim" style="width:100%;">
	               <thead>
	                 <tr>
	                   <th>Kode</th>
	                   <th>Tanggal</th>
	                   <th>Status</th>
	                   <th>Action</th>
	                 </tr>
	               </thead>
	            </table>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="modal" id="modallistpengirimanbarang" tabindex="-1" role="dialog" aria-hidden="true">
	   <div class="modal-dialog">
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	            <h4 class="modal-title">List Barang Pengiriman</h4>
	         </div>
	         <div class="modal-body">
	         	<table class="table table-bordered" id="tablelistdetailkirim" style="width:100%;">
	               <thead>
	                 <tr>
	                   <th>Barang</th>
	                   <th>Jumlah</th>
	                 </tr>
	               </thead>
	            </table>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	         </div>
	      </div>
	   </div>
	</div>
	
	<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
		
		<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
		
		
	</footer><!--/Footer-->
  
    <script src="{{asset('mronline/js/jquery.js')}}"></script>
	<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('mronline/js/price-range.js')}}"></script>
    <script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('mronline/js/main.js')}}"></script>
    <script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('customs/js/select2.min.js') }}"></script>
    <script src="{{ asset('js/datatable.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	var periode = <?php echo($periode) ;?>;
	var userLevel = 5; //perlu diganti
	var departments = <?php echo $departments;?>;
	var privilege = <?php echo $privilege?>;
	var subdepts = <?php echo $subdepts?>;
	// console.log(subdepts);
	var periode_id = 0;
	var subdept_id = 0;
	$("#select2periode").select2({
		data : periode,
		placeholder: "Pilih Periode",
	});

	$(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

    var tablebrgmr = $('#barangBody').DataTable({
    	data:[],
    	"lengthChange": false,
    	"searching": false,
		columns: [
		{ "data": "no" },
		{ "data": "barang" },
		{ "data": "jumlah" },
		{ "data": "dikirim" },
		{ "data": "keterangan" },
		{ "data": "action" }
		],
    });

    var tablerevisimr = $('#tablerevisimr').DataTable({
    	data:[],
    	"lengthChange": false,
    	"searching": false,
		columns: [
		{ "data": "tanggal" },
		{ "data": "barang" },
		{ "data": "jumlah_lama" },
		{ "data": "jumlah_baru" },
		{ "data": "keterangan_lama" },
		{ "data": "keterangan_baru" },
		{ "data": "user_id" },
		],
    });

    $('#select2subdepts').select2({
    	data:subdepts,
    	placeholder:'Pilih Department terlebih dahulu'
    });

    $('#select2depts').select2({
    	data:departments,
    	placeholder: 'Pilih Department',
    });//.val('{{session()->get("cart")->subdepts["dept_id"]}}').trigger('change')

    function configSelectByPrivilege(){
    	if(parseInt(privilege[1]) <= 1 ){
			var projects1 = '';
    		$('#select2depts').val($('#select2depts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
	    	$('#select2subdepts').val($('#select2subdepts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
	    	nama_dept = $('#select2depts').select2('data')[0].text;
	    	subdept_id = $('#select2subdepts').select2('data')[0].id;
		}
		else if(parseInt(privilege[1]) < 4 ){
			// console.log('etogege');
			var projects1 = '';
			$('#select2depts').val($('#select2depts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
			nama_dept = $('#select2depts').select2('data')[0].text;
		}
    }

    configSelectByPrivilege();

    var table = $('#tablelistmr').DataTable({
    	// "searching": false,
    	// ajax : '{{route("mr.dt_history")}}',
    	data:[],
		columns: [
		{ "data": "uruts" },
		{ "data": "nomr" },
		{ "data": "projects" },
		{ "data": "saldo" },
		{ "data": "tanggal" },
		{ "data": "user" },
		{ "data": "approval" },
		{ "data": "status_kirim" },
		{ "data": "total" },
		{ "data": function (data, type, dataToSet) {
        	return data.action1 + ' ' +data.action2+' '+data.action3;
    	}}
		],
    });

    var table2 = $('#tableListBarang').DataTable({
    	// "searching": false,
    	// ajax : '{{route("mr.dt_history")}}',
    	data:[],
		columns: [
		{ "data": "nama" },
		{ "data": "kode" },
		{ "data": "nomr" },
		{ "data": "jumlah" },
		{ "data": "total" },
		/*{ "data": function(data,type,dataToSet){
			return 'Rp '+data.total.toLocaleString('de-DE');
		}},*/
		],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Rp '+ total.toLocaleString()
            );
        },
    });

	$(document).on("select2:selecting","#select2periode",function(e) {
		/*if(userLevel==5){
			id = e.params.args.data.id;
			$.ajax({
	            url: 'ajax_getMRs',
	            data: {
	            	"periode_mrs_id" : id,
	            }
	        }).done(function(data){
	        	// console.log(data);
	        	if(data.length==0) {
	        		// $("#periode_MR").html('Tidak ada data');
	        		table.clear().draw();
	        		table2.clear().draw();
	        	}
	        	else {
	        		table.clear().draw();
					table.rows.add(data).draw();
					table2.clear().draw();
					table2.rows.add(data.barangs).draw();
	        	}
	        }).fail(function(){
		        console.log('Terjadi Kesalahan');
		    });
		}
		else{
			
		}*/
		periode_id = e.params.args.data.id;
		updateTable();
	});

    $(document).on('select2:select','#select2depts',function(){
	    var id = $(this).val();
	    nama_dept = $('#select2depts').select2('data')[0].text;
	    if(nama_dept.toLowerCase()=='opd') isOPD = 1;
	    else isOPD =0;
	    if(id){
	        url = '<?php echo url('/'); ?>'+'/mrmainpage/getSubdept/'+id;
	        $.ajax({
	        	url:url,
	        	data:{
	        		'id':id,
	        		'dept':nama_dept,
	        		'nogetgis' : 1,
	        	}
	        }).done(function (gg){
	        	// console.log(gg);
	          if(gg == "null"){
	            subdepts = null;
	          }
	          else{
	            gg = jQuery.parseJSON(gg);
	            subdepts = gg;
	          }
	          $('#select2subdepts').empty().select2({
	            placeholder:'Pilih Department terlebih dahulu',
	            data: subdepts
	          }).val('').trigger('change');
	        }).fail(function(){
	        	swal('Gagal Mengambil Data !', 'Silahkan Coba Lagi', 'error').catch(swal.noop);
	        	// console.log('Terjadi Kesalahan');
	        	resetForm();
	        });
	    }
  	});

    $(document).on('select2:selecting','#select2subdepts',function(e){
  		subdept_id = e.params.args.data.id;
  		// console.log(periode_id+' | '+subdept_id);
  		if(periode_id>0){
  			updateTable();
  		}
  	});
	
	function updateTable(){
		$.ajax({
            url: 'ajax_getMRs',
            data: {
            	"periode_mrs_id" : periode_id,
            	"subdepts" : subdept_id,
            	"lvl" : privilege[1],
            }
        }).done(function(data){
        	// console.log(data);
        	if(data.length==0) {
        		// $("#periode_MR").html('Tidak ada data');
        		table.clear().draw();
        		table2.clear().draw();
        	}
        	else {
        		table.clear().draw();
				table.rows.add(data.mr).draw();
				table2.clear().draw();
				table2.rows.add(data.barangs).draw();
        	}
        }).fail(function(){
	        console.log('Terjadi Kesalahan');
	    });
	}

	var last_mr_id='0';
	$(document).on("click",".mr_clickable",function(e) {
		id = $(this).attr('mr_id');
		suntingUpdateTotal = $(this).parent().prev();
		nama_project = $(this).parent().parent().find('td:eq(1)').html();
		data = table.rows($(this).parent().parent()).data()[0]['status_kirim'];
		// console.log(data);
		if(data=="belum kirim"){
			unedit = 0
		}
		else{
			unedit = 1;
		}
		if(last_mr_id!=id){
			last_mr_id = id;
			$.ajax({
	            url: 'ajax_getMRs',
	            data: {
	            	"mr_subdepts_id": id,
	            	'unedit' : unedit
	            }
	        }).done(function(data){
	        	console.log(data);
	        	if(data.data.length>0){
	        		tablebrgmr.clear().draw();
					tablebrgmr.rows.add(data.data).draw();
					tablerevisimr.clear().draw();
					tablerevisimr.rows.add(data.revisi).draw();
					nomorurut(tablebrgmr);
					// nomorurut(tablerevisimr);
		        	$('#modalBarangMr .modal-header').html('<h4>'+nama_project+'</h4><h5>Saldo : Rp '+data.data[0].saldo+'</h5>');
		        	$('#modalBarangMr').modal('show');
	        	}
	        	else{
	        		tablebrgmr.clear().draw();
	        		tablerevisimr.clear().draw();
	        		swal('Oops','Tidak ada Data','error').catch(swal.noop);
	        	}
	        }).fail(function(){
		        console.log('Terjadi Kesalahan');
		    });
		}
		else {
			$('#modalBarangMr').modal('show');
		}
	});

	/*$(document).on('click','.btnUpdate',function(e) {
		id = $(this).parent().parent().attr('isiMrId');
		jumlah = $(this).parent().parent().find('input').val();
		parent = $(this).parent().parent();
		url = '{{ route("subdept.mr.historyUpdateJumlahBarang") }}';
		$.ajax({
			url:url,
			type:"patch",
			data:{
				"_token": "{{ csrf_token() }}",
				'id':id,
				'jumlah':jumlah,
			}
		}).done(function(data){
			alert('Data updated !');
			parent.find('td[class="jumlah"]').html(jumlah);
		}).fail(function(data){
			console.log('gagal update');
		})
	});*/

	$(document).on('click','.btnHapus',function(e) {
		id = $(this).parent().parent().attr('isiMrId');
		jumlah = $(this).parent().parent().find('input').val();
		url = '{{ route("subdept.mr.historyDelBarang") }}';
		parent = $(this).parent().parent();
		$.ajax({
			url:url,
			type:"delete",
			data:{
				"_token": "{{ csrf_token() }}",
				'id':id,
			}
		}).done(function(data){
			alert('Data deleted !');
			parent.remove();
		}).fail(function(data){
			console.log('gagal delete');
		})
	});

	var mr_id_clicked;
	var status_edit;
	$(document).on('click','.mr_edit_kirim',function(event){
		//show modal 
		$('#inputstatusbaru').val('');
		$('#modalUbahStatusKirim .modal-header').html('<h4>'+ $(this).parent().parent().find('td:eq(1)').html() +'</h4>');
		$('#modalUbahStatusKirim').modal('show');
		mr_id_clicked = $(this).attr('mr_id');
		status_edit = $(this).parent().parent().find('td:eq(5)');
	});

	$('#formStatus').on('submit',function(e){
    	e.preventDefault();
    	url = "{{ route('subdept.mr.user.updateStatus') }}";
    	$.ajax({
    		type: 'POST',
    		url:url,
    		data: $("#formStatus").serialize() + "&mr_id=" + mr_id_clicked,
    	}).done(function(response){
    		// console.log(response);
    		$('#modalUbahStatusKirim').modal('hide');
    		$('#inputstatusbaru').val('');
    		status_edit.html(response);
    		swal({
			    type: 'success',
			    html: 'Berhasil mengubah Status Kirim',
			    confirmButtonColor: '#FE980F',
					}).catch(swal.noop);
    	}).fail(function(response){
    		$('#modalUbahStatusKirim').modal('hide');
    		swal('Oops...', 'Terjadi Kesalahan !', 'error').catch(swal.noop);
    	});
    });

    $(document).on('click','.btnApprove',function(){
    	id = $(this).attr('mr_id');
    	buttonku = $(this);
    	// console.log(table.row(buttonku.parent().parent()).data().action2);
    	url = "{{ route('mr.user.postApprove') }}";
    	swal({
    		type :'info',
          	html: 'Apakah anda yakin akan memberi approval ?',
		    showCancelButton: true,
		    // confirmButtonColor: '#d2322d',
		    cancelButtonText: 'Cancel',
    	}).then(function(){
    		$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "mr_id" : id,
		        },
		    }).done(function(response) {
		    	console.log(response);
		    	if(response.berhasil==1){
		    		swal('berhasil','Berhasil memberi approval !','success')
		    			// location.reload();
		    			table.row(buttonku.parent().parent()).data().action2 = '';
		    			table.row(buttonku.parent().parent()).data().approval = 'oke';
		    			table.row(buttonku.parent().parent()).invalidate().draw();
	
		    	}else {
		    		swal('Oops...', 'Gagal memberi approval !', 'error').catch(swal.noop);
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error').catch(swal.noop);
	        });
    	}).catch(swal.noop);
    });

    $(document).on('click','.mr_delete',function(){
    	id = $(this).attr('mr_id');
    	url = "{{ route('mr.user.deleteMR') }}";;
    	rowku = $(this).parent().parent();
    	swal({
    		type :'warning',
          	html: 'Apakah anda yakin akan menghapus ?',
		    showCancelButton: true,
		    confirmButtonColor: '#d2322d',
		    cancelButtonText: 'Cancel',
    	}).then(function(){
    		$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "mr_id" : id
		        },
		    }).done(function(response) {
		    	// console.log(response);
		    	if(response.berhasil==1){
		    		swal('berhasil','MR berhasil dihapus !','success');
		    		table.row(rowku).remove().draw();
		    	}else {
		    		swal('Oops...', 'Gagal Menghapus MR !', 'error');
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
    	}).catch(swal.noop);
    });

    var tablekirim = $('#tablelistkirim').DataTable({
    	'processing': true,
		'serverside':true,
		"pageLength": 20,
		"lengthChange": false,
		"searching": false,
		columns: [
			{ "data": "kode" },
			{ "data": "tgl_kirim" },
			{ "data": "status" },
			{ "data": "action" },
		],
    });

    var tabledetailkirim = $('#tablelistdetailkirim').DataTable({
    	'processing': true,
		'serverside':true,
		"pageLength": 20,
		"lengthChange": false,
		"searching": false,
    });

    $(document).on('click','.mr_lihat_kirim',function(){
    	data = table.row($(this).closest('tr')).data();
    	// console.log(data);
    	$.ajax({
    		url : "{{route('mr.getlistkirim',1)}}",
    		data :{
    			'id' : data['mr_id'],
    		}
    	}).done(function(response){
    		// console.log(response);
    		tablekirim.clear().draw();
    		tablekirim.rows.add(response).draw();
    		$('.nama_mrkirim').html(data['nomr'])
    		$('#modalPengiriman').modal('show');
    	}).fail(function(){
    		console.log('Terjadi kesalahan');
    	});
    });

    $(document).on('click','.btn_detailkirim',function(){
    	data = tablekirim.row($(this).closest('tr')).data();
    	// console.log(data);return;
    	$.ajax({
    		url : "{{route('mr.getlistkirim',2)}}",
    		data :{
    			'id' : data['id'],
    		}
    	}).done(function(response){
    		// console.log(response);
    		tabledetailkirim.clear().draw();
    		tabledetailkirim.rows.add(response).draw();
    		$('#modallistpengirimanbarang').modal('show');
    	}).fail(function(){
    		console.log('Terjadi kesalahan');
    	});
    });
    $('#modallistpengirimanbarang').on('show.bs.modal', function (e) {
        $('#modalPengiriman').modal('hide');
    });
    $('#modallistpengirimanbarang').on('hide.bs.modal', function (e) {
        $('#modalPengiriman').modal('show');
    });

    $(document).on('click','.mr_selesai',function(event){
    	buttonku = $(this);
    	id = table.row(buttonku.parent().parent()).data().mr_id;
    	url = "{{ route('mr.user.postSelesai') }}";
    	swal({
    		type :'info',
          	html: 'Apakah anda yakin akan menandai MR sudah selesai ?',
		    showCancelButton: true,
		    // confirmButtonColor: '#d2322d',
		    cancelButtonText: 'Cancel',
    	}).then(function(){
    		$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "mr_id" : id,
		        },
		    }).done(function(response) {
		    	// console.log(response);
		    	if(response.berhasil==1){
		    		swal('berhasil','Berhasil memberi approval !','success')
		    			// location.reload();
		    			table.row(buttonku.parent().parent()).data().action3 = '';
		    			table.row(buttonku.parent().parent()).data().approval = 'Selesai';
		    			table.row(buttonku.parent().parent()).invalidate().draw();
	
		    	}else {
		    		swal('Oops...', 'Gagal memberi approval !', 'error').catch(swal.noop);
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error').catch(swal.noop);
	        });
    	}).catch(swal.noop);
    });

    function nomorurut(table){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   }

   $(document).on('click','.btneditjumlahbarang',function(){
	   	suntingUpdateJmlh = $(this).parent().prev().prev();
    	suntingUpdateKtr = $(this).parent().prev();
    	id = tablebrgmr.row($(this).parent().parent()).data().id;
   		var steps = [
		  {
		    title : 'Revisi', 
			html: 'Masukkan jumlah baru',
			input: 'text',
			inputPlaceholder: '0',
			showCancelButton: true,
			confirmButtonColor: '#FE980F',
			confirmButtonText: 'Next &rarr;',
			cancelButtonText: 'Cancel',
			inputValidator: function (value) {
				return new Promise(function (resolve, reject) {
				  if (parseInt(value)>0) {
				    resolve()
				  } else {
				    reject('Input harus angka dan > 0 !')
				  }
				})
			}
		  },
		  {
  		    title : 'Masukkan Keterangan Baru :', 
  		    html : 'jika ada perubahan',
  			input: 'text',
  			inputPlaceholder: 'opsional (contoh: "Budi; Ani; ")',
  			showCancelButton: true,
  			confirmButtonColor: '#FE980F',
  			confirmButtonText: 'Submit',
  			cancelButtonText: 'Cancel',
	  		}
		];
    	swal.queue(steps)
		.then(function(gg){  
			// console.log(gg);
			// return;
			url = '{{ route("subdept.mr.historyUpdateJumlahBarang") }}';
			$.ajax({
	            type: "post",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
			        "qnty" : gg[0],
			        "keterangan" : gg[1],
		        },
	        }).done(function(response){
	        	console.log(response);
        		swal({
				    type: 'success',
				    html: 'Data berhasil diedit',
				    confirmButtonColor: '#FE980F',
				});
				suntingUpdateJmlh.html(gg[0]);
				// suntingUpdateJmlh.html(response.jmlh);
				if(gg[1].length>0){
					suntingUpdateKtr.html(gg[1]);
				}
				suntingUpdateTotal.html(response.total);
				// suntingUpdateKtr.html(response.ktr);
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
		}).catch(swal.noop);
   });

   $(document).on('click',".btn_approvekirim",function(){
   		id = tablekirim.rows($(this).parent().parent()).data()[0]['id'];
   		suntingStatusKirim = $(this).parent().prev();
   		url = '{{ route("subdept.mr.kirimselesai") }}';
			$.ajax({
	            type: "post",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
		        },
	        }).done(function(response){
	        	console.log(response);
        		swal({
				    type: 'success',
				    html: 'Pengiriman berhasil ditandai',
				    confirmButtonColor: '#FE980F',
				});
				suntingStatusKirim.html(response);
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
   });

});
</script>
<style type="text/css">
	.selected{
		border-bottom: 2px solid grey;

	}
</style>
</body>
</html>