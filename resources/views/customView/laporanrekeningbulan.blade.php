@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../isipurchaseorder/{{$id}}"><button class="btn btn-info">Kembali</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       
         
         
         <div class="col-md-12" style="margin-top: 50px;">

         <div class="col-md-12" style="margin-bottom: 10px;">
            <a href="printspbm">
              <button class="btn btn-info btn-xs">Print Laporan Rekening</button>
            </a>
         </div>
         <div class="col-md-12">

         <table class="table table-bordered example1" id='table_dashboard' >
           <thead>
                 <tr class="success">
                     <th>TGL</th>
                     <th>TRANSAKSI</th>
                     <th>SALDO SEBELUM</th>
                     <th>NOMINAL TRANSAKSI</th>
                     <th>SALDO SEKARANG</th>
                     <!-- <th>JATUH TEMPO</th>
                     <th>SALDO HUTANG</th>
                     <th>PEMBELIAN</th>
                     <th>PELUNASAN</th>
                     <th>SISA</th>
                     <th>VIA</th>
                     <th>TGL LUNAS</th>
 -->
                     
                     
                     
                     
                     

                 </tr>
          </thead>
          <tbody>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
              <?php $ct = 0 ; $temp;?>
              @foreach($datasimpan as $d)
              
              
              <tr>
                
                <td>{{$d->created_at}}</td>
                @if($ct==0)
                <?php $temp = $datarek[0]->saldoawal; ?>
                <td>{{$temp}}</td>
                @else
                <td>{{$temp}} </td>
                @endif
                <td>{{$d->saldo}}</td>
                <td>{{$temp - $d->saldo}}</td>
                <td>{{$d->catatan}}</td>
                
                

                
              </tr>
              <?php $ct++; ?>
              @endforeach
          </tbody>
         </table>
         </div>
         <div class="col-md-7">

         
         
         </div>
         </div>
       
       
       </div>
       </div>
     </div>
   </div>


@endsection

@section('datatable')

@endsection

