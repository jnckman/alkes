@extends('customView.partial.shop_template')
@push('head')
<style type="text/css">
	
</style>
@endpush
@section('content')
<div class="container">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-2">
	<div class="left-sidebar">
		<div class="col-md-12" style="padding: 0px;">
		<h2>Category</h2>
		<div class="panel-group category-products" id="accordian"><!--category-productsr-->
			<?php $ct = 1; ?>
			@foreach($kategoris as $key => $k)
			<div class="panel panel-default">
				<div class="panel-heading" id="viewdetails">
					@if($k->id == $idKat )
					<h4 class="panel-title"><a href="{{ route('shop.barangByKategori',$k->id) }}" class="ambilBarangs selected" kat_id="{{$k->id}}">{{ $k->nama }}</a></h4>
					@else
					<h4 class="panel-title"><a href="{{ route('shop.barangByKategori',$k->id) }}" class="ambilBarangs" kat_id="{{$k->id}}">{{ $k->nama }}</a></h4>
					@endif
				</div>
			</div>
			@endforeach
		</div><!--/category-products-->
		</div>
	</div>
	</div>

	<div class="col-sm-10">
		@if(strtolower(CRUDBooster::myPrivilegeName())=='customer')
		<div class="features_items" style="display:none"><!--features_items-->
		@else
		<div class="features_items"><!--features_items-->
		@endif
				@if(strtolower(CRUDBooster::myPrivilegeName())=='super administrator')
				<h2 class="title text-center">Pilih Customer</h2>
				<div class="row">
				<form class="form-inline" action="{{route('so.setcustomer')}}" method="post" id="formubahcustomer">
					{{csrf_field()}}
					<div class="col-sm-10">
						<select style="width: 100%" id="select2customer" name="customer" class="form-control"><option></option></select>
					</div>
					<div class="col-sm-2">
						<button style="width: 100%" type="button" class="btn btn-warning btnsubmitform">Ubah</button>
					</div>
				</form>
				</div>
				<br>
				@endif
		</div>
			<div class="features_items"><!--features_items-->
				<h2 class="title text-center">BARANG TERBARU</h2>
				<div class="row">
					<?php $konter = 1;?>
					@foreach($barangBaru as $key => $val)
					<?php //if($konter==3)dd($val->barang->harga_jual); ?>
					<div class="col-xs-6 col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center">
										<h5 class="namabarang">{{$val->nama}}</h5>
										<div style="height: 150px;">
											<img src="{{asset($val->gambar)}}" alt="GambarBarang" style="max-width:150px;max-height:130px">
										</div>
<!-- =================================================================================================================== -->
@if(isset($data['customer']))
<?php
$harga_jual = $val->harga_jual()->where('ket',$data['customer']->jenis_harga_jual)->latest()->first()->harga_jual; 
if(!isset($harga_jual)) $harga_jual = 0;
?>
<h4>Rp {{number_format($harga_jual)}}</h4>
<a class="btn btn-default addCart add-to-cart" barang_id="{{$val->id}}" satuan="{{$val->satuan->nama}}" harga_jual="{{$harga_jual}}" style="width: 88%"><i class="fa fa-shopping-cart">
</i>Add to cart</a>
@endif
<!-- =================================================================================================================== -->

										<a class="btn btn-default detailBarang add-to-cart" barang_id="{{$val->id}}" style="width: 88%"><i class="fa fa-eye">
										</i>Detail</a>
									</div>
							</div>
						</div>
					</div>
					@if($konter % 2==0)
					<div class="clearfix visible-xs"></div>
					@endif
					<?php $konter++?>
					@endforeach
				</div>
			</div>

			<div class="features_items">
				<h2 class="title text-center">BARANG LAINNYA</h2>
				<div class="lds-css ng-scope" style="display: none" id="wait"><div style="width:100%;height:100%" class="lds-rolling"><div></div></div></div>
				<!-- css Loading -->
				<div class="row">
				<?php $konter = 1?>
				@foreach($barangs as $key => $val)
					<div class="col-xs-6 col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center">
									<h5>{{$val->nama}}</h5>
										<div style="height: 150px;">
											<img src="{{asset($val->gambar)}}" alt="GambarBarang" style="max-width:150px;max-height:130px">
										</div>
<!-- =================================================================================================================== -->
@if(isset($data['customer']))
<?php
$harga_jual = $val->harga_jual()->where('ket',$data['customer']->jenis_harga_jual)->latest()->first()->harga_jual; 
if(!isset($harga_jual)) $harga_jual = 0;
?>
<h4>Rp {{number_format($harga_jual)}}</h4>
<a class="btn btn-default addCart add-to-cart" barang_id="{{$val->id}}" satuan="{{$val->satuan->nama}}" harga_jual="{{$harga_jual}}" style="width: 88%"><i class="fa fa-shopping-cart">
</i>Add to cart</a>
@endif
<!-- =================================================================================================================== -->
										<a class="btn btn-default detailBarang add-to-cart" barang_id="{{$val->id}}" style="width: 88%"><i class="fa fa-eye">
										</i>Detail</a>
									</div>
							</div>
						</div>
					</div>
					@if($konter % 2 == 0)
					<div class="clearfix visible-xs"></div>
					@endif
					@if($konter % 4 == 0)
					<div class="clearfix visible-md visible-sm visible-lg"></div>
					@endif
					<?php $konter++?>
				@endforeach
				</div>
				@if(count($barangs)>0)
				<div class="col-xs-12 col-md-12">
					<div class="text-center">
						@if(isset($data['customer']))
						{{ $barangs->links() }}
						@endif
					</div>
				</div>
				@endif
			</div>
	</div>

</div>
</div>
</div>

<div class="modal fade" id="modalDetailBarang" tabindex="-1" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> </div> <div class="modal-body"> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div> 
@endsection
@push('bottom')
<script type="text/javascript">
$(function() {
	//DEKLARASI VARIABLE
	var datacustomer = <?php echo $datacustomer; ?>;
	console.log(datacustomer);
	var customerku = 0;
	<?php if (session()->has('customerShop')) {
			?> customerku = <?php echo session()->get('customerShop')->id;?>;<?php 
			}
	?>
	//

	var select2customer = $('#select2customer').select2({
		placeholder: 'Pilih Customer Terlebih Dahulu',
		data: datacustomer
		// ajax:{url : '{{route("so.getcustomer")}}', processResults: function (data) {return {results: data }; }, }
	});

	if(customerku>0){
		select2customer.val(customerku).trigger('change');
	}

	$('.btnsubmitform').click(function(event){
		swal({
			type: 'info',
			title: 'Warning',
            html: 'Mengubah Customer akan menghapus Cart', 
            showCancelButton: true,
            confirmButtonColor: '#FE980F',
            confirmButtonText: 'Submit',
            cancelButtonText: 'Cancel',
		}).then(function(){
			$('#formubahcustomer').submit();
		})
	});
});
</script>
@endpush