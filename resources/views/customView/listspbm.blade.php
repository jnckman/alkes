@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> List SPBM
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(purchaseorders)}}">Purchase Order</a></li>
   <li class="active">List SPBM</li>
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <div class="col-md-10">
            <a href="../lihatisispbm/{{$id}}" class="pull-right">
              <button class="btn btn-success">
                Lihat Flow Barang Masuk
              </button>
            </a>
          </div>
          <div class="col-md-2">
            <a href="../returisispbm/{{$id}}" class="pull-right">
              <button class="btn btn-warning">
                Retur Barang
              </button>
            </a>
          </div>

        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-bordered example1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>no spbm</th>
                     <th>tanggal terima</th>
                     <th>penerima</th>
                     <th>gudang</th>
                     <th>no faktur</th>
                     <th>action</th>
                     <th>approve</th>
                     
                     
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>


@endsection

@section('datatable')
<script type="text/javascript">
    $(document).ready(function() {
    
          $('.example1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../listspbmdtajax/{{$id}}',
              
          } );


      } );
</script>
@endsection

