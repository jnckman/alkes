@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Admin
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Admin</li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
   <div class="box box-default">
      <!-- <form id="formubahperiode"> -->
      <div class="box-header">
         <h3 class="box-title">Periode  :</h3>
      </div>
      <div class="box-body">
         <select id="select2periode" class="form-control"></select>
      </div>
      <div class="box-footer">
         <button type="button" id="btnubahperiode" class="btn btn-success">Ubah</button>
      </div>
      <!-- </form> -->
   </div>
</div>
</div>

<div class="row" >
   <div class="col-md-7">
      <div class="box box-danger"">
         <div class="box-header with-border">
            <h3 class="box-title">Total PO Berjalan</h3>
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm" id="refreshpo" >
                  <i class="fa fa-refresh"></i></button>
                  <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button>
                </div>
            </div>
         </div>
         <div class="box-body">
            <canvas id="bar-po" width="400" height="100"></canvas>
            <canvas id="pie-po" width="400px" height="120px"></canvas>
         </div>
      </div>
   </div>
   <div class="col-md-5">
      @for($i=0;$i<3;$i++)
      <div class="row"><div class="col-md-12">
      <div class="box box-danger"">
      <div class="box-header with-border">
         <div class="box-tools pull-right">
            <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn btn-default btn-sm">
               <i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm" >
               <i class="fa fa-question"></i></button>
            </div>
         </div>
      </div>
      <div class="box-body">
         <h1>TODO : chart lain</h1>
      </div>
      <div class="box-footer">
         
      </div>
      </div>
      </div></div>
      @endfor
   </div>
</div>


<div class="row" >
   <div class="col-md-7">
      <div class="box box-info"">
         <div class="box-header with-border">
            <p class="box-title">Total MR berjalan</p>
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm" id="refreshmr">
                  <i class="fa fa-refresh"></i></button>
                  <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button>
                </div>
            </div>
         </div>
         <div class="box-body">
            <canvas id="bar-mr" width="350" height="250"></canvas>
         </div>
      </div>
   </div>
</div>

<div class="row" ><div class="col-md-7">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Nilai Stok</p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshstok"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <canvas id="bar-nilai-stok" width="350" height="250"></canvas>
      </div>
   </div>
</div></div>

<div class="row" ><div class="col-md-7">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Pengeluaran Barang</p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshpengeluaran"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <canvas id="bar-pengeluaran" width="350" height="250"></canvas>
      </div>
   </div>
</div></div>

<div class="row" ><div class="col-md-7">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Time To MR</p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshtimemr"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <canvas id="bar-time-mr" width="350" height="250"></canvas>
      </div>
   </div>
</div></div>

<div class="row" ><div class="col-md-7">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Time To PO</p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshtimepo"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <canvas id="bar-time-po" width="350" height="250"></canvas>
      </div>
   </div>
</div></div>

</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">
var dataPO;
var labelPO;
var dataMR;
var labelMR;
var datasets=[];
var labels = [];
var colors = [];

$(function(){
   var periodes = {!!$periodes!!};
   var periode = {{$pnow}};
   $('#select2periode').select2({
      data: periodes,
   });
   $('#select2periode').val({{$pnow}}).trigger('change');  
   // console.log('halow');
   inisialisasi();
   function inisialisasi(id=0){
      if(id==0){
         getpo();
         getmr();
         getnilaistok();
         getpengeluaranbarang();
         gettimemr();
         gettimepo();
      }
      else{
         getpo();
         getmr();
         getnilaistok();
         getpengeluaranbarang(); 
         gettimemr();
         gettimepo();
      }
   }
   // getmr();
   // var chartbarpo;
   var tick1 ={beginAtZero:true,
               fontSize: 11,
               max : 100,
               };
   var tick2 ={
      beginAtZero:true,
      fontSize: 11,
      };
   var ctxbarpo = document.getElementById("bar-po").getContext('2d');
   var ctxbarmr = document.getElementById("bar-mr").getContext('2d');
   var ctxbartimemr = document.getElementById("bar-time-mr").getContext('2d');
   var ctxbartimepo = document.getElementById("bar-time-po").getContext('2d');
   var ctxbarnilaistok = document.getElementById("bar-nilai-stok").getContext('2d');
   var ctxbarpengeluaran = document.getElementById("bar-pengeluaran").getContext('2d');
   var chartbarpo = initializebar(labels,datasets,colors,ctxbarpo,'Persentase PO',tick1);
   var chartbarmr = initializebar(labels,datasets,colors,ctxbarmr,'Persentase MR',tick1);
   var chartbarnilaistok = initializebar2(ctxbarmr,datasets);
   var chartbarpengeluaran = initializebar2(ctxbarmr,datasets);
   var chartbartimemr = initializebar(labels,datasets,colors,ctxbarmr,'Time To MR',tick1);
   var chartbartimepo = initializebar(labels,datasets,colors,ctxbarmr,'Time To PO',tick1);
   function initializebar(labels,data,colors,ctxku,label,tixku){
      return new Chart(ctxku, {
         type: 'horizontalBar',
         responsive: true,
         // barValueSpacing: 200,
         data: {
            labels: labels,
            datasets: 
            [{
               label: label,
               data: data,
               backgroundColor: colors,
               borderColor: colors,
                  borderWidth: 1
            }]
         },
         options: {
            scales: {
               xAxes: [{
                  ticks: tixku,
                  gridLines: {
                       display: true,
                       drawBorder: true,
                       drawOnChartArea: false,
                   },
                  /*callback: function(value, index, values) {
                     if(parseInt(value) >= 1000){
                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                     } else {
                        return '$' + value;
                     }
                  }*/
               }],
               yAxes:[{
                  ticks:{
                     fontSize: 11,
                  }
               }]
            },
         }
      });
   }

   function initializebar2(ctxku,data){ //bar vertical
      return new Chart(ctxku, {
          type: 'bar',
          data: data,
          options: {
            tooltips: {
               mode: 'label',
               label: 'mylabel',
               callbacks: {
                  label: function(tooltipItem, data) {
                     return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); }, },
            },
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true,
                     userCallback: function(value, index, values) {
                        if(value >=1000000000){
                           value = value/1000000000 +' M';
                           return value;
                        }
                        if(value >=1000000){
                           value = value/1000000 +' jt';
                           return value;
                        }
                        if(value >= 1000){
                           value = value/1000 +' K';
                           return value;
                        }
                     }  
                  },
                  gridLines: {
                       display: true,
                       drawBorder: true,
                       drawOnChartArea: false,
                   },
                }],
                xAxes:[{
                  // categoryPercentage: 1.0,
                  barPercentage: 1
                }]
             }
          }
       });
   }

   function chartRemoveData(chart) {
      // chart.data.labels.pop();
      panjang = chart.data.labels.length;
      for(i=0;i<panjang;i++){
         chart.data.labels.pop();
      }
      panjang2 =chart.data.datasets[0].data.length;
      for(i=0;i<panjang2;i++){
         // console.log(chart.data.datasets[0].data);
         chart.data.datasets[0].data.pop();
      }
      // chart.data.datasets.pop();
      // chart.data.datasets[0].forEach((dataset) => {
      //    dataset.data.pop();
      // });
      chart.update();
   }
   function chartAddData(chart, label, data) {
      chart.data.labels.push(label);
      chart.data.datasets.forEach((dataset) => {
         dataset.data.push(data);
      });
      chart.update();
   }

   function getpo(){
      // console.log('tes');
      $.ajax({
         url : "{{route('laporan.dashboardadmingetpo')}}",
         // data: "{{$periodesekarang}}"
      }).done(function(response){
         // console.log(response);
         if(response.data.length>0){
            chartbarpo.destroy();
            ctxbarpo.canvas.height = response.data.length*28;
            chartbarpo = initializebar(response.labels,response.data,response.colors,ctxbarpo,'Persentase PO',tick1);
         }
         else{
            console.log('Tidak ada data PO');
         }
         /*chartRemoveData(chartbarpo);
         for (var i = 0; i < response.label.length; i++) {
            chartAddData(chartbarpo,response.label[i],response.data[i]);
         }*/
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function getmr(){
      $.ajax({
         url : "{{route('laporan.dashboardadmingetmr')}}",
         // data: "{{$periodesekarang}}"
      }).done(function(response){
         if(response.data.length>0){
            chartbarmr.destroy();
            ctxbarmr.canvas.height = response.data.length*28;
            chartbarmr = initializebar(response.labels,response.data,response.colors,ctxbarmr,'Persentase MR',tick1);
         }
         else{
            console.log('Tidak ada data MR');
         }
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function getnilaistok(){
      $.ajax({
         url : "{{route('laporan.dashboardadmingetnilaistok')}}",
         data:{
            'gudang' : [1,
                        // 5, //=>cacat
                        6],
         }
      }).done(function(response){
         // console.log(response);
         if(response.labels.length>0){
            chartbarnilaistok.destroy();
            chartbarnilaistok = initializebar2(ctxbarnilaistok,response);
         }
         else{
            console.log('tidak mendapat data');
         }
         // ctxbarmr.canvas.height = response.data.length*20;
         // chartbarmr = initializebar(response.labels,response.data,response.colors,ctxbarmr,'Persentase MR');
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function getpengeluaranbarang(){
      $.ajax({
         url : "{{route('laporan.dashboardadmingetpengeluaranbarang')}}",
         data:{
            'periode' : periode,
         }
      }).done(function(response){
         // console.log(response);
         if(response.labels.length>0){
            chartbarpengeluaran.destroy();
            chartbarpengeluaran = initializebar2(ctxbarpengeluaran,response);
         }
         else{
            console.log('tidak mendapat data');
         }
         // ctxbarmr.canvas.height = response.data.length*20;
         // chartbarmr = initializebar(response.labels,response.data,response.colors,ctxbarmr,'Persentase MR');
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function gettimemr(){
      $.ajax({
         url : "{{route('laporan.timetofullmr')}}",
         data: {
            'periode' : periode
         }
      }).done(function(response){
         if(response.data.length>0){
            chartbartimemr.destroy();
            ctxbartimemr.canvas.height = response.data.length*28;
            chartbartimemr = initializebar(response.labels,response.data,response.colors,ctxbartimemr,'Time To MR',tick2);
         }
         else{
            console.log('Tidak ada data MR');
         }
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function gettimepo(){
      $.ajax({
         url : "{{route('laporan.timetopo')}}",
         data: {
            'periode' : periode
         }
      }).done(function(response){
         if(response.data.length>0){
            chartbartimepo.destroy();
            ctxbartimepo.canvas.height = response.data.length*28;
            chartbartimepo = initializebar(response.labels,response.data,response.colors,ctxbartimepo,'Time To PO',tick2);
         }
         else{
            console.log('Tidak ada data PO');
         }
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   $(document).on('click','#refreshpo',function(){
      getpo();
   });
   $(document).on('click','#refreshmr',function(){
      getmr();
   });
   $(document).on('click','#refreshstok',function(){
      getnilaistok();
   });
   $(document).on('click','#refreshpengeluaran',function(){
      getpengeluaranbarang();
   });
   $(document).on('click','#refreshtimepo',function(){
      gettimepo();
   });
   $(document).on('click','#refreshtimemr',function(){
      gettimemr();
   });

   $(document).on('click','#btnubahperiode',function(){
      periode = $('#select2periode').val();
      inisialisasi();
   });
});
</script>
@endpush
