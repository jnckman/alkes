@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> Pilih Department
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li class="active">List Department</li>
@endsection

@section('content')

   <div class="container-fluid">
     <div class="row">
       <div class="box box-default">
         <div class="box-body">
         <div class="col-md-12">
           <div class="col-md-4" style="padding: 0px;border-top-left-radius: 14px;border-bottom-left-radius: 14px;height: 40px;border-left: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
             <span class="col-md-12" style="color:grey;font-weight:bolder;background-color: white;height: 100%;border-top-left-radius: 10px;border-bottom-left-radius: 10px;" >Memilih Periode MR</span>
           </div>
           <div class="col-md-4" style="padding: 0px;height: 40px;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
             <span class="col-md-12" style="color:grey;background-color: white;height: 100%;" >Purchase Request</span>
           </div>
           <div class="col-md-4" style="padding: 0px;border-top-right-radius: 14px;border-bottom-right-radius: 14px;height: 40px;border-right: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
             <span class="col-md-12" style="color:grey;background-color: white;height: 100%;border-top-right-radius: 10px;border-bottom-right-radius: 10px;" >Purchase Order</span>
           </div>
         </div>
         <div class="col-md-12">
           <h5>Periode MR : {{$periode->judul}} | Tanggal Periode : {{$periode->tglmulai}} | {{$jumlahmrpr}}/{{$jumlahmr}} MR  Sudah PR | {{$jumlahmrapprove}}/{{$jumlahmr}} MR Sudah di Approve</h5>
         </div>

           <div class="col-md-2 ">
            <a href="../rekapalldept/{{$id}}"><button class="btn btn-info">Lihat Barang Semua MR</button></a>
          </div>
          @if($jumlahmrpr == 0)
          <div class="col-md-2 ">
            <a href="../listdepartmentsall/{{$id}}"><button class="btn btn-info">Mulai Rekap</button></a>
          </div>
          @else
          <div class="col-md-2 ">
            <a href="../listdepartmentsall/{{$id}}"><button class="btn btn-warning">Lanjutkan Rekap</button></a>
          </div>
          @endif
          <div class="col-md-6 col-md-offset-2">
            <!-- <div class="col-md-4" style="text-align: right;">
              <h4><b>Filter Tampilan : </b></h4>
            </div>
            <div class="col-md-3">
              <a href="../listdepartmentsall/{{$id}}"><button class="btn btn-warning ">Lihat Semua MR</button></a>
            </div>
            <div class="col-md-4">
              <a href="../listdepartments/{{$id}}"><button class="btn btn-warning ">Lihat Per Department</button></a>
            </div> -->
          </div>

         </div>
       </div>
     </div>
   </div>
   <div class="container-fluid">
     <div class="row" >

      <div class="box box-default" style="padding-top: 20px;">

      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>

                     <th>Nama Department</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>
@endsection

@push('bottom')
<script type="text/javascript">
  $(document).ready(function() {

          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../listdepartmentsdtajax/{{$id}}',

          } );


      } );
</script>
@endpush
