@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
</style>
@endpush

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id='table_master'>
                  <thead>
                     <tr>
                        <th>Kode</th>
                        <th>Nama</th>


                        <!-- <th>Jenis</th> -->
                        
                        <th>Satuan</th>
                        <th>Kategori</th>
                        <th>HPP</th>
                        <th>Min</th>
                        <th>Max</th>
                        <th>Akum Stok</th>
                        <!-- <th>Tipe</th> -->
                        <th>Status</th>
                        <th>Harga Beli Akhir</th>
                        
                        <th>Batch / Lot</th>
                        <th>Expired</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tfoot>
                     <th colspan="5"><p class="text-rght">Total nilai Barang :</p></th>
                     <th colspan="5"></th>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modalPriceList" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Price List</h4>
         </div>
         <div class="modal-body">
            <div class="box">
               <table id="tablePricelist" class='table table-striped table-bordered'>
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Supplier</th>
                        <th>Harga</th>
                     </tr>
                     <tr id="thMerah" style="display:none" class="danger"><th colspan="3" class="text-center">Tidak Ada Data</th></tr>
                  </thead>
                  <tbody id="tbodyPricelist">
                  </tbody>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div id="modalAddPriceList" class="modal fade" role="dialog">
   <div class="modal-dialog">
   <form id="form_addPricelist" method="post" action="{{ route('add.pricelist.barang') }}">
      {{ csrf_field() }}
      <input type="hidden" id="id_barang" name="id_barang" value="">
      <input type="hidden" id="nama_supplier" name="nama_supplier" value="">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Add Pricelist Barang</h4>
         </div>
         <div class="modal-body">
            <div class="form-horizontal">
               <div class="form-group">
                  <label class="control-label col-sm-3" for="kode">Supplier</label>
                  <div class="col-md-8">
                     <select class="form-control selectpicker" name="supplier" id="select_supplier" data-live-search="true" title="Pilih Supplier" required>
                        @foreach($suppliers as $sup)
                        <option value="{{ $sup->id }}">{{ $sup->nama }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-3" for="harga.p">Harga</label>
                  <div class="col-sm-8"><input type="number" class="form-control" id="harga.p" name="harga.p" required></div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <!-- <button type="button" id="print" class="btn btn-success"><i class="fa fa-print"></i> Cetak</button> -->
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </form>
   </div>
</div>

<div id="modal_trackrecord" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h5 class="modal-title">Track Record</h5>
           <h4 id="title-kode">Track Record</h5>
           <h4 class="modal-title"></h4>
           <hr>
           <p><b>Akumstok :</b> <span id="track_akumstok"></span></p>
           <p><b>Stok awal :</b> <span id="track_stokawal"></span></p>
         </div>
         <div class="modal-body">
            <div class="box">
               <table id="table_trackrecord_total" class="table">
                  <thead>
                     <tr>
                        <th>Jenis</th>
                        <th>total Kirim/Datang</th>
                        <th>total SO/PO</th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
               <hr>
               <table id="table_trackrecord" class='table table-striped table-bordered' style="width: 100%" ">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Masuk/Keluar</th>
                        <th>NoGerak</th>
                        <th>jumlah SO/PO</th>
                        <th>jumlah Datang/Keluar</th>
                        <th>Stok Berjalan</th>
                     </tr>
                     <tr class="thMerah" style="display:none" class="danger"><th colspan="3" class="text-center">Tidak Ada Data</th></tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.datatables.bootstrap.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.handlebars.js') }}"></script>
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
   $(function() { //NOTE sama kaya document.ready
      var row;
      table = $('#table_master').DataTable({
   		processing: true,
   		serverSide: true,
   		ajax: "{{ route('get.dt.master') }}",
   		columns: [
   			{data: 'kode', name: 'kode'},
   			{data: 'nama', name: 'nama'},
   			// {data: 'jenis', name: 'jenis'},
            {data: 'namaSatuan', name: 'namaSatuan'},
            {data: 'namaKategori', name: 'namaKategori'},
            {data: 'avgprice', name: 'avgprice', render: $.fn.dataTable.render.number(',')},
            // {data: 'ukuran', name: 'ukuran'},
            {data: 'min', name: 'min'},
            {data: 'max', name: 'max'},
            {data: 'akumstok', name: 'akumstok'},
            // {data: 'tipe', name: 'tipe'},
            //{data: 'ppn', name: 'ppn'},
            {data: 'status', name: 'status'},
            {data: 'harga_beli_akhir', name: 'harga_beli_akhir',render: $.fn.dataTable.render.number(',')},
            
            {data: 'batch', name: 'batch'},
            {data: 'expired', name: 'expired'},
			   {data: 'action', name: 'action', orderable: false, searchable: false}
   		],
         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            total = table.row(0).data().gegewepe;
            // Update footer
            $( api.column( 6 ).footer() ).html(
                'Rp '+ total
            );
        },

   		lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ]
	   });

   });

   function addPriceBarang(id) {
      $('#id_barang').val(id);
      $('#modalAddPriceList').modal('show');
   }

   function numberWithCommas(x) {
       return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
   }
   function showModalPriceList(id_barang) {
      $("#tbodyPricelist").empty(); //NOTE kosongin tbody di modalnya
      //var route = $('#routeGetPriceList').val();
      $.ajax({
         url: "{{ route('getPricelist') }}",
         type: "GET",
         data: {id: id_barang},
         success:function(data){
            if(data[0]){ //cek data ada atau tidak
               // console.log(data);
               if($("#thMerah").show()) {
   					$("#thMerah").hide();
   				}
               $("#print").show();
               var table = $("#tablePricelist");
               $.each(data, function(no, el){
   			      table.append("<tr><td>"+(no+1)+"</td><td>"+el.nama+"</td><td>"+numberWithCommas(el.harga)+"</td></tr>");
   		    	});
            }
            else {
               // console.log("Tidak ada data");
               $("#thMerah").show();
               $("#print").hide();
            }
         }
      });
      $('#modalPriceList').modal('show');
   }

   function trackrecord(id){
      datarow = table.row($(row).parent().parent()).data();
      // console.log(data);
      modal = $("#modal_trackrecord");
      modal.find('h4.modal-title').html(datarow['nama']);
      modal.find('#track_akumstok').html(datarow['akumstok']);
      modal.find('#title-kode').html(datarow['kode']);
      modal.find("tbody").empty();
      $.ajax({
         url: "{{ route('barangs.trackrecord') }}",
         type: "GET",
         data: {"id": id},
         success:function(data){
            console.log(data);
            if(data[0]){ //cek data ada atau tidak
               // console.log(data);
               if(modal.find(".thMerah").show()) {
                  modal.find(".thMerah").hide();
               }
               var table = $("#table_trackrecord");
               var stokjalan = parseInt(datarow['akumstok']);
               $.each(data[0].hasil, function(no, el){
                  no = parseInt(no);
                  if(el.jenis == "KELUAR"){
                     stokjalan = stokjalan + parseInt(el.jumlah2);
                  }
                  else{
                     stokjalan = stokjalan - parseInt(el.jumlah2);  
                  }
                  // if(el.jumlah2 != el.jumlah1){
                  //    table.append('<tr style="border: 5px solid red;"><td>'+(no+1)+"</td><td>"+el.tanggal+"</td><td>"+el.jenis+"</td><td>"+el.nomor+"</td><td>"+el.tjumlah1+"</td><td>"+el.jumlah2+"</td><td>"+stokjalan+"</td></tr>");
                  // }else{
                     table.append("<tr><td>"+(no+1)+"</td><td>"+el.tanggal+"</td><td>"+el.jenis+"</td><td>"+el.nomor+"</td><td>"+el.tjumlah1+"</td><td>"+el.jumlah2+"</td><td>"+stokjalan+"</td></tr>");
                  // }
                  
               });
               table2 = $('#table_trackrecord_total');
               table2.append('<tr><td>Keluar</td><td>'+data[0].tk.jumlah2+'</td><td>'+data[0].tk.jumlah1+'</td>');
               table2.append('<tr><td>Datang</td><td>'+data[0].td.jumlah2+'</td><td>'+data[0].td.jumlah1+'</td>');
               stokawal = datarow['akumstok'] - data[0].td.jumlah2+data[0].tk.jumlah2;
               modal.find('#track_stokawal').html(stokawal);
            }
            else {
               modal.find(".thMerah").show();
            }
         }
      });
      modal.modal("show");
   }

   function deleteBarang(id) {
      swal({
         title: 'Are you sure?',
         text: "You won't be able to revert this!",
         type: 'warning',
         showCancelButton: true,
         allowOutsideClick:true,
         confirmButtonText: 'Yes!',
         confirmButtonColor: '#DD6B55',
         closeOnConfirm: false
      },function() {
         var url = '{{ CRUDBooster::adminPath("barangs") }}/delete_barang/'+id;
         window.location.href = url;
      });
   }

   $("#select_supplier").change(function() {
      var nama = $('#select_supplier option:selected').text();
      $("#nama_supplier").val(nama);
   });


</script>
@endpush
