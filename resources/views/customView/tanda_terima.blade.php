<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Surat Jalan</title>
      <link href="{{ asset('customs/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('customs/css/bootstrap_render_print.css') }}" rel="stylesheet" type="text/css" />
      <style type="text/css">
         @page {
            size: A3;
             margin: 1cm 2cm;

             @top-center {
                 content: element(pageHeader);
             }

             @bottom-center {
                 content: element(pageFooter);
             }
         }

         #pageHeader{
             position: running(pageHeader);
         }

         #pageFooter{
             position: running(pageFooter);
         }
         @media print {
             footer {page-break-after: always;}
         }
      </style>
   </head>
   @foreach($data['kirim_barang'] as $mr_subdept)
   
      <body>
         <div class="row" style="padding: 20px 20px 20px 20px;">
            <div class="col-md-12">
               <div class="col-md-12">
                  <img src="{{ asset('gambar/logo.png') }}" style="width: 150px;">
               </div>
               <div style="width: 350px;">
                  <h5><strong>PT. TATA KARYA GEMILANG</strong></h5>
                  <h6>Jl. Sukun Mataram Bumi Sejahtera No.3, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281</h6>
                  <h6>0878-3986-3930</h6>
               </div>
               <div>
                  <h6 class="text-right"><strong>TANDA TERIMA</strong></h6>
                  <h6 class="text-right"><strong>{{ $mr_subdept[0]->area }}/{{ $mr_subdept[0]->no_nota }} / {{ $data['tanggal'] }}</strong></h6>
               </div>
            </div>
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id='table_master'>
                  <thead>
                     <tr class="success">
                        <th>NO</th>
                        <th>Barang</th>
                        <th>Jumlah</th>
                        <th>Satuan</th>
                        <th>Kekurangan</th>
                        <th>Keterangan</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i=1; ?>
                     @foreach($mr_subdept->sortBy('nama') as $m)
                     <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $m->nama }}</td>
                        <td>{{ $m->jumlah }}</td>
                        <td>{{ $m->satuan }}</td>
                        <td>{{ $m->jumlahmr - $m->jumlah }}</td>
                        @if(isset($m->keterangan))
                        <td>{{ $m->keterangan }}</td>
                        @else
                        <td></td>
                        @endif
                     </tr>
                     <?php $i++; ?>
                     @endforeach
                  </tbody>
               </table>
            </div>
            <div class="col-md-12">
               <div class="col-md-3">
                  <br/><br/>
                  <p class="pull-left">(________________)</p>
                  <p class="pull-right">(________________)</p>
               </div>
            </div>
         </div>
      </body>
      <footer></footer>
   @endforeach
</html>

<script src="{{ asset('customs/js/bootstrap.min.js')}}"></script>
