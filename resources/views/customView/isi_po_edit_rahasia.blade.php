@extends('crudbooster::admin_template')

@push('head')
@endpush
@section('content')
<div class="box">
<div class="box-body table-responsive">
<h3>{{$po[0]->nama}}</h3>
<form action="{{route('po.editrahasia')}}" method="post">
	{{csrf_field()}}
	ID PO : <input type="number" readonly name="id_po" value="{{$po[0]->id}}">
	<table class="table table-hover table-striped table-bordered">
		<thead>
		<tr>         
	         <th>id</th>
	         <th>nama barang</th>
	         <th>satuan</th>
	         <th>jumlah</th>
	         <th>jumlah baru</th>
	         <th>harga</th>
	         <th>total</th>
	     </tr>
	     </thead>
	     <tbody>
	     	@foreach($isi_pos as $key => $isi)
	     	<tr>
	     		<td><input type="" readonly name="id[]" value="{{$isi->id}}" /></td>
	     		<td>{{$isi->nama}}</td>
	     		<td>{{$isi->satuan}}</td>
	     		<td>{{$isi->jumlah}}</td>
	     		<td><input type="number" placeholder="0" name="jumlahbaru[]" min='0'/></td>
	     		<td><input type='number' name="harga[]" value="{{$isi->harga}}" /></td>
	     		<td><input type='number' name="total_akhir[]" value="{{$isi->total_akhir}}" class="total_isi" /></td>
	     	</tr>
	     	@endforeach
	     </tbody>
	</table>
	<button type="submit">Submit Edit</button>
	<input type="number" readonly name="po_total_akhir" value="{{$po[0]->total_akhir}}">
</form>
</div>
</div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function(){
	$(document).on('input','input[name="jumlahbaru[]"]',function(){
		// alert($(this).val());
		jumlahbaru = $(this).val();
		if(jumlahbaru==''){
			jumlahbaru = $(this).parent().prev().html();
		}
		totalbaru = $(this).parent().parent().find('input[name="harga[]"]').val()*jumlahbaru;
		$(this).parent().parent().find('input[name="total_akhir[]"]').val(totalbaru);
		hitungtotal();
	});

	function hitungtotal(){
		totalku = 0;
		$('.total_isi').each(function(e){
			totalku += parseInt($(this).val());
		});
		$('input[name="po_total_akhir"]').val(totalku);
	}
});
</script>
@endpush
