@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-cart-plus" aria-hidden="true"></i> Susun Purchase Order
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/lihatpembukuanrekap/{{ $id }}">Purchase Request</a></li>
   <li class="active">Susun PO</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <div class="col-md-4" style="padding: 0px;border-top-left-radius: 14px;border-bottom-left-radius: 14px;height: 40px;border-left: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
            <span class="col-md-12" style="color:white;font-weight:bolder;background-color: #5fa6ff;height: 100%;border-top-left-radius: 10px;border-bottom-left-radius: 10px;" >Memilih Periode MR</span>
          </div>
          <div class="col-md-4" style="padding: 0px;height: 40px;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
            <span class="col-md-12" style="color:white;background-color: #5fa6ff;height: 100%;font-weight:bolder;" >Lakukan Rekap</span>
          </div>
          <div class="col-md-4" style="padding: 0px;border-top-right-radius: 14px;border-bottom-right-radius: 14px;height: 40px;border-right: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
            <span class="col-md-12" style="color:white;font-weight:bolder;background-color: #5fa6ff;height: 100%;border-top-right-radius: 10px;border-bottom-right-radius: 10px;" >Purchase Order</span>
          </div>
        </div>
        <div class="col-md-12">
          <h4>Daftar Supplier</h4>
        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <!-- <a href="../rekapallsubdeptmr/{{$id}}"><button class="btn btn-info">Rekap seluruh Sub Departments</button></a> -->
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>

                     <th>nama supplier</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" style="height: 200px;">
                 <div class="col-md-12" style='margin-top: 10px;'>
                   <div class="col-md-4" style="padding-left: 0px;">
                     Total Pengeluaran :
                   </div>
                   <div class="col-md-8 totalpl">

                   </div>
                 </div>
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">

                   </div>
                 </div> -->
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>
                            <th>
                                Nama Barang
                            </th>
                            <th>
                               Jumlah
                            </th>
                            <th>
                              Harga satuan
                            </th>
                            <th>
                              Total
                            </th>
                          </thead>
                          <tbody class='tabelisibarang'>

                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   <div class="modal fade" id="buatpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <form action='submitpo' method='POST'>
             <div class="modal-body" >
                 <div class="col-md-12" style='margin-top: 10px;'>
                   <div class="col-md-4" style="padding-left: 0px;">
                     FORM PO
                   </div>
                   <div class="col-md-8 totalpl">

                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>No PO</label>
                     <input type="text" name="nopo" class="form-control input-sm" value="PO-{{date('md')}}-{{$last}}" id='nopoform' readonly="">
                   </div>
                 </div>
                 <!-- <div class="col-md-6">
                   <div class="form-group">
                     <label>Tanggal PO</label>
                     <input type="date" name="tglpo" class="form-control input-sm">
                   </div>
                 </div> -->
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Jenis PO</label>
                     <select name='jenispo' class="form-control input-sm">
                       <option value="1">Tempo</option>
                       <option value="2">Tunai</option>
                     </select>


                   </div>
                 </div>
                 
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>Kepada</label>
                     <input type="text" name="customers_id" class="form-control input-sm" id="kepada" readonly>
                   </div>
                 </div>
                 
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Tempo</label>
                     <input type="text" name="hari" class="form-control input-sm" placeholder="lama tempo">
                   </div>
                 </div>

                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Ongkir</label>
                     <input type="text" name="ongkir" class="form-control" placeholder="ongkir pengiriman" value="0">
                   </div>
                 </div>
                 <div class="col-md-12">
                 <input type="hidden" name="periode_mrs_id" class="form-control" value="" id="periodemr">
                   <table class="table table-bordered">
                     <thead>
                       <th>
                           Nama Barang
                       </th>
                       <th>
                          Jumlah - satuan
                       </th>
                       <th>
                         Harga satuan
                       </th>
                       <th>
                         Total
                       </th>
                       <th>
                         Diskon
                       </th>
                       <th>
                         Harga Akhir
                       </th>

                     </thead>
                     <tbody class='tabelisibarang'>

                     </tbody>
                   </table>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>Diskon Total</label>
                     <input type="text" name="diskontotal" class="form-control input-sm diskontotal" placeholder="diskon total" value="0">
                   </div>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>PPN(%)</label>
                     <input type="text" name="ppn" class="form-control input-sm">
                   </div>
                 </div>
                 
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>Total</label>
                     <input type="text" name="totalakhir" readonly class="form-control totalplinput" placeholder="total">
                   </div>
                 </div>
                 <div class="col-md-12">
                  {{csrf_field()}}
                   <button type='submit' class="btn btn-success">SUBMIT PO</button>

                 </div>
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">

                   </div>
                 </div> -->

             </div>
             </form>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
    </div>
@endsection

@section('datatable')
<script type="text/javascript">
  $(document).ready(function() {

          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../../susunpurchaseorderdtajax/{{$id}}/{{$kode}}',

          } );
          var tabel;
          var isitabel='';
          var currentidsup;
          var currentattr = $('#nopoform').attr('value');
          var total = 0;
          var totalsetelahdiskon = 0;
          var lastpickidsup='';
          var totalsemua = 0;
          $('body').on('click', '.buatform', function(event) {
            event.preventDefault();
            currentidsup = $(this).attr('id');
            currentidsup
            $('#nopoform').attr('value',currentattr + currentidsup);
            //alert(currentidsup + ' ' + lastpickidsup);
            if((isitabel=='' || lastpickidsup=='' || lastpickidsup != currentidsup) || (lastpickidsup == currentidsup && isitabel !='')){



              $.ajax({

                          type: 'get',
                          url: '../../ambilbarangpo/'+currentidsup+'/{{$id}}/po/{{$kode}}',
                          data: {},
                          dataType: 'json',
                          success: function (data) {
                            //alert(data['jenis']);
                            if(data['jenis']=='kosong'){
                                if(data!=''){
                                  total = 0;
                                  isitabel = '';
                                  $('.tabelisibarang').html('');
                                  $.each(data['data'],function(index,d){

                                    isitabel='<tr><td>'+d.nama+'<input type="hidden" value="'+d.barangid+'" name="barangsid[]"><input type="hidden" value="'+d.jumlah+'" name="jumlah[]"><input type="hidden" value="'+d.supid+'" name="pricelistsid[]"></td><td>'+d.jumlah+' - '+d.satuan+'</td><td>'+d.harga+'</td><td>'+(d.jumlah*d.harga)+'</td><input type="hidden" value="'+d.harga+'" name="hargaid[]"><td><input class="form-control inputdiskon input-sm" name="diskonbarang[]" placeholder="diskonbarang" hargatotal="'+(d.jumlah*d.harga)+'" barangid='+d.barangid+'></td><td><input class="form-control input-sm diskonperrow totalakhirrow'+d.barangid+' " readonly placeholder="total" value="'+(d.jumlah*d.harga)+'" name="diskonperrow"></td></tr>'+isitabel;
                                    total = d.harga*d.jumlah + total;
                                    $('#kepada').val(d.supnama);
                                    $('#periodemr').val(d.periode_mrs_id);
                                  });
                                  
                                  $('.totalpl').html(total);
                                  $('.totalplinput').val(total);
                                  $('.tabelisibarang').html(isitabel);
                                  

                                  // $('#pilihanharga'+rekapid).html('');
                                  // $('#pilihanharga'+rekapid).html(data);

                                }
                                $('#buatpo').modal('show');
                            }
                            else{
                              alert('Maaf PO Sudah Dibuat, Cek Pada Menu PO');
                            }


                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
            }
            else{
              alert(isitabel);
            }

          });

          $('body').on('click', '.lihatisi', function(event) {
            event.preventDefault();

            currentidsup = $(this).attr('id');
            lastpickidsup = currentidsup;

            $.ajax({

                        type: 'get',
                        url: '../../ambilbarangpo/'+currentidsup+'/{{$id}}/isi',
                        data: {},
                        dataType: 'json',
                        success: function (data) {


                                if(data!=''){
                                  total = 0;
                                  isitabel = '';

                                  $('.tabelisibarang').html('');
                                  $.each(data['data'],function(index,d){
                                    isitabel='<tr><td>'+d.nama+'<input type="hidden" value="'+d.barangid+'" name="barangsid[]"><input type="hidden" value="'+d.jumlah+'" name="jumlah[]"><input type="hidden" value="'+d.supid+'" name="pricelistsid[]"></td><td>'+d.jumlah+'</td><td>'+d.harga+'</td><td>'+(d.jumlah*d.harga)+'</td><input type="hidden" value="'+d.harga+'" name="hargaid[]"></tr>'+isitabel;
                                    total = d.harga*d.jumlah + total;

                                    $('#kepada').val(d.supnama);
                                    $('#periodemr').val(d.periode_mrs_id);
                                  });
                                  $('.totalpl').html(total);
                                  
                                  $('.tabelisibarang').html(
                                    isitabel


                                  )

                                  // $('#pilihanharga'+rekapid).html('');
                                  // $('#pilihanharga'+rekapid).html(data);

                                }
                                $('#myModal').modal('show');



                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
          });

          var harga;
          var diskon;
          
          $(document).on("input",".inputdiskon", function(e){
            var tampung = $(this).val();
            if(tampung.indexOf('%') >= 0){
              $tampungconvert = tampung.substr(0, tampung.indexOf('%')); 
              harga = $(this).attr('hargatotal') -  ($tampungconvert * $(this).attr('hargatotal')/100);
            }
            else{
              harga = $(this).attr('hargatotal') - $(this).val();
            }
            var barangid = $(this).attr('barangid');
            
            //totalsetelahdiskon = totalsetelahdiskon + harga;
            
            //totalsetelahdiskon = totalsetelahdiskon - harga;
            
            $('.totalakhirrow'+barangid).val(harga);
          });

          $(document).on("input",".diskontotal", function(e){
            var tampung = 0;
            totalsetelahdiskon = $('.totalplinput').val();
            $('.diskonperrow').each(function () { 
              tampung = tampung +  (1*$(this).val());
              
            });
            //alert();
            $('.totalplinput').val((tampung/2) - $(this).val());
          });


      } );
</script>
@endsection
