@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-close" aria-hidden="true"></i> Pending MR
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/lihatpembukuanrekap/{{ $id }}">Purchase Request</a></li>
   <li class="active">Susun PO</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        
        <div class="col-md-12">
          <h4>Daftar SO Belum dikirim</h4>
        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <!-- <a href="../rekapallsubdeptmr/{{$id}}"><button class="btn btn-info">Rekap seluruh Sub Departments</button></a> -->
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>

                     <th>Nomor SO</th>
                     <th>Customer</th>
                     <th>Action</th>
                 </tr>
          </thead>
          <tbody>
          	@foreach ($data as $d)
          	<tr>
	          @foreach ($d->isi_sales_order as $i) 
	            @if($i['dikirim']<$i['jumlah'])
	            	<td>{{$d->nomor}}</td>
	            	<td>{{$d->customer->nama}}</td>
	            	<td><button class="btn btn-info btn-xs lihatmr" id="{{$d->id}}">Lihat SO</button></td>
	            	@break
	              
	            @endif
	          @endforeach
	        </tr>
			@endforeach
			
          </tbody>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" >
                 
                 
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>
                            <th>
                                Nama Barang
                            </th>
                            <th>
                               Jumlah SO
                            </th>
                            <th>
                              Jumlah Dikirim
                            </th>
                          </thead>
                          <tbody class='tabelisibarang'>

                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>   
   
@endsection

@section('datatable')

@endsection
@push('bottom')
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.lihatmr').click(function(event) {
				idmr = $(this).attr('id');
				$.ajax({

                        type: 'get',
                        url: 'lihatisipendingso/'+idmr,
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                              console.log(data);

                                
                                  
                                  isitabel = '';

                                  $('.tabelisibarang').html('');
                                  $.each(data,function(index,d){
                                    if(d.jumlah > d.dikirim)
                                    isitabel='<tr class="tes"><td>'+d.nama+'"</td><td>'+d.jumlah+'</td><td>'+d.dikirim+'</td></tr>'+isitabel;
                                    else
                                    isitabel='<tr class=""><td>'+d.nama+'"</td><td>'+d.jumlah+'</td><td>'+d.dikirim+'</td></tr>'+isitabel;
                                    
                                  });
                                  
                                  
                                  $('.tabelisibarang').html(
                                    isitabel


                                  )

                                  // $('#pilihanharga'+rekapid).html('');
                                  // $('#pilihanharga'+rekapid).html(data);

                                
                                 $('#myModal').modal('show');



                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
          });
			});
		
	</script>
  <style type="text/css">
    .tes{
      background: #ffcaca;
    }
  </style>
@endpush
