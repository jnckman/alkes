@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> Daftar Isi P.O
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(purchaseorders)}}">Purchase Order</a></li>
   <li class="active">Isi P.O.</li>
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nopo}} | <i class="fa fa-calendar-o fa-fw" aria-hidden="true"></i> : @if(isset($result[0]->judul)) {{$result[0]->judul}} @else PO Pembelian @endif | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}} | <i class="fa fa-truck fa-fw" aria-hidden="true"></i>  : Rp {{number_format($result[0]->ongkir)}} </h5> 
        </div>
        <div class="col-md-4 col-md-offset-1">
          <a href="../purchaseorders/print/{{$id}}"><button class="btn btn-info">Print Rekap PO</button></a>
          <!-- <a href="../purchaseorders/printapprove/{{$id}}"><button class="btn btn-info">Print Approve PO</button></a> -->
          <a href="../lihatisispbm/{{$id}}"><button class="btn btn-info">List SPBM</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
        <ul class="nav nav-tabs nav-justified">
          <li class="active"><a data-toggle="tab" href="#tab_isi">Isi PO</a></li>
          <li ><a data-toggle="tab" href="#tab_revisi">History Revisi</a></li>
        </ul>
      <div class="tab-content">
      <div id="tab_isi" class="tab-pane fade in active">
        <br/>
        <br/>
        <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                     
                     <th>id</th>
                     <th>nama barang</th>
                     <th>satuan</th>
                     <th>jumlah</th>
                     <th>harga</th>
                     <th>diskon</th>

                     <th>total</th>
                     
                 </tr>
           </thead>
         </table>
        </div>
       </div>
       <div id="tab_revisi" class="tab-pane fade">
          <br/>
          <br/>
          <div class="col-md-12">
            <table class="table table-hover table-striped table-bordered table2" id='table_dashboard' style="width:100%">
              <thead>
                   <tr>
                       <th>ID</th>
                       <th>Tanggal</th>
                       <th>User</th>
                       <th>Barang</th>
                       <th>Harga PO</th>
                       <th>Harga Baru</th>
                       <th>Jumlah PO</th>
                       <th>Jumlah Baru</th>
                       <th>Perubahan Supplier</th>
                       <th>Keterangan</th>
                       <th>Status</th>
                   </tr>
              </thead>
            </table>
          </div>
       </div>
       </div>
       </div>
       </div>
   </div>
 </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" style="height: 200px;">
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>
                           
                            <th>
                               Jumlah
                            </th>
                            
                          </thead>
                          <tbody class='tabelisibarang'>
                            
                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">
                 
                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   
@endsection

@section('datatable')
<script type="text/javascript">
  $(document).ready(function() {
    
          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../isipurchaseorderdtajax/{{$id}}',
              
          } );
          
          var isipo='';
          var jumlahbaru='';
          $('body').on('click', '.editpo', function(event) {
                  isipo = $(this).attr('id');
                  $.ajax({

                        type: 'get',
                        url: '../ambilbarangeditpo/'+isipo+'',
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                            $('.tabelisibarang').html('');
                            var html='';
                            
                              html= '<tr><Td>'+data.jumlah+'</td><Td><input type="text" nme="jumlahbaru" class="form-control inputjumlahbaru" placeholder="jumlah baru"></td><td><button class="btn btn-success simpanjumlahbaru">Save</button></td></tr>';
                            $('.tabelisibarang').html(html);
                            
                            $('#myModal').modal('show');
                            

                              
                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
          });

          $('body').on('click', '.simpanjumlahbaru', function(event) {

                    jumlahbaru = $('.inputjumlahbaru').val();
                          $.ajax({

                                type: 'post',
                                url: '../simpaneditpo',
                                data: {'idpo':isipo,'jumlahbaru':jumlahbaru},
                                dataType: 'json',
                                success: function (data) {
                                    
                                    
                                    $('#myModal').modal('hide');
                                    $('.table1').DataTable().ajax.reload()

                                      
                                  },
                                  error: function (data) {
                                    alert('gagal');
                                      console.log(data);
                                  }
                            });
          });

          $('body').on('click', '.lihatisi', function(event) {
            event.preventDefault();

            currentidsup = $(this).attr('id');
            lastpickidsup = currentidsup;
            
            $.ajax({

                        type: 'get',
                        url: '../ambilbarangpo/'+currentidsup+'/{{$id}}/isi',
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                            
                            
                                if(data!=''){
                                  total = 0;
                                  isitabel = '';
                                  $('.tabelisibarang').html('');
                                  $.each(data['data'],function(index,d){
                                    isitabel='<tr><td>'+d.nama+'<input type="hidden" value="'+d.barangid+'" name="barangsid[]"><input type="hidden" value="'+d.jumlah+'" name="jumlah[]"><input type="hidden" value="'+d.supid+'" name="pricelistsid[]"></td><td>'+d.jumlah+'</td><td>'+d.harga+'</td><td>'+(d.jumlah*d.harga)+'</td></tr>'+isitabel;
                                    total = d.harga*d.jumlah + total;
                                    $('#kepada').val(d.supnama);  
                                    $('#periodemr').val(d.periode_mrs_id);  
                                  });
                                  $('.totalpl').html(total);
                                  $('.tabelisibarang').html(
                                    isitabel


                                  )

                                  // $('#pilihanharga'+rekapid).html('');
                                  // $('#pilihanharga'+rekapid).html(data);
                                  
                                }
                                $('#myModal').modal('show');
                            

                              
                          },
                          error: function (data) {
                            alert('gagal');
                              console.log(data);
                          }
                    });
          });

      $('.table2').DataTable({
              'processing': true,
              'serverside':true,
              "order": [[ 0, "desc" ]],
              'ajax': '{{route("po.isi.getRevisiByPo",$id)}}',
      })

      } );
</script>
@endsection
