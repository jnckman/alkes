@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan IMITF
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Customer Dua Berlian</li>
@endsection

@section('content')
<div class="box box-default">
   <div class="box-header with-border">
   <h4>Last Keyword : {{$input['date']}}</h4>
   <form action="dubercustpost" method='POST'>
      <input type="date" name="date" class="form-control">
      {{csrf_field()}}
      <button type="submit" class="btn btn-info" style="margin-top: 20px;width: 100%;">Lihat</button>
   </form>
   </div>
   <div class="col-md-12">
      <table class="table table-bordered table-responsive">
         <thead>
            <tr>
               <td>
               ITEM_NUM</td><td> ITEM_NAME</td><td>  SUB DIV  </td>
            </tr>
         </thead>
         <tbody>
         
            @foreach($jual as $d)
            <tr>
               
               <td>{{$d->barang->itemnum}}</td>
               <td>{{$d->barang->nama}}</td>
               <td> - </td>
               
            </tr>
            @endforeach
         </tbody>
      </table>
   </div>   
</div>



@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">

@endpush
