@extends('crudbooster::admin_template')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('content')
<p><a title="Return" href="{{CRUDBooster::adminPath('harga_juals/barang/'.$barang->id)}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Data Harga Jual</a></p>
<div class="box box-default" style="padding-top: 20px;">
<div class="box-body">
<form method="post" action="{{route('harga_jual.postaddbybarang')}}">
  {{csrf_field()}}
  <div class="form-group">
    <label>Barang : </label>
    <input type="text" readonly class="form-control" value="{{$barang->nama}}"/>
    <input type="hidden" readonly value="{{$barang->id}}" name="barang_id">
  </div>
  <div class="form-group">
    <label>Harga Jual* : </label>
    <input type="number" class="form-control" min="0" placeholder="0" name="harga_jual" required />
  </div>
  <div class="form-group">
    <label>Keterangan* : </label>
    <select class="form-control" name="ket" id="select2ket">
      <option value="dua_berlian">Dua Berlian</option>
      <option value="gemilang">Gemilang</option>
      <option value="online_shop">Online Shop</option>
    </select>
  </div>
  <div class="form-group">
    <label>Harga Diskon : </label>
    <input type="number" class="form-control" min="0" placeholder="0" name="harga_diskon"/>
  </div>
  <button type="submit" class="btn btn-success">Submit</button>
</form>
</div></div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function(){
	$('#select2ket').select2({
    placeholder:'Pilih ket',
  })
});
</script>
@endpush