@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <div class="col-md-8">
            <span style="font-size: 24px;">- List Retur Barang -</span>
          </div>
          <div class="col-md-2">
            <a href="../lihatisispbm/{{$id}}" class="pull-right">
              <button class="btn btn-success">
                Lihat Flow Retur Barang Masuk
              </button>
            </a>
          </div>
          <div class="col-md-2">
            
              <button class="btn btn-warning buatretur">
                Buat Retur Barang
              </button>
            
          </div>

        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$po[0]->nopo}} | <i class="fa fa-calendar fa-fw" aria-hidden="true"></i> : {{$po[0]->tglpo}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-bordered example1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>Id</th>
                     <th>No Retur</th>
                     <th>Tanggal Buat</th>
                     <th>Total Retur</th>
                     <th>Action</th>
                     
                     
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Buat Nota Retur</h4>
             </div>
             <div class="modal-body" >
                 <form action='../postnewretur' method='POST'>
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <div class="col-md-12 form-group">
                        <label>No Nota Retur</label>
                        <input type="text" name="noretur" class="form-control noretur" readonly="" id="noretur">
                        {{csrf_field()}}
                      </div>
                      <!-- <div class="col-md-12 form-group">
                        <label>SPBM</label>
                        <select class="form-control" required="" name='spbm' id="select2spbm"><option></option></select>
                        
                      </div> -->

                      <!-- <div class="col-md-12 form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" placeholder="isikan keterangan">

                      </div> -->
                      <input type="hidden" name="purchaseorders_id" class="form-control" value="{{$id}}" >
                      <table class="table table-bordered">
                          <thead>
                            <th>No</th>
                            <th>Jumlah PO</th>
                            <th>Retur</th>
                            
                          </thead>
                          <tbody>

                            @foreach($po as $val)
                            <tr>
                              <td>
                                {{$val->nama}}
                              </td>
                              <td>
                                {{$val->jumlahtotal}}
                              </td>
                              <td>
                                <input type="text" name="jumlahretur[]" class="form-control" placeholder="masukan jumlah retur">
                                <input type="hidden" name="total[]" class="form-control" value="{{$val->jumlahtotal}}">
                                
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                      </table>

                 </div>
                 <button  class="btn-success btn buatnotaretur">Buat Surat Retur</button>
                 </form>
             </div>
             <div class="modal-footer">
                 
                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>


@endsection
    
@section('datatable')

<script type="text/javascript">
    $(document).ready(function() {
          $.fn.modal.Constructor.prototype.enforceFocus = function() {};
          var listspbm = <?php echo $listspbm; ?>;
          var d = new Date();

          n = d.getMonth();

          y = d.getFullYear();
          $('#select2spbm').select2({
            data:listspbm,
            placeholder:'Pilih Project'
          });

          $('.example1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../listreturdtajax/{{$id}}',
              
          } );
          // $('.buatnotaretur').click(function(event) {
          //   var noretur = $('#noretur').val();
          //   var idspbm = $('#select2spbm ').val();
          //   $.ajax({

          //         type: 'post',
          //         url: '../postnewretur',
          //         data: {'noretur':noretur,'idspbm':idspbm},
          //         dataType: 'json',
          //         success: function (data) {
                      
                      
          //             $('.noretur').val('RE'+data+n+y);
          //             $('#myModal').modal('show');

                        
          //           },
          //           error: function (data) {
          //             alert('gagal');
          //               console.log(data);
          //           }
          //     });
          // });
          $('.buatretur').click(function(event) {
            $.ajax({

                  type: 'get',
                  url: '../getlastreturid',
                  data: {},
                  dataType: 'json',
                  success: function (data) {
                      
                      
                      $('.noretur').val('RE'+data+n+y);
                      $('#myModal').modal('show');

                        
                    },
                    error: function (data) {
                      alert('gagal');
                        console.log(data);
                    }
              });
            
          });


      } );
</script>
<<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
              <link rel="shortcut icon" href="images/ico/favicon.ico">
              <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
              <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
              <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
              <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
              <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
              <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
              <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<style type="text/css">
  .selected{
    border-bottom: 2px solid grey;

  }
  .select2{
    width: 100% !important; 
  }
</style>
<script src="{{ asset('customs/js/select2.min.js') }}"></script> 
@endsection

