@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
<style>
   .custom:hover {
      background-color: #499349 !important;
      cursor: pointer;
   }
</style>
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Pengiriman Barang Part 4</li>
@endsection

@section('content')
<div class="custom-container">
   <div class="panel panel-default">
      <div class="panel-body">
   		<div class="stepwizard">
            <div class="stepwizard-step">
                <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-crosshairs" aria-hidden="true"></i> Pilih MR</i></a>
            </div>
            <div class="stepwizard-step">
                <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Ambil Barang</a>
            </div>
            <div class="stepwizard-step">
                <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-plus" aria-hidden="true"></i> Bagi Barang</a>
            </div>
            <div class="stepwizard-step">
                <a href="" type="button" class="btn btn-success btn-lg"><i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim Barang</a>
            </div>
         </div>
   	</div>
   </div>
</div>

<div class="custom-container">
   <div class="panel panel-default">
   	<div class="panel-body padding-l0 padding-r0">
         <div class="col-md-2 col-md-offset-10" style="margin-bottom: 10px;">
            <button onclick="finish()" class="btn btn-info btn-block pull-right" type="button" name="button">FINISH</button>
         </div>

         <?php $semua_nota = $detail_data->unique('nota_id')->sortBy('nota_id'); ?>
         <div class="col-md-12 flex-container">
         @foreach($semua_nota as $nota)
            <div class="col-md-4" style="margin-bottom: 10px;">
               <div class="card">
                  <div class="card-header font-s">{{ $nota->no_nota }}</div>
                  <div id="{{ $b->barang_id }}" class="card-main">
                     <table class='table table-striped table-bordered' style="margin-bottom: 0;">
                        <thead>
                           <tr class="info">
                              <th class="th-center font-s2"><strong>Kode</strong></th>
                              <th class="th-center font-s2"><strong>Nama Barang</strong></th>
                              <th class="th-center font-s2"><strong>Jumlah</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($detail_data->sortBy('kode') as $i => $b)
                              @if($b->nota_id == $nota->nota_id)
                                 <tr>
                                    <td class="font-s2" align="center" valign="center">{{ $b->kode }}</td>
                                    <td class="font-s2" align="center" valign="center">{{ $b->nama }}</td>
                                    <td class="font-s2" align="center" valign="center">{{ $b->jumlah }}</td>
                                 </tr>
                                 <?php unset($isi_mr_subdepts[$i]); ?>
                              @endif
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         @endforeach
         </div>
      </div>
   </div>
</div>

@endsection

@push('bottom')
<script type="text/javascript">
   $(function () {
      var barang_habis = "<?php echo $data["barang_habis"]; ?>";

      if(barang_habis == 0){
         console.log("ada sisa barang");
      }

      $('div[data-toggle="collapse"]').click(function(e) {
         e.stopPropagation();
      });

   });

   function finish() {
      swal({
         title: "Proses Pengiriman Berhasil",
         text: "Silakan Cetak Surat Jalan dan Tanda Terima",
         type: "success",
         confirmButtonText: 'OK',
         closeOnConfirm: true
      },
      function(isConfirm) {
         if (isConfirm) {
            window.location.href = "{{ route('index.list.pengiriman.barang') }}";
         }
      });
   }

</script>
@endpush
