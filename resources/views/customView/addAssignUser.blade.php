@extends('crudbooster::admin_template')
@section('content')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
<p><a title="Return" href="{{CRUDBooster::adminPath('assign_users')}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Data Assign_Users</a></p>
<div class="panel panel-default">
    <div class="panel-heading">
     <strong><i class="fa fa-glass"></i> Add Assign_Users</strong>
    </div>
    <form  method="post" id="form" enctype="multipart/form-data" action="{{$ruteku}}">
    <div class="panel-body">
	   	<div class="box-body">
	   		{{csrf_field()}}
	   		@if(isset($idku))
	   		<input type="hidden" name="id" value="{{$idku}}">
	   		@endif
        	<div class="form-group">
        		<label class="col-sm-2 text-right">User Id*</label>
        		<select class="col-sm-10" name="user_id" id="select2user"><option></option></select>
        	</div>
        	<div class="form-group">
        		<label class="col-sm-2 text-right">Data Table*</label>
        		<select class="col-sm-10" name="table_id" id="select2tableid">
        		</select>
        	</div>
	    </div>
    </div>
    <div class="box-footer" style="background: #F5F5F5">
		<div class="form-group">
			<label class="control-label col-sm-2"></label>
			<div class="col-sm-10">
			<a href="http://localhost/wellhos/public/admin/assign_users" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Back</a>
			<input name="submit" value="Save" class="btn btn-success" type="submit">
			</div>
		</div>
	</div>
	</form>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	var users = <?php echo $users;?>;
	var dataku = <?php echo $dataku;?>;
	var table_id = <?php echo $table_id;?>;
	console.log(dataku.id);

	$('#select2user').select2({
		data : users,
		placeholder: 'Pilih User*',
	});
 
	$('#select2tableid').select2({
		data:table_id,
		placeholder:'Pilih User terlebih dahulu'
	});

	$(document).on('change','#select2user',function(e){
		// alert('etogege');
		// console.log($('#select2tablename').val());
		valku = $(this).val();
		url = '{{route("assign.gettableid")}}';
		$.ajax({
  				url : url,
  				data:{
  					'value': valku,
  				}
  			}).done(function(response){
  				// console.log(response);
  				$('#select2tableid').empty().select2({
			    	data:response,
			    	placeholder:'Pilih Data Table'
			    })
			    if(dataku.table_id){
			    	$('#select2tableid').val(dataku.table_id).trigger('change');
			    }
			    else{
			    	$('#select2tableid').val('').trigger('change');
			    }
  			}).fail(function(){
  				swal('Gagal Mengambil Data !', 'Silahkan Coba Lagi', 'error');
  			})
	});

	if(dataku.id){
		$('#select2user').val(dataku.user_id).trigger('change');
		// $('#select2user').val(dataku.user_id).trigger('select2:selecting');
		// $('#select2tablename').val(dataku.table_name).trigger('change');
		// if(dataku.table_id){
		// 	$('#select2tableid').val(dataku.table_id).trigger('change');
		// }
	}

});
</script>
@endpush