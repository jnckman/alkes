@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Barang Keluar
@endsection

@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Barang Keluar</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <form id='formfilter'>
            {{csrf_field()}}
         <div class="box-header">
            <h3 class="box-title">Filter Bulan :</h3>
         </div>
         <div class="box-body">
            <input name="tanggal" id="inputdate" class="form-control" placeholder="Kosongkan untuk melihatt semua data" />
         </div>
         <div class="box-footer">
            <button type="submit" class="btn btn-info">Submit</button>
         </div>
         </form>
      </div>
   </div>   
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body no-padding table-responsive">
            <table id='table1' class="table table-bordered table-striped table-hover">
               <thead>
                  <tr>
                     <th></th>
                     <th>Tgl Faktur</th>
                     <th>TOP</th>
                     <th>Jatuh Tempo</th>
                     <th>No SO</th>
                     <th>Customer</th>
                     <th>No NPWP</th>
                     <th>Alamat 1</th>
                     <th>Alamat 2</th>
                     <th>Barang</th>
                     <th>Kode Barang</th>
                     <th>QTY</th>
                     <th>UOM</th>
                     <th>Harga Satuan</th>
                     <th>Jumlah</th>
                     <th>DISC</th>
                     <th>NILAI DISC</th>
                     <th>NETTO</th>
                     <th>PPN</th>
                     <th>TOTAL</th>
                     <th>BankMandiri</th>
                     <th>Keterangan</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tfoot>
                  <tr>
                     <th colspan="15" class="text-right">TOTAL : </th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
                  <tr>
                     <th></th>
                     <th>Tgl Faktur</th>
                     <th>TOP</th>
                     <th>Jatuh Tempo</th>
                     <th>No SO</th>
                     <th>Customer</th>
                     <th>No NPWP</th>
                     <th>Alamat 1</th>
                     <th>Alamat 2</th>
                     <th>Barang</th>
                     <th>Kode Barang</th>
                     <th>QTY</th>
                     <th>UOM</th>
                     <th>Harga Satuan</th>
                     <th>Jumlah</th>
                     <th>DISC</th>
                     <th>NILAI DISC</th>
                     <th>NETTO</th>
                     <th>PPN</th>
                     <th>TOTAL</th>
                     <th>BankMandiri</th>
                     <th>Keterangan</th>
                     <th>Action</th>
                  </tr>
               </tfoot>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
   var table = $('#table1').DataTable({
      'ajax': {
         'url': '{{route("laporan.dtbarangkeluar")}}'
      },
      'processing': true,
      'serverside':true,
      "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      // "order": [[ 1, 'asc' ]]
      // "scrollX": true
      "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = [
            api .column( 15 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 ),
            api .column( 20 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 )
            ]
            // Total over this page
            pageTotal = [
            api.column( 15, { page: 'current'} ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 ),
            api.column( 20, { page: 'current'} ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 )
            ]
            // Update footer
            $( api.column( 15 ).footer() ).html(
                pageTotal[0].toLocaleString() +' <br>( '+ total[0].toLocaleString() +' )'
            );
            $( api.column( 20 ).footer() ).html(
                pageTotal[1].toLocaleString() +' <br>( '+ total[1].toLocaleString() +' )'
            );
        }
   });

   nomorurut();

   function nomorurut(){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   }

   $('#inputdate').datepicker( {
      format: "yyyy-mm",
      viewMode: "months", 
      minViewMode: "months"
   });

   $('#formfilter').submit(function(event){
      event.preventDefault();
      $.ajax({
         url: '{{route("laporan.dtbarangkeluar")}}',
         type : 'get',
         data: $(this).serialize(),
      }).done(function(response){
         data = response.data;
         // console.log(data);
         table.clear().draw();
         if(data.length>0){
            table.rows.add(data).draw();
            nomorurut();
         }
         else{
            swal('Oops','Tidak ada data','info');
         }
      }).fail(function(){
         swal('Oops','Terjadi kesalahan','error');
      });
   });
});
</script>
@endpush
