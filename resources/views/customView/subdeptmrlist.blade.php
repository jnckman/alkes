@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-hand-paper-o" aria-hidden="true"></i> Sub Department MR List
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id_listdept }}">List Department</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/materialrequestsystem/{{ $id_listdept }}/{{$result[0]->wellhos_departments_id}}">MR by System</a></li>
   <li class="active">Sub Department MR List</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i><b>Sub Departemen</b> : {{$result[0]->nama}} </h5>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../../rekapsubdeptmr/{{$id}}/{{$id_listdept}}"><button class="btn btn-info">Rekap Seluruh Project</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-bordered example1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>Nomor MR</th>
                     <th>Nama Project</th>
                     <th>Pic Project</th>
                     <th>Gudang</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

@endsection

@push('bottom')
<script type="text/javascript">
    $(document).ready(function() {

          $('.example1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': "{{ route('dt.subdeptmrlist', ['id' => $id,'idperiode'=>$id_listdept]) }}",
            //   'ajax': '../subdeptmrlistdtajax/{{$id}}',

          } );
      } );
</script>
@endpush
