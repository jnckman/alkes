@extends('crudbooster::admin_template')

@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('content')
<div class="box">
	<form method="post" action="{{route('so.bulkapprove')}}">
		{{csrf_field()}}
	<div class="box-header">
		<button type="submit" class="btn btn-info">Bulk Approve</button>
	</div>
	<div class="box-body table-responsive">
		<table class="table table-striped table-bordered" id="tableData">
			<thead>
				<th><input type="checkbox" name="select_all" value="1" class="example-select-all"></th>
				<th>Nomor</th>
				<th>User</th>
				<th>Metode Bayar</th>
				<th>Jatuh Tempo</th>
				<th>Diskon</th>
				<th>Total</th>
				<th>Status</th>
				<th>Approval</th>
				<th>Action</th>
			</thead>
			<tfoot>
				<th><input type="checkbox" name="select_all" value="1" class="example-select-all"></th>
				<th>Nomor</th>
				<th>User</th>
				<th>Metode Bayar</th>
				<th>Jatuh Tempo</th>
				<th>Diskon</th>
				<th>Total</th>
				<th>Status</th>
				<th>Approval</th>
				<th>Action</th>
			</tfoot>
		</table>
	</div>
	<div class="box-footer">
	</div>
	</form>
</div>
@endsection

@push('bottom')
<script type="text/javascript">
$(function(){
	var table = $('#tableData').DataTable({
   		processing: true,
   		serverSide: true,
   		ajax:"{{ route('so.dt_pending') }}",
   		fixedHeader: {
            header: true,
            footer: true
        },
        columnDefs: [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
            if(data!='0')
             return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"/>';
            else
              return '';
         }
        }],
	});

	// Handle click on "Select all" control
   $('.example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('.example-select-all').prop('checked', this.checked);
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;
      var konter = 0;
      e.preventDefault();
      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!(this.checked)){
          konter++;
          // console.log(jumlahdata);
         }
         if(this.checked){
          $('#frm-example').append('<input type="hidden" name="stok[]" value="'+ $(this).parent().parent().find('td:eq(1)').html()+'" />')
         }
      });
      if(konter==jumlahdata){
        swal('Oops','Pilih Data untuk di-Approve','error');
      }
      else this.submit();

   });
});
</script>
@endpush