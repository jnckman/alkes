@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
</style>
@endpush

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<p>Transfer barang : <b>{{ $barangku->nama}}</b> ; Gudang : <b>{{ $gudangku->nama }}</b> ; Stock : <b>{{ $barang_gudangs->stok}} </b> </p>
	</div>
	<div class="panel-body">
		<form method="post" action="{{ route('barang_gudangs.transfer.barang') }}" id="formTransfer">
			{{ csrf_field() }}
			<div class='form-group'>
				<label>Pilih Gudang :</label>
				<select name="id_gudang_k" id="pilih_gudang" class="selectpicker" data-live-search="true">
					@foreach($semua_gudang as $r)
					<option value="{{ $r->id }}">{{ $r->nama }}</option>
					@endforeach
				</select>
			</div>

			<div class='form-group'>
				<label>Jumlah :</label>
				<input type='number' name='jumlah' min="0" required class='form-control' placeholder="0" />
			</div>
			<div class='form-group'>
				<label>Catatan :</label>
				<input type='text' name='catatan' class='form-control' />
			</div>
			<input type="hidden" name="id_gudang_d" value="{{ $gudangku->id }}">
			<input type="hidden" name="id_barang" value="{{ $barangku->id }}">
			<button type="submit" class="btn btn-success pull-right">Transfer</button>
		</form>
	</div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
@endpush
