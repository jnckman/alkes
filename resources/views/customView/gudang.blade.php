@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
   .modal-dialog{
      overflow-y: initial !important
   }
   .c-height {
      max-height: calc(100vh - 300px);
      overflow-y: auto;
   }
   .custom-right[style] {
   padding-right: 0px !important;
   }
   .p-right-left {
   padding-left: 0px !important;
   padding-right: 0px !important;
   }
   @media screen and (min-width: 770px) {
      .p-right{
         padding-right: 10px !important;
      }
   }
   @media screen and (max-width: 768px) {
      .p-top{
         padding-top: 10px !important;
      }
   }
</style>
@endpush

@section('tombol')
<button type="button" onClick="transfer_global()" class="btn btn-info btn-sm"><i class="fa fa-exchange"></i> Transfer Barang</button>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id='table_master'>
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Nama Gudang</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modalLihatBarang" class="modal fade c1" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 id="title_modal" class="modal-title"><b>Daftar Barang</b></h4>
         </div>
         <div class="modal-body">
            <div class="box">
               <table id="tableBarang" class='table table-striped table-bordered'>
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Stok</th>
                     </tr>
                     <tr id="thMerah" style="display:none" class="danger"><th colspan="4" class="text-center"><i class="fa fa-search"></i> Tidak Ada Barang</th></tr>
                  </thead>
                  <tbody id="tbodyBarang">
                  </tbody>
               </table>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" id="btn_transfer" onclick="transfer_barang()" class="btn btn-info"><i class="fa fa-exchange"></i> Transfer Barang</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div id="modalTransferBarang" class="modal fade c1"  tabindex="-1" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <form id="form_transfer" method="post" action="{{ route('transfer_barang') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id_gudang_d" name="id_gudang_d">
            <input type="hidden" id="id_gudang_k" name="id_gudang_k">
            <div class="modal-header">
               <h4 class="modal-title"><b>Transfer Barang</b></h4>
            </div>
            <div class="modal-body">
               <div id="alert" class="alert alert-warning" style="display:none; margin-bottom:10px;"><p id="alert_msg"></p></div>
               <div class="row" style="padding:0 10px 0 10px;">
                  <div class="col-md-4 col-xs-6">
   	               <h5 style="margin-left:5px;"><b>Dari</b></h5>
               		<select id="d_gudang" class="selectpicker" title="Pilih Gudang" data-live-search="true">
                        @foreach($result as $r)
                        <option value="{{ $r->id }}">{{ $r->nama }}</option>
                        @endforeach
               		</select>
                  </div>
                  <div class="col-md-4 col-md-offset-3 col-xs-6">
   	               <h5 style="margin-left:5px;"><b>Ke</b></h5>
               		<select id="k_gudang" class="selectpicker" title="Pilih Gudang" data-live-search="true">
                        @foreach($result as $r)
                        <option value="{{ $r->id }}">{{ $r->nama }}</option>
                        @endforeach
               		</select>
                  </div>
                  <div class="col-md-12 c-height" style="margin-top: 10px;">
                     <div class="col-md-7 col-xs-12 p-right-left p-right p-top">
                        <table class="table table-striped table-bordered" id='tableTransfer_a'>
                           <thead>
                              <tr class="success">
                                 <th>Nama Barang</th>
                                 <th>Stok</th>
                                 <th>Transfer</th>
                              </tr>
                              <tr id="thMerah_a" style="display:none" class="danger"><th colspan="3" class="text-center"><i class="fa fa-search"></i> Tidak Ada Barang</th></tr>
                           </thead>
                           <tbody id="tbodyBarang_modal_a">
                           </tbody>
                        </table>
                     </div>
                     <div class="col-md-5 col-xs-12 p-right-left">
                        <table class="table table-striped table-bordered" id='tableTransfer_b'>
                           <thead>
                              <tr class="success">
                                 <th>Nama Barang</th>
                                 <th>Stok</th>
                              </tr>
                              <tr id="thMerah_b" style="display:none" class="danger"><th colspan="2" class="text-center"><i class="fa fa-search"></i> Tidak Ada Barang</th></tr>
                           </thead>
                           <tbody id="tbodyBarang_modal_b">
                           </tbody>
                        </table>
                     </div>
            	   </div>
               </div>
            </div>
            <div class="modal-footer">
               <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <textarea rows="2" cols="60" style="margin-left:10px;" name="catatan" placeholder=" catatan..." form="form_transfer"></textarea>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <button id="btn_submit" type="button" class="btn btn-info">Transfer</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
 </div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.datatables.bootstrap.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.handlebars.js') }}"></script>
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
   var id_gudang_global=0;
   $(function() {
      // var table_ajax = $('#table').DataTable({
      //    'processing': true,
      //    'serverside':true,
      //    'lengthMenu': [ 25, 50, 75, 100 ],
      //    'ajax': '../admin/gudangs/ajax'
      // });
      $('#table_master').DataTable({
         processing: true,
         serverSide: true,
         ajax: "{{ route('get.dt.master.gudang') }}",
         columns: [
            {data: 'kode', name: 'kode'},
            {data: 'nama', name: 'nama'},
            {data: 'notelp', name: 'notelp'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
         ],
         lengthMenu: [ [25, 50, -1], [25, 50, "All"] ]
      });
   });

   $('.c1').on('hidden.bs.modal', function () {
      //NOTE tambah id="body" di tag body, admin_template
      $('#body').addClass('custom-right');
   });

   $("#d_gudang").change(function() {
      var d = check_pilih_gudang();
      id_gudang_global = d[0];
      $('#alert').fadeOut();
      call_dt_modal(true, d[0]);
   });

   $("#k_gudang").change(function() {
      var d = check_pilih_gudang();
      $('#alert').fadeOut();
      $('#id_gudang_k').val(d[1]);
      call_dt_modal(false, d[1]);
   });

   function check_pilih_gudang() {
      var data =  [$('#d_gudang option:selected').val(), $('#k_gudang option:selected').val()];
      return data;
   }

   function showAlert(msg) {
      // $('#').show();
      // $('#').fadeIn();
      // $('#').slideDown();
      $('#alert_msg').text(msg);
      $('#alert').fadeIn();
   }

   function lihatBarang(id_gudang) {
      //alert(id_gudang);
      id_gudang_global = id_gudang;
      $("#tbodyBarang").empty();
      $.ajax({
         url: "{{ route('get_gudang_barang') }}",
         type: "GET",
         data: {id: id_gudang},
         success:function(data){
            if(data[0]){ //cek data ada atau tidak
               //console.log(data);
               if($("#thMerah").show()) {
   					$("#thMerah").hide();
   				}
               $("#btn_transfer").show();
               var table = $("#tableBarang");
               $.each(data, function(no, el){
   			      table.append("<tr><td>"+(no+1)+"</td><td>"+el.kode+"</td><td>"+el.nama+"</td><td>"+el.stok+"</td></tr>");
               });
            }
            else {
               // console.log("Tidak ada data");
               $("#thMerah").show();
               $("#btn_transfer").hide();
            }
         }
      });
      $('#modalLihatBarang').modal('show');
   }

   function transfer_global() {
      id_gudang_global=0;
      transfer_barang();
   }

   function transfer_barang() {
      if($('#modalLihatBarang').is(':visible')){
         $('#modalLihatBarang').modal('hide');
      }
      if(id_gudang_global != 0){
         $("#d_gudang").val(id_gudang_global).change();
      }
      $('#modalTransferBarang').modal('show');
   }

   function call_dt_modal(boolean, id_gudang) {
      //NOTE boolean utk check table yg mana di modalTransferBarang
      if(boolean) $("#tbodyBarang_modal_a").empty();
      else $("#tbodyBarang_modal_b").empty();
      $.ajax({
         url: "{{ route('get_gudang_barang') }}",
         type: "GET",
         data: {id: id_gudang},
         success:function(data){
            if(data[0]){ //cek data ada atau tidak
               // console.log(data);
               if(boolean){ //NOTE utk table kiri
                  if($("#thMerah_a").show()) {
      					$("#thMerah_a").hide();
      				}
                  var table = $("#tableTransfer_a");
                  $.each(data, function(no, el){
      			      table.append("<tr><td>"+el.nama+"</td><td>"+el.stok+"</td><td><input style='width:50px' type='number' name="+el.barangs_id+"></td></tr>");
                  });
                  $('#id_gudang_d').val(id_gudang_global);
               }
               else{ //NOTE utk table kanan
                  if($("#thMerah_b").show()) {
      					$("#thMerah_b").hide();
      				}
                  var table = $("#tableTransfer_b");
                  $.each(data, function(no, el){
      			      table.append("<tr><td height='41'>"+el.nama+"</td><td height='41'>"+el.stok+"</td></tr>");
                  });
               }
            }
            else {
               if (boolean) $("#thMerah_a").show();
               else $("#thMerah_b").show();
            }
         }
      });
   }

   $('#btn_submit').click(function(){
      var d = check_pilih_gudang();
      // if(!s) console.log("ini 0");
      if(!d[0] || !d[1]){
         //console.log(true);
         showAlert("Silahkan pilih gudang...");
      }
      else if(d[0] == d[1]){
         showAlert("Gudang tidak boleh sama...");
      }
      else $('#form_transfer').submit();
   });

</script>
@endpush
