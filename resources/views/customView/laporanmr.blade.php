@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> List Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">List Pengiriman</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <div class="col-md-2" style="padding: 0px;border: solid 1px black;text-align: center;padding-top: 30px;padding-bottom: 30px;cursor: pointer;">
                  <a href="laporanmr/periodemr"><i class="fa fa-calendar fa-5x"></i><Br>Periode MR</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.datatables.bootstrap.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.handlebars.js') }}"></script>

@endpush
