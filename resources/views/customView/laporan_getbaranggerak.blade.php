@extends('crudbooster::admin_template')
@section('header22')
   <i class="fa fa-briefcase" aria-hidden="true"></i> Laporan Pergerakan Barang
@endsection
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Pergerakan Barang</li>
@endsection
@section('content')
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">{{$tglku}} -> Sekarang</h3>
				<div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <!-- <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button> -->
                </div>
	            </div>
	            <br>
	            <h3 class="box-title">Fast Moving / Slow Moving</h3>
			</div>
			<div class="box-body table-responsive">
	            <table id='table1' class="table table-bordered table-striped table-hover">
	               <thead>
	                  <tr>
	                     <th>Kode</th>
	                     <th>Nama Barang</th>
	                     <th>Jumlah</th>
	                  </tr>
	               </thead>
	               <tfoot>
	                  <tr>
	                     <th>Kode</th>
	                     <th>Nama Barang</th>
	                     <th>Jumlah</th>
	                  </tr>
	               </tfoot>
	            </table>
	        </div>
		</div>
	</div>
</div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	var table = $('#table1').DataTable({
		fixedHeader: {
            header: true,
            footer: true
        },
	    'processing': true,
	    'serverside':true,
	    "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
            }
        ],
        "order": [],
        "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
        'ajax' : {
        		url : '{{route("laporan.barangPopuler")}}',
        		data:{
        			'fastmoving' : 1
        		}
        		}
	});

	function nomorurut(){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   	}

});
</script>
@endpush