@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> Isi SPBM 
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(purchaseorders)}}">Purchase Order</a></li>
   <li><a href="{{CRUDBooster::adminPath().'/listspbm/'.$id}}">List SPBM</a></li>
   <li class="active">Detail</li>
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="{{CRUDBooster::adminPath().'/listspbm/'.$id}}"><button class="btn btn-info">Kembali</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       
         
         
         <div class="col-md-12" style="margin-top: 50px;">
         

         <table class="table table-bordered example1" id='table_dashboard' >
           <thead>
                 <tr class="success">
                     <th>Barang</th>
                     <th>Tanggal Datang</th>
                     <th>Jumlah</th>
                     <th>No Spbm</th>
                     <th>action</th>
                     
                     

                 </tr>
          </thead>
          
         </table>
         
        
         </div>
         
       
       </div>
       </div>
     </div>
   </div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action='../../editisispbmajax' method='post'>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Pilih Harga</h4>
                        </div>
                        <div class="modal-body" style="height: 200px;">
                            <div class="col-md-12" style='margin-top: 10px;'>
                              <div class="col-md-4" style="padding-left: 0px;">
                                Barang Datang di Input :
                              </div>
                              <div class="col-md-8 totalpl">

                              </div>
                            </div>

                            <div class="col-md-12" style="margin-top: 10px;">
                              <div class="col-md-4 " style="padding-left: 0px;">Edit Barang Datang : </div>
                              <div class="col-md-8 totalsk">
                                

                                    <input type="text" name="jumlah" class="form-control" placeholder="silahkan input" id='inputstok'>
                                    {{CSRF_field()}}

                                
                              </div>
                            </div>
                            <input type="hidden" name="" id='supplierid' value=''>
                            <div class="col-md-12" style="margin-top: 10px;">
                            
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            <input type="hidden" id="spbmid" name="id" value="0">
                        </div>
                    </div>
</form>
</div>
@endsection

@section('datatable')
  <script type="text/javascript">
    $(document).ready(function() {
    
          var curjumlah;
          var curisispbm;
          var dtb = $('.example1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../../isilistspbmdetaildtajax/{{$id}}/{{$subid}}',
              
          } );

          $('body').on('click', '.editisispbm', function (){
            $('.totalpl').html('');
            $('.totalpl').html($(this).attr('jumlah'));
            $('#myModal').modal('show');
            curjumlah = $('#inputstok').val();
            curisispbm = $(this).attr('id');
            $('#spbmid').val(curisispbm);

          });

          // $('body').on('click', '#btn-save', function (){
          //     curjumlah = $('#inputstok').val();
          //       $.ajax({

          //               type: 'POST',
          //               url: '../../editisispbmajax',
          //               data: {'id':curisispbm,'jumlah':curjumlah},
          //               dataType: 'json',
          //               success: function (data) {
          //                   console.log(data);
          //                   if(data == 1){
          //                     alert('berhasil di ubah');
                              
          //                     $('#myModal').modal('hide');
          //                     //dtb.clear().draw();
          //                     dtb.ajax.reload(); // Redraw the DataTable
          //                   }

          //               },
          //               error: function (data) {
          //                 alert('gagal');
          //                   console.log(data);
          //               }
          //       });
          // });          


      } );
  </script>
@endsection

