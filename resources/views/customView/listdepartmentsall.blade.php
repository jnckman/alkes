@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endpush

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> List Department
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li class="active">List Department</li>
@endsection

@section('content')

   <div class="container-fluid">
     <div class="row">
       <div class="box box-default">
         <div class="box-body">
            <div class="col-md-12">
              <div class="col-md-4" style="padding: 0px;border-top-left-radius: 14px;border-bottom-left-radius: 14px;height: 40px;border-left: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:white;font-weight:bolder;background-color: #5fa6ff;height: 100%;border-top-left-radius: 10px;border-bottom-left-radius: 10px;" >Memilih Periode MR</span>
              </div>
              <div class="col-md-4" style="padding: 0px;height: 40px;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:grey;background-color: white;height: 100%;" >Purchase Request</span>
              </div>
              <div class="col-md-4" style="padding: 0px;border-top-right-radius: 14px;border-bottom-right-radius: 14px;height: 40px;border-right: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:grey;background-color: white;height: 100%;border-top-right-radius: 10px;border-bottom-right-radius: 10px;" >Purchase Order</span>
              </div>
            </div>
           <div class="col-md-12">
             <h5>Periode MR : {{$periode->judul}} | Tanggal Periode : {{$periode->tglmulai}}</h5>
           </div>
           <div class="col-md-1">
             <a href="../listdepartments/{{$id}}"><button class="btn btn-warning">Back</button></a>
           </div>
           <div class="col-md-2 ">
            <a href="../rekapalldept/{{$id}}"><button class="btn btn-info">Lihat Barang Semua MR</button></a>
          </div>
          
          <div class="col-md-2 ">
            <button class="btn btn-success submitrekap" style="width: 100%;" >Submit Rekap</button>
          </div>
          
          
          
          <div class="col-md-6 col-md-offset-2">
            <!-- <div class="col-md-4" style="text-align: right;">
              <h4><b>Filter Tampilan : </b></h4>
            </div>
            <div class="col-md-3">
              <a href="../listdepartmentsall/{{$id}}"><button class="btn btn-warning ">Lihat Semua MR</button></a>
            </div>
            <div class="col-md-4">
              <a href="../listdepartments/{{$id}}"><button class="btn btn-warning ">Lihat Per Department</button></a>
            </div>  
          </div> -->
          <div class="col-md-12">
            
          </div>
          
         </div>
       </div>
     </div>
   </div>
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;">
        <table class="table table-bordered table-striped" id="table_dashboard">
          <thead>
            <tr>
              <th>tanggal rekap</th>
              <th>kode</th>
              <th>status</th>
              <th>option</th>
            </tr>
          </thead>    
          <tbody>

            @foreach($listrekap as $lr)
            <tr>
              <td>{{$lr->tanggalrekap}}</td>
              <td><b>{{$lr->approved}}/{{$lr->total}} Still Not Approved !! Check PR </b></td>
              <td>{{$lr->kode}}</td>
              <td>
                <a href="../lihatpembukuanrekap/{{$id}}/{{$lr->kode}}">Lihat Purchase Request</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
   <div class="container-fluid">
     <div class="row" >

      <div class="box box-default" style="padding-top: 20px;">

      <div class="box-body table-responsive no-padding">
       <div class="col-md-12 nodata" style="display: none;">
         <h5 class="" style="text-align: center;font-weight: bolder;">No Data Available</h5>
       </div>
       <div class="col-md-12 tablewrapper">
          <form action='lockingrekaprevisi' method='post' id="formsubmitrekap">
          <input type="hidden" name="id" value="{{$id}}">
          {{ csrf_field() }}
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th><input type="checkbox" name="select_all" value="1" id="checkall"></th>
                     <th>No MR</th>
                     <th>Nama Sub Department</th>
                     <th>Area Project</th>
                     <th>Nama Department</th>
                     <th>Tanggal Pengisian</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
         </form>
       </div>
       </div>
       </div>
     </div>
   </div>
@endsection

@push('bottom')
<script type="text/javascript">
  $(document).ready(function() {

          var table = $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../listdepartmentsalldtajax/{{$id}}',
              "bPaginate": false,
              'columnDefs': [{
               'targets': 0,
               'searchable': false,
               'orderable': false,
               
              }]
          } );

          $('.submitrekap').click(function(event) {
            if(confirm("Apakah anda yakin melakukan rekap, proses rekap tidak bisa di undo !!")){
                    $('#formsubmitrekap').submit();
                }
                else{
                    return false;
                }
            
          });

          $('#checkall').on('click', function(){
             // console.log("checkall");
             // Get all rows with search applied
             var rows = table.rows({ 'search': 'applied' }).nodes();
             // Check/uncheck checkboxes for all rows in the table
             $('input[type="checkbox"]', rows).prop('checked', this.checked);

          });
  } );
</script>
@endpush
