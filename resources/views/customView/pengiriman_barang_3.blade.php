@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
<style>
   .line-red {
      border: 2px solid #DD4B39 !important;
   }
</style>
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Pengiriman Barang Part 3</li>
@endsection

@section('content')
<div class="container">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="stepwizard">
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-paper-plane" aria-hidden="true"></i> Jenis Pengiriman</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-crosshairs" aria-hidden="true"></i> List MR/SO</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Ambil Barang</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-success btn-lg"><i class="fa fa-plus" aria-hidden="true"></i> Bagi Barang</a>
            </div>
         </div>
     </div>
  </div>
</div>

<div class="container">
   <div class="panel panel-default">
   	<div class="panel-body padding-lr0">

         <form id="form_3" method="post" action="{{ route('action.pengiriman.barang', 3)}}">
            
            <input type="hidden" name="jenis[]" value="{{ $jenis[0] }}">
            <input type="hidden" name="jenis[]" value="{{ $jenis[1] }}">
            <input type="hidden" name="periode" value="{{ $periode }}">
            <input type="hidden" name="edan" value="{{ $periode }}">
            <input type="hidden" name="barang_habis" id="barang_habis" value="">
            <?php $barang = $detail_data->unique('barang_id')->sortBy('nama');
                  $brg_ambil = $barang_terambil->toArray();
                  $total_barang_ambil = $barang_terambil->sum('jumlah');
            ?>
            <div class="col-md-2 pull-right" style="margin-bottom: 10px;">
               <button id="btn_next" type="submit" class="btn btn-info btn-block pull-right">NEXT</button>
            </div>

            <div class="col-md-12" style="margin-top: 15px;">
               <div class="col-md-4">
                  <p>
                     <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> Barang Terambil
                     </button>
                  </p>
                  <div class="collapse in" id="collapseExample">
                     <div class="card" style="margin: 0;">
                        <div class="card-main">
                           <table class="table table-striped table-bordered" style="margin-bottom: 0px !important;">
                              <thead>
                                 <tr class="success">
                                    <th class="th-center font-s2"><strong>Barang</strong></th>
                                    <th class="th-center font-s2"><strong>Jumlah</strong></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($barang_terambil->sortBy('nama') as $brg_id => $bt)
                                    <tr id="tr{{ $brg_id }}">
                                       <td id="td_name{{ $brg_id }}" align="center" class="text-center font-s2">{{ $bt[nama] }}</td>
                                       <td id="td{{ $brg_id }}" align="center" class="text-center font-s2">{{ $bt[jumlah] }}</td>
                                    </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>

               <div id="body_table" class="col-md-8 scroll-div">
                  <div id="all_table" class="col-md-12 flex-container">
                     <?php $ct = 0; ?>
                     @foreach($barang as $b)
                     
                     <div class="col-md-6" style="margin 0 5px 10px 5px 0; padding: 0 10px 0 10px;">
                        <div id="card{{ $b->barang_id }}" class="card">
                           <div class="card-header font-s">{{ $b->nama }}</div>
                           <div class="card-main">
                              <table id="{{ $b->barang_id }}" class='table table-striped table-bordered' style="margin-bottom: 0;">
                                 <thead>
                                    <tr class="info">
                                       <th class="th-center font-s2"><strong>
                                          @if($jenis[0]==1)
                                          MR
                                          @else
                                          SO
                                          @endif
                                       </strong></th>
                                       <th class="th-center font-s2"><strong>Belum Dikirim</strong></th>
                                       <th class="th-center font-s2"><strong>Mau Dikirim</strong></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <?php $subct = 0; ?>
                                    @foreach($detail_data->sortBy('jumlah') as $index => $nota)

                                       @if($b->barang_id == $nota->barang_id)
                                          <?php $jml_barang = ""; ?>
                                          @foreach($brg_ambil as $index2 => &$ba)
                                             @if($index2 == $nota->barang_id)
                                             <?php
                                                if($ba['jumlah'] > $nota->jumlah) {
                                                   $jml_barang = $nota->jumlah;
                                                   $brg_ambil[$index2]['jumlah'] -= $nota->jumlah;
                                                }
                                                else if($ba['jumlah'] <= $nota->jumlah && $ba['jumlah'] > 0) {
                                                   $jml_barang = $ba['jumlah'];
                                                   unset($brg_ambil[$index2]);
                                                }
                                                else {
                                                   $jml_barang = "";
                                                }
                                                break;
                                             ?>
                                             @else
                                             <?php $jml_barang = ""; ?>
                                             @endif
                                          @endforeach
                                          <tr>
                                             <td class="" style="font-size: 11px;" align="center" valign="center">{{ $nota->nomor_nota }}<br>[ {{$nota->namacust}} ]</td>
                                             <td class="font-s2" align="center" valign="center">{{ $nota->jumlah }}-{{ $nota->nota_id }}-{{ $b->barang_id }}</td>
                                             <td align="center" valign="center"><input class='jml_barang' style='width: 50px; font-size:12px;' type='number' min='0' max='{{ $nota->jumlah }}' name="input[{{$nota->nota_id}}][{{$b->barang_id}}]" value="{{ $jml_barang }}">
                                          <?php $subct++; ?>
                                          <?php $ct++; ?>
                                             </td>
                                          </tr>
                                          <?php unset($detail_data[$index]); ?>
                                       @endif
                                    @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<div tabindex="-1">
   <form id="form_4" method="post" action="{{ route('action.pengiriman.barang', 3)}}">
      {{csrf_field()}}
      <input type="hidden" name="input" id='inputku'>
   </form>
</div>
@endsection

@push('bottom')
<script type="text/javascript">
   var total_barang_kirim = [];

   $(function () {
      $('div[data-toggle="collapse"]').click(function(e) {
         e.stopPropagation();
      });
   });

   $('#form_3').submit(function(e){
      e.preventDefault();
      var valid = true;

      //NOTE nge-cek total barang kirim dgn barang barang terambil
      $('#all_table table').each(function(){
         var total = 0;

         $(this).find('tr').each(function() {
            total += parseInt($(this).find('.jml_barang').val())||0;
         });
         total_barang_kirim[this.id] = total;
         // console.log(total);
         // console.log(this.id);
         if (parseInt($("#td" + this.id).html()) < total){
            // barang = $("#td_name" + this.id).html();
            $("#tr" + this.id).addClass("danger");
            $("#card" + this.id).addClass("line-red");
            // console.log(barang);
            valid = false;
         }
         else if (parseInt($("#td" + this.id).html()) >= total) {
            $("#tr" + this.id).removeClass("danger");
            $("#card" + this.id).removeClass("line-red");
         }
      });
      // console.log(total_barang_kirim);
      if (valid) {
         var total=0;
         $(".jml_barang").each(function() {
             var value = $(this).val();
            //  console.log(value);
             // add only if the value is number
             if(!isNaN(value) && value.length != 0) {
                 total += parseInt(value);
             }
         });

         var habis = 0;
         if (total == <?php echo $total_barang_ambil ?>) habis = 1;

         var form = this;
         if (habis == 0) { //NOTE warning ada sisa barang
            console.log(total_barang_kirim);
            // $.each(total_barang_kirim, function(key, value) {
            //       // console.log("barang"+key);
            //       // var total_ambil = parseInt($("#td" + key).html());
            //       // console.log("total ambil: " + total_ambil);
            //       console.log(key + "-" + value);
            // });
            swal({
               title: "Masih ada sisa barang, lanjut?",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: '#DD6B55',
               confirmButtonText: 'Ya',
               cancelButtonText: "Tidak",
               closeOnConfirm: false,
               closeOnCancel: true
            },
            function(isConfirm) {
               if (isConfirm) {
                  $('#barang_habis').val(habis);
                  // form.submit();
                  $('#inputku').val($('#form_3').serialize());
                  $('#form_4').submit();
               }
            });
         }
         else {
            swal({
               title: "Sudah benar ?",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: '#DD6B55',
               confirmButtonText: 'Ya',
               cancelButtonText: "Tidak",
               closeOnConfirm: false,
               closeOnCancel: true
            },
            function(isConfirm) {
               if (isConfirm) {
                  $('#barang_habis').val(habis);
                  // alert('hehe');
                  $('#inputku').val($('#form_3').serialize());
                  $('#form_4').submit();
               }
            });
         }
      }
      else swal("Barang Tidak Cukup");
   });
</script>
@endpush
