@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Surat Jalan
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
<link href="{{ asset('customs/css/bootstrap_render_print.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{ route('index.list.pengiriman.barang') }}">List Pengiriman</a></li>
   <li><a href="{{ route('view.detail.pengiriman.barang', ['kode' => $kode]) }}">Detail Pengiriman</a></li>
   <li class="active">Surat Jalan</li>
@endsection

@section('content')
<p><a href="{{ route('view.detail.pengiriman.barang', ['kode' => $kode]) }}"><i class='fa fa-chevron-circle-left'></i> &nbsp; Back To Detail Pengiriman</a></p>
<div class="custom-container">
   <div class="col-md-12 padding-lr0">
      <div class="col-md-6 padding-lr0">
         <h3 style="padding:0; margin:0;"><strong>{{ $kode }}</strong></h3>
      </div>
      <div class="col-md-6 padding-lr0">
         <a href="{{ route('surat.jalan.pdf', ['kode' => $kode ]) }}" class="btn btn-primary pull-right"><i class="fa fa-print" aria-hidden="true"></i> Download</a>
      </div>
   </div>
</div>

<div class="custom-container" style="padding-top: 50px;">
   <div class="panel panel-default">
   	<div class="panel-body padding-lr0">
         <div class="col-md-12 table-responsive">
            <table class="table table-striped table-bordered" id='table_master'>
               <thead>
                  <tr class="success">
                     <th>Kode</th>
                     <th>Nama</th>
                     <th>Jumlah</th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.datatables.bootstrap.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.handlebars.js') }}"></script>
<script type="text/javascript">
   $(function () {
      dt_master();
   });

   function dt_master() {
      $('#table_master').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('surat.jalan.dt', ['kode' => $kode]) }}",
         "columns": [
            {data: 'kode', name: 'barangs.kode'},
            {data: 'nama', name: 'barangs.nama'},
            {data: 'jumlah', name: 'jumlah'}
         ],
         "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
      });
   }
</script>
@endpush
