@extends('customView.partial.shop_template')
@section('content')
<div class="container">
<div class="container-fluid">
<div class="row">
	<h2 class="title text-center">Data User</h2>
	<div class="col-md-12">
	<div class="col-sm-6 col-md-4">
		<div class="product-image-wrapper">
			<img src="{{asset($dataUser->photo)}}" alt="Belum ada Foto">
		</div>
	</div>
	<div class="col-sm-6 col-md-8">
		<div class="form-group">
			<label>Nama</label>
			<form method="post" action="{{ route('shop.profile.sunting','name') }}">
				{{csrf_field()}}
			<div class="input-group">
				<input type="text" class="form-control" readonly="true" name="update" id="inputNama" value="{{$dataUser->name}}">
				<div class="input-group-btn" >
					<button class="btn btn-warning btnSuntingNama" type="button"><i class="glyphicon glyphicon-pencil"></i></button>
					<div id="buttonTambahanNama" style="display: none">
						<button class="btn btn-warning" type="submit"><i class="glyphicon glyphicon-ok"></i></button><button class="btn btn-warning btncancel" type="button"><i class="glyphicon glyphicon-remove"></i></button>
					</div>
				</div>
			</div>
			</form>
		</div>
		<div class="form-group">
			<label>Email</label>
			<form method="post" action="{{route('shop.profile.sunting','email')}}">
				{{csrf_field()}}
			<div class="input-group">
				<input type="email" class="form-control" readonly="true" name="update" id="inputEmail" value="{{$dataUser->email}}">
				<div class="input-group-btn">
					<button class="btn btn-warning btnSuntingEmail" type="button"><i class="glyphicon glyphicon-pencil"></i></button>
					<div id="buttonTambahanEmail" style="display: none">
						<button class="btn btn-warning" type="submit"><i class="glyphicon glyphicon-ok"></i></button><button class="btn btn-warning btncancel" type="button"><i class="glyphicon glyphicon-remove"></i></button>
					</div>
				</div>
			</div>
			</form>
		</div>
		<div class="form-group">
			<label>Password</label>
			<form method="patch" action="{{route('shop.profile.sunting','password')}}">
				<button type="button" class="form-control btn btn-warning btnSuntingPassword">Ubah Password</button>
				<input class="form-control" type="password" name="oldPassword" style="display: none">
				<input class="form-control" type="password" name="password1" style="display: none">
				<input class="form-control" type="password" name="password2" style="display: none">
			</form>
		</div>
	</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
	<ul class="nav nav-tabs nav-justified">
	  <li class="active"><a data-toggle="tab" href="#tab_salesorder">Sales Order</a></li>
	  <li ><a data-toggle="tab" href="#tab_returso">Retur </a></li>
	</ul>
	<div class="tab-content">
	  <div id="tab_salesorder" class="tab-pane fade in active">
	  	<br/>
	  	<br/>
	    <h2 class="title text-center">HISTORY SALES ORDER</h2>
		<div class="table-responsive" >
			<table class="table table-hover table-striped" id="tableListBarang" style="width: 100%">
				<thead>
					<th>No</th>
					<th>No SO</th>
					<th>Nama Barang</th>
					<th>Jumlah SO</th>
					<th>Jumlah Terkirim</th>
					<th>Total</th>
					<th>Metode Bayar</th>
					<th>Jatuh Tempo</th>
					<th>Status</th>
					<th>Action</th>
				</thead>
				
			</table>
		</div>
	  </div>
	  <div id="tab_returso" class="tab-pane fade">
	  	<br/>
	  	<br/>
	    <h2 class="title text-center">HISTORY RETUR</h2>
	    <div class="table-responsive" >
			<table class="table table-hover table-striped" id="tableListBarang2" style="width: 100%">
				<thead>
					<th>No</th>
					<th>No Retur</th>
					<th>Barang</th>
					<th>Jumlah</th>
					<th>Status</th>
					<th>Keterangan</th>
				</thead>
			</table>
		</div>
	  </div>
	</div>
	</div>
</div>
</div>
</div>

<div class="modal fade" id="modalPengiriman" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h4>Status Pengiriman</h4>
    			</div>
    			<div class="modal-body">    				
    				<div class="table-responsive">
    				<table class="table table-hover table-striped" id='tablePengiriman' style="width: 100%">
    					<thead>
    						<th>Kode</th>
    						<th>Tanggal</th>
    						<th>Status</th>
    						<th>Action</th>
    					</thead>
    				</table>
    				</div>
    			</div>
    			<div class="modal-footer">
		          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
		        </div>
    		</div>
    	</div>
</div>

@endsection
@push('bottom')
<script type="text/javascript">
$(function() {
	var namalama = '';
	var emaillama = '';
	var tableKirim = $('#tablePengiriman').DataTable({
        "lengthChange": false,
        "searching": false,
        data:[],
		columns: [
		{ "data": "kode" },
		{ "data": "tanggal" },
		{ "data": "status" },
		{ "data": "action" },
		],
	});
	var table = $('#tableListBarang').DataTable({
		"pageLength": 50,
    	"lengthChange": false,
    	// "searching": false,
    	'processing': true,
        'serverside':true,
    	ajax : '{{route("shop.dtsales_order")}}',
		"columnDefs": [
            { "visible": false, "targets": [1,5,6,7] },
        ],
        // "order": [[ 1, 'asc' ]],
        // "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var groupid = -1;
            var totale = new Array();
			var subtotale = new Array();
			// totale['Totale']= new Array();
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                	groupid++;
                    $(rows).eq( i ).before(
                        '<tr class="group groupso">'+
                        '<td class="text-center"><b>'+group+'</b></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td></tr>'
                    );
				 	//<p class="text-center">Rp '+rows.data()[i][5].toLocaleString('de-DE')+' - '+rows.data()[i][6]+jatuhTempo+'</p>
				 	//'+rows.data()[i][10]+'
				 	// '+rows.data()[i][11]+'
                    last = group;
                }

                val = api.row(api.row($(rows).eq( i )).index()).data();      //current order index
                $.each(val,function(index2,val2){
                		if (typeof subtotale[groupid] =='undefined'){
                            subtotale[groupid] = new Array();
                        }
                        if (typeof subtotale[groupid][index2] =='undefined'){
                            subtotale[groupid][index2] = 0;
                        }
                        // if (typeof totale['Totale'][index2] =='undefined'){ totale['Totale'][index2] = 0; }
                        
                        valore = Number(val2);
                        subtotale[groupid][index2] += valore;
                        // totale['Totale'][index2] += valore;
                });

            });

            $('tbody').find('.groupso').each(function (i,v) {
            	rowku = table.row($(v).next());
            	if(rowku.data()[7]==null)jatuhTempo = '';
            	else jatuhTempo = ' - '+rowku.data()[7];
            	$(this).find('td:eq(1)').append(rowku.data()[13]);
            	$(this).find('td:eq(2)').append('<p class="text-center">Rp '+rowku.data()[5].toLocaleString('de-DE')+' - '+rowku.data()[6]+jatuhTempo+'</p>');
            	$(this).find('td:eq(4)').append(rowku.data()[10]);
            	$(this).find('td:eq(5)').append(rowku.data()[11]);
            	if(subtotale.length!=0){
            		persentampil = (subtotale[i][4]/subtotale[i][3]*100).toFixed(2);
	            	$(this).find('td:eq(3)').html(persentampil+' %');
	            	if(persentampil==100 && $(this).find('td:eq(4)').html().toLowerCase()!='selesai' ){
	            		$(this).find('td:eq(5)').append(
	            			'<a href="returSO/'+table.row($(v).next()).data()[12]+'" class="btn btn-xs btn-danger btnretur" style="margin-left:5px" data-togle="tooltip" title="Retur"><i class="fa fa-step-backward"></i></a>'+
	            			'<button type="button" class="btn btn-warning btn-xs btnselesai" data-togle="tooltip" title="Tandai Selesai" style="margin-left:5px"><i class="fa fa-check"></i></button>');
	            	}
            	}
            });
        }
	});

	var table2 = $('#tableListBarang2').DataTable({
		"pageLength": 50,
    	"lengthChange": false,
    	// "searching": false,
    	'processing': true,
        'serverside':true,
    	ajax : '{{route("shop.dt_returso")}}',
		"columnDefs": [
            { "visible": false, "targets": [1] },
        ],
        // "order": [[ 1, 'asc' ]],
        // "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td class="text-center" colspan="2"><b>'+group+'</b></td><td></td><td>'+rows.data()[i][6]+'</td><td>'+rows.data()[i][7].split("\r").join("<br/>")+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
	});

	$('.btnSuntingNama').click(function(event) {
		namalama = $('#inputNama').val();
		$('#inputNama').attr('readonly',false);
		$('#buttonTambahanNama').show();
		$(this).hide();
	});

	$('.btnSuntingEmail').click(function(event) {
		emaillama = $('#inputEmail').val();
		$('#inputEmail').attr('readonly',false);
		$('#buttonTambahanEmail').show();
		$(this).hide();
	});

	$('#buttonTambahanNama .btncancel,#buttonTambahanEmail .btncancel').click(function() {
		inputform = $(this).parent().parent().prev();
		inputform.attr('readonly',true);
		// console.log(inputform.attr('name'));
		if(inputform.attr('id')=='inputNama'){
			inputform.val(namalama);$('.btnSuntingNama').show();
		}
		else {
			inputform.val(emaillama);$('.btnSuntingEmail').show();
		}
		$(this).parent().hide();
	});

	$(document).on( 'click', '.btnKirim', function () {
        var data = table.row( $(this).parents('tr').next() ).data();
        //console.log(data);
        id = data[12];
        url = "{{route('shop.getPengiriman')}}"
        $.ajax({
            type:"GET",
            url: url,
            data: {
                "id": id
            }
        }).done(function(data){
            console.log(data);
            if(data.length==0) {
        		tableKirim.clear().draw();
        		swal('Oops','SO belum ada pengiriman','info').catch(swal.noop);
        	}
        	else {
        		tableKirim.clear().draw();
				tableKirim.rows.add(data).draw();
				$('#modalPengiriman').modal('show');
        	}
        }).fail(function(){
            swal('Oops','Gagal Mengambil Data','error').catch(swal.noop);
        });
    });

    $(document).on('click','.btnselesai',function(){
    	// alert('gege');
    	buttonku = $(this);
    	data = table.row($(this).parent().parent().next()).data();
    	// console.log(data[12]);
    	swal({
    		title:'Confirmation',
    		html:'Apakah anda yakin akan mendandai SO telah selesai ?',
    		type:'warning',
    		showCloseButton: true,
			showCancelButton: true,
    	}).then(function(){
    		$.ajax({
    			type: 'post',
    			url : '{{route("so.tandaiselesai")}}',
    			data :{
    				id: data[12],
    				_token: "{{ csrf_token() }}",
    			}
    		}).done(function(response){
    			console.log(response);
    			if(response=='berhasil'){
    				table.row(buttonku.parent().parent().next()).data()[10] = 'Selesai';
	    			table.row(buttonku.parent().parent().next()).data()[11] = '<button type="button" class="btn btn-info btn-xs btnKirim" style="margin-left:5px"><i class ="fa fa-truck"></i></button>';
	    			buttonku.remove();
	    			table.draw();
	    			swal('Updated','SO berhasil ditandai SELESAI','success').catch(swal.noop);
    			}
    		}).fail(function(response){
    			swal('Oops','Terjadi Kesalahan','error').catch(swal.noop);
    		});
    	})
    	.catch(swal.noop);
    });

    $(document).on('click',".btn_approvekirim",function(){
   		id = tableKirim.rows($(this).parent().parent()).data()[0]['id'];
   		suntingStatusKirim = $(this).parent().prev();
   		url = '{{ route("subdept.mr.kirimselesai") }}';
			$.ajax({
	            type: "post",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
		        },
	        }).done(function(response){
	        	console.log(response);
        		swal({
				    type: 'success',
				    html: 'Pengiriman berhasil ditandai',
				    confirmButtonColor: '#FE980F',
				});
				suntingStatusKirim.html(response);
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
   });

});
</script>
@endpush