@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Detail Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset ('customs/css/pace-theme-minimal.css') }}">
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{ route('index.list.pengiriman.barang0') }}">List Pengiriman</a></li>
   <li class="active">Detail Pengiriman</li>
@endsection

@section('content')
<p><a href="{{ route('index.list.pengiriman.barang0') }}"><i class='fa fa-chevron-circle-left'></i> &nbsp; Back To List Pengiriman</a></p>
<div class="custom-container">
   <div class="col-md-12 padding-lr0">
      <div class="col-md-6 padding-lr0">
         <h3 style="padding:0; margin:0;"><strong>{{ $kode }}</strong></h3>
      </div>
      <div class="col-md-6 padding-lr0">
         <a href="{{ route('surat.jalan.view', ['kode' => $kode ]) }}" class="btn btn-info pull-right"><i class="fa fa-eye" aria-hidden="true"></i> Surat Jalan</a>
         <a href="{{ route('tanda.terima', ['kode' => $kode ]) }}" class="btn btn-primary pull-right" style="margin-right: 10px;"><i class="fa fa-download" aria-hidden="true"></i> Tanda Terima</a>
         @if (session()->has('sisa_brg'))
         <button data-toggle="modal" data-target="#modal_sisa_barang" class="btn btn-danger pull-right" style="margin-right: 10px;"><i class="fa fa-delicious" aria-hidden="true"></i></i> Sisa Barang</button>
         @endif
      </div>
   </div>
</div>

<div class="custom-container" style="padding-top: 50px;">
   <div class="panel panel-default">
   	<div class="panel-body padding-lr0">
         <?php $semua_nota = $detail_data->unique('nota_id')->sortBy('nota_id'); ?>
         <div class="col-md-12 flex-container">
         @foreach($semua_nota as $nota)
            <div class="col-md-4" style="margin-bottom: 10px; margin-top: 10px;">
               <div class="card" style="margin: 0;">
                  <div class="card-header font-s">
                     <div class="col-md-12 padding-lr0">
                        <div class="col-md-8 col-md-offset-2 padding-lr0">{{ $nota->no_nota }}</div>
                        <div class="col-md-8 col-md-offset-2 padding-lr0">{{ $nota->no_nota2 }}</div>
                        <div class="col-md-2 padding-lr0">
                           <div class="dropdown">
                              <button class="btn btn-warning btn-xs dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-chevron-down"></i></button>
                              <ul class="dropdown-menu edit-dd">
                                 <li><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
                                 <li><a class="btnTandaTerima" jenis_nota="{{$nota->jenis_nota}}" nota_id="{{$nota->nota_id}}"><i class="fa fa-th-list"></i>Tanda Terimas</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="{{ $b->barang_id }}" class="card-main">
                     <table class='table table-striped table-bordered' style="margin-bottom: 0;">
                        <thead>
                           <tr class="info">
                              <th class="th-center font-s2"><strong>Kode</strong></th>
                              <th class="th-center font-s2"><strong>Nama Barang</strong></th>
                              <th class="th-center font-s2"><strong>Jumlah</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($detail_data->sortBy('nama') as $i => $b)
                              @if($b->nota_id == $nota->nota_id)
                                 <tr>
                                    <td class="font-s2" align="center" valign="center">{{ $b->kode }}</td>
                                    <td class="font-s2" align="center" valign="center">{{ $b->nama }}</td>
                                    <td class="font-s2" align="center" valign="center">{{ $b->jumlah }}</td>
                                 </tr>
                                 <?php unset($isi_mr_subdepts[$i]); ?>
                              @endif
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         @endforeach
         </div>
      </div>
   </div>
</div>

@if (session()->has('sisa_brg'))
<?php $sisa_brg = session('sisa_brg'); ?>
<form method="post" action="{{ route('kembali.barang') }}">
   {{ csrf_field() }}
   <input type="hidden" name="pengambilan_barang_id" value="{{ $sisa_brg[0]['pengambilan_barang_id'] }}">
   <div id="modal_sisa_barang" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Sisa Pengambilan Barang</h4>
            </div>
            <div class="modal-body">
               <div class="box">
                  <table class='table table-striped table-bordered'>
                     <thead>
                        <tr class="success">
                           <th>Kode</th>
                           <th>Nama</th>
                           <th>Sisa</th>
                        </tr>
                     </thead>
                     <tbody id="">
                        @foreach($sisa_brg as $s)
                        <input type="hidden" name="{{ $s['barang_id'] }}" value="{{ $s['jumlah'] }}">
                        <tr>
                           <td>{{ $s['kode'] }}</td>
                           <td>{{ $s['nama'] }}</td>
                           <td>{{ $s['jumlah'] }}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-info">Kembalikan Barang</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</form>
@endif
<div class="modal fade" id="modalTandaTerima" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Daftar Tanda Terima</h4>
         </div>
         <div class="modal-body" style="height: 200px;" id="table_dashboard">
            <table class="table table-bordered" id="tableTandaTerima">
               <thead>
                 <tr>
                   <th>Tgl Cetak</th>
                   <th>Tgl Terima</th>
                   <th>User</th>
                   <th>Diterima</th>
                 </tr>
               </thead>
            </table>
         </div>
         <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
         </div>
      </div>
   </div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">
   $(function () {
      var barang_habis = "<?php echo $barang_habis; ?>";
      // console.log(barang_habis);
      if(barang_habis == 0) {
         // console.log("ada sisa barang");
         $('#modal_sisa_barang').modal('show');
      }

      $('div[data-toggle="collapse"]').click(function(e) {
         e.stopPropagation();
      });
   });

   $('.btnTandaTerima').click(function(event){
      url = "{{ route('pb.listTandaTerimaPengirimanajax') }}";
      id = $(this).attr('nota_id');
      jenis_nota = $(this).attr('jenis_nota');
      $.ajax({
         url: url,
         data:{
            "id":id,
            "jenis_nota":jenis_nota,
         }
      }).done(function(data){
         // console.log(data);
         tableTandaTerima.clear().draw();
         tableTandaTerima.rows.add(data).draw();
         $('#modalTandaTerima').modal('show');
      }).fail(function(){
         swal('Oops','Gagal mengambil data','error');
      })
   });

   var tableTandaTerima = $('#tableTandaTerima').DataTable({
      'processing': true,
      'serverside':true,
      "pageLength": 20,
      "lengthChange": false,
      "searching": false,
      columns: [
        { "data": "tgl_cetak" },
        { "data": "tgl_terima" },
        { "data": "user" },
        { "data": "diterima" },
        ],
   });
</script>
@endpush
