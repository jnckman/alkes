@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endpush

@section('header22')
   <i class="fa fa-hand-paper-o" aria-hidden="true"></i> Isi Sub Department MR
@endsection

@section('breadcrumb')
<li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
<li><a href="#" onclick="history.back();">List Department</a></li>
<li><a href="#" onclick="history.back();">MR by System</a></li>
<li><a href="#" onclick="history.back();">Sub Department MR List</a></li>
<li class="active">Isi Sub Department MR</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">

        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nomr}} | <i class="fa fa-money fa-fw" aria-hidden="true"></i> : Rp {{number_format($result[0]->total)}} | <i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->area}}</h5>
        </div>

      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-bordered example1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>Nama Barang</th>
                     <th>Kode Barang</th>
                     <th>Jumlah</th>
                     <th>Stok Akumulasi</th>
                     <th>Satuan</th>
                     <th>Harga Average</th>
                     <th>Harga Average*Jumlah</th>
                     <th>Keterangan</th>
                     <th>Action</th>
                 </tr>
          </thead>
          <tfoot>
              <tr>
                          <th colspan="6" style="text-align:right">Total:</th>
                          <th colspan="3"></th>
                      </tr>

          </tfoot>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>


@endsection

@push('bottom')
<script type="text/javascript">
    $(document).ready(function() {

          $('.example1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../isisubdeptmrdtajax/{{$id}}',
              "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                     'Total : '+total.toLocaleString("en") +',-'
                );
            },

          });


      } );
</script>
@endpush
