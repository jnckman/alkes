@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/funkyradio.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Pengiriman Barang</li>
@endsection

@section('content')
<div class="container">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="stepwizard">
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-success btn-lg"><i class="fa fa-paper-plane" aria-hidden="true"></i> Jenis Pengiriman</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-crosshairs" aria-hidden="true"></i> List MR/SO</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Ambil Barang</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-plus" aria-hidden="true"></i> Bagi Barang</a>
            </div>
         </div>
     </div>
  </div>
</div>

<div class="container">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="row" style="margin-bottom: 20px;">
            
            <div class="col-md-2">
            
               <h5><strong>Sales Order</strong></h5>
               
               <div class="funkyradio">
                  
                  <div class="funkyradio-success">
                     <input type="radio" name="radio" id="radio2" value="2"/>
                     <label for="radio2">Sales Order</label>
                  </div>
               </div>
            
            </div>
            <div class="col-md-2">
               <h5><strong>Pengiriman</strong></h5>
               
               <div class="funkyradio">
                  
                  <div class="funkyradio-warning">
                     <input type="radio" name="radio2" id="radio4" value="2"/>
                     <label for="radio4">Baru</label>
                  </div>
               </div>
            </div>

            <div class="col-md-2">
               <h5><strong>Pilih Jenis</strong></h5>
               
               <select required id="pilih_periode" name="gudang_id" class="selectpicker show-menu-arrow form-control" data-style="btn-primary" title="....." data-live-search="true">
                  <option value="0" selected="">NON MR</option>
                  @foreach($periode as $p)
                  <option value="{{ $p->id }}">{{ $p->judul }} | {{$p->mr_subdept()->count()}} MR</option>
                  @endforeach
               </select>
            </div>
            <div class="col-md-6">
            <button onclick="next(2)" class="btn btn-info btn-lg col-md-12" style="margin: 20px 0 0 0;"><strong style="font-size:20px;">Next</strong></button>
            </div>
         </div>
     </div>
  </div>
</div>
@endsection

@push('bottom')
<script type="text/javascript">

   function next(id) {
      // console.log(id);
      var radio = $("input[name='radio']:checked").val();
      var radio2 = $("input[name='radio2']:checked").val();
      var periode = $('#pilih_periode option:selected').val();
      //console.log(periode);
      if (!radio) {
         //console.log("radio = " + radio);
         swal({
            title: "Silakan Pilih MR atau SO",
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Tutup',
            closeOnConfirm: true
         });
      }
      else if (!radio2) {
         swal({
            title: "Silakan Pilih Pengiriman",
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Tutup',
            closeOnConfirm: true
         });
      }
      else {
         var tipe, kirim;
         if (radio == 1) tipe = "Material Request";
         else tipe = "Sales Order";

         if (radio2 == 1) kirim = "Retur";
         else kirim = "Baru";

         swal({
            title: "Anda memilih "+ tipe +", pengiriman "+ kirim +" ?",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ya',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
         },
         function(isConfirm) {
            if (isConfirm) {
               // console.log(id);
               window.location.href = "{{ CRUDBooster::adminPath() }}/kirim-barang/"+radio+"/"+radio2+"/"+1+"/"+periode;
            }
         });

      }
   }

</script>
@endpush
