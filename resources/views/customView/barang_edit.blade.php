@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap3.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/custom-container.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/wawan.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
</style>
@endpush

@section('content')
<p><a href="#" onclick="window.history.back(); return false;"><i class='fa fa-chevron-circle-left'></i> &nbsp; Back To List Data Barang</a></p>

<div class="custom-container">
   <div class="panel panel-default">
      <div class="panel-heading"><strong>Edit Barang Form</strong></div>
      <form id="edit_form" method="post" enctype="multipart/form-data" action="{{ route('edit.barang')}}">
         {{ csrf_field() }}
         <input type="hidden" name="id" value="{{ $barang->id }}">
         <div class="panel-body">
            <div class="col-md-12">
               <div class="form-group">
                  <label>Nama Barang</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="nama" readonly placeholder="{{ $barang->nama }}">
                  @else
                  <input type="text" class="form-control" name="nama" placeholder="{{ $barang->nama }}">
                  @endif
               </div>
               <div class="form-group">
                  <label>Kode Barang</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="kode" readonly placeholder="{{ $barang->kode }}">
                  @else
                  <input type="text" class="form-control" name="kode" placeholder="{{ $barang->kode }}">
                  @endif
               </div>
               <div class="form-group">
                  <label>Batch / Lot</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="batch" readonly placeholder="{{ $barang->batch }}">
                  @else
                  <input type="text" class="form-control" name="batch" placeholder="{{ $barang->batch }}">
                  @endif
               </div>
               <div class="form-group">
                  <label>Akum Stok ( Perhatikan stok barang gudang anda )</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="akumstok" readonly placeholder="{{ $barang->akumstok }}">
                  @else
                  <input type="text" class="form-control" name="akumstok" placeholder="{{ $barang->akumstok }}">
                  @endif
               </div>
               <div class="form-group">
                  <label>Expired Barang</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="expired" readonly placeholder="{{ $barang->expired }}">
                  @else
                  <input type="text" class="form-control" name="expired" placeholder="{{ $barang->expired }}">
                  @endif
               </div>
               <div class="form-group">
                  <label>Nama Barang</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="nama" readonly placeholder="{{ $barang->nama }}">
                  @else
                  <input type="text" class="form-control" name="nama" placeholder="{{ $barang->nama }}">
                  @endif
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Merk</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="jenis" placeholder="{{ $barang->jenis }}" readonly>
                  @else
                  <input type="text" class="form-control" name="jenis" placeholder="{{ $barang->jenis }}">
                  @endif
               </div>

               <div class="form-group col-md-4">
                  <label>Ukuran</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="ukuran" placeholder="{{ $barang->ukuran }}" readonly>
                  @else
                  <input type="text" class="form-control" name="ukuran" placeholder="{{ $barang->ukuran }}">
                  @endif
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Tipe</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="tipe" placeholder="{{ $barang->tipe }}" readonly>
                  @else
                  <input type="text" class="form-control" name="tipe" placeholder="{{ $barang->tipe }}"> 
                  @endif
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Min Stok</label>
                  <input type="number" min="1" class="form-control" name="min" placeholder="{{ $barang->min }}">
               </div>

               <div class="form-group col-md-4">
                  <label>Max Stok</label>
                  <input type="number" min="1" class="form-control" name="max" placeholder="{{ $barang->max }}">
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Status</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="status" placeholder="{{ $barang->status }}" readonly>
                  @else
                  <input type="text" class="form-control" name="status" placeholder="{{ $barang->status }}">
                  @endif
               </div>

               <div class="form-group col-md-3 padding-l0">
                  <label>Harga Beli</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="number" min="1" class="form-control" name="harga_beli_akhir" placeholder="{{ $barang->harga_beli_akhir }}" readonly>
                  @else
                  <input type="number" min="1" class="form-control" name="harga_beli_akhir" placeholder="{{ $barang->harga_beli_akhir }}">
                  @endif
               </div>

               <div class="form-group col-md-3">
                  
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <label>Harga Beli</label>
                  <!-- <input type="number" min="1" class="form-control" name="harga_jual" placeholder="{{ $barang->harga_jual }}" readonly> -->
                  @else
                     
                        <label>Harga Beli </label>
                        <select class="form-control" name='priclists_id'>
                           @foreach($hargajual as $h)
                              @if($hargajual[0]->priclists_id == $h->id)
                              <option value="{{$h->id}}">{{$h->harga}} | {{$h->namasup}} [terakhir]</option>
                              @else
                              <option value="{{$h->id}}">{{$h->harga}} | {{$h->namasup}}</option>
                              @endif
                           @endforeach
                        </select>
                     
                  <!-- <input type="number" min="1" class="form-control" name="harga_jual" placeholder="{{ $barang->harga_jual }}"> -->
                  @endif
               </div>

               <div class="form-group col-md-3">
                  <label>Average Price</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="avgprice" placeholder="{{ $barang->avgprice }}" readonly>
                  @else
                  <input type="text" class="form-control" name="avgprice" placeholder="{{ $barang->avgprice }}">
                  @endif
               </div>

               <div class="form-group col-md-3 padding-r0">
                  <label>PPN</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="ppn" placeholder="{{ $barang->ppn }}" readonly>
                  @else
                  <input type="text" class="form-control" name="ppn" placeholder="{{ $barang->ppn }}">
                  @endif
               </div>

               <div class="form-group col-md-6 padding-l0">
                  <label>Kategori</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <select class="form-control selectpicker" data-live-search="true" title="pilih kategori" disabled>
                     @foreach($kategori as $k)
                        @if($k->id == $barang->kategoris_id)
                           <option selected value="{{ $k->id }}">{{ $k->nama }}</option>
                        @else
                           <option value="{{ $k->id }}">{{ $k->nama }}</option>
                        @endif
                     @endforeach
                  </select>
                  <input name="kategoris_id" type="hidden" value="{{$barang->kategoris_id}}"/>
                  @else
                  <select class="form-control selectpicker" name="kategoris_id" data-live-search="true" title="pilih kategori">
                     @foreach($kategori as $k)
                        @if($k->id == $barang->kategoris_id)
                           <option selected value="{{ $k->id }}">{{ $k->nama }}</option>
                        @else
                           <option value="{{ $k->id }}">{{ $k->nama }}</option>
                        @endif
                     @endforeach
                  </select>
                  @endif
               </div>

               <div class="form-group col-md-6 padding-r0">
                  <label>Satuan</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <select class="form-control selectpicker" name="satuans_id" data-live-search="true" title="pilih satuan" disabled>
                     @foreach($satuan as $s)
                        @if($s->id == $barang->satuans_id)
                           <option selected value="{{ $s->id }}">{{ $s->nama }}</option>
                        @else
                           <option value="{{ $s->id }}">{{ $s->nama }}</option>
                        @endif
                     @endforeach
                  </select>
                  <input name="satuans_id" type="hidden" value="{{$barang->satuans_id}}"/>
                  @else
                  <select class="form-control selectpicker" name="satuans_id" data-live-search="true" title="pilih satuan">
                     @foreach($satuan as $s)
                        @if($s->id == $barang->satuans_id)
                           <option selected value="{{ $s->id }}">{{ $s->nama }}</option>
                        @else
                           <option value="{{ $s->id }}">{{ $s->nama }}</option>
                        @endif
                     @endforeach
                  </select>
                  @endif
               </div>

               <div class="form-group">
                  <label>Spesifikasi</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <textarea name="spesifikasi" class="form-control" rows="5" readonly>{{ $barang->spesifikasi }}</textarea>
                  @else
                  <textarea name="spesifikasi" class="form-control" rows="5">{{ $barang->spesifikasi }}</textarea>
                  @endif
               </div>

               <div class="form-group">
                  <label>Cara Pakai</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <textarea name="cara_pakai" class="form-control" rows="8" readonly>{{ $barang->cara_pakai }}</textarea>
                  @else
                  <textarea name="cara_pakai" class="form-control" rows="8">{{ $barang->cara_pakai }}</textarea>
                  @endif
               </div>

               <div class="form-group">
                  <label>Link Video</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="text" class="form-control" name="video" placeholder="{{ $barang->video }}" readonly>
                  @else
                  <input type="text" class="form-control" name="video" placeholder="{{ $barang->video }}">
                  @endif
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Gambar</label>
                  @if(CRUDBooster::myPrivilegename()=='PO-2')
                  <input type="file" class='form-control input-gambar' name="gambar" disabled/>
                  @else
                  <input type="file" class='form-control input-gambar' name="gambar"/>
                  <div id="div-preview" class="col-sm-6 padding-l0" style="margin-top: 10px;">
                     <img src="{{ asset($barang->gambar) }}" id="preview" style="width:300px;">
                  </div>
                  @endif
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Submit</button>
            <div class="clearfix"></div>
         </div>
      </form>
   </div>
</div>


@endsection

@push('bottom')
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
   $('.input-gambar').on('change', function() {
      var reader = new FileReader();

      reader.onload = function (e) {
         $('#preview').attr("src", e.target.result);
      };
      reader.readAsDataURL(this.files[0]);

      $('#div-preview').show();
   });

   $('#edit_form').submit(function(e){
      e.preventDefault();
      var form = this;
      swal({
         title: "Sudah benar ?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: '#DD6B55',
         confirmButtonText: 'Ya',
         cancelButtonText: "Tidak",
         closeOnConfirm: false,
         closeOnCancel: true
      },
      function(isConfirm) {
         if (isConfirm) {
            form.submit();
         }
      });
   });
</script>
@endpush
