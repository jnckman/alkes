@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;padding-left: 20px;padding-right: 20px;">
        <h5>Jurnal</h5>
        <form class="form-inline" action="jurnalbulan" method="post">
          <div class="form-group">
            <label for="email">Bulan</label>
            <select class="form-control" name="bulan" >
            <option class="" value="1">Januari</option>
            <option class="" value="2">Februari</option>
            <option class="" value="3">Maret</option>
            <option class="" value="4">April</option>
            <option class="" value="5">Mei</option>
            </select>
          </div>
           {{ csrf_field() }}
          <div class="form-group" style="margin-left: 20px;">
            <label for="pwd">Tahun</label>
            <select class="form-control" name="tahun">
            <option class="" value="2017">2017</option>
            <option class="" value="2018">2018</option>
            <option class="" value="2019">2019</option>
            <option class="" value="2020">2020</option>
            <option class="" value="2021">2021</option>
            </select>
          </div>

          
          <div class="form-group" style="margin-left: 20px;">
          <label>Option</label>
          <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </form>
      
      </div>

      
     </div>


@endsection

@section('datatable')

@endsection

