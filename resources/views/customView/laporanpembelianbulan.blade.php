@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">

          <h5><i class="fa fa-calendar fa-fw" aria-hidden="true"></i> Laporan Pembelian : {{$data['bulan']}} - {{$data['tahun']}} </h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../isipurchaseorder/{{$id}}"><button class="btn btn-info">Kembali</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       
         
         
         <div class="col-md-12" style="margin-top: 10px;">

         <div class="col-md-12" style="margin-bottom: 10px;">
            <a href="printspbm">
              <button class="btn btn-info btn-xs">Print Laporan Pembelian</button>
            </a>
         </div>
         <div class="col-md-12">

         <table class="table table-bordered example1 table-responsive" id='' style="width:1600px;">
           <thead>
                 <tr class="success">
                     <th>TGL FAKTUR</th>
                     <th>NO FAKTUR</th>
                     <th>NO FAKTUR PAJAK</th>
                     <th>SUPPLIER</th>
                     <th>NAMA BARANG</th>
                     <th>HARGA @</th>
                     <th>QTY DTG</th>
                     <th>QTY BELI</th>
                     <th>BRUTO</th>
                     <th>DISC</th>
                     <th>RP(DISC)</th>
                     <th>DPP</th>
                     <th>PPN</th>
                     <th>TOTAL PB</th>
                     <th>MASA PAJAK</th>
                     <th>TOTAL HUTANG</th>
                     <th>JATUH TEMPO</th>
                     <th>HPP/PCS</th>
                     
                     

                 </tr>
          </thead>
          <tbody>
              
              @foreach($datasimpan as $d)
              
              <?php dd($d->Purchaseorder); ?>
              <tr>
                

                <td>{{$d->tglterimafaktur}}</td>
                <td>{{$d->nofaktur}}</td>
                <td>{{$d->noefaktur}}</td>
                <td>{{$d->Purchaseorder}}</td>
                
              </tr>
              

              

              @endforeach
          </tbody>
         </table>
         </div>
         <div class="col-md-7">

         
         
         </div>
         </div>
       
       
       </div>
       </div>
     </div>
   </div>


@endsection

@section('datatable')

@endsection

