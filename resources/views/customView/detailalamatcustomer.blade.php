@extends('crudbooster::admin_template')

@section('content')
<!-- Your custom  HTML goes here -->
<a href="{{CRUDBooster::mainpath($slug=NULL)}}"><button class="btn btn-danger" style="margin-bottom: 20px;">Kembali</button></a>
<input id="routeGetPriceList" type="hidden" value="{{ route('getPricelist') }}">
<form action='submit' method='POST'>
<div class="box box-success">
  {{csrf_field()}}
  <div class="box-body">
  <div class="col-md-12">
    <div class="col-md-6 form-group">
      <label>Alamat</label>
      <input type="text" name="alamat" class="form-control" placeholder="Input Address Here">
    </div>
    <div class="col-md-3 form-group">
      <label>Option</label>
      <button type="submit" class="btn btn-success" style="width: 100%;">Add New</button>
    </div>

    <input type="hidden" name="customers_id" value="{{$id}}">
  </div>

  </div>
</div>
</form>

<div class="box">

   <table class='table table-striped table-bordered'>
     <thead>
         <tr>
            <th>Alamat</th>

            <th>Option</th>
         </tr>
     </thead>
     <tbody>
       @foreach($data as $d)
         <tr>
            <td>{{ $d->alamat }}</td>

            <td>
            <a class="btn btn-xs btn-success btn-edit" title="Edit Data" href="../alamats/edit/{{$d->id}}?return_url=http%3A%2F%2Flocalhost%3A8080%2Fwellhosproject%2Fpublic%2Fadmin%2Fdetailrek%2F{{$id}}"><i class="fa fa-pencil"></i></a>
            </td>
            <td>
              <a class="btn btn-xs btn-warning btn-delete" title="Delete" href="javascript:;" onclick="swal({
                      title: &quot;Are you sure ?&quot;,
                      text: &quot;You will not be able to recover this record data!&quot;,
                      type: &quot;warning&quot;,
                      showCancelButton: true,
                      confirmButtonColor: &quot;#ff0000&quot;,
                      confirmButtonText: &quot;Yes!&quot;,
                      cancelButtonText: &quot;No&quot;,
                      closeOnConfirm: false },
                      function(){  location.href=&quot;../alamats/delete/{{$d->id}}&quot; });"><i class="fa fa-trash"></i></a>
            </td>

          </tr>
       @endforeach
     </tbody>
   </table>

   <!-- ADD A PAGINATION -->

</div>


@endsection
