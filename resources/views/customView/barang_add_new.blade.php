@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap3.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/custom-container.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/wawan.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
</style>
@endpush


@section('content')
<div class="custom-container">
   <div class="panel panel-default">
      <div class="panel-heading"><strong>Add Barang Form</strong></div>
      <form method="post" enctype="multipart/form-data" action="{{ route('add.barang')}}">
         {{ csrf_field() }}
         <div class="panel-body">
            <div class="col-md-12">
               <div class="form-group">
                  <label>Kode Barang</label>
                  <input type="text" class="form-control" name="kode" required placeholder="kode barang">
               </div>
               <div class="form-group">
                  <label>Nama Barang</label>
                  <input type="text" class="form-control" name="nama" required placeholder="nama barang">
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Merk</label>
                  <input type="text" class="form-control" name="jenis" required placeholder="jenis barang">
               </div>

               <div class="form-group col-md-4">
                  <label>Ukuran</label>
                  <input type="text" class="form-control" name="ukuran" required placeholder="ukuran barang">
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Tipe</label>
                  <input type="text" class="form-control" name="tipe" required placeholder="tipe barang">
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Min Stok</label>
                  <input type="number" min="1" class="form-control" name="min" required placeholder="min stok">
               </div>

               <div class="form-group col-md-4 ">
                  <label>Max Stok</label>
                  <input type="number" min="1" class="form-control" name="max" required placeholder="max stok">
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Status</label>
                  <input type="text" class="form-control" name="status" required placeholder="status">
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Batch List</label>
                  <input type="text" class="form-control" name="batch" required placeholder="kode batch">
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Expired</label>
                  <input type="text" class="form-control" name="expired" required placeholder="Expired">
               </div>

               <input type="hidden" min="1" class="form-control" name="harga_jual" required value="0">
               <input type="hidden" min="1" class="form-control" name="harga_beli" required placeholder="harga beli" value="0">

               <div class="form-group col-md-4 padding-r0">
                  <label>HPP</label>
                  <input type="text" class="form-control" name="avg_price" required>
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>PPN</label>
                  <input type="text" class="form-control" name="ppn"  required>
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Kategori</label>
                  <select class="form-control selectpicker" name="kategori" data-live-search="true" title="pilih kategori" required>
                     @foreach($kategori as $k)
                     <option value="{{ $k->id }}">{{ $k->nama }}</option>
                     @endforeach
                  </select>
               </div>

               <div class="form-group col-md-4 padding-r0">
                  <label>Satuan</label>
                  <select class="form-control selectpicker" name="satuan" data-live-search="true" title="pilih satuan" required>
                     @foreach($satuan as $s)
                     <option value="{{ $s->id }}">{{ $s->nama }}</option>
                     @endforeach
                  </select>
               </div>

               <div class="form-group col-md-6 padding-l0">
                  <label>Spesifikasi</label>
                  <textarea name="spek" class="form-control" rows="5"></textarea>
               </div>

               <div class="form-group col-md-6">
                  <label>Cara Pakai</label>
                  <textarea name="cp" class="form-control" rows="8"></textarea>
               </div>

               <div class="form-group col-md-6">
                  <label>Link Video</label>
                  <input type="text" class="form-control" name="video" required>
               </div>

               <div class="form-group col-md-4 padding-l0">
                  <label>Gambar</label>
                  <input type="file" class='form-control input-gambar' name="gambar"/>
                  <div id="div-preview" class="col-sm-6 padding-l0" style="display: none; margin-top: 10px;">
        				   <img id="preview" style="width:300px;">
        			   </div>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Submit</button>
            <div class="clearfix"></div>
         </div>
      </form>
   </div>
</div>


@endsection

@push('bottom')
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
   $('.input-gambar').on('change', function() {
      var reader = new FileReader();

      reader.onload = function (e) {
         $('#preview').attr("src", e.target.result);
      };
      reader.readAsDataURL(this.files[0]);

      $('#div-preview').show();
   });
</script>
@endpush
