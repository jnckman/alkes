@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> List Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">List Pengiriman</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <div class="col-md-2" style="padding: 0px;border: solid 1px black;text-align: center;padding-top: 30px;padding-bottom: 30px;cursor: pointer;">
                  <a href="laporanmr/periodemr"><i class="fa fa-calendar fa-5x"></i><Br>Periode MR</a>

               </div>
            </div>
         </div>
      </div>

      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-6">
              <div class="chart-container" style="position: relative; height:40vh; width:80vw">
                <canvas id="bar-chart"></canvas>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.datatables.bootstrap.js') }}"></script>
<script src="{{ asset('customs/js/yajrabox.handlebars.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" /></script>
<script type="text/javascript">
   
      new Chart(document.getElementById("bar-chart"), {
          type: 'bar',
          data: {
            labels: <?php echo json_encode($data['judulperiode']); ?>,
            datasets: [
              {
                label: "Total MR (Rupiah)",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: <?php echo json_encode($data['nominalperiode']); ?>
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Total MR per Periode'
            },
            scaleLabel:
            function(label){return  '$' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
          }
      });


   
</script>

@endpush
