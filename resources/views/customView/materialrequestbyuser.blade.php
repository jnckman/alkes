<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../mronline/js/html5shiv.js"></script>
    <script src="../mronline/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
	<script src="{{ asset('customs/js/pace.min.js') }}"></script>
	<link href="{{ asset('customs/css/pace-minimal-blue.css')}}" rel="stylesheet" />
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					
					@include('customView.partial.navbar')
				</div>
			</div>
		</div><!--/header-middle-->
	
		
	</header><!--/header-->
	@include('customView.partial.warnings')
	@if($periodeada == 0)
	<div class='alert alert-warning'>
	<h4 class="text-center"><i class="icon fa fa-info"></i></h4>
	<h2 class="text-center">Tidak Ada Periode MR yang aktif</h2>
	<h4 class="text-center">silahkan hubungi admin untuk mengaktifkan periode mr</h4>
	</div>
	@endif
	<section id="slider"><!--slider-->
		<div class="container">
			<ol class="breadcrumb">
				  <li>Material Request</li>
				</ol>

		</div>
	</section><!--/slider-->
	
	<section style="padding-bottom: 100px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-4" style="padding: 0px;">
					<div class="col-sm-12">
						<h5 style="text-align: center;">Tutorial Pengajuan MR</h5>
					</div>
					<div class="col-sm-12" style="padding: 0px;">
						<ol>
							<li style="text-align: justify;">Memilih Periode. Periode yang dapat dipilih adalah periode yang sedang berlangsung dan periode untuk 3 bulan kedepan.</li>
							<li style="text-align: justify;">Memilih Department, Subdepartment, dan Project</li>
							<li style="text-align: justify;">Tekan Tombol Next dibawah tombol "Tambah Project" atau Home pada Navbar diatas</li>
							<li style="text-align: justify;">Pada halaman home, menambahkan barang ke dalam keranjang berdasarkan project yang dipilih</li>
							<li style="text-align: justify;">Menekan Tombol Checkout (<a class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>)</li>
							<li style="text-align: justify;">Pada halaman Checkout, pastikan barang sesuai kebutuhan lalu tekan tombol "Buat material request"</li>

						</ol>
							<br><br><br>
					</div>
				</div>
				<div class="col-sm-8 padding-right">
					<div class="col-sm-12">
						<div class="features_items"><!--features_items-->
							
							
						</div><!--features_items-->
						
						<div class="category-tab"><!--category-tab-->
							<div class="tab-content">
								<div class="tab-pane fade active in">
									<!-- TODO: CSS button ? -->
									<form id="formProjects" action="{{route('subdept.mr.postProject')}}" method="post">
										{{ csrf_field() }}
										<div class="form-group">
											<label>Pilih Periode :</label>
											<select class="form-control" required id="select2periode" name="periode"><option></option></select>
										</div>
										<div class="form-group">
											<label>Pilih Department :</label>
											<select class="form-control" id="select2depts"><option></option></select>
										</div>
										<div class="form-group">
											<label>Pilih Sub Department / ASM :</label>
											<select class="form-control" id="select2subdepts"><option></option></select>
										</div>
										<div class="form-group">
											<label>Pilih Project :</label>
											<select class="form-control" id="select2projects" required name="projects"><option></option></select>	
										</div>
									<div class="form-group">
											<button type="button" class="btnSubmit btn btn-info " style="width: 100%;">Tambah Project</button>
									</div>
									
									</form>
								</div>
							</div>
						</div><!--/category-tab-->
						<div class="lds-css ng-scope" style="display: none" id="wait">
						  <div style="width:100%;height:100%" class="lds-rolling">
					    	<div></div>
						</div>
						</div>
					</div>
					<div class="col-sm-12" style="margin-top: 80px;">
					@if($projectslist)
					<div class="row">
						<h4 style="text-align: center;"><u>List Project</u></h4>
					</div>
									<div class="row">
										<div class="table">
						    				<table class="table table-hover table-striped">
						    					<thead>
						    						<th>No</th>
							    					<th>Project</th>
							    					<th>Subdept</th>
							    					<th>Action</th>
						    					</thead>
						    					<tbody>
						    						<?php $i =1; ?>
						    						@foreach($projectslist as $key => $value)
						    						<tr>
						    							<td>{{$i}}</td>
						    							<td>{{$value['text']}}</td>
						    							<td>{{$value['subdept']['nama']}}</td>
						    							<td><button class="btn btn-danger btn-xs btnHapusProjects" projects_id="{{$key}}"><li class="fa fa-trash-o"></li></button></td>
						    						</tr>
						    						<?php $i++?>
						    						@endforeach
						    					</tbody>
						    				</table>
										</div>
									</div>
					@else
					<h4 class="text-center">Belum menambahkan Project</h4>
					@endif
					</div>
					<div class="col-sm-12">
						@if($projectslist)
						<a href="{{ url('/mrmainpage/')}}" class="btn btn-success pull-right" style="width: 100%">Lanjutkan</a>
						@endif
					</div>
				</div>
			</div>
			<br/>
			
		</div>
	</section>
	
	<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
		
		<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
		
		
	</footer><!--/Footer-->
  
    <script src="{{asset('mronline/js/jquery.js')}}"></script>
	<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('mronline/js/price-range.js')}}"></script>
    <script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('mronline/js/main.js')}}"></script>
    <script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('customs/js/select2.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	var project_text='';
	var setuju = '{{$setuju}}';
	var projects1 = <?php echo $projects?>;
	var subdepts = <?php echo $subdepts?>;
	// console.log(subdepts);
	var privilege = <?php echo $privilege?>;
	// console.log(privilege);
	var periode = <?php echo $periode?>;
	var departments = <?php echo $departments;?>;
	var nama_dept = <?php echo $nama_dept?>;
	var subdept_nama= '';
	var isOPD = 0;

	// $(document).ajaxStart(function(){
 //        $("#wait").css("display", "block");
 //    });
 //    $(document).ajaxComplete(function(){
 //        $("#wait").css("display", "none");
 //    });

    //======================================START configurasi select START==================================================
    $('#select2periode').select2({
    	data:periode,
    	placeholder:'Pilih Periode'
    })//.val('{{session()->get("cart")->periode}}').trigger('change');
    ;

    var periode_id;

	$(document).on('select2:selecting','#select2periode',function(e){
    	periode_id = e.params.args.data.id;
    	if(parseInt(privilege[1]) <= 1 ){
    		getProjectMr0();
    	}
    })//.val('{{session()->get("cart")->periode}}').trigger('change');
    ;    

    $('#select2projects').select2({
    	data:projects1,
    	placeholder:'Pilih Project',
    })
    ;

    $('#select2subdepts').select2({
    	data:subdepts,
    	placeholder:'Pilih Department terlebih dahulu'
    })//.val('{{session()->get("cart")->subdepts["id"]}}').trigger('change');
	;

    $('#select2depts').select2({
    	data:departments,
    	placeholder: 'Pilih Department',
    })//.val('{{session()->get("cart")->subdepts["dept_id"]}}').trigger('change')
    
    //======================================END configurasi select END==================================================

    function configSelectByPrivilege(){
    	if(parseInt(privilege[1]) <= 1 ){
			var projects1 = '';
    		$('#select2depts').val($('#select2depts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
	    	$('#select2subdepts').val($('#select2subdepts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
	    	nama_dept = $('#select2depts').select2('data')[0].text;
	    	subdept_nama = $('#select2subdepts').select2('data')[0].text;
	    	// console.log($('#select2subdepts').val());
	    	id = $('#select2subdepts').select2('data')[0].id;
	    	// console.log(id);
	    	if(nama_dept.toLowerCase()=='opd') {
		  			dept = 'opd';
		  		}
	  		else {
	  			dept = 'notOpd';
	  		}
		}
		else if(parseInt(privilege[1]) < 4 ){
			var projects1 = '';
			$('#select2depts').val($('#select2depts option:nth-child(2)').val()).trigger('change').prop('disabled', true);
			nama_dept = $('#select2depts').select2('data')[0].text;
		}
    }

    configSelectByPrivilege();

    $(document).on('select2:select','#select2depts',function(){
	    var id = $(this).val();
	    nama_dept = $('#select2depts').select2('data')[0].text;
	    if(nama_dept.toLowerCase()=='opd') isOPD = 1;
	    else isOPD =0;
	    if(id){
	        url = '<?php echo url('/'); ?>'+'/mrmainpage/getSubdept/'+id;
	        $.ajax({
	        	url:url,
	        	data:{
	        		'id':id,
	        		'dept':nama_dept,
	        	}
	        }).done(function (gg){
	        	// console.log(gg);
	          if(gg == "null"){
	            subdepts = null;
	          }
	          else{
	            gg = jQuery.parseJSON(gg);
	            subdepts = gg;
	          }
	          $('#select2subdepts').empty().select2({
	            placeholder:'Pilih Department terlebih dahulu',
	            data: subdepts
	          }).val('').trigger('change');
	        }).fail(function(){
	        	swal('Gagal Mengambil Data !', 'Silahkan Coba Lagi', 'error').catch(swal.noop);
	        	// console.log('Terjadi Kesalahan');
	        	resetForm();
	        });
	    }
	    else {
	      $('#select2subdepts, #select2projects').empty().select2({
	        placeholder: 'Pilih Department terlebih dahulu',
	        data: null
	      }).trigger('change');
	    }
  	});

  	function resetForm(){
  		$('#select2periode').val('').trigger('change');
    	$('#select2depts').val('').trigger('change');
    	$('#select2subdepts').val('').trigger('change');
		$('#select2projects').val('').trigger('change');
  	}

  	$(document).on('select2:selecting','#select2subdepts',function(e){
  		id = e.params.args.data.id;
  		subdept_nama = e.params.args.data.text;
  		url = '{{route("mr.getUser.ajaxGetProject")}}';
  		if(nama_dept.toLowerCase()=='opd') {
  			dept = 'opd';
  		}
  		else {
  			dept = 'notOpd';
  		}
  		$.ajax({
  				url : url,
  				data:{
  					'id' : id,
  					'dept' : dept,
  					'periode_id' : periode_id,
  				}
  			}).done(function(response){
  				$('#select2projects').empty().select2({
			    	data:response,
			    	placeholder:'Pilih Project'
			    }).val('').trigger('change');
  			}).fail(function(){
  				swal('Gagal Mengambil Data !', 'Silahkan Coba Lagi', 'error').catch(swal.noop);
  				resetForm();
  			})
  	});


    $("#select2projects").val("{{session()->get('mr_projects')[0]}}").trigger('change');

    $(document).on('select2:selecting','#select2projects',function(e){
    	project_text = e.params.args.data.text;
    });

    $('.btnSubmit').click(function(){
    	$('#formProjects').append(
    		'<input name="project_text" value="'+project_text+'" type="hidden"/>'+
    		'<input name="subdept_nama" value="'+subdept_nama+'" type="hidden"/>'+
    		'<input name="dept_nama" value="'+nama_dept+'" type="hidden"/>'+
    		'<input name="dept_id" value="'+$('#select2depts').val()+'" type="hidden"/>'+
    		'<input name="subdepts" value="'+$('#select2subdepts').val()+'" type="hidden"/>'
		);
		$('#formProjects').submit();
    });

    $(document).on('click','.btnHapusProjects',function(e){
    	url = '{{ route("subdept.mr.delCart") }}';
    	projects_id = $(this).attr('projects_id');
    	swal({
    		type :'warning',
          	html: 'Apakah anda yakin akan menghapus project ?',
		    showCancelButton: true,
		    confirmButtonColor: '#d2322d',
		    cancelButtonText: 'Cancel',
    	}).then(function(){
    		$.ajax({
	            type: "DELETE",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "projects" : projects_id
		        },
		    }).done(function(response) {
		    	// console.log(response);
		    	if(response.berhasil==1){
		    		swal('berhasil','Project berhasil dihapus dari Cart !','success')
		    		.then(function(){
		    			location.reload();
		    		});
		    	}else {
		    		swal('Oops...', 'Project gagal dihapus !', 'error');
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
    	}).catch(swal.noop);
    });

    if(setuju==0){
		$('#select2periode').select2("enable",false);
		$('#select2projects').select2("enable",false);
		$('#select2depts').select2("enable",false);
		$('#select2subdepts').select2("enable",false);
		$('.btnSubmit').prop('disabled', true);
	}
	
	function getProjectMr0(){
		$.ajax({
	  		url : '{{route("mr.getUser.ajaxGetProject")}}',
	        dataType: "json",
	        type: "GET",
	        data: {
	            'id' : id,
					'dept' : dept,
					'periode_id' : periode_id,
	        }
	  	}).done(function(response){
	  		console.log(response);
	  		$('#select2projects').empty().select2({
	    		placeholder : 'Pilih Project',
	    		data : response,
	    	}).val('').trigger('change');
	  	}).fail(function(){
	  		console.log('Gagal Mengambil Data');
	  	});
	}
});
</script>
<style type="text/css">
	.selected{
		border-bottom: 2px solid grey;

	}
	.select2{
		width: 100% !important; 
	}
</style>
</body>
</html>