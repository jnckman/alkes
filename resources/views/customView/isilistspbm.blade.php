@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../isipurchaseorder/{{$id}}"><button class="btn btn-info">Kembali</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       
         @foreach($hasil as $h)
         
         <div class="col-md-12" style="margin-top: 50px;">

         <div class="col-md-12" style="margin-bottom: 10px;">
            <a href="printspbm">
              <button class="btn btn-info btn-xs">Print SPBM</button>
            </a>
         </div>
         <div class="col-md-5">

         <table class="table table-bordered example1" id='table_dashboard' >
           <thead>
                 <tr class="success">
                     <th>nomor spbm</th>
                     <th>nomor faktur</th>
                     <th>nomor e-faktur</th>
                     <th>masa pajak</th>
                     <th>tanggal datang</th>
                     <th>supplier</th>
                     
                     

                 </tr>
          </thead>
          <tbody>
              <tr>
                <td>{{$h['nospbm']}}</td>
                <td>{{$h['nofaktur']}}</td>
                <td>{{$h['efaktur']}}</td>
                <td>{{$h['masapajak']}}</td>
                <td>{{$h['tanggaldatang']}}</td>
                <td>{{$h->supplier['nama']}}</td>
              </tr>
          </tbody>
         </table>
         </div>
         <div class="col-md-7">

         
         <table class="table table-bordered example1" id='table_dashboard'>
           <thead>
                 <tr class="info">
                     <th>nama barang datang</th>
                     <th>jumlah diterima</th>
                     <th>kekurangan</th>
                     <th>dipesan</th>
                     

                 </tr>
          </thead>
          <tbody>
              @foreach($h->isispbm as $i)
              
              <?php $diterima=0; ?>
              <?php $dipesan = $h->Purchaseorder->Isi_po($i->barangs_id)[0]['jumlah']; ?>
              <tr>
                <td>{{$i->Barangs_barangdatang['nama']}} | {{$i->Barangs_barangdatang['kode']}}</td>
                <td>{{$i->jumlah}}</td>
                <?php $diterima = $i->jumlah + $diterima; ?>
                <td>{{$dipesan-$diterima}}</td>
                <td>{{$h->Purchaseorder->Isi_po($i->barangs_id)[0]['jumlah']}}</td>
              </tr>
              @endforeach
          </tbody>
         </table>
         </div>
         </div>
         @endforeach
       
       </div>
       </div>
     </div>
   </div>


@endsection

@section('datatable')

@endsection

