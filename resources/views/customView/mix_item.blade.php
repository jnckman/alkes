@extends('crudbooster::admin_template')
@section('content')
@push('head')
<link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet" />
@endpush
<!-- <h2 class="text-center">Mix Items</h2> -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="stepwizard">
        <div class="stepwizard-row">
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',1) }}" type="button" class="btn btn-primary btn-circle">1</a>
                <p>Resep</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',2) }}" type="button" class="btn btn-default btn-circle">2</a>
                <p>Pilih Bahan</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',3) }}" type="button" class="btn btn-default btn-circle">3</a>
                <p>Finishing</p>
            </div>
        </div>
    </div>
	</div>
	<div class="panel-body">
		<form method='post' action="{{ route('reseps.aksi')}}">
		{{ csrf_field() }}
      <div class="row">
        <div class='form-group col-md-6'>
          <label>Nama</label>
          <input type='text' name='nama' required class='form-control' value="{{ Session::get('mix_1')[0]['nama'] }}" />
        </div>
        <div class='form-group col-md-6'>
          <label>Kode</label>
          <input type='text' name='kode' required class='form-control' value="{{ Session::get('mix_1')[0]['kode'] }}"/>
        </div>
      </div>
      <div class="row">
        <div class='form-group col-md-6'>
          <label>Satuan</label>
          <select name="satuans" required class="form-control select2satuans">
          </select>
        </div>
        <div class='form-group col-md-6'>
          <label>Jenis</label>
          <input type='text' name='jenis' required class='form-control' value="{{ Session::get('mix_1')[0]['jenis'] }}"/>
        </div>
      </div>
      <div class="row">
        <div class='form-group col-md-6'>
          <label>Kategori</label>
          <select type='text' name='kategoris' required class='form-control select2kategoris'></select>
        </div>
        <div class="form-group col-md-6">
          <label>Group</label>
          <select type='text' name='groups' class='form-control select2groups' required></select>
        </div>
      </div>
    </div>
    <div class='panel-footer'>
      <a href="{{ route('resep.mix.batal') }}" class="btn btn-danger">Cancel</a>
      <button type='submit' class='btn btn-primary pull-right'>
      	<input type="hidden" name="lanjut" value="k2"/>
      	Next</button>
      </form>
      <br>
      <br>
    </div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
	var kategoris = <?php echo($kategoris); ?>;
	var satuans = <?php echo($satuans); ?>;
  var groups = <?php echo($groups); ?>;

	var selKategori = "{{ Session::get('mix_1')[0]['kategoris'] }}";
	var selSatuan = "{{ Session::get('mix_1')[0]['satuan'] }}";
  var selGudang = "{{ Session::get('mix_1')[0]['gudang_tujuan'] }}";

$(function(){
	$(".select2kategoris").select2({
		data:kategoris,
    placeholder : "Belum ada Kategori"
	});
	$(".select2satuans").select2({
		data:satuans,
    placeholder : "Belum ada Satuan"
	});
  $(".select2groups").select2({
    data:groups,
    placeholder : "Belum ada Group"
  });

	if(selKategori){
		$(".select2kategoris").val(selKategori).trigger("change");
	};
	if(selSatuan){
		$(".select2satuans").val(selSatuan).trigger("change");
	};
  if(selGudang){
    $(".select2gudangs").val(selSatuan).trigger("change");
  };
})
</script>
@endpush
