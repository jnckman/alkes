@extends('crudbooster::admin_template')
@section('content')
@push('head')
<link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
@endpush
<div class="panel panel-default">
	<div class="panel-heading">
		<p>Split barang : <b>{{ $barangku->nama}}</b> ; Gudang : <b>{{ $gudangku->nama }}</b> ; Stock : <b>{{ $barang_gudangs->stok}} </b> </p>
	</div>
	<div class="panel-body">
		<form method="post" action="{{ route('barang_gudangs.postsplit') }}" id="formSplit">
			{{ csrf_field() }}
			<div class='form-group'>
				<label>Nama :</label>
				<input type="hidden" name="barang_gudangs_1" value="{{ $barang_gudangs->id }}">
				<input type='text' name='nama' required class='form-control' placeholder="Masukkan nama"  />
			</div>
			<div class='form-group'>
				<label>Kode :</label>
				<input type='text' name='kode' required class='form-control' placeholder="Masukkan kode"  />
			</div>
			<div class='form-group'>
				<label>Stok Baru :</label>
				<input type='number' name='pembagi' min="0" max="{{$barang_gudangs->stok}}" required class='form-control' placeholder="0" />
			</div>
			<div class="form-group">
	        	<label>Min</label>
	        	<input type="number" min="0" name="min" required class="form-control" placeholder="0"/>
	        </div>
	        <div class="form-group">
	        	<label>Max</label>
	        	<input type="number" name="max" min="0" required class="form-control" placeholder="0"/>
	        </div>
			<!-- <div class='form-group'>
				<label>Gudang TUjuan </label>
				<select name="gudangs" required class="form-control select2gudangs"></select>
			</div> -->
			<input type="hidden" name="gudangs" value="{{$barang_gudangs->gudangs_id}}">
			<button type="submit" class="btn btn-success pull-right">Submit</button>
		</form>
		<div>
			<!-- <p>Contoh pembagian :</p>
			<p>Barang X dengan jumlah stok 20 dibagi menjadi 2 maka setiap barang hasil split akan memiliki stok sebanyak 10 </p>
			<p>20 / 2 = 10</p> -->
		</div>
	</div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
var gudangs = <?php echo($gudangs); ?>;
var stok = <?php echo $barang_gudangs->stok; ?>;
$(function(){
	$(".select2gudangs").select2({
	    data:gudangs
	});

	// $("#formSplit").submit(function(e) {
	// 	var input = $("#formSplit input[name='pembagi']").val();
	// 	if(stok % input != 0) {
	// 		e.preventDefault();
	// 		swal('Oops...', 'Stok tidak habis dibagi '+input+' !', 'error');
	// 	}
	// })
});
</script>
@endpush
