<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../mronline/js/html5shiv.js"></script>
    <script src="../mronline/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					@include('customView.partial.navbar')
				</div>
			</div>
		</div><!--/header-middle-->
	
		
	</header><!--/header-->

	
	<section>
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="{{route('subdept.mr.mainpage')}}">Material Request</a></li>
			  <li><a href="{{route('subdept.mr.getBarangsByKategoris','all')}}">Pilih Barang</a></li>
			  <li>Checkout</li>
			</ol>
		<div class="col-md-12">
			<div class="row">
			</div>
			<form method="POST" action="{{ route('subdept.mr.postCart') }}" id="formPostCart">
			<a href="{{ route('subdept.mr.home')}}" type="button" class="btn btn-default">Back</a>
			
			<br clear="clearfix">	
			
			<div class="category-tab"><!--category-tab-->
				<div class="tab-content">
							<a id="showBarangs"></a>
							{{ csrf_field() }}
							<input name="projects" type="hidden" value="{{$projects['id']}}" />
							<input type="hidden" name="subdept_id" required value="{{ $subdept['id'] }}"/>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>User :</label>
								<input class="form-control" required value="{{CRUDBooster::myName()}}" readonly="" />
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Subdept :</label>
								<input class="form-control" required value="{{ $subdept['nama'] }}" readonly="" />
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Project:</label>
								<input class="form-control" required value="{{ $projects['text']}}" readonly="" />
							</div>
							<div class="form-group col-md-8 col-md-offset-2" style="display: none">
								<label>Gudang:</label>
								<input type="text" class="form-control" required name="gudangs" readonly="" placeholder="Masukkan Gudang" value="headquarter"/>
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Limit Total:</label>
								<input type="text" class="form-control" id="limitTotal" value="" readonly/>
								<input type="hidden" class="form-control" id="limitTotal2" name="limitTotal" value="" readonly/>
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Limit Bulan:</label>
								<input type="text" class="form-control" id="limitBulan" value="" readonly/>
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Saldo:</label>
								<input type="text" class="form-control" id="saldo" value="" readonly/>
							</div>
							<div class="form-group col-md-8 col-md-offset-2">
								<label>Total:</label>
								<input type="text" class="form-control" id="inputtotal" readonly="" value=""/>
								<input type="hidden" class="form-control" id="inputtotalnonrupiah" readonly="" value="" name='total'/>

							</div>
							<!-- <div class="form-group col-md-8 col-md-offset-2">
								<label>Periode Material Request:</label>
								<select class="form-control" required name="periode" id="select2periode"><option></option></select>
							</div> -->
							@foreach($barangs as $b)
							<div class="row">
								<div class="col-md-6 col-xs-8 col-md-offset-2">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<div class="col-md-4">
													<img src="{{ asset($b['item']->gambar) }}" alt="" style="width:100% ;" />	
												</div>
												<div class="col-md-8">
													<div class="col-md-12">
														<div class="row text-right">
															<span class="col-xs-4 text-right">Nama :</span>
															<span class="namaBarang col-xs-8">{{ $b['item']->nama }}</span>
														</div>
														<div class="row text-right">
															<span class="col-xs-4 text-right">Jumlah :</span>
															<span class="jumlahBarang col-xs-8">{{$b['jmlh']}} </span>
														</div>
														<div class="row text-right">
															<span class="col-xs-4 ">Harga :</span> 
															<span class="harga col-xs-8">{{$b['item']->avgprice}}</span>
														</div>
														<div class="row text-right">
															<span class="col-xs-4">Total :</span>
															<span class="hargaTotal col-xs-8" harga="{{$b['item']->avgprice}}" valku="{{$b['item']->avgprice*$b['jmlh']}}">{{$b['item']->avgprice*$b['jmlh']}}</span>
														</div>
														<div class="row text-right">
															<span class="col-xs-4">Keterangan:</span>
															<span class="keterangan col-xs-8">{{$b['keterangan']}}</span>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 ">
									<div class="col-md-12">
									<button type="button" class="btn btn-warning btnSunting" barang_id="{{ $b['item']->id}}" style="width: 100%;"><i class="fa fa-pencil" aria-hidden="true"></i></button>
									</div>
									<div class="col-md-12 col-xs-4" style="margin-top: 4%;">
									<!-- <button type="button" class="btn btn-info btnTukar" style="width: 100%;" barang_id="{{ $b['item']->id}}" data-toggle="modal" data-target="#modalTukar">
										<i class="fa fa-refresh" aria-hidden="true"></i>
									</button> -->
									</div>
									<div class="col-md-12 col-xs-4" style="margin-top: 4%;">
									<button type="button" class="btn btn-danger btnHapus" barang_id="{{ $b['item']->id}}" style="width: 100%;">
										<i class="fa fa-trash-o" aria-hidden="true"></i>
									</button>
									</div>
								</div>
									
								
							</div>
							<hr>
							@endforeach
							<div class="form-group col-md-8 col-md-offset-2">
								<button type="button" class="btn btn-success pull-right" id="btnSubmitMR" style="width: 100%;">Submit</button>
							</div>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</form>
				</div>
			</div><!--/category-tab-->
		</div>
		</div>
	</section>
	<div class="lds-css ng-scope" style="display: none" id="wait"><div style="width:100%;height:100%" class="lds-rolling"><div></div></div>
</div>

<div class="modal fade" id="modalTukar" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
    			<form method="POST" action="{{ route('subdept.mr.tukarBarang') }}" id="formTukarBarang">
	    			<div class="modal-body">    				
    					{{ csrf_field() }}
		    				<label>Cari Barang : </label>
		    				<div class="form-group">
		    				<select type='text' class="form-control" required name="tukar" id="select2gantiBarang">
		    					<option></option>
		    				</select>
	    				</div>
	    				<div class="form-group">
		    				<label>Jumlah : </label>
	    					<input type='number' required name="qnty" class="form-control"/>
	    				</div>
	    				<div id="cariBarang">
	    				</div>
	    			</div>
	    			<div class="modal-footer">
	    			  <button type="submit" class="btn btn-flat btn-success">Submit</button>
			          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </form>
    		</div>
    	</div>
	</div>

	<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
		
		<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
		
		
	</footer><!--/Footer-->
  
    <script src="{{asset('mronline/js/jquery.js')}}"></script>
	<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('mronline/js/price-range.js')}}"></script>
    <script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('mronline/js/main.js')}}"></script>
    <script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('customs/js/select2.min.js') }}"></script>

<script type="text/javascript">

$(function() {
	$("#select2project").select2();
	$("#select2periode").select2();
	var projects_id = {{ $projects['id'] }};
	var anggaran = {{$limitBulan}};
	var limitTotal = {{$limitTotal}};
	// console.log(anggaran);
	var totalProject=0;
	$('#limitBulan').val('Rp '+( anggaran ).toLocaleString('de-DE')+' ');
	$('#limitTotal').val('Rp '+( limitTotal ).toLocaleString('de-DE')+' ');
	$('#limitTotal2').val(limitTotal);
	hitungTotalProject();
	$('#saldo').val('Rp '+(limitTotal-totalProject).toLocaleString('de-DE')+' ');
	function hitungTotalProject(){
		totalProject=0;
		$('.hargaTotal').each(function(e){
			totalProject += parseInt($(this).attr('valku'));
		});
		$('#inputtotal').val('Rp '+(totalProject.toLocaleString('de-DE'))+' ,-');
		$('#inputtotalnonrupiah').val(totalProject);
	}

	$("#select2gantiBarang").select2({
		minimumInputLength: 3,
		placeholder: "Masukkan nama barang",
		ajax: {
			delay: 500,
            url: 'ajax_getBarang',
            // dataType: 'json',
            /*data: {
		        "_token": "{{ csrf_token() }}",
		        "qnty" : gg
	        },*/
	        data: function (params) {
			    var queryParameters = {
			      term: params.term
			    }

			    return queryParameters;
			  },
			processResults: function (data) {
			    return {
			      results: data
			    };
			},
		}
	})

	$(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

    $(document).on("click",".btnHapus",function(e){
    	id = $(this).attr('barang_id');
    	namabarang = $(this).closest('.row').find('.namaBarang').html();
    	url = '{{ route("subdept.mr.delCart") }}';
    	swal({
    		type: 'warning',
    		text: 'Apakah anda yakin akan menghapus '+namabarang+' dari Cart ?',
    		showCancelButton: true,
            confirmButtonColor: '#d2322d',
    	}).then(function(){
    		$.ajax({
	            type: "DELETE",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
			        "projects":projects_id,
		        },
		    }).done(function(response) {
		    	if(response.berhasil==1){
		    		swal('berhasil',namabarang+' berhasil dihapus dari Cart !','success')
		    		.then(function(){
		    			location.reload();
		    		});
		    	}else {
		    		swal('Oops...', 'Barang gagal dihapus !', 'error');
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        })
    	});
    });

	$(document).on("click",".btnSunting",function(e){
		id = $(this).attr('barang_id');
		namabarang = $(this).closest('.row').find('.namaBarang').html();
		nextP = $(this).closest('.row');
        var steps = [
        	{				
  				text: 'Masukkan jumlah '+namabarang+' :',
                input: 'text',
                inputPlaceholder: '0',
                showCancelButton: true,
                confirmButtonColor: '#FE980F',
                confirmButtonText: 'Next &rarr;',
                cancelButtonText: 'Cancel',
                inputValidator: function (value) {
	  			    return new Promise(function (resolve, reject) {
	  			      if (parseInt(value)>0) {
	  			        resolve()
	  			      } else {
	  			        reject('Masukkan Angka > 0 !')
	  			      }
	  			    })
  				}
  			}
			,
			{
	  		    title : 'Masukkan Keterangan :', 
	  			input: 'text',
	  			inputPlaceholder: 'opsional (contoh: "Budi; Ani; ")',
	  			showCancelButton: true,
	  			confirmButtonColor: '#FE980F',
	  			confirmButtonText: 'Submit',
	  			cancelButtonText: 'Cancel',
	  		}
	  	];
		swal.queue(steps)
		.then(function(gg){  
			url = "{{ route('subdept.mr.addCart') }}";
			$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
			        "qnty" : gg[0],
			        "keterangan" : gg[1],
			        "update" : 1,
			        "projects": projects_id
		        },
	        }).done(function(response){
                if(response.berhasil == 0) {
                	swal({
                		type: 'error',
                		html: 'Terjadi Kesalahan !',
                		confirmButtonColor: '#FE980F',	
                	})
                }
                else{
                	swal({
					    type: 'success',
					    html: 'Jumlah '+namabarang+' berhasil disunting',
					    confirmButtonColor: '#FE980F',
					});
					nextP.find('.jumlahBarang').html(response.jmlh);
					nextP.find('.keterangan').html(response.ktr);
					nextP.find('.hargaTotal').html(function(){
						totalBarang = parseInt($(this).attr('harga')*response.jmlh);
						$(this).html('Rp '+totalBarang.toLocaleString()+',-');
						$(this).attr('valku',totalBarang);
					});
					hitungTotalProject();
                }
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
		}).catch(swal.noop);
    });

	
	$('.harga, .hargaTotal').each(function(){
		duit = parseInt($(this).html()).toLocaleString();
		$(this).html('Rp '+duit+',-');
	});


	var idTukar;
    $(".btnTukar").click(function(){
    	idTukar = $(this).attr('barang_id');
    })

    $("#formTukarBarang").submit(function(){
    	$(this).append('<input type="hidden" name="barangs" value='+idTukar+' /> ');
    	return true;
    });

    $('#btnSubmitMR').click(function(){
    	// console.log('total: '+totalProject+' anggaran: '+anggaran);
    	if(totalProject>anggaran || ((anggaran-totalProject)<(anggaran*10/100)) ){
    		if( (anggaran-totalProject)<anggaran)
    		textku = 'Total melebihi Limit Bulan';
    		else if((anggaran-totalProject)<(anggaran*10/100))
    		textku = 'Limit Bulan akan habis';
    		else textku = 'error text anggaran; hubungi technubi';
    		swal({
    			type: 'warning',
	    		text: textku,
	    		showCancelButton: true,
	    		confirmButtonText: 'Lanjut!',
	            confirmButtonColor: '#d2322d',
    		}).then(function(){
    			$('#formPostCart').submit();
    		}).catch(swal.noop);
    	}
    	else{
    		// swal('Yeay','Total '+totalProject,'success');
    		$('#formPostCart').submit();
    	}
    });

});
</script>
</body>
</html>
