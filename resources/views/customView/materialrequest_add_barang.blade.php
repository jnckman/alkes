<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../mronline/js/html5shiv.js"></script>
    <script src="../mronline/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">

</head><!--/head-->

<body >
	<header id="header" style="position: fixed;width: 100%;z-index: 99;background: white;"><!--header-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					
					@include('customView.partial.navbar')
					
				</div>
				<div class="col-md-6 col-xs-12" style="padding: 0px;padding-top: 10px;background: #00b4ea;z-index: 999">
					<div class="col-sm-1 col-xs-2" style="padding-top: 10px;padding-bottom: 10px;">
						<i class="fa fa-search fa-2x visible-md visible-lg visible-sm" aria-hidden="true" style="color: white;"></i>
						<i class="fa fa-search fa-2x visible-xs" aria-hidden="true" style="color: white;"></i>
					</div>
					<div class="col-sm-11 col-xs-10" style="padding-top: 10px;padding-bottom: 10px;">
						<!-- <div class="search_box"> -->
							<input type="text" id="search_barang" class="form-control input" placeholder="Ketikan nama barang atau kode barang untuk mencari"  />
						<!-- </div> -->
					</div>
				</div>
				<div class="col-md-6 col-xs-12" style="padding: 0px;padding-top: 10px;background: #ff9f17;">
					<!-- <ol class="breadcrumb">
					  <li><a href="{{route('subdept.mr.mainpage')}}">Material Request</a></li>
					  <li class="active">Pilih Barang</li>
					</ol> -->
					<div class="col-sm-9 col-xs-8" style="padding-top: 10px;padding-bottom: 10px;padding-right: 0px;">
						<div class="">
							<select type="text" id="select2projects"><option></option></select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-4" style="padding-top: 10px;padding-bottom: 10px;">
						<button type="button" id="btnListBarang" title="list barang" class="btn btn-info" data-toggle="modal" data-target="#listBarang" style="width: 38%;"><i class="fa fa-bars"></i></button>
						<a href="" class="btn btn-success" id="btnCart" title="checkout" style="width: 38%;"><i class="fa fa-sign-out"></i></a>
					</div>
				</div>
				
				
			</div>
		</div><!--/header-middle-->
	
		
	</header><!--/header-->
	@if(@$alerts)
        		@foreach(@$alerts as $alert)
        			<div class='callout callout-{{$alert[type]}}'>        				
        					{!! $alert['message'] !!}
        			</div>
        		@endforeach
    @endif
	<section id="slider" style="padding-top: 200px;"><!--slider-->
		<div class="container">
			
			@include('customView.partial.warnings')
		</div>
	</section><!--/slider-->
	
	<section style="padding-bottom: 100px;" id="bodysection">
		<div class="container">
			
			
		
			<div class="col-sm-2" style="margin-top: 43px;">
				<div class="left-sidebar">
					<div class="col-md-12" style="padding: 0px;">
						<h2 style="cursor: pointer;font-size: 15px;" class="updown">Add by Group <i class="fa fa-arrows-v" aria-hidden="true"></i></h2>
						<div class="panel-group category-products groupaccordion" id="accordian" style="padding: 0px;padding-top: 10px;"><!--category-productsr-->
							<?php $i=0;?>
							@foreach($groups as $key =>$g)
							@if($i == 4 )
							@break
							@endif
							<div class="panel panel-default" style="padding: 0px;">
								<div class="panel-heading" >
									<button type="button" class="btn btn-warning tambahByGroupPopuler" group_id="{{$g->id}}" style="width: 100%;font-size: 12px;">{{$g->text}}</button>
								</div>
							</div>
							<?php $i++;?>
							@endforeach
							<div class="panel panel-default">
								<div class="panel-heading">
									<button type="button" class="btn btn-flat btn-info " id="tambahByGroup" style="width: 100%;">Other Group</button>
								</div>
							</div>
								
						</div><!--/category-products-->
					</div>
						
				</div>

					<div class="left-sidebar">
						<div class="col-md-12" style="padding: 0px;">
						<h2 class="updowncategory" style="cursor: pointer;font-size: 15px;">Category <i class="fa fa-arrows-v" aria-hidden="true"></i></h2>
						<div class="panel-group category-products categoryaccordion" id="accordian"><!--category-productsr-->
							<?php $ct = 1; ?>
							@foreach($kategoris as $key => $k)
							<div class="panel panel-default">
								<div class="panel-heading" id="viewdetails">
									@if($k->id == $id )
									<h4 class="panel-title"><a href="{{ route('subdept.mr.getBarangsByKategoris',$k->id) }}" class="ambilBarangs selected" kat_id="{{$k->id}}">{{ $k->nama }}</a></h4>
									@else
									<h4 class="panel-title"><a href="{{ route('subdept.mr.getBarangsByKategoris',$k->id) }}" class="ambilBarangs" kat_id="{{$k->id}}">{{ $k->nama }}</a></h4>
									@endif
									collapse
								</div>
							</div>
							@if($ct==4)
							<div class="collapse" id="moreKategori">
							@endif
							<?php $ct++; ?>
							@endforeach
							</div>
							@if(count($kategoris)>4)
							<p><a class="btn" data-toggle="collapse" data-target="#moreKategori">See More &raquo;</a></p>
							@endif
						</div><!--/category-products-->
						</div>
						
						
						
					
					</div>
					
				@if($privilege[1]>2 || CRUDBooster::isSuperadmin() == true )
				<div class="left-sidebar">
					<div class="col-md-12" style="padding: 0px;">
						<div class="panel-group category-products" id="accordian" ><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<button type="button" class="btn btn-warning" style=";" data-toggle="modal" data-target="#modalRequest" style="width: 100%;">Usulkan Barang</button>
								</div>
							</div>
						</div>
					</div>	
				</div>
				@endif
			</div>
				
			<div class="col-sm-10 padding-right">
				<div class="features_items"><!--features_items-->
					
					
				</div><!--features_items-->
				
				<div class="category-tab"><!--category-tab-->
					<div class="tab-content">
						<div class="tab-pane fade active in">
							<!-- TODO: CSS button ? -->
						<div class="row">
							<div class="col-sm-12">
								<!-- <div class="search_box"> -->
									<!-- <input type="text" id="search_barang" class="form-control" placeholder="Gunakan nama barang atau kode barang untuk mencari" style="width: 100% !important"/> -->
								<!-- </div> -->
							</div>
						</div>
							
							<a id="showBarangs"></a>
							<!-- css loading -->
							<div class="lds-css ng-scope" style="display: none" id="wait">
							  <div style="width:100%;height:100%" class="lds-rolling">
							    <div></div>
							  </div>
							</div>
							<!-- end css loading -->
							<div id="dataBarangs">
							<div class="features_items"><!--features_items-->
							<h2 class="title text-center">BARANG TERBARU</h2>
							<?php $konter = 1?>
								<?php $i=0;?>
								@foreach($barangBaru as $key=>$b)
								<div class="col-sm-3 col-xs-6">
									<div class="product-image-wrapper">
										<div class="single-products" >
											<div class="productinfo text-center">
												<img src="{{ asset($b->gambar) }}" alt="" style="height: 100px;width: 100px;" />
												<p>{{ $b->nama }}</p>
												<button type="button" style="width: 80%;" barang_id="{{ $b->id }}" barang_stok="{{ $b->akumstok}}" satuan="{{$b->satuan->nama}}" barang_harga="{{ $b->avgprice}}" class="btn btn-default addCart add-to-cart"><i class="fa fa-shopping-cart" ></i>Add to cart</button>

												<button type="button" style="width: 80%;" barang_id="{{ $b->id }}" class="btn btn-default detailBarang add-to-cart"><i class="fa fa-eye"></i>Detail</button>
											</div>
										</div>
									</div>
								</div>
								<?php $i++;?>
								@if($konter % 4 == 0)
								<div class="clearfix visible-md visible-lg"></div>
								@endif
								@if($konter % 2 == 0)
								<div class="clearfix visible-xs visible-sm"></div>
								@endif
								<?php $konter++?>
								@endforeach
							</div>
							<h2 class="title text-center">BARANG LAINNYA</h2>
								<?php $i=0;?>
								<?php $konter = 1?>
								@foreach($barangs as $key=>$b)
								<div class="col-sm-3 col-xs-6">
									<div class="product-image-wrapper">
										<div class="single-products" >
											<div class="productinfo text-center">
												<img src="{{ asset($b->gambar) }}" alt="" style="height: 100px;width: 100px;" />
												<p>{{ $b->nama }}</p>
												<button type="button" style="width: 80%;" barang_id="{{ $b->id }}" barang_stok="{{ $b->akumstok}}" satuan="{{$b->satuan->nama}}" barang_harga="{{ $b->avgprice}}" class="btn btn-default addCart add-to-cart"><i class="fa fa-shopping-cart" ></i>Add to cart</button>

												<button type="button" style="width: 80%;" barang_id="{{ $b->id }}" class="btn btn-default detailBarang add-to-cart"><i class="fa fa-eye"></i>Detail</button>
											</div>
										</div>
									</div>
								</div>
								<?php $i++;?>
								@if($konter % 4 == 0)
								<div class="clearfix visible-md visible-lg"></div>
								@endif
								@if($konter % 2 == 0)
								<div class="clearfix visible-xs visible-sm"></div>
								@endif
								<?php $konter++?>
								@endforeach
								{{ $barangs->links() }}
							</div>
						</div>
						
					</div>
				</div><!--/category-tab-->
					
					
					
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalRequest" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
	    			<div class="modal-body">
	    				<div class="form-group">
	    					<label>Gambar *:</label> (maskimal 2MB)
	    					<input type="file" required class="form-control" id="inputgambar" accept="image/*" onchange="uploadPhotos()">
	    				</div>
	    			</div>
    			<form method="POST" id="formRequest" enctype="multipart/form-data">
	    			<div class="modal-body">
    					{{ csrf_field() }}
    					<div class="form-group">
    						<label>Nama *: </label>
		    				<input type='text' class="form-control" required name="nama" id="formRequestNama"/>
	    				</div>
	    				<div class="form-group">
    						<label>Keterangan *: </label>
		    				<textarea type='text' class="form-control" required name="keterangan" id="formRequestKeterangan"></textarea>
	    				</div>
	    				<!-- <div class="form-group">
	    					<label>Gambar *:</label> (maskimal 2MB)			
		    				<input name="gambar" type="file" id="takePictureField" accept="image/*" onchange="uploadPhotos()" />
	    				</div> -->
	    				<div id="div-preview" class="col-sm-6 col-sm-offset-2" ">
							<img id="preview" style="max-width:200px; max-height: 200px">
						</div>
	    			</div>
	    			<div class="modal-footer">
	    				<button type="submit" class="btn btn-flat btn-success">Submit</button>
				        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
	    			</div>
    			</form>
    		</div>
    	</div>
	</div>

	<div class="modal fade" id="groupModal" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
    			<form id="formGroups">
	    			<div class="modal-body">    				
    					{{ csrf_field() }}
    					
		    				<label>Cari berdasarkan Group : </label>
		    				<div class="form-group">
		    				<select type='text' class="form-control" required name="groups" id="select2Groups">
		    					<option></option>
		    				</select>
	    				</div>
	    				<div class="table-responsive">
	    				<table class="table table-hover table-striped" id="showGroupBarang">
	    				</table>
	    				</div>
	    			</div>
	    			<div class="modal-footer">
	    			  <button type="submit" class="btn btn-flat btn-success" id="formGroupsSubmit">Submit</button>
			          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </form>
    		</div>
    	</div>
	</div>

	<div class="modal fade" id="groupModal2" role="dialog">
		<div class="modal-dialog">
    		<div class="modal-content">
    			<form id="formGroupPopuler">
	    			<div class="modal-body">    				
    					{{ csrf_field() }}
	    				<div class="table-responsive">
	    				<table class="table table-hover table-striped" id='showGroupPopulerBarang'>
	    				</table>
	    				</div>
	    			</div>
	    			<div class="modal-footer">
	    			  <button type="submit" class="btn btn-flat btn-success" id="formGroupsPopulerSubmit">Submit</button>
			          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </form>
    		</div>
    	</div>
	</div>

	<div class="modal fade" id="listBarang" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 id="namaProjects"></h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive" >
	    				<table class="table table-hover table-striped" id="tableListBarang" style="width: 100%">
	    					<thead>
	    						<th>No</th>
	    						<th>Nama Barang</th>
	    						<th>Jumlah</th>
	    						<th>Keterangan</th>
	    						<th>Action</th>
	    					</thead>
	    					
	    				</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
      </div>
    </div>
</div>

	<div class="modal fade" id="modalDetailBarang" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> </div> <div class="modal-body"> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>
	
	<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
		
		<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
		
		
	</footer><!--/Footer-->
  
    <script src="{{asset('mronline/js/jquery.js')}}"></script>
	<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('mronline/js/price-range.js')}}"></script>
    <script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('mronline/js/main.js')}}"></script>
    <script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('customs/js/select2.min.js') }}"></script>
    <script src="{{ asset('js/datatable.min.js') }}"></script>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$('.groupaccordion').slideUp();
	$('.categoryaccordion').slideUp();
});
var urlParams;
(window.onpopstate = function () {
var match,
    pl     = /\+/g,  // Regex for replacing addition symbol with a space
    search = /([^&=]+)=?([^&]*)/g,
    decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
    query  = window.location.search.substring(1);

urlParams = {};
while (match = search.exec(query))
   urlParams[decode(match[1])] = decode(match[2]);
})();

$(function() {
	// console.log(urlParams);
	var groups = <?php echo($groups);?>;
	var projects = <?php echo($projects);?>;
	var projects_id = 0;
	var urlCart = "{{url('mrmainpage/cart/')}}";

	if(urlParams['projects']){
		projects_id = urlParams['projects'];
		$('#btnCart').attr("href",urlCart+'/'+projects_id);
	}

	$(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

    $('#select2projects').select2({
    	data: projects,
    	placeholder : 'Pilih Project'
    }).val(projects_id).trigger('change');

    $("#tambahByGroup").click(function(e){
    	if(projects_id==0) {
    		swal('Oops','Pilih Project terlebih dahulu','info').catch(swal.noop);
	    	return ;
    	}
    	$('#groupModal').modal('show');
    });

    $('.tambahByGroupPopuler').click(function(e){
    	if(projects_id==0) {
    		swal('Oops','Pilih Project terlebih dahulu','info').catch(swal.noop);
	    	return ;
    	}
    	id = $(this).attr('group_id');
    	url = '{{ url("mrmainpage/getGroupBarangs")}}'+"/"+id,
    	idproject = $('#select2projects').val();
    	$.get(url, function (gg) {
	    	$('#showGroupPopulerBarang').html(
    				'<thead>'+
						'<th>No</th>'+
						'<th>Nama</th>'+
						'<th>Jumlah</th>'+
					'</thead>'+
					'<tbody>'+
					'</tbody>'
    				);
    		for (var i = 0; i < gg.data.length; i++) {
    			$("#showGroupPopulerBarang").append(
    			'<tr>'+
    			'<td><b>'+(i+1)+'</b></td>'+
    			'<td>'+gg.data[i].nama+'</td>'+
    			'<td><input type="number" required name="qntys['+gg.data[i].id+']" /></td>'+
    			'</tr>'
    			);
    		}
    		$("#showGroupPopulerBarang").append(
    			'<input type="hidden" name="groups" value="'+id+'"/>'+
    			'<input type="hidden" name="projects" value="'+idproject+'" />'+
    			'</div>'
    		);
    	});
    	$('#groupModal2').modal('show');
    });

    $('#select2projects').on('change',function(){
    	projects_id = $(this).val();
    	$('#btnCart').attr("href",urlCart+'/'+projects_id);
    });

    $('.pagination li a').click(function(e){
    	if(projects_id != 0) {
    		urlLama = $(this).attr('href');
    		$(this).attr('href',urlLama+'&projects='+projects_id);
    	}
    });

	$(document).on("click",".addCart",function(e){
		id = $(this).attr('barang_id');
		stok = $(this).attr('barang_stok');
		harga = $(this).attr('barang_harga');
		satuan = $(this).attr('satuan');
		namabarang = $(this).prev().html();
        swalAddCart(id,namabarang,satuan,stok,harga);
    });

	var simpanHtml='0';
	var timeout_cari;
	var xhr_cari;

    $(document).on("input","#search_barang", function(e){
    	val = $(this).val();
    	url = "{{ route('subdept.mr.cariBarang') }}";
    	//kasih delay 1 detik
    	if (timeout_cari) {
	        clearTimeout(timeout_cari);
	    }
	    if ( xhr_cari) {
	        xhr_cari.abort();
	    }
	    timeout_cari = setTimeout( function () {
	    	if(val.length > 3) {
				if(simpanHtml=='0'){
					simpanHtml = $('#dataBarangs').html().replace(/\n|\t/g, ' ');
				}
				$('#dataBarangs').remove();
				xhr_cari = $.ajax({
		            type: "POST",
		            url: url,
		            data: {
				        "_token": "{{ csrf_token() }}",
				        "cari" : val,
			        },
		        }).done(function(response) {
		        	if(response.count==0){
		        		$("#showBarangs").append(
		        			'<div id="dataBarangs"><p>Barang tidak ditemumkan</p></div>'
			        	);
		        	}else {
		        		$("#showBarangs").append(
				        	'<div id="dataBarangs"><h2 class="title text-center">HASIL PENCARIAN</h2> </div>'
				        	);
		        		for (var i = 0; i < response.hasil.length; i++) {
		        			$("#dataBarangs").append(
				        		'<div class="col-sm-3 col-xs-6">'+
										'<div class="product-image-wrapper">'+
											'<div class="single-products">'+
												'<div class="productinfo text-center">'+
													'<img src="'+response.hasil[i]['gambar']+'" alt="" style="height: 100px;width: 100px;""/>'+
													'<p>'+ response.hasil[i]['nama'] +'</p>'+
													'<button type="button" barang_id="'+ response.hasil[i]['id'] +'" barang_stok="'+response.hasil[i]['akumstok']+'" barang_harga="'+response.hasil[i]['avgprice']+'" satuan="'+response.hasil[i]['satuan']+'" class="btn btn-default addCart add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>'+
													'<button type="button" style="width: 80%;" barang_id="'+response.hasil[i]['id']+'" class="btn btn-default detailBarang add-to-cart"><i class="fa fa-eye"></i>Detail</button>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
				        	'</div>'
		        			);
		        			if( (i+1) % 4 == 0)
								$("#dataBarangs").append('<div class="clearfix visible-md visible-lg"></div>');
							if( (i+1) % 2 == 0)
								$("#dataBarangs").append('<div class="clearfix visible-xs visible-sm"></div>');
		        		}
		        	}
		        }).fail(function(){
		        });
	    	}
	    	if(val==''){
	    		$("#dataBarangs").remove();
	    		$("#showBarangs").append('<div id="dataBarangs"></div>');
	    		$("#dataBarangs").append(simpanHtml);
	    	}
	    }, 1000);
	    /////////
    });

    $("#select2Groups").select2({
    	data:groups,
    	placeholder: "Pilih Group",
    });

    $('#tableListBarang').DataTable({
    	"lengthChange": false,
    	"searching": false,
    });

    $("#btnListBarang").click(function(){
    	$('#namaProjects').html($('#select2projects').select2('data')[0].text);
		$.ajax({
            url: '{{route("subdept.mr.ajax_getListBarang")}}',
            data: {
            	"id": projects_id,
            }
        }).done(function(response){
        	// console.log(response);
        	$('#tableListBarang').DataTable().destroy();
        	$('#tableListBarang').DataTable({
        		"pageLength": 50,
		    	"lengthChange": false,
		    	"searching": false,
        		"data" : response,
        	});
        }).fail(function(){
        	console.log('gagal');
        });
    });

    $(document).on('click','.btnEditJumlah',function(e){
    	id = $(this).attr('brg_id');
    	stok = $(this).attr('barang_stok');
    	satuan = $(this).attr('satuan');
    	harga = $(this).attr('barang_harga');
    	suntingUpdateJmlh = $(this).parent().prev().prev();
    	suntingUpdateKtr = $(this).parent().prev();
    	swalAddCart(id,' ',satuan,stok,harga,1);
    });

    $(document).on("select2:selecting","#select2Groups", function(e){
    	id = e.params.args.data.id,
    	url = '{{ url("mrmainpage/getGroupBarangs")}}'+"/"+id,
    	$.get(url, function (gg) {
    		// console.log(gg);
    		$("#showGroupBarang").html(
    				'<thead>'+
						'<th>No</th>'+
						'<th>Nama</th>'+
						'<th>Jumlah</th>'+
					'</thead>'+
					'<tbody>'+
					'</tbody>'
    				);
    		for (var i = 0; i < gg.data.length; i++) {
    			$("#showGroupBarang").append(
    			'<tr>'+
    			'<td><b>'+(i+1)+'</b></td>'+
    			'<td>'+gg.data[i].nama+'</td>'+
    			'<td><input type="number" required name="qntys['+gg.data[i].id+']" /></td>'+
    			'</tr>'
    			);
    		}
    		$("#showGroupBarang").append(
    			'<input type="hidden" name="projects" value="'+$('#select2projects').val()+'" />'+
    			'</div>'
    		);
    	});
    });

    $(document).on('click','.btnHapusBarangCart',function(e){
    	url = '{{ route("subdept.mr.delCart") }}';
    	id = $(this).prev().attr('brg_id');
    	trhapus = $(this).parent().parent();
    	swal({
    		type :'warning',
          	html: 'Apakah anda yakin akan menghapus barang dari Cart ?',
		    showCancelButton: true,
		    confirmButtonColor: '#d2322d',
		    cancelButtonText: 'Cancel',
    	}).then(function(){
    		$.ajax({
	            type: "DELETE",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "projects" : projects_id,
			        "id" : id,
		        },
		    }).done(function(response) {
		    	// console.log(response);
		    	if(response.berhasil==1){
		    		swal('berhasil','Project berhasil dihapus dari Cart !','success')
		    		// .then(function(){location.reload();})
		    		;
		    		trhapus.remove();
		    	}else {
		    		swal('Oops...', 'Project gagal dihapus !', 'error');
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
    	}).catch(swal.noop);
    });

    function swalAddCart(id,namabarang='',satuan='',stok,harga,update=0){
    	harga = parseInt(harga);
    	if(projects_id ==0 ){
    		swal('Oops...', 'Pilih Project terlebih dahulu!', 'info');
    		return
    	}
    	var steps = [
		  {
		    title : 'Masukkan jumlah <i>('+satuan+')</i> '+namabarang+' :', 
			html: 'stok: <b>'+stok+'</b> | harga: <b>Rp'+harga.toLocaleString('de-DE')+' ,-</b>',
			input: 'text',
			inputPlaceholder: '0',
			showCancelButton: true,
			confirmButtonColor: '#FE980F',
			confirmButtonText: 'Next &rarr;',
			cancelButtonText: 'Cancel',
			inputValidator: function (value) {
				return new Promise(function (resolve, reject) {
				  if (parseInt(value)>0) {
				    resolve()
				  } else {
				    reject('Input harus angka dan > 0 !')
				  }
				})
			}
		  },
		  {
  		    title : 'Masukkan Keterangan :', 
  			input: 'text',
  			inputPlaceholder: 'opsional (contoh: "Budi; Ani; ")',
  			showCancelButton: true,
  			confirmButtonColor: '#FE980F',
  			confirmButtonText: 'Submit',
  			cancelButtonText: 'Cancel',
	  		}
		];
    	swal.queue(steps)
		.then(function(gg){  
			// console.log(gg);
			url = "{{ route('subdept.mr.addCart') }}";
			$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
			        "qnty" : gg[0],
			        "keterangan" : gg[1],
			        "projects" : projects_id,
			        "update" : update
		        },
	        }).done(function(response){
	        	console.log(response);
	        	if(response.berhasil==1){
	        		swal({
					    type: 'success',
					    html: namabarang+' berhasil ditambahkan',
					    confirmButtonColor: '#FE980F',
					});
	        	}
	        	else if(response.berhasil==2) {
	        		swal({
					    type: 'success',
					    html: 'Jumlah berhasil diedit',
					    confirmButtonColor: '#FE980F',
					});
					suntingUpdateJmlh.html(response.jmlh);
					suntingUpdateKtr.html(response.ktr);
	        	}
	        	else
	        	swal('Oops...', 'Terjadi Kesalahan !', 'info');	
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
		}).catch(swal.noop);
    };

    $('#formGroups').on('submit',function(e){
    	e.preventDefault();
    	url = "{{ route('subdept.mr.addCart') }}";
    	$.ajax({
    		type: 'POST',
    		url:url,
    		data: $("#formGroups").serialize(),
    	}).done(function(response){
    		$('#groupModal').modal('hide');
    		$('#showGroupBarang').html('');
    		$('#select2Groups').val('').trigger('change');
    		swal({
			    type: 'success',
			    html: 'Berhasil menambahkan data ke Cart',
			    confirmButtonColor: '#FE980F',
					}).catch(swal.noop);
    	}).fail(function(response){
    		swal('Oops...', 'Terjadi Kesalahan !', 'error').catch(swal.noop);
    	});
    });

    $('#formGroupPopuler').on('submit',function(e){
    	e.preventDefault();
    	url = "{{ route('subdept.mr.addCart') }}";
    	$.ajax({
    		type: 'POST',
    		url:url,
    		data: $("#formGroupPopuler").serialize(),
    	}).done(function(response){
    		$('#groupModal2').modal('hide');
    		$('#showGroupPopulerBarang').html('');
    		swal({
			    type: 'success',
			    html: 'Berhasil menambahkan data ke Cart',
			    confirmButtonColor: '#FE980F',
					}).catch(swal.noop);
    	}).fail(function(response){
    		console.log(response);
    		swal('Oops...', 'Terjadi Kesalahan !', 'error').catch(swal.noop);
    	});
    });

    $(document).on('click','.detailBarang',function(event){
        url = "{{route('shop.getDetailBarang')}}";
        id = $(this).attr('barang_id');
        $.ajax({
            type:"GET",
            url: url,
            data: {
                "id": id
            }
        }).done(function(response){
            // console.log(response);
            $('#modalDetailBarang').find('.modal-header').html('<h4>'+response.kode+'</h4>');
            modal_body =$ ('#modalDetailBarang').find('.modal-body');
            modal_body.html(
            '<div class="form-group">'+
                '<label>Nama : </label>'+
                '<p>'+response.nama+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Kategori : </label>'+
                '<p>'+response.kategori+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Satuan : </label>'+
                '<p>'+response.satuan+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Jenis : </label>'+
                '<p>'+response.jenis+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Ukuran : </label>'+
                '<p>'+response.ukuran+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Cara pakai : </label>'+
                '<p>'+response.cara_pakai+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Spesifikasi : </label>'+
                '<p>'+response.spesifikasi+'</p>'+
            '</div>'+
            '<div class="form-group">'+
                '<label>Link Video : </label>'+
                '<p><a href="'+response.video+'">'+response.video+'</a></p>'+
            '</div>');
            $('#modalDetailBarang').modal('show');
        }).fail(function(){
            swal('Oops','Gagal Mengambil Data','error').catch(swal.noop);
        });
    });

    $('.ambilBarangs').click(function(event){
    	event.preventDefault();
    	if(projects_id!=0){
    		window.location.href = $(this).attr('href')+'?projects='+projects_id;
    	}
    	else{
    		window.location.href = $(this).attr('href');
    	}
    });
    $('.updown').click(function(event) {
    	$('.groupaccordion').slideToggle();
    });
    $('.updowncategory').click(function(event) {
    	$('.categoryaccordion').slideToggle();
    });

	$('img').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
	});

	// $('#inputgambar').on('change',function(event){
 //    	var reader = new FileReader();
 //    	reader.onload = function(e){
 //    		$('#preview').attr('src',e.target.result);
 //    	};
 //    	reader.readAsDataURL(this.files[0]);
 //    	// $('#div-preview').show();
 //    	$("#modalRequest .modal-footer").attr('style','margin-top:200px;');
 //    	console.log($('#preview').height());
 //    });

    var tampungGambar;

	uploadPhotos = function(){
	    // Read in file
	    var file = event.target.files[0];

	    // Ensure it's an image
	    if(file.type.match(/image.*/)) {
	        console.log('An image has been loaded');

	        // Load the image
	        var reader = new FileReader();
	        reader.onload = function (readerEvent) {
	            var image = new Image();
	            image.onload = function (imageEvent) {

	                // Resize the image
	                var canvas = document.createElement('canvas'),
	                    max_width = 800,
	                    max_height = 600,
	                    width = image.width,
	                    height = image.height;
	                if (width > height) {
	                    if (width > max_width) {
	                        height *= max_width / width;
	                        width = max_width;
	                    }
	                } else {
	                    if (height > max_height) {
	                        width *= max_height / height;
	                        height = max_height;
	                    }
	                }
	                canvas.width = width;
	                canvas.height = height;
	                canvas.getContext('2d').drawImage(image, 0, 0, width, height);
	                var dataUrl = canvas.toDataURL('image/jpeg');
	                var resizedImage = dataURLToBlob(dataUrl);
	                $.event.trigger({
	                    type: "imageResized",
	                    blob: resizedImage,
	                });
	            }
	            image.src = readerEvent.target.result;
           		$('#preview').attr('src',image.src);
	        }
	        reader.readAsDataURL(file);
           	$("#modalRequest .modal-footer").attr('style','margin-top:200px;');
	    }
	};
	/* Utility function to convert a canvas to a BLOB */
var dataURLToBlob = function(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = parts[1];

        return new Blob([raw], {type: contentType});
    }

    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
}
/* End Utility function to convert a canvas to a BLOB      */

/* Handle image resized events */
$(document).on("imageResized", function (event) {
    // var data = new FormData($("form[id*='formRequest']")[0]);
    if (event.blob) {
        tampungGambar = event.blob;

        // $.ajax({
        //     url: event.url,
        //     data: data,
        //     cache: false,
        //     contentType: false,
        //     processData: false,
        //     type: 'POST',
        //     success: function(data){
        //        //handle errors...
        //     }
        // });
    }
});

$('#formRequest').submit(function(event){
	event.preventDefault();
	var fd = new FormData();
	fd.append('gambar', tampungGambar);
	var other_data = $('#formRequest').serializeArray();
    $.each(other_data,function(key,input){
        fd.append(input.name,input.value);
    });
	// console.log(fd.get('gambar'));
	// return;
	$.ajax({
		data:fd,
		url: "{{ route('subdept.mr.requestBarang') }}",
		contentType: false,
	    processData: false,
		type: 'POST',
	}).done(function(response){
		if(response.berhasil==1){
			swal('Success','Berhasil Melakukan Request','success');
			$('#formRequestKeterangan').val('');
			$('#formRequestNama').val('');
			$('#inputgambar').val('');
	   		$('#preview').attr('src','#');
	   		tampungGambar = '';
		}
		else if(response.berhasil==2){
			swal('Oops','Pilih Gambar Terlebih Dahulu','warning');	
		}
		else{
			swal('Oops','Terjadi Kesalahan','error');
		}
	}).fail(function(){
		swal('Oops','Terjadi Kesalahan','error');
	});
});

});
</script>
<style type="text/css">
	img {
	    cursor: zoom-in;
	}
	.selected{
		border-bottom: 2px solid grey;

	}
	.select2{
		width: 100% !important; 
	}
	.category-tab ul{
		background: transparent;
	}
	@media only screen and (min-width: 300px){
		#bodysection{
			padding-top: 40px;
		}
	}
</style>
</body>
</html>