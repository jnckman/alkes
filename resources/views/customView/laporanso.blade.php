@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-close" aria-hidden="true"></i> LAPORAN SO
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/lihatpembukuanrekap/{{ $id }}">Purchase Request</a></li>
   <li class="active">Susun PO</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        
        
        <div class="col-md-6">
          
        </div>
        
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body ">
       <div class="col-md-12">
         <form action="submitlaporan" method="POST">
            <label>Dari</label>
           <input type="date" name="tanggal" class="form-control">
            <label>Ke</label>
           <input type="date" name="tanggalend" class="form-control">
            <label>Jenis Laporan</label>
            {{csrf_field()}}
           <select name="jenislaporan" class="form-control">
             <option value="customer">Customer All</option>
             <option value="customer_specific">Customer Specific</option>
             <option value="barang">Barang All</option>
             <option>a</option>
           </select>
           <button type="submit">Cari</button>
         </form>
       </div>
       </div>
       </div>
     </div>
   </div>

    
   
@endsection

@section('datatable')

@endsection

