@extends('customView.partial.shop_template')
@push('head')
<style type="text/css">
	.vertical-align {

	    vertical-align: middle;
	}
</style>
@endpush
@section('content')
<div class="container">
	<form method="post" action="{{route('shop.checkout')}}">{{csrf_field()}}
<div class="row" id="cart_items">
	<h2 class="title text-center">SHOPPING CART</h2>
	@if(session()->has('customerShop'))
	<div class="row">
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-2">
				<label>Customer: </label> 
			</div>
			<div class="col-sm-10">
				<p>{{session()->get('customerShop')->nama}} </p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label>Rekening :</label>
			</div>
			<div class="col-sm-10">
				@if(isset(session()->get('customerShop')->detailrek))
				<p>{{session()->get('customerShop')->detailrek['norek']}} </p>
				@else
				<p>Belum ada data rekening </p>
				@endif
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="col-sm-2">
			<label>Alamat: </label>
		</div>
		<div class="col-sm-10">
			<select id="select2alamat" name="alamat" class="form-control"><option></option></select>
		</div>
	</div>
	</div>
	<br/>
	@endif
	<div class="table-responsive cart_info">
	<table class="table table-condensed">
		<thead>
			<th></th>
			<th>Nama Barang</th>
			<th>Harga Satuan</th>
			<th></th>
			<th>Jumlah</th>
			@if((strtolower(json_decode($data['privilege'])[0])=='so' && strtolower(json_decode($data['privilege'])[1])=='1') || CRUDBooster::isSuperAdmin()==true)
			<th>Diskon</th>
			@endif
			<th>Total</th>
			<th></th>
		</thead>
	<tbody>
	@foreach($barangs as $key=>$b)
	<tr barang_id="{{$b['item']->id}}" harga = "{{$b['item']->harga_jual}}">
		<td class="cart-product">
			<img src="{{asset($b['item']->gambar)}}" alt="GambarBarang" style="max-height: 150px;">	
		</td>
		<td class="cart_description">
			<p>{{$b['item']->nama}}</p>
		</td>
		<td class="cart_price">
			<p>Rp <span class="hargaSatu">{{number_format($b['total']/$b['jmlh'])}}</span></p>
		</td>
		<td><h4>&times;</h4></td>
		<td class="cart_quantity">
			<div class="cart_quantity_button">
				<button type="button" class="btn btn-warning btnSunting">{{$b['jmlh']}}</button>
			</div>
		</td>
		@if((strtolower(json_decode($data['privilege'])[0])=='so' && strtolower(json_decode($data['privilege'])[1])=='1') || CRUDBooster::isSuperAdmin()==true)
		<td>
			<input type="text" class="form-control inputdiskon" hargaTotal="{{$b['total']}}" placeholder="0">
			<input type="hidden" class="diskons" name="diskon[{{$b['item']->id}}]" value="0">
		</td>
		@else
		<td></td>
		@endif
		<td class="cart_total">
			<p class="cart_total_price" valku="{{$b['total']}}">Rp <span class="hargaTotal">{{number_format($b['total'])}}</span></p>
		</td>
		<td class="cart_delete">
			<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
		</td>
	</tr>
	@endforeach
	<tfoot>
	<tr>
		<td colspan="1"><label>Metode Bayar*:</label></td>
		<td>
			<select class="form-control" required name="bayar">
				<option></option>
				<option val='tunai'>Tunai</option>
				<option val='tempo'>Tempo</option>
			</select>
		</td>
		<td colspan="2"></td>
		@if((strtolower(json_decode($data['privilege'])[0])=='so' && strtolower(json_decode($data['privilege'])[1])=='1') || CRUDBooster::isSuperAdmin()==true)
		<td><b>Potongan:</b></td>
		<td>
			<input type="text" class="form-control inputdiskontotal" hargaTotal="{{$b['total']}}" placeholder="0">
			<input type="hidden" class="diskonstotal" name="diskon_total" value="0">
		</td>
		@else
		<td colspan="2"></td>
		@endif
		<td>
			<p class="cart_total_price" id="tampilTotal"></p>
		</td>
	</tr>
	<tr>
		<td colspan="6"></td>
		<td><button type="submit" class="btn btn-warning">Checkout</button></td>
	</tr>
	</tfoot>
	</tbody>
	<input type="hidden" id="diskonTotal" name="diskonTotal" >
	</table>
	</div>
</div>
</form>
</div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function() {
	var totalProject=0;
	hitungTotal();
	var totalp2=totalProject;
	var maprivilege = <?php echo $data['privilege'];?>;
	var alamats = <?php echo $alamats;?>;
	// console.log(maprivilege[0]);
	$('#select2alamat').select2({
		placeholder: 'Pilih Alamat',
		data : alamats
	})
	$('select[name="bayar"]').select2({
		placeholder: 'Pilih Metode Bayar'
	})
	$(document).on("click",".cart_quantity_delete",function(e){
    	id = $(this).closest('tr').attr('barang_id');
    	namabarang = $(this).closest('tr').find('.cart_description p').html();
    	url = '{{ route("shop.delCart") }}';
    	swal({
    		type: 'warning',
    		text: 'Apakah anda yakin akan menghapus barang ini dari Cart ?',
    		showCancelButton: true,
            confirmButtonColor: '#d2322d',
    	}).then(function(){
    		$.ajax({
	            type: "DELETE",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
		        },
		    }).done(function(response) {
		    	if(response.berhasil==1){
		    		swal('berhasil','Barang berhasil dihapus dari Cart !','success')
		    		.then(function(){
		    			location.reload();
		    		});
		    	}else {
		    		swal('Oops...', 'Barang gagal dihapus !', 'error');
		    	}
		    }).fail(function() {
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        })
    	});
    });

    $(document).on("click",".btnSunting",function(e){
		id = $(this).closest('tr').attr('barang_id');
		nextP = $(this).closest('tr');
		harga_jual = nextP.find('.cart_total_price').attr('valku') / $(this).html();
		url = "{{ route('shop.addCart') }}";
		if((maprivilege[0].toLowerCase()=='so' && maprivilege[1]=='1')|| maprivilege[0].toLowerCase()=='super administrator'){
            // console.log('ok');
            var steps = [
            {
              type: 'info',
              html: 'Masukkan jumlah baru :', 
              input: 'text',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Next &rarr;',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                  if (parseInt(value)>0) {
                    resolve()
                  } else {
                    reject('Input harus angka dan > 0 !')
                  }
                })
              }   
            },
            {
              title: 'Masukkan harga baru <b>(opsional)</b> :',
              input: 'number',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Submit',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if(value){
                        if (value<0) {
                            reject('Input harus > 0 !')
                        }
                    }
                    resolve()
                })
              }   
            }
            ]
            swalku = swal.queue(steps);
        }
        else{
            swalku = swal({
              type: 'info',
              html: 'Masukkan jumlah baru :', 
              input: 'text',
              showCancelButton: true,
              inputPlaceholder : '0',
              confirmButtonColor: '#FE980F',
              confirmButtonText: 'Submit',
              cancelButtonText: 'Cancel',
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                  if (parseInt(value)>0) {
                    resolve()
                  } else {
                    reject('Input harus angka dan > 0 !')
                  }
                })
              }
            });
        }
		swalku.then(function(gg){  
			// console.log(gg);
			// return;
			if(Array.isArray(gg)){
				if(gg[1]){
					var hargaBaru = gg[1];
	                var qnty = gg[0];
				}
				else{
					var hargaBaru = -1;
	                var qnty = gg[0];	
				}
            }else {
                var hargaBaru = -1;
                var qnty = gg;
            }
			$.ajax({
	            type: "POST",
	            url: url,
	            data: {
			        "_token": "{{ csrf_token() }}",
			        "id": id,
			        "qnty" : qnty,
			        "update" : 1,
			        "hargaBaru" : hargaBaru,
			        "harga_jual" : harga_jual,
		        },
	        }).done(function(response){
	        	// console.log(response);
	        	// return;
                if(response.berhasil == 0) {
                	swal({
                		type: 'error',
                		html: 'Terjadi Kesalahan !',
                		confirmButtonColor: '#FE980F',	
                	})
                }
                else{
                	swal({
					    type: 'success',
					    html: 'Jumlah berhasil disunting',
					    confirmButtonColor: '#FE980F',
					});
					nextP.find('.btnSunting').html(response.jmlh);
					// console.log(nextP.find('cart_total_price'));
					nextP.find('.cart_total_price').attr('valku',response.total);
					// nextP.find('.cart_total_price').html(function(){
					// });
					nextP.find('.hargaTotal').html(response.total.toLocaleString());
					nextP.find('.inputdiskon').attr('hargaTotal',response.total);
					nextP.find('.hargaSatu').html((response.total/response.jmlh).toLocaleString());
					hitungTotal();
                }
            }).fail(function(){
	         	swal('Oops...', 'Terjadi Kesalahan !', 'error');
	        });
		}).catch(swal.noop);
    });
    $(document).on("input",".inputdiskon", function(e){
        var tampung = $(this).val();
        var diskonsku= 0;
        if(tampung.indexOf('%') >= 0){
          $tampungconvert = tampung.substr(0, tampung.indexOf('%')); 
          diskonsku = $tampungconvert * $(this).attr('hargaTotal')/100
          harga = $(this).attr('hargaTotal') -  diskonsku;
        }
        else{
          diskonsku = $(this).val();
          harga = $(this).attr('hargaTotal')- diskonsku;
        }
        if(diskonsku == '') diskonsku = 0;
        $(this).parent().parent().find('.hargaTotal').html(harga.toLocaleString('en-EN'));
        $(this).next().val(diskonsku);
    	totalDiskon=0;
		$('.diskons').each(function(e){
			totalDiskon += parseInt($(this).val());
		});
		totalDiskon += parseInt($('.inputdiskontotal').next().val());
		totalp2 = totalProject-totalDiskon;
		// console.log(totalp2);
		$('#diskonTotal').val(totalDiskon);
		$('#tampilTotal').html('Rp '+(totalp2).toLocaleString('en'));
      });

    $(document).on("input",".inputdiskontotal", function(e){
    	var tampung = $(this).val();
    	var diskonsku= 0;
    	// console.log(totalProject+ ' | '+totalp2);
    	if(tampung.indexOf('%') >= 0){
              $tampungconvert = tampung.substr(0, tampung.indexOf('%')); 
              diskonsku = $tampungconvert * totalProject/100;
              // console.log(totalp2+' - '+diskonsku);
              harga = totalProject -  diskonsku;
            }
            else{
              diskonsku = $(this).val();
              harga = totalProject - diskonsku;
        }
        if(diskonsku == '') diskonsku = 0;
        $(this).parent().parent().find('.hargaTotal').html(harga.toLocaleString('en-EN'));
        $(this).next().val(diskonsku);
    	totalDiskon=0;
		$('.diskons').each(function(e){
			totalDiskon += parseInt($(this).val());
		});
		totalDiskon += parseInt($(this).next().val());
		// console.log(totalDiskon);
		$('#diskonTotal').val(totalDiskon);
		$('#tampilTotal').html('Rp '+(totalProject-totalDiskon).toLocaleString('en'))
    });

    function hitungTotal(){
    	totalProject = 0;
		$('.cart_total .cart_total_price').each(function(e){
			totalProject += parseInt($(this).attr('valku'));
			// console.log($(this).attr('valku'));
		});
		totalp2 = totalProject;
		$('#tampilTotal').html('Rp '+totalProject.toLocaleString('en'))
	}
});
</script>
@endpush