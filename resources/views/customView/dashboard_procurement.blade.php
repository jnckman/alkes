@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Admin
@endsection

@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Admin</li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
   <div class="box box-default">
      <!-- <form id="formubahperiode"> -->
      <div class="box-header">
         <h3 class="box-title">Periode  :</h3>
      </div>
      <div class="box-body">
         <select id="select2periode" class="form-control"></select>
      </div>
      <div class="box-footer">
         <button type="button" id="btnubahperiode" class="btn btn-success">Ubah</button>
      </div>
      <!-- </form> -->
   </div>
</div>
</div>

<div class="row" >
   <div class="col-md-12">
      <div class="box box-danger"">
         <div class="box-header with-border">
            <h3 class="box-title">PO Berjalan</h3>
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm" id="refreshpo" >
                  <i class="fa fa-refresh"></i></button>
                  <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button>
                </div>
            </div>
         </div>
         <div class="box-body no-padding">
            <div class="table-responsive">
               <table id='tablepo' class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>No PO</th>
                        <th>Supplier</th>
                        <th>Progress</th>
                        <th>Label</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="row" >
   <div class="col-md-12">
      <div class="box box-info"">
         <div class="box-header with-border">
            <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm" id="refreshminmax">
                  <i class="fa fa-refresh"></i></button>
                  <button type="button" class="btn btn-default btn-sm" >
                  <i class="fa fa-question"></i></button>
                </div>
            </div>
            <p>Stok Min / Max</p>
         </div>
         <div class="box-body no-padding">
            <ul class="nav nav-tabs nav-justified">
                  <li class="active"><a data-toggle="tab" href="#tab_min">Stok Dibawah Batas Minimal</a></li>
                  <li ><a data-toggle="tab" href="#tab_max">Stok Diatas Batas Maksimal</a></li>
            </ul>
            <div class="tab-content">
               <div id="tab_min" class="tab-pane active">
                  <div class="table-responsive">
                     <table id='table1' class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Kode</th>
                              <th>Barang</th>
                              <th>Gudang</th>
                              <th>Stok</th>
                              <th>Min</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
               <div id="tab_max" class="tab-pane">
                  <div class="table-responsive">
                     <table id='table2' class="table table-bordered table-striped" style="width: 100%">
                        <thead>
                           <tr>
                              <th>Kode</th>
                              <th>Barang</th>
                              <th>Gudang</th>
                              <th>Stok</th>
                              <th>Max</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
   </div>
</div>

<div class="row" ><div class="col-md-6">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Time To PO</p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshtimepo"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <div id="time-po"></div>
      </div>
   </div>
</div>
<div class="col-md-6">
   <div class="box box-warning">
      <div class="box-header with-border">
         <p class="box-title">Time To Supplier Fullfillment </p>
         <div class="box-tools pull-right">
             <div class="btn-group">
               <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-default btn-sm" id="refreshtimemr"><i class="fa fa-refresh"></i></button>
               <button type="button" class="btn btn-default btn-sm"><i class="fa fa-question"></i></button>
             </div>
         </div>
      </div>      
      <div class="box-body">
         <div id="time-mr"></div>
      </div>
   </div>
</div></div>

</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">
$(function(){
   var periodes = {!!$periodes!!};
   var periode = {{$pnow}};
   $('#select2periode').select2({
      data: periodes,
   });
   $('#select2periode').val({{$pnow}}).trigger('change');  
   var tablepo = $('#tablepo').DataTable({
      'ajax' : {
         // url : "{{route('laporan.dashboardadmingetpo')}}",
         data: {
            'procurement' : 1,
         }
      },
      columns: [
      { "data": "nopo" },
      { "data": "supplier" },
      { "data": function (data, type, dataToSet) {
         return '<div class="progress progress-xs">'+
                               '<div class="progress-bar progress-bar-danger" style="width: '+data.persen+'%"></div>'+
                             '</div>'
      }},
      { "data": "label" },
      ],
   });
   var tablemin = $('#table1').DataTable({
      'processing': true,
      'serverside':true,
      "order": [],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      'ajax' : {
         // url : '{{route("laporan.stokminmax")}}',
         data:{
            'min' : 1
         }
      }
   });
   var tablemax = $('#table2').DataTable({
      'processing': true,
      'serverside':true,
      "order": [],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      'ajax' : {
         // url : '{{route("laporan.stokminmax")}}',
         data:{
            'min' : 0
         }
      }
   });

   inisialisasi();

   function inisialisasi(){
      tablepo.ajax.url("{{route('laporan.dashboardadmingetpo')}}").load();
      tablemin.ajax.url('{{route("laporan.stokminmax")}}').load();
      tablemax.ajax.url('{{route("laporan.stokminmax")}}').load();
      gettimepo();
      gettimemr();
   }

   

   function gettimepo(){
      $.ajax({
         url : "{{route('laporan.timetopo')}}",
         data: {
            'periode' : periode
         }
      }).done(function(response){
         // console.log(response)
         if(response.data.length>0){
            // chartbartimepo.destroy();
            // ctxbartimepo.canvas.height = response.data.length*28;
            // chartbartimepo = initializebar(response.labels,response.data,response.colors,ctxbartimepo,'Time To PO',tick2);
            $('#time-po').html('');
            for(i=0;i<response.data.length;i++){
               $('#time-po').append('<div class="col-sm-6">'+
                  '<div class="clearfix">'+
                    '<span class="pull-left">'+response.labels[i]+'</span>'+
                    '<small class="pull-right">'+response.data[i]+' Hari</small>'+
                  '</div>'+
                  '<div class="progress xs">'+
                    '<div class="progress-bar progress-bar-red" style="width: '+response.data[i]+'%;"></div>'+
                  '</div>'
                  )
            }
         }
         else{
            $('#time-po').html('Tidak Ada PO');
         }
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   function gettimemr(){
      $.ajax({
         url : "{{route('laporan.timetopo')}}",
         data: {
            'periode' : periode
         }
      }).done(function(response){
         console.log(response);
         if(response.data.length>0){
            // chartbartimepo.destroy();
            // ctxbartimepo.canvas.height = response.data.length*28;
            // chartbartimepo = initializebar(response.labels,response.data,response.colors,ctxbartimepo,'Time To PO',tick2);
            $('#time-mr').html('');
            for(i=0;i<response.data.length;i++){
               $('#time-mr').append('<div class="col-sm-6">'+
                  '<div class="clearfix">'+
                    '<span class="pull-left">'+response.labels[i]+'</span>'+
                    '<small class="pull-right">'+response.data[i]+' Hari</small>'+
                  '</div>'+
                  '<div class="progress xs">'+
                    '<div class="progress-bar progress-bar-green" style="width: '+response.data[i]+'%;"></div>'+
                  '</div>'
                  )
            }
         }
         else{
            $('#time-mr').html('Tidak Ada PO selesai');
         }
      }).fail(function(){
         console.log('terjadi kesalahan');
      });
   }

   $(document).on('click','#btnubahperiode',function(){
      periode = $('#select2periode').val();
      inisialisasi();
   });

   $(document).on('click','#refreshpo',function(){
      tablepo.ajax.reload();
   });

   $(document).on('click','#refreshpo',function(){
      tablemin.ajax.url('{{route("laporan.stokminmax")}}').load();
      tablemax.ajax.url('{{route("laporan.stokminmax")}}').load();
   });

   $(document).on('click','#refreshtimepo',function(){
      gettimepo();
   });

   $(document).on('click','#refreshtimemr',function(){
      gettimemr();
   });

});
</script>
@endpush
