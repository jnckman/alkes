@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endpush

@section('header22')
   <i class="fa fa-cart-plus" aria-hidden="true"></i> Detail Purchase Order Pembelian
@endsection

@section('breadcrumb')
   
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <h4>Data Purchase Order</h4>
        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nopo}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-calendar-o fa-fw" aria-hidden="true"></i> : {{$result[0]->tglpo}} | <i class="fa fa-money fa-fw" aria-hidden="true"></i> : {{number_format($result[0]->total_akhir)}}</h5>
        </div>
        <div class="col-md-12" style="margin-top: 20px;">
          <button class="btn btn-info buatform" style="width: 100%;">Tambahkan Order</button>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>Id</th>
                     <th>Nama Barang</th>
                     <th>Jumlah</th>
                     <th>Satuan</th>
                     <th>Harga Satuan</th>
                     <th>Diskon</th>
                     <th>Total</th>
                     <th>Action</th>
                     
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" style="height: 200px;">
                 <div class="col-md-12" style='margin-top: 10px;'>
                   <div class="col-md-12 form-group" style="padding-left: 0px;">
                     <label>No PO</label>
                     <input type="text" name="nopo" class="form-control" value="PO/B/">
                   </div>
                   <div class="col-md-12 totalpl">

                   </div>
                 </div>
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>
                            <th>
                               Nama Barang Baru
                            </th>
                            <th>
                               Jumlah
                            </th>
                            <th>
                              Harga satuan
                            </th>
                            <th>
                              Total
                            </th>
                          </thead>
                          <tbody class='tabelisibarang'>

                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   <div class="modal  fade" id="isipo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             
             <div class="modal-body" >
                

                
                 <div class="col-md-12 container" style='margin-top: 10px;'' id="extab2">

                   <ul class="nav nav-tabs nav-justified" >
                     <li class="active"><a data-toggle="tab" href="#home">Pembelian Barang NON Baru</a></li>
                     <li><a data-toggle="tab" href="#menu1">Pembelian Barang Baru</a></li>
                     
                   </ul>

                   <div class="tab-content" style="padding-top: 20px;">
                     <div id="home" class="tab-pane fade in active">
                      <form action='../../purchaseorderspembelian/submitisipobaranglama' method='POST'>
                      <div class="col-md-12">
                       <div class="col-md-6">
                         <div class="form-group">
                            <label>Barang</label>
                            <select class="form-control select2-hidden-accessible" id="select2barangs" required name="barangid">
                              <option></option>
                            </select>
                         </div>
                         
                       </div>
                       <div class="col-md-6">
                         <div class="form-group">
                           <label>Pricelist</label>
                           <select class="form-control select2-hidden-accessible" id="select2hargas" required name="pricelistid">
                           <option></option>
                           </select>
                         </div>
                         
                       </div>
                      </div>
                      <div class="col-md-12">
                      <div class="col-md-12">
                        <div class="form-group">
                           <label>ppn</label>
                           <input type="text" name="ppn" class="form-control" placeholder="ppn" >
                         </div>
                      </div>
                      </div>
                      <div class="col-md-12">
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Jumlah</label>
                           <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" id='jumlahinput'>
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Diskon (%)</label>
                           <input type="number" name="diskon" class="form-control" placeholder="Diskon" id='diskoninput'>
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Total</label>
                           <input type="text" name="total" class="form-control" placeholder="Jumlah" id='totalinput'>
                         </div>
                       </div>
                       <div class="col-md-12" >
                         <div class="form-group">
                          {{csrf_field()}}
                           <button type='submit' class="btn btn-success" style="width: 100%;">Submit</button>
                         </div>
                       </div>
                      </div>
                       <input type="hidden" name="purchaseorders_id" value="{{$id}}">
                       <input type="hidden" name="suppliers_id" value="{{$supplier}}">
                       
                        </form>
                     </div>
                     <div id="menu1" class="tab-pane fade">
                     <form action='../../purchaseorderspembelian/submitisipo' method='POST'>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Nama Barang</label>
                           <input type="text" name="nama" class="form-control" id='nopoform' placeholder="Nama Barnag Baru">
                         </div>
                       </div>
                       <div class="col-md-4" style="margin-bottom: 10px;">
                         <div class="form-group">
                           <label>Kategori</label>
                           <select class="form-control select2-hidden-accessible" id="select2kategoris" required name="kategoris_id">
                           <option></option>
                           </select>
                         </div>
                       </div>
                       <div class="col-md-4" style="margin-bottom: 10px;">
                         <div class="form-group">
                           <label>Satuan</label>
                           <select class="form-control select2-hidden-accessible" id="select2suppliers" required name="satuans_id">
                           <option></option>
                           </select>
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Harga</label>
                           <input type="number" name="harga" class="form-control" placeholder="Harga" id="hargasatuannew">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Jumlah</label>
                           <input type="number" name="jumlah" class="form-control" placeholder="Jumlah" id="jumlahinputnew">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Diskon (%)</label>
                           <input type="number" name="diskon" class="form-control" placeholder="Diskon" id="diskoninputnew">
                         </div>
                       </div>
                       <div class="col-md-12">
                         <div class="form-group">
                           <label>Total</label>
                           <input type="number" name="total" class="form-control" placeholder="Total" id="totalinputnew">
                         </div>
                       </div>
                       
                       <input type="hidden" name="purchaseorders_id" value="{{$id}}">
                       <input type="hidden" name="suppliers_id" value="{{$supplier}}">
                       
                       
                       
                       
                       
                       <div class="col-md-12" >
                        <div class="form-group">
                        {{csrf_field()}}
                         <button type='submit' class="btn btn-success">Submit</button>
                         </div>
                       </div>
                       </form>
                     </div>
                     
                   </div>
                   
                 </div>
                 
                

             </div>
             
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   <div class="modal modaledit fade" id="editpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
             </div>
             
             <div class="modal-body" >
                

                
                 <div class="col-md-12" style='margin-top: 10px;''>
                   <ul class="nav nav-tabs">
                     <li class="active"><a data-toggle="tab" href="#homeedit">Pembelian Barang NON Baru</a></li>
                     <li><a data-toggle="tab" href="#menu1edit">Pembelian Barang Baru</a></li>
                     
                   </ul>

                   <div class="tab-content">
                     <div id="homeedit" class="tab-pane fade in active">
                      <form  method='POST' id='editisipolama' action='../../editisipolama'>
                        <div class="col-md-12">
                         <div class="col-md-6">
                           <div class="form-group">
                              <label>Barang [old : <span id='oldnamabarang'></span>]</label>
                              <select class="form-control select2-hidden-accessible" id="select2barangsedit" required name="barangid">
                                <option></option>
                              </select>
                           </div>
                           
                         </div>
                         <div class="col-md-6">
                           <div class="form-group">
                             <label>Pricelist [old : <span id='oldhargabarang'></span>]</label>
                             <select class="form-control select2-hidden-accessible" id="select2hargasedit" required name="pricelistid">
                             <option></option>
                             </select>
                           </div>
                           
                         </div>
                        </div>
                        <div class="col-md-12">
                         <div class="col-md-4">
                           <div class="form-group">
                             <label>Jumlah</label>
                             <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" id='oldjumlahinput'>
                           </div>
                         </div>
                         <div class="col-md-4">
                           <div class="form-group">
                             <label>Diskon (%)</label>
                             <input type="number" name="diskon" class="form-control" placeholder="Diskon" id='olddiskoninput'>
                           </div>
                         </div>
                         <div class="col-md-4">
                           <div class="form-group">
                             <label>Total</label>
                             <input type="text" name="total" class="form-control" placeholder="Jumlah" id='oldtotalinput'>
                           </div>
                         </div>
                       
                        </div>
                       <input type="hidden" name="purchaseorders_id" value="{{$id}}">
                       <input type="hidden" name="isipo_id" value="" id="oldisipoid">
                       <input type="hidden" name="suppliers_id" value="{{$supplier}}">
                       <div class="col-md-12" >
                       <div class="form-group">
                        {{csrf_field()}}
                         <button type='submit' class="btn btn-success">Submit Edited</button>
                      
                        </form>
                        </div>
                        </div>
                     </div>
                     <div id="menu1edit" class="tab-pane fade">
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Nama Barang</label>
                           <input type="text" name="nama" class="form-control" id='nama' placeholder="Nama Barnag Baru">
                         </div>
                       </div>
                       <div class="col-md-4" style="margin-bottom: 10px;">
                         <div class="form-group">
                           <label>Kategori <span id="kategori"></span></label>
                           <select class="form-control select2-hidden-accessible" id="select2kategorisedit" required name="kategoris_id">
                           <option></option>
                           </select>
                         </div>
                       </div>
                       <div class="col-md-4" style="margin-bottom: 10px;">
                         <div class="form-group">
                           <label>Satuan <span id="satuan"></span></label>
                           <select class="form-control select2-hidden-accessible" id="select2suppliersedit" required name="satuans_id">
                           <option></option>
                           </select>
                         </div>
                       </div>
                       
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Harga</label>
                           <input type="number" name="harga" class="form-control" placeholder="Harga" id="hargasatuan">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Jumlah</label>
                           <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" id="jumlahinput">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                           <label>Diskon</label>
                           <input type="number" name="diskon" class="form-control" placeholder="Diskon" id="diskon">
                         </div>
                       </div>
                       <div class="col-md-12">
                         <div class="form-group">
                           <label>Total</label>
                           <input type="text" name="total" class="form-control" placeholder="Total" id="totalinput">
                         </div>
                       </div>
                       <input type="hidden" name="idisipo" id="idisipo">
                       <input type="hidden" name="idbarangpo" id="idbarangpo">
                       <input type="hidden" name="purchaseorders_id" value="{{$id}}">
                       <input type="hidden" name="suppliers_id" value="{{$supplier}}">
                       
                       
                       
                       
                       
                       <div class="col-md-12" >
                        {{csrf_field()}}
                         <button type='submit' class="btn btn-success">Submit Edited</button>

                       </div>
                     </div>
                     <div id="menu2edit" class="tab-pane fade">
                       <h3>Menu 2</h3>
                       <p>Some content in menu 2.</p>
                     </div>
                   </div>
                   <!-- <div class="col-md-4" style="padding-left: 0px;">
                     FORM PO
                   </div>
                   <div class="col-md-8 totalpl">

                   </div> -->
                 </div>
                 
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">

                   </div>
                 </div> -->

             </div>
             
             <div class="modal-footer">

                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
@endsection

@push('bottom')


<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {

          var globaljumlah = 0;
          var globaldiskon = 0;
          var globalhargasatuan = 0;
          var globaltotal = 0;

          var globaljumlahnew = 0;
          var globaldiskonnew = 0;
          var globalhargasatuannew = 0;
          var globaltotalnew = 0;

          
          var editglobaljumlah = 0; 
          var editglobaldiskon = 0; 
          var editglobalhargasatuan = 0; 
          var editglobaltotal = 0; 
          
          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': 'dtajax/{{$id}}',

          } );
          
          $.fn.modal.Constructor.prototype.enforceFocus = function() {};
          
          var satuans = <?php echo $satuan; ?>;
          var barangs = <?php echo $barang; ?>;
          var kategoris = <?php echo $kategori; ?>;
          
          $('#select2suppliers').select2({
             data:satuans,
             placeholder:'Pilih Satuan'
          });
          $('#select2kategoris').select2({
             data:kategoris,
             placeholder:'Pilih Satuan'
          });
          $('#select2barangs').select2({
             data:barangs,
             placeholder:'Pilih Barang'
          });

          $('#select2barangsedit').select2({
             data:barangs,
             placeholder:'Pilih Barang'
          });



          $('#select2suppliersedit').select2({
             data:satuans,
             placeholder:'Pilih Satuan'
          });
          $('#select2kategorisedit').select2({
             data:kategoris,
             placeholder:'Pilih Satuan'
          });
          $('body').on('click', '.buatform', function(event) {
            event.preventDefault();
            $('#isipo').modal('show');
          });

          $('body').on('change','#select2barangs',function(){
            id = $(this).val()
            idsup = {{$supplier}}

            hasil = [];
            $.ajax({

                type: 'post',
                url: '../../ambildatapricelist',
                data: {'id':id,'idsup':idsup},
                dataType: 'json',
                success: function (hasil) {
                    
                    $('#select2hargas').val('');
                    $('#select2hargas').html('');

                    $('#select2hargas').select2({data:null});

                    $('#select2hargas').select2({data:hasil});

                   
                },
                error: function (hasil) {
                   alert('gagal');
                      console.log(hasil);
                }
             });

          });

          $('body').on('change','#select2barangsedit',function(){
            id = $(this).val()
            idsup = {{$supplier}}

            hasil = [];
            $.ajax({

                type: 'post',
                url: '../../ambildatapricelist',
                data: {'id':id,'idsup':idsup},
                dataType: 'json',
                success: function (hasil) {
                  console.log(hasil);
                    $('#select2hargasedit').val('');
                    $('#select2hargasedit').html('');
                    $('#select2hargasedit').select2({data:{} });
                    $('#select2hargasedit').select2({data:hasil});

                   
                },
                error: function (hasil) {
                   alert('gagal');
                      console.log(hasil);
                }
             });

          });

          $("body").on('keyup','#oldjumlahinput',function(){

              editglobalhargasatuan = $('#select2hargasedit').select2('data')[0].text.split('-');
              editglobalhargasatuan = editglobalhargasatuan[1];
              //alert(editglobalhargasatuan);
              editglobaljumlah = $('#oldjumlahinput').val(); 
              editglobaldiskon = $('#olddiskoninput').val();
              if(editglobaldiskon < 100){
                editglobaldiskon = editglobaldiskon * 10 / 100;
              }

              editglobaltotal = (1 * editglobaljumlah) * (editglobalhargasatuan * 1 ) ;
              
              if(editglobaldiskon < 100){
                editglobaltotal = editglobaltotal  - (editglobaltotal * editglobaldiskon);
              }
              else{
                editglobaltotal = editglobaltotal - editglobaldiskon
              }

              $('#oldtotalinput').val(editglobaltotal);
              //alert(editglobaltotal);

          })
          $("body").on('keyup','#olddiskoninput',function(){

              editglobalhargasatuan = $('#select2hargasedit').select2('data')[0].text.split('-');
              editglobalhargasatuan = editglobalhargasatuan[1];
              //alert(editglobalhargasatuan);
              editglobaljumlah = $('#oldjumlahinput').val(); 
              editglobaldiskon = $('#olddiskoninput').val();
              if(editglobaldiskon < 100){
                editglobaldiskon = editglobaldiskon / 100;
              }

              editglobaltotal = (1 * editglobaljumlah) * (editglobalhargasatuan * 1 ) ;

              if(editglobaldiskon < 100){
                editglobaltotal = editglobaltotal  - (editglobaltotal * editglobaldiskon);
              }
              else{
                editglobaltotal = editglobaltotal - editglobaldiskon
              }

              
              $('#oldtotalinput').val(editglobaltotal);
              //alert(editglobaltotal);

          })


          $("body").on('keyup','#jumlahinput',function(){
              globalhargasatuan = $('#select2hargas').select2('data')[0].text.split('-');

              globaltotal = (1*(globalhargasatuan[1])) * $('#jumlahinput').val();
              $('#totalinput').val(globaltotal.toLocaleString());
          });

          $("body").on('keyup','#jumlahinputnew',function(){

              globalhargasatuannew = $('#hargasatuannew').val();

              globaltotalnew = globalhargasatuannew * $('#jumlahinputnew').val();

              $('#totalinputnew').val(globaltotalnew);
          });

          $("body").on('keyup','#diskoninput',function(){
              globalhargasatuan = $('#select2hargas').select2('data')[0].text.split('-');;

              globaltotal = (1*(globalhargasatuan[1])) * $('#jumlahinput').val() - (($('#diskoninput').val()/100)*(1*(globalhargasatuan[1])) * $('#jumlahinput').val());
              $('#totalinput').val(globaltotal.toLocaleString());
          });

          $("body").on('keyup','#diskoninputnew',function(){
              globalhargasatuannew = $('#hargasatuannew').val();

              globaltotalnew = globalhargasatuannew * $('#jumlahinputnew').val() - (($('#diskoninputnew').val()/100)*(1*globalhargasatuannew) * $('#jumlahinputnew').val());
              
              $('#totalinputnew').val(globaltotalnew);
          });
          $('body').on('click','.btnedit',function(){
            
             
             event.preventDefault();
             $('#editpo').modal('show');
             id = $(this).attr('id');
             $.ajax({

                type: 'post',
                url: '../../ambildataeditdetail',
                data: {'id':id},
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                   $('#harga').val(data.harga);
                   $('#oldhargabarang').html(data.harga);
                   $('#olddiskoninput').val(data.diskon);
                   $('#diskon').val(data.diskon);
                   $('#jumlah').val(data.jumlah);
                   $('#oldjumlahinput').val(data.jumlah);
                   $('#total').val(data.total_akhir);
                   $('#oldtotalinput').val(data.total_akhir);
                   $('#nama').val(data.nama);
                   $('#oldnamabarang').html(data.nama);
                   $('#idisipo').val(data.id);
                   $('#oldisipoid').val(data.id);
                   $('#idbarangpo').val(data.barangs_id);
                   var alamat = $('#editisipolama').attr('action')+'/'+id;
                   
                   $('#editisipolama').attr('action',alamat);



                   
                },
                error: function (data) {
                   alert('gagal');
                      console.log(data);
                }
             });
          })


      } );
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<style type="text/css">
 .selected{
  border-bottom: 2px solid grey;
 }
 .select2{
  width: 100% !important;
 }
</style>
@endpush
