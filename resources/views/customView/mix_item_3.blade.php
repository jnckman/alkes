@extends('crudbooster::admin_template')
@section('content')
@push('head')
<link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet" />
@endpush
<!-- <h2 class="text-center">Mix Items</h2> -->
<div class="panel panel-default">
  <div class="panel-heading">
    <!-- TIGA LINGKARAN -->
    <div class="stepwizard">
        <div class="stepwizard-row">
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',1) }}" type="button" class="btn btn-default btn-circle">1</a>
                <p>Resep</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',2) }}" type="button" class="btn btn-default btn-circle">2</a>
                <p>Pilih Bahan</p>
            </div>
            <div class="stepwizard-step">
                <a href="{{ route('resep.mix',3) }}" type="button" class="btn btn-primary btn-circle">3</a>
                <p>Finishing</p>
            </div>
        </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered">
        <thead>
          {{--@foreach($mix_1 as $k1 => $m1)
            <!-- <th>{{ $k1 }}</th> -->
          @endforeach--}}
          <th>Nama</th>
          <th>Kode</th>
          <th>Satuan</th>
          <th>Jenis</th>
          <th>Kategori</th>
          <th>Group</th>
        </thead>
        <tbody>
          @foreach($mix_1 as $k2 => $m2)
            <td>{{ $m2 }}</td>
          @endforeach
        </tbody>
      </table>
    </div>
    <br><br>
    <hr>
    @if( $mix_2 )
    <div class="table-responsive">
      <table id="table_dashboard" class="table table-hover table-striped table-bordered">
        <thead>
          <th>Nama</th>
          <th>Kode</th>
          <th>Jumlah</th>
        </thead>
        <tbody>
             @foreach( $mix_2 as $k => $m)
            <tr>
              @foreach( $m as $k2 => $m2)
                @if(($k2==='id'))

                @else
                  <td> {{ $m2 }} </td>
                @endif
              @endforeach
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
    @else
      Belum ada data
    @endif
    <div class="form-group">
      <form method='post' action="{{ route('reseps.aksi')}}">
          <label>Gudang Tujuan</label>
          <select name="gudangs" class="form-control" id="select2gudangs" required>
            <option></option>
          </select>
          <input type="hidden" name="lanjut" value="end">
    </div>
  </div>
    <div class='panel-footer'>
      {{ csrf_field() }}
      <a href="{{ route('resep.mix',2) }}" type="button" class="btn btn-primary">Prev</a>
      <button type="submit" class="btn btn-success pull-right">Submit</button>
      </form>
      <br>
      <br>
    </div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
  var gudangs = <?php echo($gudangs); ?>;

  $('#select2gudangs').select2({
    data : gudangs,
    placeholder : 'Pilih Gudang tujuan',
  })
})
</script>
@endpush
