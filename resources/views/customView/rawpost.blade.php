@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan RAW DATA
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Customer Dua Berlian</li>
@endsection

@section('content')
<div class="box box-default">
   
   <div class="col-md-12">
      <table class="table table-bordered table-responsive">
         <thead>
            <tr>
               <td>
               ITEM</td><td>  CUSTOMER </td><td>INV </td><td>  INVDATE</td><td>  SALETYPE </td><td>QTYPO</td><td> QTY</td><td>   VALUE</td><td> DISC</td>
            </tr>
         </thead>
         <tbody>
         
            @foreach($jual as $d)
            @if(sizeof($d->sales_order) == 1)
            <tr>
               
               <td>{{$d->barang->itemnum}}</td>
               <td>{{$d->sales_order->customer->nocust}}</td>
               <td>{{$d->sales_order->id}}/TKG/{{$tgl[1]}}/{{$tgl[2]}}</td>
               <td>??</td>
               <td>0</td>
               <td>{{$d->jumlah}}</td>
               <td>{{$d->jumlah}}</td>
               <td>{{number_format($d->total)}}</td>
               <td>{{$d->diskon}}</td>
                  
            </tr>
            @endif
            @endforeach
         </tbody>
      </table>
   </div>   
</div>



@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">

@endpush
