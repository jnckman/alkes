@extends('crudbooster::admin_template')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('content')
<p><a title="Return" href="{{CRUDBooster::adminPath('barangs')}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Data Barangs</a></p>
<div class="box box-default" style="padding-top: 20px;">
<div class="box-body">
	Nama Barang : <b>{{$barang->nama}}</b>
	<a href="{{CRUDBooster::adminPath('harga_juals/addhargajualbybarang/'.$barang->id)}}" class="btn btn-sm btn-primary pull-right" title="Add Data">
              <i class="fa fa-plus-circle"></i> Add Harga Jual
            </a>
</div></div>
<div class="box box-default" style="padding-top: 20px;">
 <div class="box-body table-responsive no-padding">
    <div class="col-md-12">
       <table class="table table-striped table-bordered" id='table_master'>
          <thead>
             <tr>
                <th>Tanggal</th>
                <th>Harga Jual (Rp)</th>
                <th>Harga Diskon (Rp)</th>
                <th>Ket</th>
                <th>Action</th>
             </tr>
          </thead>
          <tfoot>
             <tr>
                <th>Tanggal</th>
                <th>Harga Jual (Rp)</th>
                <th>Harga Diskon (Rp)</th>
                <th>Ket</th>
                <th>Action</th>
             </tr>
          </tfoot>
       </table>
    </div>
 </div>
</div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function(){
	var table = $('#table_master').DataTable({
		'ajax': {
			'url': '{{route("harga_jual.bybarang_dt",$barang->id)}}'
		},
		fixedHeader: {
			header: true,
			footer: true
		},
		'processing': true,
		'serverside':true,
		'order': [[0, 'desc']]
	});
});
</script>
@endpush