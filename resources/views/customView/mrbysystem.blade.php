@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-hand-paper-o" aria-hidden="true"></i> Material Request by System
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id_periode }}">List Department</a></li>
   <li class="active">MR by System</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i><b>Departement</b> : {{$result[0]->nama}}</h5>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../../rekapallsubdeptmr/{{$id}}/{{$id_periode}}"><button class="btn btn-info">Rekap seluruh Sub Departments</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                     <th>ID</th>
                     <th>Nama Sub Department</th>
                     <th>Phone</th>
                     <th>Email</th>
                     <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>
@endsection

@push('bottom')
<script type="text/javascript">
   $(document).ready(function() {

      // alert("{{$id_periode}}");

      $('.table1').DataTable( {
        'processing': true,
        'serverside':true,
        'ajax': "{{ route('mr.ajax', ['id' => $id, 'id_listdept' => $id_periode]) }}",
        //'ajax': '../mrdtajax/{{ $id }}',

      });
      
   });
</script>
@endpush
