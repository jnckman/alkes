@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-close" aria-hidden="true"></i> Pending MR
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/lihatpembukuanrekap/{{ $id }}">Purchase Request</a></li>
   <li class="active">Susun PO</li>
@endsection

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        
        <div class="col-md-12">
          <h4>LAPORAN SO [{{$data['jenislaporan']}}] - [{{$data['tanggal']}} - {{$data['tanggalend']}}]</h4>
        </div>
        
        
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>

                     <th>CUST_NUM</th>
                     <th>SLSM_NUM</th>
                     <th>SDIS_NUM</th>
                     <th>CUST_GROUP</th>
                     <th>TYPE_NUM</th>
                     <th>ADDRESS</th>
                 </tr>
          </thead>
          <tbody>
          	
	          @foreach ($hasil as $i) 
	            <tr>
	            	<td>{{$i->id}}</td>
                <td>CL_1</td>
                <td>14</td>
                <td>{{$i->group}}</td>
                <td>{{$i->nama}}</td>
                <td>{{$i->group}}</td>
                <td>{{$i->alamat}}</td>
              </tr>
	            	
	              
	            
	          @endforeach
	        
			
          </tbody>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   
   
@endsection

@section('datatable')

@endsection

