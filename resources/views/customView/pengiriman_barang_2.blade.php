@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Pengiriman Barang Part 2
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/wawan.css') }}" rel="stylesheet">
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Pengiriman Barang Part 2</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="stepwizard">
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-paper-plane" aria-hidden="true"></i> Jenis Pengiriman</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-crosshairs" aria-hidden="true"></i> List MR/SO</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-success btn-lg"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Ambil Barang</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-plus" aria-hidden="true"></i> Bagi Barang</a>
            </div>
         </div>
     </div>
  </div>
</div>

<div class="container-fluid">
   <div class="panel panel-default">
      <div class="panel-body padding-lr0">
         <div class="panel-group col-md-12">
            
            <div class="col-md-12">
               <p>
                  <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
                     <i class="fa fa-home" aria-hidden="true"></i> Gudang
                  </button>
               </p>
               <div class="collapse" id="collapseExample2">
                  <div class="card2 card-block">
                     <table class="table table-striped table-bordered" style="margin-bottom: 0px !important;">
                        <tr>
                           <th class="th-center font-s">{{ $gudang->nama }}</th>
                        </tr>
                        <tr>
                           <th class="th-center font-s">{{ $gudang->notelp }}</th>
                        </tr>
                        <tr>
                           <th class="th-center font-s">{{ $gudang->email }}</th>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="col-md-12">
               <p>
                  <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                     <i class="fa fa-list-ul" aria-hidden="true"></i> Daftar
                     @if($jenis[0] == 1) MR
                     @else SO
                     @endif
                  </button>
               </p>
               <div class="collapse" id="collapseExample">
                  <div class="card2 card-block">
                     <table class="table table-striped table-bordered" style="margin-bottom: 0px !important;">
                        <thead>
                           <tr class="success">
                              <th class="th-center font-s"><strong>Barang</strong></th>
                              <th width="10%" class="th-center font-s"><strong>Permintaan</strong></th>
                           </tr>
                        </thead>
                        <tbody>

                           @foreach($detail_data->groupBy('nota_id') as $nota)
                              <?php $i=0 ?>
                              @foreach($nota as $n)
                              
                                 @if($i == 0)
                                 <tr>
                                    <td align="center" class="text-center font-s2" colspan="2"><button class="btn btn-xs btn-info btn-block toggler" data-prod-cat="{{ $n->nota_id }}" type="button" name="button">{{ $n->nomor_nota }} {{$n->area}}</button></td>
                                 </tr>
                                 @endif
                                 <tr class="cat{{ $n->nota_id }}" style="display: none;">
                                    <td align="center" class="text-center font-s2">{{ $n->nama }} </h6></td>
                                    <td align="center" class="text-center font-s2">
                                       @if ($n->jumlah != 0) {{ $n->jumlah }}
                                       @else Selesai
                                       @endif
                                    </td>
                                 </tr>
                                 <?php $i++; ?>
                              @endforeach
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <form id="form_2" method="post" action="{{ route('action.pengiriman.barang', 2)}}">
            {{ csrf_field() }}
            @foreach($nota_id as $nota_id)
            <input type="hidden" name="nota_id[]" value="{{ $nota_id }}">
            @endforeach
            <input type="hidden" name="jenis[]" value="{{ $jenis[0] }}">
            <input type="hidden" name="jenis[]" value="{{ $jenis[1] }}">
            <input type="hidden" name="periode" value="{{ $periode }}">
            <input type="hidden" id="gudang_id" name="gudang_id" value="{{ $gudang->id }}">

            <div class="col-md-12">
               <div class="col-md-6 pull-right" style="margin-bottom: 10px;">
                  <button id="btn_next" type="submit" class="btn btn-info btn-block pull-right">NEXT</button>
               </div>
            </div>

            <div class="col-md-12" style="margin-top: 10px;">
               <table class="table table-striped table-bordered" id='table_barang'>
                  <thead>
                     <tr class="success">
                        <th class="font-s"><strong>Nama Barang</strong></th>
                        <th class="th-center font-s" width="15%"><strong>Sisa Permintaan</strong></th>
                        <th class="th-center font-s" width="15%"><strong>Stok</strong></th>
                        <th class="th-center font-s" width="15%"><strong>Ambil</strong></th>
                     </tr>
                  </thead>
                  <tbody id="tbody_barang">
                     @foreach($detail_data->groupBy('barang_id') as $b)
                     
                     @if($b->sum('jumlah') != 0)
                     <tr>
                        <td class="font-s2">{{ $b[0]->nama }}</h6></td>
                        <td class="font-s2" align="center">{{ $b->sum('jumlah') }}</td>
                        @foreach($barang_gudang as $index => $bg)
                           @if($bg->barang_id == $b[0]->barang_id)
                              @if($bg->stok != 0)
                                 <td class="font-s2" align="center">{{ $bg->stok }}</td>
                                 <td align="center" valign="center">
                                    <?php
                                       if ($bg->stok > $b->sum('jumlah')) $max = $b->sum('jumlah');
                                       else $max = $bg->stok;
                                       if($b->sum('jumlah')<=$bg->stok){
                                          $defaultinsert = $b->sum('jumlah');
                                       }
                                       else{
                                          $defaultinsert = $bg->stok;
                                       }
                                       // dd($defaultinsert);
                                    ?>
                                    <input class="input_ambil" style='width:50px' type='number' min='0' max='{{ $max }}' name="barang[{{$b[0]->barang_id}}][jumlah]" value="{{$defaultinsert}}">
                                    <input type="hidden" name="barang[{{$b[0]->barang_id}}][nama]" value="{{ $b[0]->nama }}">
                                 </td>
                              @else
                                 <td class="danger font-s2" align="center">Kosong</td>
                                 <td></td>
                              @endif
                              <?php unset($barang_gudang[$index]); ?>
                           @endif
                        @endforeach
                     </tr>
                     @endif
                     @endforeach
                  </tbody>
               </table>
            </div>
         </form>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script type="text/javascript">

$(function () {
   $(".toggler").click(function(e){
      e.preventDefault();
      $('.cat'+$(this).attr('data-prod-cat')).toggle();
   });
});

$('#form_2').submit(function(e){
   e.preventDefault();
   var form = this;
   swal({
      title: "Sudah benar ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya',
      cancelButtonText: "Tidak",
      closeOnConfirm: false,
      closeOnCancel: true
   },
   function(isConfirm) {
      if (isConfirm) {
         $('#tbody_barang tr td').each(function () {
            if ($(this).find('.input_ambil').val() == "") {
               $(this).remove();
            }
         });
         form.submit();
      }
   });
});

</script>
@endpush
