@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text" aria-hidden="true"></i> Laporan Admin
@endsection

@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
<style type="text/css">
   textarea {
       resize: none;
   }
</style>
@endpush

@section('content')
<div class="box box-default">
   <!-- <form id="formubahperiode"> -->
   <div class="box-header">
      <ul class="nav nav-tabs nav-justified">
        <li class="active"><a data-toggle="tab" href="#tab_add_retur"><b>Tambah Retur MR</b></a></li>
        <li ><a data-toggle="tab" href="#tab_retur"><b>List Retur</b></a></li>
      </ul>
   </div>
   <div class="box-body">
      <div class="tab-content">
        <div id="tab_add_retur" class="tab-pane fade in active">
         <div class="table-responsive">
            <table id="table1" class="table table-bordered table-hover" style="width:100%">
               <thead>
                  <tr>
                     <th>NoMR</th>
                     <th>Project</th>
                     <th>Area</th>
                     <th>Periode</th>
                     <th>Subdept</th>
                     <th>Tanggal</th>
                     <th>Total</th>
                     <th>Creator</th>
                     <th>Approval</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tfoot>
                  
               </tfoot>
            </table>
         </div>
        </div>
        <div id="tab_retur" class="tab-pane fade in">
         <div class="table-responsive">
            <table id="table2" class="table table-bordered table-hover" style="width:100%">
               <thead>
                  <tr>
                     <th>NoRetur</th>
                     <th>NoMR</th>
                     <th>Area</th>
                     <th>Periode</th>
                     <th>Tanggal</th>
                     <th>Creator</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
            </table>
         </div>
        </div>
     </div>
   </div>
   <div class="box-footer">
   </div>
   <!-- </form> -->
</div>

<div class="modal" id="modal_isi_mr" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"></h4>
            <h4 class="modal-title2"></h4>
         </div>
         <form id="form1">
            {{csrf_field()}}
            <input type="hidden" name="mr_subdepts_id" id="mr_subdepts_id">
         <div class="modal-body" id="table_dashboard">
            <table class="table table-bordered table-striped" id="table3" style="width:100%;">
               <thead>
                 <tr>
                   <th>Barang</th>
                   <th>Jumlah</th>
                   <th>Keterangan</th>
                   <th>Retur</th>
                 </tr>
               </thead>
            </table>
            <hr/>
            <label>Keterangan Retur : </label>
            <textarea name="ketRetur" rows="5" placeholder="Masukkan Keterangan Retur Disini" style="width: 100%"></textarea>
         </div>
         <div class="modal-footer">
            <button type="submit" id="btnsubmit1" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
         </form>
         <form id="form2">
         {{csrf_field()}}
         </form>
      </div>
   </div>
</div>

<div class="modal" id="modal_isi_retur" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"></h4>
            <h4 class="modal-title2"></h4>
         </div>
         <div class="modal-body" id="table_dashboard">
            <table class="table table-bordered table-striped" id="table4" style="width:100%;">
               <thead>
                 <tr>
                   <th>Barang</th>
                   <th>Jumlah</th>
                 </tr>
               </thead>
            </table>
            <hr/>
            <label>Keterangan Retur : </label>
            <pre id="show_keterangan_retur"></pre>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
   var table1 = $("#table1").DataTable({
      processing: true,
      serverside:true,
      lengthMenu: [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      columns: [
         { "data": "nomr" },
         { "data": "project" },
         { "data": "area" },
         { "data": "periode" },
         { "data": "subdept" },
         { "data": "tanggal" },
         { "data": "total" },
         { "data": "creator" },
         { "data": "approval" },
         { "data": "action" },
      ],
      order : [],
      ajax : {
            url : '{{route("retur_mr.listmr")}}',
         },
   });
   var table2 = $("#table2").DataTable({
      processing: true,
      serverside:true,
      lengthMenu: [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      columns: [
         { "data": "noretur" },
         { "data": "nomr" },
         { "data": "area" },
         { "data": "periode" },
         { "data": "tanggal" },
         { "data": "creator" },
         { "data": "status" },
         { "data": "action" },
      ],
      order : [],
      ajax : {
            url : '{{route("retur_mr.listretur")}}',
         },
   });

   var table3 = $("#table3").DataTable({
      paging: false,
      processing: true,
      serverside:true,
      order : [],
      lengthChange: false,
      searching: false,
      bInfo : false,
      columns: [
         { "data": "barang" },
         { "data": "jumlah" },
         { "data": "keterangan" },
         { "data": function (data, type, dataToSet) {
            return '<input style="width:100%" placeholder= "0" name="input['+data.id+']" type="number" min="0" max="'+data.jumlah+'" />';
         }}
      ],
      });

   var table4 = $("#table4").DataTable({paging: false, processing: true, serverside:true, order : [], lengthChange: false, searching: false, bInfo : false, columns: [{ "data": "barang" }, { "data": "jumlah" }, ], });

   $(document).on("click",".btn_isi_mr",function(){
      data = table1.row($(this).parent().parent()).data();
      url = 'returlistisimr/'+data['id'];
      table3.ajax.url(url).load();
      $("#modal_isi_mr").find(".modal-title").html(data['nomr']+' | '+data['area']);
      $("#modal_isi_mr").find(".modal-title2").html(data['periode']);
      $("#mr_subdepts_id").val(data['id'])
      $("#modal_isi_mr").modal();
   });

   $(document).on("click",".btn_isi_retur",function(){
      data = table2.row($(this).parent().parent()).data();
      console.log(data);
      url = 'returlistisiretur/'+data['id'];
      table4.ajax.url(url).load();
      $("#modal_isi_retur").find(".modal-title").html(data['nomr']+' | '+data['area']);
      $("#modal_isi_retur").find(".modal-title2").html(data['noretur']);
      $("#modal_isi_retur").find("#show_keterangan_retur").html(data['keterangan']);
      $("#modal_isi_retur").modal();
   });

   $("#form1").on("submit",function(event){
      event.preventDefault();
      var form = $("#form1");
      if(checkInputFormKosongSemua()==false){
         alert('Input kosong');
         return;
      }

      $.ajax({
         url:"{{route('retur_mr.insert_retur')}}",
         data: $("#form1").serialize(),
         type:"post"
      }).done(function(response){
         console.log(response);
         $("#modal_isi_mr").modal('hide');
         $("textarea[name='ketRetur']").val('');
         alert('Berhasil Menambahkan Data !');
         table2.ajax.reload();
      }).fail(function(){
         alert('Gagal Menambahkan Data !');
      });
   });

   function checkInputFormKosongSemua(){
      konter1 = 0;
      konter2 = 0;
      $('input[type="number"]').each(function(){
         if(this.value=='')konter1++;
         konter2++;
      });
      if(konter1 == konter2) return false;
      return true;
   }
})
</script>
@endpush
