@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-th-list" aria-hidden="true"></i> List MR Belum Approve
@endsection

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">List MR Belum Approve</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id='table_master'>
                  <thead>
                     <tr class="success">
                        <th>No</th>
                        <th>List MR</th>
                        <th>Periode</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i=1; ?>
                     @foreach ($periode_mr->groupBy('periode_mr_id') as $k => $periode)
                     <tr>
                        <td>{{ $i }}</td>
                        <td>
                           @foreach ($mr_subdept as $k2 => $mr)
                              @if ($periode[0]->periode_mr_id == $mr->periode_mr_id)
                                 {{ $mr->nomor_mr }},
                                 <?php unset($mr_subdept[$k2]); ?>
                              @endif
                           @endforeach
                        </td>
                        <td>{{ $periode[0]->periode_mr_judul }}</td>
                        <td><a href="{{ route('pembukuan.rekap', ['id' => $periode[0]->periode_mr_id ]) }}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Detail</a></td>
                     </tr>
                        <?php $i++; ?>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
