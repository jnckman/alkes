@extends('crudbooster::admin_template')
@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/pace-theme-minimal.css') }}">
@endpush
@section('header22')
   <i class="fa fa-th-list" aria-hidden="true"></i> List Pending Approve PR
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">List Pending Approve PR</li>
@endsection

@section('content')
@if (session('status_delete'))
   <div class="alert alert-success">Delete Berhasil !!!</div>
   {{ session()->forget('status_delete') }}
@elseif (session('status_addprice'))
   <div class="alert alert-success">Pricelist Berhasil di tambah !!!</div>
   {{ session()->forget('status_addprice') }}
@elseif (session('change_pricelist'))
   <div class="alert alert-success">Pricelist Berhasil di ganti !!!</div>
   {{ session()->forget('change_pricelist') }}
@endif
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default">
         <div class="box-body table-responsive" id="table_dashboard">
               <form id='formApproval' method="post" action="{{route('pr.updateApprove')}}">
               <table class="table table-hover table-striped table-bordered" id='tableList'>
                {{csrf_field()}}
                  <thead>
                     <tr>
                      <th>Tanggal PR</th>
                       <th>Nama Barang</th>
                       <th>Kode</th>
                       <th>Jumlah</th>
                       <th>Min/Max</th>
                       <th>Sat</th>
                       <th>Average</th>
                       <th>Selected</th>
                       <th>Supplier * Satuan</th>
                       <th>Action</th>
                       <th width="5%"><input type="checkbox" name="select_all" value="1" class="checkall"></th>
                     </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                      <tr>
                        <th colspan="7" style="text-align:right">Total:</th>
                        <th colspan="2"></th>
                        <th width="5%"><input type="checkbox" name="select_all" value="1" class="checkall"></th>
                     </tr>
                     <!-- <tr>
                       <th>ID</th>
                       <th>Nama Barang</th>
                       <th>Kode</th>
                       <th>Jumlah</th>
                       <th>Min/Max</th>
                       <th>Sat</th>
                       <th>Average</th>
                       <th>Selected</th>
                       <th>Supplier * Satuan</th>
                       <th>Action</th>
                       
                     </tr> -->
                  </tfoot>
               </table>
               <button type="submit" class="btn btn-info btn-sm pull-right" onclick="return confirm('Apakah anda yakin?')">Approve</button>
               </form>
        </div>
      </div>
   </div>
</div>

<div class="modal" id="myModallistmr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Daftar MR</h4>
            <!-- <h5><B>Perhatikan !! Setiap Perubahan jumlah akan mengganti MR bersangkutan</B></h5> -->
         </div>
         <div class="modal-body" style="height: 200px;">
            <table class="table table-bordered" id="table_dashboard">
               <thead>
                 <tr>
                   <th>No MR</th>
                   <th>Project </th>
                   <th>Barang di Request</th>
                   <th>Action</th>
                   <!-- <td>Reset Jumlah</td> -->
                 </tr>
               </thead>
               <tbody class="table-body-mrpr">
               </tbody>
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="modalListBarang" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Daftar Barang</h4>
         </div>
         <div class="modal-body" style="height: 200px;" id="table_dashboard">
            <table class="table table-bordered" id="tableListBarang">
               <thead>
                 <tr>
                   <th>Barang</th>
                   <th>Jumlah</th>
                   <th>Total</th>
                 </tr>
               </thead>
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#myModallistmr">Back</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<script type="text/javascript">
$(function(){

  var tableListBelumApprove = $('#tableList').DataTable({
      'ajax': {
         'url': '{{ route("pr.dtlistBelumApprove") }}'
      },
      fixedHeader: {
            header: true,
            footer: true
        },
      'columnDefs': [{
         'targets': [8,9],
         'searchable': false,
         'orderable': false,
        }],
      'processing': true,
      'serverside':true,
      'order': [[1, 'asc']],
      "footerCallback": function ( row, data, start, end, display ) {
                 var api = this.api(), data;

                 // Remove the formatting to get integer data for summation
                 var intVal = function ( i ) {
                     return typeof i === 'string' ?
                         i.replace(/[\$,]/g, '')*1 :
                         typeof i === 'number' ?
                             i : 0;
                 };

                 // Total over all pages
                 dttotal = api
                     .column( 7 )
                     .data()
                     .reduce( function (a, b) {
                         return intVal(a) + intVal(b);
                     }, 0 );

                 // Update footer
                 $( api.column( 7 ).footer() ).html(
                      dttotal.toLocaleString("en") +',-'
                 );
         },

      });

  var tableListBarang = $('#tableListBarang').DataTable({
      'processing': true,
      'serverside':true,
      "pageLength": 20,
      "lengthChange": false,
      "searching": false,
      columns: [
        { "data": "barang" },
        { "data": "jumlah" },
        { "data": "total" },
        ],
  });

  $('body').on('click','.listmrpr',function() {
     var idbarang = $(this).attr('id');
     var idperiode = $(this).attr('idperiode');
     var html ='';
     $.ajax({

        type: 'get',
        url: '{{route("pr.raselistmrprajax")}}',
        data: {'id':idbarang,'idperiode':idperiode},
        dataType: 'json',
        success: function (data) {
           $.each(data, function(index, element) {
              html='<tr>'+
              // '<td>'+element['id']+'</td>'+
              '<td>'+element['nomr']+'</td><td>'+element['nama']+'</td><td>'+element['jumlah']+'</td>'+
              '<td><button type="button" class="btn btn-info btn-xs btnBarangMr" mr_id="'+element['id']+'"> List Barang</button></td>'+
              '</tr>'+html
           });
           $('.table-body-mrpr').html(html);
           $('#myModallistmr').modal('show');

        },

        error: function (data) {
        alert('gagal');
           console.log(data);
        }
     });
  });

  $(document).on('click','.btnBarangMr',function(event){
    id = $(this).attr('mr_id');
    $.ajax({
            url: '{{route("pr.raselistmrprbarangajax")}}',
            data: {
              "id" : id,
            }
        }).done(function(data){
          // console.log(data);
            tableListBarang.clear().draw();
            tableListBarang.rows.add(data).draw();
            $('#myModallistmr').modal('hide');
            $('#modalListBarang').modal('show');
        }).fail(function(){
          console.log('Terjadi Kesalahan');
      });
  });

  $('.checkall').on('click', function(){
      // Get all rows with search applied
      var rows = tableListBelumApprove.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('.checkall').prop('checked', this.checked);
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });


});
</script>
@endpush