@extends('crudbooster::admin_template')
@section('content')
   <p><a href="#" onclick="window.history.back(); return false;"><i class='fa fa-chevron-circle-left'></i> &nbsp; Back To List Data Barang</a></p>

   <!-- Your html goes here -->
   <div class='panel panel-default'>
      <div class='panel-heading'>
         Detail Barang
         @if(CRUDBooster::isUpdate() && $button_edit)
         <a class="btn btn-warning btn-xs pull-right" style="" href='{{ CRUDBooster::mainpath("edit/$row->id") }}'><strong>EDIT</strong></a>
         @endif
      </div>
      <div class='panel-body'>
         <table class="table table-striped table-bordered">
            <tr>
               <th width="30%">Kode Barang</th>
               <td>{{ $row->kode }}</td>
            </tr>
            <tr>
               <th>Nama</th>
               <td>{{ $row->nama }}</td>
            </tr>
            <tr>
               <th>Jenis</th>
               <td>{{ $row->jenis }}</td>
            </tr>
            <tr>
               <th>Tipe</th>
               <td>{{ $row->tipe }}</td>
            </tr>
            <tr>
               <th>Ukuran</th>
               <td>{{ $row->ukuran }}</td>
            </tr>
            <tr>
               <th>Satuan</th>
               <td>{{ $row->satuan }}</td>
            </tr>
            <tr>
               <th>Kategori</th>
               <td>{{ $row->kategori }}</td>
            </tr>
            <tr>
               <th>Min</th>
               <td>{{ $row->min }}</td>
            </tr>
            <tr>
               <th>Max</th>
               <td>{{ $row->max }}</td>
            </tr>
            <tr>
               <th>Akumulasi Stok</th>
               <td>{{ $row->akumstok }}</td>
            </tr>
            <tr>
               <th>Status</th>
               <td>{{ $row->status }}</td>
            </tr>
            <tr>
               <th>PPN</th>
               <td>{{ $row->ppn }}</td>
            </tr>
            <tr>
               <th>Harga Beli Terakhir</th>
               <td>{{ $row->harga_beli_akhir }}</td>
            </tr>
            <tr>
               <th>Harga Rata - Rata</th>
               <td>{{ $row->avgprice }}</td>
            </tr>
            <tr>
               <th>Harga Jual</th>
               <td>{{ $row->harga_jual }}</td>
            </tr>
            <tr>
               <th>Spesifikasi</th>
               <td>{{ $row->spesifikasi }}</td>
            </tr>
            <tr>
               <th>Cara Pakai</th>
               <td>{{ $row->cara_pakai }}</td>
            </tr>
            <tr>
               <th>Link Video</th>
               <td>{{ $row->video }}</td>
            </tr>
            <tr>
               <th>Gambar</th>
               <td><img src="{{ asset($row->gambar) }}" style="height:200px;"></td>
            </tr>
         </table>
      </div>
   </div>
@endsection
