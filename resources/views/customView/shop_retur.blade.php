@extends('customView.partial.shop_template')
@section('content')
<div class="container"><div class="container-fluid"><div class="row">
	<form method="post" action="{{route('shop.postReturSO')}}" id="form_1">
		{{csrf_field()}}
	<div class="col-sm-12">
	<a href="{{route('shop.getProfil')}}" class="btn btn-default">BACK</a>
	<h2 class="title text-center">Data User</h2>
	<div class="table-responsive" >
		<table class="table table-hover table-striped" id="tableListBarang" style="width: 100%">
			<thead>
				<th>No</th>
				<th>Nama Barang</th>
				<th>Jumlah</th>
				<th>Jumlah Retur</th>
				<!-- <th>No SO</th> -->
				<!-- <th>Total</th> -->
				<!-- <th>Metode Bayar</th> -->
				<!-- <th>Jatuh Tempo</th> -->
				<!-- <th>Status</th> -->
			</thead>
			<tbody>
				<input type="hidden" name="sales_order_id" value="{{$SO->id}}">
				<?php $i=1; ?>
				@foreach($SO->isi_sales_order()->get() as $key => $val)
				<tr>
					<td>{{$i}}</td>
					<td>{{$val->barang->nama}}</td>
					<td>{{$val->jumlah}}</td>
					<td>
						<input type="hidden" name="isi_sales_order_id[]" value="{{$val->id}}">
						<input type="number" placeholder="0" class="form-control" min="0" max='{{$val->jumlah}}' name="jumlah[]"/>
					</td>
				</tr>
				<?php $i++;?>
				@endforeach
			</tbody>	
		</table>
	</div>
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<label>Keterangan *:</label>
			<textarea class="form-control" required name="keterangan" rows="8" placeholder="Masukkan keterangan"></textarea>
		</div>
		<button type="submit" class="form-control btn btn-warning">Submit</button>
	</div>
	</form>
</div></div></div>
@endsection
@push('bottom')
<script type="text/javascript">
$(function() {
	$('#form_1').submit(function(e){
		e.preventDefault();
		hehe = this;
		if(checkInputFormKosongSemua()){
			swal({
				type :'info',
				html: 'Apakah ada yakin?',
				showCancelButton: true,
				confirmButtonColor: '#f0ad4e',
				cancelButtonText: 'Cancel',
			}).then(function(){
				hehe.submit();
			}).catch(swal.noop);
		}
		else{
			swal('Ooops','anda belum memasukkan jumlah barang','error').catch(swal.noop);
		};
	});

	function checkInputFormKosongSemua(){
		konter1 = 0;
		konter2 = 0;
		$('input[name="jumlah[]"]').each(function(){
			if(this.value=='')konter1++;
			konter2++;
		});
		if(konter1 == konter2) return false;
		return true;
	}
});
</script>
@endpush