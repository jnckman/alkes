<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('mronline/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('mronline/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('mronline/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../mronline/js/html5shiv.js"></script>
    <script src="../mronline/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
    <style type="text/css">
    	.mr_clickable:hover .single-products .productinfo{
    		background-color: #FE980F;
    		cursor: pointer;
    	}
    	.mr_clickable:hover .single-products .productinfo p{
    		color : #fff;
    	}
    </style>

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					@include('customView.partial.navbar')
				</div>
			</div>
		</div><!--/header-middle-->
	
		
	</header><!--/header-->
	@if(@$alerts)
        		@foreach(@$alerts as $alert)
        			<div class='callout callout-{{$alert[type]}}'>        				
        					{!! $alert['message'] !!}
        			</div>
        		@endforeach
    @endif
	
	<section>
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="{{route('subdept.mr.mainpage')}}">Material Request</a></li>
			  <li class="active">History</li>
			</ol>
			<a href="{{ route('subdept.mr.mainpage')}}" type="button" class="btn btn-default">Back</a>
			<h3>History</h3>
			<div class="form-group">
				<label>Pilih Periode MR</label>
				<select id="select2periode" class="form-control"><option></option></select>
			</div>
			<br>
			<br>
			<div class="lds-css ng-scope" style="display: none" id="wait"><div style="width:100%;height:100%" class="lds-rolling"><div></div></div></div>
			<div id="periode_MR">
			</div>
		</div>
	</section>

	
	<footer id="footer" style="position: fixed;bottom: 0;width: 100%;"><!--Footer-->
		
		<h6 style="text-align: center;background-color: none !important;">Copyright © 2017 Technubi IT Solution. All rights reserved.</h6>
		
		
	</footer><!--/Footer-->
  
    <script src="{{asset('mronline/js/jquery.js')}}"></script>
	<script src="{{asset('mronline/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('mronline/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('mronline/js/price-range.js')}}"></script>
    <script src="{{asset('mronline/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('mronline/js/main.js')}}"></script>
    <script src="{{ asset('customs/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('customs/js/select2.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	var periode = <?php echo($periode) ;?>;

	$("#select2periode").select2({
		data : periode,
		placeholder: "Pilih Periode",
	});

	$(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

	$(document).on("select2:selecting","#select2periode",function(e) {
		id = e.params.args.data.id;
		$.ajax({
            url: 'ajax_getMRs',
            data: {
            	"periode_mrs_id" : id,
            }
        }).done(function(data){
        	if(data.length==0) {
        		$("#periode_MR").html('Tidak ada data');
        	}
        	else {
        		$("#periode_MR").html('');
        	}
        	for (var i = 0; i < data.length; i++) {
        		$("#periode_MR").append(
	        		'<div class="col-sm-6 col-xs-6 mr_clickable" mr_id="'+data[i].id+'" mr_app="'+data[i].approval+'">'+
					'<div class="product-image-wrapper">'+
						'<div class="single-products">'+
							'<div class="productinfo text-center">'+
								'<p><b>No MR :</b></p>'+
								'<p>'+data[i].nomr+'</p>'+
								'<p><b>Tanggal :</b></p>'+
								'<p>'+data[i].tanggal+'</p>'+
								'<p><b>Approval :</b></p>'+
								'<p>'+data[i].approval+'</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'</div>'
        		);
        	}
        }).fail(function(){
	        console.log('Terjadi Kesalahan');
	    });
	});
	
	var last_mr_id='0';
	$(document).on("click",".mr_clickable",function(e) {
		id = $(this).attr('mr_id');
		approval = $(this).attr('mr_app');
		if(last_mr_id!=id){
			last_mr_id = id;
			$.ajax({
	            url: 'ajax_getMRs',
	            data: {
	            	"mr_subdepts_id": id
	            }
	        }).done(function(data){
	        	console.log(data);
	        	if(approval==='oke') {
	        		editth = '';
	        		edittr = '';
	        	}
	        	else {
	        		editth = '<th>Jumlah Baru</th><th>Aksi</th>';
	        		edittr = '<td><input type="number" placeholder="0"/></td><td><button type="button" class="btn btn-warning btnUpdate">update</button><button type="button" class="btn btn-danger btnHapus">delete</button></td>';
	        	}
	        	$('#barangBody').html(
	        		'<thead>'+
						'<th>No</th>'+
						'<th>Nama</th>'+
						'<th>Jumlah</th>'+
						editth+
					'</thead>'+
					'<tbody>'+
					'</tbody>'
	        		);
	        	for (var i = 0; i < data.length; i++) {
	        		$('#barangBody').append(
						'<tr isiMrId="'+data[i].id+'">'+
						'<td>'+(i+1)+'</td>'+
						'<td>'+data[i].nama+'</td>'+
						'<td class="jumlah">'+data[i].jumlah+'</td>'+
						edittr+
						'</tr>'
					);
	        	}
	        	$('#modalBarangMr').modal('show');
	        }).fail(function(){
		        console.log('Terjadi Kesalahan');
		    });
		}
		else {
			$('#modalBarangMr').modal('show');
		}
	});

	$(document).on('click','.btnUpdate',function(e) {
		id = $(this).parent().parent().attr('isiMrId');
		jumlah = $(this).parent().parent().find('input').val();
		parent = $(this).parent().parent();
		url = '{{ route("subdept.mr.historyUpdateJumlahBarang") }}';
		$.ajax({
			url:url,
			type:"patch",
			data:{
				"_token": "{{ csrf_token() }}",
				'id':id,
				'jumlah':jumlah,
			}
		}).done(function(data){
			alert('Data updated !');
			parent.find('td[class="jumlah"]').html(jumlah);
		}).fail(function(data){
			console.log('gagal update');
		})
	});

	$(document).on('click','.btnHapus',function(e) {
		id = $(this).parent().parent().attr('isiMrId');
		jumlah = $(this).parent().parent().find('input').val();
		url = '{{ route("subdept.mr.historyDelBarang") }}';
		parent = $(this).parent().parent();
		$.ajax({
			url:url,
			type:"delete",
			data:{
				"_token": "{{ csrf_token() }}",
				'id':id,
			}
		}).done(function(data){
			alert('Data deleted !');
			parent.remove();
		}).fail(function(data){
			console.log('gagal delete');
		})
	});

});
</script>
<style type="text/css">
	.selected{
		border-bottom: 2px solid grey;

	}
</style>
</body>
</html>