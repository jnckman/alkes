@extends('crudbooster::admin_template')
@section('content')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
<p><a title="Return" href="{{CRUDbooster::adminpath('purchaseorders')}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Purchase Order </a></p>
<ul class="timeline">
	@if(count($hasil)>0)
	@foreach($hasil as $k => $v)
    <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-red">
            {{date('d-M-Y',strtotime($v->tanggal_bayar))}}
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-money bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> {{ date('H:i:s',strtotime($v->created_at)) }} </span>

            <h3 class="timeline-header">COA : {{$v->coa}}</h3>

            <div class="timeline-body">
                <div class="row">
                	<div class="col-sm-3">
                		Nominal:	
                	</div>
                	<div class="col-sm-9">
                		Rp {{ number_format($v->nominal) }}
                	</div>
                </div>
                <div class="row">
                	<div class="col-sm-3">
                		Status:	
                	</div>
                	<div class="col-sm-9">
                		{{ ($v->status) }}
                	</div>
                </div>
                <div class="row">
                	<div class="col-sm-3">
                		Keterangan:
                	</div>
                	<div class="col-sm-9">
                		<p >{{$v->keterangan}}</p>
                	</div>
                </div>
            </div>

            <!-- <div class="timeline-footer">
                <a class="btn btn-primary btn-xs">...</a>
            </div> -->
        </div>
    </li>
    <!-- END timeline item -->
    @endforeach
    @else
    <div class="box box-danger">
    	<div class="box-header">
    		
    	</div>
    	<div class="box-content">
    		<h3 class="text-center">Belum ada Data</h3>
    	</div>
    	<div class="box-footer">
    		
    	</div>
    </div>
    @endif
</ul>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush