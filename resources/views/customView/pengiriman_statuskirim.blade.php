@extends('crudbooster::admin_template')
@section('header22')
   <i class="fa fa-bar-chart" aria-hidden="true"></i> Laporan Pengeluaran Barang
@endsection
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan Pengeluaran Barang</li>
@endsection
@section('content')
<div class="container-fluid">
<div class="row" >
   <div class="col-md-12">
      <div class="box box-info"">
         <div class="box-header with-border">
            <p></p>
         </div>
         <div class="box-body no-padding">
            <ul class="nav nav-tabs nav-justified">
                  <li class="active"><a data-toggle="tab" href="#tab_min">MR</a></li>
                  <li ><a data-toggle="tab" href="#tab_max">SO</a></li>
            </ul>
            <div class="tab-content">
               <div id="tab_min" class="tab-pane active">
                  <div class="table-responsive">
                     <table id='table1' class="table table-bordered table-striped">
                        <thead>
                           <tr>
                           	<th>ID</th>
                            <th>Nomor</th>
                            <th>Tgl Pembuatan</th>
                            <th>Area</th>
                            <th>Pembuat</th>
                            <th>Progress Kirim(%)</th>
                            <th>Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
               <div id="tab_max" class="tab-pane">
                  <div class="table-responsive">
                     <table id='table2' class="table table-bordered table-striped" style="width: 100%">
                        <thead>
                           <tr>
                            <th>ID</th>
                            <th>Nomor</th>
                            <th>Tgl Pembuatan</th>
                            <th>Customer</th>
                            <th>Pembuat</th>
                            <th>Progress Kirim</th>
                            <th>Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
   </div>
</div>
</div>

<div class="modal" id="modalProgress" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Pengiriman</h4>
         </div>
         <div class="modal-body" id="table_dashboard">
            <table class="table table-bordered" id="table3" style="width:100%;">
               <thead>
                 <tr>
                   <th>ID</th>
                   <th>Kode</th>
                   <th>Tanggal Kirim</th>
                   <th>Pembuat</th>
                   <th>Status</th>
                   <th>Action</th>
                 </tr>
               </thead>
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	
	var tablemin = $('#table1').DataTable({ // MR
      'processing': true,
      'serverside':true,
      "order": [],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      'ajax' : {
         url : '{{route("pengiriman.status_kirim.MR")}}',
      }
   });

   var tablemax = $('#table2').DataTable({ //SO
      'processing': true,
      'serverside':true,
      "order": [],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      'ajax' : {
         url : '{{route("pengiriman.status_kirim.SO")}}',
      }
   });

   var table3 = $('#table3').DataTable({ //SO
      'processing': true,
      'serverside':true,
      "searching": false,
      "order": [],
      "lengthMenu": [[25, 50, 100, 250, 500], [25, 50, 100, 250, 500]],
      data :[],
   });

   $(document).on('click','.btn_detail_mr',function(){
   		id = tablemin.rows($(this).parent().parent()).data()[0][0];
   		$.ajax({
            url: '{{route("pengiriman.status_kirim.listpengiriman")}}',
            data: {
            	"id" : id,
            	"table" : 'mr',
            }
        }).done(function(data){
        	console.log(data);
        	if(data.length==0) {
        		table3.clear().draw();
        		$('#modalProgress').modal();
        	}
        	else {
				table3.clear().draw();
				table3.rows.add(data).draw();
				$('#modalProgress').modal();
        	}
        }).fail(function(){
	        console.log('Terjadi Kesalahan');
	    });
   });

   $(document).on('click','.btn_detail_so',function(){
   		id = tablemax.rows($(this).parent().parent()).data()[0][0];
   		$.ajax({
            url: '{{route("pengiriman.status_kirim.listpengiriman")}}',
            data: {
            	"id" : id,
            	"table" : 'so',
            }
        }).done(function(data){
        	// console.log(data);
        	if(data.length==0) {
        		table3.clear().draw();
        		$('#modalProgress').modal();
        	}
        	else {
				table3.clear().draw();
				table3.rows.add(data).draw();
				$('#modalProgress').modal();
        	}
        }).fail(function(){
	        console.log('Terjadi Kesalahan');
	    });
   });

	/*nomorurut();

	function nomorurut(){
      table.on( 'order.dt search.dt', function () {
           table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = i+1;
           } );
       } ).draw();
   	}*/
});
</script>
@endpush