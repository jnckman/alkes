@extends('crudbooster::admin_template')
@push('head')
<script src="{{ asset('customs/js/pace.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link href="{{ asset('customs/css/pace-theme-minimal.css')}}" rel="stylesheet" />
@endpush
@section('content')
<div tabindex="-1">
   <form id="form2" method="post" action="{{ route('stokopnam.postadd')}}">
      {{csrf_field()}}
      <input type="hidden" name="input" id='inputku'/>
   </form>
</div>
<div class="box box-default">
	<form id="form1">
    <div class="box-header with-border">

	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table id='table1' class="table table-bordered table-hover">
				<thead>
					<th>ID</th>
					<th>Kode Barang</th>
					<th>Nama Barang</th>
					<th>Gudang</th>
					<th>Akumstok</th>
					<th>Stok Masuk</th>
					<th>Stok Keluar</th>
					<th>Stok Baru</th>
				</thead>
				<tfoot>

				</tfoot>
			</table>
		</div>
	</div>
	<div class="box-footer">
		<button type="submit" id="btnsubmit" class="btn btn-success btn-sm" disabled="true" style="width: 100%">Submit</button>
	</div>
	</form>
</div>
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	// alert('etogege');
	var table = $('#table1').DataTable({
		fixedHeader: {
            header: true,
            footer: true
        },
        'buttons': [
	        'copy', 'excel', 'pdf'
	    ],
	    'processing': true,
	    'serverside':true,
	    "order": [],
	    "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 7
            }
        ],
	    "lengthMenu": [[25, 50, 100, 250, -1], [25, 50, 100, 250, 'All']],
	    'ajax' : {
        		url : '{{route("stokopnam.getadddt")}}',
        		},
		"initComplete": function(settings, json){ 
			    if ( table.data().count() ) {
				    // alert( 'Empty table' );
				    $('#btnsubmit').attr('disabled',false)
				}
			 }
	});

	$(document).on('submit','#form1',function(event){
		event.preventDefault();
		// alert('oke');
		$('#inputku').val(table.$('input').serialize());
		// $('#inputku').val($('#form1').serialize());
	    $('#form2').submit();
	});

});
</script>
@endpush