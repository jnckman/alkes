<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
		@page { size: auto;  margin: 0mm; }
	</style>
</head>
<body>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3><b>PT. TATA KARYA GEMILANG</b></h3>
					<h5>Jl. Sukun MBS no 3 Condong Catur Yogyakarta</h5>
					<h5>P : 0274 88 99 81 e: purchasing@gemilang.co.id</h5>
					<h5>NPWP 02.398.317.4.542.000</h5>
					<h5>Tanggal : {{date('d:m:Y')}}</h5>
					<h5>No : 01/TTB-JOGJA/XII/2017</h5>

				</div>
				@foreach($tes as $po)

				<div class="col-md-12">
					<div class="col-md-2">
						<div class="row">
						<h3>Nama MR : </h3>
						</div>
						<div class="row">
							{{$po['nomr']}}
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<h3>Area</h3>
						</div>
						<div class="row">
							{{$po->project->area}}
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							
						</div>
						<div class="row">
							
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table-bordered table">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Satuan</th>
								
								<th>QTY</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $ct = 0; ?>
							@foreach($po->isi_mr as $cc)

							<tr>
								<td>{{$ct++}}</td>
								<td>{{$cc->barang->kode}}</td>
								<td>{{$cc->barang->nama}}</td>
								<td>{{$cc->barang->satuan->nama}}</td>
								<td>{{$cc->jumlah}}</td>
							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
				@endforeach
				<div class="col-md-12">
					<table class="table-bordered table">
						<tr class="text-center">
							<td>Logistik & Adm</td>
							<td>Penerima</td>
							
						</tr>
						<tr class="text-center" style="height: 100px;">
							<td style="vertical-align:bottom"></td>
							<td style="vertical-align:bottom"></td>
							
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
</body>

<script type="text/javascript">
$(function(){
	function printmz(){
		window.print();
		// window.location = "{{url('admin/notas')}}";
	}

	printmz();
});
</script>
</html>