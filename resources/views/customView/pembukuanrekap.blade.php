@extends('crudbooster::admin_template')

@push('head')
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset ('customs/css/bootstrap3.css') }}">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
</style>
@endpush

@section('header22')
   <i class="fa fa-cart-plus" aria-hidden="true"></i> Purchase Request
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li class="active">Purchase Request</li>
@endsection

@section('content')
@if (session('status_delete'))
   <div class="alert alert-success">Delete Berhasil !!!</div>
   {{ session()->forget('status_delete') }}
@elseif (session('status_addprice'))
   <div class="alert alert-success">Pricelist Berhasil di tambah !!!</div>
   {{ session()->forget('status_addprice') }}
@elseif (session('change_pricelist'))
   <div class="alert alert-success">Pricelist Berhasil di ganti !!!</div>
   {{ session()->forget('change_pricelist') }}
@endif
<div class="container-fluid">
   <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
              <div class="col-md-4" style="padding: 0px;border-top-left-radius: 14px;border-bottom-left-radius: 14px;height: 40px;border-left: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:white;font-weight:bolder;background-color: #5fa6ff;height: 100%;border-top-left-radius: 10px;border-bottom-left-radius: 10px;" >Memilih Periode MR</span>
              </div>
              <div class="col-md-4" style="padding: 0px;height: 40px;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:white;background-color: #5fa6ff;height: 100%;font-weight:bolder;" >Lakukan Rekap</span>
              </div>
              <div class="col-md-4" style="padding: 0px;border-top-right-radius: 14px;border-bottom-right-radius: 14px;height: 40px;border-right: 10px solid #545454;border-bottom: 8px solid #545454;border-top: 8px solid #545454;background-color: #545454;">
                <span class="col-md-12" style="color:grey;background-color: white;height: 100%;border-top-right-radius: 10px;border-bottom-right-radius: 10px;" >Purchase Order</span>
              </div>
            </div>
            <div class="col-md-8" style="padding: 0px;">
               <div class="col-md-12" >
                  <h5><i class="fa fa-book fa-fw" aria-hidden="true"></i> {{$result2[0]->judul}} | <i class="fa fa-calendar-o fa-fw" aria-hidden="true"></i> : {{$result2[0]->tglmulai}} - {{$result2[0]->tglselesai}} |  <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result2[0]->catatan}} </h5>
               </div>
               <div class="col-md-12">
                  <h5><b>Harga</b> : Berfungsi melakukan set harga supplier ke barang</h5>
                  <h5><b>List MR</b> : Melihat MR yang menggunakan barang tersebut</h5>
                  <h5><b>Tambah Barang</b> : Menambah barang baru ke PR</h5>

               </div>
            </div>
            <div class="col-md-4">
           <!-- <div class="col-md-2">
             <a href="../ajukanatasan/{{$id}}"><button class="btn btn-warning">Notifikasi ke Atasan</button></a>
           </div> -->
              <!-- <div class="col-md-6" style="margin-top: 10px;">
                 <button style="width: 100%;" class="btn btn-warning tambahbarangbaru btn-sm">Tambah Barang PO</button>
              </div> -->
              <div class="col-md-6" style="margin-top: 10px;">
                 <button  style="width: 100%;" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalAddPriceList">Add Pricelist</button>
              </div>
              <!-- <div class="col-md-6" style="margin-top: 10px;">
                 <a href="../log/{{$id}}"><button  style="width: 100%;" class="btn btn-success btn-sm">LOG</button></a>
              </div> -->
              <div class="col-md-6" style="margin-top: 10px;">
                 <a href="../../susunpurchaseorder/{{$id}}/{{$tgl}}" class="linksusunpo"><button style="width: 100%;" class="btn btn-success btn-sm">Susun P.O</button></a>
              </div>

            </div>
         </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-hover table-striped table-bordered display select table1" cellspacing="0" id='table_dashboard'>
                  <thead>
                     <tr>
                       
                       <th>ID</th>
                       <th width="2%"><input type="checkbox" name="select_all" value="1" id="checkall"></th>
                       <th>Barang</th>
                       <th>Kode</th>
                       <th>MR</th>
                       <th>Min/Max</th>
                       <th>Stok</th>
                       <th>PR</th>
                       <th>Sat</th>
                       <th style="width: 7%;">PO Request</th>
                       <th>Selected</th>
                       <th>Supplier * PR</th>
                       <th>Action</th>
                       
                       
                     </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                     <tr>
                        <th colspan="9" style="text-align:right">Total:</th>
                        <th></th>
                        <th></th>
                        
                        
                     </tr>
                     
                  </tfoot>
                  <tr>
                       <th colspan="13"><button id="save_pr" style="width: 100%;" class="btn btn-info btn-xs">Approve All PR</button><button id="unsave_pr" style="width: 100%;" class="btn btn-warning btn-sm">Undo Approve</button></th>
                     </tr>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Pilih Harga</h4>
         </div>

         <div class="modal-body" style="height: 100px;">
            

            <input type="hidden" name="" id='supplierid' value=''>

            <div class="col-md-12" style="margin-top: 10px;">
            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
               <div class=" error">
                  <select class="form-control" id='pilih-supplier'>

                  </select>
               </div>
            </form>
            </div>
         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-save" value="add">Simpan Harga</button>
            <input type="hidden" id="task_id" name="task_id" value="0">
         </div>
      </div>
   </div>

   <form action="{{ route('add.log.pricelist.pr') }}" id="form_log_pricelist_pr">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" id="rekap_id" name="rekap_id" value="">
      <input type="hidden" id="act" name="act" value="">
   </form>
</div>

<div class="modal fade" id="myModallistmr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Daftar MR</h4>
            <!-- <h5><B>Perhatikan !! Setiap Perubahan jumlah akan mengganti MR bersangkutan</B></h5> -->
         </div>
         <div class="modal-body" >
            <table class="table table-bordered" id="table_dashboard">
               <thead>
                 <tr>
                   <td>Id</td>
                   <td>No MR</td>
                   <td>Project </td>
                   <td>Barang di Request</td>
                   <!-- <td>Reset Jumlah</td> -->
                 </tr>
               </thead>
               <tbody class="table-body-mrpr">
               </tbody>
            </table>
         </div>
         <div class="modal-footer">
            <!-- <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button> -->
            <input type="hidden" id="task_id" name="task_id" value="0">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="myModaladdbarangbaru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
      <form action="../tambahbarangpricelist" method='post'>
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
             <h4 class="modal-title" id="myModalLabel">Daftar MR</h4>
         </div>
         <div class="modal-body" >

             {{csrf_field()}}
               <div class="form-group">
                 <label>Nama Barang</label>
                 <select class="form-control select2-hidden-accessible" id="select2barangs" required name="barangs_id">
                 <option></option>
                 </select>
               </div>
               <div class="form-group">
                 <label>Jumlah</label>
                 <input type= "text" name="jumlah" class="form-control" placeholder="jumlah barang diminta">
               </div>
               <div class="form-group">
                 <label>Supplier</label>
                 <select class="form-control select2-hidden-accessible" id="select2suppliers" required name="suppliers_id">
                 <option></option>
                 </select>
               </div>
               <div class="form-group">
                 <label>Harga</label>
                 <input type= "text" name="harga" class="form-control" placeholder="harga supplier">
               </div>
               <input type= "hidden" name="periode_mrs_id" class="form-control" value='{{$id}}'>

         </div>
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btn-save-barang" value="add">Save changes</button>
             <input type="hidden" id="task_id" name="task_id" value="0">
         </div>
      </form>
     </div>
   </div>
</div>

<div id="modalAddPriceList" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <form id="form_addPricelist" method="post" action="{{ route('add.pricelist.barang.3') }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" id="nama_supplier" name="nama_supplier" value="">
   <!-- Modal content-->
     <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Pricelist Barang</h4>
        </div>
        <div class="modal-body">
           <!-- input untuk FORM -->
            <div class="form-horizontal">
               <div class="form-group">
                  <label class="control-label col-sm-3" for="kode">Barang</label>
                  <div class="col-md-8">
                     <select class="form-control selectpicker" name="id_barang" data-live-search="true" title="Pilih Barang" required>
                      @foreach($barang2 as $b)
                      <option value="{{ $b->id }}">{{ $b->nama }}</option>
                      @endforeach
                     </select>
                 </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-3" for="kode">Supplier</label>
                  <div class="col-md-8">
                     <select class="form-control selectpicker" name="supplier" id="select_supplier" data-live-search="true" title="Pilih Supplier" required>
                      @foreach($supplier2 as $sup)
                      <option value="{{ $sup->id }}">{{ $sup->nama }}</option>
                      @endforeach
                     </select>
                 </div>
              </div>
              <div class="form-group">
                 <label class="control-label col-sm-3" for="harga.p">Harga</label>
                 <div class="col-sm-8"><input type="number" class="form-control" id="harga.p" name="harga.p" required></div>
              </div>
              <div>

              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button class="btn btn-success simpanharga">Submit</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
     </div>
  </form>
  </div>
</div>


<div id="modalchangeprice" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <form id="form_addPricelist" method="post" action="{{ route('add.pricelist.barang.2') }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" id="nama_supplier" name="nama_supplier" value="">
   <!-- Modal content-->
     <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Pricelist Barang</h4>
        </div>
        <div class="modal-body">
           <!-- input untuk FORM -->
            <div class="form-horizontal">
               <div class="form-group">
                  <label class="control-label col-sm-3" for="kode">Barang</label>
                  <div class="col-md-8">
                     <select class="form-control selectpicker" name="id_barang" data-live-search="true" title="Pilih Barang" required>
                      @foreach($barang2 as $b)
                      <option value="{{ $b->id }}">{{ $b->nama }}</option>
                      @endforeach
                     </select>
                 </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-3" for="kode">Supplier</label>
                  <div class="col-md-8">
                     <select class="form-control selectpicker" name="supplier" id="select_supplier" data-live-search="true" title="Pilih Supplier" required>
                      @foreach($supplier2 as $sup)
                      <option value="{{ $sup->id }}">{{ $sup->nama }}</option>
                      @endforeach
                     </select>
                 </div>
              </div>
              <div class="form-group">
                 <label class="control-label col-sm-3" for="harga.p">Harga</label>
                 <div class="col-sm-8"><input type="number" class="form-control" id="harga.p" name="harga.p" required></div>
              </div>
              <div>

              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button type="submit" class="btn btn-success">Submit</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
     </div>
  </form>
  </div>
</div>
<form id="save_approve_pr" method="post" action="{{ route('approve.pr') }}">
   {{ csrf_field() }}
   <input type="hidden" name="id" value="{{ $id }}">

</form>
<form id="unsave_approve_pr" method="post" action="{{ route('unapprove.pr') }}">
   {{ csrf_field() }}
   <input type="hidden" name="id" value="{{ $id }}">

</form>

@endsection

@push('bottom')

<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
   // Array holding selected row IDs
   var rows_selected = [];

   $("#select_supplier").change(function() {
      var nama = $('#select_supplier option:selected').text();
      $("#nama_supplier").val(nama);
   });

   $(document).ready(function() {

      var total = 0;
      var temp = 0;
      var totalnow = 0;
      var id ;
      var rekapid;
      var supplierid = 0;
      var kdata;
      var hasiljson;
      var index;
      var approved;

      var datatable = $('.table1').DataTable( {
         'processing': true,
         'serverside':true,
         'ajax': '../../lihatpembukuanrekapdtajax/{{$id}}/{{$tgl}}',
         "bPaginate": false,
         'initComplete':function(data){
          
            dttotal = data.json.total;
            approved = data.json.approvedstatus;
            hasiljson = data.json.data;

            if(approved == 0){
                        //$('#save_pr').hide();
                        $('#unsave_pr').show();
                        //$('.linksusunpo').hide();
                      }
            else{

                        $('#unsave_pr').show();
                        $('.linksusunpo').show();
            }
            
         },
         "footerCallback": function ( row, data, start, end, display ) {
                 var api = this.api(), data;

                 // Remove the formatting to get integer data for summation
                 var intVal = function ( i ) {
                  i = i.toString().replace(',','')*1;

                     return typeof i === 'string' ?
                         i.replace('/[\$,]/g', '')*1 :
                         typeof i === 'number' ?
                             i : 0;
                 };

                 // Total over all pages
                 dttotal = api
                     .column( 11 )
                     .data()
                     .reduce( function (a, b) {
                         return intVal(a) + intVal(b);
                     }, 0 );

                 // Total over this page
                 pageTotal = api
                     .column( 11, { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                         return intVal(a) + intVal(b);
                     }, 0 );

                 // Update footer
                 $( api.column( 11 ).footer() ).html(
                      'Total : '+dttotal.toLocaleString("en") +',-'
                 );
         },
         'columnDefs': [
            {
               'searchable': false,
               'orderable': false,
               'className': "text-center",
               "targets": [ 9 ]
            },
            {
               'targets':   10,
               'searchable': false,
               'orderable': false,
               'className': 'text-center'
            }
         ]
      });


      

          //set harga
      $('body').on('click', '.isiharga', function (){
         totalnow = total;

         $('.totalpl').html(totalnow);

         id = $(this).attr('id');
         index = $(this).attr('urutan');

         rekapid = $(this).attr('idrekap');

         //NOTE utk log pricelist
         $('#rekap_id').val(rekapid);

         if($('#pilihanharga'+rekapid).text() == "belum dipilih"){
            $('#act').val("First Pricelist");
         }
         else {
            $('#act').val("Change Pricelist");
         }

         $.ajax({

            type: 'get',
            url: '../../ambilsupplier/'+id,
            data: '',
            dataType: 'json',
            success: function (data) {

               var option = '';
               for (var i = data.length - 1; i >= 0; i--) {
                  option = '<option value='+(data[i].harga * hasiljson[index][3])+' supplierid="'+data[i].id+'">'+data[i].nama+' - '+data[i].harga+'</option>' + option;
               }

               $('#pilih-supplier').html('<option>Silahkan Pilih</option>'+option)

               $('#frmTasks').trigger("reset");

            },

            error: function (data) {
               console.log('Error:', data);
            }
         });
         $('#myModal').modal('show');
      });

      $('body').on('click','.listmrpr',function() {
         var idbarang = $(this).attr('id');
         var html ='';
         $.ajax({

            type: 'post',
            url: '../../listmrprajax',
            data: {'id':idbarang,'idperiode':<?php echo $id; ?>},
            dataType: 'json',
            success: function (data) {
                //'<td><input type="text" placeholder="jumlah baru" class="form-control jumlahbaru'+element['id']+'" ></td><td></input><button class="btn btn-success gantijumlah" id="'+element['id']+'">ganti</button></td>'
               $.each(data, function(index, element) {
                  html='<tr><td>'+element['id']+'</td><td>'+element['nomr']+' - '+element['area']+'</td><td>'+element['nama']+'</td><td>'+element['jumlah']+'</td>'+'</tr>'+html
               });
               $('.table-body-mrpr').html(html);
               $('#myModallistmr').modal('show');

            },

            error: function (data) {
            alert('gagal');
               console.log(data);
            }
         });
      });

      $('body').on('click','.gantijumlah',function(){
         var idmrsubdept = $(this).attr('id');
         var jumlah = $('.jumlahbaru'+idmrsubdept).val();
         var html ='';

         $.ajax({

            type: 'post',
            url: '../../gantijumlahisimrsubdept',
            data: {'id':idmrsubdept,'jumlahbaru':jumlah,'idperiode':<?php echo $id; ?>},
            dataType: 'json',
            success: function (data) {
                $('.table1').DataTable().ajax.reload();
                alert('data berhasil di rubah')
                $('#myModallistmr').modal('hide');

            },
            error: function (data) {
              alert('gagal');
                console.log(data);
            }
         });
      });

      $('body').on('click','.simpanharga',function(){
        $("#form_addPricelist").submit(function(){
          return false;
        });
         var dataform = $('#form_addPricelist').serialize();

         $.ajax({

            type: 'post',
            url: "{{ route('add.pricelist.barang.3') }}",
            data: dataform,
            dataType: 'json',
            success: function (data) {
               alert('berhasil');
               $('#modalAddPriceList').modal('hide');

            },
            error: function (data) {
              alert('gagal');
                console.log(data);
            }
         });
      });
          //simpan setelah ganti harga
      $('body').on('click','#btn-save',function(){
        
         kdata = [];
         kdata.push(rekapid);
         kdata.push(supplierid);
         
         $.ajax({

            type: 'post',
            url: '../../simpansupplier',
            data: {'id':rekapid,'sup':supplierid,'pobaru':$('.isijumlahpo'+hasiljson[index][0]).val()},
            dataType: 'json',
            success: function (data) {
               if(data[0]!=''){
               // alert(index); ok

               hasiljson[index][10] = data[0];
               var value = $('.isijumlahpo'+hasiljson[index][0]).val();
               hasiljson[index][11] = addCommas(value * data[1]);
               hasiljson[index][9] = '<input type="text" name="pobaru[]" value="'+value+'" style="width:100%" class="isijumlahpo isijumlahpo'+hasiljson[index][0]+'"></input>';

               datatable.clear().draw();
               datatable.rows.add(hasiljson); // Add new data
               datatable.columns.adjust().draw(); // Redraw the DataTable
               //$('#pilihanharga'+rekapid).html('');
               //$('#pilihanharga'+rekapid).html(data[0]);

               //$('#form_log_pricelist_pr').submit();

               $('#myModal').modal('hide');

               }
            },
            error: function (data) {
               alert('gagal');
                  console.log(data);
            }
         });
      })
      $('body').on('change', '.isijumlahpo', function(event) {
        index = $(this).attr('urutan');
        var value = $('.isijumlahpo'+hasiljson[index][0]).val();
        if($('.isijumlahpo'+hasiljson[index][0]).attr('defaultvalue') > value)
        {
            alert('jumlah po baru tidak bisa kurang dari PR, karena PR dibuat dari MR');
            datatable.clear().draw();
            datatable.rows.add(hasiljson); // Add new data
            datatable.columns.adjust().draw(); // Redraw the DataTable
        }
        else{
          hasiljson[index][11] = addCommas($(this).val() * ((1*hasiljson[index][11].replace(/,/g, ''))/hasiljson[index][7]));
          hasiljson[index][9] = '<input type="text" name="pobaru[]" value="'+value+'" style="width:100%" class="isijumlahpo isijumlahpo'+hasiljson[index][0]+'"></input>';
          rekapid = $(this).attr('idrekap');
          $.ajax({

             type: 'post',
             url: '../../simpanjumlah',
             data: {'id':rekapid,'pobaru':$('.isijumlahpo'+hasiljson[index][0]).val()},
             dataType: 'json',
             success: function (data) {
              alert('berhasil');
                if(data[0]!=''){
                // alert(index); ok
                datatable.clear().draw();
                datatable.rows.add(hasiljson); // Add new data
                datatable.columns.adjust().draw(); // Redraw the DataTable
                //$('#pilihanharga'+rekapid).html('');
                //$('#pilihanharga'+rekapid).html(data[0]);

                //$('#form_log_pricelist_pr').submit();

                $('#myModal').modal('hide');

                }
             },
             error: function (data) {
                alert('gagal');
                   console.log(data);
             }
          });
          
        }
        /* Act on the event */
      });

      $('.tambahbarangbaru').click(function(event) {
         $('#myModaladdbarangbaru').modal('show');
      });

      $('body').on('change', '#pilih-supplier', function (){

         temp = this.value;
         totalnow = total - this.value;
         supplierid = $(this).find(':selected').attr('supplierid');

         $('.totalsk').html(totalnow);
      });

      function addCommas(nStr) {
         nStr += '';
         x = nStr.split('.');
         x1 = x[0];
         x2 = x.length > 1 ? '.' + x[1] : '';
         var rgx = /(\d+)(\d{3})/;
         while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
         }
         return x1 + x2;
      }

      //NOTE utk checkAll datatable
      $('#checkall').on('click', function(){
         // console.log("checkall");
         // Get all rows with search applied
         var rows = datatable.rows({ 'search': 'applied' }).nodes();
         // Check/uncheck checkboxes for all rows in the table
         $('input[type="checkbox"]', rows).prop('checked', this.checked);

      });

      $('#save_pr').on('click', function() {
         checkboxes = $(".centangbox");
         

         for (var i = 0; i < checkboxes.length; i++) {
            rekap_id = $(".box"+i).attr('rekap_id');
            //alert(hasiljson[i][11]);
            if(hasiljson[i][10]==0){

              alert('maaf ada harga supplier 0 ');
              hasiljson[i][1] ='<b style="background-color:red;">'+hasiljson[i][1]+'</b>';
              datatable.clear().draw();
               datatable.rows.add(hasiljson); // Add new data
               datatable.columns.adjust().draw(); // Redraw the DataTable
              break;
            }
            else{
              if ($(".box"+i).prop('checked') == true){
                 // console.log("checked");
                 $('<input>').attr({
                    type: 'hidden',
                    name: rekap_id,
                    value: 1
                 }).appendTo('#save_approve_pr');
                 
              }
              else {
                 // console.log("belum checked");
                 $('<input>').attr({
                    type: 'hidden',
                    name: rekap_id,
                    value: 0
                 }).appendTo('#save_approve_pr');
                 
              }
              $('#save_approve_pr').submit();
           }
           
          }
      });
      $('#unsave_pr').on('click', function() {
        checkboxes = $(".centangbox");
        for (var i = 0; i < checkboxes.length; i++) {
            rekap_id = $(".box"+i).attr('rekap_id');
            if ($(".box"+i).prop('checked') == true){
               // console.log("checked");
               $('<input>').attr({
                  type: 'hidden',
                  name: rekap_id,
                  value: 1
               }).appendTo('#unsave_approve_pr');
               
            }
            else {
               // console.log("belum checked");
               $('<input>').attr({
                  type: 'hidden',
                  name: rekap_id,
                  value: 0
               }).appendTo('#unsave_approve_pr');
               
            }

         }
         $('#unsave_approve_pr').submit();
      });

      $('.sync').on('click', function() {
          alert('maaf fitur sinkronsisasi belum aktif');
      });
   });

   jQuery(document).ready(function($) {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};
      var barang = <?php echo $barang; ?>;

      $('#select2barangs').select2({
         data:barang,
         placeholder:'Pilih Barang'
      });

      var supplier = <?php echo $supplier; ?>;
      $('#select2suppliers').select2({
         data:supplier,
         placeholder:'Pilih Supplier'
      });
   });

</script>

   <link rel="shortcut icon" href="images/ico/favicon.ico">
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
   <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
   <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
   <style type="text/css">
   .selected{
      border-bottom: 2px solid grey;
   }
   .select2{
      width: 100% !important;
   }
   </style>
@endpush
