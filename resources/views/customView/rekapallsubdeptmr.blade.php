@extends('crudbooster::admin_template')

@section('content')
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->pic}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <a href="../materialrequestsystem/{{$id}}"><button class="btn btn-info">Kembali</button></a>
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered tablerekapall" id='table_dashboard'>
           <thead>
                 <tr>

                     <th>nama barang</th>
                         <th>Kode barang</th>
                         <th>Jumlah barang</th>
                         <th>Akumulasi stok gudang</th>
                         <th>Satuan stok</th>
                         <th>Harga average</th>
                         <th>Harga average * jumlah</th>
                 </tr>
          </thead>
          <tfoot>
              <tr>
                          <th colspan="6" style="text-align:right">Total:</th>
                          <th></th>
              </tr>


          </tfoot>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>


@endsection

@section('datatable')
<script type="text/javascript">
  $(document).ready(function() {

          $('.tablerekapall').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../../rekapallsubdeptmrdtajax/{{$id}}/{{$id_periode}}',
                "footerCallback": function ( row, data, start, end, display ) {
                  var api = this.api(), data;

                  // Remove the formatting to get integer data for summation
                  var intVal = function ( i ) {
                      return typeof i === 'string' ?
                          i.replace(/[\$,]/g, '')*1 :
                          typeof i === 'number' ?
                              i : 0;
                  };

                  // Total over all pages
                  total = api
                      .column( 6 )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                  // Total over this page
                  pageTotal = api
                      .column( 6, { page: 'current'} )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                  // Update footer
                  $( api.column( 6 ).footer() ).html(
                       'Total : '+total.toLocaleString("en") +',-'
                  );
              },

          } );


      } );
</script>
@endsection
