@extends('customView.partial.shop_template')
@section('content')
<div class="container">
<div class="row">
	<h2 class="title text-center">Hasil Pencarian</h2>
	<div class="col-md-12 col-xs-12 col-sm-12">
		<?php $hitung=1; ?>
		@if(!empty($hasils) && count($hasils)>0)
		@foreach($hasils as $key => $val)
		<div class="col-xs-6 col-md-2">
			<div class="product-image-wrapper">
				<div class="single-products">
						<div class="prouctinfo text-center">
							<h5 class="namabarang">{{$val->nama}}</h5>
							<img src="{{asset($val->gambar)}}" alt="GambarBarang" style="max-height: 150px;">
<!-- =================================================================================================================== -->
@if(isset($data['customer']))
<?php
$harga_jual = $val->harga_jual()->where('ket',$data['customer']->jenis_harga_jual)->latest()->first()->harga_jual; 
if(!isset($harga_jual)) $harga_jual = 0;
?>
<h4>Rp {{number_format($harga_jual)}}</h4>
<a class="btn btn-default addCart add-to-cart" barang_id="{{$val->id}}" satuan="{{$val->satuan->nama}}" harga_jual="{{$harga_jual}}" style="width: 88%"><i class="fa fa-shopping-cart">
</i>Add to cart</a>
@endif
<!-- =================================================================================================================== -->
							<a class="btn btn-default detailBarang add-to-cart" barang_id="{{$val->id}}" style="width: 88%"><i class="fa fa-eye">
							</i>Detail</a>
						</div>
				</div>
			</div>
		</div>
		@if($hitung % 2==0)
		<div class="clearfix visible-xs"></div>
		@endif
		@if($hitung % 6==0)
		<div class="clearfix visible-md visible-lg"></div>
		@endif
		<?php $hitung++; ?>
		@endforeach
		<div class="col-xs-12 col-md-12">
			<div class="text-center">
				{{ $hasils->links() }}
			</div>
		</div>
		@else
		<h5 style="text-align: center;">Kata kunci tidak ditemukan</h5>
		@endif
	</div>
<div class="modal fade" tabindex="-1" id="modalDetailBarang" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> </div> <div class="modal-body"> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div> 
@push('botton')
<script type="text/javascript">
$(function(){

});
</script>
@endpush
@endsection