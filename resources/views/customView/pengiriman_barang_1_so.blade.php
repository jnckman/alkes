@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-truck" aria-hidden="true"></i> Pengiriman Barang
@endsection

@push('head')
<link href="{{ asset('customs/css/step-wizard.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/bootstrap-select.min.css') }}" rel="stylesheet">
<link href="{{ asset ('customs/css/custom-container.css') }}" rel="stylesheet">
<style>
   .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
     width:100%;
   }
   .bootstrap-select.btn-group .dropdown-toggle .filter-option {
      color: white;
   }
</style>
@endpush

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Pengiriman Barang</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="stepwizard">
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-paper-plane" aria-hidden="true"></i> Jenis Pengiriman</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-success btn-lg"><i class="fa fa-crosshairs" aria-hidden="true"></i> List MR/SO</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Ambil Barang</a>
            </div>
            <div class="stepwizard-step">
               <a href="" type="button" class="btn btn-danger btn-lg disabled"><i class="fa fa-plus" aria-hidden="true"></i> Bagi Barang</a>
            </div>
         </div>
     </div>
  </div>
</div>


   <div class="panel panel-default">
      <div class="panel-body padding-lr0">

         <form id="form_1" method="post" action="{{ route('action.pengiriman.barang', 1)}}">
            <input type="hidden" name="jenis_pengiriman" value="{{session('jenis_pengiriman')}}">
            <input type="hidden" name="jenis[]" value="{{ $jenis[0] }}">
            <input type="hidden" name="jenis[]" value="{{ $jenis[1] }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-5">
                 <div class="col-md-4">
                    <label for="pilih_gudang">Pilih Gudang</label>
                    <select required id="pilih_gudang" name="gudangs_id" class="selectpicker show-menu-arrow" data-style="btn-primary" title="....." data-live-search="true">
                       @foreach($gudang->where('stok_gudang', '!=', 0) as $g)
                       <option value="{{ $g->id }}">{{ $g->nama }} - {{$g->id}}</option>
                       @endforeach
                    </select>
                 </div>
                 
                 <div class="col-md-4">
                    <label for="pilih_mr">Pilih
                       @if($jenis[0] == 1) MR
                       @else SO
                       @endif
                    </label>
                    <select required id="pilih_mr" class="selectpicker show-menu-arrow" data-style="btn-primary" title="....." data-live-search="true">
                       
                          @foreach($data as $d)
                          @if($d->persen < 100)
                            <option value="{{ $d->id }}" data-tanggal="{{ $d->created_at }}" data-nomr="{{ $d->nomor }}">{{ $d->nomor }} / {{ $d->persen }} % / {{ $d->alamatpemilik }} / {{$d->nama}}</option>
                          @endif
                          @endforeach
                       
                    </select>
                 </div>
              </div>  
              <div class="col-md-2 col-md-offset-5" style="margin-top: 20px;">
                 <button id="btn_next" type="submit" class="btn btn-info btn-block pull-right">NEXT</button>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3" style="margin-top: 20px;">
                <label>List SO selesai</label>
                <table class="table table-striped table-bordered" style="font-size: 9pt">
                    <thead>
                       <tr class="success">
                          <th>No
                             @if($jenis[0] == 1) MR
                             @else SO
                             @endif
                          </th>
                          <th style="text-align: center;">Alamat</th>
                       </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $d)
                        @if($d->persen >= 100)
                          <tr>
                            <td>{{ $d->nomor }}</td>
                            <td>{{$d->nama}}</td>
                          </tr>
                          
                        @endif
                      @endforeach
                    </tbody>
                 </table>
              </div>
              <div class="col-md-9" style="margin-top: 20px;">
                <label>List SO Terpilih</label>
                 <table class="table table-striped table-bordered" id='table_MR' style="font-size: 9pt">
                    <thead>
                       <tr class="success">
                          <th>No
                             @if($jenis[0] == 1) MR
                             @else SO
                             @endif
                          </th>
                          <th width="30%" style="text-align: center;">Alamat</th>
                          <th width="15%" style="text-align: center;">Action</th>
                       </tr>
                    </thead>
                    <tbody id="tbody_MR">
                       <tr></tr>
                    </tbody>
                 </table>
              </div>
            </div>

         </form>
       </div>
   </div>

@endsection

@push('bottom')
<script src="{{ asset('customs/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
   var mr_terpilih = [];

   $(function () {
      $("#pilih_mr").change(function() {
         var mr = $('#pilih_mr option:selected');
         var x = mr_terpilih.indexOf(mr.val());

         var area = $('#pilih_mr option:selected');
          var areaval = area.val();
         
         
         var mrdata = <?php echo json_encode($data); ?>;
         console.log(area);
         console.log(areaval);
         
         console.log(mrdata);
         $.each(mrdata, function(index, val) {
             if(val.id == areaval){
              console.log(val);
               mr_terpilih.push(val);
              if(val.persen==null ){
                val.persen = 0;
              }
              var rowCount = $('#tbody_MR tr').length;
              var row = '<tr>'+
                          '<td>'+val.nomor+' - '+val.nama+' - '+val.persen+'% </td>'+
                          '<td align="center">'+val.alamatpemilik+'</td>'+
                          '<td align="center"><button class="btn btn-danger btn-xs deleteMR" title="Hapus" mr="'+val.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button><input type="hidden" name="'+rowCount+'" value="'+val.id+'"></td></tr>';
              $('#tbody_MR').append(row);
             }

         });
         
         // console.log(mr_terpilih.length);
         // if (x == -1){
         //    mr_terpilih.push(mr.val());

         //    var rowCount = $('#tbody_MR tr').length;
         //    var row = '<tr>'+
         //                '<td>'+mr.data('nomr')+'</td>'+
         //                '<td align="center">'+mr.data('tanggal')+'</td>'+
         //                '<td align="center"><button class="btn btn-danger btn-xs deleteMR" title="Hapus" mr="'+mr.val()+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button><input type="hidden" name="'+rowCount+'" value="'+mr.val()+'"></td></tr>';
         //    $('#tbody_MR').append(row);
         // }
         // else{
         //    console.log("mr sudah dipilih");
         // }
             //}
      });
   });

   if (!Array.prototype.remove) {
      Array.prototype.remove = function(val) {
         var i = this.indexOf(val);
         return i>-1 ? this.splice(i, 1) : [];
      };
   }

   $('#table_MR').on('click','.deleteMR', function(event){
      event.preventDefault();
      var x = $(this).closest('button').attr("mr");
      mr_terpilih.remove(x);
      $(this).closest('tr').remove();
      return false;
   });

</script>
@endpush
