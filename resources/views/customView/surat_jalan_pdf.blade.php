<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Surat Jalan</title>
      <link href="{{ asset('customs/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('customs/css/bootstrap_render_print.css') }}" rel="stylesheet" type="text/css" />
   </head>
   <body>
      <div class="row" style="padding: 20px 20px 20px 20px;">
         <div class="col-md-12">
            <div class="col-md-12">
               <img src="{{ asset('gambar/logo.png') }}" style="width: 150px;">
            </div>
            <div style="width: 350px;">
               <h5><strong>PT. TATA KARYA GEMILANG</strong></h5>
               <h6>Jl. Sukun Mataram Bumi Sejahtera No.3, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281</h6>
               <h6>0878-3986-3930</h6>
            </div>
            <div>
               <h6 class="text-right"><strong>SURAT JALAN</strong></h6>
               <h6 class="text-right"><strong>{{ $data['kode'] }} / {{ $data['tanggal'] }}</strong></h6>
            </div>
         </div>
         <div class="col-md-12">
            <table class="table table-striped table-bordered" id='table_master'>
               <thead>
                  <tr class="success">
                     <th>NO</th>
                     <th>Barang</th>
                     <th>Jumlah</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $i=1; ?>
                  @foreach($data['kirim_barang']->sortBy('nama') as $k)
                  <tr>
                     <td>{{ $i }}</td>
                     <td>{{ $k->nama }}</td>
                     <td>{{ $k->jumlah }}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach
               </tbody>
            </table>
         </div>
         <div class="col-md-12">
            <div class="col-md-3">
               <br><br><br>
               <p class="text-right">(________________)</p>
            </div>
         </div>
      </div>
   </body>
</html>

<script src="{{ asset('customs/js/bootstrap.min.js')}}"></script>
