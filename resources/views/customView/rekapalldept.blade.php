
@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-list" aria-hidden="true"></i> Rekap Keseluruhan
@endsection

@section('breadcrumb')
   <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li><a href="{{CRUDBooster::adminPath(periode_mrs)}}">Periode MR</a></li>
   <li><a href="{{CRUDBooster::adminPath()}}/listdepartments/{{ $id }}">List Department</a></li>
   <li class="active">Rekap Keseluruhan</li>
@endsection

@section('content')
   
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;">

      <div class="box-body table-responsive no-padding">
        <a href="../listdepartmentsall/{{$id}}">Kembali</a>
      </div>
      </div>

      <div class="box box-default" style="padding-top: 20px;">

      <div class="box-body table-responsive no-padding">
       <div class="col-md-12 nodata" style="display: none;">
         <h5 class="" style="text-align: center;font-weight: bolder;">No Data Available</h5>
       </div>
       <div class="col-md-12 tablewrapper">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>


                     <th>nama barang</th>
                         <th>kode barang</th>
                         <th>jumlah barang</th>
                         <th>Akumulasi stok gudang</th>
                         <th>Min</th>
                         <th>satuan stok</th>
                         <th>Harga average</th>
                         <th>Harga average * jumlah</th>
                 </tr>
          </thead>
          <tfoot>
              <tr>
                          <th colspan="7" style="text-align:right">Total:</th>
                          <th></th>
              </tr>


          </tfoot>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>


@endsection

@push('bottom')
<script type="text/javascript">
  $(document).ready(function() {

          var table = $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': '../rekapalldeptdtajax/{{$id}}',
              "footerCallback": function ( row, data, start, end, display ) {
                  var api = this.api(), data;

                  // Remove the formatting to get integer data for summation
                  var intVal = function ( i ) {
                      return typeof i === 'string' ?
                          i.replace(/[\$,]/g, '')*1 :
                          typeof i === 'number' ?
                              i : 0;
                  };

                  // Total over all pages
                  total = api
                      .column( 7 )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                  // Total over this page
                  pageTotal = api
                      .column( 7, { page: 'current'} )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                  // Update footer
                  $( api.column( 7 ).footer() ).html(
                       'Total : '+total.toLocaleString("en") +',-'
                  );
              },
              

          } );
          




      } );
</script>
@endpush
