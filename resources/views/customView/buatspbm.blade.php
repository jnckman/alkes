@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">



   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <a href="../purchaseorders">
            <button class="btn btn-success">Kembali</button>
          </a>
        </div>
        <div class="col-md-6">
          <h5>Purchase Orders Status : <i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$hasil2[0]->nopo}} | <i class="fa fa-calendar fa-fw" aria-hidden="true"></i> : {{$hasil2[0]->tglpo}}</h5><h5>Supplier Status : <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$hasil2[0]->nama}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$hasil2[0]->notelp}}</h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          
        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
        <div class=" box-body">
        <form action='../submitspbm' method='POST'>

          <input type="hidden" name="purchaseorders_id" value='{{$id}}'>
          <input type="hidden" name="suppliers_id" value='{{$hasil[0]->customers_id}}'>
          <input type="hidden" name="first" value='{{$first}}'>
          <div class="col-md-12 form-group">
            <label>No SPBM</label>
            <input type="input" name="nospbm" class="form-control" readonly="" value="SPBM-{{date('ym')}}-{{$id}}-{{$lastid + 1}}">
            {{csrf_field()}}
          </div>
          <div class="col-md-12 form-group">
            <label>Tanggal Datang</label>
            <input type="date" name="tanggaldatang" class="form-control" value="<?php echo date("Y-m-d");?>">
          </div>
          <!-- <div class="col-md-12 form-group">
            <label>No Faktur</label>
            <input type="text" name="nofaktur" class="form-control" placeholder="isi jika diperlukan">
          </div>
          <div class="col-md-12 form-group">
            <label>No E - Faktur</label>
            <input type="text" name="efaktur" class="form-control" placeholder="isi jika diperlukan" value="none">
          </div>
          <div class="col-md-12 form-group">
            <label>Masa Pajak</label>
            <input type="text" name="masapajak" class="form-control" placeholder="isi jika diperlukan" value="none">
          </div> -->
          <div class="col-md-12 form-group">
            <label>Gudang Tujuan</label>
            <select class="form-control" name="gudangs_id" focus>
            @foreach($gudang as $g)
              <option class="form-control" value="{{$g->id}}">
                {{$g->nama}}
              </option>
            @endforeach
            </select>
            
          </div>
          <?php $ct = 0; ?>
          @if($first==0)

            @foreach($hasil as $h)

              <div class="col-md-12 form-group">

                <label>{{$h['nama']}} | Kode : {{$h['kode']}} | Satuan : {{$h['satuan']}} |dipesan : 
                <?php $jumlahasli=0; ?>
                @if($h->revisi->last()['jumlah_baru']==null)
                  {{$h->jumlah}}
                  <?php $jumlahasli = $h->jumlah; ?>
                @else
                  {{$h->revisi->last()['jumlah_baru']}}
                  <?php $jumlahasli = $h->revisi->last()['jumlah_baru']; ?> 
                    @if($h->revisi->last()['status'] == 'new') 
                      [Waiting Approval]
                    @endif 
                @endif | 
                @if($first == 0) 
                  Belum ada barang masuk 
                  <?php $h['total']=0;$h['jumlahdtg']=0; ?>
                @else 
                  terakhir masuk : {{$h['jumlahdtg']}} - {{$h['total']}} 
                @endif </label>
              </div>
              <div class="col-md-6 form-group">
                <input type="text" name="barangs_jumlah[]" class="form-control" placeholder="jumlah barang datang">
                <input type="hidden" name="barangs_id[]" class="form-control" value="{{$h['bid']}}">
                <input type="hidden" name="total[]" class="form-control" value="{{$h['total']}}">
                <input type="hidden" name="namabarang[]" class="form-control" value="{{$h['nama']}}">
                <input type="hidden" name="dipesan[]" class="form-control" value="{{$jumlahasli}}">
                <input type="hidden" name="sudahdatang[]" class="form-control" value="{{$h['jumlahdtg']}}">

              </div>
              <div class="col-md-6 form-group">
                <input type="text" name="barangs_kondisi[]" class="form-control" placeholder="kondisi barang datang" value="bagus">
              </div>
            @endforeach
          @else
            
            @foreach($hasil as $h)

              <?php $jumlahasli=0; $text='';?>
              @if($h->tes($h['ipoid']) == null || sizeof($h->tes($h['ipoid']))==0) 
                
                <?php $jumlahasli = $h->jumlah;$text = $h->jumlah; ?>
              @else
                
                <?php $jumlahasli = $h->tes($h['ipoid'])[0]['jumlah_baru']; $text = $h->tes($h['ipoid'])[0]['jumlah_baru']; ?> 
              @endif

              @if($jumlahasli != $h->total)
                  
                <div class="col-md-12 form-group">
                  <label>{{$h['nama']}} | Kode : {{$h['kode']}} | Satuan : {{$h['satuan']}} |dipesan : 
                  {{$text}} 
                  | 
                  @if($first == 0) 
                    <?php $h['total']=0; ?>
                    Belum ada barang masuk 
                  @else 
                    total barang datang : {{$h['total']}}
                  @endif</label>

                </div>
                <div class="col-md-6 form-group">
                  <input type="text" name="barangs_jumlah[]" class="form-control" placeholder="jumlah barang datang">
                  <input type="hidden" name="barangs_id[]" class="form-control" value="{{$h['bid']}}">
                  <input type="hidden" name="namabarang[]" class="form-control" value="{{$h['nama']}}">
                  <input type="hidden" name="sudahdatang[]" class="form-control" value="{{$h['total']}}">

                </div>
                <div class="col-md-6 form-group">
                  <input type="text" name="barangs_kondisi[]" class="form-control" placeholder="kondisi barang datang" value="bagus">
                </div>
                <input type="hidden" name="dipesan[]" class="form-control" value="{{$jumlahasli}}">
                
              @else
              <div class="col-md-12 form-group">
                <label>{{$h['nama']}} | Kode : {{$h['kode']}} | Satuan : {{$h['satuan']}} |dipesan : {{$h->revisi->last()['jumlah_baru']}} | 
                @if($first == 0) 
                  Belum ada barang masuk 
                @else 
                  total barang datang : {{$h['total']}}
                @endif 
                | <B style='color:green;'>SEMUA SUDAH DI TERIMA</B></label>
              </div>
              @endif

            @endforeach
          
          @endif
          <div class="col-md-12 form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>

          
        </form>
        </div>
     </div>
   </div>


@endsection

@section('datatable')

@endsection

