@extends('crudbooster::admin_template')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
   <div class="container-fluid">
     <div class="row" >
      <div class="box box-default" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
          <h4>Prosedur Perubahan PO</h4>
        </div>
        <div class="col-md-6">
          <h5><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> : {{$result[0]->nopo}} | <i class="fa fa-user fa-fw" aria-hidden="true"></i> : {{$result[0]->nama}} | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> : {{$result[0]->email}} | <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> : {{$result[0]->phone}}</h5> 
        </div>
        <div class="col-md-2 col-md-offset-3">
          

        </div>
      </div>
      </div>
      <div class="box box-default" style="padding-top: 20px;">
      <div class="box-body table-responsive no-padding">
       <div class="col-md-12">
         <table class="table table-hover table-striped table-bordered table1" id='table_dashboard'>
           <thead>
                 <tr>
                     
                     <th>
                                id
                            </th>
                            <th>
                               No PO
                            </th>
                            <th>
                              Status
                            </th>
                            <th>
                              Counter
                            </th>
                            <th>Action</th>
                 </tr>
          </thead>
         </table>
       </div>
       </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <div class="modal-body" style="height: 200px;">
                 <div class="col-md-12" style='margin-top: 10px;''>
                   <div class="col-md-4" style="padding-left: 0px;">
                     Total Pengeluaran : 
                   </div> 
                   <div class="col-md-8 totalpl">
                     
                   </div>
                 </div>
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">
                     
                   </div>
                 </div> -->
                 <input type="hidden" name="" id='supplierid' value=''>
                 <div class="col-md-12" style="margin-top: 10px;">
                      <table class="table table-bordered">
                          <thead>
                            <th>
                                id
                            </th>
                            <th>
                               No PO
                            </th>
                            <th>
                              Status
                            </th>
                            <th>
                              Counter
                            </th>
                          </thead>
                          <tbody class='tabelisibarang'>
                            
                          </tbody>
                      </table>
                 </div>
             </div>
             <div class="modal-footer">
                 
                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
   <div class="modal fade" id="buatpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <h4 class="modal-title" id="myModalLabel">Isi Data</h4>
             </div>
             <form action='submitpo' method='POST'>
             <div class="modal-body" >
                 <div class="col-md-12" style='margin-top: 10px;''>
                   <!-- <div class="col-md-4" style="padding-left: 0px;">
                     FORM PO
                   </div> 
                   <div class="col-md-8 totalpl">
                     
                   </div> -->
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>No POx</label>
                     <input type="text" name="nopo" class="form-control" >
                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Tanggal PO</label>
                     <input type="date" name="tglpo" class="form-control">
                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Kode SP</label>
                     <input type="text" name="kodesp" class="form-control">
                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Kepada</label>
                     <input type="text" name="customers_id" class="form-control" id="kepada" readonly>
                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Jenis PO</label>
                     <select name='jenispo' class="form-control">
                       <option value="1">Tempo</option>
                       <option value="2">Non PO</option>
                     </select>
                     

                   </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Tempo</label>
                     <input type="date" name="tempo" class="form-control">
                   </div>
                 </div>

                 <div class="col-md-6">
                   <div class="form-group">
                     <label>Ongkir</label>
                     <input type="text" name="ongkir" class="form-control">
                   </div>
                 </div>
                 <div class="col-md-12">
                 <input type="hidden" name="periode_mrs_id" class="form-control" value="" id="periodemr">
                   <table class="table table-bordered">
                     <thead>
                       <th>
                           Nama Barang
                       </th>
                       <th>
                          Jumlah - satuan
                       </th>
                       <th>
                         Harga satuan
                       </th>
                       <th>
                         Total
                       </th>
                       
                     </thead>
                     <tbody class='tabelisibarang'>
                       
                     </tbody>
                   </table>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label>PPN(%)</label>
                     <input type="text" name="" class="form-control">
                   </div>
                 </div>
                 <div class="col-md-12">
                  {{csrf_field()}}
                   <button type='submit' class="btn btn-success">SUBMIT PO</button>
                  
                 </div>
                 <!-- <div class="col-md-12" style="margin-top: 10px;">
                   <div class="col-md-4 " style="padding-left: 0px;">Total Sekarang : </div>
                   <div class="col-md-8 totalsk">
                     
                   </div>
                 </div> -->
                 
             </div>
             </form>
             <div class="modal-footer">
                 
                 <input type="hidden" id="task_id" name="task_id" value="0">
             </div>
         </div>
     </div>
   </div>
@endsection

@section('datatable')
<script type="text/javascript">
  $(document).ready(function() {
    
          $('.table1').DataTable( {
              'processing': true,
              'serverside':true,
              'ajax': 'listrequesteditposdtajax',
              
          } );
          var tabel;
          var isitabel='';
          var currentidsup;
          var total = 0;
          var lastpickidsup='';
          

          


      } );
</script>
@endsection
