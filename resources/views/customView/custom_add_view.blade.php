@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <form method='post' action='{{CRUDBooster::mainpath("savecustom")}}'>
    <div class='panel-heading'>Add SPBM</div>
    <div class='panel-body'>
      
        <div class='form-group'>
          <label>SPBM</label>
          <select class="form-control select2-hidden-accessible" id="select2barangs" required name="barangdatangs_id">
            <option></option>
          </select>
          <input type="hidden" name="bukufakturpos_id" value="{{$id}}">
          <input type="hidden" name="url" value="{{$url}}">
          {{csrf_field()}}
        </div>
         
        <!-- etc .... -->
        
      
    </div>
    <div class='panel-footer'>
      <input type='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
  
@endsection
@push('bottom')
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
    
    jQuery(document).ready(function($) {
      var barangs = <?php echo $category; ?>;
      
      $('#select2barangs').select2({
         data:barangs,
         placeholder:'Pilih Barang'
      });
    });
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/rase.loading.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('customs/css/select2.min.css') }}">
<style type="text/css">
 .selected{
  border-bottom: 2px solid grey;
 }
 .select2{
  width: 100% !important;
 }
</style>
@endpush