@extends('crudbooster::admin_template')

@section('header22')
   <i class="fa fa-file-text-o" aria-hidden="true"></i> Laporan MR
@endsection

@push('head')
<link href="{{ asset('customs/css/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('breadcrumb')
   <li><a href="{{ CRUDBooster::adminPath() }}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
   <li class="active">Laporan MR</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-default">
            <div class="box-header with-border">
               <button type="button" class="btn btn-default pull-right btn-md" data-target="#modalFilterPeriode" data-toggle="modal"><i class="fa fa-gear"></i> Filter</button>
            </div>
            <div class="box-body">
               <canvas id="bar-periode" width="400px" height="120px;"></canvas>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h5 class="text-center">Barang populer di MR</h5>
            </div>
            <div class="box-body">
               <div class="table-responsive">
               <table class="table table-bordered table-hover table-striped" id="table_dashboard">
                  <thead>
                     <th>Kode</th>
                     <th>Nama</th>
                     <th>Jumlah</th>
                  </thead>
               </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modalFilterPeriode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form id="formFilter">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Filter Graph Laporan :</h4>
         </div>
         <div class="modal-body" style="padding: 20px">
               <div class="form-group">
                  <label>Filter Berdasarkan</label>
                  <select id="select2tahunbulan" name="opsi" style="width:100%">
                     <option></option>
                     <option value="thn">Tahun</option>
                     <option value="bln">Bulan</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Start</label>
                  <input type="text" name="tglmulai" id="inputMulai" class="form-control" readonly="true" placeholder="Tanggal Mulai" />
               </div>
               <div class="form-group">
                  <label>End</label>
                  <input type="text" name="tglselesai" id="inputSelesai" class="form-control" readonly="true" placeholder="Tanggal Selesai" />
               </div>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('customs/js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('customs/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(function(){

   $('#select2tahunbulan').select2({
      placeholder : 'Pilih Tahun/Bulan'
   }).on('select2:select',function(){
      $('input[name="tglmulai"]').val('').attr('readonly',false);
      $('input[name="tglselesai"]').val('').attr('readonly',false);
      // console.log($(this).val());
      ubahDatePicker($(this).val());
   });

   function ubahDatePicker(option){
      $('#inputMulai').datepicker('remove');
      $('#inputSelesai').datepicker('remove');
      if(option=='bln'){
         $('input[name="tglmulai"],input[name="tglselesai"]').datepicker( {
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months"
         });
      }
      if(option=='thn'){
         $('input[name="tglmulai"],input[name="tglselesai"]').datepicker( {
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
         });
      }
   }

   var labelchart = {!!$labelChart!!};
   var datachart = {!!$dataChart!!};
   // console.log(datachart);
   var colors = randomColor(labelchart); 
   var ctx = document.getElementById("bar-periode").getContext('2d');

   function randomColor(labelchart){
      data = [];
      for (var i = 0; i < labelchart.length; i++) {
         data[i] = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
      }
      return data;
   }

   var chartPeriode = new Chart(ctx, {
      type: 'bar',
      data: {
         labels: labelchart, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
         datasets: [{
            label: 'Total Biaya MR',
            data: datachart,
            backgroundColor: colors,
            borderColor: colors,
               borderWidth: 1
         }]
      },
      options: {
         scales: {
            yAxes: [{
               ticks: {
                  beginAtZero:true
                  },
               gridLines: {
                    display: true,
                    drawBorder: true,
                    drawOnChartArea: false,
                },
               /*callback: function(value, index, values) {
                  if(parseInt(value) >= 1000){
                     return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  } else {
                     return '$' + value;
                  }
               }*/
            }]
         },
         title: {
              display: true,
              text: 'Total MR per Periode'
            }
      }
   });

   var tableBarang = $('#table_dashboard').DataTable({
      'processing': true,
      'serverside':true,
      "lengthChange": false,
      "searching": false,
      'ajax': '{{route("laporan.barangPopuler")}}',
      'order' : [[1,'desc']],
   });

   $('#formFilter').submit(function(event){
      event.preventDefault();
      url = '{{route("laporan.mr.filterPeriode")}}';
      $.ajax({
         url: url,
         type:'get',
         data:$('#formFilter').serialize(),
      }).done(function(response){
         chartRemoveData(chartPeriode);
         for (var i = 0; i < response.labelChart.length; i++) {
            chartAddData(chartPeriode,response.labelChart[i],response.dataChart[i]);
         }
         $('#modalFilterPeriode').modal('hide');
      }).fail(function(){
         swal('Ops','Terjadi Kesalahan','error');
      });
   });

   function chartRemoveData(chart) {
      // chart.data.labels.pop();
      panjang = chart.data.labels.length;
      for(i=0;i<panjang;i++){
         chart.data.labels.pop();
      }
      panjang2 =chart.data.datasets[0].data.length;
      for(i=0;i<panjang2;i++){
         // console.log(chart.data.datasets[0].data);
         chart.data.datasets[0].data.pop();
      }
      // chart.data.datasets.pop();
      // chart.data.datasets[0].forEach((dataset) => {
      //    dataset.data.pop();
      // });
      chart.update();
   }
   function chartAddData(chart, label, data) {
      chart.data.labels.push(label);
      chart.data.datasets.forEach((dataset) => {
         dataset.data.push(data);
      });
      chart.update();
   }
});
</script>
@endpush
