<?php
Route::post('/admin/simpansupplier', [ 'uses'=>'AdminWellhossubdeptsController@simpansupplier']);
Route::post('/admin/simpanjumlah', [ 'uses'=>'AdminWellhossubdeptsController@simpanjumlah']);
Route::get('/admin/ajukanatasan/{id}', [ 'uses'=>'AdminWellhossubdeptsController@ajukanatasan']);


Route::get('/admin/printtandaterima/{id}', [ 'uses'=>'AdminWellhossubdeptsController@printtandaterima']);

Route::get('/admin/susunpurchaseorder/{id}/{kode}', [ 'uses'=>'AdminWellhossubdeptsController@susunpurchaseorder']);
Route::get('/admin/susunpurchaseorderdtajax/{id}/{kode}', [ 'uses'=>'AdminWellhossubdeptsController@susunpurchaseorderdtajax']);
Route::get('/admin/ambilbarangpo/{id}/{mrid}/{status}/{kode}', [ 'uses'=>'AdminWellhossubdeptsController@ambilbarangpo']);

Route::post('/admin/susunpurchaseorder/{id}/submitpo', [ 'uses'=>'AdminWellhossubdeptsController@buatpurchaseorder']);
Route::get('/admin/isipurchaseorder/{id}', [ 'uses'=>'AdminWellhossubdeptsController@isipurchaseorder', 'as'=>'lihatpo']);
Route::get('/admin/isipurchaseorderdtajax/{id}', [ 'uses'=>'AdminWellhossubdeptsController@isipurchaseorderdtajax']);
Route::get('/admin/isisubdeptmr/{id}', [ 'uses'=>'AdminWellhossubdeptsController@isisubdeptmr']);
Route::get('/admin/isisubdeptmr/aktifkan/{id}', [ 'uses'=>'AdminWellhossubdeptsController@isisubdeptmraktifkan']);
Route::get('/admin/isisubdeptmrdtajax/{id}', [ 'uses'=>'AdminWellhossubdeptsController@isisubdeptmrdtajax']);
Route::post('/admin/listmrprajax', [ 'uses'=>'AdminWellhossubdeptsController@listmrprajax']);
Route::post('/admin/tambahbarangpricelist', [ 'uses'=>'AdminWellhossubdeptsController@tambahbarangpricelist']);
Route::get('/admin/pindahkangudang', [ 'uses'=>'AdminWellhossubdeptsController@pindahkangudang']);


Route::get('/admin/buatspbm/{id}', [ 'uses'=>'AdminIsiPosController@buatspbm']);
Route::post('/admin/submitspbm', [ 'uses'=>'AdminIsiPosController@submitspbm']);
Route::get('/admin/listspbm/{id}', [ 'uses'=>'AdminIsiPosController@listspbm']);
Route::get('/admin/listspbmdtajax/{id}', [ 'uses'=>'AdminIsiPosController@listspbmdtajax']);

Route::get('/admin/lihatisispbm/{id}', [ 'uses'=>'AdminIsiPosController@isilistspbm']);
Route::get('/admin/lihatisispbmdetail/{id}/{subid}', [ 'uses'=>'AdminIsiPosController@isilistspbmdetail']);
Route::get('/admin/isilistspbmdetaildtajax/{id}/{subid}', [ 'uses'=>'AdminIsiPosController@isilistspbmdetaildtajax']);
Route::post('/admin/editisispbmajax', [ 'uses'=>'AdminIsiPosController@editisispbmajax']);


Route::get('/admin/pengirimanbarangs', [ 'uses'=>'AdminPengirimansController@pengirimanbarangs']);
Route::get('/admin/pengirimanbarangsdtajax', [ 'uses'=>'AdminPengirimansController@pengirimanbarangsdtajax']);
Route::post('/admin/ambilisimrbyprojectajax', [ 'uses'=>'AdminPengirimansController@ambilisimrbyprojectajax']);
Route::post('/admin/submitpengiriman', [ 'uses'=>'AdminPengirimansController@submitpengiriman']);
Route::post('/admin/detailpengirimanajax', [ 'uses'=>'AdminPengirimansController@detailpengirimanajax']);

Route::get('/admin/requesteditpo/{id}', [ 'uses'=>'AdminPurchaseordersController@requesteditpo']);

Route::get('/admin/adjustpricelist', [ 'uses'=>'AdminPurchaseordersController@adjustpricelist']);

Route::get('/admin/listrequesteditpo', [ 'uses'=>'AdminPurchaseordersController@listrequesteditpo']);
Route::get('/admin/listrequesteditposdtajax', [ 'uses'=>'AdminPurchaseordersController@listrequesteditposdtajax']);
Route::get('/admin/reviewpengajuaneditpo/{id}', [ 'uses'=>'AdminPurchaseordersController@reviewpengajuaneditpo']);
Route::get('/admin/cancelreviewpengajuaneditpo/{id}', [ 'uses'=>'AdminPurchaseordersController@cancelreviewpengajuaneditpo']);
Route::get('/admin/approvepengajuaneditpo/{id}', [ 'uses'=>'AdminPurchaseordersController@approvepengajuaneditpo']);
Route::get('/admin/ambilbarangeditpo/{id}', [ 'uses'=>'AdminPurchaseordersController@ambilbarangeditpo']);
Route::post('/admin/simpaneditpo', [ 'uses'=>'AdminPurchaseordersController@simpaneditpo']);


Route::get('/admin/listreturdtajax/{id}', [ 'uses'=>'AdminReturbarangsController@listreturdtajax']);
Route::get('/admin/returisispbm/{id}', [ 'uses'=>'AdminReturbarangsController@returisispbm']);
Route::get('/admin/getlastreturid', [ 'uses'=>'AdminReturbarangsController@getlastreturid']);
Route::post('/admin/postnewretur', [ 'uses'=>'AdminReturbarangsController@postnewretur']);


Route::get('/admin/gantijumlahisimrsubdept/{id}', [ 'uses'=>'AdminWellhossubdeptsController@gantijumlahisimrsubdept']);


Route::get('/admin/purchaseorderspembelian', ['as'=>'purchaseorderspembelian.index' ,'uses'=>'AdminPurchaseordersController@purchaseorderspembelian']);
Route::get('/admin/purchaseorderspembelianmr', ['as'=>'purchaseorderspembelian.index.mr' ,'uses'=>'AdminPurchaseordersController@purchaseorderspembelianmr']);
Route::get('/admin/purchaseorderspembeliannonmr', ['as'=>'purchaseorderspembelian.index.nonmr' ,'uses'=>'AdminPurchaseordersController@purchaseorderspembeliannonmr']);
Route::post('/admin/ambildataeditdetail', ['as'=>'purchaseorderspembelian.edit' ,'uses'=>'AdminPurchaseordersController@ambildataeditdetail']);
Route::post('/admin/editisipolama/{id}', ['as'=>'purchaseorderspembelian.editisipolama' ,'uses'=>'AdminPurchaseordersController@editisipolama']);
Route::post('/admin/ambildatapricelist', ['as'=>'purchaseorderspembelian.edit' ,'uses'=>'AdminPurchaseordersController@ambildatapricelist']);
Route::post('/admin/purchaseorderspembelian/editisipos', ['as'=>'purchaseorderspembelian.editpost' ,'uses'=>'AdminPurchaseordersController@editisipos']);
Route::get('/admin/purchaseorderspembeliandelpost/{id}', ['as'=>'purchaseorderspembeliandelpost.deletepo' ,'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandelpost']);
Route::post('/admin/purchaseorderspembelianeditpost/{id}', ['as'=>'purchaseorderspembelian.editpost' ,'uses'=>'AdminPurchaseordersController@detaileditpost']);
Route::post('/admin/purchaseorderspembeliandeletepost/{id}', ['as'=>'purchaseorderspembelian.deletepost' ,'uses'=>'AdminPurchaseordersController@detaildeletepost']);



Route::get('/admin/purchaseorderspembeliandtajax', [ 'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandtajax']);
Route::get('/admin/pogetlastid', [ 'uses'=>'AdminPurchaseordersController@pogetlastid']);
Route::post('/admin/purchaseorderspembelian/submitpopembelian', [ 'uses'=>'AdminPurchaseordersController@buatpurchaseorderspembelian']);


Route::get('/admin/rekappo', [ 'uses'=>'AdminPurchaseordersController@rekappo']);
Route::get('/admin/rekappopembelian/{id}', [ 'uses'=>'AdminPurchaseordersController@rekappopembelian']);

Route::get('/admin/purchaseorderspembelian/detail/{id}', [ 'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandetail']);
Route::get('/admin/purchaseorderspembelian/print/{id}', [ 'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandetail']);

Route::get('/admin/purchaseorderspembelian/detail/dtajax/{id}', [ 'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandetaildtajax']);
Route::get('/admin/purchaseorderspembelian/delete/{id}', [ 'uses'=>'AdminPurchaseordersController@purchaseorderspembeliandelete']);
Route::post('/admin/purchaseorderspembelian/submitisipo', [ 'uses'=>'AdminPurchaseordersController@buatisipurchaseorderspembelian']);
Route::post('/admin/purchaseorderspembelian/submitisipobaranglama', [ 'uses'=>'AdminPurchaseordersController@buatisipurchaseorderspembelianbaranglama']);


Route::get('/admin/laporanmr', ['as'=>'laporanmr', 'uses'=>'AdminMrSubdepts36Controller@laporan']);
Route::get('/admin/laporanmr/periodemr', ['as'=>'laporanmrperiode', 'uses'=>'AdminMrSubdepts36Controller@laporanperiodemr']);

Route::get('/admin/laporanso', ['as'=>'laporanso', 'uses'=>'AdminWellhossubdeptsController@laporanso']);
Route::post('/admin/submitlaporan', ['as'=>'submitlaporan', 'uses'=>'AdminWellhossubdeptsController@submitlaporan']);

// custom index
Route::post('/admin/purchaseorders?q=PO%2F%2F', ['as'=>'pobeli.index' ,'uses'=>'AdminPurchaseordersController@index']);
Route::post('/admin/purchaseorders?q=MR', ['as'=>'pomr.index' ,'uses'=>'AdminPurchaseordersController@index']);
Route::get('/admin/purchaseorders/printapprove/{id}', ['as'=>'po.printapprove' ,'uses'=>'AdminPurchaseordersController@getPrintapprove']);


Route::get('/masuk/{jenis}/{kategori}/{tanggal}', ['uses'=>'AdminBarangdatangsController@masuk']);

Route::get('/admin/pendingmr', ['as'=>'pendingmr', 'uses'=>'AdminWellhossubdeptsController@pendingmr']);
Route::get('/admin/lihatisipendingmr/{id}', ['as'=>'lihatisi', 'uses'=>'AdminWellhossubdeptsController@lihatisipendingmr']);

Route::get('/admin/approvedataspbm/{poid}/{id}', ['as'=>'approvespbm', 'uses'=>'AdminPurchaseordersController@approvespbm']);

Route::get('/admin/laporanpembelian', ['as'=>'laporanpembelian', 'uses'=>'AdminPurchaseordersController@laporanpembelian']);
Route::post('/admin/laporanpembelianbulan', ['as'=>'laporanpembelianbulan', 'uses'=>'AdminPurchaseordersController@laporanpembelianbulan']);
Route::post('/admin/laporanhutangbulan', ['as'=>'laporanpembelianbulan', 'uses'=>'AdminPurchaseordersController@laporanhutangbulan']);
Route::get('/admin/laporanrekening', ['as'=>'laporanrekening', 'uses'=>'AdminPurchaseordersController@laporanrekening']);
Route::post('/admin/laporanrekeningbulan', ['as'=>'laporanrekeningbulan', 'uses'=>'AdminPurchaseordersController@laporanrekeningbulan']);
Route::get('/admin/laporanpenjualan', ['as'=>'laporanpenjualan', 'uses'=>'AdminPurchaseordersController@laporanpenjualan']);
Route::post('/admin/laporanpenjualanbulan', ['as'=>'laporanpenjualanbulan', 'uses'=>'AdminPurchaseordersController@laporanpenjualanbulan']);
Route::get('/admin/jurnal', ['as'=>'jurnal', 'uses'=>'LaporanController@jurnal']);
Route::post('/admin/jurnalbulan', ['as'=>'jurnalbulan', 'uses'=>'LaporanController@jurnalbulan']);



Route::post('/admin/isibukufakturpos/savecustom', ['as'=>'savecustom', 'uses'=>'AdminIsibukufakturposController@savecustom']);
?>