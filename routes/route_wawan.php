<?php
Route::group(['middleware' => ['hanyaLogin']], function() {
   Route::prefix('admin')->group(function () {

      //NOTE barangs
      Route::get('/barangs/dt_master', ['as' => 'get.dt.master', 'uses'=>'AdminBarangsController@dt_master']);

      Route::get('/barang/detail/{id}',['as'=>'getBarangDetail','uses'=>'AdminBarangsController@getDetail']);

      Route::get('/barangs/getpricelist', ['as' => 'getPricelist', 'uses'=>'AdminBarangsController@ajax_pricelist']);

      Route::get('/barangs/ajax', ['as' => 'barang_ajax', 'uses'=>'AdminBarangsController@barang_ajax']);

      Route::get('/barangs/delete_barang/{id}', ['as' => 'delete.barang', 'uses'=>'AdminBarangsController@delete_barang']);

      Route::post('/barangs/add_pricelist', ['as' => 'add.pricelist.barang', 'uses'=>'AdminBarangsController@add_pricelist']);

      Route::post('/barangs/add_barang', ['as' => 'add.barang', 'uses'=>'AdminBarangsController@add_barang']);

      Route::post('/barangs/edit_barang', ['as' => 'edit.barang', 'uses'=>'AdminBarangsController@edit_barang']);


      //NOTE gudangs
      Route::post('/gudangs/transfer', ['as' => 'transfer_barang', 'uses'=>'AdminGudangsController@transferBarang']);

      Route::get('/gudangs/dt_master', ['as' => 'get.dt.master.gudang', 'uses'=>'AdminGudangsController@dt_master']);

      Route::get('/gudangs/barang', ['as' => 'get_gudang_barang', 'uses'=>'AdminGudangsController@gudang_barang']);


      //NOTE barang_gudangs
      Route::get('/barang_gudangs/transfer/{id}',['as'=>'barang_gudangs.transfer.view','uses'=>'AdminBarangGudangs30Controller@transferView']);

      Route::post('/barang_gudangs/transfer',['as'=>'barang_gudangs.transfer.barang','uses'=>'AdminBarangGudangs30Controller@transferBarang']);

      Route::get('/barang_gudangs/detail_barang/{id}',['as'=>'getBarangDetail.barangGudangs','uses'=>'AdminBarangGudangs30Controller@getDetail']);


      //NOTE periode_mrs
      Route::post('/add_pricelist', ['as' => 'add.pricelist.barang.2', 'uses'=>'AdminPeriodeMrsController@add_pricelist']);

      Route::post('/add_pricelistajax', ['as' => 'add.pricelist.barang.3', 'uses'=>'AdminPeriodeMrsController@add_pricelistajax']);

      Route::get('/log_pricelist_pr', ['as' => 'add.log.pricelist.pr', 'uses'=>'AdminPeriodeMrsController@log_pricelist']);

      Route::post('/approve_pr', ['as' => 'approve.pr', 'uses'=>'AdminPeriodeMrsController@approve_pr']);
      Route::post('/unapprove_pr', ['as' => 'unapprove.pr', 'uses'=>'AdminPeriodeMrsController@unapprove_pr']);


      //NOTE api pusat wellhos
      Route::get('/list-asm', ['as' => 'get.list.asm', 'uses'=>'ApiWellhosController@getListAsm']);


      //NOTE pengiriman barangs
      Route::get('/kirim-barang', ['as' => 'index.pengiriman.barang' ,'uses'=>'AdminPengirimanBarangs77Controller@getIndex']);

      Route::get('/kirim-barang/{jenis1}/{jenis2}/{id}', ['as' => 'view.kirim' ,'uses'=>'AdminPengirimanBarangs77Controller@view_kirim']);
      Route::get('/kirim-barang/{jenis1}/{jenis2}/{id}/{periode}', ['as' => 'view.kirim' ,'uses'=>'AdminPengirimanBarangs77Controller@view_kirim']);

      Route::post('/action_kirim_barang/{id}', ['as' => 'action.pengiriman.barang' ,'uses'=>'AdminPengirimanBarangs77Controller@action_kirim_barang']);


      //NOTE list pengiriman barangs
      Route::get('/indexpengiriman', ['as' => 'index.list.pengiriman.barang0' ,'uses'=>'ListPengirimanController@getIndex0']);
      Route::get('/listpengiriman/{id}', ['as' => 'index.list.pengiriman.barang' ,'uses'=>'ListPengirimanController@getIndex']);

      Route::get('/list-pengiriman-dt_master', ['as' => 'dt.master.list.pengiriman', 'uses'=>'ListPengirimanController@dt_master']);
      Route::get('/list-pengiriman-dt_master0', ['as' => 'dt.master.list.pengiriman0', 'uses'=>'ListPengirimanController@dt_master0']);

      Route::get('/list-pengiriman/detail-pengiriman/{kode}', ['as' => 'view.detail.pengiriman.barang' ,'uses'=>'ListPengirimanController@view_detail_kirim_barang']);

      Route::get('/list-pengiriman/surat-jalan/{kode}', ['as' => 'surat.jalan.view', 'uses'=>'ListPengirimanController@surat_jalan_view']);

      Route::get('/surat-jalan-dt-master/{kode}', ['as' => 'surat.jalan.dt', 'uses'=>'ListPengirimanController@surat_jalan_dt']);

      Route::get('/surat-jalan-pdf/{kode}', ['as' => 'surat.jalan.pdf', 'uses'=>'ListPengirimanController@surat_jalan_pdf']);

      Route::get('/tanda-terima/{kode}', ['as' => 'tanda.terima', 'uses'=>'ListPengirimanController@tanda_terima']);
      Route::get('/tanda-terima/{kode}/{tgl}', ['as' => 'tanda.terima2', 'uses'=>'ListPengirimanController@tanda_terima']);

      Route::get('/tanda-terima-single/{pengiriman_id}', ['as' => 'tanda.terima.single', 'uses'=>'ListPengirimanController@tanda_terima_single']);

      Route::post('/list-pengiriman/kembali-barang', ['as' => 'kembali.barang', 'uses'=>'ListPengirimanController@kembali_barang']);


      //NOTE menu Material Request
      Route::get('/list-mr-belum-approve', ['as' => 'list.mr.blm.acc', 'uses'=>'AdminWellhosSubdeptsController@list_belum_approve']);

      Route::get('/list-mr-belum-approve/dt_master', ['as' => 'dt.master.list.mr.blm.acc', 'uses'=>'AdminWellhosSubdeptsController@dt_list_belum_approve']);

   });
});
