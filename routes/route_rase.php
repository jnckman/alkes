<?php
Route::group(['middleware'=>['hanyaLogin']],function(){
	Route::get('/mrmainpage/projects', ['as'=>'subdept.mr.mainpage', 'uses'=>'AdminMrSubdepts36Controller@materialrequestmainpage']);
	Route::post('/mrmainpage/projects', ['as'=>'subdept.mr.postProject', 'uses'=>'AdminMrSubdepts36Controller@postProject']);
	Route::get('/mrmainpage/kategori/{id}',['as'=>'subdept.mr.getBarangsByKategoris','uses'=>'AdminMrSubdepts36Controller@getBarangsByKategoris']);
	Route::get('/mrmainpage',['as'=>'subdept.mr.home', 'uses'=>'AdminMrSubdepts36Controller@getBarangsByKategoris']);
	Route::post('/mrmainpage/addCart',['as'=>'subdept.mr.addCart','uses'=>'AdminMrSubdepts36Controller@addCart']);
	Route::get('/mrmainpage/cart/{id}',['as'=>'subdept.mr.getCart','uses'=>'AdminMrSubdepts36Controller@getCart']);
	Route::post('/mrmainpage/postCart',['as'=>'subdept.mr.postCart','uses'=>'AdminMrSubdepts36Controller@postCart']);
	Route::delete('/mrmainpage/delCart',['as'=>'subdept.mr.delCart','uses'=>'AdminMrSubdepts36Controller@delCart']);
	Route::post('/mrmainpage/cari',['as'=>'subdept.mr.cariBarang','uses'=>'AdminMrSubdepts36Controller@cariBarang']);
	Route::get('/mrmainpage/getGroupBarangs/{id}',['as'=>'subdept.mr.getGroupBarangs','uses'=>'AdminMrSubdepts36Controller@getGroupBarangs']);
	Route::post('/mrmainpage/historyUpdateJumlahBarang',['as'=>'subdept.mr.historyUpdateJumlahBarang','uses'=>'AdminMrSubdepts36Controller@historyUpdateJumlahBarang']);
	Route::delete('/mrmainpage/historyDelBarang',['as'=>'subdept.mr.historyDelBarang','uses'=>'AdminMrSubdepts36Controller@historyDelBarang']);
	Route::post('/mrmainpage/requestBarang',['as'=>'subdept.mr.requestBarang','uses'=>'AdminMrSubdepts36Controller@requestBarang']);
	Route::get('/mrmainpage/getSubdept/{id}',['as'=>'subdept.mr.getSubdept','uses'=>'AdminMrSubdepts36Controller@getSubdept']);
	Route::get('/mrmainpage/ajax_getBarang',['as'=>'subdept.mr.ajax_getBarang','uses'=>'AdminMrSubdepts36Controller@ajax_getBarang']);
	Route::get('/mrmainpage/ajax_getListBarang',['as'=>'subdept.mr.ajax_getListBarang','uses'=>'AdminMrSubdepts36Controller@ajax_getListBarang']);
	Route::post('/mrmainpage/tukarBarang',['as'=>'subdept.mr.tukarBarang','uses'=>'AdminMrSubdepts36Controller@tukarBarang']);
	Route::get('/mrmainpage/user',['as'=>'mr.getUser','uses'=>'AdminMrSubdepts36Controller@getUser']);
	Route::post('/mrmainpage/user/approve',['as'=>'mr.user.postApprove','uses'=>'AdminMrSubdepts36Controller@postApprove']);
	Route::post('/mrmainpage/user/selesai',['as'=>'mr.user.postSelesai','uses'=>'AdminMrSubdepts36Controller@postSelesai']);
	Route::post('/mrmainpage/user/deleteMR',['as'=>'mr.user.deleteMR','uses'=>'AdminMrSubdepts36Controller@deleteMR']);
	Route::get('/mrmainpage/user/dt_history',['as'=>'mr.dt_history','uses'=>'AdminMrSubdepts36Controller@dt_history']);
	Route::get('/mrmainpage/user/dt_barangs',['as'=>'mr.dt_history','uses'=>'AdminMrSubdepts36Controller@dt_barangs']);
	Route::post('/mrmainpage/user/updateStatus',['as'=>'subdept.mr.user.updateStatus','uses'=>'AdminMrSubdepts36Controller@updateStatus']);
	Route::get('/mrmainpage/history_mr',['as'=>'mr.getUser.history_mr','uses'=>'AdminMrSubdepts36Controller@history_mr']);
	Route::get('/mrmainpage/ajax_getMRs',['as'=>'mr.getUser.ajax_getMRs','uses'=>'AdminMrSubdepts36Controller@ajax_getMRs']);
	Route::get('/mrmainpage/ajaxGetProject',['as'=>'mr.getUser.ajaxGetProject','uses'=>'AdminMrSubdepts36Controller@ajaxGetProject']);
	Route::get('/mrmainpage/getlistkirim/{id}',['as'=>'mr.getlistkirim','uses'=>'AdminMrSubdepts36Controller@getlistkirim']);
	Route::post('/mrmainpage/kirimselesai',['as'=>'subdept.mr.kirimselesai','uses'=>'AdminMrSubdepts36Controller@kirimselesai']);
	
	//SHOP
	Route::get('/shop',['as'=>'getShop','uses'=>'AdminShopsController@getHome']);
	Route::get('/shop/cart',['as'=>'shop.getCart','uses'=>'AdminShopsController@getCart']);
	Route::get('/shop/test',['uses'=>'AdminShopsController@testFlash']);
	Route::get('/shop/kategori/{id}',['as'=>'shop.barangByKategori','uses'=>'AdminShopsController@getHome']);
	Route::post('/shop',['as'=>'shop.addCart','uses'=>'AdminShopsController@addCart']);
	Route::get('/shop/cari/',['as'=>'shop.cariBarang','uses'=>'AdminShopsController@cariBarang']);
	Route::delete('/shop/cart',['as'=>'shop.delCart','uses'=>'AdminShopsController@delCart']);
	Route::post('/shop/cart',['as'=>'shop.checkout','uses'=>'AdminShopsController@checkout']);
	Route::get('/shop/getDetailBarang',['as'=>'shop.getDetailBarang','uses'=>'AdminShopsController@getDetailBarang']);
	Route::post('/shop/setcustomer',['as'=>'so.setcustomer','uses'=>'AdminShopsController@setcustomer']);
	Route::get('/shop/getcustomer',['as'=>'so.getcustomer','uses'=>'AdminShopsController@getcustomer']);
	Route::get('/shop/profil/',['as'=>'shop.getProfil','uses'=>'AdminShopsController@getProfil']);
	Route::get('/shop/profil/dtsales_order',['as'=>'shop.dtsales_order','uses'=>'AdminShopsController@dtsales_order']);
	Route::get('/shop/profil/dt_returso',['as'=>'shop.dt_returso','uses'=>'AdminShopsController@dt_returso']);
	Route::get('/shop/profil/getPengiriman',['as'=>'shop.getPengiriman','uses'=>'AdminShopsController@getPengiriman']);
	Route::post('/shop/profil/sunting/{id}',['as'=>'shop.profile.sunting','uses'=>'AdminShopsController@profilSunting']);
	Route::get('/shop/returSO/{id}',['as'=>'shop.returso','uses'=>'AdminShopsController@returSO']);
	Route::post('/shop/returSO',['as'=>'shop.postReturSO','uses'=>'AdminShopsController@postReturSO']);
	Route::post('shop//tandaiselesai',['as'=>'so.tandaiselesai','uses'=>'AdminShopsController@tandaiselesai']);
});

Route::group(['middleware'=>['web','\crocodicstudio\crudbooster\middlewares\CBBackend']],function(){
	//harga_jual
	Route::get('admin/harga_jual/bybarang_dt/{id}',['as'=>'harga_jual.bybarang_dt','uses'=>'AdminHargaJualsController@bybarang_dt']);
	Route::post('admin/harga_juals/addhargajualbybarang',['as'=>'harga_jual.postaddbybarang','uses'=>'AdminHargaJualsController@postaddbybarang']);

	//stokopnam
	Route::post('tambahstokopnam',['as'=>'stokopnam.tambahstokopnam','uses'=>'AdminStokopnamsController@tambahstokopnam']);	

	// Retur SO
	Route::post('admin/returso/isi/{id}',['as'=>'returso.postApprove','uses'=>'AdminRetursoController@postApprove']);
	Route::get('admin/returso/isi/{id}',['as'=>'returso.isiRetur','uses'=>'AdminRetursoController@isiRetur']);
	Route::get('admin/returso/isi/{id}/dt_isi',['as'=>'isiRetur.dt_isi','uses'=>'AdminRetursoController@dt_isi_retur']);

	// PO
	Route::get('admin/purchaseorders/laporan',['as'=>'po.getLaporan','uses'=>'AdminPurchaseordersController@getLaporan']);
	Route::get('admin/purchaseorders/getProgress',['as'=>'po.rase.getProgress','uses'=>'AdminPurchaseordersController@getProgress']);
	Route::post('admin/revisipo',['as'=>'po.postRevisi','uses'=>'AdminPurchaseordersController@postRevisi']);
	Route::get('admin/purchaseorders/dtisirevisi/{id}',['as'=>'po.isi.getRevisiByPo','uses'=>'AdminRevisiPosController@getRevisiByPo']);
	Route::get('admin/selectsupplier',['as'=>'po.getlistsupplier','uses'=>'AdminPurchaseordersController@getlistsupplier']);
	Route::get('admin/update_isi_po_dengan_revisi',['as'=>'po.update_isi_po_dengan_revisi','uses'=>'AdminRevisiPosController@update_isi_po_dengan_revisi']);

	//Revisi PO
	Route::post('admin/revisi_pos/reject/{id}',['as'=>'po.revisi.reject','uses'=>'AdminRevisiPosController@reject']);

	// PR
	Route::get('admin/pr/listBelumApprove',['as'=>'pr.getBelumApproved','uses'=>'AdminRekapMrsController@getBelumApproved']);
	Route::get('admin/pr/dtlistBelumApprove',['as'=>'pr.dtlistBelumApprove','uses'=>'AdminRekapMrsController@dtlistBelumApprove']);
	Route::get('admin/pr/raselistmrprajax',['as'=>'pr.raselistmrprajax','uses'=>'AdminRekapMrsController@raselistmrprajax']);
	Route::get('admin/pr/raselistmrprbarangajax',['as'=>'pr.raselistmrprbarangajax','uses'=>'AdminRekapMrsController@raselistmrprbarangajax']);
	Route::post('admin/pr/listBelumApprove',['as'=>'pr.updateApprove','uses'=>'AdminRekapMrsController@updateApprove']);
	//laporan dua berlian
	Route::get('admin/laporan/dubercust',['as'=>'laporan.dubercust','uses'=>'LaporanController@dubercust']);
	Route::post('admin/laporan/dubercustpost',['as'=>'laporan.dubercustpost','uses'=>'LaporanController@dubercustpost']);
	
	Route::post('admin/laporan/imitmfpost',['as'=>'laporan.imitmfpost','uses'=>'LaporanController@imitmfpost']);
	
	Route::post('admin/laporan/rawpost',['as'=>'laporan.rawpost','uses'=>'LaporanController@rawpost']);

	//laporan
	Route::get('admin/laporan/mr',['as'=>'laporan.mr.index','uses'=>'LaporanController@getMrIndex']);
	Route::get('admin/laporan/aruskas',['as'=>'laporan.aruskas','uses'=>'LaporanController@aruskas']);
	Route::get('admin/laporan/getaruskas',['as'=>'laporan.getaruskas','uses'=>'LaporanController@getaruskas']);
	Route::get('admin/laporan/mr/filterPeriode',['as'=>'laporan.mr.filterPeriode','uses'=>'LaporanController@filterPeriode']);
	Route::get('admin/laporan/dtBarangPopuler',['as'=>'laporan.barangPopuler','uses'=>'LaporanController@barangPopuler']);
	Route::get('admin/laporan/chartPeriode',['as'=>'laporan.mr.chartPeriode','uses'=>'LaporanController@chartPeriode']);
	Route::get('admin/laporan/barangmasuk',['as'=>'laporan.getbarangmasuk','uses'=>'LaporanController@getbarangmasuk']);
	Route::get('admin/laporan/dtbarangmasuk',['as'=>'laporan.dtbarangmasuk','uses'=>'LaporanController@dtbarangmasuk']);
	Route::get('admin/laporan/dtbarangkeluar',['as'=>'laporan.dtbarangkeluar','uses'=>'LaporanController@dtbarangkeluar']);
	Route::get('admin/laporan/barangkeluar',['as'=>'laporan.getbarangkeluar','uses'=>'LaporanController@getbarangkeluar']);
	Route::get('admin/laporan/getpo',['as'=>'laporan.getpo','uses'=>'LaporanController@getpo']);
	Route::get('admin/laporan/getperiode',['as'=>'laporan.getperiode','uses'=>'LaporanController@getperiode']);
	Route::get('admin/laporan/getpodt',['as'=>'laporan.getpodt','uses'=>'LaporanController@getpodt']);
	Route::get('admin/laporan/getlaporanadmindashboard',['as'=>'laporan.getlaporanadmindashboard','uses'=>'LaporanController@getlaporanadmindashboard']);
	Route::get('admin/laporan/dashboardadmingetpo',['as'=>'laporan.dashboardadmingetpo','uses'=>'LaporanController@dashboardadmingetpo']);
	Route::get('admin/laporan/dashboardadmingetmr',['as'=>'laporan.dashboardadmingetmr','uses'=>'LaporanController@dashboardadmingetmr']);
	Route::get('admin/laporan/dashboardadmingetnilaistok',['as'=>'laporan.dashboardadmingetnilaistok','uses'=>'LaporanController@dashboardadmingetnilaistok']);
	Route::get('admin/laporan/dashboardadmingetpengeluaranbarang',['as'=>'laporan.dashboardadmingetpengeluaranbarang','uses'=>'LaporanController@dashboardadmingetpengeluaranbarang']);
	Route::get('admin/laporan/pengeluaranbarangperproject',['as'=>'laporan.pengeluaranbarangperproject','uses'=>'LaporanController@pengeluaranbarangperproject']);
	Route::get('admin/laporan/timetofullmr',['as'=>'laporan.timetofullmr','uses'=>'LaporanController@timetofullmr']);
	Route::get('admin/laporan/timetopo',['as'=>'laporan.timetopo','uses'=>'LaporanController@timetopo']);
	Route::get('admin/laporan/getbaranggerak',['as'=>'laporan.getbaranggerak','uses'=>'LaporanController@getbaranggerak']);
	Route::get('admin/laporan/getpengeluaranbarang',['as'=>'laporan.getpengeluaranbarang','uses'=>'LaporanController@getpengeluaranbarang']);
	Route::get('admin/laporan/dashboard_procurement',['as'=>'laporan.dashboard_procurement','uses'=>'LaporanController@dashboard_procurement']);
	Route::get('admin/laporan/stokminmax',['as'=>'laporan.stokminmax','uses'=>'LaporanController@stokminmax']);

	//assign
	Route::get('admin/assign_users/gettableid',['as'=>'assign.gettableid','uses'=>'AdminAssignUsersController@gettableid']);
	Route::post('admin/assign_users/postSave',['as'=>'assign.postSave','uses'=>'AdminAssignUsersController@postSave']);
	Route::post('admin/assign_users/postEdit',['as'=>'assign.postEdit','uses'=>'AdminAssignUsersController@postEdit']);

	//Pengiriman Barang
	Route::get('admin/listTandaTerimaPengiriman',['as'=>'pb.listTandaTerimaPengirimanajax','uses'=>'ListPengirimanController@listTandaTerimaPengirimanajax']);

	// rahasia
	Route::post('rahasia/suppliers/wepe/{id}',['as'=>'rahasia.updateBarang','uses'=>'AdminSuppliersController@postRahasia']);//lihat source code, untuk upload file
	Route::get('rahasia/barang/addhargarata',['as'=>'rahasia.addhargarata','uses'=>'AdminBarangsController@addhargarata']); //tambah yg blm ad harga rata
	Route::get('rahasia/so/rahasia_update_ppn',['as'=>'rahasia.rahasia_update_ppn','uses'=>'AdminShopsController@rahasia_update_ppn']);//update semua ppn ke 10%
	Route::post('admin/purchaseorders/editrahasia',['as'=>'po.editrahasia','uses'=>'AdminPurchaseordersController@editrahasia']);// ad tombol siap pakai di purchaseordercontroller yg tidak ditayangkan untuk mengubah jumlah isi po secara manual + sudah ada bantuan hittung total

	//sales order
	Route::get('admin/salesorders/dt_pending',['as'=>'so.dt_pending','uses'=>'AdminSalesOrdersController@dt_pending']);
	Route::post('admin/salesorders/bulkapprove',['as'=>'so.bulkapprove','uses'=>'AdminSalesOrdersController@bulkapprove']);

	Route::get('/admin/checksession',function(){
		dd(Session::all());
	});
	Route::get('/admin/pendingso', ['as'=>'pendingso', 'uses'=>'AdminWellhossubdeptsController@pendingso']);
	Route::get('/admin/lihatisipendingso/{id}', ['as'=>'lihatisiso', 'uses'=>'AdminWellhossubdeptsController@lihatisipendingso']);

	Route::get('/admin/stokopnams/getadddt', ['as'=>'stokopnam.getadddt', 'uses'=>'AdminStokopnamsController@getadddt']);
	Route::post('/admin/stokopnams/postadd', ['as'=>'stokopnam.postadd', 'uses'=>'AdminStokopnamsController@postadd']);

	// mr
	Route::post('/admin/isisubdeptmr/hapus_isi_mr',['as'=>'hapus_isi_mr','uses'=>'AdminWellhossubdeptsController@hapus_isi_mr']);

	Route::get('admin/po/getlistperiode',['as'=>'po.getlistperiode','uses'=>'AdminPurchaseordersController@getlistperiode']);

	// barang
	Route::get('rahasia/barang/trackrecord',['as'=>'barangs.trackrecord','uses'=>'AdminBarangsController@trackrecord']);
	Route::get('admin/barang/stokawal/{id}',['as'=>'barangs.stokawal','uses'=>'AdminBarangsController@stokawal']);
	Route::get('admin/barang/stokawal',['as'=>'barangs.stokawal','uses'=>'AdminBarangsController@stokawal']);
	Route::post('admin/barang/stokawal',['as'=>'barangs.poststokawal','uses'=>'AdminBarangsController@poststokawal']);
	Route::get('admin/barang/hitung_umur_ekonomis',['as'=>'barangs.hitung_umur_ekonomis','uses'=>'AdminBarangsController@hitung_umur_ekonomis']);

	//baranggudang
	Route::get('admin/baranggudang/stokawal/{id}',['as'=>'barangs.stokawal_barang_gudang','uses'=>'AdminBarangsController@stokawal_barang_gudang']);
	Route::get('admin/baranggudang/stokawal',['as'=>'barangs.stokawal_barang_gudang','uses'=>'AdminBarangsController@stokawal_barang_gudang']);

	//returmr
	Route::get('admin/retur_mrs/listmr',['as'=>'retur_mr.listmr','uses'=>'AdminReturMrsController@listmr']);
	Route::get('admin/retur_mrs/listretur',['as'=>'retur_mr.listretur','uses'=>'AdminReturMrsController@listretur']);
	Route::get('admin/returlistisimr/{id}',['as'=>'retur_mr.listisimr','uses'=>'AdminReturMrsController@listisimr']);
	Route::get('admin/returlistisiretur/{id}',['as'=>'retur_mr.listisiretur','uses'=>'AdminReturMrsController@listisiretur']);
	Route::post('admin/retur_mrs/insert_retur',['as'=>'retur_mr.insert_retur','uses'=>'AdminReturMrsController@insert_retur']);


	Route::get('admin/printfaktur/{id}',['as'=>'printfaktur.printfaktur','uses'=>'AdminPurchaseordersController@printfaktur']);

	Route::get('admin/pengiriman/status_kirim',['as'=>'pengiriman.status_kirim','uses'=>'AdminPengirimanBarangs77Controller@pengirimanStatusKirim']);
	Route::get('admin/pengiriman/status_kirim/dt_mr',['as'=>'pengiriman.status_kirim.MR','uses'=>'AdminPengirimanBarangs77Controller@pengirimanStatusKirim_mr']);
	Route::get('admin/pengiriman/status_kirim/dt_so',['as'=>'pengiriman.status_kirim.SO','uses'=>'AdminPengirimanBarangs77Controller@pengirimanStatusKirim_so']);
	Route::get('admin/pengiriman/status_kirim/listpengiriman',['as'=>'pengiriman.status_kirim.listpengiriman','uses'=>'AdminPengirimanBarangs77Controller@pengirimanStatuslistpengiriman']);
});

Route::get('api/masuk/{jenispo}/{kategoribrg}/{input}',['as'=>'api.getmasuk','uses'=>'ApiWellhosController@getmasuk']);
Route::get('api/keluar/{jenispo}/{kategoribrg}/{input}',['as'=>'api.getkeluar','uses'=>'ApiWellhosController@getkeluar']);
Route::get('api/masukbarang/{jenispo}/{kategoribrg}/{input}',['as'=>'api.getkeluar','uses'=>'ApiWellhosController@getmasukdetail']);
Route::get('api/keluarbarang/{jenispo}/{kategoribrg}/{input}',['as'=>'api.getkeluar','uses'=>'ApiWellhosController@getkeluardetail']);
Route::get('api/so/{pilihan}/{input}',['as'=>'api.getjualjohnson','uses'=>'ApiWellhosController@getjualjohnson']);
Route::get('api/getlistperiode/{input}',['as'=>'api.getlistperiode','uses'=>'ApiWellhosController@getlistperiode']);

Route::get('api/getbarang',['as'=>'api.getbarang','uses'=>'ApiWellhosController@getallbarang']);
Route::get('api/getgroup',['as'=>'api.getgroup','uses'=>'ApiWellhosController@getallgroup']);
Route::get('api/getbarangkhusus',['as'=>'api.getallitem','uses'=>'ApiWellhosController@getallitem']);





