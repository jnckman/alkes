<?php

Route::get('/', function () {
    return view('welcome');
});

//Material Request
Route::get('/admin/materialrequest', [ 'uses'=>'AdminBarangsController@materialrequestbyuser']);
// Route::get('/admin/mrmainpage', ['as'=>'mr.mainpage', 'uses'=>'AdminBarangsController@materialrequestmainpage']);
// Route::get('/admin/mrmainpage{id}',['as'=>'mr.getBarangsByKategoris','uses'=>'AdminBarangsController@getBarangsByKategoris']);
// Route::post('/admin/mrmainpage/addCart',['as'=>'mr.addCart','uses'=>'AdminBarangsController@addCart']);
// Route::get('/admin/mrmainpage/cart',['as'=>'mr.getCart','uses'=>'AdminBarangsController@getCart']);
// Route::post('/admin/mrmainpage/postCart',['as'=>'mr.postCart','uses'=>'AdminBarangsController@postCart']);
// Route::delete('/admin/mrmainpage/delCart',['as'=>'mr.delCart','uses'=>'AdminBarangsController@delCart']);
Route::get('/admin/materialrequestsystem', [ 'uses'=>'AdminWellhossubdeptsController@mr']);
Route::get('/admin/mrdtajax', [ 'uses'=>'AdminWellhossubdeptsController@mrdtajax']);
Route::get('/admin/manageshop', [ 'as'=>'manageshop','uses'=>'AdminBarangsController@shop']);

//mr

Route::get('/admin/listdepartments/{id}', [ 'uses'=>'AdminWellhosDepartmentsController@listdepartments']);
Route::get('/admin/listdepartmentsall/{id}', [ 'uses'=>'AdminWellhosDepartmentsController@listdepartmentsall']);
Route::post('/admin/listdepartmentsall/submitrekap', [ 'uses'=>'AdminWellhosDepartmentsController@listdepartmentsallsubmit']);
Route::get('/admin/listdepartmentsdtajax/{id}', [ 'uses'=>'AdminWellhosDepartmentsController@listdepartmentsdtajax']);
Route::get('/admin/listdepartmentsalldtajax/{id}', [ 'uses'=>'AdminWellhosDepartmentsController@listdepartmentsalldtajax']);


Route::get('/admin/materialrequestsystem/{id_periode_mr}/{id_department}', [ 'uses'=>'AdminWellhosSubdeptsController@mr']);
Route::get('/admin/mrdtajax/{id}/{id_listdept}', ['as'=>'mr.ajax', 'uses'=>'AdminWellhossubdeptsController@mrdtajax']);

Route::get('/admin/subdeptmrlist/{id}/{id_listdept}', ['as'=>'getSubdeptmrlist', 'uses'=>'AdminWellhossubdeptsController@subdeptmrlist']);
Route::get('/admin/subdeptmrlistdtajax/{id}/{idperiode}', ['as'=>'dt.subdeptmrlist', 'uses'=>'AdminWellhossubdeptsController@subdeptmrlistdtajax']);


Route::get('/admin/rekapsubdeptmr/{id}/{idperiode}', [ 'uses'=>'AdminWellhossubdeptsController@rekapsubdeptmr']);
Route::get('/admin/rekapsubdeptmrdtajax/{id}/{idperiode}', [ 'uses'=>'AdminWellhossubdeptsController@rekapsubdeptmrdtajax']);

Route::get('/admin/rekapallsubdeptmr/{id}/{id_periode}', [ 'uses'=>'AdminWellhossubdeptsController@rekapallsubdeptmr']);
Route::get('/admin/rekapallsubdeptmrdtajax/{id}/{id_periode}', [ 'uses'=>'AdminWellhossubdeptsController@rekapallsubdeptmrdtajax']);

Route::get('/admin/rekapalldept/{id}', [ 'uses'=>'AdminWellhossubdeptsController@rekapalldept']);
Route::get('/admin/rekapalldeptdtajax/{id}', [ 'uses'=>'AdminWellhossubdeptsController@rekapalldeptdtajax']);

Route::get('/admin/bukukanrekap/{id}', [ 'uses'=>'AdminWellhossubdeptsController@lockingrekap']);
Route::post('/admin/listdepartmentsall/lockingrekaprevisi', [ 'uses'=>'AdminWellhossubdeptsController@lockingrekaprevisi']);
Route::get('/admin/lihatpembukuanrekap/{id}/{tgl}', [ 'as' => 'pembukuan.rekap', 'uses'=>'AdminWellhossubdeptsController@lihatpembukuanrekap']);
Route::get('/admin/lihatpembukuanrekapdtajax/{id}/{tgl}', [ 'uses'=>'AdminWellhossubdeptsController@lihatpembukuanrekapdtajax']);
Route::get('/admin/ambilsupplier/{id}', [ 'uses'=>'AdminWellhossubdeptsController@ambilsupplier']);

// dept A
	//subdept a1,a2,a3

//dept
	//supdept b1,b2,b3
	//b1 p1, b1 p2,b1 p3

// Route::get('/admin/listmr/{id}', [ 'uses'=>'AdminMrDepartments63Controller@mrlist']);
// Route::get('/admin/listmrdtajax/{id}', [ 'uses'=>'AdminMrDepartments63Controller@mrlistdtajax']);

//Route::get('/admin/rekapsubdeptmrdtajax2/{id}', [ 'uses'=>'AdminWellhossubdeptsController@rekapsubdeptmrdtajax2']);

//detail_rek
Route::get('/admin/detailrek/{id}', [ 'uses'=>'AdminCustomers56Controller@detailrek']);
Route::post('/admin/detailrek/submit', [ 'uses'=>'AdminCustomers56Controller@submitrek']);

//detail_alamat
Route::get('/admin/detailalamat/{id}', [ 'uses'=>'AdminCustomers56Controller@detailalamat']);
Route::post('/admin/detailalamat/submit', [ 'uses'=>'AdminCustomers56Controller@submitalamat']);

//reseps
Route::post('/admin/reseps/aksi',['as'=>'reseps.aksi', 'uses'=>'AdminResepsController@postAksi']);
Route::get('/admin/reseps/getGudang_barang/{id}',['as'=>'resep.getGudang_barang', 'uses'=>'AdminResepsController@getGudang_barang']);
Route::get('/admin/reseps/mix/batal',['as'=>'resep.mix.batal','uses'=>'AdminResepsController@getMixBatal']);
Route::get('/admin/reseps/mix/{id}',['as'=>'resep.mix','uses'=>'AdminResepsController@getAksi']);
Route::get('/admin/reseps/mix/del/{id}',['as'=>'resep.mix.del', 'uses'=>'AdminResepsController@delBarang']);

//barang_gudangs
Route::get('/admin/barang_gudangs/split/{id}',['as'=>'barang_gudangs.getsplit','uses'=>'AdminBarangGudangs30Controller@getSplitItem']);
Route::post('/admin/barang_gudangs/split',['as'=>'barang_gudangs.postsplit','uses'=>'AdminBarangGudangs30Controller@postSplitItem']);



require __DIR__.'/route_iyek.php';
require __DIR__.'/route_rase.php';
require __DIR__.'/route_wawan.php';
