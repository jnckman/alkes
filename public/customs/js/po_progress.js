var total=0;
var tableListBarang = $('#tableListBarang').DataTable({
	'processing': true,
	'serverside':true,
	"pageLength": 20,
	"lengthChange": false,
	"searching": false,
  //tes
    // "columnDefs": [
    //         { "visible": false, "targets": [2,3] },
    //     ],
	columns: [
	{ "data": "barang" },
	{ "data": "jumlah" },
  { "data": "total" },
  { "data": "isi_pos_id" },
  { "data": "harga_po" },
  // { "data": "harga_po_revisi" },
  { "data": "tampil" },
  { "data": "action" },
	],
	"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total1 = api .column( 1 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            total2 = api .column( 2 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            total = total1/total2*100;
            // Update footer
            $( api.column( 5 ).footer() ).html(
                total.toLocaleString('en-EN') +' %'
            );
        }
});
function checkProgress(id){
    // url = window.location.href+'/getProgress';
    url = '../admin/purchaseorders/getProgress';
    $.ajax({
        url: url,
        data: {
          "id" : id,
        }
    }).done(function(response){
        // console.log(response);
        tableListBarang.clear().draw();
        tableListBarang.rows.add(response.results).draw();
        /*if(total!=100){
            tableListBarang.column(7).visible(false);
        }
        else{
            tableListBarang.column(7).visible(true);   
        }*/
        $('#modalProgress').modal({backdrop: 'static'}).modal('show');
        // $('#modalProgress').modal('show');
    }).fail(function(){
        swal('Oops','Gagal Mengambil Data','error');
    });
};

$('#modalRevisi, #modalsplit').on('show.bs.modal', function (e) {
        $('#modalProgress').modal('hide');
});
$('.box:eq(1)').find('.box-header').find('br').remove();
$('.box:eq(1)').find('.box-header').append('Periode : <select class="col-xs-12 col-md-4" id="select2periode"><option value="0">===Pilih Periode===</option></select>');
$('.box:eq(1)').find('.box-header').append('<br style="clear:both">');

$('#total_akhir').attr('readonly','true');
$('#diskon').attr('name','diskon2');
$('#diskon').attr('id','diskon2');
$('#form').prepend('<input type="hidden" name="diskon" id="diskon"/>')
total = parseInt($('#total_akhir').val());
var dontkirim = 0;
$(function(){

  //edit PO //////////////////////
  if(($('#diskon2').val())>=0 && $.isNumeric($('#diskon2').val())){
    total += parseInt($('#diskon2').val());
  }
  $('#diskon2').on('input',function(){
    var diskonsku;
    tampung = $(this).val();
    if(tampung.indexOf('%') >= 0){
      tampungconvert = tampung.substr(0, tampung.indexOf('%')); 
      diskonsku = tampungconvert * total/100
      total2 = total -  diskonsku;
    }
    else{
      if($.isNumeric(tampung)==false){
      dontkirim =1;
        return;
      }
      else{
        dontkirim = 0;
      }
      diskonsku = tampung;
      total2 = total - diskonsku;
    }
    $('#diskon').val(diskonsku);
    $('#total_akhir').val(total2);
  });

  $('#form').on('submit',function(event){
    if(dontkirim==1){
      event.preventDefault();
      alert('Format Diskon tidak sesuai !');
    }
  })
  ///////////////////////


  $('#select2periode').select2({
    placeholder:'Filter Periode'
  });

  $.ajax({
    url : 'po/getlistperiode'
  }).done(function(response){
    var data = $("#select2periode").select2('data');
    for(i=0;i<response.length;i++){
      var newOption = new Option(response[i].text, response[i].id, false, false);
      $('#select2periode').append(newOption);
    }
  }).fail(function(){
    console.log('Gagal mengambil periode');
  });

  $(document).on('change','#select2periode',function(){
    if($("#select2periode").val() != 0){
      $('input[name="q"]').val($('#select2periode option:selected').text());
      $('input[name="q"]').parent().parent().submit();
    }
  });

  $('#collapse-bantuan').trigger('click');
    var modalRevisi;
    $('#select2supplier').select2({
        placeholder:'Pilih Supplier',
        // data : suppliers,
        ajax: {
         url: 'selectsupplier',
         processResults: function (data) {
            return {
              results: data
            };
         }
        },
    });
    tableListBarang.column(1).visible(false);
    tableListBarang.column(2).visible(false);;
    tableListBarang.column(3).visible(false);;

    $(document).on('click','.btnRevisi',function(event){
        var tr = $(this).parent().parent();
        var data = tableListBarang.row(tr).data();
        // console.log(data);
        modalRevisi = $('#modalRevisi');
        setmodalku(modalRevisi,tr,data);
    });

    $(document).on('click','.btnSplit',function(event){
        var tr = $(this).parent().parent();
        var data = tableListBarang.row(tr).data();
        modalRevisi = $('#modalsplit');
        setmodalku(modalRevisi,tr,data,1);
    });

    function setmodalku(modalRevisi,tr,data,issplit=0){
        modalRevisi.find('input[name="harga_baru"]').val('');
        modalRevisi.find('textarea[name="keterangan"]').val('');
        modalRevisi.find('input[name="isi_pos_id"]').val(data.isi_pos_id);
        modalRevisi.find('input[name="namaBarang"]').val(data.barang);
        modalRevisi.find('input[name="harga_po"]').val(data.harga_po);
        modalRevisi.find('input[name="jumlah_lama"]').val(data.total);
        if(issplit=1){
            modalRevisi.find('input[name="split_jumlah"]').attr('max',data.total);
        }
        $('#modalProgress').modal('hide');
        modalRevisi.modal({backdrop: 'static'}).modal('show');
    }

    $('.formRevisi').submit(function(event){
        // $(document).on('submit','#formRevisi',function(event){
        event.preventDefault();
        // alert('mensubmit');
        $.ajax({
            type: 'post',
            url : 'revisipo',
            data : $(this).serialize(),
        }).success(function(response){
            console.log(response);
            if(response=='berhasil'){
                modalRevisi.modal('hide');
                $('#modalProgress').modal('hide');
                swal("Success!", "Berhasil Mengajukan Revisi!", "success")
            }
            else{
                swal('Oops','Gagal Mengirim Data','error');    
            }
        }).fail(function(){
            swal('Oops','Terjadi Kesalahan','error');
        });
    });
    $("[data-toggle='tooltip']").tooltip();
})

// $(".btn_progress_po").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'kedatangan').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".btn_isi_po").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'isi po').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".btn_buat_spbm").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'buat spbm').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".btn_list_spbm").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'list spbm').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".btn_print").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'print rekap po').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".approve-1").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'approve-1 po').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".approve-2").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'approve-2 po').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });
// $(".btnbayar").parent().hover(function() {
//     $(this).css('cursor','pointer').attr('title', 'timeline bayar').attr('data-toggle', 'tooltip').attr('data-placement', 'bottom');
// }, function() {
//     $(this).css('cursor','auto');
// });

$('.button_action').find('a').wrap('<li class="pindah"></li>')
dropdownActionku();
function dropdownActionku(){
  $(".button_action").parent().append('<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action<span class="caret"></span></button><ul class="dropdown-menu dropaction" aria-labelledby="dropdownMenu1">');
  var i=0;
  $(".pindah").each(function(){
    $(this).appendTo($(this).parent().parent().find('.dropaction'));
    // $(this).find('a').append($(this).find('a').attr('title'));
    $(this).find('a').removeClass();
  });
}