<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Satuan extends Model
{
   use SoftDeletes;

	protected $table = 'satuans';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang() {
		return $this->hasMany('App\Models\Barang','satuans_id');
	}
}
