<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
   use SoftDeletes;

	protected $table = 'projects';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function mr_subdept(){
		return $this->hasMany('App\Models\Mr_subdept','projects_id');
	}
}
