<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wellhos_department extends Model
{
   use SoftDeletes;

	protected $table = 'wellhos_departments';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function mr_subdept(){
		return $this->hasManyThrough(
				'\App\Models\Mr_subdept','App\Models\Wellhos_subdept',
				'wellhos_departments_id','wellhos_subdepts_id','id'
				);
	}

	public function wellhos_subdept(){
		return $this->hasMany('App\Models\Wellhos_subdept','wellhos_departments_id');
	}
}
