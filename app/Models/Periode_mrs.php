<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Periode_mrs extends Model
{
   use SoftDeletes;

	protected $table = 'periode_mrs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function mr_subdept(){
		return $this->hasMany('App\Models\Mr_subdept','periode_mrs_id');
	}

	public function purchaseorder(){
		return $this->hasMany('App\Models\Purchaseorder','periode_mrs_id');
	}
}