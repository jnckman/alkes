<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Returso extends Model
{
   use SoftDeletes;

	protected $table = 'returso';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function isi_returso() {
		return $this->hasMany('App\Models\Isi_returso','returso_id');
	}

}
