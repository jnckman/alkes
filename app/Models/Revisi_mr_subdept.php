<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Revisi_mr_subdept extends Model
{
   use SoftDeletes;

	protected $table = 'revisi_mr_subdepts';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function isi_mr_subdept(){
		return $this->belongsTo('App\Models\Isi_mr_subdept','isi_mr_id');
	}
		
	public function user(){
		return $this->belongsTo('App\Models\Cms_user','user_id');
	}
}
