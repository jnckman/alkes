<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wellhos_subdept extends Model
{
   use SoftDeletes;

	protected $table = 'wellhos_subdepts';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function mr_subdept(){
		return $this->hasMany('App\Models\Mr_subdept','wellhos_subdepts_id');
	}

	public function mr_subdeptperiode($id){
		return $this->hasMany('App\Models\Mr_subdept','wellhos_subdepts_id')->where('periode_mrs_id',$id)->where('selesai',0);	
	}


}
