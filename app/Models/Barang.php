<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang extends Model
{
   use SoftDeletes;

	protected $table = 'barangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function Isispbm(){
		return $this->hasOne('App\Models\Isispbm');
	}

	public function kategori() {
		return $this->belongsTo('App\Models\Kategori','kategoris_id');
	}

	public function group() {
		return $this->belongsTo('App\Models\Group', 'group');
	}

	public function isi_mr_subdept() {
		return $this->hasMany('App\Models\isi_mr_subdept','barangs_id');
	}

	public function gudang_barang(){
		return $this->belongsToMany('App\Models\Gudang','barang_gudangs','barangs_id','gudangs_id');
	}

	public function harga_jual(){
		return $this->hasMany('App\Models\Harga_jual','barang_id');
	}

	public function satuan(){
		return $this->belongsTo('App\Models\Satuan','satuans_id');	
	}

	public function isi_po(){
		return $this->hasMany('App\Models\Isi_po','barangs_id');
	}

	public function diskon(){
		return $this->hasMany('App\Models\Diskon','barangs_id')->where('tgl_mulai','<=',date('Y-m-d'))
															->where('tgl_selesai','>=',date('Y-m-d'))
																;
	}

	public function detail_pengambilan_barang(){
		return $this->hasMany('App\Models\Detail_Pengambilan_Barang','barang_id');
	}
	public function isispbmr(){
		return $this->hasMany('App\Models\Isispbm','barangs_id');
	}

	public function umur(){
		return $this->hasOne('App\Models\Umur_ekonomis','barangs_id');
	}
}
