<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Cms_user extends Model
{
   // use SoftDeletes;

	protected $table = 'cms_users';
	protected $hidden = [

    ];

	protected $guarded = [];
	// protected $softDelete = false;

	public function mr_subdept(){
		return $this->hasMany('App\Models\Mr_subdept','user_id');
	}

	public function privilege(){
		return $this->belongsTo('App\Models\Cms_privilege','id_cms_privileges');
	}
}
