<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mr_subdept extends Model
{
   use SoftDeletes;

	protected $table = 'mr_subdepts';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function pengiriman_barang(){
		return $this->hasMany('App\Models\Pengiriman_Barang','global_id')->where('pengiriman_barangs.jenis','=','mr_baru');
	}

	public function project(){
		return $this->belongsTo('App\Models\Project','projects_id');
	}

	public function periode_mrs(){
		return $this->belongsTo('App\Models\Periode_mrs','periode_mrs_id');	
	}
	public function isi_mr_subdept(){
		return $this->hasMany('App\Models\Isi_mr_subdept','mr_subdepts_id');
	}
	public function isi_mr(){
		return $this->hasMany('App\Models\Isi_mr_subdept','mr_subdepts_id');	
	}
	public function user(){
		return $this->belongsTo('App\Models\Cms_user','user_id');
	}

	public function detail_pengiriman_barang(){
		return $this->hasManyThrough('App\Models\Detail_Pengiriman_Barang','App\Models\Pengiriman_Barang'
									,'global_id','pengiriman_barang_id','id'
									)
									->where('pengiriman_barangs.jenis','=','mr_baru');
	}
	public function revisi_mr_subdept(){
		return $this->hasManyThrough('App\Models\Revisi_mr_subdept','App\Models\Isi_mr_subdept'
									,'mr_subdepts_id','isi_mr_id','id'
									)
									;
	}
}
