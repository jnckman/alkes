<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barangdatang extends Model
{
   use SoftDeletes;

	protected $table = 'barangdatangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function Isispbm(){
	//	dd($this->hasMany('App\Models\Isispbm')->leftJoin('App\Model\Supplier'));
		return $this->hasMany('App\Models\Isispbm','barangdatangs_id');
	}
	
	public function Supplier(){
		return $this->belongsTo('App\Models\Supplier','suppliers_id');
	}

	public function Purchaseorder(){
		return $this->belongsTo('App\Models\Purchaseorder','purchaseorders_id');
	}

	public function Bukuhutang(){
		return $this->hasMany('App\Models\Bukuhutuang','barangdatangs_id');
	}
}


