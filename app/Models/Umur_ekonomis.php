<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Umur_ekonomis extends Model
{
   // use SoftDeletes;

	protected $table = 'Umur_ekonomis';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		$this->belongsTo('App\Models\Barang','barangs_id');
	}
}
