<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales_order extends Model
{
   use SoftDeletes;

	protected $table = 'sales_orders';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function isi_sales_order(){
		return $this->hasMany('App\Models\Isi_sales_order','sales_orders_id');
	}

	public function customer(){
		return $this->belongsTo('App\Models\Customer','customers_id');	
	}
}	
