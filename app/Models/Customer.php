<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
   use SoftDeletes;

	protected $table = 'customers';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function alamat(){
		return $this->hasMany('App\Models\Alamat','customers_id');
	}

	public function rekening(){
		return $this->hasMany('App\Models\Bank','customers_id');
	}
}	
