<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Assign_user extends Model
{
   // use SoftDeletes;

	protected $table = 'assign_users';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

}
