<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengambilan_Barang extends Model
{
   use SoftDeletes;

	protected $table = 'pengambilan_barangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function detail_pengambilan_barang(){
		return $this->hasMany('App\Models\Detail_Pengambilan_Barang','pengambilan_barang_id');
	}
}
