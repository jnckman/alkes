<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Revisi_po extends Model
{
   use SoftDeletes;

	protected $table = 'revisi_pos';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function isi_pos(){
		return $this->belongsTo('App\Models\Isi_po','isi_pos_id');
	}
}
