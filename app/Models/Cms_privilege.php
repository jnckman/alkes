<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Cms_privilege extends Model
{
   // use SoftDeletes;

	protected $table = 'cms_privileges';
	protected $hidden = [

    ];

	protected $guarded = [];
	// protected $softDelete = false;
}
