<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lock_rekap extends Model
{
   use SoftDeletes;

	protected $table = 'lock_rekaps';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barangs_id');
	}

	public function pricelist(){
		return $this->belongsTo('App\Models\Pricelist','pricelists_id');
	}
}
