<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Harga_jual extends Model
{
   use SoftDeletes;

	protected $table = 'harga_juals';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barang_id');
	}
}
