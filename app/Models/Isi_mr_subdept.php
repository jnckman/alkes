<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isi_mr_subdept extends Model
{
   	use SoftDeletes;

	protected $table = 'isi_mr_subdepts';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang() {
		return $this->belongsTo('\App\Models\Barang','barangs_id');
	}

	public function detail_pengiriman_barang(){
		return $this->hasManyThrough('\App\Models\Detail_Pengiriman_Barang','App\Models\Pengiriman_Barang'
									,'global_id','pengiriman_barang_id','mr_subdepts_id')
									->where('pengiriman_barangs.jenis','=','mr_baru')
									;
	}

	public function mr_subdept(){
		return $this->belongsTo('App\Models\Mr_subdept','mr_subdepts_id');
	}

	public function back_mr_subdept(){
		return $this->belongsTo('App\Models\Mr_subdept','mr_subdepts_id');
	}
}
