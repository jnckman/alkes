<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isi_po extends Model
{
   use SoftDeletes;

	protected $table = 'isi_pos';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];
	
	public function getJumlahpesan($poid,$barangid){
		return $this->where('barangs_id',$barangid)->where('purchaseorders_id',$poid)->get();
	}

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barangs_id');
	}

	public function revisi(){
		return $this->hasMany('App\Models\Revisi_po','isi_pos_id');
	}

	public function purchaseorder(){
		return $this->belongsTo('App\Models\Purchaseorder','purchaseorders_id');
	}

	public function tes($id){
		$data = Revisi_po::where('isi_pos_id',$id)->orderBy('id','desc')->limit(1)->get();
		return $data;
	}
	


}
