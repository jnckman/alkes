<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail_Pengiriman_Barang extends Model
{
   use SoftDeletes;

	protected $table = 'detail_pengiriman_barangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barang_id');
	}
}
