<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gudang extends Model
{
   use SoftDeletes;

	protected $table = 'gudangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->belongsToMany('App\Models\Barang','barang_gudangs','gudangs_id','barangs_id');
	}

	public function stokopnam(){
		return $this->hasMany('App\Models\Stokopnam','gudangs_id');
	}

	public function cariBarang($request){
		return $this->belongsToMany('App\Models\Barang','barang_gudangs','gudangs_id','barangs_id')
						->where('barangs.nama','like', '%'.$request.'%')
						// ->orWhere('barangs.kode','like', '%'.$request.'%')
						->whereNull('barangs.deleted_at')
						;
	}

	public function pengiriman_barang(){
		return $this->hasManyThrough('App\Models\Pengiriman_barang','App\Models\Pengambilan_Barang'
									,'gudang_id','pengambilan_barang_id','id'
									);
	}

	public function pengambilan_barang(){
		return $this->hasMany('App\Models\Pengambilan_Barang','gudang_id');
	}

	public function detail_pengambilan_barang(){
		return $this->hasManyThrough('App\Models\Detail_Pengambilan_Barang','App\Models\Pengambilan_Barang'
									,'gudang_id','pengambilan_barang_id','id')
									;
	}	
}
