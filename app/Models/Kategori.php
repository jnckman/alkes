<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kategori extends Model
{
   use SoftDeletes;

	protected $table = 'kategoris';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->hasMany('App\Models\Barang','kategoris_id');
	}

	public function isi_mr_subdept(){
		return $this->hasManyThrough(
			'App\Models\Isi_mr_subdept','App\Models\Barang',
			'kategoris_id','barangs_id','id'
		);
	}
}
