<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang_gudang extends Model
{
   use SoftDeletes;

	protected $table = 'barang_gudangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function gudang(){
		return $this->belongsTo('App\Models\Gudang','gudangs_id');
	}

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barangs_id');
	}
}
