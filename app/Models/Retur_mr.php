<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Retur_mr extends Model
{
   // use SoftDeletes;

	protected $table = 'retur_mrs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

}
