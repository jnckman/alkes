<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alamat extends Model
{
   use SoftDeletes;

	protected $table = 'alamats';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function customer(){
		return $this->belongsTo('App\Models\Customer','customers_id');
	}
}
