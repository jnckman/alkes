<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isi_returso extends Model
{
   use SoftDeletes;

	protected $table = 'isi_returso';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function isi_sales_order(){
		return $this->belongsTo('App\Models\Isi_sales_order','isi_sales_order_id');
	}
}
