<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isi_sales_order extends Model
{
   	use SoftDeletes;

	protected $table = 'isi_sales_orders';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barang(){
		return $this->belongsTo('App\Models\Barang','barangs_id');
	}

	public function sales_order(){
		return $this->belongsTo('App\Models\Sales_order','sales_orders_id');
	}
}
