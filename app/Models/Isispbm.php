<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isispbm extends Model
{
   use SoftDeletes;

	protected $table = 'isispbms';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function Barangdatangs(){

		return $this->belongsTo('barangdatangs')->leftJoin('App\Models\Suppliers');
	}

	public function barangdatang(){
		return $this->belongsTo('App\Models\Barangdatang','barangdatangs_id');
	}

	public function Barangs(){

		return $this->belongsTo('App\Models\Barang');
	}

	public function Barangs_barangdatang(){

		return $this->belongsTo('App\Models\Barang','barangs_id')->select('nama','kode');
	}

	public function getJumlahpesanan($poid,$barangid){
		//dd($poid);
		
		return $this->belongsTo('App\Models\Isi_po')->where('barangs_id',$barangid)->where('purchaseorders_id',$poid)->get();

	}
	public function getJumlahpesananbaru($poid,$barangid){
		//dd($poid);
		
		return Isi_po::where('barangs_id',$barangid)->where('purchaseorders_id',$poid)->get();

	}

}
