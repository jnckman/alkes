<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengiriman_Barang extends Model
{
   use SoftDeletes;

	protected $table = 'pengiriman_barangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function detail_pengiriman_barang(){
		return $this->hasMany('App\Models\Detail_Pengiriman_Barang','pengiriman_barang_id');
	}
}
