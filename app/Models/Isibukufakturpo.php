<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Isibukufakturpo extends Model
{
   use SoftDeletes;

	protected $table = 'isibukufakturpos';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	// public function Isispbm(){
	// //	dd($this->hasMany('App\Models\Isispbm')->leftJoin('App\Model\Supplier'));
	// 	return $this->hasMany('App\Models\Isispbm','barangdatangs_id');
	// }
	
	// public function Supplier(){
	// 	return $this->belongsTo('App\Models\Supplier','suppliers_id');
	// }

	public function Bukufakturpo(){
		return $this->belongsTo('App\Models\Bukufakturpo','bukufakturpos_id');
	}


}


