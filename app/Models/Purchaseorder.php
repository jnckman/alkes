<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchaseorder extends Model
{
   use SoftDeletes;

	protected $table = 'purchaseorders';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];
//oke
	public function Isi_po($id){
		//dd($id);
		return $this->hasMany('App\Models\Isi_po','purchaseorders_id')->where('barangs_id',$id)->get();
	}
	public function Isi_popo(){
		//dd($id);
		return $this->hasMany('App\Models\Isi_po','purchaseorders_id');
	}

	public function isi(){
		return $this->hasMany('App\Models\Isi_po','purchaseorders_id');
	}

	public function Barangdatangs(){
		return $this->hasMany('App\Models\Barangdatang','purchaseorders_id');
	}

	public function revisi_po(){
		return $this->hasManyThrough('App\Models\Revisi_po','App\Models\Isi_po'
									,'purchaseorders_id','isi_pos_id','id'
									);
	}

	public function isispbm(){
		return $this->hasManyThrough('App\Models\Isispbm','App\Models\Barangdatang'
									,'purchaseorders_id','barangdatangs_id','id'
									);
	}

	public function supplier(){
		return $this->belongsTo('App\Models\Supplier','customers_id');
	}

	public function Bukufakturpo(){
		return $this->hasMany('App\Models\Bukufakturpo','purchaseorders_id');
	}

}
