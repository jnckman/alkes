<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bukuhutang extends Model
{
   use SoftDeletes;

	protected $table = 'bukuhutangs';

	protected $hidden = [

    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function barangdatang(){
		//dd($id);
		return $this->belongsTo('App\Models\Barangdatang','barangdatangs_id');
	}


}
