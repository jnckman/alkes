<?php
namespace App;

Class Cart {
	public $items = null;
	// public $qnty;

	public function __construct($oldCart) {
		if($oldCart) {
			$this->items = $oldCart->$items;
		}
	}

	public function add($item,$id,$jmlh){
		$storedItem = ['item'=>$item,'jmlh'=>$jmlh];
		if($this->item) {
			if(array_key_exists($id, $this->items)) {
				// $storedItem = $this->item[$id];
				return 0;
			}
		}

		$this->items[$id] = $storedItem;
		return 1;
	}
}
