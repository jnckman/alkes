<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Pricelist;
	use App\Models\Barang;
	use App\Models\Barang_gudang;
	use App\Models\Supplier;
	use Maatwebsite\Excel\Facades\Excel;
	use DataTables;
	// use Illuminate\Http\Request as HttpReq;
	use Storage;
	use Hash;

	class AdminBarangsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_dropdown";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = true;
			$this->button_export = false;
			$this->table = "barangs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-6','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Jenis','name'=>'jenis','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Tipe','name'=>'tipe','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Kategori','name'=>'kategoris_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-6','datatable'=>'kategoris,nama'];
			$this->form[] = ['label'=>'Ukuran','name'=>'ukuran','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Min','name'=>'min','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Max','name'=>'max','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Akumstok','name'=>'akumstok','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Satuan','name'=>'satuans_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-6','datatable'=>'satuans,nama'];
			$this->form[] = ['label'=>'Avgprice','name'=>'avgprice','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Ppn','name'=>'ppn','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Harga Beli Akhir','name'=>'harga_beli_akhir','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Harga Jual','name'=>'harga_jual','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Cara Pakai','name'=>'cara_pakai','type'=>'textarea','validation'=>'required','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Spesifikasi','name'=>'spesifikasi','type'=>'textarea','validation'=>'required','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Video','name'=>'video','type'=>'text','validation'=>'required','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Gambar','name'=>'gambar','type'=>'upload','validation'=>'required|image|max:3000','width'=>'col-sm-6'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-6','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Jenis','name'=>'jenis','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Tipe','name'=>'tipe','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Kategori','name'=>'kategoris_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-6','datatable'=>'kategoris,nama'];
			//$this->form[] = ['label'=>'Ukuran','name'=>'ukuran','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Min','name'=>'min','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Max','name'=>'max','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Akumstok','name'=>'akumstok','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Satuan','name'=>'satuans_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-6','datatable'=>'satuans,nama'];
			//$this->form[] = ['label'=>'Avgprice','name'=>'avgprice','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Ppn','name'=>'ppn','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Harga Beli Akhir','name'=>'harga_beli_akhir','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Harga Jual','name'=>'harga_jual','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Cara Pakai','name'=>'cara_pakai','type'=>'textarea','validation'=>'required','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Spesifikasi','name'=>'spesifikasi','type'=>'textarea','validation'=>'required','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Video','name'=>'video','type'=>'text','validation'=>'required','width'=>'col-sm-6'];
			//$this->form[] = ['label'=>'Gambar','name'=>'gambar','type'=>'upload','validation'=>'required|image|max:3000','width'=>'col-sm-6'];
			# OLD END FORM

			$this->sub_module = array();
	      $this->addaction = array();
	      $this->button_selected = array();
	      $this->alert        = array();
	      $this->index_button = array();
	      $this->table_row_color = array();
	      $this->index_statistic = array();
	      $this->script_js = NULL;
	      $this->pre_index_html = null;
	      $this->post_index_html = null;
	      $this->load_js = array();
	      $this->style_css = '
	      .btn_dropku{
	      	margin-left: -20px;
	      	margin-right: 7px;
	      }';
	      $this->load_css = array();
	    }

	   public function actionButtonSelected($id_selected,$button_name) {}
	   public function hook_query_index(&$query) {}
	   public function hook_row_index($column_index,&$column_value) {}
	   public function hook_before_add(&$postdata) {}
	   public function hook_after_add($id) {}
	   public function hook_before_edit(&$postdata,$id) {}
	   public function hook_after_edit($id) {}
	   public function hook_before_delete($id) {}
	   public function hook_after_delete($id) {
	   	DB::table('barang_gudangs')->where('barangs_id',$id)->whereNull('deleted_at')->delete();
	   }

		public function shop(){
 		   $data = [];
 		   $data['page_title'] = 'Products Data';
 		   $data['result'] = DB::table('barangs')
 									->whereNull('deleted_at')
 									->orderby('id','desc')
 									->paginate(15);

 	    	$this->cbView('customView.shop',$data);
 	   }

		public function getIndex() { //NOTE utk customView
  	 		//First, Add an auth
	   	if(!CRUDBooster::isView()) CRUDBooster::denyAccess();

		   $data = [];
		   $data['page_title'] = 'Barang';
			$data['suppliers'] = Supplier::select('id', 'nama')->whereNull('deleted_at')->get();
			// dd($data['suppliers']);
		   $this->cbView('customView.barang',$data);
		}

		public function ajax_pricelist() {
			$id2 = Request::get('id');
			$data = DB::table('pricelists')
					->leftJoin('suppliers', 'pricelists.suppliers_id', '=', 'suppliers.id')
					->where('pricelists.barangs_id', $id2)
					->whereNull('pricelists.deleted_at')
					->get();
			return $data;
		}

		public function getAdd() {
			$data = [];
		   $data['page_title'] = 'Add New Barang';
			$data['kategori'] = DB::table('kategoris')->select('id', 'nama')->whereNull('deleted_at')->get();
			$data['satuan'] = DB::table('satuans')->select('id', 'nama')->whereNull('deleted_at')->get();
			$this->cbView('customView.barang_add_new',$data);
		}

		public function add_barang() {
			$data = Request::all();
			// dd($data);
			$new_id = DB::table('barangs')->orderBy('id', 'desc')->first();
			$path = Storage::disk('local')->putFile("uploads/barang", $data["gambar"]);
			// dd($new_id);
			$new = new Barang;
			$new->nama = $data['nama'];
			$new->jenis = $data['jenis'];
			$new->tipe = $data['tipe'];
			$new->ukuran = $data['ukuran'];
			$new->status = $data['status'];
			$new->avgprice = $data['avg_price'];
			$new->min = $data['min'];
			$new->max = $data['max'];
			$new->ppn = $data['ppn'];
			$new->harga_beli_akhir = $data['harga_beli'];
			$new->harga_jual = $data['harga_jual'];
			if($data['kode']==''){
				$idbaru = (int)$new_id->id +1;
				$new->kode = "b-" . $idbaru;
			}
			else{
				$new->kode = $data['kode'];
			}
			$new->satuans_id = $data['satuan'];
			$new->kategoris_id = $data['kategori'];
			$new->cara_pakai = $data['cp'];
			$new->spesifikasi = $data['spek'];
			$new->gambar = $path;
			$new->video = $data['video'];
			$new->batch = $data['batch'];
			$new->batch = $data['expired'];
			$new->save();

			return redirect(CRUDBooster::mainpath());
		}

		public function getEdit($id) {
			$data = [];
		   $data['page_title'] = 'Edit Barang';
			$data['barang'] = DB::table('barangs')->where('id', '=', $id)->first();
			$data['kategori'] = DB::table('kategoris')->select('id', 'nama')->whereNull('deleted_at')->get();
			$data['satuan'] = DB::table('satuans')->select('id', 'nama')->whereNull('deleted_at')->get();
			$data['hargajual'] = DB::table('pricelists')
			->leftJoin('barangs','barangs.id','=','pricelists.barangs_id')
			->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
			->select('pricelists.id','pricelists.nama','pricelists.harga','barangs.priclists_id','suppliers.nama as namasup')
			->where('barangs_id',$id)->whereNull('pricelists.deleted_at')->get();
			
		 
			$this->cbView('customView.barang_edit',$data);
		}

		public function edit_barang() {
			$data = Request::except('_token');
			$id = $data['id'];
			unset($data['id']);
			$data = array_filter($data, function($value) { return $value !== null; });
			// dd($data);
			if ($data['gambar']){
				$path = Storage::disk('local')->putFile("uploads/barang", $data["gambar"]);
				$data['gambar'] = $path;
			}
			

			$simpan = DB::table('barangs')->where('id', $id)->update($data);

			$harga = DB::table('pricelists')->where('id',$data['priclists_id'])->get()[0]->harga;
			$simpan2 = DB::table('barangs')->where('id', $id)->update(['harga_beli_akhir'=>$harga]);
			
			return redirect(CRUDBooster::mainpath());
		}

		public function getDetail($id) {
	  		//Create an Auth
	  		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
	    		CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
	  		}
			// dd($id);
	  		$data = [];
	  		$data['page_title'] = 'Detail Barang';
	  		$data['row'] = DB::table('barangs')->where('barangs.id',$id)
						->leftJoin('satuans', 'barangs.satuans_id', '=', 'satuans.id')
						->leftJoin('kategoris', 'barangs.kategoris_id', '=', 'kategoris.id')
						->select('barangs.id',
									'barangs.kode',
									'barangs.nama',
									'barangs.jenis',
									'barangs.tipe',
									'barangs.ukuran',
									'barangs.min',
									'barangs.max',
									'barangs.akumstok',
									'barangs.status',
									'barangs.ppn',
									'barangs.harga_beli_akhir',
									'barangs.harga_jual',
									'barangs.gambar',
									'barangs.avgprice',
									'barangs.spesifikasi',
									'barangs.video',
									'barangs.cara_pakai',
									'satuans.nama as satuan',
									'kategoris.nama as kategori')
						->first();
			// dd($data);
	  		$this->cbView('customView.barang_detail',$data);
		}

		public function gudang_ajax() {
			$path = CRUDBooster::mainpath();
			$hasil = DB::table('barangs')->whereNull('deleted_at')->get();
			foreach($hasil as $d){
			  	$data[] = [	$d->nama,
						  		$d->avgprice,
						  		$d->hj,
						  		$d->email,
							  	'<a class="btn btn-xs btn-success" style="margin-right:5px;" title="Edit" href="'.$path.'/edit/'.$d->id.'"><i class="fa fa-pencil"></i></a>'.
								'<button type="button" class="btn btn-info btn-xs" style="margin-right:5px;" title="Lihat Barang" onClick="lihatBarang('.$d->id.')"><i class="fa fa-bars"></i></button>'
						  	];
			}
			$results = [
							"draw" => 1,
							"recordsTotal" => count($data),
							"recordsFiltered" => count($data),
							"data" => $data
						];
			echo json_encode($results);
		}

		public function dt_master() {
			$ggg = Barang::whereNull('barangs.deleted_at')
						->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
						->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
						->leftJoin('pricelists','pricelists.id','=','barangs.priclists_id')
						// ->sum()
						->select('kategoris.nama as namaKategori','satuans.nama as namaSatuan','barangs.*','pricelists.harga as harga_beli_akhir')
						// ->get()
						;
			$data = $ggg->get();
			// dd($data);
			return DataTables::of($data)
					->addColumn('action', function ($data) { //untuk ambil jam saja
						$action = '
<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Dropdown
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
  ';
						if(CRUDBooster::isUpdate()){
							if(CRUDBooster::isSuperadmin()==true){
								$action .='
								<li><a href="'.CRUDBooster::mainpath('edit/'.$data->id).'"><button class="btn btn-success btn-xs btn_dropku" title="Edit" type="button"><i class="fa fa-pencil"></i></button>Edit</a></li>
								<li><a href="'.CRUDBooster::mainpath('detail/'.$data->id).'"><button type="button" class="btn btn-primary btn-xs btn_dropku" title="Detail" ><i class="fa fa-eye"></i></button>Detail</a></li>
								<li><a href="#" onClick="showModalPriceList('.$data->id.')"><button type="button" class="btn btn-info btn-xs btn_dropku" title="Price List" ><i class="fa fa-bars"></i></button>Price List</a></li>
								<li><a href="#" onClick="addPriceBarang('.$data->id.')"><button type="button" style="width: 23px;" class="btn btn-warning btn-xs btn_dropku" title="Add Pricelist" ><i class="fa fa-usd"></i></button>Add Pricelist</a></li>
								<li><a href="'.CRUDBooster::adminPath('harga_juals/barang/'.$data->id).'"><button class="btn btn-success btn-xs btn_dropku" title="Harga Jual" type="button"><i class="fa fa-shopping-cart"></i></button>Harga Jual</a></li>
								<li><a href="#" onClick="row=this;trackrecord('.$data->id.')"><button type="button" class="btn btn-info btn-xs btn_dropku" title="Trackrecord"><i class="fa fa-history"></i></button>Trackrecord</a></li>
								<li><a href="#" onClick="deleteBarang('.$data->id.')"><button type="button" class="btn btn-danger btn-xs btn_dropku" title="Delete" ><i class="fa fa-trash"></i></button>Delete</a></li>';
								//<a class="btn btn-danger btn-xs" title="Delete" href="'.route('delete.barang',['id' => $data->id]).'"><i class="fa fa-trash"></i></a>
							}
							else if(CRUDBooster::myPrivilegename()=="PO-2"){
								$action .='
								<a href="'.CRUDBooster::mainpath('edit/'.$data->id).'"><button class="btn btn-success btn-xs btn_dropku" title="Edit" type="button"><i class="fa fa-pencil"></i></button></a>';
							}
							$action .='</ul></div>';
							return $action;
						}
					})
					->addColumn('gegewepe',number_format($ggg->sum(DB::raw('akumstok * avgprice'))) )
					->make(true);
		}

		public function delete_barang($id) {
			Barang::where('id', $id)->delete();
			DB::table('barang_gudangs')->where('barangs_id',$id)->whereNull('deleted_at')->delete();
			CRUDBooster::redirect(CRUDBooster::adminPath('barangs'), 'Delete Berhasil !!!','success');
		}

		public function add_pricelist() {
			$data = Request::all();
			// dd($data);
			$tgl = date("Ymd");
			$nama_pricelist = $data["harga_p"]."-".$data["nama_supplier"]."-".$data["id_barang"]."-".$tgl;
			// dd($nama_pricelist);
			$p = new Pricelist;
			$p->nama = $nama_pricelist;
			$p->harga = $data["harga_p"];
			$p->barangs_id =$data["id_barang"];
			$p->suppliers_id = $data["supplier"];
			$p->save();
			// session(['status_addprice' => true]);
			CRUDBooster::redirect(CRUDBooster::adminPath('barangs'), 'Pricelist Berhasil di tambah !!!', 'success');
		}

		public function addhargarata(){
			//tambah yg blm ad harga rata
			$user = DB::table('cms_users')->where('id',CRUDBooster::myId())->first();
			// dd($user);
			$request = request();
			if (Hash::check($request->pass, $user->password)) {
				$barang1 = Barang::
								// ->
								whereNull('deleted_at')
								->where('avgprice','=',0)
								->orWhere('avgprice',null)
								// ->whereNotNull('harga_beli_akhir')
								->get()
								;
								// dd($barang1);
				// $i = 0;
				foreach ($barang1 as $k => $b) {
					$i++;
					if($b->harga_beli_akhir == null ){
						// echo $i;
						// dd($b);
						// dd('ada null');
						$po = $b->isi_po->last();
						if(isset($po)){
							// dd($po);
							$b->update(['harga_beli_akhir'=>$po->harga,'avgprice'=>$po->harga]);
						}
					}
					else{
						$b->update(['avgprice'=>$b->harga_beli_akhir]);
					}
				}
				dd('done');
			}
			dd('ea');
			
		}

		public function hitung_umur_ekonomis($id=0){
			if($id==0){
				$barang = Barang::join('umur_ekonomis','umur_ekonomis.barangs_id','=','barangs.id')
								// ->update(['updated_at'=>now()])
								->get();
								;
								dd($barang);
			}else{
				$barang = Barang::find($id);
			}

		}

		public function trackrecord($id=0){
			$request = request();
			if($id == 0){
				$id = $request->id;
			}
			$datang = DB::table("isispbms")
						->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
						->leftJoin('purchaseorders','purchaseorders.id','=','barangdatangs.purchaseorders_id')
						->leftJoin('isi_pos',function($q){
							$q->on('isi_pos.purchaseorders_id','=','purchaseorders.id');
							// $q->on('isi_pos.barangs_id','=','isispbms.barangs_id');
						})
						->whereNull('barangdatangs.deleted_at')
						->whereNull('isispbms.deleted_at')
						->where('isispbms.barangs_id',$id)
						->where('isi_pos.barangs_id',$id)
						->select(
							DB::raw('CONCAT("<a href=\"lihatisispbm/",purchaseorders.id,"\">",barangdatangs.nospbm,"</a>") as nomor')
							,DB::raw('CONCAT("<a href=\"isipurchaseorder/",barangdatangs.purchaseorders_id,"\">",isi_pos.jumlah,"</a>") as tjumlah1')
							,'isi_pos.jumlah as jumlah1','isispbms.jumlah as jumlah2',DB::raw('"DATANG" as jenis'),'barangdatangs.tanggaldatang as tanggal','barangdatangs.created_at')
						->get()
						;
						// dd($datang);
			$td = ['jumlah1'=>$datang->sum('jumlah1'),
					'jumlah2'=>$datang->sum('jumlah2')];

			$keluar1 = DB::table('detail_pengiriman_barangs') //MR
						->leftJoin('pengiriman_barangs','pengiriman_barangs.id','=','detail_pengiriman_barangs.pengiriman_barang_id')
						->leftJoin('mr_subdepts','mr_subdepts.id','=','pengiriman_barangs.global_id')
						// ->leftJoin('mr_subdepts',function($q){
						// 	$q->on('mr_subdepts.id','=','pengiriman_barangs.global_id');
						// 	$q->on('pengiriman_barangs.jenis','=','mr_baru');
						// })
						->leftJoin('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
						->whereNull('pengiriman_barangs.deleted_at')
						->whereNull('detail_pengiriman_barangs.deleted_at')
						->whereNull('mr_subdepts.deleted_at')
						->whereNull('isi_mr_subdepts.deleted_at')
						->where('detail_pengiriman_barangs.barang_id',$id)
						->where('pengiriman_barangs.jenis','like','%mr%')
						->where('isi_mr_subdepts.barangs_id',$id)
						->select(
							DB::raw('CONCAT("<a href=\"list-pengiriman/detail-pengiriman/",pengiriman_barangs.kode,"\">",pengiriman_barangs.kode,"</a>") as nomor')
							,DB::raw('CONCAT("<a href=\"isisubdeptmr/",pengiriman_barangs.global_id,"\">",isi_mr_subdepts.jumlah,"</a>") as tjumlah1')
							 ,'isi_mr_subdepts.jumlah as jumlah1'
							,'detail_pengiriman_barangs.jumlah as jumlah2',DB::raw('"KELUAR" as jenis'),'pengiriman_barangs.tgl_kirim as tanggal','pengiriman_barangs.created_at')
						// ->get()
						;

			$keluar2 = DB::table('detail_pengiriman_barangs') //SO
						->leftJoin('pengiriman_barangs','pengiriman_barangs.id','=','detail_pengiriman_barangs.pengiriman_barang_id')
						->leftJoin('sales_orders','sales_orders.id','=','pengiriman_barangs.global_id')
						->leftJoin('isi_sales_orders','isi_sales_orders.sales_orders_id','=','sales_orders.id')
						->whereNull('pengiriman_barangs.deleted_at')
						->whereNull('detail_pengiriman_barangs.deleted_at')
						->whereNull('sales_orders.deleted_at')
						->whereNull('isi_sales_orders.deleted_at')
						->where('detail_pengiriman_barangs.barang_id',$id)
						->where('pengiriman_barangs.jenis','like','%so%')
						->where('isi_sales_orders.barangs_id',$id)
						->select(
							DB::raw('CONCAT("<a href=\"list-pengiriman/detail-pengiriman/",pengiriman_barangs.kode,"\">",pengiriman_barangs.kode,"</a>") as nomor')
							,DB::raw('CONCAT("<a href=\"isi_sales_orders?parent_table=sales_orders&parent_columns=nomor,user_id,total,metode_bayar,status&parent_columns_alias=&parent_id=/",pengiriman_barangs.global_id,"\">",isi_sales_orders.jumlah,"</a>") as tjumlah1')
							 ,'isi_sales_orders.jumlah as jumlah1'
							,'detail_pengiriman_barangs.jumlah as jumlah2',DB::raw('"KELUAR" as jenis'),'pengiriman_barangs.tgl_kirim as tanggal','pengiriman_barangs.created_at')
						->union($keluar1)
						->get()
						;
						// dd($keluar2);
			$tk = ['jumlah1'=>$keluar2->sum('jumlah1'),
					'jumlah2'=>$keluar2->sum('jumlah2')];

			$hasil = $keluar2->merge($datang);
			$hasil = $hasil->sortByDesc('tanggal')->values();
			// dd($hasil);
			$data =[];
			$data[] = ['hasil'=>$hasil,
						'td'=>$td, //total datang
						'tk'=>$tk, //total keluar
						];
						// dd($data);
			return $data;
		}

		public function normalisasi_stok_barang(){
			$request = request();
			if(isset($request->barangs_id)){

			}
			else{
				
			}
		}

		public function stokawal($id=0){
			$request = request();
			$user = DB::table('cms_users')->where('id',CRUDBooster::myId())->first();
			if (!Hash::check($request->pass, $user->password)) {
				dd('hayo !');
			}
			if($id==1){
				$barangs = Barang::whereNull('deleted_at')->get();
				$arrayinsert = [];
				foreach ($barangs as $key => $b) {
					$data = $this->trackrecord($b->id);
					// dd($hasil);
					$stokawal = $b->akumstok - $data[0]['td']['jumlah2']+$data[0]['tk']['jumlah2'];
					$arrayinsert[] = ['created_at'=>now(),'barangs_id'=>$b->id,'kode'=>$b->kode,'stok_awal'=>$stokawal];
				}
				DB::table('stok_awals')->insert($arrayinsert);
				dd('done');
			}
			if($id==2){
    			$data = 'b';
    			return view('customView.secret_stokawal',['data'=>$data]);
			}
			if($id==3){
				$data = DB::table("stok_awals")
							->leftJoin('barangs','barangs.id','=','stok_awals.barangs_id')
							->whereRaw('stok_awal != stok_awal_excel')->get();
				// dd($data);
				foreach ($data as $key => $v) {
					echo ' : '.$v->id.' |  : '.$v->barangs_id.' |  : '.$v->kode.' |  : '.$v->stok_awal.' | : '.$v->stok_awal_excel.' | '.$v->nama.'<br>';
				}
			}
			if($id==4){
					$gg = DB::table("stok_awals")
								->whereRaw('stok_awal != stok_awal_excel')->get();
					// $gg = DB::table("stok_awals")->where('barangs_id',681)->get();
					// $data = $this->trackrecord(681);
					// dd($data);
					// dd($gg);
					foreach ($gg as $key => $v) {
						if($v->barangs_id == 681 || $v->barangs_id == 705 || $v->barangs_id == 767){
							continue;
						}
						$data = $this->trackrecord($v->barangs_id);
						$aaa = (int)$v->stok_awal_excel + (int)$data[0]['td']['jumlah2'] - (int)$data[0]['tk']['jumlah2'];
						// dd($gg);
						DB::table('barangs')->where('id',$v->barangs_id)->update(['akumstok'=>$aaa]);
					}
					dd('done');
			}
		}

		public function poststokawal(){
			$request = request();
			$file = $request->file('fileku');
	    		// dd($file);
	    		if(isset($file)){
	    			$rows =Excel::load($file)->get();
		    		$data = $rows->toArray();
		    		// dd($data);
		    		$jmlh = 0;
		    		foreach ($data as $key => $value) {
		    			$sss= rtrim($value['akumstok']);
		    			// dd($sss);
		    			$jmlh ++;
		    			$update = DB::table('stok_awals')->where('kode',rtrim($value['kode']))
				    									->update([
			    											'stok_awal_excel'=>(int)$sss,
			    											])
		    											// ->get()
		    											;
						// dd($update);
		    		}
	    			dd($jmlh);
	    		}
		}


		public function stokawal_barang_gudang($id=0){
			$request = request();
			$user = DB::table('cms_users')->where('id',CRUDBooster::myId())->first();
			if (!Hash::check($request->pass, $user->password)) {
				dd('hayo !');
			}
			if($id==1){
				$barangs = Barang_gudang::whereNull('deleted_at')->get();
				$arrayinsert = [];
				foreach ($barangs as $key => $b) {
					$data = $this->trackrecord($b->id);
					// dd($hasil);
					$stokawal = $b->stok - $data[0]['td']['jumlah2']+$data[0]['tk']['jumlah2'];
					$arrayinsert[] = ['created_at'=>now(),'barangs_id'=>$b->barangs_id,'kode'=>$b->kode,'stok_awal'=>$stokawal];
				}
				DB::table('stok_awals')->insert($arrayinsert);
				dd('done');
			}
			if($id==2){
				$data = 'bg';
    			return view('customView.secret_stokawal',['data'=>$data]);
			}
			if($id==3){
				$data = DB::table("stok_awals")
							->leftJoin('barangs','barangs.id','=','stok_awals.barangs_id')
							// ->whereNull('barangs.deleted_at')
							->whereRaw('stok_awal != stok_awal_excel')
							// ->toSql()
							->get()
							;
				// dd($data);
				foreach ($data as $key => $v) {
					echo ' : '.$v->id.' |  : '.$v->barangs_id.' |  : '.$v->kode.' |  : '.$v->stok_awal.' | : '.$v->stok_awal_excel.' | '.$v->nama.'<br>';
				}
			}
			if($id==4){
					$gg = DB::table("stok_awals")
								->whereRaw('stok_awal != stok_awal_excel')->get();
					// $gg = DB::table("stok_awals")->where('barangs_id',681)->get();
					// $data = $this->trackrecord(681);
					// dd($data);
					// dd($gg);
					foreach ($gg as $key => $v) {
						if($v->barangs_id == 681 
							|| $v->barangs_id == 705 
							|| $v->barangs_id == 767
						){
							continue;
						}
						$data = $this->trackrecord($v->barangs_id);
						$aaa = (int)$v->stok_awal_excel + (int)$data[0]['td']['jumlah2'] - (int)$data[0]['tk']['jumlah2'];
						// dd($gg);
						DB::table('barang_gudangs')->where('id',$v->barangs_id)->update(['stok'=>$aaa]);
					}
					dd('done');
			}
		}
}