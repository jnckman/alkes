<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Lock_rekap;

	class AdminRekapMrsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "rekap_mrs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Norekap","name"=>"norekap"];
			$this->col[] = ["label"=>"Periode","name"=>"periode"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Norekap','name'=>'norekap','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Periode','name'=>'periode','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Norekap","name"=>"norekap","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Periode","name"=>"periode","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


		public function getBelumApproved(){
			$data = [];
			$this->cbView('customView.pr_getBelumApproved',$data);
		}	    

		public function dtlistBelumApprove(){
			/*$lock = Lock_rekap::where('approve',0)->get();

			$data = [];
			foreach($lock as $d){
				if($d->jumlah < $d->min){
		          $d->nama = '<b style="color:red">'.$d->barang->nama.'</b>';
		          $minmax = '<b style="color:red">'.$d->min.'</b> / '.$d->max;
		        }
		        elseif($d->jumlah > $d->barang->max){
		          $d->nama = $d->barang->nama;
		          $minmax = ''.$d->min.'<b style="color:red"> / '.$d->max.' </b>';
		        }
		        else{
		          $minmax = ''.$d->min.' / '.$d->max;
		        }

	     		$data[] = [
		     			$d->barang->nama,
		     			$d->barang->kode,
		     			$d->jumlah,
						$minmax,
	     				$d->id,
	     		 		];
	    	 }
	    	// dd($data);
	    	$results = [
	    				"draw" => 0,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	return response()->json($results);*/
	    	$hasil = DB::table('lock_rekaps')
            ->leftJoin('barangs','barangs.id','=','lock_rekaps.barangs_id')
            ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
            ->leftJoin('pricelists','pricelists.id','=','lock_rekaps.pricelists_id')
            ->leftJoin('suppliers','pricelists.suppliers_id','=','suppliers.id')
            ->select('lock_rekaps.id',
                'barangs.nama',
                'barangs.kode',
                'lock_rekaps.jumlah',
                'barangs.avgprice',
                'satuans.nama as satuan',
                'barangs.id as barangid',
                'suppliers.nama as namasup',
                'pricelists.harga',
                'barangs.min',
                'barangs.max',
                'lock_rekaps.approve',
                'lock_rekaps.periode_mrs_id as idperiode','lock_rekaps.tanggalrekap'
            )
            ->whereNull('lock_rekaps.deleted_at')
            ->where('lock_rekaps.approve',0)
            // ->where('lock_rekaps.periode_mrs_id',$id)
            ->get();

		      $total = 0;
		      $kata;
		      $hargasup =0;
		      $urutan = 0;
		      $data = [];
		      foreach($hasil as $d) {

		        $hargasup = 0;

		        if($d->namasup == null){
		          $kata='belum dipilih';
		        }
		        else{
		          $kata = $d->namasup.' - '.$d->harga;
		          $hargasup = number_format($d->harga*$d->jumlah);
		          $total = (($d->harga * $d->jumlah) + $total);
		        }

		        if($d->jumlah < $d->min){
		          $d->nama = '<b style="color:red">'.$d->nama.'</b>';
		          $minmax = '<b style="color:red">'.$d->min.'</b> / '.$d->max;
		        }
		        elseif($d->jumlah > $d->max){
		          $d->nama = $d->nama;
		          $minmax = ''.$d->min.'<b style="color:red"> / '.$d->max.' </b>';
		        }
		        else{
		          $minmax =   ''.$d->min.' / '.$d->max;
		        }

		        //NOTE tombol checkbox
		        if ($d->approve == 1) {
		          $checkbox = 'Approved';
		        }
		        else {
		          $checkbox = '<input type="checkbox" rekap_id="'.$d->id.'" name="id[]" value="'.$d->id.'">';
		        }

		        //NOTE tombol action
		        $action = '<button type="button" class="btn btn-xs btn-success listmrpr" id="'.$d->barangid.'" idperiode="'.$d->idperiode.'" style="margin-left:5px;">List MR </button>';

		        //NOTE harga selected
		        $harga_selected = '<span id="pilihanharga'.$d->id.'">'.$kata.'</span>';

		        $data[] = [
		               // $d->id,
		        		$d->tanggalrekap,
		               $d->nama,
		               $d->kode,
		               $d->jumlah,
		               $minmax,
		               $d->satuan,
		               number_format($d->avgprice),
		               $harga_selected,
		               $hargasup,
		              $action,
		              $checkbox
		        ];
		        $urutan++;
		      }

		      //$data[0]=[1,'a'];
		      //$data[1]=[2,'b'];
		      $results = [
		        "total" =>$total,
		        "draw" => 1,
		            "recordsTotal" => count($data),
		            "recordsFiltered" => count($data),
		            "data" => $data
		          ];
		    return response()->json($results);
		}
	    
	    public function raselistmrprajax(){
	    	$request = request();
	    	$id = $request->id;
	    	$idperiode = $request->idperiode;
	        $data = DB::table('isi_mr_subdepts')
	        ->leftJoin('mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
	        ->leftJoin('periode_mrs','periode_mrs.id','=','mr_subdepts.periode_mrs_id')
	        ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
	        ->select(
	        	'isi_mr_subdepts.id',
	        	'mr_subdepts.nomr','projects.nama','isi_mr_subdepts.jumlah')
	        ->where('isi_mr_subdepts.barangs_id',$id)
	        ->where('mr_subdepts.periode_mrs_id',$idperiode)
	        ->whereNull('isi_mr_subdepts.deleted_at')->get();

	        echo json_encode($data);
	    }

	    public function raselistmrprbarangajax(){
	    	$request = request();
	    	$id = $request->id;

	    	$barangs = DB::table('isi_mr_subdepts')
	    						->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
	    						->where('isi_mr_subdepts.id',$id)
	    						->select('isi_mr_subdepts.*','barangs.nama as barang')
	    						->get();
	    	return response()->json($barangs);
	    }

	    public function updateApprove(){
	    	$request = request();
	    	// dd($request->id);
	    	$update = DB::table('lock_rekaps')->whereIn('id',$request->id)->update(['approve'=>1]);
	    	// dd($update);
	    	if($update){
	    		CRUDBooster::redirect( route('pr.getBelumApproved'),'Approve berhasil','success');
	    	}
	    	else{
	    		CRUDBooster::redirect( route('pr.getBelumApproved'),'Approve gagal, harap hubungi admin','error');
	    	}	
	    }

	}