<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Isi_sales_order;
	use App\Models\Harga_jual;

	class AdminIsiSalesOrdersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "isi_sales_orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Barangs","name"=>"barangs_id","join"=>"barangs,nama"];
			$this->col[] = ["label"=>"Jumlah","name"=>"jumlah"];
			$this->col[] = ["label"=>"Diskon","name"=>"diskon"];
			$this->col[] = ["label"=>"Total","name"=>"total"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Barang','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama','datatable_format'=>'nama,\' | \',avgprice'];
			$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'
			// ,'help'=>'Pilih barang terlebih dahulu','readonly'=>'true'
			];
			$this->form[] = ['label'=>'Diskon','name'=>'diskon','type'=>'number','validation'=>'required','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Total','name'=>'total','type'=>'number','validation'=>'required','width'=>'col-sm-10'
			// ,'help'=>'otomatis by sistem','readonly'=>'true'
			];
			$this->form[]= ['label'=>'sales orders','name'=>'sales_orders_id','type'=>'hidden'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Barang','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			//$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10','help'=>'Pilih barang terlebih dahulu','readonly'=>'true'];
			//$this->form[] = ['label'=>'Diskon','name'=>'diskon','type'=>'number','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Total','name'=>'total','type'=>'number','validation'=>'required','width'=>'col-sm-10','help'=>'otomatis by sistem','readonly'=>'true'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        // $this->load_js[] = asset("customs/js/isi_sales_orders.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
	        // $request = request();
	        // dd($request);
	        // $postdata['sales_orders_id'] = $request->sales_orders_id;
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	        //check apakah di SO ada barang duplikat
	        $request = request();
	        $isi_so = Isi_sales_order::find($id);
	        $check = Isi_sales_order::where('barangs_id',$isi_so->barangs_id)
	        							->where('sales_orders_id',$isi_so->sales_orders_id)
	        							->where('id','<>',$id)
					        			->whereNull('deleted_at')
					        			->get();
					        			// dd($check);
			$total = $check->sum('total');
			$total += $isi_so->total;
			$jumlah = $check->sum('jumlah');
			$jumlah += $isi_so->jumlah;
			$diskon = $check->sum('diskon');
			$diskon += $isi_so->diskon;
			$isi_so->update(['jumlah'=>$jumlah,'total'=>$total,'diskon'=>$diskon]);
			$isi_so->sales_order->update(['total'=>DB::raw('total + '.$isi_so->total),
										'diskon'=>DB::raw('diskon + '.$isi_so->diskon)])
										;
	        $check = Isi_sales_order::where('barangs_id',$isi_so->barangs_id)
	        							->where('sales_orders_id',$isi_so->sales_orders_id)
	        							->where('id','<>',$id)
					        			->whereNull('deleted_at')
					        			->delete();
	    	CRUDBooster::redirect($request->returnku,'Berhasil menambah Data','success');
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	    	
	    	$request= request();

	    	CRUDBooster::redirect($request->returnku,'Berhasil Mengubah Data','success');
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here
	        $isi = Isi_sales_order::find($id);
	        $isi->sales_order->update(['total'=>DB::raw('total - '.$isi->total)]);
	    	// dd($isi);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function getadd(){
	    	$this->addeditku();
	    }

	    public function getedit($id){
	    	$this->addeditku($id);
	    }

	    public function addeditku($id=0){
	    	$request= request();
	    	
	    	if(!isset($request->parent_id)){
	    		CRUDBooster::redirect( CRUDBooster::adminPath('sales_orders'),'Pilih SO !');
	    	}
	    	$data=[];
	    	$data['edit'] = [];
	    	$customer = DB::table('sales_orders')
	    					->leftJoin('customers','customers.id','=','sales_orders.customers_id')
	    					->where('sales_orders.id',$request->parent_id)
	    					->select('customers.jenis_harga_jual','sales_orders.id as soid')
	    					->first();
			$data['type'] = 'add';
	    	if($id!=0){
	    		$data['edit'] = DB::table('isi_sales_orders')->where('id',$id)->first();
	    		// dd($data['edit']);
	    		$data['type'] = 'edit';
	    	}
	    	$hasil = Harga_jual::leftJoin('barangs','barangs.id','=','harga_juals.barang_id')
	    						->select("barangs.nama AS text",'barangs.id as id','harga_juals.harga_jual as hj')
	    						->where('harga_juals.ket',$customer->jenis_harga_jual)
	    						->get();
	    						;
			$data['barangs'] = [];
	    	foreach ($hasil as $key => $v) {
	    		$data['barangs'][] = ['id'=>$v->id,
	    							'text'=>$v->text.' | '.$v->hj
	    							];
	    	}
	    	$data['barangs'] = json_encode($data['barangs']);

	    	$data['page_title'] = 'Add Isi Sales Orders';
	    	$data['so_id'] = $customer->soid;
	    	// dd($data);
	    	$this->cbView('customView.isi_so_add',$data);
	    }

	}