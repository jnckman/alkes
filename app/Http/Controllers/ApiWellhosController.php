<?php

namespace App\Http\Controllers;
use Session;
use DB;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ApiWellhosController extends Controller
{
   public function getListAsm() {
      $client = new Client();
      // $res = $c->get('http://api.gis.gemilang.co.id/accounting/asm.php')->json();
      // $res = $client->get('http://api.gis.gemilang.co.id/accounting/asm.php');
      // $r = Request::create('http://api.gis.gemilang.co.id/accounting/asm.php', 'GET');
      $request = new \GuzzleHttp\Psr7\Request('GET', 'http://api.gis.gemilang.co.id/accounting/asm.php');
      dd(($request));
   }
   public function getallbarang(){
      $hasil = DB::table('barangs')->whereNull('deleted_at')->get();
      return json_encode($hasil);
   }
   public function getallgroup(){
      $hasil = DB::table('groups')->whereNull('deleted_at')->get();
      return json_encode($hasil);
   }

   public function getmasuk($jenispo=-1,$kategoribrg=0,$tgl=0){ //jumlah masuk PO
   		$request = request();
   		//jenis PO : 1 = TEMPO; 2 = TUNAI
   		switch (strtolower($jenispo)) {
   			case 'tunai':
   				$jpo = 2;
   				break;
   			case 'non-tunai': //tempo 
	   			$jpo = 1;
   				break;
   			default:
   				break;
   		}

   		$hasil = DB::table('isi_pos')->leftJoin('purchaseorders','purchaseorders.id','=','isi_pos.purchaseorders_id')
   									->leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
   									->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
   									->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
   									->where('kategoris.nama','like','%'.$kategoribrg.'%')
   									->where('purchaseorders.jenispo',$jpo)
   									->where('purchaseorders.tglpo',$tgl)
   									->whereNull('purchaseorders.deleted_at')
   									->whereNull('isi_pos.deleted_at')
   									// ->select('purchaseorders.nopo','barangs.nama as barang','purchaseorders.jenispo','isi_pos.total_akhir','kategoris.nama as kategori') //debuging
   									->select('purchaseorders.ongkir','purchaseorders.nopo','suppliers.nama as supplier'
   											,DB::raw('SUM(isi_pos.total_akhir) as total_akhir'))
   									// ->groupBy('')
   									// ->sum('isi_pos.total_akhir')
   									->groupBy('nopo','ongkir','supplier'
   											// ,'total_akhir'
   											)
   									->get()
   									;
   									// dd($hasil);
   		return response()->json(['jumlah_rupiah'=>$hasil->sum('total_akhir'),'jumlah_pp'=>$hasil->sum('ongkir'),'data'=>$hasil]);
   		dd($request);
   }

   public function getkeluar($table=0,$kategoribrg=0,$tgl=0){ //jumlah keluar MR
   		$request = request();
   		switch (strtolower($table)) {
   			case 'department':
   				$hasil = DB::table('isi_mr_subdepts')
   							->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
   							->leftJoin('wellhos_subdepts','wellhos_subdepts.id','=','mr_subdepts.wellhos_subdepts_id')
   							->leftJoin('wellhos_departments','wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
   							->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
   							->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
   							->whereNull('isi_mr_subdepts.deleted_at')
   							->whereNull('mr_subdepts.deleted_at')
   							->whereNull('wellhos_subdepts.deleted_at')
   							->whereNull('wellhos_departments.deleted_at')
   							->where('kategoris.nama','like','%'.$kategoribrg.'%')
   							->where('mr_subdepts.tanggal',$tgl)
   							->select('wellhos_departments.nama as department'
   								,DB::raw('SUM(mr_subdepts.total) as total')
   								)
   							->groupBy('department')
   							->get();
   							;
   							// dd($hasil);
   				return response()->json(['jumlah_rupiah'=>$hasil->sum('total'),'data'=>$hasil]);
   				break;
   			case 'project':
	   			$hasil = DB::table('isi_mr_subdepts')
   							->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
   							->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
   							->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
   							->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
   							->whereNull('isi_mr_subdepts.deleted_at')
   							->whereNull('mr_subdepts.deleted_at')
   							->whereNull('projects.deleted_at')
   							->where('kategoris.nama','like','%'.$kategoribrg.'%')
   							->where('mr_subdepts.tanggal',$tgl)
   							->select('projects.nama as project','projects.area as area_project'
   								,DB::raw('SUM(mr_subdepts.total) as total')
   								)
   							->groupBy('project','area_project')
   							->get();
   							;
	   			return response()->json(['jumlah_rupiah'=>$hasil->sum('total'),'data'=>$hasil]);
   				break;
   			default:
   				return response()->json('table tidak ditemukan; gunakan :[department,project]');
   				break;
   		}	
   }

   public function getmasukdetail($table=0,$kategoribrg=0,$tgl=0){ //jumlah keluar MR
         $request = request();
         switch (strtolower($table)) {
            case 'tunai':
               $jpo = 2;
               break;
            case 'non-tunai': //tempo 
               $jpo = 1;
               break;
            default:
               break;
         }
         $hasil = DB::table('isispbms')
                        ->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
                        ->leftJoin('purchaseorders','purchaseorders.id','=','barangdatangs.purchaseorders_id')
                        ->leftJoin('isi_pos',function($join){
                          $join->on('isi_pos.purchaseorders_id','=','purchaseorders.id');
                          $join->on('isispbms.barangs_id','=','isi_pos.barangs_id');
                        })
                        ->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
                        ->leftJoin('barangs','barangs.id','=','isispbms.barangs_id')
                        ->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
                        ->whereNull('isispbms.deleted_at')
                        ->whereNull('barangdatangs.deleted_at')
                        ->whereNull('purchaseorders.deleted_at')
                        ->where('kategoris.nama','like','%'.$kategoribrg.'%')
                        ->where('barangdatangs.tanggaldatang',$tgl)
                        ->where('purchaseorders.jenispo',$jpo)
                        ->where('purchaseorders.jenispo',$jpo)
                        ->select('barangs.nama as namabarang','isi_pos.harga','suppliers.nama','purchaseorders.ppn','isispbms.jumlah','purchaseorders.jenispo')
                        ->get();
                        ;
                        // dd($hasil);
         return json_encode($hasil);
         //response()->json(['jumlah_rupiah'=>$hasil->sum('total'),'data'=>$hasil]);


         
   }

   public function getkeluardetail($table=0,$kategoribrg=0,$tgl=0){ //jumlah keluar MR
         $request = request();
         
            $hasil = DB::table('pengiriman_barangs')
                        ->leftJoin('detail_pengiriman_barangs','pengiriman_barangs.id','=','detail_pengiriman_barangs.pengiriman_barang_id')
                        ->leftJoin('barangs','barangs.id','=','detail_pengiriman_barangs.barang_id')
                        ->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
                        ->leftJoin('mr_subdepts','pengiriman_barangs.global_id','=','mr_subdepts.id') 
                        ->leftJoin('wellhos_subdepts','wellhos_subdepts.id','=','mr_subdepts.wellhos_subdepts_id') 
                        ->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
                        ->whereNull('pengiriman_barangs.deleted_at')
                        ->whereNull('mr_subdepts.deleted_at')
                        ->where('kategoris.nama','like','%'.$kategoribrg.'%')
                        ->where('pengiriman_barangs.tgl_kirim',$tgl)
                        ->where('pengiriman_barangs.jenis','LIKE','%mr_baru%')
                        ->select('barangs.nama as namabarang','detail_pengiriman_barangs.jumlah','kategoris.nama as namakategori','barangs.avgprice as ratarata','wellhos_subdepts.nama as namasubdept','projects.nama as namaproject','mr_subdepts.nomr as kodemr')
                        ->get();


        
         return json_encode($hasil);
         //response()->json(['jumlah_rupiah'=>$hasil->sum('total'),'data'=>$hasil]);


         
   }

   public function getjualjohnson($pilihan,$inpt){
      if(strtolower($pilihan) == 'periode'){
         // $periode = DB::table('periode_mrs')->where('id',$inpt)->first();
         $input = explode('-', $inpt);
         if(count($input)!=2){
            return json_encode('format input Y-m');
         }
         // dd($input);
         $so = DB::table('sales_orders')
                  ->leftJoin('customers','customers.id','=','sales_orders.customers_id')
                  ->whereNull('sales_orders.deleted_at')
                  // ->where('sales_orders.created_at','<=',date('Y-m-d H:i:s',strtotime($periode->tglselesai)))
                  // ->where('sales_orders.created_at','>=',date('Y-m-d H:i:s',strtotime($periode->tglmulai)))
                  ->whereYear('sales_orders.created_at',$input[0])
                  ->whereMonth('sales_orders.created_at',$input[1])
                  ->select('sales_orders.total','sales_orders.diskon','sales_orders.nomor','sales_orders.created_at','sales_orders.ppn','customers.nama as customer')
                  ->get()
                  ;
      }
      else if(strtolower($pilihan) == 'tanggal'){
         $so = DB::table('sales_orders')
                  ->leftJoin('customers','customers.id','=','sales_orders.customers_id')
                  ->whereNull('sales_orders.deleted_at')
                  ->whereDate('sales_orders.created_at','=',$inpt)
                  ->select('sales_orders.total','sales_orders.diskon','sales_orders.nomor','sales_orders.created_at','sales_orders.ppn','customers.nama as customer')
                  ->get()
                  ;
      }
      else{
         return response()->json('format salah');
      }
      $data = [];
      foreach ($so as $key => $v) {
         $data[] = ['konsumen' => $v->customer,
                     'invoice'=>$v->nomor,
                     'tgl'=>date('Y-m-d',strtotime($v->created_at)),
                     'jumlah_pokok'=> $v->total+$v->diskon,
                     'diskon' => $v->diskon,
                     'ppn' => $v->ppn,
                     'total_tagihan'=>$v->total + ($v->total * $v->ppn / 100)];
      }
      return json_encode($data);
   }

   public function getlistperiode($tahun){
      $data = DB::table('periode_mrs')
                  ->whereNull('deleted_at')
                  ->where('tahun',$tahun)
                  ->select('id','judul','tahun','tglmulai as tgl_mulai','tglselesai as tgl_selesai','catatan')
                  ->get();
      return json_encode($data);
   }

   public function getallitem(){
      $data = DB::table('barangs')
      ->leftJoin('kategoris','kategoris.id','=','barangs.kategoris_id')
      ->whereNull('barangs.deleted_at')->select('barangs.nama','kategoris.nama as katnama','barangs.avgprice','barangs.akumstok')->get();
      return json_encode($data);
   }
}
