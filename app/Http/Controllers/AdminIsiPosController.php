<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;
	use App\Models\Barangdatang;
	use App\Models\Isispbm;
	use App\Models\Isi_po;
	use App\Models\Barang_gudang;
	use Auth;

	class AdminIsiPosController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "isi_pos";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Barangs Id","name"=>"barangs_id","join"=>"barangs,nama"];
			$this->col[] = ["label"=>"Jumlah","name"=>"jumlah"];
			$this->col[] = ["label"=>"harga","name"=>"barangs_id","join"=>"pricelists,harga"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			//$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here
	    	dd('x');
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function buatspbm($id){
	    	

			
	    	//cek spbm sudah ada atau belum ?
	    	
	    	$exist = DB::table('barangdatangs')->where('purchaseorders_id',$id)->orderBy('id','Desc')->wherenull('barangdatangs.deleted_at')->limit(1)->get();
	    	if(sizeof($exist) == 0){
	    		    	$hasil = isi_po::
	    		    	leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
	    		    	->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
	    		    	->leftJoin('purchaseorders','isi_pos.purchaseorders_id','=','purchaseorders.id')
	    		    	->select('isi_pos.id','barangs.nama','satuans.nama as satuan','isi_pos.jumlah','isi_pos.harga','purchaseorders.customers_id','barangs.kode','barangs.id as bid')
	    				->where('isi_pos.purchaseorders_id',$id)
	    		    	->whereNull('isi_pos.deleted_at')
	    		    	->get();
	    		    	

	    		    	
	    		    	

	    	}
	    	else{
						
	    			$hasil = $hasil = isi_po::
	    				leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
	    				->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
	    		    	->leftJoin('purchaseorders','isi_pos.purchaseorders_id','=','purchaseorders.id')
	    		    	->leftJoin('barangdatangs','barangdatangs.purchaseorders_id','=','purchaseorders.id') 
	    		    	//->leftJoin('isispbms','isispbms.barangs_id','=','barangs.id') 

	    		    	->Join('isispbms',function($join){
	    		    	  $join->on('barangdatangs.id','=','isispbms.barangdatangs_id');
	    		    	  $join->on('isispbms.barangs_id','=','barangs.id');
	    		    	})
	    		    	//->leftJoin('isispbms','isispbms.barangdatangs_id','=','barangdatangs.id')
	    		    	//->select('isispbms.*')
	    		     	->select('barangs.id as bid','barangs.nama','satuans.nama as satuan','isi_pos.harga','isi_pos.jumlah','purchaseorders.customers_id','barangs.kode',DB::raw('sum(isispbms.jumlah) as total'),'purchaseorders.id as pid','isi_pos.id as ipoid')
	    		     	->groupBy('isispbms.barangs_id','barangs.id','barangs.nama','satuans.nama','isi_pos.harga','purchaseorders.customers_id','barangs.kode','isi_pos.jumlah','isi_pos.id','purchaseorders.id')
	    		    	->orderby('barangs.id','desc')
	    				->where('isi_pos.purchaseorders_id',$id)
	    				//->whereNull('isispbms.deleted_at')
	    		    	->whereNull('isi_pos.deleted_at')->get();
	    		    	//dd($hasil[0]->tes($hasil[0]['ipoid']));
	    		  //   	$hasil = isi_po::
	    		  //   	leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
	    		  //   	->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
	    		  //   	->leftJoin('purchaseorders','isi_pos.purchaseorders_id','=','purchaseorders.id')
	    		  //   	->leftJoin('barangdatangs','barangdatangs.purchaseorders_id','=','purchaseorders.id')
	    		  //   	->leftJoin('isispbms','isispbms.barangdatangs_id','=','barangdatangs.id')
	    		  //   	->select('barangs.id as bid','barangs.nama','satuans.nama as satuan','isi_pos.harga','isi_pos.jumlah','purchaseorders.customers_id','barangs.kode',DB::raw('sum(isispbms.jumlah) as total'),'isispbms.barangs_id as spbmbid')
	    		  //   	->groupBy('isispbms.barangs_id','barangs.nama','satuans.nama','isi_pos.harga','purchaseorders.customers_id','barangs.kode','isi_pos.jumlah','barangs.id')
	    		  //   	->orderby('barangs.id','desc')
	    				// ->where('isi_pos.purchaseorders_id',$id)
	    		  //   	->whereNull('isi_pos.deleted_at')
	    		  //   	->get();

	    		    	// $hasil = Isispbm::
	    		    	// leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    		    	// ->leftJoin('purchaseorders','purchaseorders.id','=','barangdatangs.purchaseorders_id')
	    		    	// ->leftJoin('barangs','barangs.id','=','isispbms.barangs_id')
	    		    	// ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
	    		    	// ->leftJoin('isi_pos','isi_pos.purchaseorders_id','=','purchaseorders.id')
	    		    	// ->select('isispbms.barangs_id as spbmbid',DB::raw('sum(isispbms.jumlah) as total'),'barangs.id as bid','barangs.nama','satuans.nama as satuan','purchaseorders.customers_id','barangs.kode')
	    		    	// ->groupBy('isispbms.barangs_id','barangs.id','barangs.nama','satuans.nama','purchaseorders.customers_id','barangs.kode')
	    		    	// ->where('purchaseorders.id',$id)->get();
	    		    	
	    		    	
	    	}
	    	
	    	$gudang = DB::table("gudangs")->whereNull('deleted_at')->get();
	    	$hasil2 = DB::table('purchaseorders')
	    	->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
	    	->select('purchaseorders.nopo','purchaseorders.tglpo','suppliers.nama','suppliers.notelp')
	    	->where('purchaseorders.id',$id)->whereNull('purchaseorders.deleted_at')->get();
	    	

	    	$data['hasil'] = $hasil;
	    	$data['hasil2'] = $hasil2;
	    	$data['first'] = sizeof($exist);
	    	$data['lastid'] = $exist[0]->id;

	    	$data['id']= $id;
	    	$data['gudang']= $gudang;
	    	$this->cbView('customView.buatspbm',$data);
	    	
	    }

	    public function submitspbm(Request $request){
	    		
	    		$input  = $request::all();
	    		
	    		$counter = 0;
	    		foreach ($input['barangs_id'] as $i) {
	    			if($input['barangs_jumlah'][$counter] + $input['sudahdatang'][$counter] > $input['dipesan'][$counter])
	    				return redirect()->back()->with(['message'=>'Jumlah barang yang di input melebihi yang di pesan'.$input['barangs_jumlah'][$counter],'message_type'=>'error']);
	    			$counter++;
	    		}
	    		
	    		$data = DB::table('barangdatangs')->insertGetId(
    				['suppliers_id' => $input['suppliers_id'], 
    				 'nospbm' => $input['nospbm'],
    				 'purchaseorders_id' => $input['purchaseorders_id'],
    				 'tanggaldatang'=>$input['tanggaldatang'],
    				 'gudangs_id' => $input['gudangs_id'],
    				 'penerima' =>'orang gudang',
    				 'created_at' =>Carbon::now()->toDateTimeString(),
    				 'nofaktur' => $input['nofaktur'],
    				 'masapajak' => $input['masapajak'],
    				 'efaktur' => $input['efaktur']
    				 ]
    			);
	    		
				$ct=0;
				


	    		foreach ($input['barangs_id'] as $i) {
		    		if($input['barangs_jumlah'] == 0){

		    		}
		    		else{
		    			DB::table('isispbms')->insert(
		    				['barangdatangs_id'=>$data,
		    				'barangs_id'=>$i,
		    				'jumlah'=>$input['barangs_jumlah'][$ct]+$input['total'][$ct],
		    				'kondisi'=>$input['barangs_kondisi'][$ct],
		    				'created_at'=>Carbon::now()->toDateTimeString()
		    				]
		    			);
		    			
		    			
		    			$exist = DB::table('barang_gudangs')->where(['barangs_id'=>$i,'gudangs_id'=>$input['gudangs_id']]);
		    			if(count($exist->get())  >0) {
		    				//return redirect()->back()->with(['message'=>'gagal','message_type'=>'danger']);
		    				$jumlah = $input['barangs_jumlah'][$ct]+$input['total'][$ct];
		    			    $exist->update(['stok'=>DB::raw('stok + '.$jumlah)]);
		    			}
		    			else  {
		    			    //$data=array('barangs_id'=>00,'gudangs_id'=>100);
		    			    DB::table('barang_gudangs')->insert(
		    				['kode'=>$input['namabarang'][$ct].'-',
		    				 'barangs_id'=>$i,
		    				 'gudangs_id'=>$input['gudangs_id'],
		    				 'stok'=>$input['barangs_jumlah'][$ct],
		    				 'min'=>0,
		    				 'max'=>0
		    				]);


		    			}
		    			$pricelist = DB::table('isi_pos')->wherenull('deleted_at')->where('purchaseorders_id',$input['purchaseorders_id'])
		    			->where('barangs_id',$i)->get()[0]->harga;
		    			
		    			$nambah = DB::table('barangs')->where('id',$i)->select('barangs.akumstok','barangs.avgprice')->get();
		    			$akhirratarata =0;
		    			$sekarang = $nambah[0]->akumstok + $input['barangs_jumlah'][$ct];
		    			if($input['barangs_jumlah'][$counter] + $input['sudahdatang'][$counter] == $input['dipesan'][$counter]){
		    				if($nambah[0]->avgprice == 0){
		    					$nambah[0]->avgprice == 0;
		    				}
		    				else{
		    					//681280 +  681961280 
		    					//dd($nambah[0]->akumstok.' - '.$nambah[0]->avgprice.' - '.$input['dipesan'][$ct].' -- '.$input['dipesan'][$ct]);
		    					//14000/15
		    					$akhirratarata = 
		    						(($nambah[0]->akumstok * $nambah[0]->avgprice) + 
		    						(($input['dipesan'][$ct]*1) * $pricelist))
		    						/($nambah[0]->akumstok + ($input['dipesan'][$ct]*1));
		    					
		    				}
		    				
		    			}
		    			$tambahan = DB::table('barangs')->where('id',$i)->update(['akumstok'=>$sekarang]);
		    			$tambahan = DB::table('barangs')->where('id',$i)->update(['avgprice'=>$akhirratarata]);
		    			//$tambahan = DB::table('barangs')->where('id',$i)->update(['selesai'=>1]);
		    			
		    		}
		    		$ct++;
		    		$counter++;
    			}
	    		CRUDBooster::redirect(CRUDBooster::adminPath('purchaseorders/'),'');
    			return redirect()->back()->with(['message'=>'SPBM Berhasil di buat','message_type'=>'success']);
	    }

	    public function listspbm($id){
	    		
	    		$data['id'] = $id;
    			
	    		$data['result']= DB::table('purchaseorders')
	    		->leftJoin('suppliers','purchaseorders.customers_id','=','suppliers.id')
	    		->where('purchaseorders.id',$id)->get();
    			$this->cbView('customView.listspbm',$data);
	    }

	    public function listspbmdtajax($id){
	    		$data = array();
	    		$hasil = DB::table('barangdatangs')
	    		->leftJoin('gudangs','barangdatangs.gudangs_id','=','gudangs.id')
	    		->select('barangdatangs.nospbm','barangdatangs.tanggaldatang','barangdatangs.penerima','gudangs.nama as namagudang','barangdatangs.id','barangdatangs.nofaktur')
	    		->whereNull('barangdatangs.deleted_at')
	    		->where('barangdatangs.purchaseorders_id',$id)
	    		->get();

	    		foreach($hasil as $d){
	    					$txt ='';
	    					$bh = DB::table('bukuhutangs')->where('barangdatangs_id',$d->id)->whereNull('deleted_at')->get();
	    					if(sizeof($bh) == 0){
	    						$txt = '<a href="../approvedataspbm/'.$id.'/'.$d->id.'"><button class="btn btn-xs btn-success isispbm" id="'.$d->id.'">Approve SPBM</button></a>';
	    					}
	    					else{
	    						$txt = 'approved';
	    					}
	    					$data[] = [
	    			 				   $d->nospbm,
	    			 				   $d->tanggaldatang,
	    			 				   $d->penerima,
	    			 				   $d->namagudang,
	    			 				   $d->nofaktur,
	    			 				   '<a href="../lihatisispbmdetail/'.$id.'/'.$d->id.'"><button class="btn btn-xs btn-success isispbm" id="'.$d->id.'">Lihat isi SPBM</button></a>'
	    			 				   ,
	    			 				   $txt
	    			 				   ];

	    			 				   
	    			 		$urutan++;
	    				 }
	    					$results = [
	    					"total" =>$total,
	    					"draw" => 1,
	    		        	"recordsTotal" => count($data),
	    		        	"recordsFiltered" => count($data),
	    		        	"data" => $data
	    		        	];
	    		echo json_encode($results);
	    }
	    //oke

	    public function isilistspbm($id){
	    	
	    	$hasil = Barangdatang::where('purchaseorders_id',$id)->get();

	    	//dd($data[0]->Isispbm[0]->barangs_id);
	    	$data['hasil'] = $hasil;
	    	$data['id'] = $id;
	    	$this->cbView('customView.isilistspbm',$data);
	    }
	    public function isilistspbmdtajax($id){

	    		$hasil = DB::table('isispbms')
	    		->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    		->leftJoin('barangs','barangs.id','=','isispbms.barangs_id')
	    		->select('isispbms.jumlah','barangs.nama','barangs.kode','barangdatangs.nospbm')
	    		->whereNull('isispbms.deleted_at')
	    		->get();

	    		foreach($hasil as $d){
	    					$data[] = [
	    			 				   $d->nama,
	    			 				   $d->kode,
	    			 				   $d->jumlah,
	    			 				   
	    			 				   $d->nospbm,
	    			 				   ];

	    			 				   
	    			 		$urutan++;
	    				 }
	    					$results = [
	    					"total" =>$total,
	    					"draw" => 1,
	    		        	"recordsTotal" => count($data),
	    		        	"recordsFiltered" => count($data),
	    		        	"data" => $data
	    		        	];
	    		echo json_encode($results);
	    }


	    public function isilistspbmdetail($id,$subid){
	    	
	    	$hasil = Barangdatang::all();
	    	$data['hasil'] = $hasil;
	    	$data['id'] = $id;
	    	$data['subid'] = $subid;
	    	$this->cbView('customView.isilistspbmdetail',$data);
	    }
	    public function isilistspbmdetaildtajax($id,$subid){

	    		$hasil = DB::table('isispbms')
	    		->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    		->leftJoin('barangs','barangs.id','=','isispbms.barangs_id')
	    		->select('isispbms.id','isispbms.jumlah','barangs.nama','barangs.kode','barangdatangs.nospbm')
	    		->whereNull('isispbms.deleted_at')
	    		->where('barangdatangs_id',$subid)
	    		->get();

	    		foreach($hasil as $d){
	    					$data[] = [
	    			 				   $d->nama,
	    			 				   $d->kode,
	    			 				   $d->jumlah,
	    			 				   $d->nospbm,
	    			 				   '<button class="btn btn-xs btn-success editisispbm" id="'.$d->id.'" jumlah="'.$d->jumlah.'">Edit</button>'.
	    			 				   '<a href="../lihatisispbmdetail/'.$id.'/'.$d->id.'" style="margin-left:5px;"><button class="btn btn-xs btn-danger isispbm" id="'.$d->id.'">Hapus</button></a>'
	    			 				   ];

	    			 				   
	    			 		$urutan++;
	    				 }
	    					$results = [
	    					"total" =>$total,
	    					"draw" => 1,
	    		        	"recordsTotal" => count($data),
	    		        	"recordsFiltered" => count($data),
	    		        	"data" => $data
	    		        	];
	    		echo json_encode($results);
	    }

	    public function editisispbmajax(Request $request){
	    	$input  = $request::all();
	    	$jumlahlama = $input['jumlah'];
	    	$selisih = 0;


	    	$isispbms1  = DB::table('isispbms')->where(['id'=>$input['id']]);
	    	$isispbms = $isispbms1->get();

	    	$selisih = $jumlahlama - $isispbms[0]->jumlah;

	    	$hasil = $isispbms1->update(['jumlah'=>$input['jumlah']]);
	    	
	    	
	    	$barangdatang = DB::table('barangdatangs')->where('id',$isispbms[0]->barangdatangs_id)->get();
	    	
	    	$hasil2 = DB::table('barang_gudangs')->where('barangs_id',$isispbms[0]->barangs_id)
	    	->where('gudangs_id',$barangdatang[0]->gudangs_id);

	    	$barangdatanghasil = $hasil2->get()[0]->stok + $selisih;
	    	//dd($barangdatang[0]->gudangs_id);
	    	//dd($isispbms[0]->barangs_id);
	    	//dd($hasil2->get()[0]);
	    	//dd($barangdatanghasil);
	    	$hasil2 = $hasil2->update(['stok'=>$barangdatanghasil]);

	    	$hasil3 = DB::table('barangs')->where('id',$isispbms[0]->barangs_id);
	    	$stokhasil3 = $hasil3->get()[0]->akumstok + $selisih;
	    	//dd($stokhasil3);
	    	$hasil3 = $hasil3->update(['akumstok'=> $stokhasil3 ]);

	    	return back();
	    }


	    //By the way, you can still create your own method in here... :) 


	}