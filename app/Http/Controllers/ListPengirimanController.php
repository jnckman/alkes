<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Illuminate\Support\Collection;
	use Carbon\Carbon;
	use DataTables; //NOTE utk yajra datatables WAJIB !!!
	use PDF;
	use App\Models\Surat_jalan;
	use App\Models\Tanda_terima;
	use App\Models\Pengambilan_Barang;
	use App\Models\Pengiriman_Barang;
	use App\Models\Detail_Pengambilan_Barang;
	use App\Models\Detail_Pengiriman_Barang;
	use App\Models\Barang_gudang;
	use App\Models\isi_mr_subdept;

	class ListPengirimanController extends Controller {

		public function getIndex0(){
			// if(!CRUDBooster::isView()) CRUDBooster::denyAccess();
			$data = [];
			$data['page_title'] = 'List Pengiriman';

		   return view('customView.list_pengiriman0',$data);
		}

		public function getIndex($tgl=0) {
			if($tgl ==0 )
			{
				$tgl = date('Y-m-d');
			}
			// if(!CRUDBooster::isView()) CRUDBooster::denyAccess();
		    $data = [];
			$data['page_title'] = 'List Pengiriman';

		   return view('customView.list_pengiriman',['data'=>$data,'tgl'=>$tgl]);
		}

		public function dt_master0() {
			$hasil = DB::table('pengiriman_barangs')
						->select('pengiriman_barangs.tgl_kirim')
						->groupBy('pengiriman_barangs.tgl_kirim')
						->whereNull('pengiriman_barangs.deleted_at')
						->orderBy('pengiriman_barangs.tgl_kirim','desc')
						->get();
						;
			$data=[];
			foreach ($hasil as $key => $value) {
				$data[] = ['tgl_kirim'=>$value->tgl_kirim,
						'action'=>'<a href="'.route('index.list.pengiriman.barang', $value->tgl_kirim).'" class="btn btn-success btn-sm"><i class="fa fa-bars" aria-hidden="true"></i>List Pengiriman</a>
						<a href="'.route('tanda.terima2',['0',$value->tgl_kirim]).'" class="btn btn-success btn-sm" style="margin-left:5px;"><i class="fa fa-print" aria-hidden="true"></i>Print</a>'
						];
			}
			$results = [
	              "draw" => 1,
                  "recordsTotal" => count($data),
		          "recordsFiltered" => count($data),
                  "data" => $data
                  ];
			return response()->json($results);				
		}

		public function dt_master() {
			//NOTE groupBy di pengaruhi oleh select-nya juga
			// dd('hehe');
			$request = request();
			// dd($request);
			$data = DB::table('pengiriman_barangs')
						->leftJoin('cms_users', 'pengiriman_barangs.user', '=', 'cms_users.id')
						->orderBy('pengiriman_barangs.id', 'desc')
						->select(['pengiriman_barangs.kode',
									'pengiriman_barangs.tgl_kirim',
									'pengiriman_barangs.status',
									'cms_users.name','pengiriman_barangs.id',
									'pengiriman_barangs.jenis',
									'pengiriman_barangs.global_id'
									])
						->distinct('pengiriman_barangs.kode')
						->groupBy('pengiriman_barangs.kode', 'pengiriman_barangs.tgl_kirim', 'pengiriman_barangs.status', 'cms_users.name','pengiriman_barangs.id','pengiriman_barangs.jenis','pengiriman_barangs.global_id')
						->whereNull('pengiriman_barangs.deleted_at')
						->where('pengiriman_barangs.tgl_kirim','=',$request->tgl)
						->get();
						// ->unique('kode');
			// dd($data);
			return DataTables::of($data)
					->addColumn('tujuan',function($data){
						if(strtolower(explode('_',$data->jenis)[0])=='mr'){
						    return 	DB::table('mr_subdepts')->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')->select('projects.area')->where('mr_subdepts.id','=',$data->global_id)->first()->area;
						}
						else{
							return 	DB::table('sales_orders')->leftJoin('customers','customers.id','=','sales_orders.customers_id')->select('customers.nama')->where('sales_orders.id','=',$data->global_id)->first()->nama;
						}
						
					})
					->addColumn('action', function ($data) { //untuk ambil jam saja
						if(CRUDBooster::isUpdate()){
							return '
							<a href="'.route('view.detail.pengiriman.barang', ['kode' => $data->kode]).'" class="btn btn-success btn-sm" title="Cetak Tanda Terima"><i class="fa fa-eye" aria-hidden="true"></i> Detail Pengiriman</a>';
						}
					})
					->make(true);

			// $dataz = [];
			// $tmp = '';
			// foreach ($data as $key => $value) {
			// 		$dataz[] = ['action'=>'<a href="'.route('view.detail.pengiriman.barang', ['kode' => $value->kode]).'" class="btn btn-success btn-sm" title="Cetak Tanda Terima"><i class="fa fa-eye" aria-hidden="true"></i> Detail Pengiriman</a>',
			// 				'kode'=>$value->kode,
			// 				'name'=>$value->name,
			// 				'tgl_kirim'=>$value->tgl_kirim,
			// 				'status'=>$value->status,
			// 				'jenis'=>$value->jenis,
			// 				];
			// }
			// $results = [
   //            "draw" => 1,
   //                "recordsTotal" => count($dataz),
		 //          "recordsFiltered" => count($dataz),
   //                "data" => $dataz
   //                ];
			// return response()->json($results);
		}

		public function view_detail_kirim_barang($kode) {
			$data['kode'] = $kode;

			//NOTE MR / SO, baru / retur
			$pb = DB::table('pengiriman_barangs')->where('kode', '=', $kode)->first();
			// dd($pb->jenis);

			$q = DB::table('detail_pengiriman_barangs')
				->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
				->leftJoin('barangs', 'detail_pengiriman_barangs.barang_id', '=', 'barangs.id');

			if ($pb->jenis == "mr_baru"){
				$q =	$q->leftJoin('mr_subdepts', 'pengiriman_barangs.global_id', '=', 'mr_subdepts.id')
							->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
							->select('mr_subdepts.nomr as no_nota','projects.area as no_nota2');
			}
			else if ($pb->jenis == "mr_retur"){
				dd('belum ada tabel');
			}
			else if ($pb->jenis == "so_baru"){
				$q =	$q->leftJoin('sales_orders', 'pengiriman_barangs.global_id', '=', 'sales_orders.id')
							->leftJoin('customers','customers.id','=','sales_orders.customers_id')
							->select('sales_orders.nomor as no_nota','customers.nama as no_nota2');
			}
			else if ($pb->jenis == "so_retur"){
				$q =	$q->leftJoin('returso', 'pengiriman_barangs.global_id', '=', 'returso.id')
							->leftJoin('customers','customers.id','=','sales_orders.customers_id')
							->select('returso.nomor as no_nota','customers.nama as no_nota2');
			}
			$q =	$q->where('pengiriman_barangs.kode', '=', $kode)
						->addSelect('pengiriman_barangs.kode as pengiriman_kode',
							// 'pengiriman_barangs.id as pengiriman_id',
							// 'detail_pengiriman_barangs.id as detail_id',
							'pengiriman_barangs.jenis as jenis_nota',
							'detail_pengiriman_barangs.barang_id as barang_id',
							'detail_pengiriman_barangs.jumlah as jumlah',
							'pengiriman_barangs.global_id as nota_id',
							'pengiriman_barangs.pengambilan_brg_id as pengambilan_brg_id',
							'barangs.kode as kode',
							'barangs.nama as nama')
				->get();
			$data['detail_data'] = $q;

			if(session()->has('sisa_brg')) {
				$data["barang_habis"] = 0;
				$sisa_brg = session('sisa_brg');

				foreach ($sisa_brg as $k => $v) {
					foreach ($data["detail_data"] as $k2 => $v2) {
						if ($v['barang_id'] == $v2->barang_id && $v['pengambilan_barang_id'] == $v2->pengambilan_brg_id){
							$sisa_brg[$k]['nama'] = $v2->nama;
							$sisa_brg[$k]['kode'] = $v2->kode;
							break;
						}
					}
				}
				session()->forget(['sisa_brg']);
				session(['sisa_brg' => $sisa_brg]);
			}
			else {
				$data["barang_habis"] = 1;
				// dd("tidak");
			}
			// $this->cbView('customView.detail_pengiriman',$data);
			return view('customView.detail_pengiriman',$data);
		}

		public function surat_jalan_view($kode) {
			$data = [];
			$data['kode'] = $kode;
			return view('customView.surat_jalan_view', $data);
		}

		public function surat_jalan_dt($kode) {
			$data = DB::table('detail_pengiriman_barangs')
						->leftJoin('barangs', 'detail_pengiriman_barangs.barang_id', '=', 'barangs.id')
						->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
						->where('pengiriman_barangs.kode', '=', $kode)
						->orderBy('barangs.nama')
						->groupBy('barangs.nama', 'barangs.kode', 'detail_pengiriman_barangs.barang_id')
						->select('detail_pengiriman_barangs.barang_id as barang_id',
						 			'barangs.nama as nama',
									'barangs.kode as kode',
									DB::raw('SUM(detail_pengiriman_barangs.jumlah) as jumlah'));

			return DataTables::of($data)->make(true);
		}

		public function surat_jalan_pdf($kode) {
			$data = [];
			$data['kode'] = $kode;
			$data['tanggal'] = date("Y-m-d");
			$data['kirim_barang'] = DB::table('detail_pengiriman_barangs')
						->leftJoin('barangs', 'detail_pengiriman_barangs.barang_id', '=', 'barangs.id')
						->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
						->whereNull('detail_pengiriman_barangs.deleted_at')
						->whereNull('pengiriman_barangs.deleted_at')
						->where('pengiriman_barangs.kode', '=', $kode)
						->groupBy('barangs.nama', 'detail_pengiriman_barangs.barang_id')
						->selectRaw('detail_pengiriman_barangs.barang_id as barang_id , barangs.nama as nama, sum(detail_pengiriman_barangs.jumlah) as jumlah')
						->orderBy('detail_pengiriman_barangs.barang_id', 'asc')
						->get();

			$pdf = PDF::loadView('customView.surat_jalan_pdf', ['data' => $data]);
			$pdf->setPaper('A4', 'potrait');
            $nama = "Surat-jalan-".$kode."-".$data['tanggal'].".pdf";

			$new = new Surat_jalan;
			$new->tgl_cetak = $data['tanggal'];
			$new->user = Session::get('admin_id');
			$new->pengiriman_kode = $kode;
			$new->save();
			// return view('customView.surat_jalan', $data);
         return $pdf->download($nama);
		}

		public function tanda_terima($kode,$tgl=0) {

			$data = Pengiriman_Barang::whereNull('deleted_at')->get();
			// foreach ($data as $d) {
			// 	foreach ($d->detail_pengiriman_barang as $k) {
			// 		$data = Isi_mr_subdept::where('barangs_id',$k->barang_id)->where('mr_subdepts_id',$d->global_id)->get();
			// 	}
			// }
			//echo phpinfo();
			$data = [];
			$data['tanggal'] = date("Y-m-d");

			if($tgl !=0){
				$pb = DB::table('pengiriman_barangs')->where('tgl_kirim', '=', $tgl)->first();
			}
			else{ //NOTE MR / SO, baru / retur
				$pb = DB::table('pengiriman_barangs')->where('kode', '=', $kode)->first();
			}
			
			$q = DB::table('detail_pengiriman_barangs')
					->leftJoin('barangs', 'barangs.id', '=', 'detail_pengiriman_barangs.barang_id')
					->leftJoin('satuans', 'satuans.id', '=', 'barangs.satuans_id')
					->leftJoin('pengiriman_barangs', 'pengiriman_barangs.id', '=', 'detail_pengiriman_barangs.pengiriman_barang_id');

			if ($pb->jenis == "mr_baru"){
				$q =	$q->leftJoin('mr_subdepts', 'pengiriman_barangs.global_id', '=', 'mr_subdepts.id')
						  ->leftJoin('projects', 'mr_subdepts.projects_id', '=', 'projects.id')
						  ->leftJoin('isi_mr_subdepts',function($query){
										$query->on('isi_mr_subdepts.barangs_id','=','detail_pengiriman_barangs.barang_id');
										$query->on('isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id');
										})
							->select('mr_subdepts.nomr as no_nota','projects.area','pengiriman_barangs.global_id as nota_id',
									'pengiriman_barangs.jenis as jenis',
									'detail_pengiriman_barangs.jumlah as jumlah','satuans.nama as satuan',
									'barangs.nama as nama','pengiriman_barangs.id as pebaid',
									'isi_mr_subdepts.keterangan','isi_mr_subdepts.dikirim','isi_mr_subdepts.jumlah as jumlahmr');
			}
			else if ($pb->jenis == "mr_retur"){
				dd('belum ada tabel');
			}
			else if ($pb->jenis == "so_baru"){
				$q =	$q->leftJoin('sales_orders', 'pengiriman_barangs.global_id', '=', 'sales_orders.id')
							->leftJoin('isi_sales_orders','isi_sales_orders.sales_orders_id','=','sales_orders.id')
							->leftJoin('customers','customers.id','=','sales_orders.customers_id')
							->select('sales_orders.nomor as no_nota','customers.nama as area','pengiriman_barangs.global_id as nota_id',
								'pengiriman_barangs.id as pebaid',
								'pengiriman_barangs.jenis as jenis',
								'detail_pengiriman_barangs.jumlah as jumlah',
								'isi_sales_orders.jumlah as jumlahmr',
								'isi_sales_orders.dikirim',
								'barangs.nama as nama','satuans.nama as satuan');
			}
			else if ($pb->jenis == "so_retur"){
				$q =	$q->leftJoin('returso', 'pengiriman_barangs.global_id', '=', 'returso.id')
							->select('returso.nomor as no_nota');
			}


			if($tgl == 0){
				$q = $q->where('pengiriman_barangs.kode', $kode)
						->get()->sortBy('nota_id')->groupBy('no_nota');
				
			}
			else{
				$q = $q->where('pengiriman_barangs.tgl_kirim', $tgl)
					->get()->sortBy('nota_id')->groupBy('no_nota');
			}
			// dd($q);
			$data['kirim_barang'] = $q;
			 
			$last_ID = DB::table('tanda_terimas')->orderBy('id', 'desc')->select('id')->first();
			if ($last_ID == null) $new_id = 0;
			else $new_id = $last_ID->id;

			$user = Session::get('admin_id');
			$jenis_nota = $data['kirim_barang']->first()[0]->jenis;
			$created_at = new Carbon();

			
			foreach ($data['kirim_barang'] as $v) {
				$new_id++;
				$new = [
					'id' => $new_id,
					'user' => $user,
					'tgl_cetak' => $data['tanggal'],
					'jenis_nota' => $jenis_nota,
					'dikirim' => $v[0]->dikirim,
					'nota_id' => $v[0]->nota_id,
					'created_at' => $created_at,
				];
				$items[] = $new;
			}

			// dd($q);
			// if ($pb->jenis == "mr_baru" || $pb->jenis == "mr_retur"){
			// 	return View('customView.tanda_terima',compact('data'));
			// }
			return View('customView.tanda_terima',compact('data'));
			// else if ($pb->jenis == "so_baru" || $pb->jenis == "so_retur"){
			// 	return View('customView.tanda_terima_so',compact('data'));
			// }
			 //$pdf = PDF::loadView('customView.tanda_terima', ['data' => $data]);
			// $pdf->setPaper('A3', 'potrait');
   //       $nama = "Tanda-terima-".$kode."-".$data['tanggal'].".pdf";

			//DB::table('tanda_terimas')->insert($items);
         //return $pdf->download($nama);
		}

		public function tanda_terima_single($pengiriman_id) {
			$data = [];
			$data['tanggal'] = date("Y-m-d");

			//NOTE MR / SO, baru / retur
			$pb = DB::table('pengiriman_barangs')->where('id', $pengiriman_id)->first();
			// dd($pb->jenis);

			$q = DB::table('detail_pengiriman_barangs')
					->leftJoin('barangs', 'barangs.id', '=', 'detail_pengiriman_barangs.barang_id')
					->leftJoin('satuans', 'satuans.id', '=', 'barangs.satuans_id')
					->leftJoin('pengiriman_barangs', 'pengiriman_barangs.id', '=', 'detail_pengiriman_barangs.pengiriman_barang_id');

			if ($pb->jenis == "mr_baru"){
				$q =	$q->leftJoin('mr_subdepts', 'pengiriman_barangs.global_id', '=', 'mr_subdepts.id')
							->select('mr_subdepts.nomr as no_nota');
			}
			else if ($pb->jenis == "mr_retur"){
				dd('belum ada tabel');
			}
			else if ($pb->jenis == "so_baru"){
				$q =	$q->leftJoin('sales_orders', 'pengiriman_barangs.global_id', '=', 'sales_orders.id')
							->select('sales_orders.nomor as no_nota');
			}
			else if ($pb->jenis == "so_retur"){
				$q =	$q->leftJoin('returso', 'pengiriman_barangs.global_id', '=', 'returso.id')
							->select('returso.nomor as no_nota');
			}

			$q = $q->addSelect('pengiriman_barangs.global_id as nota_id',
								'pengiriman_barangs.jenis as jenis',
								'detail_pengiriman_barangs.jumlah as jumlah',
								'barangs.nama as nama','satuans.nama as satuan')
					->where('pengiriman_barangs.id', $pengiriman_id)
					->get()->groupBy('nota_id');

			// dd($data['kirim_barang']->first());
			$data['kirim_barang'] = $q;

			$new = new Tanda_terima;
			$new->user = Session::get('admin_id');
			$new->tgl_cetak = $data['tanggal'];
			$new->jenis_nota = $data['kirim_barang']->first()[0]->jenis;
			$new->nota_id = $data['kirim_barang']->first()[0]->nota_id;
			$new->save();

			$pdf = PDF::loadView('customView.tanda_terima', ['data' => $data]);
			$pdf->setPaper('A4', 'potrait');
         $nama = "Tanda-terima-".$data['kirim_barang']->first()[0]->no_nota."-".$data['tanggal'].".pdf";

			return $pdf->download($nama);
		}

		public function kembali_barang() {
			$data = Request::except('_token');
			$pengambilan_brg_id = $data['pengambilan_barang_id'];
			unset($data['pengambilan_barang_id']);

			$q = Pengambilan_Barang::find($pengambilan_brg_id);
			$q->status = 1;
			$q->save();
			// dd($q);
			
			foreach ($data as $key => $value) {
				Detail_Pengambilan_Barang::where('pengambilan_barang_id', $pengambilan_brg_id)->where('barang_id', $key)->decrement('jumlah', (int) $value);

				Barang_gudang::where('gudangs_id', $q->gudang_id)->where('barangs_id', $key)->increment('stok', (int) $value);
			}
			// dd('selesai');
			session()->forget('sisa_brg');
			return redirect()->back()->with(['message'=>'Barang Berhasil di Kembalikan','message_type'=>'success']);
		}

		//tambahbyRase

		public function listTandaTerimaPengirimanajax(){
			$request = request();

			$list = Tanda_terima::leftJoin('cms_users','cms_users.id','=','tanda_terimas.user')
									->where('tanda_terimas.nota_id','=',$request->id)
									->where('tanda_terimas.jenis_nota','=',$request->jenis_nota)
									->select('tanda_terimas.*','cms_users.name as user')
									// ->groupBy('tanda_terimas.')
									->get()
									;

			return response()->json($list);
		}
	}
