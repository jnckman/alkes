<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Detail_Pengambilan_Barang;
	use App\Models\Pengambilan_Barang;
	use App\Models\Pengiriman_Barang;
	use App\Models\Barang;
	use App\Models\Barang_gudang;
	use App\Models\Periode_mrs;
	use App\Models\Wellhos_subdept;
	use Illuminate\Support\Collection;
	use Carbon\Carbon;

	class AdminPengirimanBarangs77Controller extends \crocodicstudio\crudbooster\controllers\CBController {
	   public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "pengiriman_barangs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Tgl Kirim","name"=>"tgl_kirim","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
			//$this->form[] = ["label"=>"Kode","name"=>"kode","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Jenis","name"=>"jenis","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Global Id","name"=>"global_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"global,id"];
			//$this->form[] = ["label"=>"User","name"=>"user","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# OLD END FORM

        	$this->sub_module = array();
	      $this->addaction = array();
	      $this->button_selected = array();
	      $this->alert        = array();
	      $this->index_button = array();
	      $this->table_row_color = array();
	      $this->index_statistic = array();
	      $this->script_js = NULL;
	      $this->pre_index_html = null;
	      $this->post_index_html = null;
	      $this->load_js = array();
	      $this->style_css = NULL;
	      $this->load_css = array();
	   }

	   public function actionButtonSelected($id_selected,$button_name) {}
	   public function hook_query_index(&$query) {}
	   public function hook_row_index($column_index,&$column_value) {}
	   public function hook_before_add(&$postdata) {}
	   public function hook_after_add($id) {}
	   public function hook_before_edit(&$postdata,$id) {}
	   public function hook_after_edit($id) {}
	   public function hook_before_delete($id) {}
	   public function hook_after_delete($id) {}

		public function getIndex() {
			if(!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			$data = [];
			$data['page_title'] = 'Pengiriman Barang';

			$q1 = DB::table('isi_mr_subdepts')//ambil jumlah barang di mr
					->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
					->select('mr_subdepts.periode_mrs_id')
					->whereNull('isi_mr_subdepts.deleted_at')
					->whereNull('mr_subdepts.deleted_at')
					->groupBy('mr_subdepts.periode_mrs_id')
					->havingRaw('IFNULL(( SUM(isi_mr_subdepts.jumlah) / NULLIF(SUM(isi_mr_subdepts.dikirim),0) * 100 ),0) < 100')
					->get();
			$periodeNotSelesai = $q1->pluck('periode_mrs_id')->toArray();

			$data['periode'] = Periode_mrs::whereNull('deleted_at')
											->whereIn('id',$periodeNotSelesai)
											->get();
			$this->cbView('customView.pengiriman_barang_pre',$data);
		}

		public function view_kirim($jenis1, $jenis2, $id,$periode = Null) {
			//NOTE jenis1 tanda MR / SO, jenis2 tanda Retur / Baru
			$data = [];
			$data['page_title'] = 'Pengiriman Barang';
			$data['jenis'] = [$jenis1, $jenis2];

			switch ($id) {
				//NOTE utk step 1 pengiriman barang
				case 1:

					$data['gudang'] = DB::table('barang_gudangs')
							->leftJoin('gudangs', 'barang_gudangs.gudangs_id', '=', 'gudangs.id')
							->groupBy('barang_gudangs.gudangs_id', 'gudangs.nama')
							->select('barang_gudangs.gudangs_id as id',
										'gudangs.nama as nama',
										DB::raw('SUM(barang_gudangs.stok) as stok_gudang'))
							->whereNull('barang_gudangs.deleted_at')
							->get();
					
					
					switch ($jenis1) {
						case 1: //NOTE MR
							switch ($jenis2) {
								case 1: //NOTE retur MR
									session(['jenis_pengiriman' => 'mr_retur']);
									dd("belom ada tabel");
									break;

								case 2: //NOTE baru MR
									session(['jenis_pengiriman' => 'mr_baru']);
									$datautama = DB::table('mr_subdepts')
											->leftJoin('projects', 'mr_subdepts.projects_id', '=', 'projects.id')
											->leftJoin('wellhos_subdepts', 'wellhos_subdepts.id', '=', 'mr_subdepts.wellhos_subdepts_id')
											->select('mr_subdepts.id',
														'mr_subdepts.nomr','mr_subdepts.periode_mrs_id as perid',
														'projects.area',
														'mr_subdepts.tanggal',
														'projects.nama','mr_subdepts.wellhos_subdepts_id as wsid','wellhos_subdepts.nama as wsnama');
									
									$data['data'] = $datautama
											// ->where('mr_subdepts.approval', '=', 'oke')
											//->where('approval','LIKE','oke')
											->whereNull('mr_subdepts.deleted_at')
											->where('mr_subdepts.periode_mrs_id',$periode)
											->orderBy('tanggal', 'asc')
											->get();
									//dd($data);
									$data['subdept'] = Wellhos_subdept::whereNull('deleted_at')->get();
									//DB::table('wellhos_subdepts')->whereNull("deleted_at")->get();
									$data['periode'] = Periode_mrs::whereNull('deleted_at')->get();
									$data['periodeid'] = $periode;
									//DB::table('periode_mrs')->whereNull("deleted_at")->get();
									// $sorted = array();
									// foreach ($data['data'] as $d) {
									//     $sdid = $d->wsid;
									    
									//     if (isset($sorted[$sdid])) {
									//         $sorted[$sdid][] = $d;
									//     } else {
									//         $sorted[$sdid] = array($d);
									//     }
									// }
									// dd($sorted);
									// $data['subdept'] = $datautama->groupBy('wellhos_subdepts_id','mr_subdepts.id',
									// 					'mr_subdepts.nomr',
									// 					'projects.area',
									// 					'mr_subdepts.tanggal',
									// 					'projects.nama','mr_subdepts.wellhos_subdepts_id','wellhos_subdepts.nama')->where('mr_subdepts.approval', '=', 'oke')
									// 		->orderBy('tanggal', 'asc')
									// 		->get();
									//dd($data);
									foreach ($data['data'] as $key => $value) {
										$nota_id[] = $value->id;
									}

									$terkirim = DB::table('detail_pengiriman_barangs')
										->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
										->whereNull('pengiriman_barangs.deleted_at')
										->whereNull('detail_pengiriman_barangs.deleted_at')
										->whereIn('pengiriman_barangs.global_id', $nota_id)
										->where('pengiriman_barangs.jenis', '=', 'mr_baru')
										->groupBy('pengiriman_barangs.global_id')
									   ->selectRaw('sum(detail_pengiriman_barangs.jumlah) as total, pengiriman_barangs.global_id as nota_id')
										->get();
									// dd($terkirim);
									if ($terkirim->isNotEmpty()) {
										$isi = DB::table('isi_mr_subdepts')
											->whereIn('isi_mr_subdepts.mr_subdepts_id', $nota_id)
											->groupBy('isi_mr_subdepts.mr_subdepts_id')
											->whereNull('isi_mr_subdepts.deleted_at')
										   ->selectRaw('sum(isi_mr_subdepts.jumlah) as total, isi_mr_subdepts.mr_subdepts_id as nota_id')
											->get();
										
										$i=0;
										foreach ($terkirim as $key => $value) {
											foreach ($isi as $key2 => $value2) {
												if ($value->nota_id == $value2->nota_id){
													//dd($value->nota_id.'-'.$value2->nota_id.'===='.$value->total.'-'.$value2->total);
													$x =  number_format(($value->total / $value2->total) * 100);
													
													$t[$i] = ['nota_id' => $value->nota_id, 'persen' => $x];
													$i++;
												}
											}
										}
										
										foreach ($t as $key => $value) {
											foreach ($data['data'] as $key2 => $value2) {
												//dd($value['persen'].'-'.$value2->id);
												//dd($value->nota_id.'-'.$value2->nota_id.'===='.$value->total.'-'.$value2->total);

												if ($value['nota_id'] == $value2->id){
													//$index[] = $key2;
													$data['data'][$key2]->persen = $value['persen'];

												}
												// else if (!in_array($key2, $index)){
												
											}
										}
										
										foreach ($data['data'] as $value) {
											foreach ($data['subdept'] as $key => $value2) {
												if($value->wsid==$value2->id){
													$data['subdept'][$key]['progress'] = $belum++;
													

												}
											}
										}
										$belum = 0;
										
										
									}
									else {
										foreach ($data['data'] as $key2 => $value2) {
											$data['data'][$key2]->persen = 0;
										}
									}
									
									$this->cbView('customView.pengiriman_barang_1',$data);
									break;
							}
							
							break;

						case 2: //NOTE SO

							switch ($jenis2) {
								case 1: //NOTE retur SO
									session(['jenis_pengiriman' => 'so_retur']);

									$data['data'] = DB::table('returso')
											->select('returso.id',
														'returso.nomor',
														'returso.created_at')
											->where('returso.status', '!=', 'selesai')
											->whereNull('returso')
											->orderBy('created_at', 'asc')
											->get();

									foreach ($data['data'] as $key => $value) {
										$nota_id[] = $value->id;
									}


									$terkirim = DB::table('detail_pengiriman_barangs')
										->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
										->whereNull('deleted_at')
										->whereIn('pengiriman_barangs.global_id', $nota_id)
										->where('pengiriman_barangs.jenis', '=', 'so_retur')
										->groupBy('pengiriman_barangs.global_id')
									   ->selectRaw('sum(detail_pengiriman_barangs.jumlah) as total, pengiriman_barangs.global_id as nota_id')
										->get();

									if ($terkirim->isNotEmpty()){
										$isi = DB::table('isi_returso')
											->whereIn('isi_returso.returSO_id', $nota_id)
											->whereNull('isi_returso.deleted_at')
											->groupBy('isi_returso.returSO_id')
										   ->selectRaw('sum(isi_returso.jumlah) as total, isi_returso.returSO_id as nota_id')
											->get();

										$i=0;
										foreach ($terkirim as $key => $value) {
											foreach ($isi as $key2 => $value2) {
												if ($value->nota_id == $value2->nota_id){
													$x = (int) ($value->total / $value2->total * 100);
													// dd($x);
													$t[$i] = ['nota_id' => $value->nota_id, 'persen' => $x];
													$i++;
												}
											}
										}

										foreach ($t as $key => $value) {
											foreach ($data['data'] as $key2 => $value2) {
												if ($value['nota_id'] == $value2->id){
													$index[] = $key2;
													$data['data'][$key2]->persen = $value['persen'];
												}
												else if (!in_array($key2, $index)){
													$data['data'][$key2]->persen = 0;
												}
											}
										}
									}
									else {
										foreach ($data['data'] as $key2 => $value2) {
											$data['data'][$key2]->persen = 0;
										}
									}

									break;

								case 2: //NOTE baru SO

									session(['jenis_pengiriman' => 'so_baru']);

									$data['data'] = DB::table('sales_orders')
											->leftJoin('customers','customers.id','=','sales_orders.customers_id')

											->select('sales_orders.id',
														'sales_orders.nomor',
														'sales_orders.created_at','customers.nama','customers.alamatpemilik')

											->where('sales_orders.status', '!=', 'selesai')
											->whereNull('sales_orders.deleted_at')
											->orderBy('sales_orders.created_at', 'asc')
											->get();

									foreach ($data['data'] as $key => $value) {
										$nota_id[] = $value->id;
									}

									$terkirim = DB::table('detail_pengiriman_barangs')
										->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
										->whereNull('detail_pengiriman_barangs.deleted_at')
										->whereIn('pengiriman_barangs.global_id', $nota_id)
										->where('pengiriman_barangs.jenis', 'LIKE', '%so_baru%')
										->groupBy('pengiriman_barangs.global_id')
									   ->selectRaw('sum(detail_pengiriman_barangs.jumlah) as total, pengiriman_barangs.global_id as nota_id')
										->get();
									
									if ($terkirim->isNotEmpty()) {

										$isi = DB::table('isi_sales_orders')
											->leftJoin('sales_orders','sales_orders.id','=','isi_sales_orders.sales_orders_id')
											->leftJoin('customers','customers.id','=','sales_orders.customers_id')
											->whereIn('isi_sales_orders.sales_orders_id', $nota_id)
											->whereNull('isi_sales_orders.deleted_at')
											//->whereNull('customers.deleted_at')
											->groupBy('isi_sales_orders.sales_orders_id','customers.nama','sales_orders.nomor')
										   ->selectRaw('sum(isi_sales_orders.jumlah) as total, isi_sales_orders.sales_orders_id as nota_id,customers.nama as nama,sales_orders.nomor as nomorso')
											->get();
										
										$i=0;

										foreach ($terkirim as $key => $value) {
											foreach ($isi as $key2 => $value2) {
												if ($value->nota_id == $value2->nota_id){
													$x = (int) ($value->total / $value2->total * 100);
													 
													$t[$i] = ['nota_id' => $value->nota_id, 'persen' => $x,'nama'=>$value2->nama];
													$i++;
												}
											}
										}
										
										foreach ($t as $key => $value) {
											foreach ($data['data'] as $key2 => $value2) {
												$index[] = $key2;
												if ($value['nota_id'] == $value2->id){
													
													$data['data'][$key2]->persen = $value['persen'];
													$data['data'][$key2]->nama = $value2->nama;
												}
												else if (!in_array($key2, $index)){
													$data['data'][$key2]->persen = 0;
													$data['data'][$key2]->nama = $value2->nama;
												}
											}
										}



									}
									else {
										foreach ($data['data'] as $key2 => $value2) {
											$data['data'][$key2]->persen = 0;

										}
									}
									$data['jenispengiriman'] = session('jenispengiriman');
									
									// dd(request());
									$this->cbView('customView.pengiriman_barang_1_so',$data);		
									break;
							}
							
							break;
					}

					 
					// $this->cbView('customView.pengiriman_barang_1',$data);
					break;

				//NOTE utk step 2 pengambilan barang
				case 2:
					$d = session('view2_kirim_barang');
					
					if (!$d) {
						CRUDBooster::redirect(route('view.kirim', [$jenis1, $jenis2, 2,0]),'');
					}
					else {
						//NOTE data yg pasti sama
						$data['gudang_id'] = $d['gudangs_id'];
						$data['periode'] = $d['gudang_id'];
						
						//$data['periode'] = $d['']
						unset($d['gudangs_id']);
						$data['nota_id'] = $d;
						 
						
						$data['gudang'] = DB::table('gudangs')->where('id', '=', $data['gudang_id'])->first();

						$data['barang_gudang'] = DB::table('barang_gudangs')
								->whereNull('barang_gudangs.deleted_at')
								->where('gudangs_id', $data['gudang_id'])
								->select('barangs_id as barang_id',
											'stok')
								->get();

						switch ($jenis1) {
							case 1: //NOTE jenis1 MR
								switch ($jenis2) {
									case 1: //NOTE retur MR
										dd("belom ada tabel");
										break;

									case 2: //NOTE baru MR
										// $x = DB::table("isi_mr_subdepts")
										// 	->leftJoin('mr_subdepts', 'isi_mr_subdepts.mr_subdepts_id', '=', 'mr_subdepts.id')
										// 	->select('mr_subdepts.id','isi_mr_subdepts.jumlah','mr_subdepts.nomr')
										// 	->whereNull('isi_mr_subdepts.deleted_at')
										// 	->whereNull('mr_subdepts.deleted_at')
										// 	->where('isi_mr_subdepts.barangs_id',2)
										// 	->where('mr_subdepts.periode_mrs_id',6)
										// 	->where('mr_subdepts.wellhos_subdepts_id',4)

										// 	->get();
										
										$data['detail_data'] = 
										DB::table('isi_mr_subdepts')
											->leftJoin('mr_subdepts', 'isi_mr_subdepts.mr_subdepts_id', '=', 'mr_subdepts.id')
											->leftJoin('projects', 'mr_subdepts.projects_id', '=', 'projects.id')
											->leftJoin('barangs', 'isi_mr_subdepts.barangs_id', '=', 'barangs.id')

											->whereIn('isi_mr_subdepts.mr_subdepts_id', $data['nota_id'])

											->where('mr_subdepts.periode_mrs_id',$data['periode'])
											//->where('isi_mr_subdepts.barangs_id',2)

											->whereNull('isi_mr_subdepts.deleted_at')
											->whereNull('mr_subdepts.deleted_at')
											->select('isi_mr_subdepts.barangs_id as barang_id',
														'isi_mr_subdepts.id as isi_nota_id',
														'isi_mr_subdepts.jumlah',
														'isi_mr_subdepts.mr_subdepts_id as nota_id',
														'barangs.nama as nama',
														'projects.area',
														'mr_subdepts.nomr as nomor_nota')
											->get();
										
										$barang_terkirim = DB::table('detail_pengiriman_barangs')
											->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
											->where('pengiriman_barangs.jenis', '=', 'mr_baru')
											->whereNull('pengiriman_barangs.deleted_at')
											->whereNull('detail_pengiriman_barangs.deleted_at')
											->whereIn('pengiriman_barangs.global_id', $data['nota_id'])
											->select('detail_pengiriman_barangs.id',
														'pengiriman_barangs.global_id as nota_id',
														'detail_pengiriman_barangs.barang_id',
														'detail_pengiriman_barangs.jumlah',
														'pengiriman_barangs.tgl_kirim')
											->get();
										
										if($barang_terkirim->isNotEmpty()) {
											
											//NOTE lgsg kurangin hitung jumlah barang yg diperlukan
											foreach ($data['detail_data'] as $index => $v) {
												foreach ($barang_terkirim as $index2 => $x) {

													if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id) {
														$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
														unset($barang_terkirim[$index2]);
													}
												}
											}
										}
										//dd($data['']);
										session(['barang_terkirim' => $barang_terkirim->toArray()]);
										break;
										//dd($barang_terkirim);
								}
								break;

							case 2: //NOTE jenis1 SO
								switch ($jenis2) {
									case 1: //NOTE retur SO
										$data['detail_data'] = DB::table('isi_returso')
											->leftJoin('returso', 'isi_returso.returSO_id', '=', 'returso.id')
											->leftJoin('isi_sales_orders', 'isi_returso.isi_sales_order_id', '=', 'isi_sales_orders.id')
											->leftJoin('barangs', 'isi_sales_orders.barangs_id', '=', 'barangs.id')
											->leftJoin('customers','customers.id','=','sales_orders.customers_id')
											->whereIn('isi_returso.returSO_id', $data['nota_id'])
											->whereNull('isi_returso.deleted_at')
											->select('isi_sales_orders.barangs_id as barang_id',
														'isi_returso.id as isi_nota_id',
														'isi_returso.jumlah',
														'isi_returso.returSO_id as nota_id',
														'barangs.nama as nama',
														'returso.nomor as nomor_nota'
														,'customers.nama as area'
													)
											->get();

											$barang_terkirim = DB::table('detail_pengiriman_barangs')
												->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
												->where('pengiriman_barangs.jenis', '=', 'so_retur')
												->whereNull('detail_pengiriman_barangs.deleted_at')
												->whereIn('pengiriman_barangs.global_id', $data['nota_id'])
												->select('detail_pengiriman_barangs.id',
															'pengiriman_barangs.global_id as nota_id',
															'detail_pengiriman_barangs.barang_id',
															'detail_pengiriman_barangs.jumlah',
															'pengiriman_barangs.tgl_kirim')
												->get();
											
											if($barang_terkirim->isNotEmpty()) {
												// dd($barang_terkirim);
												session(['barang_terkirim' => $barang_terkirim->toArray()]);
												//NOTE lgsg kurangin hitung jumlah barang yg diperlukan
												foreach ($data['detail_data'] as $index => $v) {
													foreach ($barang_terkirim as $index2 => $x) {
														if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id) {
															$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
															unset($barang_terkirim[$index2]);
														}
													}
												}
											}
										break;

									case 2: //NOTE baru SO
										$data['detail_data'] = DB::table('isi_sales_orders')
											->leftJoin('sales_orders', 'isi_sales_orders.sales_orders_id', '=', 'sales_orders.id')
											->leftJoin('barangs', 'isi_sales_orders.barangs_id', '=', 'barangs.id')
											->leftJoin('customers','customers.id','=','sales_orders.customers_id')
											->whereIn('isi_sales_orders.sales_orders_id', $data['nota_id'])
											->select('isi_sales_orders.barangs_id as barang_id',
														'isi_sales_orders.id as isi_nota_id',
														'isi_sales_orders.jumlah',
														'isi_sales_orders.sales_orders_id as nota_id',
														'barangs.nama as nama',
														'sales_orders.nomor as nomor_nota'
														,'customers.nama as area'
													)
											->whereNull('isi_sales_orders.deleted_at')
											->get();


										$barang_terkirim = DB::table('detail_pengiriman_barangs')
											->leftJoin('pengiriman_barangs', 'detail_pengiriman_barangs.pengiriman_barang_id', '=', 'pengiriman_barangs.id')
											->where('pengiriman_barangs.jenis', '=', 'so_baru')
											->whereNull('detail_pengiriman_barangs.deleted_at')
											->whereIn('pengiriman_barangs.global_id', $data['nota_id'])
											->select('detail_pengiriman_barangs.id',
														'pengiriman_barangs.global_id as nota_id',
														'detail_pengiriman_barangs.barang_id',
														'detail_pengiriman_barangs.jumlah',
														'pengiriman_barangs.tgl_kirim')
											->get();
										// dd($barang_terkirim);
										if($barang_terkirim->isNotEmpty()) {
											session(['barang_terkirim' => $barang_terkirim->toArray()]);
											//NOTE lgsg kurangin hitung jumlah barang yg diperlukan
											foreach ($data['detail_data'] as $index => $v) {
												foreach ($barang_terkirim as $index2 => $x) {
													if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id) {
														$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
														unset($barang_terkirim[$index2]);
													}
												}
											}
										}

										break;
								}
								break;
						}

						$this->cbView('customView.pengiriman_barang_2',$data);
					}
					break;

				//NOTE utk step 3 pembagian barang
				case 3:
					$d = session('view3_kirim_barang');
					
					$data['barang_terambil'] = collect($d['pengambilan_barang']);
					//dd($d);
					$barang_terkirim = session('barang_terkirim');
					// dd($barang_terkirim);
					switch ($jenis1) {
						case 1: //NOTE jenis1 MR
							switch ($jenis2) {
								case 1: //NOTE retur MR
									dd("belom ada tabel");
									break;

								case 2: //NOTE baru MR

									$data['detail_data'] = DB::table('isi_mr_subdepts')
											->leftJoin('mr_subdepts', 'isi_mr_subdepts.mr_subdepts_id', '=', 'mr_subdepts.id')
											->leftJoin('projects', 'mr_subdepts.projects_id', '=', 'projects.id')
											->leftJoin('barangs', 'isi_mr_subdepts.barangs_id', '=', 'barangs.id')
											->where('mr_subdepts.periode_mrs_id',$d['periode'])
											->whereIn('isi_mr_subdepts.mr_subdepts_id', $d['nota_id'])
											//->whereIn('isi_mr_subdepts.barangs_id', $d['barang_id_all'])
											//->where('isi_mr_subdepts.mr_subdepts_id',106)
											->whereNull('mr_subdepts.deleted_at')
											->whereNull('isi_mr_subdepts.deleted_at')
											->select('isi_mr_subdepts.barangs_id as barang_id',
														'isi_mr_subdepts.mr_subdepts_id as nota_id',
														'isi_mr_subdepts.id as isi_nota_id',
														'isi_mr_subdepts.jumlah',
														'mr_subdepts.nomr as nomor_nota','projects.area as namacust',
														'barangs.nama')
											->get();
									//dd($data);
									//dd($data);
									// dd($data['detail_data']->unique('barang_id'));

											
									if($barang_terkirim != null){
										// dd($barang_terkirim);
										//NOTE hitung total supaya tidak melebihi
										foreach ($data['detail_data'] as $index => $v) {
											foreach ($barang_terkirim as $index2 => $x) {
												if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id){

													$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
													// dd($data['detail_data'][$index]->jumlah);
													unset($barang_terkirim[$index2]);
													if ($data['detail_data'][$index]->jumlah == 0){
														unset($data['detail_data'][$index]);
													}
													break;
												}
											}
										}
									}
									// dd($data['detail_data']->unique('barang_id')->sortBy('nama'));
									break;
							}
							break;

						case 2: //NOTE jenis1 SO
							switch ($jenis2) {
								case 1: //NOTE retur SO
									$data['detail_data'] = DB::table('isi_returso')
											->leftJoin('returso', 'isi_returso.returSO_id', '=', 'returso.id')
											->leftJoin('isi_sales_orders', 'isi_returso.isi_sales_order_id', '=', 'isi_sales_orders.id')
											->leftJoin('barangs', 'isi_sales_orders.barangs_id', '=', 'barangs.id')
											->whereIn('isi_returso.returSO_id', $d['nota_id'])
											->whereIn('isi_sales_orders.barangs_id', $d['barang_id_all'])
											->select('isi_sales_orders.barangs_id as barang_id',
														'isi_returso.returSO_id as nota_id',
														'isi_returso.id as isi_nota_id',
														'isi_returso.jumlah',
														'returso.nomor as nomor_nota',
														'barangs.nama')
											->get();

									if($barang_terkirim != null){
										//NOTE hitung total supaya tidak melebihi
										foreach ($data['detail_data'] as $index => $v) {
											foreach ($barang_terkirim as $index2 => $x) {
												if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id){
													$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
													unset($barang_terkirim[$index2]);
													if ($data['detail_data'][$index]->jumlah == 0){
														unset($data['detail_data'][$index]);
													}
												}
											}
										}
									}
									break;

								case 2: //NOTE baru SO
									$data['detail_data'] = DB::table('isi_sales_orders')
											->leftJoin('sales_orders', 'isi_sales_orders.sales_orders_id', '=', 'sales_orders.id')
											->leftJoin('customers', 'customers.id', '=', 'sales_orders.customers_id')
											->leftJoin('barangs', 'isi_sales_orders.barangs_id', '=', 'barangs.id')
											->whereIn('isi_sales_orders.sales_orders_id', $d['nota_id'])
											->whereIn('isi_sales_orders.barangs_id', $d['barang_id_all'])
											->select('isi_sales_orders.barangs_id as barang_id',
														'isi_sales_orders.sales_orders_id as nota_id',
														'isi_sales_orders.id as isi_nota_id',
														'isi_sales_orders.jumlah',
														'sales_orders.nomor as nomor_nota',
														'barangs.nama','customers.nama as namacust')
											->get();

									if($barang_terkirim != null){
										//NOTE hitung total supaya tidak melebihi
										foreach ($data['detail_data'] as $index => $v) {
											foreach ($barang_terkirim as $index2 => $x) {
												if ($v->barang_id == $x->barang_id && $v->nota_id == $x->nota_id){
													$data['detail_data'][$index]->jumlah = $data['detail_data'][$index]->jumlah - $x->jumlah;
													unset($barang_terkirim[$index2]);
													if ($data['detail_data'][$index]->jumlah == 0){
														unset($data['detail_data'][$index]);
													}
												}
											}
										}
									}
									break;
							}
							break;
					}

					$this->cbView('customView.pengiriman_barang_3',$data);
					break;
			}
		}

		public function reversepengambilan($id){
			$data = DB::table('pengambilan_barangs')
			->leftJoin('detail_pengambilan_barangs','pengambilan_barangs.id','=','detail_pengambilan_barangs.pengambilan_barang_id')->where('id',$id)->get();
			foreach ($data as $d) {
				echo "string";
			}
		}

		//NOTE action_kirim_barang handle semua proses yg diperlukan sebelum lanjut view_kirim selanjutnya
		public function action_kirim_barang($id) {
			switch ($id) {
				case 1:

					$data = Request::except('_token');

					$jenis_pengiriman = session('jenis_pengiriman');

					//dd($jenis_pengiriman);
					 
					$jenis = $data['jenis'];
					unset($data['jenis']);
					
					
					session(['view2_kirim_barang' => $data]);
					CRUDBooster::redirect(route('view.kirim', [$jenis[0], $jenis[1], 2,0]),'');
					break;

				case 2:
					$data = Request::except('_token');
					$jenis_pengiriman = session('jenis_pengiriman');
					
					//dd('x');
					//$periode_id = $data['gudang_id'];
					
					$gudang_id = $data['gudang_id'];
					$nota_id = $data['nota_id'];
					$jenis = $data['jenis'];
					$periode = $data['periode'];
					//NOTE $data['barang'] indexnya merupakan barang_id dan isinya berupa jumlah barang
					$barang_id = array_keys($data['barang']);
					$s = ['barang_id_all' => $barang_id,
							'nota_id' => $nota_id,
							'pengambilan_barang' => $data['barang'],
							'gudang_id' => $gudang_id,
							'periode'=>$periode];

					session(['view3_kirim_barang' => $s]);
					// dd($barang_id);
					CRUDBooster::redirect(route('view.kirim', [$jenis[0], $jenis[1], 3,0]),'');
					break;

				case 3:

					$data = Request::except('_token');

					$exploded = explode('&', $data['input']);
					$dataku = [];
					// dd($exploded);
					// $exploded = array_slice($exploded, 5); 
			        foreach ($exploded as $row) {
			            $temp = array();
			            parse_str($row, $temp);
			            list($key, $value) = each($temp);
			            if(!empty($value)) {
			            	if($key=='input'){
			            		$k = key($value);
				            	$k2 = key($value[$k]);
				                $dataku[$key][$k][$k2] = $value[$k][$k2];
			            	}
			            	else{
			            		if(!empty($key)) {
			            			if(is_array($value)){
			            				$k = key($value);
						                $dataku[$key][] = $value[$k];
			            			}
			            			else{
			            				$dataku[$key] = $value;
			            			}
					            }
			            	}
			            }
			            // dd($value);
			        }   
					// dd($dataku);
					// unserialize($data['input']);


					// dd($data['input']);
					// return json_encode($data['input']);
					// $jenis = $data['jenis'];
					// $barang_habis = $data['barang_habis'];
					// unset($data['jenis'], $data['barang_habis']);
					$jenis = $dataku['jenis'];
					$barang_habis = $dataku['barang_habis'];
					unset($dataku['jenis'], $dataku['barang_habis']);

					$jenis_pengiriman = session('jenis_pengiriman');
					//$jenis_pengiriman = session('jenis_pengiriman');

					//dd($jenispengiriman);
					//dd($jenis .' - '.$jenispengiriman);

					$brg_ambil = session('view3_kirim_barang');
					
					
					//NOTE pengambilan_barangs
					$tgl = date("Y-m-d");
					$user_id = Session::get('admin_id');
					$created_at = new Carbon();

					$new_ambil_brg = new Pengambilan_Barang;
					$new_ambil_brg->tgl_ambil = $tgl;
					$new_ambil_brg->user = $user_id;
					$new_ambil_brg->gudang_id = $brg_ambil['gudang_id'];





					if ($barang_habis == 1) $new_ambil_brg->status = 1;
					$new_ambil_brg->save();

					//NOTE detail_pengambilan_barangs
					for ($i=0; $i < count($brg_ambil['pengambilan_barang']); $i++) {

						$new2 = [
							'pengambilan_barang_id' => $new_ambil_brg->id,
							'barang_id' => $brg_ambil['barang_id_all'][$i],
							'jumlah' => (int) $brg_ambil['pengambilan_barang'][$brg_ambil['barang_id_all'][$i]]['jumlah'],
							'created_at' => $created_at,
							// 'periode'=>$periode_id
						];

						$items_ambil[] = $new2;
						

						$bg = Barang_gudang::where('gudangs_id',$brg_ambil['gudang_id'])->where('barangs_id',$new2['barang_id'])->get()[0];

						$bg['stok'] = $bg['stok'] - $new2['jumlah'];
						$bg->save();

						$b = Barang::where('id',$new2['barang_id'])->get()[0];
						$b['akumstok'] = $b['akumstok'] - $new2['jumlah'];
						$b->save();						

						
					}
					// dd($brg_ambil);
					DB::table('detail_pengambilan_barangs')->insert($items_ambil);

					//NOTE filter data yg tidak NULL
					$data = array_filter($data, function($value) { return $value !== null; });
					$mrID_barangID = array_keys($data);
					//dd($mrID_barangID);
					//NOTE $mrID_barangID_split, index 0->mr_subdept_id, 1->barang_id, 2->jumlah
					$i=0;
					// foreach ($mrID_barangID as $x) {
					// 	$mrID_barangID_split[$i] = explode("&", $x);
					// 	$notaid = $data['nomornota'][$i];
					// 	$barangid = $data['barangid'][$i];

						
					// 	array_push($mrID_barangID_split[$i], $data[$mrID_barangID[$i]]);
					// 	$mr_subdept_id_jenis[$i] = $mrID_barangID_split[$i][0];
					// 	$i++;
					// }

					//NOTE kode utk pengiriman supayaa bisa grouping
					

					// $kode = "PB".$jenis."-".$no_kirim.'-'.date('dmY');
					// $kode = "PB-".$no_kirim;
					

					//NOTE utk pengiriman_barangs
					// $mr_subdept_id_jenis = array_unique($mr_subdept_id_jenis);


					$result = array();


					// $input = $data['input'];
					$input = $dataku['input'];
					$result = array();
					// dd($input);
					foreach ($input as $key => $data) {
					  // $id = $data['nomornota'];
					  $id = $key;
					  // dd($data);
					  if (isset($result[$id])) {
					     $result[$id][] = $data;
					  } else {
					     $result[$id] = array($data);
					  }
					}
					

					// dd($result);
					// foreach ($result as $key => $i) {
					foreach ($dataku['input'] as $key => $i) {
						// dd($key);
						$no_kirimdata = DB::table('pengiriman_barangs')->orderBy('id', 'desc')->select('id')->first()->id;
						$no_kirim = $no_kirimdata + 1;
						
						if($jenis[0] == 1)
						$kode = "MR-PB-".$no_kirim.'-'.date('dmY');
						else
						$kode = "SO-PB-".$no_kirim.'-'.date('dmY');
						// $kode = "PB-".$no_kirim;


						$new_kirim_barang = new Pengiriman_Barang;
						$new_kirim_barang->tgl_kirim = $tgl;
						if($jenis[0] == 1)
						$new_kirim_barang->jenis = 'mr_baru';
						else
						$new_kirim_barang->jenis = 'so_baru';
						$new_kirim_barang->global_id = $key;
						$new_kirim_barang->user = $user_id;
						$new_kirim_barang->kode = $kode;

						$new_kirim_barang->pengambilan_brg_id = $new_ambil_brg->id;
						$new_kirim_barang->save();


						//NOTE detail_pengiriman_barangs
						$ct = 0;
						foreach ($i as $k2 => $subi) {

							// dd($subi);
							if(isset($subi)){
								$new2 = [
									'pengiriman_barang_id' => $new_kirim_barang->id,
									// 'barang_id' => $subi['barangid'],
									'barang_id' => $k2,
									// 'jumlah' => $subi['jumlah'],
									'jumlah' => (int)$subi,
									'created_at' => $created_at
								];

								// update dikirim di isi MR
								if($kode[0]=='M'){
									$beforesave = DB::table('isi_mr_subdepts')
									->where('isi_mr_subdepts.mr_subdepts_id',$key)
									->where('isi_mr_subdepts.barangs_id',$k2);
									$cekjumlah = $beforesave->get()[0]->dikirim;
									$beforesave->update(['dikirim'=>(int)$subi + $cekjumlah]);
									
									
								}
								else{
									$beforesave = DB::table('isi_sales_orders')
									->where('isi_sales_orders.sales_orders_id',$key)
									->where('isi_sales_orders.barangs_id',$k2);
									$cekjumlah = $beforesave->get()[0]->dikirim;
									$beforesave->update(['dikirim'=>(int)$subi + $cekjumlah]);
									
								}
								$items_kirim[] = $new2;
							}
							$ct++;
						}

						//sudo docker exec -it sql_server_demo lssudo docker exec -it d4b35c531079 /opt/mssql-tools/bin/sqlcmd    -S localhost -U SA -P reallyStrongPwd123
						// foreach ($mrID_barangID_split as $index => $y) {reallyStrongPwd123
						// 	if ($x == $y[0]) {
						// 		$new2 = [
						// 			'pengiriman_barang_id' => $new_kirim_barang->id,
						// 			'barang_id' => $y[1],
						// 			'jumlah' => $y[2],
						// 			'created_at' => $created_at
						// 		];
						// 		$items_kirim[] = $new2;
						// 		unset($mrID_barangID_split[$index]);
						// 	}
						// }
						

					}
					DB::table('detail_pengiriman_barangs')->insert($items_kirim);

					if ($barang_habis == 0) {
						$sisa_brg = $items_ambil;
						$c_brg_kirim = collect($items_kirim)->groupBy('barang_id');

						// foreach ($sisa_brg as $k => $v) {
						// 	foreach ($c_brg_kirim as $k2 => $v2) {
						// 		if ($v['barang_id'] == $v2[0]['barang_id']){
						// 			$sisa_brg[$k]['jumlah'] -= $v2->sum('jumlah');
						// 			if ($sisa_brg[$k]['jumlah'] == 0) unset($sisa_brg[$k]);
						// 			unset($c_brg_kirim[$k2]);
						// 			break;
						// 		}
						// 	}
						// }
						// dd($sisa_brg);
						session(['sisa_brg' => $sisa_brg]);
						// dd("123");
					}
					session()->forget(['jenis_pengiriman', 'barang_terkirim', 'view3_kirim_barang', 'view2_kirim_barang']);
					CRUDBooster::redirect(route('view.detail.pengiriman.barang', $kode),'');
					break;
			}


		}
		public function rollbackpengiriman($id){
				// 1. ambil data pengiriman 
				// 2. baca isi pengiriman 
				// 3. cek apakah so / mr 
				// 3.1 ambil pengiriman jenis baca 2 huruf di depan
				// 4. baca global id nya 
				// 5. ambil data so / mr 
				// 6. baca apakah ada pengiriman dengan global id yang sama 
				// 6.1 jika iya maka baca isi pengiriman dan akumulasikan untuk masing masing barang
				// 6.1.1 bandingkana dengan barang diterima di so/mr apakah sudah sama atau belum
				// 6.1.1.1 apabila sama maka kurangkan sesuai dengna data pengiriman pertama
				// 6.1.1.2 apabila tidak sama set dengan akumulasi total kemudian kurangkan denga data pengiriman pertama
				// 6.2 jika tidak ada yang sama langsung kurangkan 

				// mr : 3 , 7 , 9 , 10
				// ip : 3 , 2
				// rollback : 0, 5 , 9, 10

				$p = DB::table('pengiriman_barangs')->where('id',$id)->get()[0]; //step 1

				
				
				if($p->jenis[0]=='m') //step 3
				{
					$p2id = array();
					$p2 = DB::table('pengiriman_barangs')->where('global_id',$p->global_id)->get(); // step 5
					foreach($p2 as $subp2)
						$p2id[] = $subp2->id;


					$ip2 = DB::table('detail_pengiriman_barangs')
					->select(DB::raw('sum(jumlah) as jmlh'),'barang_id')
					->groupBy('barang_id')
					->whereIn('pengiriman_barang_id',$p2id)->whereNull('deleted_at')
					->orderBy('barang_id','desc')
					->get(); //step2
					//dd($p->global_id);
					$j = DB::table('isi_mr_subdepts')->whereNull('deleted_at')->where('mr_subdepts_id',$p->global_id)->orderBy('barangs_id','desc')->get(); 
					
					$ct = 0;
					$betul = true;
					dd($j);
					foreach($ip2 as $iip){
							if($iip->jmlh != $j[$ct]->dikirim)
									
								
							//dd($iip->jmlh.' - diterima : '.$j[$ct]->dikirim.' -  urutan : '.$ct);
						$ct++;
					}
					dd($betul);

				}
				else{
					$p2 = DB::table('pengiriman_barangs')->where('global_id',$p->global_id)->get();

					$j = DB::table('isi_sales_orders')->where('sales_orders_id',$p->global_id)->get();
					dd($j);
				}
		}

		public function pengirimanStatusKirim(){
			$data = [];
			$data['page_title'] = 'Pengiriman Status Kirim';

			$q1 = DB::table('isi_mr_subdepts')//ambil jumlah barang di mr
					->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
					->select('mr_subdepts.periode_mrs_id')
					->whereNull('isi_mr_subdepts.deleted_at')
					->whereNull('mr_subdepts.deleted_at')
					->groupBy('mr_subdepts.periode_mrs_id')
					->havingRaw('IFNULL(( SUM(isi_mr_subdepts.jumlah) / NULLIF(SUM(isi_mr_subdepts.dikirim),0) * 100 ),0) < 100')
					->get();

			$this->cbView('customView.pengiriman_statuskirim',$data);
		}

		public function pengirimanStatusKirim_mr(){
			$hasil = DB::table('mr_subdepts')
						->leftJoin('isi_mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
						->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
						->leftJoin('cms_users','mr_subdepts.user_id','=','cms_users.id')
						->select('mr_subdepts.id','mr_subdepts.nomr','tanggal','projects.area','cms_users.name'
							,DB::raw('TRUNCATE(IFNULL(( SUM(isi_mr_subdepts.jumlah) / NULLIF(SUM(isi_mr_subdepts.dikirim),0) * 100 ),0),2) as progress')
							)
						// ->whereNull('isi_mr_subdepts.deleted_at')
						->where('mr_subdepts.approval','!=','belum')
						->whereNull('mr_subdepts.deleted_at')
						->groupBy('id','mr_subdepts.nomr','tanggal','projects.area','cms_users.name')
						->orderBy('id','desc')
						->get()
						->toArray()
						;
						
			foreach ($hasil as $key => $v) {
				$action = '<button type="button" class="btn btn-xs btn-primary btn_detail_mr"><i class="fa fa-eye"></i> Detail</button';
				$data[] = [$v->id,
							$v->nomr,
							$v->tanggal,
							$v->area,
							$v->name,
							$v->progress,
							$action
							];
			}
			$results = [
	            "draw" => 1,
	            "recordsTotal" => count($data),
	            "recordsFiltered" => count($data),
	            "data" => $data
	        ];
			return response()->json($results);
		}

		public function pengirimanStatusKirim_so(){
			$hasil = DB::table('sales_orders')
						->leftJoin('isi_sales_orders','sales_orders.id','=','isi_sales_orders.sales_orders_id')
						->leftJoin('customers','sales_orders.customers_id','=','customers.id')
						->leftJoin('cms_users','sales_orders.user_id','=','cms_users.id')
						->select('sales_orders.id','sales_orders.nomor','customers.nama','cms_users.name'
							,DB::raw('TRUNCATE(IFNULL(( SUM(isi_sales_orders.jumlah) / NULLIF(SUM(isi_sales_orders.dikirim),0) * 100 ),0),2) as progress')
							,DB::raw('DATE_FORMAT(sales_orders.created_at,"%Y-%m-%d") as created_at')
							)
						// ->whereNull('isi_sales_orders.deleted_at')
						->where('sales_orders.approval','=','approved')
						->whereNull('sales_orders.deleted_at')
						->groupBy('id','sales_orders.nomor','sales_orders.created_at','customers.nama','cms_users.name')
						->orderBy('id','desc')
						->get()
						->toArray()
						;
			$data = [];
			foreach ($hasil as $key => $v) {
				$action = '<button type="button" class="btn btn-xs btn-primary btn_detail_so"><i class="fa fa-eye"></i> Detail</button';
				$data[] = [$v->id,
							$v->nomor,
							$v->created_at,
							$v->nama,
							$v->name,
							$v->progress,
							$action
							];
			}
			$results = [
	            "draw" => 1,
	            "recordsTotal" => count($data),
	            "recordsFiltered" => count($data),
	            "data" => $data
	        ];
			return response()->json($results);
		}

		public function pengirimanStatuslistpengiriman(){
			$request = request();
			$id = $request->id;
			$table = $request->table;

			$hasil = DB::table('pengiriman_barangs')
						->leftJoin('cms_users','cms_users.id','=','pengiriman_barangs.user')
						->select('pengiriman_barangs.id','pengiriman_barangs.kode','pengiriman_barangs.tgl_kirim','cms_users.name','pengiriman_barangs.status')
						->where('pengiriman_barangs.jenis','LIKE',$table.'_%')
						->where('pengiriman_barangs.global_id',$id)
						->whereNull('pengiriman_barangs.deleted_at')
						->get();
			$data = [];
			foreach ($hasil as $key => $v) {
				$action ='<a href="'.route("view.detail.pengiriman.barang",$v->kode).'" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> Detail Kirim</a>';
				$data [] = [$v->id,
							$v->kode,
							$v->tgl_kirim,
							$v->name,
							$v->status,
							$action
							];
			}
			// $results = [
	  //           "draw" => 1,
	  //           "recordsTotal" => count($data),
	  //           "recordsFiltered" => count($data),
	  //           "data" => $data
	  //       ];
			return response()->json($data);
		}
}
