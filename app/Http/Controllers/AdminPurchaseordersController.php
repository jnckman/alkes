<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;
	use App\Models\Isi_po;
	use App\Models\Barang;
	use App\Models\Purchaseorder;
	use App\Models\Barangdatang;
	use App\Models\Bukuhutang;
	use App\Models\Rekpers;
	use App\Models\Logtransaksi;
	use App\Models\Bukufakturpo;


	class AdminPurchaseordersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_dropdown";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "purchaseorders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nopo","name"=>"nopo"];
			$this->col[] = ["label"=>"Tglpo","name"=>"tglpo"];
			//$this->col[] = ["label"=>"Kodesp","name"=>"kodesp"];
			$this->col[] = ["label"=>"Customer","name"=>"customers_id","join"=>"suppliers,nama"];
			$this->col[] = ["label"=>"Jenis","name"=>"jenispo",'callback_php'=>'$this->tampiljenispo($row->jenispo)'];
			//$this->col[] = ["label"=>"Ongkir","name"=>"ongkir"];
			//$this->col[] = ["label"=>"ppn","name"=>"ppn"];
			$this->col[] = ["label"=>"Tipe PO","name"=>"tipepo"];
			$this->col[] = ["label"=>"Periode MR","name"=>"periode_mrs_id","join"=>"periode_mrs,judul",'callback_php'=>'$this->isPeriode($row)'];
			$this->col[] = ["label"=>"Tempo","name"=>"tempo"];
			$this->col[] = ["label"=>"Total (Rp)","name"=>"total_akhir",'callback_php'=>'number_format($row->total_akhir)'];
			$this->col[] = ["label"=>"Progress","name"=>"id",'callback_php'=>'$this->getProgress($row->id)']; 
			$this->col[] = ["label"=>"Status","name"=>"status"];
			// $this->col[] = ["label"=>"id","name"=>"id"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			// $this->form[] = ['label'=>'Nopo','name'=>'nopo','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			// $this->form[] = ['label'=>'Tglpo','name'=>'tglpo','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			// $this->form[] = ['label'=>'Kodesp','name'=>'kodesp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			// $this->form[] = ['label'=>'Customers Id','name'=>'customers_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			// $this->form[] = ['label'=>'Jenispo','name'=>'jenispo','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Tempo','name'=>'tempo','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Ongkir','name'=>'ongkir','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Ppn','name'=>'ppn','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			// $this->form[] = ['label'=>'Periode Mrs Id','name'=>'periode_mrs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Diskon','name'=>'diskon','type'=>'text','validation'=>'integer','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Total','name'=>'total_akhir','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nopo','name'=>'nopo','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tglpo','name'=>'tglpo','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Kodesp','name'=>'kodesp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Customers Id','name'=>'customers_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Jenispo','name'=>'jenispo','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'suppliers,nama'];
			//$this->form[] = ['label'=>'Ongkir','name'=>'ongkir','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Ppn','name'=>'ppn','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Periode Mrs Id','name'=>'periode_mrs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tempo','name'=>'tempo','type'=>'date','validation'=>'required|date','width'=>'col-sm-10','datatable'=>'periode_mrs,judul'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
	        // $this->sub_module[] = ['label'=>'list SPBM','path'=>'barangdatangs','parent_columns'=>'nopo','foreign_key'=>'purchaseorders_id','button_color'=>'success','button_icon'=>'fa fa-bars'];
	        



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-dashboard btn_progress_po','color'=>'info','url'=>'javascript:checkProgress([id])','showIf'=>true];
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-folder-open btn_isi_po','color'=>'info','url'=>CRUDBooster::mainpath('../isipurchaseorder').'/[id]','showIf'=>true];
	        // $this->addaction[] = ['label'=>'','icon'=>'fa fa-file-text-o btn_buat_spbm','color'=>'info','url'=>CRUDBooster::mainpath('../buatspbm').'/[id]','showIf'=>true];
	        // $this->addaction[] = ['label'=>'','icon'=>'fa fa-book btn_list_spbm','color'=>'info','url'=>CRUDBooster::mainpath('../listspbm').'/[id]','showIf'=>true];
	        // $this->addaction[] = ['label'=>'','icon'=>'fa fa-print btn_print','color'=>'warning','url'=>CRUDBooster::mainpath('print/[id]'),'showIf'=>true];

	        //======================START silahkan tambah untuk level PO====================================
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-check btn_check approve-1','color'=>'success','url'=>CRUDBooster::mainpath('approve/1/[id]'),'showIf'=>'[status] == "new"','confirmation' => true];
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-check btn_check approve-2','color'=>'danger','url'=>CRUDBooster::mainpath('approve/2/[id]'),'showIf'=>'[status] == "approve-1"','confirmation' => true];
	        //=======================END level PO===========================================================
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-money btnbayar','color'=>'primary','url'=>CRUDBooster::mainpath('timelinebayar/[id]')];
	        // $this->addaction[] = ['label'=>'EditRahasia','icon'=>'fa fa-book-o','color'=>'danger','url'=>CRUDBooster::mainpath('editrahasia/[id]'),'showIf'=>false];




	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();


	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
	        



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = '';


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '
	        <div class="box box-info">
	        	<div class="box-header with-border">
	        		Bantuan :
	        		<div class="box-tools pull-right">
		                <div class="btn-group">
		                  <button type="button" id="collapse-bantuan" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
		                  </button>
		                </div>
		            </div>
	        	</div>
	        	<div class="box-body">
	        		<li>
	        		Tombol Approve-1 ( <i class="btn btn-xs btn-success"><i class="fa fa-check"></i></i> ) akan mengubah status PO menjadi "Approve-1".
	        		</li>
	        		<li>
	        		PO yang dapat direvisi adalah PO dengan status Approve-1.
	        		</li>
	        		<li>
	        		Setelah PO dirasa fix (tidak ada revisi lagi) maka dapat diubah statusnya menjadi Approve-2 ( <i class="btn btn-xs btn-danger"><i class="fa fa-check"></i></i> )
	        		</li>
	        	</div>
	        </div>';
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = '
            <div class="modal" id="modalProgress" tabindex="-1" role="dialog" aria-hidden="true">
        	   <div class="modal-dialog modal-lg">
        	      <div class="modal-content">
        	         <div class="modal-header">
        	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        	            <h4 class="modal-title" id="myModalLabel">Kedatangan PO</h4>
        	            <h5>Harga Barang adalah harga sudah di diskon *</h5>
        	         </div>
        	         <div class="modal-body" id="table_dashboard">
        	            <table class="table table-bordered" id="tableListBarang" style="width:100%;">
        	               <thead>
        	                 <tr>
        	                   <th>Barang</th>
        	                   <th>jumlah</th>
        	                   <th>total</th>
        	                   <th>id</th>
        	                   <th>Harga PO</th>
        	                   <th>Jumlah SPMB/PO</th>
        	                   <th>Action</th>
        	                 </tr>
        	               </thead>
        	               <tfoot>
        	                 <tr>
        	                   <th colspan="5"><p class="text-right">Kedatangan :</p></th>
        	                   <th colspan="2"></th>
	    	                 </tr>
        	               </tfoot>
        	            </table>
        	         </div>
        	         <div class="modal-footer">
        	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	         </div>
        	      </div>
        	   </div>
        	</div>
        	<div class="modal" id="modalRevisi" tabindex="-1" role="dialog" aria-hidden="true">
        	   <div class="modal-dialog">
        	      <div class="modal-content">
        	      <form class="formRevisi">
        	         <div class="modal-header">
        	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        	            <h4 class="modal-title" id="myModalLabel">Ajukan Revisi PO</h4>
        	         </div>
        	         <div class="modal-body">
        	         	<input type="hidden" name="isi_pos_id" value=""/>
        	            	<div class="form-group">
        	            		<label>Barang</label>
        	            		<input class="form-control" name="namaBarang" type="text" readonly="true" value=""/>
        	            	</div>
        	            	<div class="form-group">
        	            		<label>Jumlah Lama</label>
        	            		<input class="form-control" name="jumlah_lama" type="number" min=0 readonly="true" value=""/>
        	            	</div>
        	            	<div class="form-group">
        	            		<label>Harga Satuan Lama</label>
        	            		<input class="form-control" name="harga_po" type="number" min=0 readonly="true" value=""/>
        	            	</div>
        	            	<div class="form-group">
        	            		<label>Jumlah Satuan Baru*</label>
        	            		<input class="form-control" required name="jumlah_baru" type="number" min=0 value="" placeholder="0"/>
        	            	</div>
        	            	<div class="form-group">
        	            		<label>Harga Baru*</label>
        	            		<input class="form-control" required name="harga_baru" type="number" min=0 value="" placeholder="0"/>
        	            	</div>
        	            	<div class="form-group">
        	            		<label>Keterangan*</label>
        	            		<textarea name="keterangan" required class="form-control" rows="5" style="resize: vertical;" placeholder="Masukkan Keterangan"></textarea>
        	            	</div>
        	         </div>
        	         <div class="modal-footer">
	        	        <button type="submit" class="btn btn-success">Submit</button>
        	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	         </div>
        	         </form>
        	      </div>
        	   </div>
        	</div>
        	<div class="modal" id="modalsplit" role="dialog" aria-hidden="true">
			   <div class="modal-dialog">
			      <div class="modal-content">
			      <form class="formRevisi">
			         <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			            <h4 class="modal-title" id="myModalLabel">Ajukan Split PO</h4>
			         </div>
			         <div class="modal-body">
			         	<input type="hidden" name="isi_pos_id" value=""/>
			            	<div class="form-group">
			            		<label>Barang</label>
			            		<input class="form-control" name="namaBarang" type="text" readonly="true" value=""/>
			                        <input name="split" type="hidden" readonly="true" value="1"/>
			            	</div>
			            	<div class="form-group">
			            		<label>Jumlah Lama</label>
			            		<input class="form-control" name="jumlah_lama" type="number" min=0 readonly="true" value=""/>
			            	</div>
			            	<div class="form-group">
			            		<label>Harga Lama</label>
			            		<input class="form-control" name="harga_po" type="number" min=0 readonly="true" value=""/>
			            	</div>
			            	<div class="form-group">
			            		<label>Jumlah Split*</label>
			            		<input class="form-control" required name="split_jumlah" type="number" min=0 value="" placeholder="0"/>
			            	</div>
			            	<div class="form-group">
			            		<label>Harga Split*</label>
			            		<input class="form-control" required name="split_harga" type="number" min=0 value="" placeholder="0"/>
			            	</div>
			                <div class="form-group">
			                        <label>Supplier:*</label>
			                        <select id="select2supplier" required name="supplier" style="width:100%"><option></option></select>
			                </div>
			            	<div class="form-group">
			            		<label>Keterangan*</label>
			            		<textarea name="keterangan" required class="form-control" rows="5" style="resize: vertical;" placeholder="Masukkan Keterangan"></textarea>
			            	</div>
			         </div>
			         <div class="modal-footer">
				        <button type="submit" class="btn btn-success">Submit</button>
			            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			         </div>
			         </form>
			      </div>
			   </div>
			</div>
	        	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset('customs/js/select2.min.js');
	        $this->load_js[] = asset('customs/js/po_progress.js');
	        $this->load_js[] = asset('customs/js/pace.min.js');
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = "
	        .modal-dialog{
			    overflow-y: initial !important
			}
			.modal-body{
			    max-height: calc(100vh - 210px);
			    overflow-y: auto;
			}
			.dropaction{
				margin-left: -80px;
			}
			";
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        $this->load_css[] = asset('customs/css/pace-theme-minimal.css');
	        $this->load_css[] = asset('customs/css/select2.min.css');
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	    }


	    /*
	    | -----------------------------------------5----------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	    	
	    	
	         $query->where('tipepo','LIKE','%non_mr%');

	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	 
		    // $this->selesai = 0;       
	    	if($column_index==9){
	    		if($column_value=='100 %'){
	    			$this->selesai = 1;
	    			// dd($column_value);
	    		}
	    		else $this->selesai = 0;
	    		// dd($column_value);
	    	}

	    	// dd($this->selesai);
	    	/*if($column_index == 10){
	    		if($this->selesai == 1){
	    			$column_value .= '<a class="btn btn-xs btn-info" title="% Progress" onclick="" href="javascript:checkProgress(3)"><i class=""></i> % Progress</a>&nbsp;';
	    		}
	    	}*/
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	        // $request = request();
	    	// $postdata['diskon'] = $request->diskon;
	    	// dd($postdata);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function adjustpricelist(){
	    	$data = Barang::all();
	    	foreach ($data as $d) {
	    		if($d['avgprice'] > $d['harga_beli_akhir'] * $d['akumstok']){
	    			echo $d['nama'].'-'.$d['harga_beli_akhir'].'-'.$d['akumstok'].'-'.$d['avgprice'].'<Br>';
	    		}

	    	}
	    }

	    public function requesteditpo($id){
	    	$database = DB::table('listrequesteditpos');
	    	$current = $database->where('purchaseorders_id',$id)->get();
	    	if(sizeof($current)==0){
			$data = $database->where('purchaseorders_id',$id)->insert([
	    		'created_at'=>Carbon::now()->toDateTimeString(),
	    		'users_id'=>1,
	    		'purchaseorders_id'=>$id,
	    		'counter'=>$current[0]->counter,
	    		'status'=>1

	    	]);
			return redirect()->back()->with(['message'=>'Data Baru di Masukan','message_type'=>'success']);
			}
	    	elseif($current->status==1){
	    		return redirect()->back()->with(['message'=>'Masih Belum di Akses Oleh Team','message_type'=>'danger']);
	    	}
	    	else{
	    		$data = $database->update([
	    			'created_at'=>Carbon::now()->toDateTimeString(),
	    			'users_id'=>1,
	    			'purchaseorders_id'=>$id,
	    			'counter'=>$current->counter + 1,
	    			'status'=>1
	    	]);
	    		return redirect()->back()->with(['message'=>'Data berhasil di Update','message_type'=>'success']);
	    	}
	    	

	    }

	    public function listrequesteditpo(){
	    	$data['id']=0;
			$this->cbView('customView.listrequesteditpo',$data);
	    }

	    public function listrequesteditposdtajax(){
	    	$hasil = DB::table('listrequesteditpos')
	    	->leftJoin('purchaseorders','listrequesteditpos.purchaseorders_id','=','purchaseorders.id')
	    	->select('listrequesteditpos.id','purchaseorders.nopo','listrequesteditpos.status','listrequesteditpos.counter')
	    	->whereNull('listrequesteditpos.deleted_at')->get();

	    	$status = '';
	    	foreach($hasil as $d){
	    				if($d->status==1){$status ='Belum di Review';$button = "<a href='reviewpengajuaneditpo/".$d->id."'><button class='btn btn-success'>Review Pengajuan</button></a>"."<a href='reviewpengajuaneditpo/".$d->id."'><button class='btn btn-success'>Lihat isi PO</button></a>";}
	    				elseif($d->status==2){$status ='Sudah di Review Belum di Approve';$button="<a href='reviewpengajuaneditpo/".$d->id."'><button class='btn btn-danger'>Cancel Review Pengajuan</button></a>"."<a href='approvepengajuaneditpo/".$d->id."' style='margin-left:5px;'><button class='btn btn-success'>Approve Pengajuan</button></a>";}
	    				elseif($d->status==3){$status ='Sudah di Approve';$button="<a href='reviewpengajuaneditpo/".$d->id."'><button class='btn btn-danger'>Cancel Approve</button></a>";}
	    				else $status='error';

	    				$data[] = [
	    		 				   $d->id,
	    		 				   $d->nopo,
	    		 				   '<B>'.$status.'</b>',
	    		 				   $d->counter,
	    		 				   $button
	    		 				   ];


	    		 		$urutan++;
	    			 }
	    				$results = [
	    				"total" =>$total,
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);
	    }

	    public function reviewpengajuaneditpo($id){
	    	$data= DB::table('listrequesteditpos')->where('id',$id)->update([
	    		'status'=>2
	    	]);
	    	return redirect()->back()->with(['message'=>'Data berhasil di Update','message_type'=>'success']);

	    }
	    public function cancelreviewpengajuaneditpo($id){
	    	$data= DB::table('listrequesteditpos')->where('id',$id)->update([
	    		'status'=>2
	    	]);
	    	return redirect()->back()->with(['message'=>'Data berhasil di Update','message_type'=>'success']);

	    }
	    public function approvepengajuaneditpo($id){
	    	$data= DB::table('listrequesteditpos')->where('id',$id)->update([
	    		'status'=>3
	    	]);
	    	return redirect()->back()->with(['message'=>'Data berhasil di Update','message_type'=>'success']);

	    }

	    public function ambilbarangeditpo($id){
	    	$data = DB::table('isi_pos')->where('id',$id)->get()[0];

	    	return json_encode($data);
	    }
	    public function simpaneditpo(Request $request){
	    	$input = Request::all();
	    	$data = DB::table('isi_pos')->where('id',$input['idpo'])->update(['jumlah'=>$input['jumlahbaru']]);
	    	$hasil = 'ok';
	    	
	    	return json_encode($hasil);
	    }
	    public function purchaseorderspembelian(){


	    	$supplier = DB::table('suppliers')->whereNull('deleted_at')->get();
	    	$periode = DB::table('periode_mrs')->whereNull('deleted_at')->get();
	    	foreach ($supplier as $b) {
				$suppliers[] = ([
					'id' => $b->id,
					'text' => 'nama : '.$b->nama.' | '.$b->kode,
				]);
			}
			$data['periode'] = $periode;
	    	$data['supplier'] = json_encode($suppliers);
	    	$this->cbView('customView.purchaseorderspembelian',$data);

	    }
	    public function purchaseorderspembelianmr(){
	    	$supplier = DB::table('suppliers')->whereNull('deleted_at')->get();
	    	$periode = DB::table('periode_mrs')->whereNull('deleted_at')->get();
	    	foreach ($supplier as $b) {
				$suppliers[] = ([
					'id' => $b->id,
					'text' => 'nama : '.$b->nama.' | '.$b->kode,
				]);
			}
			$data['periode'] = $periode;
	    	$data['supplier'] = json_encode($suppliers);
	    	$this->cbView('customView.purchaseorderspembelianmr',$data);
	    }
	    public function purchaseorderspembeliannonmr(){
	    	$supplier = DB::table('suppliers')->whereNull('deleted_at')->get();
	    	$periode = DB::table('periode_mrs')->whereNull('deleted_at')->get();
	    	foreach ($supplier as $b) {
				$suppliers[] = ([
					'id' => $b->id,
					'text' => 'nama : '.$b->nama.' | '.$b->kode,
				]);
			}
			$data['periode'] = $periode;
	    	$data['supplier'] = json_encode($suppliers);
	    	$this->cbView('customView.purchaseorderspembeliannonmr',$data);
	    }


	    public function purchaseorderspembeliandtajax(){
	    	$request = request();
	    	$hasil = DB::table('purchaseorders')
		    	->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
		    	->select('purchaseorders.id','suppliers.nama','purchaseorders.created_at','purchaseorders.nopo','purchaseorders.total_akhir','purchaseorders.tempo','purchaseorders.tipepo','purchaseorders.status','purchaseorders.hari')
		    	->whereNull('purchaseorders.deleted_at')
		    	->where('purchaseorders.periode_mrs_id',0);
	    	if(isset($request->mr)){
	    		if($request->mr=='1'){
	    			$tp = $hasil->where('tipepo','like','mr');
	    		}
	    		else{
	    			$tp = $hasil->where('tipepo','like','non_mr');
	    		}
	    	}
	    	
	    	$hasil = $hasil->orderBy('id', 'desc')
				    	->get()
	    				;

	    	$data=array();
	    	$status = '';

	    	
	    	foreach($hasil as $d){
	    				if($d->status == 'new'){
	    					$actionsetuju = '<a href="purchaseorderspembelian/detail/'.$d->id.'"><button class="btn btn-warning btn-xs btnEditJumlah" style="margin-left:5px;margin-right:5px;"><i class="fa fa-th-list"></i></button></a>'.
	    		 				   '<a class="btn btn-xs btn-danger" title="Delete PO" onclick="" href="purchaseorderspembeliandelpost/'.$d->id.'"><i class="fa fa-trash-o"></i></a>&nbsp;
								  	';
	    				}
	    				else{
	    					$actionsetuju = '<a class="btn btn-xs btn-info" title="Buat SPBM" onclick="" href="buatspbm/'.$d->id.'"><i class="fa fa-truck"></i></a>&nbsp;          
								  	<a class="btn btn-xs btn-info" title="List SPBM" onclick="" href="listspbm/'.$d->id.'"><i class="fa fa-book"></i></a>&nbsp;
								  	';	
	    				}

	    				$data[] = [
	    		 				   $d->id,
	    		 				   $d->tipepo,
	    		 				   $d->nama,
	    		 				   $d->created_at,
	    		 				   $d->nopo,
	    		 				   number_format($d->total_akhir),
	    		 				   $d->hari.' hari',
	    		 				   $d->status,
	    		 				   $actionsetuju,
	    		 				   ];


	    		 		$urutan++;
	    			 }
	    				$results = [
	    				"total" =>$total,
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);
	    }

	    public function pogetlastid(){
	    	$hasil = DB::table('purchaseorders')
	    	->whereNull('purchaseorders.deleted_at')->orderby('id','desc')->select('id')->first();
	    	if($hasil==null)
	    		$hasil['id'] = 0;
	    	echo json_encode($hasil);
	    }

	    public function buatpurchaseorderspembelian(Request $request){
	    	$input = Request::all();
	    	
	    	//$counter = DB::table('purchaseorders')->select('counter')->where('purchaseorders',0)->wherenull('deleted_at')->orderBy('desc','id')->get();
	    	//dd($counter[0]);
	    	//dd($insert);
	    	$proc = DB::table('purchaseorders')->insert([
	    		'created_at'=>Carbon::now()->toDateTimeString(),
	    		'nopo'=>$input['nopo'].'-'.$input['tipepo'],
	    		'hari'=>$input['hari'],
	    		'tempo'=>$input['tempo'],
	    		'ongkir'=>$input['ongkir'],
	    		'tglpo'=>Carbon::now()->toDateTimeString(),
	    		'periode_mrs_id'=>0,
	    		'tipepo' =>$input['tipepo'],
	    		'customers_id'=>$input['supplierid'],
	    		'ppn'=>$input['ppn'],

	    		
	    	]);
	    	return redirect()->back();
	    }
	    public function purchaseorderspembeliandetail($id){
	    	
	    	$po = DB::table('purchaseorders')->where('id',$id)->get()[0];
	    	$supid = $po->customers_id;
	    	
			$data['id']= $id;
	    	$satuan = DB::table('satuans')->whereNull('deleted_at')->get();
	    	$kategori = DB::table('kategoris')->whereNull('deleted_at')->get();
	    	$barang = DB::table('pricelists')
	    	->leftJoin('barangs','barangs.id','=','pricelists.barangs_id')
	    	->select('barangs.*')
	    	->where('pricelists.suppliers_id',$supid)
	    	->whereNull('pricelists.deleted_at')
	    	->whereNull('barangs.deleted_at')
	    	->get();

	    	
	    	foreach ($satuan as $b) {
				$satuans[] = ([
					'id' => $b->id,
					'text' => $b->nama,
				]);
			}
			foreach ($kategori as $b) {
				$kategoris[] = ([
					'id' => $b->id,
					'text' => $b->nama,
				]);
			}
			foreach ($barang as $b) {
				$barangs[] = ([
					'id' => $b->id,
					'text' => $b->nama,
				]);
			}
			$hasil = DB::table('purchaseorders')
			->leftJoin('suppliers','purchaseorders.customers_id','=','suppliers.id')
			->where('purchaseorders.id',$id)
	    	->whereNull('purchaseorders.deleted_at')->where('purchaseorders.periode_mrs_id',0)->get();

	    	$data['satuan'] = json_encode($satuans);
	    	$data['kategori'] = json_encode($kategoris);
	    	$data['supplier'] = $hasil[0]->customers_id;
	    	$data['barang'] = json_encode($barangs);

	    	$data['result']= $hasil;
	    	
	    	$this->cbView('customView.purchaseorderspembeliandetail',$data);
	    }
	    public function purchaseorderspembeliandetaildtajax($id){
	    	$request = request();
	    	$hasil = DB::table('isi_pos')
	    	->leftJoin('barangs','isi_pos.barangs_id','=','barangs.id')
	    	->leftJoin('satuans','barangs.satuans_id','=','satuans.id')
	    	->select('isi_pos.id','barangs.nama','satuans.nama as namasatuan','isi_pos.jumlah','isi_pos.harga','isi_pos.diskon','isi_pos.total_akhir')
	    	->whereNull('isi_pos.deleted_at')
	    	->where('purchaseorders_id',$id)->get();
	    	$data = array();
	    	$status = '';
	    	foreach($hasil as $d){
	    				$data[] = [
	    		 				   $d->id,
	    		 				   $d->nama,
	    		 				   $d->jumlah,
	    		 				   $d->namasatuan,
	    		 				   number_format($d->harga),
	    		 				   $d->diskon,
	    		 				   number_format($d->total_akhir),
	    		 				   '<button class="btn btn-warning btn-xs btnedit" style="margin-left:5px;" id="'.$d->id.'"><i class="fa fa-pencil-square"></i> Edit</button>'.'<a href="../../purchaseorderspembelian/delete/'.$d->id.'">
	    		 				   <button class="btn btn-danger btn-xs btndelete" style="margin-left:5px;">
	    		 				   <i class="fa fa-trash"></i> Delete</button></a>',
	    		 				   ];


	    		 		$urutan++;
	    			 }
	    				$results = [
	    				"total" =>$total,
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);
	    }
	    public function purchaseorderspembeliandelete($id){
	    	$data = Isi_po::find($id);
			$idpo = $data->purchaseorders_id;
			$po = Purchaseorder::find($idpo);
						$po->total_akhir = $po->total_akhir - ($data->harga*$data->jumlah);
			if($po->total_akhir < 0)
				$po->total_akhir = 0;
			$data->delete();
			$po->save();
	    	return back();
	    }
	    public function buatisipurchaseorderspembelian(Request $request){
	    	$input = Request::all();
	    	
	    	DB::table('barangs')->insert([
	    		'nama' => $input['nama'],
	    		'status' => 'NEW',
	    		'satuans_id' => $input['satuans_id'],
	    		'kategoris_id' => $input['kategoris_id']

	    	]);
	    	$barangs_id = DB::connection() -> getPdo() -> lastInsertId();
	    	DB::table('pricelists')->insert([
	    		'nama' => 'baru',
	    		'harga' => $input['harga'],
	    		'barangs_id' => $barangs_id,
	    		'suppliers_id' => $input['suppliers_id'],

	    	]);
	    	
	    	DB::table('isi_pos')->insert([
	    		'created_at'=>Carbon::now()->toDateTimeString(),
	    		'barangs_id'=>$barangs_id,
	    		'jumlah'=>$input['jumlah'],
	    		'harga'=>$input['harga'],
	    		'diskon'=>$input['diskon'],
	    		'total_akhir'=>$input['total'],

	    		'purchaseorders_id'=>$input['purchaseorders_id'],
	    	]);
	    	return redirect()->back();


	    }
	    public function buatisipurchaseorderspembelianbaranglama(Request $request){
	    	$input = Request::all();
	    	
	    	
	    	$datapricelist = DB::table('pricelists')->whereNull('deleted_at')->where('id',$input['pricelistid'])->get()[0];

	    	$simpan2 = DB::table('barangs')->where('id', $datapricelist->barangs_id)->update(['harga_beli_akhir'=>$datapricelist->harga,'priclists_id'=>$datapricelist->id]);

	    	
	    	$hargafix = str_replace(',','',$input['total']);
	    	$isi_po = Isi_po::create([
	    		'created_at'=>Carbon::now()->toDateTimeString(),
	    		'barangs_id'=>$input['barangid'],
	    		'jumlah'=>$input['jumlah'],
	    		'harga'=>$datapricelist->harga,
	    		'diskon'=>$input['diskon'],
	    		'total_akhir'=>$hargafix,
	    		'purchaseorders_id'=>$input['purchaseorders_id'],
	    	]);

	    	$check = Isi_po::where('barangs_id',$isi_po->barangs_id)
	        							->where('purchaseorders_id',$isi_po->purchaseorders_id)
	        							->where('id','<>',$isi_po->id)
					        			->whereNull('deleted_at')
					        			->get();
			$total_akhir = $check->sum('total_akhir');
			$total_akhir += $isi_po->total_akhir;
			$jumlah = $check->sum('jumlah');
			$jumlah += $isi_po->jumlah;
	    	$isi_po->update(['jumlah'=>$jumlah,'total_akhir'=>$total_akhir]);
	    	$check = Isi_po::where('barangs_id',$isi_po->barangs_id)
	        							->where('purchaseorders_id',$isi_po->purchaseorders_id)
	        							->where('id','<>',$isi_po->id)
					        			->whereNull('deleted_at')
					        			->delete();
	    	$data = Purchaseorder::find($input['purchaseorders_id']);
	    	$data->total_akhir = $data->total_akhir+$hargafix;
	    	$data->save();
	    	return redirect()->back();


	    }


	    public function getLaporan(){
	    	if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
	    	$data = [];
	    	$data['page_title'] = 'Laporan PO';
	    	$this->cbView('customView.purchaseorderslaporan',$data);
	    }

	    public function chartLaporan(){
	    	$request = request();
	    	// $isi = Isi_po::find($request->id)->where()
	    }
	    //By the way, you can still create your own method in here... :) 

	    public function isPeriode($row){
	    	// dd($row);
	    	if($row->periode_mrs_id==0){
	    		return 'PO pembelian';
	    	}
	    	else return $row->periode_mrs_judul;
	    }

	    public function getProgress($row=0)
	    {
	    	$request=request();
	    	if($row!=0)
	    	$id = $row;
	    	else $id = $request->id;
	    	$datas = Isi_po::leftJoin('barangdatangs','isi_pos.purchaseorders_id','=','barangdatangs.purchaseorders_id')
	    								->leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
	    								->leftJoin('isispbms','barangdatangs.id','=','isispbms.barangdatangs_id')
	    								->leftJoin('purchaseorders','purchaseorders.id','=','isi_pos.purchaseorders_id')
	    								->where('isi_pos.purchaseorders_id','=',$id)
	    								->whereNull('isi_pos.deleted_at')
	    								->select(
	    									'isi_pos.id','barangs.nama','isi_pos.jumlah as total','isi_pos.harga as harga_po'
	    									,'purchaseorders.status'
	    									,DB::raw('SUM(CASE When isispbms.barangs_id = isi_pos.barangs_id Then isispbms.jumlah Else 0 End) as jumlah')
	    								)
	    								->groupby('isi_pos.id','barangs.nama','total','harga_po','status')
	    								->get()
	    								;
	    								// dd($row);
	    	$results = [];
	    	foreach ($datas as $key => $value) {
	    		// dd($value->revisi->where('status','<>','rejected')->last());
	    		$r = $value->revisi->where('status','approve')->last();
	    		if(isset($r->jumlah_baru)){
	    			$totalku = $r->jumlah_baru;
	    		}
	    		else{
	    			$totalku = $value->total;
	    		}
	    		// dd($value->status);
	    		if(strtolower($value->status)=='approve-1'){
	    			if(strtolower($value->revisi->last()->status)=='new' ) {
		    			$action = 'Pengajuan revisi terakhir belum mendapat approval';
		    		}
		    		else{
		    			$action = '<button type="button" class="btn btn-xs btn-warning btnRevisi"><i class="fa fa-pencil-square"></i></button>'.'<button type="button" class="btn btn-xs btn-danger btnSplit" style="margin-left:5px;" data-placement="bottom" title="Ubah Supplier"><i class="fa fa-exchange"></i></button>';//btn split
		    		}
	    		}
	    		$results[] = ['barang'=>$value->nama,
	    						'jumlah'=>$value->jumlah,
	    						'total'=>$totalku,
	    						'isi_pos_id'=>$value->id,
	    						'harga_po' => $value->harga_po,
	    						// 'harga_po_revisi' => $r->harga_baru,
	    						'tampil'=>$value->jumlah.' / '.$totalku,
	    						'action'=> $action
	    					];
	    	}
	    	$persentase = 0;
	    	$jmlh = 0;
	    	$ttl = 0;
	    	foreach ($results as $key => $value) {
	    		$jmlh += (int)$value['jumlah'];
	    		$ttl += (int)$value['total'];
	    	}
	    	$persentase = ($jmlh / $ttl)*100;
	    	
	    	if($row==0)return response()->json(['results'=>$results]);
	    	else return $persentase.' %';
	    }

	    public function postRevisi(){
	    	$request = request();
	    	// return response()->json($request);
	    	if(isset($request->split)){
	    		// return response()->json($request);
	    		if($request->jumlah_lama==$request->split_jumlah){
	    			$jumlahbaru = $request->jumlah_lama; // artinya isi PO dipindah semua
	    		}
	    		else{
	    			$jumlahbaru = $request->jumlah_lama-$request->split_jumlah; //isi PO dibagi sebagian
	    		}
	    		$insert = DB::table('revisi_pos')->insert([
	    											'created_at'=>now(),
	    											'isi_pos_id'=>$request->isi_pos_id,
	    											'harga_po'=>$request->harga_po,
	    											'harga_baru'=>$request->harga_po,
	    											'tgl_revisi'=>date('Y-m-d'),
	    											'keterangan'=>$request->keterangan,
	    											'jumlah_lama'=>$request->jumlah_lama,
	    											'jumlah_baru' =>$jumlahbaru,
	    											'user_id'=>CRUDBooster::myId(),
	    											'split'=>1,
	    											'split_jumlah' =>$request->split_jumlah,
	    											'split_harga' =>$request->split_harga,
	    											'split_suppliers_id' => $request->supplier,
	    											]);	
	    	}
	    	else
	    	{
	    		$insert = DB::table('revisi_pos')->insert([
	    											'created_at'=>now(),
	    											'isi_pos_id'=>$request->isi_pos_id,
	    											'harga_po'=>$request->harga_po,
	    											'harga_baru'=>$request->harga_baru,
	    											'tgl_revisi'=>date('Y-m-d'),
	    											'keterangan'=>$request->keterangan,
	    											'jumlah_lama'=>$request->jumlah_lama,
	    											'jumlah_baru' =>$request->jumlah_baru,
	    											'user_id'=>CRUDBooster::myId(),
	    											]);
	    	}
	    	return response()->json('berhasil');
	    	return response()->json('gagal');
	    }

	    public function ambildataeditdetail(Request $request){
	    	$data = $request::all();
	    	$hasil = DB::table('isi_pos')
	    	->leftJoin('barangs','isi_pos.barangs_id','=','barangs.id')
	    	->select('isi_pos.created_at',
	    		'isi_pos.barangs_id',
	    		'isi_pos.jumlah',
	    		'isi_pos.harga',
	    		'isi_pos.diskon',
	    		'isi_pos.total_akhir',
	    		'isi_pos.purchaseorders_id',
	    		'isi_pos.id',
	    		'barangs.nama'
	    		)
	    	->where('isi_pos.id',$data['id'])->get();
	    	return json_encode($hasil[0]);
	    	
	    }
	    public function editisipos(Request $request){
	    	$input = $request::all();
	    	
	    	DB::table('barangs')->where('id',$input['idbarangpo'])->update([
	    		'nama' => $input['nama'],
	    		'status' => 'NEW',
	    		'satuans_id' => $input['satuans_id'],
	    		'kategoris_id' => $input['kategoris_id']

	    	]);
	    	
	    	DB::table('isi_pos')->where('id',$input['idisipo'])->update([
	    		'updated_at'=>Carbon::now()->toDateTimeString(),
	    		'jumlah'=>$input['jumlah'],
	    		'harga'=>$input['harga'],
	    		'diskon'=>$input['diskon'],
	    		'total_akhir'=>$input['total'],
	    		
	    	]);
	    	return redirect()->back()->with(['message'=>'berhasil di edit','message_type'=>'success']);
	    	
	    }

	    public function editisipolama($id){
    		//dd(Request::all());

    		$input = Request::all();
    		$data = DB::table('isi_pos')
    		->whereNull('deleted_at')
    		->where('id',$input['isipo_id']);

    		$dataget = $data->get()[0];
    		$oldvalue = $dataget->total_akhir;

    		$datapo = DB::table('purchaseorders')->where('id',$dataget->purchaseorders_id);
    		
    		$datapoget = $datapo->get()[0];
    		$oldvaluetotal = $datapoget->total_akhir;

    		$newvaluetotal = $oldvaluetotal - $oldvalue + $input['total'];
    		 
    		//jumlah , harga diskon,barang id
    		$datapo->update(['total_akhir'=>$newvaluetotal]);
    		$harga = $input['total'] / $input['jumlah'];
    		$data->update(['harga'=>$harga, 'jumlah'=>$input['jumlah'], 'diskon'=>$input['diskon'],'total_akhir'=>$input['total'],'barangs_id'=>$input['barangid']]);
    		//$data->update([''])
    		// ->where('purchaseorders_id',$input['purchaseorders_id'])
    		// ->where('barangs_id',$input['barangid']);
    		return back();

    	}
    	
	    public function ambildatapricelist(Request $request){
	    	$input = $request::all();

	    	$hasil = DB::table('pricelists')->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
	    	->select('suppliers.nama','pricelists.harga','pricelists.id')
	    	->where('barangs_id',$input['id'])
	    	->where('suppliers.id',$input['idsup'])
	    	->whereNull('pricelists.deleted_at')->get();

	    	//return json_encode($hasil);

	    	foreach ($hasil as $b) {
	    		if($b->harga == null){

	    		}
	    		else{
	    			$hargas[] = ([
	    				'id' => $b->id,
	    				'text' => $b->nama.' - '.$b->harga,
	    			]);	
	    		}
				
			}
			return json_encode($hargas);
	    }

	    public function getPrint($id){
    	    $dataku = Isi_po::where('purchaseorders_id',$id)->get();
    	    $po = DB::table('purchaseorders')
    	    			->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
    	    			->select('purchaseorders.nopo as po_nopo','purchaseorders.tglpo as po_tglpo',
    	    				'suppliers.kode as suppliers_kode','suppliers.nama as suppliers_nama', 'suppliers.alamat as suppliers_alamat','purchaseorders.total_akhir as po_total','purchaseorders.diskon as po_diskon'
    	    				,'purchaseorders.ppn as po_ppn','purchaseorders.ongkir as po_ongkir'
    	    			)
    	    			->where('purchaseorders.id',$id)
    	    			->first()
    	    			;
    	    $approver = DB::table('staticdatas')->whereNull('deleted_at')->where('name','LIKE','approval_po_name')->get()[0];
			foreach ($dataku as $key => $value) {
				$r = $value->revisi()->where('status','approve')->latest()->first();
				// dd($dataku);
				if (isset($r)) {
					$dataku[$key]->jumlah=$r->jumlah_baru;
					$dataku[$key]->harga=$r->harga_baru;
				}
			}
    	    return view('customView.printpo',['isipo'=>$dataku,'po'=>$po,'approver'=>$approver]);
    	    			// dd($dataku);}
    	}

    	public function getPrintApprove($id){
    	    $dataku = Isi_po::where('purchaseorders_id',$id)->get();
    	    $po = DB::table('purchaseorders')
    	    			->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
    	    			->select('purchaseorders.nopo as po_nopo','purchaseorders.tglpo as po_tglpo',
    	    				'suppliers.kode as suppliers_kode','suppliers.nama as suppliers_nama', 'suppliers.alamat as suppliers_alamat','purchaseorders.total_akhir as po_total','purchaseorders.diskon as po_diskon'
    	    				,'purchaseorders.ppn as po_ppn','purchaseorders.ongkir as po_ongkir'
    	    			)
    	    			->where('purchaseorders.id',$id)
    	    			->first()
    	    			;

			foreach ($dataku as $key => $value) {
				$r = $value->revisi()->where('status','approve')->latest()->first();
				// dd($dataku);
				if (isset($r)) {
					$dataku[$key]->jumlah=$r->jumlah_baru;
					$dataku[$key]->harga=$r->harga_baru;
				}
			}
    	    return view('customView.printpoapproval',['isipo'=>$dataku,'po'=>$po]);
    	    			// dd($dataku);}
    	}

    	public function geteditrahasia($id){
    		$data=[];
    		$po = Purchaseorder::where('purchaseorders.id',$id)
	        ->leftJoin('listrequesteditpos','listrequesteditpos.purchaseorders_id','=','purchaseorders.id')
	        ->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
	        ->leftJoin('periode_mrs','periode_mrs.id','=','purchaseorders.periode_mrs_id')
	        ->select('purchaseorders.nopo','suppliers.nama','listrequesteditpos.status','purchaseorders.ongkir','periode_mrs.judul',
	        	'purchaseorders.id','purchaseorders.total_akhir')
	        ->whereNull('purchaseorders.deleted_at')->get();		

	        // $setuju= DB::table('listrequesteditpos')->where('purchaseorders_id',$id)->get()[0];

	        $hasil = DB::table('isi_pos')
	            ->leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
	            ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
	            ->select('isi_pos.id','barangs.nama','satuans.nama as satuan','isi_pos.jumlah','isi_pos.harga','isi_pos.total_akhir')
	            ->where('isi_pos.purchaseorders_id',$id)
	            ->whereNull('isi_pos.deleted_at')->get();

	            // foreach($hasil as $d){
	            //    $data[] = [
	            //          $d->id,
	            //          $d->nama,
	            //          $d->satuan,
	            //          $d->jumlah,
	            //          number_format($d->harga),
	            //          number_format($d->harga*$d->jumlah),
	            //          "<button class='btn btn-xs btn-danger' disabled=''>Belum Bisa Edit PO</button>"
	            //          ];


	            //   $urutan++;
	            // }
	            // dd($po);
	        return view('customView.isi_po_edit_rahasia',['po'=>$po,'isi_pos'=>$hasil]);
    	}

    	public function editrahasia(){
    		$request= request();
    		// dd($request->request);
    		DB::table('purchaseorders')->where('id',$request->id_po)->update(['total_akhir'=>$request->po_total_akhir]);
    		foreach ($request->id as $key => $id) {
    			if($request->jumlahbaru[$key]>0){
    				DB::table('isi_pos')->where('id',$id)->update(['jumlah'=>$request->jumlahbaru[$key],
    															'total_akhir'=>$request->total_akhir[$key]]);
    			}
    		}
    		// DB::table('isi_pos');
    		return back();
    	}

    	public function tampiljenispo($id){
    		if($id==1){
    			return 'Tempo';
    		}
    		else if ($id==2){
    			return 'Tunai';
    		}
    		else{
    			return 'Pembelian';
    		}
    	}

    	public function getlistsupplier(){
    		$request = request();
    		$supplier = DB::table('suppliers')->whereNull('deleted_at');
    		if(isset($request->q)){
    			$supplier = $supplier->where('nama','like','%'.$request->q.'%');
    		}
    		$supplier = $supplier->select('id','nama as text')->get();
    		return response()->json($supplier);
    	}

    	public function rekappo(){
    		return 'gege';
    	}

    	public function getapprove($lv,$id){
    		// dd($lv);
    		if($lv==1){
    			DB::table('purchaseorders')->where('id',$id)->update(['status'=>'approve-1']);
    			CRUDBooster::redirect(CRUDBooster::adminPath('purchaseorders'),'Berhasil memberi Approval-1','success');
    		}
    		else if($lv==2){
    			DB::table('purchaseorders')->where('id',$id)->update(['status'=>'approve-2']);
    			
    			CRUDBooster::redirect(CRUDBooster::adminPath('purchaseorders'),'Berhasil memberi Approval-2','success');
    		}
    		else{
    			CRUDBooster::redirect(CRUDBooster::adminPath('purchaseorders'),'url salah','danger');
    		}
    	}

    	// public function editisipolama($id){

    	// 	$input = Request::all();
    	// 	$data = DB::table('isi_pos')
    	// 	->whereNull('deleted_at')
    	// 	->where('purchaseorders_id',$input['purchaseorders_id'])
    	// 	->where('barangs_id',$input['barangid']);

    	// }

    	public function getlistperiode(){
    		$hasil = DB::table("periode_mrs")->whereNull('deleted_at')->select('id','judul as text')->get();
    		return response()->json($hasil);
    	}

    	public function gettimelinebayar($id){
    		$data['page_title']= 'Timeline Bayar PO';
    		$data['hasil'] = DB::table("bayar_pos")
    					->leftJoin('isi_bayar_pos','bayar_pos_id','=','bayar_pos.id')
    					->leftJoin('coas','bayar_pos.coa_id','=','coas.id')
    					->leftJoin('cms_users','bayar_pos.user_id','=','cms_users.id')
    					->where('bayar_pos.po_id','=',$id)
    					->select('bayar_pos.tanggal_bayar','bayar_pos.status','cms_users.name as user','coas.nama as coa','coas.kode as coa_kode','isi_bayar_pos.nominal','isi_bayar_pos.keterangan','bayar_pos.created_at')
    					->orderBy('tanggal_bayar','desc')
    					->get();
    					;

    		$this->cbView('customView.timeline_bayar_po',$data);
    	}

    	public function laporanpembelian(){
    		return View('customView.laporanpembelian');
    	}
    	public function laporanpembelianbulan(){
    		$data = Request::all();
    		$datasimpan = Bukufakturpo::whereYear('tglfaktur',$data['tahun'])->whereMonth('tglfaktur',$data['bulan'])->get();
    		//ßdd($datasimpan[0]->isibukufakturpo);
    		//$datasimpan = Barangdatang::whereYear('created_at',$data['tahun'])->whereMonth('created_at',$data['bulan'])->get();
    		//dd($datasimpan);
    		return View('customView.laporanpembelianbulan',compact('datasimpan','data'));
    	}

    	public function laporanhutangbulan(){
    		$data = Request::all();

    		$datasimpan = Bukuhutang::whereYear('created_at',$data['tahun'])
    		->groupby('barangdatangs_id','id','deleted_at','created_at','updated_at','hutang','beli','lunas','sisa','tgllunas','via')->get();
    		//dd($datasimpan[0]->tgl);
    		return View('customView.laporanhutangbulan',compact('datasimpan'));	
    	}

    	public function laporanrekening(){
    		$data = Request::all();
    		$datarek = DB::table('rekpers')->wherenull('deleted_at')->get();
			//$datasimpan = Bukuhutang::whereYear('created_at',$data['tahun'])->whereMonth('created_at',$data['bulan'])->get();
    		return View('customView.laporanrekening',compact('datasimpan','datarek'));	
    	}

    	public function laporanrekeningbulan(){
    		$data = Request::all();
    		
    		$datarek = DB::table('rekpers')->wherenull('deleted_at')->where('id',$data['rekening'])->get();
    		//$datalog = DB::table('log')->wherenull('deleted_at')->get();
			$datasimpan = Logtransaksi::whereYear('created_at',$data['tahun'])->whereMonth('created_at',$data['bulan'])->where('bank','LIKE',$datarek[0]->atasnama.' - '.$datarek[0]->bank)->get();


    		return View('customView.laporanrekeningbulan',compact('datasimpan','datarek'));	
    	}

    	public function approvespbm($poid,$id){

    		$data = Barangdatang::find($id);
    		$totalbeli  = 0;
    		foreach ($data->isispbm as $ispbm) {
    			$isipo  = $ispbm->getjumlahpesananbaru($data->purchaseorder->id,$ispbm->barangs->id);	
    			$totalbeli = ($isipo[0]['harga'] * $ispbm['jumlah']) + $totalbeli;
    			
    		}
    		$isipo[$counter] = $ispbm->getjumlahpesananbaru($d->purchaseorder->id,$ispbm->barangs->id);
    		//$terakhir = Bukuhutang::orderBy('id', 'desc')->first();
    		$terakhir = Bukuhutang::orderBy('id', 'desc')->where('barangdatangs_id',$bdid)->wherenull('deleted_at')->first();
	    	if($terakhir == null){
	    		$buku = new Bukuhutang;
	    		$buku->hutang =$totalbeli;
	    		$buku->beli = $totalbeli;
	    		$buku->lunas = 0;
	    		$buku->sisa = $buku->beli;
	    		$buku->tgllunas = date('Y-m-d');
	    		$buku->barangdatangs_id = $id;
	    		$buku->via = 'x';
	    		$buku->save();	
	    	}
	    	else{
	    		$buku = new Bukuhutang;
	    		$buku->hutang =$totalbeli + $terakhir->hutang;
	    		$buku->beli = $totalbeli;
	    		$buku->lunas = 0;
	    		$buku->sisa = $buku->hutang;
	    		$buku->tgllunas = date('Y-m-d');
	    		$buku->barangdatangs_id = $id;
	    		$buku->via = 'x';
	    		$buku->save();	
	    	}




	    	return redirect()->back()->with(['message'=>'Data smbm berhasil di approve, buku hutang berhasil di update','message_type'=>'success']);
    	}

    	public function purchaseorderspembeliandelpost($id){
    		$ipo = Isi_po::where('id',$id)->delete();
    		$po = Purchaseorder::find($id)->delete();
    		return redirect()->back()->with(['message'=>'PO Berhasil di hapus','message_type'=>'success']);	
    	}
	}