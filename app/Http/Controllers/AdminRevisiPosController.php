<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Revisi_po;
	use App\Models\Isi_po;
	use App\Models\Purchaseorder;
	use App\Models\Pricelist;
	use App\Models\Cms_user;
	use Hash;

	class AdminRevisiPosController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "created_at,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_dropdown";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "revisi_pos";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nomor PO","name"=>"isi_pos_id","join"=>"isi_pos,purchaseorders_id"];
			$this->col[] = ["label"=>"Tgl Revisi","name"=>"tgl_revisi"];
			$this->col[] = ["label"=>"User","name"=>"user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Barang","name"=>"isi_pos_id",'callback_php'=>'$this->namaBarang($row->isi_pos_id)'];
			$this->col[] = ["label"=>"Harga PO","name"=>"harga_po"];
			$this->col[] = ["label"=>"Harga Baru","name"=>"harga_baru"];
			$this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];
			$this->col[] = ["label"=>"Jumlah Lama","name"=>"jumlah_lama"];
			$this->col[] = ["label"=>"Jumlah Baru","name"=>"jumlah_baru"];
			$this->col[] = ["label"=>"Split","name"=>"split"];
			$this->col[] = ["label"=>"Split Jumlah","name"=>"split_jumlah"];
			$this->col[] = ["label"=>"Split Harga","name"=>"split_harga"];
			$this->col[] = ["label"=>"Split Supplier","name"=>"split_suppliers_id","join"=>"suppliers,nama"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Harga Po','name'=>'harga_po','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Harga Baru','name'=>'harga_baru','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Tgl Revisi','name'=>'tgl_revisi','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Harga Po','name'=>'harga_po','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Harga Baru','name'=>'harga_baru','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tgl Revisi','name'=>'tgl_revisi','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'Approve','icon'=>'fa fa-check','color'=>'success','url'=>CRUDBooster::mainpath('approve/[id]'),'showIf'=>'[status]=="new"','confirmation' => true];
	        $this->addaction[] = ['label'=>'Reject','icon'=>'fa fa-times','color'=>'danger','url'=>CRUDBooster::mainpath('reject/[id]'),'showIf'=>'[status]=="new"','confirmation' => true];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	if($column_index==0){
	    		$column_value = $this->nomorPO($column_value);
	    	}
	    	if($column_index==9){
	    		if($column_value==1){
	    			$column_value = 'True';
	    		}
	    		else{
	    			$column_value = 'False';
	    		}
	    	}
	    	/*if($column_index==7){
	    		$column_value = '<a class="btn btn-xs btn-success btn-edit" title="Edit Data" href="'.CRUDBooster::mainpath('edit/'.$row->id).'"><i class="fa fa-pencil"></i></a>';
	    	}*/
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	    	// dd(request()->status);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function nomorPO($id){
	    	return DB::table('purchaseorders')->where('id',$id)->first()->nopo;
	    }

	    public function getReject($id){
	    	if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$update = DB::table('revisi_pos')->where('id',$id)->update(['status'=>'rejected']);
			if($update==0){
				CRUDBooster::redirect(CRUDBooster::adminPath('revisi_pos'),'Gagal melakukan update');
			}
			else CRUDBooster::redirect(CRUDBooster::adminPath('revisi_pos'),'Data berhasil diupdate','success');
	    }

	    public function getApprove($id){

	    	$menampung = array();//buat iyek
	    	if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$update = Revisi_po::where('id',$id)->first();
			// dd($update);
			$total2 =0;
			if($update->split==1){ 
				$po = Purchaseorder::where('periode_mrs_id',$update->isi_pos->purchaseorder->periode_mrs_id)->where('customers_id',$update->split_suppliers_id)->whereNull('deleted_at')->latest()->first();
				$akankah_bertemu_dengan_barang_itu = 0;
				$polama = $update->isi_pos->purchaseorder;
				if(!isset($po) || count($po->barangdatangs)>0){ //jika tidak ada po dengan periode dan supplier yg sama ATAU po sudah ada barang datang; maka buat po baru
						// jenis po == 2
					// dd($update->isi_pos->purchaseorder);
					// $polama = $po;
					$po = new Purchaseorder;
					$po->created_at = now();
					$po->tglpo= date('Y-m-d');
					$po->customers_id=$update->split_suppliers_id;
					$po->jenispo=2;
					$po->periode_mrs_id= $update->isi_pos->purchaseorder->periode_mrs_id;
					$po->total_akhir=$update->split_harga;
					$po->save();	
					$nomorku = 'PO-'.$po->tglpo.'-'.($po->id-1);
					$po->update(['nopo'=>$nomorku]);
				}
				else{ // ada PO dengan barang yg akan dipindah; check apakah ada barang di PO tersebut
					$d9 = 0;
					foreach ($po->isi as $key => $v) {
						if($v->barangs_id == $update->isi_pos->barangs_id){
							$akankah_bertemu_dengan_barang_itu = 1;
							$d9 = $v->id;
							break;
						}
					}
				}
				// se saya rasa ada bug disini, dimana jika memindah seluruh barang & tidak buat po baru 
				if($update->split_jumlah == $update->jumlah_lama){ // memindah seluruh barang
					// dd('iki>');
					$isi = $update->isi_pos;
					if($akankah_bertemu_dengan_barang_itu==1){ // ada barang di PO yg akan dituju
						// dd('iku>');
						$isi = Isi_po::find($d9); //defisini isi ulang; isi disini merupakan isi PO yang dituju yang akan ditambah jumlahnya
						if(isset($isi->revisi->where('status','approve')->last()->jumlah_baru)){ //check apakah isi yang dituju memiliki revisi
							$jumlahko = $isi->revisi->where('status','approve')->last()->jumlah_baru;
							$hargako = $isi->revisi->where('status','approve')->last()->harga_baru;
						}
						else{ // tidak memiliki revisi
							$jumlahko = $isi->jumlah;
							$hargako = $isi->harga;
						}
						$revisi_baru = new Revisi_po(); //buat revisi baru
						$revisi_baru->created_at = now();
						$revisi_baru->jumlah_lama = $jumlahko;
						$revisi_baru->jumlah_baru = $update->split_jumlah + $jumlahko; //tambah jumlah
						$revisi_baru->harga_po = $hargako;
						$revisi_baru->harga_baru = $update->split_harga; //overwrite harga
						$revisi_baru->status = 'approve';
						$revisi_baru->tgl_revisi = date('Y-m-d');
						$revisi_baru->keterangan = $update->keterangan;
						$revisi_baru->user_id = $update->user_id;
						$revisi_baru->isi_pos_id = $isi->id;
						$revisi_baru->save();
						// $isi->update([
							// 'jumlah'=>$update->split_jumlah,
									// 'harga'=> $update->split_harga,
						// 			'total_akhir'=>$update->split_harga * ($update->split_jumlah+$isi->jumlah),
									// ]);
						$update->isi_pos->delete();
					}
					else{ //tidak ada barang yg sama di PO yang dituju; cukup pindah saja isi_posnya
						//belum check
						// dd('gege');
						
						// sudah pindah seluruh barang , ini untuk update harga baru isi po nya yang di pindah
						// saya lihat sudah dip indah semua tapi belum di update harga barunya
						// maaf kalau saya salah
						$isi->update(['harga'=>$update->split_harga,
									'purchaseorders_id'=>$po->id,
									'total_akhir'=>$update->split_harga * $update->split_jumlah
								]); 
						
						$update->update(['split_po_baru'=>$isi->id]);
						//$menampung['isipopertama'] = $isi;
						// dd($polama);
					}
				}
				else{ //tidak bikin PO baru
					// dd($d9);
					if($akankah_bertemu_dengan_barang_itu==1){ //menambahkan jumlah isi po ke PO yang sudah ada 
						// dd('bertemu');
						$isi = Isi_po::find($d9);
						//jika barang di PO sudah ada REVISI
						if(isset($isi->revisi->where('status','approve')->last()->jumlah_baru)){ // SOLUSI :buat revisi baru untuk isi po terkait atau bisa juga dengan memindah isipos id revisi yg dilakukan update ini... karena jika menunggu approval lama maka dilakukan buat baru ( memenuhi db ) PS: sebenarnya bisa dipindah saja id nya atau ditambahkan field baru pada revisi (po lama po baru jika split) 
							//IF ini perlu dilakukan test lagi; 14-des-17 4:02AM aman 
							// dd($update);
							$revisi = new Revisi_po();
							$revisi->created_at = now();
							$revisi->jumlah_lama = $isi->jumlah;
							$revisi->jumlah_baru = $update->split_jumlah + $isi->jumlah; //menambah jumlah barang di PO bukan overwrite jumlah
							$revisi->harga_po = $isi->harga; 
							$revisi->harga_baru = $update->split_harga; //harga yg dioverwrite
							$revisi->status = 'approve'; // langsung diapprove sehingga tampil perubahan harga dan jumlah
							$revisi->tgl_revisi = date('Y-m-d');
							$revisi->keterangan = $update->keterangan;
							$revisi->user_id = $update->user_id;
							$revisi->isi_pos_id = $isi->id;
							$revisi->save();
							$isi->update(['jumlah'=>$isi->jumlah + $update->split_jumlah,
									'harga'=> $update->split_harga,
									'total_akhir'=>$update->split_harga * ($update->split_jumlah+$isi->jumlah),
									]);
						}
						else{ //isi_po tidak memiliki revisi ; 
							$isi->update(['jumlah'=>$isi->jumlah + $update->split_jumlah,
									'harga'=> $update->split_harga,
									'total_akhir'=>$update->split_harga * ($update->split_jumlah+$isi->jumlah),
									]);
						}
					}
					else{ //tidak ada barang yg sama di PO yg dituju
						 //dd('tidak bertemu');
						$isi = Isi_po::insert(['created_at'=>now(),
											'purchaseorders_id'=>$po->id,
											'jumlah'=>$update->split_jumlah,
											'harga'=>$update->split_harga,
											'total_akhir'=>$update->split_harga * $update->split_jumlah,
											'barangs_id'=>$update->isi_pos->barangs_id,
										]);

						$update->update(['split_po_baru'=>$isi]);
					}	
				}
				$isi_pos = $po->isi;
				
				//$menampung['isipokedua'] = $po->isi;
				// dd($isi_pos);
				//hitung PO lama
				///////
			}
			else{ // mengubah harga / jumlah saja tanpa split  (14-des-2017 sepertinya aman)
				$isi_pos = Isi_po::where('purchaseorders_id','=',$update->isi_pos->purchaseorders_id)
							->get();
				// dd($update->isi_pos);
				$po = $isi_pos[0]->purchaseorder;
			}
			$update->isi_pos->update(['total_akhir'=>$update->jumlah_baru*$update->harga_baru,
										'jumlah'=>$update->jumlah_baru,
										'harga'=>$update->harga_baru,
										]);
			$gg = $update->update(['status'=>'approve']);
			// dd($isi_pos);
			$total =0;
			foreach ($isi_pos as $key => $value) {
				$r = $value->revisi->where('status','approve')->last();
				if(isset($r->harga_baru)){
					$harga = $r->harga_baru;
					$jumlah = $r->jumlah_baru;
					// dd('akankah');
				}
				else{
					$harga = $value->harga;
					$jumlah = $value->jumlah;
				}
				$total += $harga*$jumlah;
			}
			// dd($total);
			// dd($po);
			// $total = $total - $isi_pos[0]->purchaseorder->$diskon;
			if($update->split==1){  //ada split maka parent split dihitung ulang (menghitung dgn jumlah baru yg dikurangi)
				if(count($polama->isi)==0){
					$polama->delete();
				}
				else{//hitung kembali total untuk po 
					foreach ($polama->isi as $key => $value) {
						$r = $value->revisi->where('status','approve')->last();
						if(isset($r->harga_baru)){
							$harga = $r->harga_baru;
							$jumlah = $r->jumlah_baru;
							// dd('akankah');
						}
						else{
							$harga = $value->harga;
							$jumlah = $value->jumlah;
						}
						$total2 += $harga*$jumlah;
					}
				}
				$polama->update(['total_akhir'=>$total2]); //po 2 dan total2 didefinisikan pada if split atas
			}
			$pricelist = Pricelist::where('barangs_id',$update->isi_pos->barangs_id)
									->where('suppliers_id',$po->customers_id)
									->latest()
									->first()
									;
			if(isset($pricelist)){
				$pricelist->update(['harga'=>$update->harga_baru]);
			}
			$po->update(['total_akhir'=>$total]); // menghitung po 1 (perlu menghitung TOTAL untuk PO yang dikurangkan isinya)
			//dd($menampung);
			if($gg==false){
				CRUDBooster::redirect(CRUDBooster::adminPath('revisi_pos'),'Gagal melakukan update');
			}
			else CRUDBooster::redirect(CRUDBooster::adminPath('revisi_pos'),'Data berhasil diupdate','success');
			CRUDBooster::redirect(CRUDBooster::adminPath('revisi_pos'),'Gagal melakukan update');
	    }

	    public function getDetail($id){
	    	$parent = Revisi_po::find($id)->isi_pos->purchaseorder->id;
	    	CRUDBooster::redirect(CRUDBooster::adminPath('isipurchaseorder/'.$parent),'');
	    }

	    public function getRevisiByPo($id){
	    	$hasil = Purchaseorder::find($id)->revisi_po()
	    				->leftjoin('cms_users','cms_users.id','=','revisi_pos.user_id')
	    				->leftjoin('suppliers','revisi_pos.split_suppliers_id','=','suppliers.id')
	    				->select('cms_users.name as user','revisi_pos.*','suppliers.nama')
	    				->get()
	    				;
	    	
	    	$data =[];
	    	foreach ($hasil as $key => $d) {
	    		$data[] = [
	    					$d->id,
							$d->tgl_revisi,
							$d->user,
							$d->isi_pos->barang->nama,
							$d->harga_po,
							$d->harga_baru,
							$d->jumlah_lama,
							$d->jumlah_baru,
							$d->nama,
							$d->keterangan,
							$d->status
						];
	    	}

	    	$results = [
	            "total" =>$total,
	            "draw" => 1,
	            "recordsTotal" => count($data),
	            "recordsFiltered" => count($data),
	            "data" => $data
            ];
	        echo json_encode($results);
	    }

	    public function namaBarang($id){
	    	$barang = DB::table('isi_pos')->leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')->where('isi_pos.id',$id)->select('barangs.nama')->first()->nama;
	    	return $barang;
	    }

	    public function update_isi_po_dengan_revisi(){
	    	$user = Cms_user::find(CRUDBooster::myId());
	    	$request = request();
	    	if (Hash::check($request->password, $user->password)) {
	    		// dd('password');
		    	$isi_po = Isi_po::has('revisi')->get();
		    	foreach ($isi_po as $key => $p) {
		    		$r = $p->revisi->where('status','approve')->last();
		    		$p->update(['total_akhir'=>$r->jumlah_baru*$update->harga_baru,
								'jumlah'=>$r->jumlah_baru,
								'harga'=>$r->harga_baru
								]);
		    	}

		    dd('done');
	    	}
	    	dd('fail');
	    }
	}