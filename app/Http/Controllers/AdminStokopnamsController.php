<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Isi_stokopnam;
	use App\Models\Stokopnam;
	use App\Models\Barang_gudang;

	class AdminStokopnamsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "stokopnams";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Tanggal","name"=>"tanggal"];
			$this->col[] = ["label"=>"Kode","name"=>"kode"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:4|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Kode Barang','name'=>'kode_barang','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			//$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'gudangs,nama'];
			//$this->form[] = ['label'=>'Gudangs Id','name'=>'gudangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Stok','name'=>'stok','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
	        $this->sub_module[] = ['label'=>'','path'=>'isi_stokopnams','parent_columns'=>'kode,tanggal','foreign_key'=>'stokopnams_id','button_color'=>'info','button_icon'=>'fa fa-bars'];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
	        // $this->index_button[] = ['label'=>'Add Stokopnam','color'=>'danger','url'=>'#'];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
	        $(document).ready( function() { 
	        	$(function(){
					$('#add-stokopnam').click(function(){
						c = confirm('Apakah anda yakin akan membuat data stokopnam baru?');
						if(c==true){
							gg = $('<form action=\"../tambahstokopnam\" method=\"POST\">' + 
						    '<input type=\"hidden\" name=\"aid\" value=\"'+1+' \">' +
						    '<input type=\"hidden\" name=\"_token\" value=\"'+$('meta[name=\"csrf-token\"]').attr('content')+'\">'+
						    '</form>').appendTo($(document.body))
						    // console.log(gg);
						    .submit();
						}
					});
				});
			});
	        ";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        // $this->load_js[] = asset('customs/js/stokopnam_index.js');
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function getadd(){
	    	$data = [];
	    	$data['page_title'] = 'Tambah Stokopnam';
	    	$this->cbView('customView.stokopnam_main',$data);
	    }

	    public function tambahstokopnam(){
	    	$request= request();
	    	//lama
	    	if($request->aid == 1){
	    		$id = Stokopnam::insertGetId(['created_at'=>now(),
	    												'tanggal'=>date('Y-m-d'),
	    											]);
		    	$stokopnams = Stokopnam::find($id)->update(['kode'=>'stokopnam-'.date('Y/m').'-'.$id]);
		    	// dd($stokopnams);
		    	$bg = DB::table('barang_gudangs')
		    			->leftJoin('barangs','barangs.id','=','barang_gudangs.barangs_id')
		    			->whereNull('barang_gudangs.deleted_at')
		    			->select('barang_gudangs.barangs_id','barang_gudangs.gudangs_id','barang_gudangs.stok','barangs.avgprice')
		    			->get()
		    			;
		    	$gg = [];
		    	foreach ($bg as $key => $value) {
		    		$value->created_at = now();
		    		$value->stokopnams_id = $id;
		    		if($value->avgprice==null){
		    			$value->avgprice=0;
		    		}
		    		$gg[] = (array)$value;
		    	}
		    	// dd($bg);
		    	Isi_stokopnam::insert($gg);
		    	CRUDBooster::redirect(CRUDBooster::adminPath('stokopnams'),'Berhasil Menambahkan Data','success');
		    	// dd($bg);
	    	}
	    	// dd('oke');
	    }

	    public function getadddt(){
	    	// $request= request();
	    	$lsto = DB::table('stokopnams')
	    				->whereNull('deleted_at')
	    				->latest()
	    				->first()
	    				;
	    	$barang_gudangs = Barang_gudang::whereNull('deleted_at')->get();

	    	// $barang_gudangs = DB::table('barang_gudangs')
	    	// 					->leftJoin('barangs','barangs.id','=','barang_gudangs.barangs_id')
	    	// 					->leftJoin('gudangs','gudangs.id','=','barang_gudangs.gudangs_id')
	    	// 					->leftJoin('isispbms','isispbms.barangs_id','=','barang_gudangs.barangs_id')
	    	// 					->leftJoin('barangdatangs',function($q){
	    	// 						$q->on('barangdatangs.id','=','isispbms.barangdatangs_id');
	    	// 						$q->on('barangdatangs.gudangs_id','=','barang_gudangs.gudangs_id');
	    	// 					})
	    	// 					->leftJoin('pengambilan_barangs','pengambilan_barangs.gudang_id','=','barang_gudangs.gudangs_id')
	    	// 					->leftJoin('detail_pengambilan_barangs',function($q){
	    	// 						$q->on('detail_pengambilan_barangs.barang_id','=','barang_gudangs.barangs_id');
	    	// 						$q->on('detail_pengambilan_barangs.pengambilan_barang_id','=','pengambilan_barangs.id');
	    	// 					})
	    	// 					// ->whereIn('gudangs.id','barang_gudangs.gudangs_id')
	    	// 					// ->whereDate('pengambilan_barangs.tgl_ambil','2017-01-01')
	    	// 					->whereNull('barang_gudangs.deleted_at')
	    	// 					->whereNull('pengambilan_barangs.deleted_at')
	    	// 					// ->get()
	    	// 					;
	    	// if(isset($lsto)){
	    	// 	// $barang_gudangs = $barang_gudangs->where('pengambilan_barangs.tgl_ambil','>',$lsto->tanggal)
	    	// 	// 								->where('barangdatangs.tanggaldatang','>',$lsto->tanggal)
	    	// 	// 								;
	    	// 	// dd($lsto->tanggal);
	    	// 	$barang_gudangs = $barang_gudangs
	    	// 					->select('barangs.id','gudangs.id as gid','barangs.kode as kbarang','barangs.nama as barang','gudangs.nama as gudang','barang_gudangs.id as bgid'
	    	// 						,DB::raw('IFNULL (barang_gudangs.stok,0) as stok')
	    	// 						,DB::raw('IFNULL (barangs.avgprice,0) as avgprice')
	    	// 						,DB::raw(
	    	// 							// 'CASE WHEN barangdatangs.tanggaldatang > DATE("'.$lsto->tanggal.'") THEN "oke" else 0 END as datang'
	    	// 							'SUM(
		    // 								CASE WHEN isispbms.barangs_id = barang_gudangs.barangs_id 
		    // 								AND isispbms.barangdatangs_id = barangdatangs.id 
		    // 								AND barangdatangs.gudangs_id = barang_gudangs.gudangs_id 
		    // 								AND barangdatangs.tanggaldatang > DATE("'.$lsto->tanggal.'") 
		    // 								THEN isispbms.jumlah ELSE 0 END
		    // 							) as datang'
		    // 						)
	    	// 						,DB::raw(
	    	// 							// 'CASE WHEN pengambilan_barangs.tgl_ambil = DATE("'.$lsto->tanggal.'") THEN "oke" else 0 END as keluar'
	    	// 							'SUM(
	    	// 								CASE WHEN detail_pengambilan_barangs.barang_id = barang_gudangs.barangs_id 
	    	// 								AND detail_pengambilan_barangs.pengambilan_barang_id = pengambilan_barangs.id 
	    	// 								AND pengambilan_barangs.gudang_id = barang_gudangs.gudangs_id 
	    	// 								AND pengambilan_barangs.tgl_ambil > DATE("'.$lsto->tanggal.'")
	    	// 								THEN detail_pengambilan_barangs.jumlah ELSE 0 END
	    	// 							) as keluar'
	    	// 						)
	    	// 					)
	    	// 					->groupBy('id','gid','kbarang','barang','gudang','stok','avgprice','bgid')
	    	// 					// ->tosql()
	    	// 					->get()
	    	// 					;
	    	// 					// dd($barang_gudangs);
	    	// }
	    	// else{
	    	// 	$barang_gudangs = $barang_gudangs
	    	// 					->select('barangs.id','gudangs.id as gid','barangs.kode as kbarang','barangs.nama as barang','gudangs.nama as gudang','barang_gudangs.id as bgid'
	    	// 						,DB::raw('IFNULL (barang_gudangs.stok,0) as stok')
	    	// 						,DB::raw('IFNULL (barangs.avgprice,0) as avgprice')
	    	// 						,DB::raw('SUM(CASE WHEN isispbms.barangs_id = barang_gudangs.barangs_id AND isispbms.barangdatangs_id = barangdatangs.id AND barangdatangs.gudangs_id = barang_gudangs.gudangs_id THEN isispbms.jumlah ELSE 0 END) as datang')
	    	// 						,DB::raw('SUM(CASE WHEN detail_pengambilan_barangs.barang_id = barang_gudangs.barangs_id AND detail_pengambilan_barangs.pengambilan_barang_id = pengambilan_barangs.id AND pengambilan_barangs.gudang_id = barang_gudangs.gudangs_id THEN detail_pengambilan_barangs.jumlah ELSE 0 END) as keluar')
	    	// 					)
	    	// 					->groupBy('id','gid','kbarang','barang','gudang','stok','avgprice','bgid')
	    	// 					->get();
	    	// }
	    	// dd($barang_gudangs);
	    	$data=[];
	    	foreach ($barang_gudangs as $key => $v) {
	    		$action = '<input type="number" min="0" name="input['.$v->bgid.'][stok]" value="'.$v->stok.'" />';
	    		$action .= '<input type="hidden" min="0" name="input['.$v->bgid.'][barangs_id]" value="'.$v->id.'" />';
	    		$action .= '<input type="hidden" min="0" name="input['.$v->bgid.'][gudangs_id]" value="'.$v->gid.'" />';
	    		$action .= '<input type="hidden" min="0" name="input['.$v->bgid.'][avgprice]" value="'.$v->avgprice.'" />';
	    		$action .= '<input type="hidden" min="0" name="input['.$v->bgid.'][stok_masuk]" value="'.$v->datang.'" />';
	    		$action .= '<input type="hidden" min="0" name="input['.$v->bgid.'][stok_keluar]" value="'.$v->keluar.'" />';
	    		// $data[] = [$v->id,
	    		// 			$v->kbarang,
	    		// 			$v->barang,
	    		// 			$v->gudang,
	    		// 			$v->stok,
	    		// 			$v->datang,
	    		// 			$v->keluar,
	    		// 			$action];
	    		if(isset($lsto)){
	    			$datang = $v->barang->isispbmr()->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    						->where('barangdatangs.gudangs_id','=',$v->gudangs_id)
	    						->whereDate('barangdatangs.tanggaldatang','>',$lsto->tanggal)
	    						->sum('jumlah');
					$keluar = $v->barang->detail_pengambilan_barang()->leftJoin('pengambilan_barangs','pengambilan_barangs.id','=','detail_pengambilan_barangs.pengambilan_barang_id')
	    						->where('pengambilan_barangs.gudang_id','=',$v->gudangs_id)
	    						->whereDate('pengambilan_barangs.tgl_ambil','>',$lsto->tanggal)
	    						->sum('jumlah');
	    		}
	    		else{
	    			$datang = $v->barang->isispbmr()->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    						->where('barangdatangs.gudangs_id','=',$v->gudangs_id)->sum('jumlah');
	    			$keluar = $v->barang->detail_pengambilan_barang()->leftJoin('pengambilan_barangs','pengambilan_barangs.id','=','detail_pengambilan_barangs.pengambilan_barang_id')
	    						->where('pengambilan_barangs.gudang_id','=',$v->gudangs_id)->sum('jumlah');
	    		}
	    		$data[] = [$v->barang->id,
	    					$v->barang->kode,
	    					$v->barang->nama,
	    					$v->gudang->nama,
	    					$v->stok,
	    					$datang,
	    					$keluar,
	    					$action];
	    	}
			$results = [
	    				"draw" => 0,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	return response()->json($results);
	    }

	    public function postadd(){
	    	// dd('wut?');
	    	$request = Request::except('_token');
			$exploded = explode('&', $request['input']);
			// dd($exploded);
			$dataku = [];
			$sid = Stokopnam::insertGetId(['created_at'=>now(),'tanggal'=>date('Y-m-d')]);
	    	$stokopnams = Stokopnam::find($sid)->update(['kode'=>'stokopnam-'.date('Y/m').'-'.$sid]);
			foreach ($exploded as $row) {
	            $temp = array();
	            parse_str($row, $temp);
	            list($key, $value) = each($temp);
	            if(!empty($value)) {
	            	if($key=='input'){
	            		$k = key($value);
		            	$k2 = key($value[$k]);
		            	// $k3 = key($value[$k][$k2]);
		            	// dd($value[$k][$k2]);
		                $dataku[$k][$k2] = $value[$k][$k2];
		                if(!isset($dataku[$k]['stokopnams_id'])){
		                	$dataku[$k]['stokopnams_id'] = $sid;
		                }
	            	}
	            }
	            // dd($value);
	        }   
	        // dd($dataku);
	        DB::table('isi_stokopnams')->insert($dataku);
	        CRUDBooster::redirect(CRUDBooster::adminPath('stokopnams'),'Berhasil Menambahkan Data','success');
	    }
	}