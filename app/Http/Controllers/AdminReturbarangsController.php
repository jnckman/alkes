<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

	class AdminReturbarangsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "Returbarangs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Noretur","name"=>"noretur"];
			$this->col[] = ["label"=>"Totalretur","name"=>"totalretur"];
			$this->col[] = ["label"=>"Barangdatangs Id","name"=>"barangdatangs_id","join"=>"barangdatangs,nospbm"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Noretur','name'=>'noretur','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Totalretur','name'=>'totalretur','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Barangdatangs Id','name'=>'barangdatangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangdatangs,id'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Noretur","name"=>"noretur","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Totalretur","name"=>"totalretur","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Barangdatangs Id","name"=>"barangdatangs_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"barangdatangs,id"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }
	    public function listreturdtajax($id){
	    	$hasil = DB::table('returbarangs')
	    	
	    	->leftJoin('purchaseorders','returbarangs.purchaseorders_id','=','purchaseorders.id')
	    	->select('returbarangs.id','returbarangs.noretur','returbarangs.created_at','returbarangs.noretur','returbarangs.totalretur')
	    	->whereNull('returbarangs.deleted_at')
	    	->where('returbarangs.purchaseorders_id',$id)
	    	->get();
	    	
	    	$status = '';
	    	foreach($hasil as $d){
	    				

	    				$data[] = [
	    		 				   $d->id,
	    		 				   $d->noretur,
	    		 				   $d->created_at,
	    		 				   $d->total,
	    		 				   '<button class="btn btn-success btn-xs">Edit Retur</button>'
	    		 				   ];


	    		 		$urutan++;
	    			 }
	    				$results = [
	    				"total" =>$total,
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);
	    }
	    public function returisispbm($id){
	    	$data['id'] = $id;

	    	$data['po'] = DB::table('isispbms')
	    	->leftJoin('barangdatangs','barangdatangs.id','=','isispbms.barangdatangs_id')
	    	->leftJoin('barangs','barangs.id','=','isispbms.barangs_id')
	    	->select('barangs.nama','isispbms.jumlah',DB::raw('sum(isispbms.jumlah) as jumlahtotal'))
	    	->where('barangdatangs.purchaseorders_id',$id)
	    	->wherenull('isispbms.deleted_at')
	    	->groupby('isispbms.barangs_id','barangs.nama','isispbms.jumlah')
	    	->get();

	    	

	    	
	    	
    		$datatemplistspbm = DB::table('barangdatangs')->where('purchaseorders_id',$id)->wherenull('deleted_at')->get();
    		foreach ($datatemplistspbm as $value) {
					
					$resultlistspbm[] = ([
						'id' => $value->id,
						'text' => $value->nospbm,
					]);
					
			}
			$data['listspbm'] = json_encode($resultlistspbm);
    		$this->cbView('customView.listreturbarangs',$data);
	    }
	    public function postnewretur(Request $request){
	    	$input = Request::all();
	    	foreach ($input['jumlahretur'] as $b) {
	    		if($b > $input['total'][$index] || $b < 0){
    				return redirect()->back()->with(['message'=>'Maaf Jumlah Retur Tidak Tepat','message_type'=>'danger']);
    			}
    			$index++;
	    	}
	    	$data = DB::table('returbarangs')->insert(
    				['created_at'=>Carbon::now()->toDateTimeString(),
    				 'noretur'=>$input['noretur'],
    				 'purchaseorders_id'=>$input['purchaseorders_id'],
    				 'status'=>'created'
    				 ]
    		);
    		$total = 0;
    		$rowId = DB::connection() -> getPdo() -> lastInsertId();
    		$index = 0;
    		foreach ($input['jumlahretur'] as $b) {

    			
    			$data = DB::table('isireturbarangs')->insert(
    				['created_at'=>Carbon::now()->toDateTimeString(),
    				 'catatan' => '',
    				 'isispbms_id'=>0,
    				 'jumlah'=>$b,
    				 'returbarangs_id'=>$rowId
    				 ]
    			);
    			$total = $b + $total;
    			
    		}
    		$data = DB::table('returbarangs')->where('id',$rowId)->update(
    				['total' => $total
    				]
    		);
	    }

	    public function getlastreturid(){
	    	$hasil =  DB::table('returbarangs')->whereNull('deleted_at')->orderby('id','desc')->first();
	    	if(sizeof($hasil)==0){
	    		return 0;
	    	}
	    	else
	    	{
	    		return $hasil->id;
	    	}
	    }


	    //By the way, you can still create your own method in here... :) 


	}