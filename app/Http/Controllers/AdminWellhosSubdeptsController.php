<?php namespace App\Http\Controllers;

  use Session;
  use Request;
  use DB;
  use CRUDBooster;
  use Carbon\Carbon;
  use App\Models\Wellhos_subdept;
  use App\Models\Mr_subdept;
  use App\Models\Sales_order;
  use App\Models\Isi_mr_subdept;
  use App\Models\Purchaseorder;
  use App\Models\Isi_po;
  use App\Models\Lock_rekap;

  class AdminWellhosSubdeptsController extends \crocodicstudio\crudbooster\controllers\CBController {

      public function cbInit() {

      # START CONFIGURATION DO NOT REMOVE THIS LINE
      $this->title_field = "nama";
      $this->limit = "20";
      $this->orderby = "id,desc";
      $this->global_privilege = false;
      $this->button_table_action = true;
      $this->button_bulk_action = true;
      $this->button_action_style = "button_icon";
      $this->button_add = true;
      $this->button_edit = true;
      $this->button_delete = true;
      $this->button_detail = true;
      $this->button_show = true;
      $this->button_filter = true;
      $this->button_import = true;
      $this->button_export = false;
      $this->table = "wellhos_subdepts";
      # END CONFIGURATION DO NOT REMOVE THIS LINE

      # START COLUMNS DO NOT REMOVE THIS LINE
      $this->col = [];
      $this->col[] = ["label"=>"Nama","name"=>"nama"];
      $this->col[] = ["label"=>"Id","name"=>"id"];
      $this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];
      $this->col[] = ["label"=>"Phone","name"=>"phone"];
      $this->col[] = ["label"=>"Email","name"=>"email"];
      // $this->col[] = ["label"=>"Pic","name"=>"pic"];
      $this->col[] = ["label"=>"Wellhos Departments_id","name"=>"wellhos_departments_id","join"=>"wellhos_departments,nama"];
      $this->col[] = ["label"=>"Deleted At","name"=>"deleted_at"];
      $this->col[] = ["label"=>"Created At","name"=>"created_at"];
      $this->col[] = ["label"=>"Updated At","name"=>"updated_at"];
      # END COLUMNS DO NOT REMOVE THIS LINE

      # START FORM DO NOT REMOVE THIS LINE
      $this->form = [];
      $this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
      $this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
      $this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
      $this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:wellhos_subdepts','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
      // $this->form[] = ['label'=>'Pic','name'=>'pic','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
      $this->form[] = ['label'=>'Wellhos Departments Id','name'=>'wellhos_departments_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'wellhos_departments,nama'];
      # END FORM DO NOT REMOVE THIS LINE

      # OLD START FORM
      //$this->form = [];
      //$this->form[] = ["label"=>"Nama","name"=>"nama","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
      //$this->form[] = ["label"=>"Keterangan","name"=>"keterangan","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
      //$this->form[] = ["label"=>"Phone","name"=>"phone","type"=>"number","required"=>TRUE,"validation"=>"required|numeric","placeholder"=>"You can only enter the number only"];
      //$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:wellhos_subdepts","placeholder"=>"Please enter a valid email address"];
      //$this->form[] = ["label"=>"Pic","name"=>"pic","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
      //$this->form[] = ["label"=>"Wellhos Departments Id","name"=>"wellhos_departments_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"wellhos_departments,nama"];
      # OLD END FORM


        $this->sub_module = array();
        $this->sub_module[] = ['label'=>'','path'=>'mr_subdepts36','parent_columns'=>'nama','foreign_key'=>'wellhos_subdepts_id','button_color'=>'success','button_icon'=>'fa fa-bars'];
        $this->addaction = array();
        $this->addaction[] = ['label'=>'','url'=>'rekap','icon'=>'fa fa-refresh','color'=>'success','showIf'=>'true'];
        $this->button_selected = array();
        $this->alert        = array();
        $this->index_button = array();
        $this->table_row_color = array();
        $this->index_statistic = array();
        $this->script_js = null;
        $this->pre_index_html = null;
        $this->post_index_html = null;
        //$this->load_js[] = asset("js/barang.js");
        $this->style_css = NULL;
        $this->load_css = array();
      }

     public function actionButtonSelected($id_selected,$button_name) {}
     public function hook_query_index(&$query) {

     }
     public function hook_row_index($column_index,&$column_value) {

     }
     public function hook_before_add(&$postdata) {}
     public function hook_after_add($id) {}
     public function hook_before_edit(&$postdata,$id) {}
     public function hook_after_edit($id) {}
     public function hook_before_delete($id) {}
     public function hook_after_delete($id) {}


     public function mr($id_periode_mr, $id_department){
      // dd($id_department);
        $data= array();
        $result = DB::table('wellhos_departments')->where('id',$id_department)->whereNull('wellhos_departments.deleted_at')->get();
        $data['id'] = $id_department;
        $data['result'] = $result;
      $data['id_periode'] = $id_periode_mr;
      // dd($data);
        $this->cbView('customView.mrbysystem',$data);
      }

      public function mrdtajax($id, $id_list){

        // $hasil = DB::table('wellhos_subdepts')->select('id','nama','phone','email')->whereNull('deleted_at')->where('wellhos_departments_id',$id)->get();
        // $id_list == periode_id ; $id = dept_id
        $hasil = Wellhos_subdept::where('wellhos_departments_id',$id)
                                        ->whereHas('mr_subdept', function ($query) use ($id_list) {
                                            $query->where('periode_mrs_id', '=', $id_list);
                                        })
                                        ->get()
                                        ;
                                        // dd($hasil);

       foreach($hasil as $d){
          $namaku = $d->nama.' ( '.$d->mr_subdept->where('periode_mrs_id','=', $id_list)->count().' MR )';
          $data[] = [$d->id,
                 $namaku,
                 $d->phone,
                 $d->email,
              '<a href="'.route('getSubdeptmrlist', ['id' => $d->id, 'id_listdept' => $id_list]).'"><button class="btn btn-xs btn-success" style="margin-left:5px;">Lihat MR </button></a>'.
                 '<a href="../subdeptmrlist/'.$d->id.'"><button class="btn btn-xs btn-info" style="margin-left:5px;">Print MR </button></a>',

                 ];
         }
        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }

      public function subdeptmrlist($id, $id_list){
        $result = DB::table('wellhos_subdepts')->where('id',$id)->whereNull('wellhos_subdepts.deleted_at')->get();

        $data= array();
        $data['id']= $id;
      $data['id_listdept'] = $id_list;
        $data['result'] = $result;
        $this->cbView('customView.subdeptmrlist',$data);
      }

      public function subdeptmrlistdtajax($id,$idperiode){
        $hasil = DB::table('mr_subdepts')
              ->leftJoin('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
              ->leftJoin('master_projects','projects.master_projects_id','=','master_projects.id')
              ->select('mr_subdepts.id','mr_subdepts.nomr','projects.nama','projects.pic','mr_subdepts.gudang','projects.area')->whereNull('wellhos_subdepts.deleted_at')->where('mr_subdepts.periode_mrs_id',$idperiode)
              ->whereNull('mr_subdepts.deleted_at')
              ->where('wellhos_subdepts.id',$id)->get();
        foreach($hasil as $d){
          $data[] = [
                 $d->nomr,
                 $d->nama.'-'.$d->area,
                 $d->pic,
                 $d->gudang,
                 '<a href="../../isisubdeptmr/'.$d->id.'">Lihat Isi MR </a>',

                 ];
         }
        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }

      public function isisubdeptmr($id){

        $data= array();
        $data['id']= $id;
        $data['result'] = DB::table('mr_subdepts')->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')->where('mr_subdepts.id',$id)
                          ->select('mr_subdepts.nomr','projects.area','mr_subdepts.total')
                          ->get();
        // dd($data);
        
        $this->cbView('customView.isisubdeptmr',$data);
      }

      public function isisubdeptmraktifkan($id){
        //$data = DB::table('mr_subdepts')->where('id',$id)->get();

        $data = Mr_subdept::find($id);

        $data->approval = 'oke';
        $data->save();
        return redirect()->back()->with(['message'=>'Berhasil di approve','message_type'=>'success']);

      }

      public function isisubdeptmrdtajax($id){

      if (Session::get('admin_id') == 1) $tombol_name = "Koreksi";
      else $tombol_name = "Edit";

        $hasil = DB::table('isi_mr_subdepts')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->select('barangs.nama','barangs.kode','isi_mr_subdepts.jumlah','barangs.akumstok','satuans.nama as satuan','isi_mr_subdepts.id as mrsubdeptid','barangs.avgprice','isi_mr_subdepts.keterangan','isi_mr_subdepts.id as id')
              ->where('isi_mr_subdepts.mr_subdepts_id',$id)
              ->whereNull('isi_mr_subdepts.deleted_at')->get();

              $data = array();
        foreach($hasil as $d){
          $data[] = [
                 $d->nama,
                 $d->kode,
                 $d->jumlah,
                 $d->akumstok,
                 $d->satuan,
                 $d->avgprice,
                 $d->avgprice*$d->jumlah,
                 $d->keterangan,
                 // '<a href="../isi_mr_subdepts38/edit/'.$d->id.'?return_url=admin/isisubdeptmr/'.$d->mrsubdeptid.'"><button class="btn btn-xs btn-info" style="margin-left:5px;" disable><i class="fa fa-scissors"></i> '.$tombol_name.'</button></a>'
                 '<form action="'.route('hapus_isi_mr').'" method="post">
                 '.csrf_field().'
                 <input type="hidden" name="id" value="'.$d->id.'" />
                 <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin akan menghapus isi MR beserta PR berkaitan?\')"><i class="fa fa-trash"></i></button>
                 </form>'
                 ];
         }
        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }

      public function hapus_isi_mr(){
        $request = request();
        $id = $request->id;
        $isi_mr = Isi_mr_subdept::find($id);
        $mr = $isi_mr->mr_subdept;
        if($mr->sudahpr==1){
          //hapus pr
          $pr = Lock_rekap::
            where([
              ['periode_mrs_id','=',$mr->periode_mrs_id],
              ['barangs_id','=',$isi_mr->barangs_id],
            ])
            // ->decrement('jumlah',$isi_mr->jumlah)
            // ->update(['jumlah'=>])
            ->first()
            ;
            $gg = $pr->actualmr - $isi_mr->jumlah;
            if($gg<0){
              $gg = 0;
            }
            $pr->update(['jumlah'=>$gg]);
            if($gg == 0){
              $pr->delete();
            }
        }
        $mr->update(['total'=>$mr->total-$isi_mr->total]);
        $isi_mr->delete();
        return redirect()->back()->with(['message'=>'Berhasil menghapus data','message_type'=>'success']);
      }

      public function rekapsubdeptmr($id,$idperiode){

        $result = DB::table('wellhos_departments')->where('id',$id)->whereNull('wellhos_departments.deleted_at')->get();

        $result2 = DB::table('mr_subdepts')
              ->leftJoin('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
              ->select('projects.nama')->whereNull('wellhos_subdepts.deleted_at')
              ->where('mr_subdepts.periode_mrs_id',$idperiode)
              ->where('wellhos_subdepts.id',$id)->get();

        $data= array();
        $data['id']= $id;
        $data['result'] = $result;
        $data['result2'] = $result2;
        $data['idperiode'] = $idperiode;
        $this->cbView('customView.subdeptmrrekap',$data);
      }

      public function rekapsubdeptmrdtajax($id,$idperiode){
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->select('barangs.nama','barangs.kode',DB::raw('sum(isi_mr_subdepts.jumlah) as total'),'barangs.akumstok','satuans.nama as satuan','barangs.avgprice')
              ->groupBy('isi_mr_subdepts.barangs_id','barangs.nama','barangs.kode','barangs.akumstok','satuans.nama','barangs.avgprice')
              ->where('mr_subdepts.wellhos_subdepts_id',$id)
              ->where('mr_subdepts.periode_mrs_id',$idperiode)
              ->whereNull('mr_subdepts.deleted_at')->get();

        foreach($hasil as $d){
          $data[] = [
                 $d->nama,
                 $d->kode,
                 $d->total,
                 $d->akumstok,
                 $d->satuan,
                 $d->avgprice,
                 $d->avgprice*$d->total,
                 ];
         }
        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }


      public function rekapallsubdeptmr($id,$id_periode){
        $result = DB::table('wellhos_subdepts')->where('id',$id)->whereNull('wellhos_subdepts.deleted_at')->get();

        //dd($hasil);
        // $result2 = DB::table('mr_subdepts')
        //      ->leftJoin('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
        //      ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
        //      ->select('projects.nama')->whereNull('wellhos_subdepts.deleted_at')
        //      ->where('wellhos_subdepts.id',$id)->get();

        $data= array();
        $data['id']= $id;
        $data['id_periode'] = $id_periode;
        $data['result'] = $result;
        $data['result2'] = $result2;
        $this->cbView('customView.rekapallsubdeptmr',$data);
      }

      public function rekapallsubdeptmrdtajax($id,$id_periode){
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->Join('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->select('barangs.nama','barangs.kode',DB::raw('sum(isi_mr_subdepts.jumlah) as total'),'barangs.akumstok','satuans.nama as satuan','barangs.avgprice')
              ->groupBy('isi_mr_subdepts.barangs_id','barangs.nama','barangs.kode','barangs.akumstok','satuans.nama','barangs.avgprice')
              ->where('mr_subdepts.periode_mrs_id',$id_periode)
              ->where('wellhos_subdepts.wellhos_departments_id',$id)
              ->whereNull('mr_subdepts.deleted_at')->get();

        foreach($hasil as $d){
          $data[] = [
                 $d->nama,
                 $d->kode,
                 $d->total,
                 $d->akumstok,
                 $d->satuan,
                 $d->avgprice,
                 $d->avgprice*$d->total
                 ];
         }

        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }

      public function rekapalldept($id){
        $result = DB::table('wellhos_subdepts')->where('id',$id)->whereNull('wellhos_subdepts.deleted_at')->get();

        //dd($hasil);
        // $result2 = DB::table('mr_subdepts')
        //      ->leftJoin('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
        //      ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
        //      ->select('projects.nama')->whereNull('wellhos_subdepts.deleted_at')
        //      ->where('wellhos_subdepts.id',$id)->get();

        $data= array();
      $data['id']= $id;
      //NOTE ini mau buat apa juga, 2 baris bawah
      //    $data['result'] = $result;
      //    $data['result2'] = $result2;
        $this->cbView('customView.rekapalldept',$data);
      }
      public function rekapalldeptdtajax($id){
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->Join('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->select('barangs.nama','barangs.kode',DB::raw('sum(isi_mr_subdepts.jumlah) as total'),'barangs.akumstok','satuans.nama as satuan','barangs.avgprice','barangs.min','barangs.id')
              ->groupBy('isi_mr_subdepts.barangs_id','barangs.nama','barangs.kode','barangs.akumstok','satuans.nama','barangs.avgprice','barangs.min','barangs.id')
              ->whereNull('mr_subdepts.deleted_at')
              ->where('mr_subdepts.periode_mrs_id',$id)
              ->get();
        $data = array();
        foreach($hasil as $d){
          $hasil =0;
          if($d->akumstok <= $d->min){
            $hasil = '<p
            style="
                margin-bottom: 0px;
                padding-left: 10px;
                width: 100%;
                background-color: #f0b7b7;
            "
            >'.$d->akumstok.' <b>*stok darurat</b></p>';
          }
          else{
            $hasil = '<p
            style="
                margin-bottom: 0px;
                padding-left: 10px;
                width: 100%;

            "
            >'.$d->akumstok.'</p>';
          }

          $data[] = [
                 '<a href="barang/detail/'.$d->id.'">'.$d->nama.'</a>',
                 $d->kode,
                 $d->total,
                 $hasil,
                 $d->min,
                 $d->satuan,
                 $d->avgprice,
                 $d->avgprice*$d->total


                 ];
         }

        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              $results = [
              "draw" => 1,
                  "recordsTotal" => count($data),
                  "recordsFiltered" => count($data),
                  "data" => $data
                  ];
        echo json_encode($results);

      }

      public function lockingrekap($id){
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->Join('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->select('barangs.id','barangs.nama','barangs.kode',DB::raw('sum(isi_mr_subdepts.jumlah) as total'),'barangs.akumstok','satuans.nama as satuan','mr_subdepts.id as mrsubdeptid','barangs.min','barangs.akumstok','barangs.priclists_id')  
              ->groupBy('isi_mr_subdepts.barangs_id','barangs.nama','barangs.kode','barangs.akumstok','satuans.nama','barangs.id','mr_subdepts.id','barangs.min','barangs.akumstok','barangs.priclists_id')
              ->where('mr_subdepts.periode_mrs_id',$id)
              ->whereNull('mr_subdepts.deleted_at')->get();

        

        
        if(sizeof($hasil)==0){
          return redirect()->back()->with(['message'=>'Maaf tidak ditemukan data / MR masih kosong','message_type'=>'danger']);
        }
        $datanonrekap = array();

        // mr 20, stok 0 , min 5
        //20- -5
        // mr 30 stok 45 , min 10
        foreach($hasil as $h){
          $actualmr = $h->total - ($h->akumstok - $h->min);
          
          if($actualmr > 0) //jika membutuhkan mr maka jumlah di isi total yang dibeli
            if($h->pricelists_id == null)
              DB::table('lock_rekaps')->insert(
                ['periode_mrs_id' => $id,
                 'status' => 'fresh added',
                 'jumlah' => $h->total - ($h->akumstok - $h->min),
                 'barangs_id'=>$h->id,
                 'actualmr' => $h->total,
                 'actualpo' => $h->total - ($h->akumstok - $h->min),

                 ]
              );
            else
              DB::table('lock_rekaps')->insert(
                ['periode_mrs_id' => $id,
                 'status' => 'fresh added',
                 'jumlah' => $h->total,
                 'barangs_id'=>$h->id,
                 'priclists_id'=>$h->pricelists_id,
                 'actualpo'=>$h->total,
                 'actualmr' => $h->total,

                 ]
              );
          // else
          //     DB::table('lock_rekaps')->insert(
          //       ['periode_mrs_id' => $id,
          //        'status' => 'fresh added',
          //        'jumlah' => 0,
          //        'barangs_id'=>$h->id,
          //        'actualmr' => $h->total

          //        ]
          //     );

          DB::table('mr_subdepts')->where('id',$h->mrsubdeptid)->update(['approval'=>'oke']);
          //DB::table('periode_mrs')->where('id',$id)->update(['selesai'=>1]);
        }
        //DB::table('periode_mrs')->where('id',$id)->update(['selesai'=>'1']);

        return back();
      }

      public function lockingrekaprevisi(Request $request){
        $terima = Request::all();
        $id = $terima['id'];
        if(sizeof($terima['mrid']) == 0)
          return redirect()->back()->with(['message'=>'Maaf tidak ditemukan data / MR masih belum di check','message_type'=>'danger']);

        
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->Join('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
              ->select('barangs.id',
                'barangs.nama',
                'barangs.kode',
                DB::raw('sum(isi_mr_subdepts.jumlah) as total'),
                'barangs.akumstok',
                'satuans.nama as satuan',
                'barangs.min',
                'barangs.akumstok',
                'barangs.priclists_id')  
              ->groupBy('isi_mr_subdepts.barangs_id',
                'barangs.nama','barangs.kode',
                'barangs.akumstok',
                'satuans.nama',
                'barangs.id',
                'barangs.min',
                'barangs.akumstok',
                'barangs.priclists_id')
              ->where('mr_subdepts.periode_mrs_id',$id)
              ->whereIn('mr_subdepts.id', $terima['mrid'])
              ->whereNull('mr_subdepts.deleted_at')->get();
        

        
        DB::table('mr_subdepts')->whereIn('mr_subdepts.id', $terima['mrid'])->update(['sudahpr'=>1]);
        
        
        if(sizeof($hasil)==0){
          return redirect()->back()->with(['message'=>'Maaf tidak ditemukan data / MR masih kosong','message_type'=>'danger']);
        }
        $datanonrekap = array();

        // mr 20, stok 0 , min 5
        //20- -5
        // mr 30 stok 45 , min 10
        $lastid = DB::table('lock_rekaps')->latest('id')->first()->id;

        $lastid++;
        foreach($hasil as $h){
          $actualmr = $h->total - ($h->akumstok - $h->min);
          
          if($actualmr > 0) //jika membutuhkan mr maka jumlah di isi total yang dibeli
            if($h->priclists_id == null)
              DB::table('lock_rekaps')->insert(
                ['periode_mrs_id' => $id,
                 'status' => 'fresh added',
                 'jumlah' => $h->total - ($h->akumstok - $h->min),
                 'barangs_id'=>$h->id,
                 'actualmr' => $h->total,
                 'tanggalrekap' => date("Y-m-d"),
                 'kode' => 'PR-'.date("Ymd-H").''.$lastid,
                 'actualpo'  => $h->total,

                 ]
              );
            else
              DB::table('lock_rekaps')->insert(
                ['periode_mrs_id' => $id,
                 'status' => 'fresh added',
                 'jumlah' => $h->total - ($h->akumstok - $h->min),
                 'barangs_id'=>$h->id,
                 'actualmr' => $h->total,
                 'pricelists_id'=>$h->priclists_id,
                 'tanggalrekap' => date("Y-m-d"),
                 'kode' => 'PR-'.date("Ymd-H").''.$lastid,
                 'actualpo'  => $h->total,

                 ]
              );
          // else
          //     DB::table('lock_rekaps')->insert(
          //       ['periode_mrs_id' => $id,
          //        'status' => 'fresh added',
          //        'jumlah' => 0,
          //        'barangs_id'=>$h->id,
          //        'actualmr' => $h->total,
          //        'tanggalrekap' => date("Y-m-d"),

          //        ]
          //     );

//          DB::table('mr_subdepts')->where('id',$h->mrsubdeptid)->update(['approval'=>'oke']);
//          DB::table('periode_mrs')->where('id',$id)->update(['selesai'=>1]);
        }
  //      DB::table('periode_mrs')->where('id',$id)->update(['selesai'=>'1']);

        return back();
      }

      public function lihatpembukuanrekap($id,$tgl){


      $lock_rekaps = DB::table('lock_rekaps')
      ->leftJoin('barangs','lock_rekaps.barangs_id','barangs.id')
      ->select('barangs.id','barangs.nama')
      ->where('lock_rekaps.periode_mrs_id',$id)
      ->where('lock_rekaps.kode','LIKE',$tgl)
      ->whereNull('lock_rekaps.deleted_at')->get();

      

      $supplier = DB::table('suppliers')->select('id','nama')->whereNull('deleted_at')->get();

      foreach ($lock_rekaps as $b) {
        $barang_terpilih[]=$b;
        $barangst[] = ([
          'id' => $b->id,
          'text' => 'nama : '.$b->nama.' | '.$b->kode,
        ]);
      }

      foreach ($supplier as $b) {
        $suppliers[] = ([
          'id' => $b->id,
          'text' => 'nama : '.$b->nama,
        ]);
      }
      $result2 = DB::table('periode_mrs')->where('id',$id)->get();
       $data= array();
       $data['id']= $id;
       $data['result'] = $result;
       $data['result2'] = $result2;
       $data['barang'] = json_encode($barangs);
       $data['supplier'] = json_encode($suppliers);
       $data['barang2'] = $barang_terpilih;
       $data['supplier2'] = $supplier;
       $data['tgl'] = $tgl;
      // dd($data['supplier');
      // dd($supplier);
      $this->cbView('customView.pembukuanrekap',$data);
     }

      public function tambahbarangpricelist(Request $request){
        $input = Request::all();
        $s = DB::table('suppliers')->where('id',$input['suppliers_id'])->get();
        $pl = DB::table('pricelists')->where('barangs_id',$input['barangs_id'])->where('suppliers_id',$input['suppliers_id'])->whereNull('deleted_at')->get();
        if(sizeof($pl) !=0)
          return redirect()->back()->with(['message'=>'Maaf data sudah ada di pricelist','message_type'=>'danger']);
        DB::table('pricelists')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'nama'=>'harga -'.$input['harga'].'-'.$s[0]->nama.' - '.$input['barangs_id'],
          'harga'=>$input['harga'],
          'barangs_id'=>$input['barangs_id'],
          'suppliers_id'=>$input['suppliers_id']

        ]);
        $rowId = DB::connection() -> getPdo() -> lastInsertId();
        DB::table('lock_rekaps')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'barangs_id'=>$input['barangs_id'],
          'pricelists_id'=>$rowId,
          'status'=>'add new',
          'jumlah'=>$input['jumlah'],
          'periode_mrs_id'=>$input['periode_mrs_id']

        ]);
        $rowId = DB::connection() -> getPdo() -> lastInsertId();
        DB::table('logpurchaserequests')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'lock_rekaps_id'=>$rowId,
          'act'=>'add new on page',
        ]);

        return redirect()->back()->with(['message'=>'Barang Berhasil di buat','message_type'=>'success']);

      }

     public function lihatpembukuanrekapdtajax($id,$tgl){

      $hasil = DB::table('lock_rekaps')
            ->leftJoin('barangs','barangs.id','=','lock_rekaps.barangs_id')
            ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
            ->leftJoin('pricelists','pricelists.id','=','lock_rekaps.pricelists_id')
            ->leftJoin('suppliers','pricelists.suppliers_id','=','suppliers.id')
            ->select('lock_rekaps.id',
                'barangs.nama',
                'barangs.kode',
                'lock_rekaps.jumlah','lock_rekaps.actualmr',
                'barangs.avgprice',
                'satuans.nama as satuan',
                'barangs.id as barangid',
                'suppliers.nama as namasup',
                'pricelists.harga',
                'barangs.min',
                'barangs.max','barangs.akumstok',
                'lock_rekaps.actualpo',
                'lock_rekaps.approve')
            ->whereNull('lock_rekaps.deleted_at')
            ->where('lock_rekaps.kode','LIKE',$tgl)
            ->where('lock_rekaps.periode_mrs_id',$id)
            ->get();
      // dd($hasil);

      $total = 0;
      $kata;
      $hargasup =0;
      $urutan = 0;
      $approvedstatus = 0;

      foreach($hasil as $d) {

        $hargasup = 0;
        if($d->jumlah == 0)
          $d->nama = '<b style="color:green;font-weight:bolder;">'.$d->nama.'</b>';
        if($d->namasup == null){
          $kata='belum dipilih';
        }
        else{
          $kata = $d->namasup.' - '.number_format($d->harga);
          if($actualpo > $jumlah)
          $hargasup = number_format($d->harga * $d->actualpo);
          else
          $hargasup = number_format($d->harga * $d->jumlah);
          $total = (($d->harga * $d->jumlah) + $total);
        }

        if($d->jumlah < $d->min && $d->min > $d->akumstok){
          $d->nama = '<b style="color:red">'.$d->nama.'</b>';
          $minmax = '<b style="color:red">'.$d->min.'</b> / '.$d->max;
        }
        elseif($d->jumlah > $d->max){
          $d->nama = $d->nama;
          $minmax = ''.$d->min.'<b style="color:red"> / '.$d->max.' </b>';
        }
        else{
          $minmax =   ''.$d->min.' / '.$d->max;
        }

        //NOTE tombol checkbox
        if ($d->approve == 1) {
          $checkbox = '<span rekap_id="'.$d->id.'" class="centangbox box'.$urutan.'">Approved</span>';
          $action = '<button class="btn btn-xs btn-success isiharga" id="'.$d->barangid.'" idrekap ="'.$d->id.'" urutan="'.$urutan.'" disabled>
          <i class="fa fa-money"></i>
          </button>'.
        '<button class="btn btn-xs btn-success listmrpr" id="'.$d->barangid.'" style="margin-left:5px;"><i class="fa fa-list"></button>';

        }
        else {
          if($d->jumlah == 0){
            $approvedstatus = 1;
              $checkbox = '<input type="checkbox" rekap_id="'.$d->id.'" class="centangbox box'.$urutan.'" >';
              $action = '<button class="btn btn-xs btn-success isiharga"  id="'.$d->barangid.'" idrekap ="'.$d->id.'" urutan="'.$urutan.'">
              <i class="fa fa-money"></i></button>'.
            '<button class="btn btn-xs btn-success listmrpr" id="'.$d->barangid.'" style="margin-left:5px;"><i class="fa fa-list"></i></button>';
          }
          else{
              $approvedstatus = 1;
              $checkbox = '<input type="checkbox" rekap_id="'.$d->id.'" class="centangbox box'.$urutan.'" >';
              $action = '<button class="btn btn-xs btn-success isiharga" id="'.$d->barangid.'" idrekap ="'.$d->id.'" urutan="'.$urutan.'">
              <i class="fa fa-money"></i></button>'.
            '<button class="btn btn-xs btn-success listmrpr" id="'.$d->barangid.'" style="margin-left:5px;"><i class="fa fa-list"></i></button>';  
          }
          
        }

        //NOTE tombol action
        

        //NOTE harga selected
        $harga_selected = '<span id="pilihanharga'.$d->id.'">'.$kata.'</span>';
        $pobaru;
        if ($d->approve == 1) {
          if($d->actualpo==0)
            $pobaru = '<input type="text" name="pobaru[]" value="'.$d->jumlah.'" style="width:100%" readonly class="isijumlahpo isijumlahpo'.$d->id.'" urutan="'.$urutan.'" defaultvalue="'.$d->jumlah.'"></input>';
          else
            $pobaru = '<input type="text" name="pobaru[]" value="'.$d->actualpo.'" style="width:100%"  readonly class="isijumlahpo isijumlahpo'.$d->id.'" urutan="'.$urutan.'" defaultvalue="'.$d->actualpo.'" idrekap="'.$d->id.'"></input>';
        }
        else{
          if($d->actualpo==0)
            $pobaru = '<input type="text" name="pobaru[]" value="'.$d->jumlah.'" style="width:100%" class="isijumlahpo isijumlahpo'.$d->id.'" urutan="'.$urutan.'" defaultvalue="'.$d->jumlah.'"></input>';
          else
            $pobaru = '<input type="text" name="pobaru[]" value="'.$d->actualpo.'" style="width:100%" class="isijumlahpo isijumlahpo'.$d->id.'" urutan="'.$urutan.'" defaultvalue="'.$d->actualpo.'" idrekap="'.$d->id.'"></input>';
        }
        $data[] = [
              
               $d->id,
               $checkbox,
               $d->nama,
               $d->kode,
               $d->actualmr,
               $minmax,
               $d->akumstok,
               $d->jumlah,
               $d->satuan,
               $pobaru,
               $harga_selected,
               $hargasup,
              $action,
              
              
        ];
        $urutan++;

      }
      

      //$data[0]=[1,'a'];
      //$data[1]=[2,'b'];
      $results = [
        "total" =>$total,
        "approvedstatus"  => $approvedstatus,
        "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data
          ];

        echo json_encode($results);
     }
      public function listmrprajax(Request $request){
        $input = Request::all();

        $data = DB::table('isi_mr_subdepts')
        ->leftJoin('mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
        ->leftJoin('periode_mrs','periode_mrs.id','=','mr_subdepts.periode_mrs_id')
        ->leftJoin('projects','mr_subdepts.projects_id','=','projects.id')
        ->select('isi_mr_subdepts.id','mr_subdepts.nomr','projects.nama','isi_mr_subdepts.jumlah','projects.area')
        ->where('isi_mr_subdepts.barangs_id',$input['id'])
        ->where('mr_subdepts.periode_mrs_id',$input['idperiode'])
        ->whereNull('isi_mr_subdepts.deleted_at')->get();

        echo json_encode($data);

      }
      public function gantijumlahisimrsubdept(Request $request){
        $input = Request::all();
        $data = DB::table('isi_mr_subdepts')->where('id',$input['id']);
        $tampung = $data->get()[0]->jumlah;
        $data->update(['jumlah'=>$input['jumlahbaru']]);

        $datarekap = DB::table('lock_rekaps')->where('barangs_id',$data->get()[0]->barangs_id);
        $datarekapupdate = $datarekap->where('periode_mrs_id',$input['idperiode'])->update([
          'jumlah'=> $datarekap->get()[0]->jumlah - $tampung + $input['jumlahbaru']
        ]);




        $hasil = 'ok';
        DB::table('logpurchaserequests')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'lock_rekaps_id'=>$datarekap->get()[0]->id,
          'act'=>'change stock request ammount',
          ]);
        echo json_encode($hasil);
      }
      public function ambilsupplier($id){
        echo json_encode(
          DB::table('pricelists')->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')->select('suppliers.nama','pricelists.id','pricelists.harga')->whereNull('pricelists.deleted_at')->where('pricelists.barangs_id',$id)->get()
        );
        

      }

      public function simpansupplier(Request $request){
         
          $data = DB::table('lock_rekaps')->where('id',$request::all()['id'])->update(
            [
             'pricelists_id'=>$request::all()['sup'],
             'actualpo'=>$request::all()['pobaru']

            ]

          );
          //dd($data);
          $hasil = DB::table('pricelists')->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
          ->select('pricelists.harga','suppliers.nama','pricelists.barangs_id')->where('pricelists.id',$request::all()['sup'])->whereNull('pricelists.deleted_at')->get();
          $kata[0] = $hasil[0]->nama.' - '.$hasil[0]->harga;
          $kata[1] = $hasil[0]->harga;
          
          DB::table('barangs')->where('id',$hasil[0]->barangs_id)->update(['priclists_id'=>$request::all()['sup']]);
          DB::table('barangs')->where('id',$hasil[0]->barangs_id)->update(['harga_beli_akhir'=>$kata[1]]);
          DB::table('logpurchaserequests')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'lock_rekaps_id'=>$request::all()['id'],
          'act'=>'change pricelist to '.$hasil[0]->harga,
          ]);
          echo json_encode($kata);
      }
      public function simpanjumlah(Request $request){
          
          $data = DB::table('lock_rekaps')->where('id',$request::all()['id'])->update(
            [
             'actualpo'=>$request::all()['pobaru']

            ]

          );
          //dd($data);
          DB::table('logpurchaserequests')->insert([
          'created_at'=>Carbon::now()->toDateTimeString(),
          'lock_rekaps_id'=>$request::all()['id'],
          'act'=>'change ammount to '.$request::all()['pobaru'],
          ]);
          $kata = '';
          echo json_encode($kata);
      }

      public function ajukanatasan($id){
        $data = DB::table('lock_rekaps')->whereNull('deleted_at')->where('periode_mrs_id',$id)->get();
        foreach ($data as $d) {
          if($d->suppliers_id == null){
            return "maaf ada yang belum di isi";
          }
        }
      }

      public function susunpurchaseorder($id,$kode){


        $data = DB::table('isi_mr_subdepts')
        ->leftJoin('mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
        ->leftJoin('lock_rekaps','lock_rekaps.barangs_id','=','isi_mr_subdepts.barangs_id')
        ->select('isi_mr_subdepts.barangs_id','lock_rekaps.barangs_id as rekapbarangid','lock_rekaps.pricelists_id','lock_rekaps.approve','lock_rekaps.actualmr','lock_rekaps.jumlah','lock_rekaps.actualpo')
        ->where('lock_rekaps.periode_mrs_id',$id)
        ->where('mr_subdepts.periode_mrs_id',$id)
        ->where('lock_rekaps.kode','LIKE',$kode)
        ->whereNull('isi_mr_subdepts.deleted_at')
        ->get();
        $cek = 0;
        foreach ($data as $d) {
          if($d->pricelists_id == null)
            //return redirect()->back()->with(['message'=>'Maaf terdapat barang yang belum diberikan harga','message_type'=>'danger']);
          if($d->approve == 0)
            return redirect()->back()->with(['message'=>'Maaf terdapat barang yang belum di approve','message_type'=>'danger']);
          if($d->jumlah == 0)
            $cek = 1;
        }
        if($cek == 1)
          return redirect()->back()->with(['message'=>'Maaf tidak ada PO , stok masih ada semua','message_type'=>'danger']);

        $data = DB::table('lock_rekaps')
        ->leftJoin('pricelists','lock_rekaps.pricelists_id','=','pricelists.id')
        ->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
        ->whereNUll('lock_rekaps.deleted_at')
        ->where('lock_rekaps.periode_mrs_id',$id)
        ->where('lock_rekaps.kode','LIKE',$kode)
        ->select('suppliers.nama')->groupBy('pricelists.suppliers_id','suppliers.nama')->get();


        $lastid = DB::table('purchaseorders')->orderby('id','Desc')->limit(1)->whereNull('deleted_at')->get();
        if(sizeof($lastid)==0)
          $last = 1;
        else
          $last = $lastid[0]->id;

        $result = DB::table('periode_mrs')->where('id',$id)->whereNull('deleted_at')->get();
        $data['last'] = $last;
        $data['result'] = $result;
        $data['id'] = $id;
        $data['kode'] = $kode;

          

        //dd($hasil);

        
        $this->cbView('customView.purchaseorder',$data);
      }

      public function susunpurchaseorderdtajax($id,$kode){
        // $hasil = DB::table('lock_rekaps')
        // ->leftJoin('pricelists','lock_rekaps.pricelists_id','=','pricelists.id')
        // ->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
        // ->leftJoin('purchaseorders','purchaseorders.customers_id','=','pricelists.suppliers_id')

        // ->select('suppliers.nama','suppliers.id','purchaseorders.id  as poid','purchaseorders.deleted_at','purchaseorders.periode_mrs_id')->groupBy('pricelists.suppliers_id','suppliers.nama','suppliers.id','purchaseorders.id','purchaseorders.deleted_at','purchaseorders.periode_mrs_id')
        // ->whereNUll('lock_rekaps.deleted_at')
        // ->where('lock_rekaps.periode_mrs_id',$id)

        // ->get();

        $hasil = DB::table('lock_rekaps')
        ->leftJoin('pricelists','lock_rekaps.pricelists_id','=','pricelists.id')
        ->leftJoin('suppliers','suppliers.id','=','pricelists.suppliers_id')
        ->leftJoin('purchaseorders',function($join){
          $join->on('purchaseorders.customers_id','=','pricelists.suppliers_id');
          $join->on('purchaseorders.periode_mrs_id','=','lock_rekaps.periode_mrs_id');
        })
        ->select('suppliers.nama','suppliers.id','purchaseorders.id  as poid','purchaseorders.deleted_at','lock_rekaps.periode_mrs_id')
        ->groupBy('pricelists.suppliers_id','suppliers.nama','suppliers.id','purchaseorders.id','purchaseorders.deleted_at','lock_rekaps.periode_mrs_id')
        ->where('lock_rekaps.jumlah','>',0)
        ->whereNUll('lock_rekaps.deleted_at')
        ->where('lock_rekaps.periode_mrs_id',$id)
        ->where('lock_rekaps.kode','LIKE',$kode)
        //->where('purchaseorders.periode_mrs_id',9)

        ->get();
        //dd(sizeof($hasil))
        //if(sizeof($hasil)==0){

        //  return redirect()->back()->with(['message'=>'Tidak Ada PO yang dibuat karena stok masih ada semua','message_type'=>'danger']);
        //}
        $data = array();
        foreach($hasil as $d){

              if($d->poid != null && $d->deleted_at == null && $d->periode_mrs_id == $id){
                     $button =  '<a href="../../isipurchaseorder/'.$d->poid.'"><button class="btn btn-xs btn-success" idrekap ="'.$d->id.'" >Lihat PO</button></a>'.
                       '<button class="btn btn-xs btn-info lihatisi" id="'.$d->id.'" style="margin-left:5px;">Lihat Isi [data PR]</button>

                       ';
                     }
              else{
                     $button =  '<button class="btn btn-xs btn-success buatform" id="'.$d->id.'" idrekap ="'.$d->id.'">Buat Form PO</button>'.
                       '<button class="btn btn-xs btn-info lihatisi" id="'.$d->id.'" style="margin-left:5px;">Lihat Isi [data PR]</button>';
                     }
              //$button =  '<button class="btn btn-xs btn-success buatform" id="'.$d->id.'" idrekap ="'.$d->id.'">Buat Form PO</button>'.
                       '<button class="btn btn-xs btn-info lihatisi" id="'.$d->id.'" style="margin-left:5px;">Lihat Isi [data PR]</button>';
              if($d->deleted_at == null)
              $data[] = [
                     $d->nama,$button





                     ];
              $total = ($d->jumlah * $d->avgprice) + $total;
             }

            //$data[0]=[1,'a'];
            //$data[1]=[2,'b'];
                  $results = [
                  "total" =>$total,
                  "draw" => 1,
                      "recordsTotal" => count($data),
                      "recordsFiltered" => count($data),
                      "data" => $data
                      ];
            echo json_encode($results);
      }

      public function ambilbarangpo($id,$mrid,$status,$kode){
        //$po = DB::table('purchaseorders')->where('customers_id',$id)->where('periode_mrs_id',$mrid)->whereNull('deleted_at')->get();
        
        $hasil['data'] = DB::table('lock_rekaps')
            ->leftJoin('barangs','barangs.id','=','lock_rekaps.barangs_id')
            ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
            ->leftJoin('pricelists','pricelists.id','=','lock_rekaps.pricelists_id')
            ->leftJoin('suppliers','pricelists.suppliers_id','=','suppliers.id')
            ->select('lock_rekaps.id','barangs.nama','barangs.kode','barangs.avgprice','satuans.nama as satuan','barangs.id as barangid','suppliers.nama as namasup','pricelists.harga','pricelists.id as priceid','suppliers.id as supid','suppliers.nama as supnama','lock_rekaps.periode_mrs_id','lock_rekaps.actualpo as jumlah')
            ->whereNull('lock_rekaps.deleted_at')
            ->where('suppliers.id',$id)
            ->where('lock_rekaps.periode_mrs_id',$mrid)
            ->where('lock_rekaps.kode','LIKE',$kode)
            ->get();

        $hasil['jenis'] = 'kosong';
        
        echo json_encode($hasil);
      }

      public function purchaseorder($id){

      }

      public function buatpurchaseorder(Request $request){
        $d = Request::all();
        
        
          
        $data = DB::table('purchaseorders')->insert(
            ['created_at'=>Carbon::now()->toDateTimeString(),
             'nopo' => $d['nopo'],
             'tglpo' => date("Y-m-d"),
             'kodesp' => $d['kodesp'],
             'customers_id'=>$d['pricelistsid'][0],
             'jenispo'=>$d['jenispo'],
             'tempo'=> date("Y-m-d"),
             'ongkir'=>$d['ongkir'],
             'periode_mrs_id'=>$d['periode_mrs_id'],
             'ppn'=>$d['ppn'],
             'hari'=>$d['hari'],
             'diskon'=>$d['diskontotal'],
             'total_akhir'=>$d['totalakhir']
             ]
          );
        $rowId = DB::connection() -> getPdo() -> lastInsertId();
        $ct = 0;

        foreach ($d['barangsid'] as $b) {
          $hasil = explode('%',$d['diskonbarang'][$ct]);
          
          
          DB::table('isi_pos')->insert(
                      ['created_at'=>Carbon::now()->toDateTimeString(),
                       'purchaseorders_id' => $rowId,
                       'barangs_id' => $d['barangsid'][$ct],
                       'jumlah' => $d['jumlah'][$ct],
                       'harga' => $d['hargaid'][$ct],
                       'diskon'=>$hasil[0],
                       'total_akhir'=> ($d['jumlah'][$ct]*$d['hargaid'][$ct])-$hasil[0]



                       ]
                    );
          $ct++;
        }

        return back();
      }

       public function isipurchaseorder($id){

        
        //      dd(Purchaseorder::find($id)->revisi_po()->get());

        $po = Purchaseorder::where('purchaseorders.id',$id)
        ->leftJoin('listrequesteditpos','listrequesteditpos.purchaseorders_id','=','purchaseorders.id')
        ->leftJoin('suppliers','suppliers.id','=','purchaseorders.customers_id')
        ->leftJoin('periode_mrs','periode_mrs.id','=','purchaseorders.periode_mrs_id')
        ->select('purchaseorders.nopo','suppliers.nama','listrequesteditpos.status','purchaseorders.ongkir','periode_mrs.judul')
        ->whereNull('purchaseorders.deleted_at')->get();

        $data['result']=$po;
        $data['id']=$id;
        $data['page_title'] = "ISI PO";

        $this->cbView('customView.isipurchaseorder',$data);
      }

      public function isipurchaseorderdtajax($id){
         $setuju= DB::table('listrequesteditpos')->where('purchaseorders_id',$id)->get()[0];

         $hasil = Isi_po::leftJoin('barangs','barangs.id','=','isi_pos.barangs_id')
            ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
            ->select('isi_pos.id','barangs.nama','satuans.nama as satuan','isi_pos.jumlah','isi_pos.harga','isi_pos.diskon')
            ->where('isi_pos.purchaseorders_id',$id)
            ->whereNull('isi_pos.deleted_at')->get();
         if($setuju->status == 3){
            $act= "<button class='btn btn-xs btn-success editpo'>Edit PO</button>";
            foreach($hasil as $d){
              
                $jumlah = $d->jumlah;
                $harga = $d->harga;
                if(count($d->revisi->where('status','approve'))>0){
                  $jumlah = $d->revisi->where('status','approve')->last()->jumlah_baru;
                  $harga = $d->revisi->where('status','approve')->last()->harga_baru;
                }
              
              $data[] = [
                     $d->id,
                     $d->nama,
                     $d->satuan,
                     $jumlah,
                     number_format($harga),
                     $d->diskon,
                     number_format(($harga*$jumlah)- $d->diskon),
                     "<button class='btn btn-xs btn-success editpo' id='".$d->id."'>Edit PO</button>"
                     ];


              $urutan++;
            }
         }
         else{
            foreach($hasil as $d){
              
                $jumlah = $d->jumlah;
                $harga = $d->harga;
                if(count($d->revisi->where('status','approve'))>0){
                  $jumlah = $d->revisi->where('status','approve')->last()->jumlah_baru;
                  $harga = $d->revisi->where('status','approve')->last()->harga_baru;
                }
              
               $data[] = [
                     $d->id,
                     $d->nama,
                     $d->satuan,
                     $jumlah,
                     number_format($harga),
                     $d->diskon,
                     number_format(($harga*$jumlah)- $d->diskon),
                     // "<button class='btn btn-xs btn-danger' disabled=''>Belum Bisa Edit PO</button>"
                     ];


              $urutan++;
            }
         }
         $results = [
            "total" =>$total,
            "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data
            ];
         echo json_encode($results);
      }

      public function list_belum_approve() {
         $data = [];
			$data['page_title'] = 'List Pengiriman AAAAAA';

         // $data['lock_rekap'] = DB::table('lock_rekaps')->whereNull('deleted_at')->where('approve', 0)->get();
         $data['mr_subdept'] = DB::table('mr_subdepts')->whereNull('deleted_at')->where('approval', '=', 'oke')
                              ->select('id as mr_subdept_id',
                                       'periode_mrs_id as periode_mr_id',
                                       'nomr as nomor_mr')
                              ->get();

         $data['periode_mr'] = DB::table('periode_mrs')
                        ->leftJoin('lock_rekaps', 'periode_mrs.id', '=', 'lock_rekaps.periode_mrs_id')
                        ->where('lock_rekaps.approve', 0)
                        ->whereNull('lock_rekaps.deleted_at')
                        ->whereNull('periode_mrs.deleted_at')
                        ->select('periode_mrs.id as periode_mr_id',
                                 'periode_mrs.judul as periode_mr_judul',
                                 'lock_rekaps.barangs_id as barang_id',
                                 'lock_rekaps.pricelists_id as pricelist_id',
                                 'lock_rekaps.jumlah as jumlah',
                                 'lock_rekaps.approve as approve')
                        ->get();
         // $d = $data['periode_mr']->groupBy('periode_mr_id');
         // dd($data);
         return view('customView.list_mr_blm_approve', $data);

      }

      public function dt_list_belum_approve() {
         dd("123");
      }

      public function pindahkangudang(){
        $data = DB::table('barangs')->select('akumstok','kode','id')->get();

        foreach ($data as $d) {
          DB::table('barang_gudangs')->insert(
            ['kode' => $d->kode,
             'barangs_id' => $d->id,
             'gudangs_id'=>1,
             'stok' => $d->akumstok
              ]
          );
        }
      }

      public function printtandaterima($id){
        $data['tes'] = mr_subdept::where('id',$id)->whereNull('deleted_at')->get();
        
        return View('customView.printtandaterima',$data);
      }

      public function lihatisipendingmr($id){
        // $data = isi_mr_subdept::where('mr_subdepts_id',$id)->whereNull('deleted_at')->get();
        $data = DB::table('isi_mr_subdepts')
        ->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
        ->select('barangs.nama','isi_mr_subdepts.jumlah','isi_mr_subdepts.dikirim')
        ->where('mr_subdepts_id',$id)->whereNull('isi_mr_subdepts.deleted_at')->get();
        return json_encode($data);
      }

      public function lihatisipendingso($id){
        // $data = isi_mr_subdept::where('mr_subdepts_id',$id)->whereNull('deleted_at')->get();
        $data = DB::table('isi_sales_orders')
        ->leftJoin('barangs','barangs.id','=','isi_sales_orders.barangs_id')
        ->select('barangs.nama','isi_sales_orders.jumlah','isi_sales_orders.dikirim')
        ->where('isi_sales_orders.sales_orders_id',$id)->whereNull('isi_sales_orders.deleted_at')->get();
        return json_encode($data);
      }

      public function pendingmr(){
        $data = Mr_subdept::whereNull('deleted_at')->orderby('periode_mrs_id','asc')->get();
        return view('customView.pendingmr',compact('data'));
      }

      public function pendingso(){
        $data = Sales_order::whereNull('deleted_at')->latest()->get();
        return view('customView.pendingso',['data'=>$data]);
      }

      public function laporanso(){
       return view('customView.laporanso'); 
      }
      public function submitlaporan(Request $request){
        // laporancustomer
        $data = Request::all();
        //2017-12-05
        //dd($data);
        if($data['jenislaporan']=='customer'){
          $hasil = DB::table('customers')->whereNull('deleted_at')->get();
          return view('customView.laporansosatu',compact('hasil','data')); 
        }
        elseif($data['jenislaporan']=='customer_specific'){
          $hasil = DB::table('sales_orders')
          ->leftJoin('customers','sales_orders.customers_id','=','customers.id')
          ->select('customers.nama','customers.id','customers.alamat invoice','customers.group')
          ->whereNull('sales_orders.deleted_at')
          ->whereDate('sales_orders.created_at',' > ',$data['tanggal'])
          ->whereDate('sales_orders.created_at',' < ',$data['tanggalend'])
          ->get();
          
          //$hasil = DB::table('customers')->whereNull('deleted_at')->get();
          return view('customView.laporansosatu',compact('hasil','data')); 
        }
        elseif($data['jenislaporan']=='barang'){
          $hasil = DB::table('barang_gudangs')
          ->leftJoin('barangs','barangs.id','=','barang_gudangs.barangs_id')
          ->select('barangs.nama','barangs.kode')
          ->whereNull('barang_gudangs.deleted_at')
          ->where('barang_gudangs.gudangs_id',6)
          ->get();
        }
        elseif($data['jenislaporan']=='custfile'){
          
        }

      }
      

  }
