<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Sales_order;

	class AdminSalesOrdersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "sales_orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nomor","name"=>"nomor"];
			$this->col[] = ["label"=>"Creator","name"=>"user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Customer","name"=>"customers_id","join"=>"customers,nama"];
			$this->col[] = ["label"=>"Metode","name"=>"metode_bayar"];
			$this->col[] = ["label"=>"Jatuh Tempo","name"=>"jatuh_tempo"];
			$this->col[] = ["label"=>"Diskon","name"=>"diskon"];
			$this->col[] = ["label"=>"Total","name"=>"total"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Rekening","name"=>"banks_id","join"=>"banks,norek"];
			$this->col[] = ["label"=>"Approval","name"=>"approval"];
			$this->col[] = ["label"=>"PPN (%)","name"=>"ppn"];
			$this->col[] = ["label"=>"Created At","name"=>"created_at"];
			$this->col[] = ["label"=>"Nofaktur","name"=>"nofaktur"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Metode Bayar','name'=>'metode_bayar','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'Tunai;Tempo'];
			$this->form[] = ['label'=>'Jatuh Tempo','name'=>'jatuh_tempo','type'=>'date','validation'=>'date','width'=>'col-sm-10','help'=>'Diisi jika metode bayar = Tempo'];
			$this->form[] = ['label'=>'Diskon','name'=>'diskon','type'=>'text','validation'=>'required|min:0','width'=>'col-sm-10','help'=>'Nilai diskon (dalam rupiah)','placeholder'=>'0'];
			$this->form[] = ['label'=>'Total','name'=>'total','type'=>'hidden','validation'=>'required|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Customer','name'=>'customers_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'customers,nama'];
			$this->form[] = ['label'=>'Alamat','name'=>'alamats_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'alamats,alamat'];
			$this->form[] = ['label'=>'PPN','name'=>'ppn','type'=>'number','validation'=>'required|min:0','width'=>'col-sm-10','help'=>'nilai persen (%)','placeholder'=>'0'];
			$this->form[] = ['label'=>'Nofaktur','name'=>'nofaktur','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Metode Bayar','name'=>'metode_bayar','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'Tunai;Tempo'];
			//$this->form[] = ['label'=>'Jatuh Tempo','name'=>'jatuh_tempo','type'=>'date','validation'=>'date','width'=>'col-sm-10','help'=>'Diisi jika metode bayar = Tempo'];
			//$this->form[] = ['label'=>'Diskon','name'=>'diskon','type'=>'text','validation'=>'required|min:0','width'=>'col-sm-10','help'=>'Nilai diskon (dalam rupiah)','placeholder'=>'0'];
			//$this->form[] = ['label'=>'Total','name'=>'total','type'=>'hidden','validation'=>'required|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Customer','name'=>'customers_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'customers,nama'];
			//$this->form[] = ['label'=>'Alamat','name'=>'alamats_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'alamats,alamat'];
			//$this->form[] = ['label'=>'PPN','name'=>'ppn','type'=>'number','validation'=>'required|min:0','width'=>'col-sm-10','help'=>'nilai persen (%)','placeholder'=>'0'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
	        $this->sub_module[] = ['label'=>'','path'=>'isi_sales_orders','parent_columns'=>'nomor,user_id,total,metode_bayar,status','foreign_key'=>'sales_orders_id','button_color'=>'success','button_icon'=>'fa fa-bars'];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'Approve', 'url'=>CRUDBooster::mainpath('approve/[id]'),'icon'=>'fa fa-check','color'=>'primary','showIf'=>'[status] == "new"','confirmation' => true];
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-money btnbayar','color'=>'primary','url'=>CRUDBooster::mainpath('timelinebayar/[id]')];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();
	        $this->button_selected[] = ['label'=>'Approve','icon'=>'fa fa-check-square-o','name'=>'bulkapprove'];
	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
	        $(document).ready( function() {
			$(function() {
	            var table = $('#table_dashboard').DataTable({
	            	bPaginate: false,
	            	bInfo: false,
	            	searching: false,

	            });
	            var tablesum = table.column(7).data().sum();
	            $('#table_dashboard tfoot').append('<tr><th colspan=\"7\" class=\"text-right\">Total</th><th>'+tablesum.toLocaleString()+'</th></tr>');
	        });
			});";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = '';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	           if($button_name=='bulkapprove'){
	           	DB::table('sales_orders')->whereIn('id',$id_selected)->update(['approval'=>'approved','status'=>'approved','updated_at'=>now()]);
	           }
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	if($column_index==6 || $column_index==7){
	    		$column_value = number_format($column_value);
	    	}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
	    	$postdata['user_id'] = CRUDBooster::myId();
	    	$postdata['status']= 'new';
	    	// $postdata->approval =  
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	        $so = Sales_order::find($id);
	        if($so->customer->jenis_harga_jual=='dua_berlian') $jenis_harga_jual = 'DB';
	    	if($so->customer->jenis_harga_jual=='gemilang') $jenis_harga_jual = 'GEMILANG';
	    	if($so->customer->jenis_harga_jual=='online_shop') $jenis_harga_jual = 'ONLINESHOP';
	    	$nomor = 'SO-'.date('ym').'-'.$so->id.'-'.$jenis_harga_jual;
	    	$so->update(['nomor'=>$nomor]);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	        $so = Sales_order::find($id);
	        // dd(is_numeric($postdata['diskon']));
	        $total = $so->total + $so->diskon;
	        if(is_numeric($postdata['diskon'])){
	        	$tampung = $postdata['diskon'];
	        	$total2 = $total - $tampung;
	        	$postdata['total'] = $total2;
	        }
	        else{
	        	$gg = explode('%',$postdata['diskon']);
	        	if(is_numeric($gg[0])){
	        		$diskonsku = $gg[0] * $total /100;
	        		$total2 = $total -  $diskonsku;
	        		$postdata['diskon'] = $diskonsku;
	        		$postdata['total'] = $total2;
	        	}
	        	else{
	        		return back()->with(['message'=>'Format Diskon Tidak sesuai','message_type'=>'warning'])->send();
					exit;

	        	}
	        }
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function getpendingapprove(){
	    	$data = [];
	    	$data['page_title'] = 'Approval SO';
	    	$this->cbView('customView.soapproval',$data);
	    }

	    public function dt_pending(){
	    	$hasil = DB::table('sales_orders')->leftJoin('cms_users','cms_users.id','=','sales_orders.user_id')
	    						->where('sales_orders.status','new')
	    						->select('cms_users.name','sales_orders.*')
	    						->orderBy('sales_orders.created_at','desc')
	    						->get()
	    						;
	    						// dd($hasil);
			$i=1;
			$data = [];
			foreach($hasil as $d){
	     		$data[] = [
	     				$d->id,
	     				$d->nomor,
		     			$d->name,
		     			$d->metode_bayar,
	     				$d->jauh_tempo,
	     				$d->diskon,
	     				$d->total,
	     				$d->status,
	     				$d->approval,
	     				'<a type="button" class="btn btn-success btn-xs pull-right"><i class="fa fa-bars"></i></a>
	     				<a class="btn btn-xs btn-primary btn-detail pull-right" href="http://localhost/wellhos/public/admin/sales_orders/detail/'.$d->id.'"><i class="fa fa-eye"></i></a>',
	     		 		];
	     		$i++;
	    	 }
	    	// dd($data);
	    	$results = [
	    				"draw" => 0,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	return response()->json($results);
	    }

	    public function getapprove($id){
	    	DB::table('sales_orders')->where('id',$id)->update(['approval'=>'approved','status'=>'approved','updated_at'=>now()]);
	    	CRUDBooster::redirect(CRUDBooster::adminPath('sales_orders'),'Berhasil Memberi Aprooval','success');
	    }

	    public function bulkapprove(){
	    	$request = request();
	    	dd($request);
	    }

	    public function gettimelinebayar($id){
    		$data['page_title']= 'Timeline Bayar SO';
    		$data['hasil'] = DB::table("bayar_sos")
    					->leftJoin('isi_bayar_sos','bayar_sos_id','=','bayar_sos.id')
    					->leftJoin('coas','bayar_sos.coa_id','=','coas.id')
    					->leftJoin('cms_users','bayar_sos.user_id','=','cms_users.id')
    					->where('bayar_sos.so_id','=',$id)
    					->select('bayar_sos.tanggal_bayar','bayar_sos.status','cms_users.name as user','coas.nama as coa','coas.kode as coa_kode','isi_bayar_sos.nominal','isi_bayar_sos.keterangan','bayar_sos.created_at')
    					->orderBy('tanggal_bayar','desc')
    					->get();
    					;

    		$this->cbView('customView.timeline_bayar_so',$data);
    	}
	}