<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminCustomers56Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "customers";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"No","name"=>"nocustomer"];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"Invoice","name"=>"alamat invoice"];
			$this->col[] = ["label"=>"NPWP","name"=>"npwp"];
			$this->col[] = ["label"=>"Telp","name"=>"notelp"];
			// $this->col[] = ["label"=>"Jenis","name"=>"jenis"];
			$this->col[] = ["label"=>"Pemilik","name"=>"namapemilik"];
			$this->col[] = ["label"=>"Alamat","name"=>"alamatpemilik"];
			$this->col[] = ["label"=>"KTP","name"=>"noktp"];
			$this->col[] = ["label"=>"Tipe","name"=>"tipe"];
			$this->col[] = ["label"=>"Jenis","name"=>"jenis_harga_jual"];
			$this->col[] = ["label"=>"User Id","name"=>"user_id","join"=>"cms_users,name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Alamat Invoice','name'=>'alamat invoice','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'NPWP','name'=>'npwp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Telp','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			// $this->form[] = ['label'=>'Jenis','name'=>'jenis','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Nama Pemilik','name'=>'namapemilik','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Alamat Pemilik','name'=>'alamatpemilik','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'No KTP','name'=>'noktp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Tipe','name'=>'tipe','type'=>'text','validation'=>'required','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Jenis Harga_jual','name'=>'jenis_harga_jual','type'=>'select2','validation'=>'required','width'=>'col-sm-9','dataenum'=>'dua_berlian|Dua Berlian;gemilang|Gemilang;online_shop|Online Shop;gemilang01|Gemilang01;dua_berlian01|Dua Berlian01'];
			$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Group','name'=>'group','type'=>'text','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Alamat Invoice','name'=>'alamat invoice','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'NPWP','name'=>'npwp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Telp','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Jenis','name'=>'jenis','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Nama Pemilik','name'=>'namapemilik','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Alamat Pemilik','name'=>'alamatpemilik','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'No KTP','name'=>'noktp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Tipe','name'=>'tipe','type'=>'text','validation'=>'required','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Jenis Harga_jual','name'=>'jenis_harga_jual','type'=>'select2','validation'=>'required','width'=>'col-sm-9','dataenum'=>'dua_berlian|Dua Berlian;gemilang|Gemilang;onlne_shop|Online Shop'];
			//$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'cms_users,name'];
			# OLD END FORM

			$this->sub_module = array();

	      $this->addaction = array();
	      $this->addaction[] = ['label'=>'','icon'=>'fa fa-money','color'=>'primary','url'=>'detailrek/[id]','class'=>'testes'];
	      $this->addaction[] = ['label'=>'','icon'=>'fa fa-book','color'=>'primary','url'=>'detailalamat/[id]','class'=>'wadwa','id'=>'dwadwa'];

	      $this->button_selected = array();
	      $this->alert = array();
	      $this->index_button = array();
	      $this->table_row_color = array();
	      $this->index_statistic = array();
	      $this->script_js = NULL;
	      $this->pre_index_html = null;
	      $this->post_index_html = null;
	      $this->load_js = array();
	      $this->style_css = NULL;
	      $this->load_css = array();
	    }

	    public function detailrek($id){
	    	$data = DB::table('banks')->where('customers_id',$id)->whereNull('deleted_at')->get();
	    	return View('customView.detailrekcustomer',compact('data','id'));
	    }
	    public function submitrek(){

	    	$data = Request::all();
	    	DB::table('banks')->insert(
	    	    array('customers_id' => $data['customers_id'],
	    	          'bank' => $data['bank'],
	    	          'norek' => $data['norek'],
	    	          'atasnama' => $data['atasnama']
	    	          )
	    	);
	    	$id = $data['customers_id'];
	    	$data = DB::table('banks')->where('customers_id',$data['customers_id'])->whereNull('deleted_at')->get();
	    	return View('customView.detailrekcustomer',compact('data','id'));
	    }
	    public function actionButtonSelected($id_selected,$button_name) {
	    }


	    public function detailalamat($id){
	    	$data = DB::table('alamats')->where('customers_id',$id)->whereNull('deleted_at')->get();
	    	return View('customView.detailalamatcustomer',compact('data','id'));
	    }
	    public function submitalamat(){

	    	$data = Request::all();
	    	DB::table('alamats')->insert(
	    	    array('customers_id' => $data['customers_id'],
	    	          'alamat' => $data['alamat'],

	    	          )
	    	);
	    	$id = $data['customers_id'];
	    	$data = DB::table('banks')->where('customers_id',$data['customers_id'])->whereNull('deleted_at')->get();
	    	return redirect('admin/detailalamat/'.$id);
	    }

	    public function hook_query_index(&$query) {}
	    public function hook_row_index($column_index,&$column_value) {
	    }
	    public function hook_before_add(&$postdata) {
	    }

	    public function hook_after_add($id) {
			  $no = "c-".$id;
			  DB::table('customers')->where('id', $id)->update(['nocustomer' => $no]);
	    }

	    public function hook_before_edit(&$postdata,$id) {
	    }
	    public function hook_after_edit($id) {
	    }
	    public function hook_before_delete($id) {
	    }
	    public function hook_after_delete($id) {
	    }
	}