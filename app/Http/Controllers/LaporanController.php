<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App\Models\Barang;
use App\Models\Periode_mrs;
use App\Models\Isispbm;
use App\Models\Isi_po;
use App\Models\Isi_sales_order;
use App\Models\Purchaseorder;
use App\Models\Supplier;
use App\Models\Mr_subdept;
use App\Models\Barang_gudang;
use App\Models\Isi_stokopnam;
use App\Models\Wellhos_department;
use App\Models\Wellhos_subdept;
use App\Models\Gudang;
use \DateTime;

class LaporanController extends \crocodicstudio\crudbooster\controllers\CBController {

	private $gudangId = 1;
	private $bisaRetur = 'approved';

	public function getMrIndex(){
		$request = request();
		$periode = Periode_mrs::leftJoin('mr_subdepts','mr_subdepts.periode_mrs_id','=','periode_mrs.id')
								->select('periode_mrs.id','periode_mrs.judul'
										,DB::raw('SUM(mr_subdepts.total) as total')
								)
								->groupBy('id','judul','periode_mrs.tglmulai')
								// ->whereYear('mr_subdepts.tanggal','>=','')
								->orderBy('periode_mrs.tglmulai')
								->get()
								;
								// dd($periode);
		$labelChart = [];
		$dataChart = [];
		foreach($periode as $key=>$value){
			$labelChart[]=$value->judul;
			if ($value->total>0) {
				$dataChart[] = $value->total;
			}
			else{
				$dataChart[] = 0;
			}
		}
		$labelChart = json_encode($labelChart);
		$dataChart = json_encode($dataChart);
		// dd($labelChart);
		return view('customView.dashboardlaporanmr',['labelChart'=>$labelChart,'dataChart'=>$dataChart]);
	}

	public function chartPeriode(){
		return 'git gut';
	}

	public function filterPeriode(){
		$request = request();
		// return response()->json($request);
		$periode = Periode_mrs::leftJoin('mr_subdepts','mr_subdepts.periode_mrs_id','=','periode_mrs.id')
								->select('periode_mrs.id','periode_mrs.judul'
										,DB::raw('SUM(mr_subdepts.total) as total')
								)
								->groupBy('id','judul','periode_mrs.tglmulai')
								;
								// dd($periode);
		if($request->opsi=='bln'){
			$tglmulai = $request->tglmulai.'-01';
			$tglselesai = $request->tglselesai.'-'.date('t',strtotime($tglselesai));
			// dd($tglselesai);
			$periode = $periode->whereDate('mr_subdepts.tanggal','>=',$tglmulai)
								->whereDate('mr_subdepts.tanggal','<=',$tglselesai)
								;
		}
		else{
			$periode = $periode->whereYear('mr_subdepts.tanggal','>=',$request->tglmulai)
								->whereYear('mr_subdepts.tanggal','<=',$request->tglselesai)
								;
		}
		$periode = $periode->orderBy('periode_mrs.tglmulai')
							->get()
							;
		// dd($periode);
		$labelChart = [];
		$dataChart = [];
		foreach($periode as $key=>$value){
			$labelChart[]=$value->judul;
			$dataChart[] = $value->total;
		}
		// $labelChart = json_encode($labelChart);
		// $dataChart = json_encode($dataChart);
		return response()->json(['labelChart'=>$labelChart,'dataChart'=>$dataChart,'jumlahPeriode'=>count($periode)]);
	}

	public function barangPopuler(){
		$request = request();
		$hasil = DB::table('barangs')
    				->leftjoin('isi_mr_subdepts','isi_mr_subdepts.barangs_id','=','barangs.id')
    				->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
    				->select('barangs.nama as barang'
    					,'barangs.kode'
    					,DB::raw('SUM(isi_mr_subdepts.jumlah) as jumlah')
    				)
    				->whereNull('barangs.deleted_at')
    				->whereDate('mr_subdepts.tanggal','>=',date('Y-m-d', strtotime(date('Y-m-d'). "-3 months")))
    				;


    	if(isset($request->periodes)){
    		$hasil = $hasil->whereIn('mr_subdepts.periode_mrs_id',$periodes);
    	}
    	if(isset($request->slowmoving)){
    		$hasil = $hasil->orderBy('jumlah','asc');
    	}
    	else{ //fastmoving
    		$hasil = $hasil->orderBy('jumlah','desc')
    						// ->addSelect('barangs.kode')
    						; //limit 1 ambil last
    		if(!isset($request->fastmoving)){
    			$hasil = $hasil->take(10);
    		}
    	}
    	$hasil = $hasil->groupBy('barang'
    							,'kode'
    							)
    				->get()
    				;

    	// dd($hasil);
    	$data =[];
    	foreach ($hasil as $key => $d) {
    		$data[] = [
    					$d->kode,
						$d->barang,
						$d->jumlah,
					];
    	}

    	$results = [
            "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data
        ];
        echo json_encode($results);
	}

	public function getbarangmasuk(){
		return view('customView.getbarangmasuk');
	}

	public function getbarangkeluar(){
		return view('customView.getbarangkeluar');
	}

	public function dtbarangmasuk(){
		$request = request();
		$isispbm = Isispbm::whereNull('deleted_at');
		if(isset($request->tanggal)){
			$tanggal = explode('-', $request->tanggal);
			// dd($tanggal);
			$isispbm = $isispbm->whereMonth('created_at','=',$tanggal[1])->whereYear('created_at','=',$tanggal[0]);
		}
		$isispbm = $isispbm->orderBy('created_at','desc')->orderBy('barangdatangs_id','asc')->get();
		$data = [];
		foreach ($isispbm as $key => $d) {
			$jumlah = $d->Barangs->avgprice*$d->jumlah;
			$ppn = $jumlah * 10/100;

			$data[] = [
						'', //no urut data 0
						$d->barangdatang->tanggaldatang, //tgl faktur 1
						$d->barangdatang->Purchaseorder->hari, //top 2
						date('Y-m-d', strtotime($d->barangdatang->Purchaseorder->tglpo. ' + '.$d->barangdatang->Purchaseorder->hari.' days')), //jatuh tempo 3
						$d->barangdatang->nospbm, // SPBM 4
						$d->barangdatang->nospbm, //noDO 5
						$d->barangdatang->Purchaseorder->supplier->nama, // supplier 6
						$d->barangdatang->Purchaseorder->supplier->npwp, // supplier npwp 7
						$d->barangdatang->Purchaseorder->supplier->alamat, //supplier alamat 8
						'alamat2', //alamat2 9
						$d->Barangs->nama, //barang 10
						$d->Barangs->kode, //kode barang 11
						$d->jumlah, //qty 12
						$d->Barangs->satuan->nama, //uom 13
						number_format($d->Barangs->avgprice), //harga satuan 14
						number_format($jumlah), //jumlah 15
						'', //disc 16
						'', //nilai disc 17
						number_format($jumlah),//netto 18
						number_format($ppn), //ppn 19
						number_format($jumlah + $ppn), //total 20
						'', //bank mandiri 21
						'', //keterangan 22
						'', //23
						];
		}
		$results = [
    				"draw" => 0,
    	        	"recordsTotal" => count($data),
    	        	"recordsFiltered" => count($data),
    	        	"data" => $data
    	        	];
		return response()->json($results);
	}

	public function dtbarangkeluar(){
		$request = request();
		$isipo = Isi_sales_order::whereNull('deleted_at');
		if(isset($request->tanggal)){
			$tanggal = explode('-', $request->tanggal);
			// dd($tanggal);
			$isipo= $isipo->whereMonth('created_at','=',$tanggal[1])->whereYear('created_at','=',$tanggal[0]);
		}
		$isipo = $isipo->orderBy('created_at','desc')->orderBy('sales_orders_id','asc')->get();
		//isi po == isi so; maaf salah ketik sudah lelah :(
		$data = [];

		foreach ($isipo as $key => $d) {
			if(isset($d->sales_order->customer) &&isset($d->sales_order->customer->alamat()->latest()->first()->alamat)){
				$alamat = $d->sales_order->customer->alamat()->latest()->first()->alamat;
			}
			else{
				$alamat = '';
			}
			$ppn = $d->total*10/100;//ppn
			if($d->total>0){
				$persendiskon = number_format(($d->diskon / $d->total *100),2);//persen diskon
				$persendiskon = $persendiskon.' %';
			}
			else
				$persendiskon = '';
			$data[]= ['', //0 urutan
					date('Y-m-d',strtotime($d->sales_order->created_at)), //1 tgl faktur
					$d->sales_order->customer->top, //2 top
					$d->sales_order->jatuh_tempo, //3 jatuh tempo
					$d->sales_order->nomor, //4 NO SO
					$d->sales_order->customer->nama, //6 nama CUSTOMER
					$d->sales_order->customer->npwp, //7 CUSTOMER NPWP
					$alamat, //8 CUSTOMER alamata 1
					'alamat2', //9 CUSTOMER alamata 2
					$d->barang->nama, //10 barang nama
					$d->barang->kode, //11 barang kode
					$d->jumlah, //12 qty
					$d->barang->satuan->nama, //13 uom
					number_format($d->total/$d->jumlah), //14 harga satuan
					number_format($d->total+$d->diskon), //15 jumlah
					$persendiskon,//16 DISC
					number_format($d->diskon), //17 nilai disc
					number_format($d->total), //18 netto
					number_format($ppn),//19 ppn
					number_format($d->total+$ppn), //20 total
					'', //21 bank mandiri
					'', //22 keterangan
					'', //23 action
					];
		}

		$results = [
    				"draw" => 0,
    	        	"recordsTotal" => count($data),
    	        	"recordsFiltered" => count($data),
    	        	"data" => $data
    	        	];
		return response()->json($results);
	}

	public function getpo(){
		return view('customView.getlaporanpo');
	}

	public function getpodt(){

		$request = request();
		$idperiode = $request->periode;
		$jenispo = $request->jenis;
		$tipepo = $request->tipepo; //0 semua; 1 MR; 2 pembilan
		// $isi = Isi_po::whereHas('purchaseorder',function($q) use ($idperiode,$jenispo){
		// 				$q->where('periode_mrs_id',$idperiode)
		// 				 ->where('jenispo',$jenispo);
		// 				})
		// 				->orderBy('purchaseorders.nopo')
		// 				->get()
		// 				;
		$po = Purchaseorder::where('periode_mrs_id',$idperiode);
		if($request->jenis>0){
			$po = $po->where('jenispo',$jenispo);
		}
		if($tipepo ==1){
			$po = $po->where('periode_mrs_id','>',0);
		}
		else if($tipepo ==2){
			$po = $po->where('periode_mrs_id','=',0);
		}

		$po = $po->orderBy('purchaseorders.nopo')->get();
		$data = [];
		foreach ($po as $key => $p) {
			$isi = $p->isi;
			foreach ($isi as $key => $v) {
				if(isset($v->revisi->where('status','approve')->harga_baru)){// kalau ada revisi
					$revisi = $v->revisi->where('status','approve')->last();
					$costunit = $revisi->harga_baru;
					$jmlh = $revisi->jumlah_baru;
					$dpp = 0; //field belum ada
					$disc = 0; //field belum ada
				}
				else{
					$costunit = $v->harga;
					$jmlh = $v->jumlah;
					$dpp = 0; //field belum ada
					$disc = $v->diskon; 
				}
				$ppn = $jmlh*$p->ppn/100;
				$subtotal = $jmlh*$costunit+$ppn-$disc;
				$data[]= ['', //0 urutan
						$v->barang->kode, //1 kodebarang
						$v->barang->nama, //2 namabarang
						$v->barang->satuan->nama, //3 satuanbarang
						number_format($costunit), //4 harga satuan
						number_format($jmlh), //5 jmlhbarang
						number_format($dpp), //6 dp
						number_format($disc), //7 disc isipo
						number_format($ppn), //8 ppn
						number_format($subtotal), //9 total isipo
						$p->tglpo, //10 tgpo
						$p->nopo, //11 nopo
						$p->supplier->kode, //12 kodesupplier
						$p->supplier->nama, //13 namasupplier
						number_format($p->total_akhir), //14 totalpo
						];
			}
		}
		$results = [
    				"draw" => 0,
    	        	"recordsTotal" => count($data),
    	        	"recordsFiltered" => count($data),
    	        	"data" => $data
    	        	];
		return response()->json($results);
	}

	public function getperiode(){
		$request = request();
		$data = DB::table('periode_mrs')->whereNull('deleted_at');
		if(isset($request->q)){
			$data = $data->where('judul','like','%'.$request->q.'%');
		}
		$data = $data->select('id','judul as text')->latest()->get();
		return response()->json($data);
	}

	public function getlaporanadmindashboard(){
		$data = [];
		// $periodes = DB::table('periode_mrs')
		// 				->whereNull('deleted_at')
		// 				->whereDate('tglselesai','>=',date('Y-m-d'))
  //                       ->whereDate('tglselesai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))
  //                       ->orWhere(function($query){
  //                         $query->where('tglmulai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))->where('tglmulai','>=',date('Y-m-d'))->where('selesai','<>',1);
  //                       })
  //                       ->orderBy('tglmulai','asc')
  //                       ->get()
  //                       ;
  //                       dd($periodes);
        $periodes = DB::table('periode_mrs')
        				->whereNull('deleted_at')
        				->select('id','judul as text' ,'tglmulai')
        				->get();
        $pnow = $periodes[0]->id;
        foreach ($periodes as $key => $value) {
        	if(date('m',strtotime($value->tglmulai)) == date('m') &&  date('Y',strtotime($value->tglmulai)) == date('Y')){
        		$pnow = $value->id;
        		// dd($pnow);
        		break;
        	}
        }
        $periodes = json_encode($periodes);
        // dd($periodes);
		return view('customView.laporanadmin1',['data'=>$data,
												'periodes'=>$periodes,
												'pnow' =>$pnow
											]);
	}

	public function dashboardadmingetpo(){ //ajax getpo
		// edit tampilan nama supplier sehinnga 15 text lalu cari spasi berikut tambahkan linebreak
		$request =request();
		$data = [];
		$label = [];
		$backgroundColor= [];
		$paletku= [];
		$konterku=1;
		$persen = 0;
		if(!isset($request->periode)){ //default
			$periodes = DB::table('periode_mrs')
						->whereNull('deleted_at')
						->whereDate('tglselesai','>=',date('Y-m-d'))
                        ->whereDate('tglselesai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))
                        ->orWhere(function($query){
                          $query->where('tglmulai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))->where('tglmulai','>=',date('Y-m-d'))->where('selesai','<>',1);
                        })
                        ->get()
                        ;
        }
        else{
        	$periodes = DB::table('periode_mrs')->whereIn('id',$request->periode)->get();
        }
                        // dd($periodes);
        foreach ($periodes as $key => $value) {
        	$paletku[$value->id] = $this->randomwarna();
        }
        // dd($paletku);
		$po = Purchaseorder::whereIn('periode_mrs_id',$periodes->pluck('id'))
						->whereNull('deleted_at')
						->orderBy('tglpo')
						->get()
						;
		foreach ($po as $k => $p) {
			$persen = 0;
			$totalbarang = 0;
			$spbm = 0;
			if(count($p->isi)==0){ // PO tanpa ada isi
				$persen = 0;
			}
			else{
				foreach ($p->isi as $k2 => $isi) { //jumlah barang diPO
					if(count($isi->revisi->where('status','approve'))>0){//isi_pos memiliki revisi
						$totalbarang += $isi->revisi->where('status','approve')->last()->jumlah_baru;
					}
					else{
						$totalbarang += $isi->jumlah;
					}
				}
				// dd($p->isispbm);
				foreach ($p->isispbm as $key => $s) {
					$spbm += $s->jumlah;
				}
				// dd($spbm.'/'.$totalbarang);
				$persen = $spbm/$totalbarang *100;
			}
			/*if($konterku%2==0){ //even
				$backgroundColor[] = "#00bfff";
			}
			else{ //odd
				$backgroundColor[] = "#4169e1";
			}*/
			if($persen>=100){
				continue;
			}
			else{
				if(isset($request->procurement)) {
					$label[] = ['nopo'=>$p->nopo,
								'supplier'=>$p->supplier->nama,
								'persen'=>$persen,
								'label'=>number_format($persen,2).' %'];
				}
				else {
					$data[] = number_format($persen,2);
					$label[] = $p->nopo. ' | '.$p->supplier->nama;
					$backgroundColor[] = $paletku[$p->periode_mrs_id];
					$konterku ++;
				}
			}
		}
		// dd($data);
		if(isset($request->procurement)) {
			$data = [
    				"draw" => 0,
    	        	"recordsTotal" => count($label),
    	        	"recordsFiltered" => count($label),
    	        	"data" => $label
    	        	];
			return response()->json($data);
		}
		else {
			return response()->json(['data'=>$data,'labels'=>$label,'colors'=>$backgroundColor]);
		}
	}

	private function randomwarna(){
		return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
	}

	public function piepoperiode(){// if needed

	}

	public function dashboardadmingetmr(){
		$request = request();
		$data = [];
		$label = [];
		$request =request();
		$data = [];
		$label = [];
		$backgroundColor= [];
		$konterku=1;
		if(!isset($request->periode)){ //default
			$periodes = DB::table('periode_mrs')
						->whereNull('deleted_at')
						->whereDate('tglselesai','>=',date('Y-m-d'))
                        ->whereDate('tglselesai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))
                        ->orWhere(function($query){
                          $query->where('tglmulai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))->where('tglmulai','>=',date('Y-m-d'))->where('selesai','<>',1);
                        })
                        ->get()
                        ;
        }    
        else{
        	$periodes = DB::table('periode_mrs')->whereIn('id',$request->periode)->get();
        }
                        // dd($periodes);
        foreach ($periodes as $key => $value) {
        	$paletku[$value->id] = $this->randomwarna();
        }
        // dd($paletku);
		$mr = Mr_subdept::whereIn('periode_mrs_id',$periodes->pluck('id'))
						->whereNull('deleted_at')
						->orderBy('tanggal')
						->get()
						;
		foreach ($mr as $k => $p) {
			$persen = 0;
			$totalbarang = 0;
			$kirim = 0;
			if(count($p->isi_mr)==0){ // mr tanpa ada isi
				$persen = 0;
			}
			else{
				foreach ($p->isi_mr as $k2 => $isi) { //jumlah barang diMR
					$totalbarang += $isi->jumlah;
				}
				foreach ($p->detail_pengiriman_barang as $key => $s) {
					$kirim += $s->jumlah;
				}
				$persen = $kirim/$totalbarang *100;
			}
			/*if($konterku%2==0){ //even
				$backgroundColor[] = "#00bfff";
			}
			else{ //odd
				$backgroundColor[] = "#4169e1";
			}*/
			if($persen==100){
				continue;
			}
			else{
				$data[] = number_format($persen,2);
				$label[] = $p->nomr. ' | '.$p->project->area;
				$backgroundColor[] = $paletku[$p->periode_mrs_id];
				$konterku ++;
			}
		}

		return response()->json(['data'=>$data,'labels'=>$label,'colors'=>$backgroundColor]);
	}

	public function dashboardadmingetnilaistok(){ //pergudang
		$request = request();
		$data = [];
		$datasets =[];
		$label = [];
		$backgroundColor = [];
		//id gudang dipakai 1==gemilang ?, 5 ==cacat ,6==johnson
		// $request->gudang; // idgudang
		//$request->durasi => ambil brp bulan ke belakang
		if(!isset($request->gudang)){
			return response()->json('masukkan input id gudang');
		}
		if(!isset($request->durasi)){//default
			$durasi = 6;
		}
		else{
			$durasi = $request->durasi;
		}
		$gudangs = $request->gudang;

		$nilaiakhir = Barang_gudang::leftJoin('gudangs','gudangs.id','=','barang_gudangs.gudangs_id')
									->leftJoin('barangs','barangs.id','=','barang_gudangs.barangs_id')
									->whereIn('barang_gudangs.gudangs_id',$gudangs)
									->whereNull('barang_gudangs.deleted_at')
									->select('gudangs.nama as gudangs'
										,DB::raw('SUM(barang_gudangs.stok*barangs.avgprice) as nilai'))
									->groupBy('gudangs')
									->get()
									// ->toArray()
									; 
									// dd($nilaiakhir);
		
		// dd($durasi);
									//QUERY nilaiakhir butuh pengechekan nilai manual 
									//dates > dates-6month (sesuasi durasi)
		$nilaidurasi = Isi_stokopnam::leftJoin('gudangs','gudangs.id','=','isi_stokopnams.gudangs_id')
								->leftJoin('stokopnams','stokopnams.id','=','isi_stokopnams.stokopnams_id')
								->whereIn('isi_stokopnams.gudangs_id',$gudangs)
								->whereNull('gudangs.deleted_at')
								->whereDate('stokopnams.tanggal','>=',date('Y-m-d', strtotime(date('Y-m-d'). ' - '.$durasi.' months')))
								->select('gudangs.nama as gudangs', 'stokopnams.tanggal'
									,DB::raw('SUM(isi_stokopnams.stok*isi_stokopnams.avgprice) as nilai')
								)
								->groupBy('gudangs','stokopnams.tanggal')
								->orderBy('stokopnams.tanggal','asc')
								->orderBy('gudangs')
								->get()
								// ->toSql()
								->toArray();
								;
								// dd($nilaidurasi);
		$tmp = '';
		$ng = [];
		$i=0;
		foreach ($nilaiakhir as $key => $value) { //menambahkan nilai akhir ke nilai durasi
			$nilaiakhir[$key]->tanggal = 'Sekarang';
			$nilaidurasi[] = $value->toArray();
		}
		// dd($nilaidurasi);
		foreach ($nilaidurasi as $key => $value) {
			if($tmp!= $value['tanggal']){// bikin dataset baru
				if($value['tanggal']!='Sekarang'){
					$label[] = date('Y-M-d',strtotime($value['tanggal']));
				}
				else{
					$label[] = $value['tanggal'];
				}
				$tmp = $value['tanggal'];
			}
			$k = array_search($value['gudangs'],$ng);
			// dd($k);
			if($k=== false){
				$ng[] =  $value['gudangs'];
				$k = array_search($value['gudangs'],$ng);
				// $datasets[$k] = ['label'=>$ng[$k]];
			}
			$data[$k][] = $value['nilai'];
		}
		// dd($data);
		$nilaitotal = [] ;
		foreach ($ng as $key => $value) {
			$color = $this->randomwarna();
			$datasets[$key] =['label'=>$value,
							'data'=>$data[$key],
							'backgroundColor'=> $color,
							];
		}
		// dd($data);
		foreach ($data as $k => $d) {
			foreach ($d as $k2 => $d2) {
				$nilaitotal[$k2] += $d2;
			}
		}
		// dd($nilaitotal);
		$colorzxc =$this->randomwarna();
		$datasets[] = ['label'=>'Total',
						'data'=>$nilaitotal,
						'borderColor'=>$colorzxc,
						'backgroundColor'=>$colorzxc,
						'type'=>'line',
						'borderWidth'=> 2,
		                'fill'=> false,
		            	];
		// dd($datasets);
		//TODO : set label dan color lalu kirim
		//dataset = [label,data,color]

		return response()->json(['datasets'=>$datasets,'labels'=>$label]);
	}

	public function dashboardadmingetpengeluaranbarang(){ // pengiriman barang / gudang filter PO dan MR apakah PO ? atau SO
		$request = request();
		$periode = $request->periode;

		// $dataku = DB::table('detail_pengambilan_barangs')
		// 			->leftJoin('pengambilan_barangs','pengambilan_barangs.id','=','detail_pengambilan_barangs.pengambilan_barang_id')
		// 			->leftJoin('pengiriman_barangs','pengiriman_barangs.id','=','pengambilan_barangs.pengiriman_barang_id')
		// 			->leftJoin('gudangs','gudangs.id','=','pengambilan_barangs.gudang_id')
		// 			// ->whereNull('detail_pengiriman_barangs.deleted_at')
		// 			->where('pengiriman_barangs.periode_mrs_id',$periode)
		// 			->whereNull('gudangs.deleted_at')
		// 			->whereNull('pengiriman_barangs.deleted_at')
		// 			->whereNull('pengambilan_barangs.deleted_at')
		// 			->select('gudangs.nama as gudang'
		// 				,DB::raw('SUM(detail_pengambilan_barangs.jumlah) as jumlah'))
		// 			->groupBy('gudang')
		// 			->get();
		// 			;
		// 			dd($dataku);
		// dd($periode);
		$hasil = Gudang::whereIn('id',[1,5])->get();
		$labels = [];
		$data = [];
		$datasets = [];
		$color = [];
		$c = $this->randomwarna();
		$totalku = [];
		foreach ($hasil as $key => $v) { //v = gudang
			$labels[] = $v->nama;
			$unch = $v->detail_pengambilan_barang()
						// ->
						->whereMonth('detail_pengambilan_barangs.created_at','=',date('m'))
						->whereYear('detail_pengambilan_barangs.created_at','=',date('Y'))
						->get()
						->sum('jumlah')
						;
			$data[] = $unch;
			$color[] = $c;
			$totalku[0] += $unch;
		}
		$data[] = $totalku[0];
		$labels[] = 'Total';
		$color[] = $c;

		$datasets[] = ['data'=>$data,
						'backgroundColor'=>$color,
						'label'=>'Pengeluaran Barang'];

		return response()->json(['datasets'=>$datasets,'labels'=>$labels]);
	}

	public function pengeluaranbarangperproject(){
		$request = request();
		//default :
		$periode = $request->periode;

		//================================PENDEKATA 1================================ 
		// pilih dept atau subdept ?
		// $dept = Wellhos_department::
		// SUDAH GET WHERE HAS PENGIRIMAN BARANG
		// MASIH PERLU DIUBAH HANYA HITUNG BRG YG DIKIRIMKAN SAJA
		//defaultnya departement; datatable harapannya bisa disearch..
		if($request->pilihan=='subdept'){
			$dept = Wellhos_subdept::whereHas('mr_subdept',function($q) use ($periode){
										$q->where('mr_subdepts.periode_mrs_id',$periode)
										->whereNull('mr_subdepts.deleted_at')
										->has('pengiriman_barang');
									})
									->where('Wellhos_departments_id',$request->dept_id)
									;
			if(isset($request->back)){
				// $dept = $dept->where('')
			}
			$dept = $dept->get();
		}
		else if ($request->pilihan=='dept') {
			$dept = Wellhos_department::whereHas('mr_subdept',function($q) use ($periode){
										$q->where('mr_subdepts.periode_mrs_id',$periode)
										->whereNull('mr_subdepts.deleted_at')
										->has('pengiriman_barang');
									})
									->get()
									;
		}else{
			$dept = Mr_subdept::has('pengiriman_barang')
								->where('periode_mrs_id',$periode)
								->where('wellhos_subdepts_id',$request->subdept_id)
								->get()
								;
		}
		
		// dd($dept);
		$data = [];
		$labels = [];
		$color = [];
		$total = 0;
		$action ='';
		foreach ($dept as $key => $d) {
			$jumlah = 0;
			if($request->pilihan=='mr'){
				$nama = $d->nomr.' | '.$d->project->area;
				$action ='';
				foreach ($d->detail_pengiriman_barang as $k3=> $dt) {
					$jumlah+= $dt->barang->avgprice * $dt->jumlah; // total per MR
				}
			}
			else{
				$nama = $d->nama;
				// dd($d);
				foreach ($d->mr_subdept as $k2 => $value) {
					foreach ($value->detail_pengiriman_barang as $k3=> $dt) {
						$jumlah+= $dt->barang->avgprice * $dt->jumlah; // total per MR
					}
				}
				if($request->pilihan=='dept'){
					$action = '<button type="button" class="btn btn-xs btn-info btndept"><i class="fa fa-bars"><i/></button>';
				}
				else{
					$action = '<button type="button" class="btn btn-xs btn-info btnsubdept"><i class="fa fa-bars"><i/></button>';
				}
			}
			$data[] = [
						'',
						$nama,
						number_format($jumlah),
						$action,
						$d->id
						];
		}
		$results = [
    				"draw" => 0,
    	        	"recordsTotal" => count($data),
    	        	"recordsFiltered" => count($data),
    	        	"data" => $data
    	        	];
		return response()->json($results);
		// foreach ($dept as $k1 => $d) {
		// 	// dd($d->mr_subdept->sum('total'));
		// 	$total=0;
		// 	$labels[] = $d->nama;
		// 	// $data[] = $d->mr_subdept->sum('total');
		// 	foreach ($d->mr_subdept as $k2 => $value) {
		// 		foreach ($value->detail_pengiriman_barang as $k3=> $dt) {
		// 			$total+= $dt->barang->avgprice * $dt->jumlah; // total per MR
		// 		}
		// 	}
		// 	$data[] = $total; //total per subdept
		// 	// $color[] = $this->randomwarna();
		// }
		// $datasets = ['label'=>'Pengeluaran Barang Subdept',
		// 			'data'=>$data,
		// 			'backgroundColor'=>$color];
		// dd($data);
		// return response()->json(['datasets'=>$datasets,'labels'=>$labels]);
		//=================================END PENDEKATAN 1=============================

		//=================================PENDEKATAN 2================================= 
		//MASIH BELUM SELECT YG ADA PENGIRIMAN BARANG
		// $hasil = DB::table('wellhos_subdepts')
		// 			->leftJoin('mr_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
		// 			->where('mr_subdepts.periode_mrs_id',$periode)
		// 			->whereNull('mr_subdepts.deleted_at')
		// 			->whereNull('wellhos_subdepts.deleted_at')
		// 			->select('wellhos_subdepts.nama'
		// 				,DB::raw('SUM(mr_subdepts.total) as total'))
		// 			->groupBy('nama')
		// 			->get()
		// 			;
		// 			dd($hasil);
		// $data = [];
		// $labels = [];
		// $color = [];
		// foreach ($hasil as $key => $value) {
		// 	$data[] = $value->total;
		// 	$labels[] = $value->nama;
		// 	$color[] = $this->randomwarna();
		// }
		// $datasets = ['label'=>'Pengeluaran Barang Subdept',
		// 			'data'=>$data,
		// 			'backgroundColor'=>$color];
		// return response()->json(['datasets'=>$datasets,'labels'=>$labels]);
		//=================================END PENDEKATAN 2=============================
	}

	public function timetofullmr(){ //butuh request get periode
		$request = request();
		$periode = $request->periode;
		$mr = Mr_subdept::has('pengiriman_barang')
						->where('periode_mrs_id',$periode)
						->whereNull('deleted_at')
						->get()
						;
		$data = [];
		$label = [];
		$color = $this->randomwarna();
		$backgroundColor = [];
		foreach ($mr as $k => $p) {
			$persen = 0;
			$totalbarang = 0;
			$kirim = 0;
			if(count($p->isi_mr)==0){ // mr tanpa ada isi
				$persen = 0;
			}
			else{
				foreach ($p->isi_mr as $k2 => $isi) { //jumlah barang diMR
					$totalbarang += $isi->jumlah;
				}
				foreach ($p->detail_pengiriman_barang as $key => $s) {
					$kirim += $s->jumlah;
				}
				$persen = $kirim/$totalbarang *100;
			}
			if($persen==100){ //hitung brp waktu yg diperlukan untuk menyelesaikan MR
				//created at last pengiriman - created at MR
				// dd('persen');;
				$date1 = new DateTime($p->tanggal);
				$date2 = new DateTime($p->pengiriman_barang->last()->tgl_kirim);
				$diff = $date2->diff($date1)->format("%a");
				$data[] = $diff;
				$label[] = $p->nomr. ' | '.$p->project->area;
				$backgroundColor[] = $color;
			}
			else{
				continue;
			}
		}
		return response()->json(['data'=>$data,'labels'=>$label,'colors'=>$backgroundColor]);
	}

	public function timetopo(){ //butuh request get periode
		$request = request();
		$periode = $request->periode;
		$po = Purchaseorder::has('Barangdatangs')
						->where('periode_mrs_id',$periode)
						->whereNull('deleted_at')
						->get()
						;
		$data = [];
		$label = [];
		// dd($po);
		$color = $this->randomwarna();
		$backgroundColor = [];
		foreach ($po as $k => $p) {
			$persen = 0;
			$totalbarang = 0;
			$spbm = 0;
			if(count($p->isi)==0){ // PO tanpa ada isi
				$persen = 0;
			}
			else{
				foreach ($p->isi as $k2 => $isi) { //jumlah barang diPO
					if(count($isi->revisi->where('status','approve'))>0){//isi_pos memiliki revisi
						$totalbarang += $isi->revisi->where('status','approve')->last()->jumlah_baru;
					}
					else{
						$totalbarang += $isi->jumlah;
					}
				}
				// dd($p->isispbm);
				foreach ($p->isispbm as $key => $s) {
					$spbm += $s->jumlah;
				}
				// dd($spbm.'/'.$totalbarang);
				$persen = $spbm/$totalbarang *100;
			}
			/*if($konterku%2==0){ //even
				$backgroundColor[] = "#00bfff";
			}
			else{ //odd
				$backgroundColor[] = "#4169e1";
			}*/
			if($persen==100){
				$date1 = new DateTime($p->tglpo);
				$date2 = new DateTime($p->Barangdatangs->last()->tanggaldatang);
				$diff = $date2->diff($date1)->format("%a");
				// dd($diff);
				$data[] = $diff;
				$label[] = $p->nopo. ' | '.$p->supplier->nama;
				$backgroundColor[] = $color;
			}
			else{
				continue;
			}
		}

		return response()->json(['data'=>$data,'labels'=>$label,'colors'=>$backgroundColor]);
	}

	public function getbaranggerak(){
		// $periodes = 
		$tglku = date('d-M-Y', strtotime(date('Y-m-d'). "-3 months"));
		return view('customView.laporan_getbaranggerak',['tglku'=>$tglku]);
	}

	public function getpengeluaranbarang(){
		$periodes = DB::table('periode_mrs')
        				->whereNull('deleted_at')
        				->select('id','judul as text' ,'tglmulai')
        				->get();
		$pnow = $periodes[0]->id;
		foreach ($periodes as $key => $value) {
        	if(date('m',strtotime($value->tglmulai)) == date('m') &&  date('Y',strtotime($value->tglmulai)) == date('Y')){
        		$pnow = $value->id;
        		break;
        	}
        }
		$periodes = json_encode($periodes);
		return view('customView.laporan_getpengeluaranbarang',['periodes'=>$periodes,'pnow'=>$pnow]);
	}
	
	public function dashboard_procurement(){
		$data = [];
		$periodes = DB::table('periode_mrs')
        				->whereNull('deleted_at')
        				->select('id','judul as text' ,'tglmulai')
        				->get();
        $pnow = $periodes[0]->id;
        foreach ($periodes as $key => $value) {
        	if(date('m',strtotime($value->tglmulai)) == date('m') &&  date('Y',strtotime($value->tglmulai)) == date('Y')){
        		$pnow = $value->id;
        		// dd($pnow);
        		break;
        	}
        }
        $periodes = json_encode($periodes);
		return view('customView.dashboard_procurement',['data'=>$data,
														'periodes'=>$periodes,
														'pnow'=>$pnow]);
	}	

	public function stokminmax(){
		$request=request();
		if($request->min ==1){ //cari stokgudang dibwh bts min
			$barang = Barang_gudang::where('stok','<','min')->select('kode','barangs_id','gudangs_id','stok','min as minmax')->get();
		}
		else{ //cari stokgudang diatas bts max
			$barang = Barang_gudang::where('stok','>','max')->select('kode','barangs_id','gudangs_id','stok','max as minmax')->get();
		}
		$data = [];
		foreach ($barang as $key => $v) {
			$data[] = [$v->kode,
						$v->barang->nama,
						$v->gudang->nama,
						$v->stok,
						$v->minmax,
						];
		}
		$results = [
            "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data
        ];
		return response()->json($results);
	}

	public function timetopostart(){
		$request= request();
		$periode = $request->periode;
		$po = Purchaseorder::has('Barangdatangs')
						->where('periode_mrs_id',$periode)
						->whereNull('deleted_at')
						->get()
						;
		$data = [];
		$label = [];
		// dd($po);
		$color = $this->randomwarna();
		$backgroundColor = [];
		foreach ($po as $k => $p) {
			$persen = 0;
			$totalbarang = 0;
			$spbm = 0;
			if(count($p->isi)==0){ // PO tanpa ada isi
				$persen = 0;
			}
			else{
				foreach ($p->isi as $k2 => $isi) { //jumlah barang diPO
					if(count($isi->revisi->where('status','approve'))>0){//isi_pos memiliki revisi
						$totalbarang += $isi->revisi->where('status','approve')->last()->jumlah_baru;
					}
					else{
						$totalbarang += $isi->jumlah;
					}
				}
				// dd($p->isispbm);
				foreach ($p->isispbm as $key => $s) {
					$spbm += $s->jumlah;
				}
				// dd($spbm.'/'.$totalbarang);
				$persen = $spbm/$totalbarang *100;
			}
			/*if($konterku%2==0){ //even
				$backgroundColor[] = "#00bfff";
			}
			else{ //odd
				$backgroundColor[] = "#4169e1";
			}*/
			if($persen==100){
				$date1 = new DateTime($p->tglpo);
				$date2 = new DateTime($p->Barangdatangs->last()->tanggaldatang);
				$diff = $date2->diff($date1)->format("%a");
				// dd($diff);
				$data[] = $diff;
				$label[] = $p->nopo. ' | '.$p->supplier->nama;
				$backgroundColor[] = $color;
			}
			else{
				continue;
			}
		}

		return response()->json(['data'=>$data,'labels'=>$label,'colors'=>$backgroundColor]);
	}

	public function aruskas(){
		$data='hehe';
		$periode = date('Y');
		$po = DB::table('purchaseorders')
				->whereNull('deleted_at')
				;
		$so = DB::table('sales_orders')
				->whereNull('deleted_at')
				;
		$po = $po->whereYear('purchaseorders.tglpo','=',date('Y'))
									;
		$so = $so->whereYear('sales_orders.created_at','=',date('Y'))
							;
		$po = $po->get();
		$so = $so->get();
		$k1 = $po->sum('total_akhir'); 
		$k2 = $po->sum('ongkir');
		$k3 = $k1 + $k2;
		$d1 = $so->sum('total');
		$keluar = ['Purchaseorder'=>$k1,
					'Ongkir PO'=>$k2,
					'Total' => $k3,
				];
		$masuk = ['Sales Order'=>$d1,
					'Total'=>$d1];
		$arus_bersih = $d1-$k3;
		return view('customView.laporan_arus_kas',['data'=>$data,'periode'=>$periode,'masuk'=>$masuk,'keluar'=>$keluar,'arus_bersih'=>$arus_bersih]);
	}

	public function getaruskas(){
		$request = request();
		$po = DB::table('purchaseorders')
				->whereNull('deleted_at')
				;
		$so = DB::table('sales_orders')
				->whereNull('deleted_at')
				;
		if($request->opsi=='bln'){
			$tglmulai = $request->tglmulai.'-01';
			$tglselesai = $request->tglselesai.'-'.date('t',strtotime($tglselesai));
			$po = $po->whereDate('purchaseorders.tglpo','>=',$tglmulai)
								->whereDate('purchaseorders.tglpo','<=',$tglselesai)
								;
			$so = $so->whereDate('sales_orders.created_at','>=',$tglmulai)
								->whereDate('sales_orders.created_at','<=',$tglselesai)
								;
		}
		else{
			$po = $po->whereYear('purchaseorders.tglpo','>=',$request->tglmulai)
								->whereYear('purchaseorders.tglpo','<=',$request->tglselesai)
								;
			$so = $so->whereYear('sales_orders.created_at','>=',$request->tglmulai)
								->whereYear('sales_orders.created_at','<=',$request->tglselesai)
								;
		}

		$po = $po->get();
		$so = $so->get();
		$k1 = $po->sum('total_akhir'); 
		$k2 = $po->sum('ongkir');
		$k3 = $k1 + $k2;
		$d1 = $so->sum('total');
		$keluar = ['Purchaseorder'=>$k1,
					'Ongkir PO'=>$k2,
					'Total' => $k3,
				];
		$masuk = ['Sales Order'=>$d1,
					'Total'=>$d1];
		// dd($datang);
		$arus_bersih = $d1-$k3;
		return response()->json(['masuk'=>$masuk,'keluar'=>$keluar,'arus_bersih'=>$arus_bersih]);
	}
	public function laporanrekening(){
    		$data = Request::all();
    		$datarek = DB::table('rekpers')->wherenull('deleted_at')->get();
			//$datasimpan = Bukuhutang::whereYear('created_at',$data['tahun'])->whereMonth('created_at',$data['bulan'])->get();
    		return View('customView.laporanrekening',compact('datasimpan','datarek'));	
    	}

    	public function laporanrekeningbulan(){
    		$data = Request::all();
    		
    		$datarek = DB::table('rekpers')->wherenull('deleted_at')->where('id',$data['rekening'])->get();
    		//$datalog = DB::table('log')->wherenull('deleted_at')->get();
			$datasimpan = Logtransaksi::whereYear('created_at',$data['tahun'])->whereMonth('created_at',$data['bulan'])->where('bank','LIKE',$datarek[0]->atasnama.' - '.$datarek[0]->bank)->get();

    		return View('customView.laporanrekeningbulan',compact('datasimpan','datarek'));	
    	}
	public function jurnal(){
			return View('customView.jurnal');	
	}
	public function jurnalbulan(){

		$first = DB::table('kasmasuk')
					->leftJoin('coas','kasmasuk.coa_id','=','coas.id')
					->select('kasmasuk.*','coas.nama as coanama',DB::raw('0 as flag'))
		            ->whereNull('kasmasuk.deleted_at');

		$users = DB::table('beban_bayars')
					->leftJoin('coas','beban_bayars.coa_id','=','coas.id')
					->select('beban_bayars.*','coas.nama as coanama',DB::raw('1 as flag'))
		            ->whereNull('beban_bayars.deleted_at')
		            ->union($first)
		            ->get();
		        //    dd($users);
		    return View('customView.jurnalbulan',compact('users'));	
	//	dd($users);
	}
}