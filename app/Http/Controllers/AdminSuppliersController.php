<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Maatwebsite\Excel\Facades\Excel;
	use App\Models\Pricelist;

	class AdminSuppliersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = true;
			$this->button_export = false;
			$this->table = "suppliers";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			$this->col[] = ["label"=>"No Telpon","name"=>"notelp"];
			$this->col[] = ["label"=>"Alamat","name"=>"alamat"];
			$this->col[] = ["label"=>"Gambar","name"=>"pic"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>'No Telpon','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Alamat','name'=>'alamat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gambar','name'=>'pic','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:suppliers','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			//$this->form[] = ['label'=>'No Telpon','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Alamat','name'=>'alamat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Gambar','name'=>'pic','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    public function getgege($id=0,$id2=0,$id3=0){
	    	echo $id2;echo $id3;
	    	dd($id);
    		/*$rows =Excel::load('C:\Users\asus.RASE-PC\Desktop\gg2.xls', function($reader) {
    			// $reader->dd();
    			// dd($reader->get());
			})->get();
    		$data = $rows->toArray()[0];
    		// dd($data);
    		$jmlh = 0;
    		foreach ($data as $key => $value) {
    			$jmlh ++;
    			DB::table('suppliers')->insert(['nama'=>rtrim($value['nama']),
    											'kode'=>$value['kode'],
    											'email'=>$value['email'],
    											'notelp'=>$value['notelp'],
    											'alamat'=>$value['alamat'],
    											'pic'=>$value['pic'],
    											'top'=>$value['top'],
    											'created_at'=>now(),
    											]);
    		}*/
			// $reader->dd();
	    }

	    public function getwepe($step=0){
	    	if(CRUDBooster::isSuperadmin()==false){
	    		return 'Lu siapa?';
	    	}
	    	if($step==0){
	    		return 'Lu siapa?';	
	    	}
	    	/*if($step==1){
	    		$rows =Excel::load('C:\Users\asus.RASE-PC\Desktop\gg1.xls', function($reader) {
				})->get();
	    		$data = $rows->toArray()[0];
	    		dd($data);
	    		$jmlh = 0;
	    		foreach ($data as $key => $value) {
	    			$jmlh ++;
	    			DB::table('pricelists')->insert(['kode'=>rtrim($value['kode']),
	    											'harga'=>$value['harga_beli'],
	    											'barangs_id'=>rtrim($value['nama']),
	    											'suppliers_id'=>$value['supplier_a'],
	    											'created_at'=>now(),
	    											]);
	    		}
	    		dd($jmlh);
	    	}*/
	    	/*if($step==2){
	    		$data = Pricelist::leftJoin('suppliers','suppliers.nama','like','pricelists.suppliers_id')
	    							->leftJoin('barangs','barangs.nama','like','pricelists.barangs_id')
	    							->select('pricelists.*','suppliers.id as id_suppliers','barangs.id as id_barangs')
	    							->get()
	    							;
				$jmlh=1;
	    		foreach ($data as $key => $value) {
	    			$value->update(['barangs_id'=>$value->id_barangs,
	    							'suppliers_id'=>$value->id_suppliers,
	    							]);
	    			$jmlh++;
	    		}
	    		dd($jmlh);
	    		dd('ganti');
	    	}*/
	    	if($step==3){
	    		// dd($step);
	    		return view('customView.secretUpdateBarang',['step'=>$step]);
	    		/*$rows =Excel::load('E:\web\wellhos\gg1.xls', function($reader) {
				})->get();
	    		$data = $rows->toArray();
	    		// dd($data);
	    		$jmlh = 0;
	    		foreach ($data as $key => $value) {
	    			$jmlh ++;
	    			$update = DB::table('barangs')->where('kode',rtrim($value['kode']))
	    									->update(['min'=>rtrim($value['min']),
	    											'max'=>rtrim($value['max']),
	    											])
	    											// ->get()
	    											;
					// dd($update);
	    		}
	    		dd($jmlh);*/
	    	}
	    	if($step==4){// add johnson
	    		return view('customView.secretUpdateBarang',['step'=>$step]);
	    	}
	    	if($step==5){// harga jual johnson
	    		return view('customView.secretUpdateBarang',['step'=>$step,'judul'=>'INSERT PERTAMA KALI harga jual johnson']); 
	    	}
	    	if($step==6){// menambahkan stok johnson ke master barang
	    		$this->postRahasia(6);
	    	}

	    }
	    //By the way, you can still create your own method in here... :)

	    public function postRahasia($id){
	    	$request = request();
	    	/*if($id == 3){
	    		$request = request();
	    		$file = $request->file('fileku');
	    		// dd($file);
	    		$rows =Excel::load($file, function($reader) {
				})->get();
	    		$data = $rows->toArray();
	    		// dd($data);
	    		$jmlh = 0;
	    		foreach ($data as $key => $value) {
	    			$jmlh ++;
	    			$update = DB::table('barangs')->where('kode',rtrim($value['kode']))
	    									->update(['min'=>rtrim($value['min']),
	    											'max'=>rtrim($value['max']),
	    											])
	    											// ->get()
	    											;
					// dd($update);
	    		}
    			dd($jmlh);
		    }*/
		    if($id==4){
	    		$file = $request->file('fileku');
	    		$rows =Excel::load($file, function($reader) {
				})->get();
	    		$data = $rows->toArray();
	    		// dd($data);
	    		$jmlh = 0;
	    		$gudang = DB::table('gudangs')->where('nama','johnson')->first();
	    		// dd($gudang);
	    		foreach ($data as $key => $value) {
	    			$jmlh ++;
	    			if($value['stok']<0 || $value['stok']==null || !is_numeric($value['stok']))
	    				$value['stok']=0;
	    			$barang = DB::table('barangs')->where('kode',$value['kode'])->first();
	    			$update = DB::table('barang_gudangs')->insert(['created_at'=>now(),
	    															'kode'=>$value['kode'],
	    															'barangs_id'=>$barang->id,
	    															'gudangs_id'=>$gudang->id,
	    															'stok'=>$value['stok'],
	    															'min'=>0,
	    															'max'=>0,
	    											])
	    											// ->get()
	    											;
					// dd($update);
	    		}
    			dd($jmlh);
		    }
		    if($id==5){
		    	$file = $request->file('fileku');
		    	$gudang = DB::table('gudangs')->where('nama','johnson')->first();
	    		// dd($gudang);
		    	$rows =Excel::load($file, function($reader) {
				})->get();
	    		$data = $rows->toArray();
	    		// dd($data);
	    		$jmlh = 0;
	    		foreach ($data as $key => $value) {
	    			$jmlh ++;
	    			$barang = DB::table('barangs')->where('kode',$value['kode'])->first();
	    			if($value['dua_berlian'] >0 ){
		    			DB::table('harga_juals')->insert(['created_at'=>now(),
	    											'barang_id'=>$barang->id,
	    											'harga_jual'=>$value['dua_berlian'],
	    											'ket'=>'dua_berlian',
	    											'harga_diskon'=>0,
	    										]);
	    			}
	    			if($value['gemilang'] >0 ){
		    			DB::table('harga_juals')->insert(['created_at'=>now(),
	    											'barang_id'=>$barang->id,
	    											'harga_jual'=>$value['gemilang'],
	    											'ket'=>'gemilang',
	    											'harga_diskon'=>0,
	    										]);
	    			}
	    			if($value['online_shop'] >0 ){
		    			DB::table('harga_juals')->insert(['created_at'=>now(),
	    											'barang_id'=>$barang->id,
	    											'harga_jual'=>$value['online_shop'],
	    											'ket'=>'online_shop',
	    											'harga_diskon'=>0,
	    										]);
	    			}
	    		}
	    		dd($jmlh);
		    }

		    if($id==6){
		    	$gudang = DB::table('gudangs')->where('nama','johnson')->first();
		    	$datajohnson = DB::table('barang_gudangs')->whereNull('deleted_at')->where('gudangs_id','=',$gudang->id)->get();
		    	// dd($datajohnson);
		    	$jmlh = 0;
		    	foreach ($datajohnson as $key => $value) {
		    		$jmlh ++;
		    		// dd($value);
		    		$barang = DB::table('barangs')->where('id',$value->barangs_id)->increment('akumstok',$value->stok);
		    	}
		    	dd($jmlh);
		    }
		}
}