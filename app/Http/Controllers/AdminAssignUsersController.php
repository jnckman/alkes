<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Assign_user;

	class AdminAssignUsersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "table_name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "assign_users";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"User Id","name"=>"user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Privilege","name"=>"user_id","callback_php"=>'$this->privilegeku($row->id)'];
			$this->col[] = ["label"=>"Dep Subdep","name"=>"table_id","callback_php"=>'$this->table_idku($row)'];
			$this->col[] = ["label"=>"Table Name","name"=>"table_name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Table Name','name'=>'table_name','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'wellhos_departments;wellhos_subdepts'];
			$this->form[] = ['label'=>'Table Id','name'=>'table_id','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			//$this->form[] = ['label'=>'Table Name','name'=>'table_name','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'wellhos_departments,wellhos_subdepts'];
			//$this->form[] = ['label'=>'Table Id','name'=>'table_id','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here
	    	DB::table('assign_users')->where('id',$id)->update(['table_id'=>null]);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        Assign_user::destroy($id);
	        // dd($id);
	    }



	    public function getAdd(){
	    	$request= request();
	    	$data = [];
	    	$data['page_title'] = 'Assign User';
	    	$data['ruteku'] = route('assign.postSave');
	    	$data['dataku'] = [];
	    	if(isset($request->newadded)){
	    		// dd($request);
	    		$data['users'] = DB::table('cms_users')
			    				->leftJoin('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
			        			->where('cms_users.id',$request->newadded)
			    				->select('cms_users.id','cms_users.name as text','cms_privileges.name as title')
			    				->get();

				// dd($data);
			    $data['dataku'] = ['id'=>$request->newadded,'user_id'=>$request->newadded];
	    	}
	    	else{
		    	$data['users'] = [];
		    	$data['users'] = DB::table('cms_users')->leftjoin('assign_users','assign_users.user_id','=','cms_users.id')
		    										->leftjoin('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
		    										->select('cms_users.id'
		    											// ,'cms_users.name as text'
		    											,DB::raw("CONCAT(cms_users.name,' | ',cms_privileges.name) AS text")
		    											,'cms_privileges.name as title')
		    										// ->where('cms_privileges.name','<>','super administrator')
		    										// ->where('cms_privileges.name','<>','customer')
		    										->where('cms_privileges.name','like','%mr%')
		    										->whereNull('assign_users.table_id')
		    										// ->whereNull('assign_users.deleted_at')
		    										->get();
				$data['table_id'] = [];
			}
	    	$data['users'] = json_encode($data['users']);
	    	$data['dataku'] = json_encode($data['dataku']);
	    	$data['table_id'] = json_encode($data['table_id']);
	    	// dd($data);
	    	$this->cbView('customView.addAssignUser',$data);
	    }

	    public function getEdit($id){
	    	$request = request();
	    	// dd($request);
	    	$data = [];
	    	$data['page_title'] = 'Edit Assign User';
	    	$data['users'] = [];
	    	$data['ruteku'] = route('assign.postEdit');
	    	$data['table_id'] = [];
	    	$data['dataku'] = [];
	    	$data['users'] = DB::table('cms_users')
			    				->join('assign_users','assign_users.user_id','=','cms_users.id')
			    				->leftjoin('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
			    				->where('assign_users.id','=',$id)
			    				->select('cms_users.id','cms_users.name as text','cms_privileges.name as title')
			    				->get();
	    	$data['dataku'] = DB::table('assign_users')->where('id',$id)->first();
	    	$data['table_id'] = DB::table($data['dataku']->table_name)
	    						// ->where('id',$data['dataku']->table_id)
	    						->select('id','nama as text')
	    						->whereNull('deleted_at')
	    						->get();
	    	$data['users'] = json_encode($data['users']);
	    	$data['dataku'] = json_encode($data['dataku']);
	    	$data['table_id'] = json_encode($data['table_id']);
	    	$data['idku']=$id;
	    	$this->cbView('customView.addAssignUser',$data);
	    }

	    public function gettableid(){
	    	$request = request();
	    	$privilege = DB::table('cms_users')->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')->where('cms_users.id',$request->value)->select('cms_privileges.name')->first()->name;
	    	if(strtolower($privilege) == 'mr-1' || strtolower($privilege) == 'mr-0'){
	    		$table = 'wellhos_subdepts';
	    	}
	    	else{
	    		$table = 'wellhos_departments';	
	    	}
	    	$data = DB::table($table)->whereNull($table.'.deleted_at')
	    									// ->whereNotIn('nama',$listnama)
	    									->select('id','nama as text')->get();
	    	if($table == 'wellhos_subdepts'){
	    		$json = file_get_contents('http://api.gis.gemilang.co.id/accounting/asm.php');
	            $dataz = json_decode($json,true);
	            // dd($dataz);
	            $gisSub = [];
	            // $listnama = [];
	            foreach ($dataz as $key => $value) {
	              $gisSub[] = (object)([
	                'id' => $value['id_asm'],
	                'text' => $value['nama_asm'].' | '.$value['area'],
	                // 'title' => 'gis'
	              ]);
	              // $listnama[] = $value['nama_asm'].' | '.$value['area'];
	            }
	            // dd($listnama);
	            $idOpd = DB::table('wellhos_departments')->where('nama','opd')->first()->id;
		    	if(count($gisSub>0)){
		    		foreach ($gisSub as $key => $value) {
		    			if($data->contains('text',$value->text)){
		    				continue;
		    			}
		    			else{
		    				$value->id = DB::table('wellhos_subdepts')->insertGetId([
		    				'nama'=>$value->text,//'pic'=>$detilSubdept[0],
					        'phone'=>'0000000',
					        'keterangan'=>'',
					        'wellhos_departments_id'=> $idOpd,
					        'created_at'=>now(),
					    	]);
		    				$data->push($value);
		    			}
		    		}
		    		// dd($data);
		    	}
	    	}
	    	// dd($data);
	    	return response()->json($data);
	    }

	    public function postSave(){
	    	$request = request();
	    	$privilege = DB::table('cms_users')->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')->where('cms_users.id',$request->user_id)->select('cms_privileges.name')->first()->name;
	    	if(strtolower($privilege) == 'mr-1' || strtolower($privilege) == 'mr-0'){
	    		$table = 'wellhos_subdepts';
	    	}
	    	else{
	    		$table = 'wellhos_departments';	
	    	}
	    	// dd($request);
	    	DB::table('assign_users')
	    		->insert(['user_id'=>$request->user_id,
	    				'table_name'=>$table,
	    				'table_id'=>$request->table_id,
	    				'created_at'=>now(),
	    			]);
	    	CRUDBooster::redirect(CRUDBooster::adminPath('assign_users'),'Berhasil Menambah Data','success');
	    }

	    public function postEdit(){
	    	$request = request();
	    	$privilege = DB::table('cms_users')->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')->where('cms_users.id',$request->user_id)->select('cms_privileges.name')->first()->name;
	    	if(strtolower($privilege) == 'mr-1' || strtolower($privilege) == 'mr-0'){
	    		$table = 'wellhos_subdepts';
	    	}
	    	else{
	    		$table = 'wellhos_departments';	
	    	}
	    	DB::table('assign_users')
	    		->where('id',$request->id)
	    		->update(['user_id'=>$request->user_id,
	    				'table_name'=>$table,
	    				'table_id'=>$request->table_id,
	    				'updated_at'=>now(),
	    			]);
	    	CRUDBooster::redirect(CRUDBooster::adminPath('assign_users'),'Berhasil Mengubah Data','success');
	    }

	    public function table_idku($row){
	    	// dd($row);
	    	return DB::table($row->table_name)->where('id',$row->table_id)->select('nama')->first()->nama;
	    }

	    public function privilegeku($id){
		    $data= DB::table('cms_users')
			    		->leftjoin('cms_privileges','cms_users.id_cms_privileges','=','cms_privileges.id')
			    		->where('cms_users.id',$id)
			    		->select('cms_privileges.name')
			    		->first()
		    		;
		    return $data->name;
	    }
	}