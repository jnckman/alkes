<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Wellhos_department;

	class AdminWellhosDepartmentsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = true;
			$this->button_export = false;
			$this->table = "wellhos_departments";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"Id","name"=>"id"];
			$this->col[] = ["label"=>"Phone","name"=>"phone"];
			$this->col[] = ["label"=>"Pic","name"=>"pic"];
			$this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			$this->form[] = ['label'=>'Pic','name'=>'pic','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:wellhos_departments','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			//$this->form[] = ['label'=>'Pic','name'=>'pic','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:wellhos_departments','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();
	        $this->sub_module[] = ['label'=>'','path'=>'wellhos_subdepts','parent_columns'=>'nama','foreign_key'=>'wellhos_departments_id','button_color'=>'info','button_icon'=>'fa fa-bars'];


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    public function listdepartments($id){
			$data= array();
			$datalr = DB::table('lock_rekaps')->where('periode_mrs_id',$id)->select('id')->whereNull('deleted_at')->get();
			$dataperiode = DB::table('periode_mrs')->where('id',$id)->whereNull('deleted_at')->get();
			$datamr = DB::table('mr_subdepts')->where('periode_mrs_id',$id)->whereNull('deleted_at');
			$data['jumlahmr'] = $datamr->count();
			$data['jumlahmrapprove'] = $datamr->where('approval','LIKE','oke')->count();
			$data['jumlahmrpr'] = $datamr->where('approval','LIKE','oke')->where('sudahpr',1)->count();

			$data['periode'] = $dataperiode[0];

	    	if(sizeof($datalr)==0)
	    		$data['rekap'] = 1;
	    	else
	    		$data['rekap'] = 0;
	    	$data['id'] = $id;
	    	$this->cbView('customView.listdepartments',$data);
	    }


	    public function listdepartmentsdtajax($id){
	    	// $hasil = DB::table('wellhos_departments')
	    	// 			->whereNull('wellhos_departments.deleted_at')
	    	// 			->get();
	    	// $idku = 3;

	    	$hasil = Wellhos_department::whereHas('mr_subdept', function ($query) use ($id) {
							    $query->where('periode_mrs_id','=', $id);
							})
	    					// ->toSql()
	    					->get()
	    					;
	    	// 				dd($hasil);
	    	/*$hasil = DB::table('wellhos_departments')
	    				->whereExists(function($query) use ($id){
	    					$query->select(DB::raw('*'))
	    						->from('mr_subdepts')
	    						->join('wellhos_subdepts','wellhos_subdepts.id','=','mr_subdepts.wellhos_subdepts_id')
	    						->where('wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
	    						->where('mr_subdepts.periode_mrs_id',$id)
	    						->whereNull('wellhos_subdepts.deleted_at')
	    						->whereNull('mr_subdepts.deleted_at')
	    						// ->whereNull('wellhos_subdepts')
	    						;
	    				})
	    				->whereNull('wellhos_departments.deleted_at')
	    				// ->toSql()
	    				->get()
	    				;*/
	    	// $hasil = DB::select('select *

	    	// 	from wellhos_departments where exists 
	    	// 	(select * from mr_subdepts inner join wellhos_subdepts on wellhos_subdepts.id = mr_subdepts.wellhos_subdepts_id where wellhos_subdepts.deleted_at is null and wellhos_departments.id = wellhos_subdepts.wellhos_departments_id and periode_mrs_id = '.$id.' and wellhos_subdepts.deleted_at is null and mr_subdepts.deleted_at is null) 
	    	// 	and wellhos_departments.deleted_at is null')
	    	// ;
	    	// dd($hasil);
	    	$data = array();
	    	foreach($hasil as $d){
	    		// dd($d->mr_subdept->where('periode_mrs_id','=', $id)->count()->tosql());
	    		$namashow = $d->nama.' ( '.$d->mr_subdept->where('periode_mrs_id','=', $id)->count() .' MR )';
	    		// $namashow = $d->nama;
	     		$data[] = [
	     				$namashow,
	     				   '<a href="../materialrequestsystem/'.$id.'/'.$d->id.'">Lihat Isi MR</a>',
							
	     		 		];
	    	 }
	    	
	    	$results = [
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);

	    }


	    public function listdepartmentsall($id){
			$data= array();
	    	$datalr = DB::table('lock_rekaps')->select('kode','tanggalrekap',DB::raw('count(approve) as total'),DB::raw('sum(approve) as approved'))->where('periode_mrs_id',$id)->groupby('kode','periode_mrs_id','tanggalrekap')->get();
	    	


	    	$dataperiode = DB::table('periode_mrs')->where('id',$id)->get();

	    	$data['periode'] = $dataperiode[0];
	    	if(sizeof($datalr)==0)
	    		$data['rekap'] = 1;
	    	else
	    		$data['rekap'] = 0;
	    	$data['id'] = $id;
	    	$data['listrekap'] = $datalr;
	    	$this->cbView('customView.listdepartmentsall',$data);
	    }
	    public function listdepartmentsalldtajax($id){
	    	$hasil = DB::table('mr_subdepts')
	    			 ->leftJoin('wellhos_subdepts','mr_subdepts.wellhos_subdepts_id','=','wellhos_subdepts.id')
	    			 ->leftJoin('wellhos_departments','wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
	    			 ->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
	    			 ->select('mr_subdepts.nomr','mr_subdepts.id','wellhos_subdepts.nama as namaws','wellhos_departments.nama as namawd','mr_subdepts.tanggal','mr_subdepts.sudahpr','projects.area'
	    			)
	    			 ->where('periode_mrs_id',$id)
	    			 ->wherenull('mr_subdepts.deleted_at')
	    			 ->get();
	    	$cb = '';
	    	$data = array();

	    	foreach($hasil as $d){

	    		if($d->sudahpr == 0)
	    			$cb = '<input id="checkBox" type="checkbox" name="mrid[]" value="'.$d->id.'">';
	    		else
	    			$cb = 'sudah PR';

	     		$data[] = [
	     				$cb,
	     				$d->nomr,
	     				$d->namaws,
	     				$d->area,
	     				$d->namawd,
	     				$d->tanggal,
	     				   '<a href="../isisubdeptmr/'.$d->id.'">Lihat Isi MR </a>',
	    		
	     		 		];
	    	 }
	    	
	    	$results = [
	    				"draw" => 1,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	echo json_encode($results);

	    }

	    public function listdepartmentsallsubmit(Request $request){
	    	$data = Request::all();
	    	$mrid = $data['mrid'];

	    }

	    //By the way, you can still create your own method in here... :)


	}
