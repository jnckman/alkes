<?php namespace App\Http\Controllers;

  use Session;
  use Request;
  use DB;
  use CRUDBooster;
  use App\Models\Barang;
  use App\Models\Mr_subdept;
  use App\Models\Isi_mr_subdept;
  use App\Models\Project;
  use App\Models\Assign_user;
  use App\Models\Kategori;
  use App\Models\Request_barang;
  use App\Models\Group;
  use App\Models\Periode_mrs;
  use App\Models\Log_mr;
  use App\Models\Cms_user;
  use App\Models\Wellhos_subdept;
  use App\Models\Detail_Pengiriman_Barang;
  use App\Cart;
  use DateTime;
  use Illuminate\Support\Facades\Storage;
  use App\Models\Master_project;
  use App\Models\Revisi_mr_subdept;

  class AdminMrSubdepts36Controller extends \crocodicstudio\crudbooster\controllers\CBController {

      public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "mr_subdepts";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nomr","name"=>"nomr"];
			$this->col[] = ["label"=>"Periode MR","name"=>"periode_mrs_id","join"=>"periode_mrs,judul"];
			$this->col[] = ["label"=>"Projects","name"=>"projects_id","join"=>"projects,nama"];
      $this->col[] = ["label"=>"Area","name"=>"projects_id","join"=>"projects,area"];
			$this->col[] = ["label"=>"Tanggal","name"=>"tanggal"];
			$this->col[] = ["label"=>"Wellhos Subdepts","name"=>"wellhos_subdepts_id","join"=>"wellhos_subdepts,nama"];
			$this->col[] = ["label"=>"Gudang","name"=>"gudang"];
			$this->col[] = ["label"=>"Total (Rp)","name"=>"total","callback_php"=>'number_format($row->total)'];
			$this->col[] = ["label"=>"User","name"=>"user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Approval","name"=>"approval"];
      // $this->col[] = ["label"=>"Progress","name"=>"id",'callback_php'=>'$this->getProgress($row->id)'];
      $this->col[] = ["label"=>"Progress Kirim %","name"=>"id",'callback_php'=>'$this->getProgress($row->id)'];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Nomr','name'=>'nomr','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gudang','name'=>'gudang','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Projects Id','name'=>'projects_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'projects,nama'];
			$this->form[] = ['label'=>'Wellhos Subdepts Id','name'=>'wellhos_subdepts_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'wellhos_subdepts,nama'];
			$this->form[] = ['label'=>'Approval','name'=>'approval','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Periode Mrs_id','name'=>'periode_mrs_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'periode_mrs,judul'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Nomr','name'=>'nomr','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Gudang','name'=>'gudang','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Projects Id','name'=>'projects_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'projects,nama'];
			//$this->form[] = ['label'=>'Wellhos Subdepts Id','name'=>'wellhos_subdepts_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'wellhos_subdepts,nama'];
			//$this->form[] = ['label'=>'Approval','name'=>'approval','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Periode Mrs_id','name'=>'periode_mrs_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'periode_mrs,judul'];
			# OLD END FORM

			/*
          | ----------------------------------------------------------------------
          | Sub Module
          | ----------------------------------------------------------------------
      | @label          = Label of action
      | @path           = Path of sub module
      | @foreign_key    = foreign key of sub table/module
      | @button_color   = Bootstrap Class (primary,success,warning,danger)
      | @button_icon    = Font Awesome Class
      | @parent_columns = Sparate with comma, e.g : name,created_at
          |
          */
          $this->sub_module = array();
          $this->sub_module[] = ['label'=>'','path'=>'isi_mr_subdepts38','parent_columns'=>'nomr,tanggal','foreign_key'=>'mr_subdepts_id','button_color'=>'success','button_icon'=>'fa fa-bars'];



          /*
          | ----------------------------------------------------------------------
          | Add More Action Button / Menu
          | ----------------------------------------------------------------------
          | @label       = Label of action
          | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
          | @icon        = Font awesome class icon. e.g : fa fa-bars
          | @color     = Default is primary. (primary, warning, succecss, info)
          | @showIf      = If condition when action show. Use field alias. e.g : [id] == 1
          |
          */
          $this->addaction = array();
          $privilegeku = explode('-',CRUDBooster::myPrivilegeName());
          if($privilegeku[1] == '4' || CRUDBooster::isSuperadmin()==true){
            $this->addaction[] = ['label'=>'', 'url'=>CRUDBooster::mainpath('approve/[id]'),'icon'=>'fa fa-check','color'=>'primary','showIf'=>'[approval] == belum','confirmation' => true,'title'=>'approve'];
          }

          /*
          | ----------------------------------------------------------------------
          | Add More Button Selected
          | ----------------------------------------------------------------------
          | @label       = Label of action
          | @icon      = Icon from fontawesome
          | @name      = Name of button
          | Then about the action, you should code at actionButtonSelected method
          |
          */
          $this->button_selected = array();


          /*
          | ----------------------------------------------------------------------
          | Add alert message to this module at overheader
          | ----------------------------------------------------------------------
          | @message = Text of message
          | @type    = warning,success,danger,info
          |
          */
          $this->alert        = array();



          /*
          | ----------------------------------------------------------------------
          | Add more button to header button
          | ----------------------------------------------------------------------
          | @label = Name of button
          | @url   = URL Target
          | @icon  = Icon from Awesome.
          |
          */
          $this->index_button = array();



          /*
          | ----------------------------------------------------------------------
          | Customize Table Row Color
          | ----------------------------------------------------------------------
          | @condition = If condition. You may use field alias. E.g : [id] == 1
          | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
          |
          */
          $this->table_row_color = array();


          /*
          | ----------------------------------------------------------------------
          | You may use this bellow array to add statistic at dashboard
          | ----------------------------------------------------------------------
          | @label, @count, @icon, @color
          |
          */
          $this->index_statistic = array();



          /*
          | ----------------------------------------------------------------------
          | Add javascript at body
          | ----------------------------------------------------------------------
          | javascript code in the variable
          | $this->script_js = "function() { ... }";
          |
          */
          $this->script_js = NULL;


            /*
          | ----------------------------------------------------------------------
          | Include HTML Code before index table
          | ----------------------------------------------------------------------
          | html code to display it before index table
          | $this->pre_index_html = "<p>test</p>";
          |
          */
          $this->pre_index_html = null;



          /*
          | ----------------------------------------------------------------------
          | Include HTML Code after index table
          | ----------------------------------------------------------------------
          | html code to display it after index table
          | $this->post_index_html = "<p>test</p>";
          |
          */
          $this->post_index_html = null;



          /*
          | ----------------------------------------------------------------------
          | Include Javascript File
          | ----------------------------------------------------------------------
          | URL of your javascript each array
          | $this->load_js[] = asset("myfile.js");
          |
          */
          $this->load_js = array();



          /*
          | ----------------------------------------------------------------------
          | Add css style at body
          | ----------------------------------------------------------------------
          | css code in the variable
          | $this->style_css = ".style{....}";
          |
          */
          $this->style_css = NULL;



          /*
          | ----------------------------------------------------------------------
          | Include css File
          | ----------------------------------------------------------------------
          | URL of your css each array
          | $this->load_css[] = asset("myfile.css");
          |
          */
          $this->load_css = array();


      }


      /*
      | ----------------------------------------------------------------------
      | Hook for button selected
      | ----------------------------------------------------------------------
      | @id_selected = the id selected
      | @button_name = the name of button
      |
      */
      public function actionButtonSelected($id_selected,$button_name) {
          //Your code here

      }


      /*
      | ----------------------------------------------------------------------
      | Hook for manipulate query of index result
      | ----------------------------------------------------------------------
      | @query = current sql query
      |
      */
      public function hook_query_index(&$query) {
          //Your code here

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for manipulate row of index table html
      | ----------------------------------------------------------------------
      |
      */
      public function hook_row_index($column_index,&$column_value) {
        //Your code here
      }

      /*
      | ----------------------------------------------------------------------
      | Hook for manipulate data input before add data is execute
      | ----------------------------------------------------------------------
      | @arr
      |
      */
      public function hook_before_add(&$postdata) {
          //Your code here

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for execute command after add public static function called
      | ----------------------------------------------------------------------
      | @id = last insert id
      |
      */
      public function hook_after_add($id) {
          //Your code here

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for manipulate data input before update data is execute
      | ----------------------------------------------------------------------
      | @postdata = input post data
      | @id       = current id
      |
      */
      public function hook_before_edit(&$postdata,$id) {
          //Your code here

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for execute command after edit public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
      */
      public function hook_after_edit($id) {
          //Your code here
          

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for execute command before delete public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
      */
      public function hook_before_delete($id) {
          //Your code here

      }

      /*
      | ----------------------------------------------------------------------
      | Hook for execute command after delete public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
      */
      public function hook_after_delete($id) {
          //Your code here

      }



      //By the way, you can still create your own method in here... :)

      public function materialrequestbyuser(){
        $data = DB::table('barangs')->whereNull('deleted_at')->get();
        $this->cbView('customView.materialrequestlogin',compact('data'));
      }

      public function materialrequestmainpage(){
        $data=[];
        $data['privilege'] = explode('-',CRUDBooster::myPrivilegeName());
        $data['subdepts'] = [];
        $data['projects'] = [];
        $data['periode'] = [];
        $data['nama_dept'] = [];
        $data['departments'] = [];
        // dd($data['privilege']);
        // http://api.gis.gemilang.co.id/accounting/periode_project.php
        if(strtolower($data['privilege'][0])!='mr' && CRUDBooster::isSuperadmin()==false){
          return redirect('/');
        }
        try{
        /*if(CRUDBooster::isSuperadmin()==false){
          $mrku = Cms_user::find(CRUDBooster::myid())->mr_subdept->where('approval','belum');
          // dd(count($mrku));
          if( count($mrku)>0 ){ //$mrku == 'belum'){
            // dd('bladihel');
            $data['setuju'] = 0;
            $data['periodeada']=1;
            session()->flash('message','<h4>MR terakhir ada belum mendapat approval</h4>');
            session()->flash('message_type','warning');
            $this->cbView('customView.materialrequestbyuser',$data);
            return;
          }
        }*/
        $data['setuju']=1;
        $projects = [];
        $periode = DB::table('periode_mrs')->whereNull('deleted_at')
                          ->where('selesai','<>',1)
                          ->whereDate('tglselesai','>=',date('Y-m-d'))
                          ->whereDate('tglselesai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))
                          ->orWhere(function($query){
                            $query->where('tglmulai','<=',date('Y-m-d', strtotime(date('Y-m-d'). "+3 months")))->where('tglmulai','>=',date('Y-m-d'))->where('selesai','<>',1);
                          })
                          // ->whereNotIn('id',$periodeMrBelumApprove)
                          ->orWhere('status','=','true')
                          ->Where('selesai','=','0') //noterase
                          // ->where('status','=','true')
                          ->select('id','judul as text')
                          ->get();
                          // dd($periode);
        if(count($periode)>0) $data['periodeada']=1;
        else $data['periodeada']=0;
        $data['nama_dept'] = '""';
        if(session()->has('cart')) {
          $cart = session()->get('cart');
          $data['projectslist'] = session()->get('cart')->projects;
        }
        if((int)$data['privilege'][1]<=1){
          $datauser = DB::table('assign_users')->leftJoin('wellhos_subdepts','wellhos_subdepts.id','=','assign_users.table_id')
                                            ->leftJoin('wellhos_departments','wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
                                            ->where('assign_users.user_id','=',CRUDBooster::myid())
                                            ->where('assign_users.table_name','=','wellhos_subdepts')
                                            ->whereNull('assign_users.deleted_at')
                                            ->select('wellhos_departments.id as id_dept','wellhos_subdepts.id as id_subdept','wellhos_departments.nama as nama_dept','wellhos_subdepts.nama as nama_subdept')
                                            ->get();
                                            // dd($datauser);
          $data['departments'][] = ['id'=>$datauser[0]->id_dept,'text'=>$datauser[0]->nama_dept];
          // dd($data['departments']);
          if(strtolower($data['departments'][0]['text'])=='opd'){
            $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/asm.php');
            $dataz = json_decode($json,true);
            // dd($dataz);
            foreach ($dataz as $key => $value) {
              $tmp = ([
                'id' => $value['id_asm'],
                'text' => $value['nama_asm'].' | '.$value['area'],
              ]);
              // dd($tmp);
              if($tmp['text']==$datauser[0]->nama_subdept){
                $data['subdepts'][] = $tmp;
              }
            }
          }
          else{
            $data['subdepts'][] = ['id'=>$datauser[0]->id_subdept,'text'=>$datauser[0]->nama_subdept];
          }
          // dd($data['subdepts']);
        }
        else if((int)$data['privilege'][1]<4 && (int)$data['privilege'][1]>0){
          $datauser = DB::table('assign_users')->leftJoin('wellhos_departments','wellhos_departments.id','=','assign_users.table_id')
                                            ->leftJoin('wellhos_subdepts','wellhos_subdepts.wellhos_departments_id','=','wellhos_departments.id')
                                            ->where('assign_users.user_id','=',CRUDBooster::myid())
                                            ->whereNull('assign_users.deleted_at')
                                            ->select('wellhos_departments.id as id_dept','wellhos_subdepts.id as id_subdept','wellhos_departments.nama as nama_dept','wellhos_subdepts.nama as nama_subdept')
                                            // ->select('wellhos_departments.id as id','wellhos_departments.nama as text')
                                            // ->first()s
                                            ->get()
                                            ;
                                            // dd($datauser[0]->nama_dept);
          $data['subdepts'] = [];
          $data['departments'] = [];
          if(strtolower($datauser[0]->nama_dept) =='opd'){
            // dd('loh');
            $data['departments'][] = ['id'=>$datauser[0]->id_dept,'text'=>$datauser[0]->nama_dept];
            $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/asm.php');
            $dataz = json_decode($json,true);
            // dd($dataz);
            foreach ($dataz as $key => $value) {
              $data['subdepts'][] = ([
                'id' => $value['id_asm'],
                'text' => $value['nama_asm'].' | '.$value['area'],
              ]);
            }
            // dd($data['subdepts']);
          }
          else{
            foreach ($datauser as $key => $value) {
              $data['subdepts'][] = ['id'=>$value->id_subdept,'text'=>$value->nama_subdept];
              $data['departments'][] = ['id'=>$value->id_dept,'text'=>$value->nama_dept];
            }
          }
        }
        else{
          // dd('gg');
          $datauser = ['etogg'];
          $data['departments'] = DB::table('wellhos_departments')->whereNull('deleted_at')
                                                                ->select('id','nama as text')->get();
                                                                // dd($data['departments']);
        }
        if(CRUDBooster::isSuperadmin()==true){
          $datauser = ['etogg'];
          $data['departments'] = DB::table('wellhos_departments')->whereNull('deleted_at')
                                                                ->select('id','nama as text')->get();
        }
        if(count($datauser)==0){
          // dd('gg');
            $data['setuju'] = 0;
            session()->flash('message','<h4>Anda Belum Memiliki Department</h4><h4>Harap hubungi Admin</h4>');
            session()->flash('message_type','warning');
            return $this->cbView('customView.materialrequestbyuser',$data);
          }
        $data['periode'] = json_encode($periode);
        $data['privilege'] = json_encode($data['privilege']);
        $data['projects'] = json_encode($data['projects']);
        $data['subdepts'] = json_encode($data['subdepts']);
        $data['departments'] = json_encode($data['departments']);
        // dd($data['departments']);
        $this->cbView('customView.materialrequestbyuser',$data);
        } catch (Exception $e) {
          die("Gagal mengambil API");
        }
      }

      public function postProject(){
        $request = request();
        // dd($request);
        $ambildb = 0;
        if(!isset($request->periode)) CRUDBooster::redirect( route('subdept.mr.mainpage'),'Pilih Periode terlebih dahulu !');
        if(isset($request->projects)){
          $gg[0] = $request->projects;
          $gg[1] = $request->project_text;
          $cart = session()->get('cart');
          if(isset($cart->projects)){
            if(array_key_exists($gg[0], $cart->projects)) {
              CRUDBooster::redirect( route('subdept.mr.mainpage'),'Project sudah ada pada list');
            }
          }
          //perlu ditambahkan jika gagal ambil data dari API
          if(strtolower($request->dept_nama) == 'opd') {
            $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/list_project_asm.php?asm='.$request->subdepts);
          }
          else {
            // $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/periode_project.php');
            //diganti dengan ambil dari DB
            $ambildb = 1;
            $json = DB::table('projects')->where('id',$request->projects)->get();
          }
          $data = json_decode($json,true);
          // echo($request->projects);
          // dd($data);
          foreach ($data as $key => $value) {
            if($value['id']==$request->projects){
              $detail = $value;
              break;
            }
          }
          if($ambildb==1){
            $detail['PIC']=DB::table('wellhos_subdepts')->where('id',$request->subdepts)->first()->pic;
          }
          // dd($detail);
          $cart->projects[$gg[0]] = ['id'=>$gg[0],'text'=>$gg[1],'detail'=>$detail];
          if(isset($request->subdepts)){
            $namaSubdept = $request->subdept_nama;
            // $namaSubdept = DB::table('wellhos_subdepts')->where('id','=',$request->subdepts)->first()->nama;
            $cart->projects[$gg[0]]['subdept'] = ([
                    'id'=>$request->subdepts,
                    'nama'=>$namaSubdept,
                    'dept_id'=>$request->dept_id,
                    'dept_nama'=>$request->dept_nama,
                  ]);
          }
          // dd($cart);
          if($ambildb==0){
            $jsoncnc = file_get_contents('http://api.gis.gemilang.co.id/accounting/detail_project.php?id='.$cart->projects[$gg[0]]['detail']['rekening']);
            // dd($cart);
            $cnc = json_decode($jsoncnc,true);
            $cart->projects[$gg[0]]['detail']['cnc'] = $cnc[0]['cnc'];
          }
          $cart->projects[$gg[0]]['periode'] = $request->periode;
          session()->put('cart',$cart);
          CRUDBooster::redirect( route('subdept.mr.mainpage'),'Data berhasil Ditambahkan','success');
        }
        else CRUDBooster::redirect( route('subdept.mr.mainpage'),'Pilih Project terlebih dahulu !');
      }

      public function getBarangsByKategoris($id=0){
        if(!session()->has('cart')) CRUDBooster::redirect( route('subdept.mr.mainpage'),'');
        $cart = session()->get('cart');
        $kategoris = Kategori::all()->sortBy(function($kategoris) {
                        return $kategoris->isi_mr_subdept()->count();
                    })->reverse();
        $data['kategoris'] = $kategoris;
        if ($id!=0) {
          $data['barangs'] = Barang::whereNull('deleted_at')
                        ->where('kategoris_id','=',$id)
                        ;
        $data['barangBaru'] = $data['barangs']->take(4)->get();
        $data['barangs'] = $data['barangs']->paginate(16);
        }
        else {
          $data['barangs'] = Barang::whereNull('deleted_at')
                        ->orderBy('created_at','desc')
                        ;
          $data['barangBaru'] = $data['barangs']->take(4)->get();
          $data['barangs'] = $data['barangs']->paginate(16);
        }
        $groups = DB::table('groups')->whereNull('deleted_at')->select('id','nama as text')->orderBy('counter','desc')->get();
        $data['groups'] = $groups;
        $data['id'] = $id; //tambahroy
        $gg = [];
        foreach ($cart->projects as $key => $value) {
          $gg[] = (['id'=>$value['id'],'text'=>$value['text']]);
        }
        $data['projects'] = json_encode($gg);
        $data['privilege'] = explode('-',CRUDBooster::myPrivilegeName());
        // dd($data['privilege']);
        // $data['privilege'][1] = (int)$data['privilege'][1];
        $this->cbView('customView.materialrequest_add_barang',$data);
        
      }

      public function addCart(){
        // session()->forget('cart');
        $request = request();
        // dd($request);
        if(!isset($request->projects)) {
          response()->json(['berhasil'=>0]);
        }
        $projects = $request->projects;
        $cart = session()->has('cart') ? session()->get('cart') : null ;
        if(isset($request->groups)) { //byGroups
          $barangs = DB::table('groupbys')->join('barangs','barangs.id','=','groupbys.barang_id') 
                  ->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
                  ->where('groupbys.group_id','=',$request->groups) 
                  ->whereNull('barangs.deleted_at') 
                  ->whereNull('groupbys.deleted_at') 
                  ->select('barangs.*','satuans.nama as satuan')
                  ->get() 
                  ; 
                Group::find($request->groups)->increment('counter');
                  // dd($barangs);
        foreach ($barangs as $key => $value) { 
              $id = $value->id; 
          if($cart){
            if(isset($cart->projects[$projects]['items']) && array_key_exists($id, $cart->projects[$projects]['items'])) {//barang sudah, tambahkan jumlahnya
              $jmlh = $cart->projects[$projects]['items'][$id]['jmlh']+(int)$request->qntys[$id];
              $cart->projects[$projects]['items'][$id]['jmlh'] = $jmlh;
              continue;
            }
          }
          $cart->projects[$projects]['items'][$id]=['item'=>$value,'jmlh'=>(int)$request->qntys[$id]];
        }
        session()->put('cart',$cart);
          // CRUDBooster::redirect( route('subdept.mr.home'),'');
          return response()->json(['berhasil'=>1,'jmlh'=>$request->qnty]);
        }
        else{
          $id = $request->id;
          if(isset($request->keterangan)){
            $keterangan = $request->keterangan;
          }
          else if(isset($cart->projects[$projects]['items'][$id]['keterangan'])){
            $keterangan = $cart->projects[$projects]['items'][$id]['keterangan'];
          }
          else{
            $keterangan = '';
          }
          if($cart && $request->update==0 && isset($cart->projects[$projects]['items'])){
            if(array_key_exists($id, $cart->projects[$projects]['items'])) {//barang sudah, tambahkan jumlahnya
              // return response()->json(['berhasil'=>0]);
              if(isset($request->keterangan)){
                $cart->projects[$projects]['items'][$id]['keterangan'] = $keterangan;
              }
              $jmlh = $cart->projects[$projects]['items'][$id]['jmlh']+(int)$request->qnty;
              $cart->projects[$projects]['items'][$id]['jmlh'] = $jmlh;
              return response()->json(['berhasil'=>1,'jmlh'=>$request->qnty]);
            }
          }
          //belum ada & update
          $barang = DB::table('barangs')->leftJoin('satuans','satuans.id','=','barangs.satuans_id')
          ->select('barangs.*','satuans.nama as satuan')->where('barangs.id',$id)->first();
          $cart->projects[$projects]['items'][$id] = ['item'=>$barang,'jmlh'=>(int)$request->qnty,'keterangan'=>$keterangan];
          session()->put('cart',$cart);
          // $jmlhCart = count($cart->projects[$projects]['items']);
          if($request->update==1) $berhasil=2;
          else $berhasil =1;
          return response()->json(['berhasil'=>$berhasil,'jmlh'=>$request->qnty,'ktr'=>$keterangan]);
          // return response()->json(['cart'=>$cart]);
        }
      }

      public function getCart($id=0){
        $cart = session()->has('cart') ? session()->get('cart') : CRUDBooster::redirect( route('subdept.mr.mainpage'),'');
        if(count($cart->projects[$id]['items'])==0) CRUDBooster::redirect( route('subdept.mr.home'),'');
        $data = [];
        $barangs_id = [];
        $data['projects'] = $cart->projects[$id];
        $data['barangs'] = $cart->projects[$id]['items'];
        if(isset($cart->projects[$id]['subdept'])) {
          $subdepts = (['id'=>$cart->projects[$id]['subdept']['id'],'nama'=>$cart->projects[$id]['subdept']['nama']]);
        }
        else{
          $user_id = CRUDBooster::myId();
          $table_subdept = "wellhos_subdepts";
          $dataz = Assign_user::join('wellhos_subdepts','wellhos_subdepts.id','=','assign_users.table_id')
                            ->where('assign_users.user_id','=',$user_id)
                            ->where('assign_users.table_name','=',$table_subdept)
                            ->whereNull('assign_users.deleted_at')
                            ->select('wellhos_subdepts.id as subdept_id','wellhos_subdepts.nama as subdept_nama')
                            ->first()
                            // ->toArray()
                            ;
        // dd($data);
          $subdepts = (['id'=>$dataz->subdept_id,'nama'=>$dataz->subdept_nama]);
        }
        if(isset($cart->projects[$id]['detail']['cnc'])){
          $d1 = new DateTime($cart->projects[$id]['detail']['tgl_kontrak']);
          $d2 = new DateTime(date("Y-m-d"));
          $lamaKontrak=($d1->diff($d2)->m + ($d1->diff($d2)->y*12)); 
          $limitTotal = (((int)$cart->projects[$id]['detail']['cnc'] * $lamaKontrak)*75/100);
          $limitBulan = $limitTotal/6;
        }
        else{
          $limitTotal = (int)$cart->projects[$id]['detail']['total'];
          $limitBulan = $limitTotal/6; 
        }
        $data['limitBulan'] = $limitBulan;
        $data['limitTotal'] = $limitTotal;
        $data['subdept'] = $subdepts;
        // dd($subdepts);
        $this->cbView('customView.materialrequest_getCart',$data);
      }

      public function delCart(){
        $request = request();
        $projects = $request->projects;
        // return response()->json($request);
        $cart = session()->has('cart') ? session()->get('cart') : null ;
        if($cart){
          if(!array_key_exists($projects, $cart->projects)) return response()->json(['berhasil'=>0]);
          else{
            if(isset($request->id)){
              $id = $request->id;
              unset($cart->projects[$projects]['items'][$id]);
            }
            else unset($cart->projects[$projects]);
            session()->put('cart',$cart);
            if(count(session()->get('cart')->projects)==0) session()->forget('cart');
            return response()->json(['berhasil'=>1]);
          }
        }
        return response()->json(['berhasil'=>0]);
      }

      public function postCart(){
        $request = request();
        $user_id = CRUDBooster::myId();
        $projects = $request->projects;
        $cart = session()->get('cart');
        $periode = $cart->projects[$projects]['periode'];
        // $subdept_id = $request->subdept_id;
        $detilSubdept = $cart->projects[$projects]['subdept']['nama'];
        // dd($detilSubdept);
        $subdept= Wellhos_subdept::updateOrCreate(
          ['nama'=>$detilSubdept,
          // 'pic'=>$detilSubdept[0]
          ],
          ['nama'=>$detilSubdept,//'pic'=>$detilSubdept[0],
          'phone'=>'0000000',
          'keterangan'=>'',
          'wellhos_departments_id'=>$cart->projects[$projects]['subdept']['dept_id'],]
        );
        // dd($subdepts);
        $subdept_id = $subdept->id;
        // dd($subdept->id);
        $masterProject_baru = Master_project::updateOrCreate(
          [
            'nama'=>$cart->projects[$projects]['detail']['id_client'],
          ],
          [
            'alamat'=>$cart->projects[$projects]['detail']['area_name'],
            'no telp'=>$cart->projects[$projects]['detail']['tlp_pic']
          ]
        );
        $namaProject = explode("|", $cart->projects[$projects]['text']);
        $project_baru = Project::updateOrCreate(
          [
            'nama'=> rtrim($namaProject[0])
          ]
          ,[
            // 'id'=>$projects,
            'area'=>rtrim($namaProject[1]),
            'total'=>$request->limitTotal,
            // 'saldo'=>$request->limitTotal-$request->total,
            'pic'=> $cart->projects[$projects]['detail']['pic_service'],
            'master_projects_id' => $masterProject_baru->id,
          ]
        );
        if($project_baru->saldo==null){
          $project_baru->update(['saldo'=>$request->limitTotal-$request->total]);
        }else{
          $project_baru->update(['saldo'=>(int)$project_baru->saldo-$request->total]);
        }
        $mr = new Mr_subdept;
        // $mr->nomr = $nomor;
        $mr->gudang = $request->gudangs;
        $mr->tanggal = date('Y-m-d');
        $mr->projects_id = $project_baru->id;
        $mr->periode_mrs_id = $periode;
        $mr->approval = 'belum';
        $mr->total = $request->total;
        if($subdept_id) $mr->wellhos_subdepts_id = $subdept_id;
        $mr->user_id = $user_id;
        $mr->save();
        $nomor = 'MR-'.date('ym').'-'.$mr->id;
        $mr->update(['nomr'=>$nomor]);
        $array = [];
        $array_log = [];
        foreach ($cart->projects[$projects]['items'] as $key => $value) {
          $isi = new Isi_mr_subdept;
          $isi->barangs_id = $key;
          $isi->jumlah = $value['jmlh'];
          $isi->mr_subdepts_id = $mr->id;
          $isi->created_at = date('Y-m-d H:i:s');
          $isi->total = $value['jmlh'] * $value['item']->avgprice;
          $isi->keterangan = $value['keterangan'];
          $array[] = $isi->toArray();

        $log = new Log_mr;
        $log->user = $user_id;
        $log->barang_id = $key;
        $log->mr_id = $mr->id;
        $log->created_at = date('Y-m-d H:i:s');
        $log->status_action = "MR by user";
        $array_log[] = $log->toArray();
        }
      // dd($array_log);
        Isi_mr_subdept::insert($array);
        Log_mr::insert($array_log);
        // dd($array);
        unset( $cart->projects[$projects] );
        if( count($cart->projects)==0 ) session()->forget('cart');
        if(session()->has('cart')) CRUDBooster::redirect( route('subdept.mr.home'),'mr berhasil dibuat','success');
        else CRUDBooster::redirect( route('subdept.mr.mainpage'),'mr berhasil dibuat','success');
      }

      public function cariBarang(){
        $request = request()->cari;
        $hasil = DB::table('barangs')->join('satuans','satuans.id','=','barangs.satuans_id')
                      ->where('barangs.nama','like', '%'.$request.'%')
                      ->orWhere('barangs.kode','like', '%'.$request.'%')
                      ->whereNull('barangs.deleted_at')
                      ->select('barangs.*','satuans.nama as satuan')
                      ->limit(20)
                      ->get()
                      ;
        $count = count($hasil);
        return response()->json(['hasil'=>$hasil,'count'=>$count]);
      }


      public function getGroupBarangs($id){
        $data = DB::table('groupbys')->join('barangs','barangs.id','=','groupbys.barang_id')
                  ->where('groupbys.group_id','=',$id)
                  ->whereNull('barangs.deleted_at')
                  ->whereNull('groupbys.deleted_at')
                  ->select('barangs.nama','barangs.id')
                  ->get()
                  ;
        return response()->json(['data'=>$data]);
      }

      public function ajax_getBarang(){
        $request = request()->term;
        $data = DB::table('barangs')->whereNull('deleted_at')
                      ->where('nama','like', '%'.$request.'%')
                      ->orWhere('kode','like', '%'.$request.'%')
                      ->select('id','nama as text')
                      ->get()
                      ;
        return response()->json($data);
      }

      public function ajax_getListBarang() {
        if(session()->has('cart')) $cart = session()->get('cart');
        else return response()->json(['berhasil'=>0]);
        $id = request()->id;
        if(!isset($cart->projects[$id]['items']))return response()->json(['berhasil'=>0]);
        $gg = $cart->projects[$id]['items'];
        $dat = [];
        $no=1;
        foreach ($gg as $key => $value) {
          $dat[] = [
            $no,
            $value['item']->nama,
            $value['jmlh'],
            $value['keterangan'],
            '<button class="btn btn-warning btn-xs btnEditJumlah" style="margin-left:5px;" brg_id="'.$value['item']->id.'" barang_harga="'.$value['item']->avgprice.'" satuan="'.$value['item']->satuan.'" barang_stok="'.$value['item']->akumstok.'" data-togle="tooltip" title="Edit Barang"><i class="fa fa-pencil-square"></i></button>
            <button class="btn btn-danger btn-xs btnHapusBarangCart"><i class="fa fa-trash-o"></i></button>'
          ];
          $no++;
        }
        // $barangs = DB::table('barangs')->whereIn('id',$dat)->select('id','nama')->get();
        return response()->json($dat);
      }

      public function tukarBarang(){
        $request = request();
        $id = $request->barangs;
        $cart = session()->get('cart');
        unset($cart->items[$id]);
        $barang = DB::table('barangs')->find($request->tukar);
        $cart->items[$request->tukar] = ['item'=>$barang,'jmlh'=>(int)$request->qnty];
        session()->put('cart',$cart);
        //session()->put('cart',$cart);
        $this->getCart();
      }

      public function getUser() {
        $data = [];
        $data['periode'] = DB::table('periode_mrs')->whereNull('deleted_at')
                              ->select('id','judul as text')
                              ->get();
        $data['privilege'] = explode('-',CRUDBooster::myPrivilegeName());
        $data['departments']=[];
        $data['subdepts']=[];
        // dd($data['privilege']);
        if(strtolower($data['privilege'][0])!='mr' && CRUDBooster::isSuperadmin()==false){
          return redirect('/');
        }
        if(CRUDBooster::isSuperadmin()==true || (int)$data['privilege'][1] == 4){
          // dd('hehe');
          $data['departments'] = DB::table('wellhos_departments')->whereNull('deleted_at')
                                          ->select('id','nama as text')->get();
                                          // dd($data['departments']);
        }
        if((int)$data['privilege'][1]<=1){
          $datauser = DB::table('assign_users')->leftJoin('wellhos_subdepts','wellhos_subdepts.id','=','assign_users.table_id')
                                            ->leftJoin('wellhos_departments','wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
                                            ->where('assign_users.user_id','=',CRUDBooster::myid())
                                            ->where('assign_users.table_name','=','wellhos_subdepts')
                                            ->whereNull('assign_users.deleted_at')
                                            ->select('wellhos_departments.id as id_dept','wellhos_subdepts.id as id_subdept','wellhos_departments.nama as nama_dept','wellhos_subdepts.nama as nama_subdept')
                                            ->get();
                                            // dd($datauser);
          // $data['subdepts'][]
          // $data['departments'][]
          // foreach ($datauser as $key => $value) {
          //   // $tampungSub = ['id'=>$value->id_subdept,'text'=>$value->nama_subdept];
          //   $tampungDep = ['id'=>$value->id_dept,'text'=>$value->nama_dept];
          //   if(!in_array($tampungDep, $data['departments'])) $data['departments'][] = ['id'=>$value->id_dept,'text'=>$value->nama_dept];
          //   // $data['subdepts'][] = ['id'=>$value->id_subdept,'text'=>$value->nama_subdept];
          // }
          $data['subdepts'][] = ['id'=>$datauser[0]->id_subdept,'text'=>$datauser[0]->nama_subdept];
          $data['departments'][] = ['id'=>$datauser[0]->id_dept,'text'=>$datauser[0]->nama_dept];
          $data['subdepts'] = json_encode($data['subdepts']);
          $data['departments'] = json_encode($data['departments']);
        }
        else if((int)$data['privilege'][1]<4 && (int)$data['privilege'][1]>0){
          $datauser = DB::table('assign_users')->leftJoin('wellhos_departments','wellhos_departments.id','=','assign_users.table_id')
                                            ->leftJoin('wellhos_subdepts','wellhos_subdepts.wellhos_departments_id','=','wellhos_departments.id')
                                            ->where('assign_users.user_id','=',CRUDBooster::myid())
                                            ->whereNull('assign_users.deleted_at')
                                            // ->where('assign_users.table_name','=','wellhos_departments')
                                            ->select('wellhos_departments.id as id_dept','wellhos_subdepts.id as id_subdept','wellhos_departments.nama as nama_dept','wellhos_subdepts.nama as nama_subdept')
                                            ->get();
                                            // dd($datauser);
          $data['subdepts'] = [];
          $data['departments'] = [];
          foreach ($datauser as $key => $value) {
            $data['subdepts'][] = ['id'=>$value->id_subdept,'text'=>$value->nama_subdept];
            $data['departments'][] = ['id'=>$value->id_dept,'text'=>$value->nama_dept];
          }
          $data['subdepts'] = json_encode($data['subdepts']);
          $data['departments'] = json_encode($data['departments']);
        }
        $data['privilege'] = json_encode($data['privilege']);
        $this->cbView('customView.materialrequest_getUser',$data);
      }

      public function history_mr(){
        $data = [];
        $data['periode'] = DB::table('periode_mrs')->whereNull('deleted_at')
                              ->select('id','judul as text')
                              ->get();
        $this->cbView('customView.materialrequest_getUser_history_mr',$data);
      }

      public function historyUpdateJumlahBarang(){
        $request = request();
        $isi = Isi_mr_subdept::where('id',$request->id)->first();
        if(strlen($request->keterangan)==0){
          $request->keterangan = $isi->keterangan;
        }
        if($isi->total==0){
          $hrga = 0;
        }
        else{
          $hrga = $isi->total / $isi->jumlah;
        }
        $total = $hrga * $request->qnty;
        $tmr = $isi->mr_subdept->total;
        $tmr += $total;
        $tmr -=$isi->total;
        $saldo = $isi->mr_subdept->project->saldo + $isi->mr_subdept->total - $tmr;
        //tambahkan hitung ulang nominal budget

        DB::table('revisi_mr_subdepts')->insert(['created_at'=>now(),
                                                'isi_mr_id'=>$isi->id,
                                                'jumlah_lama'=>$isi->jumlah,
                                                'jumlah_baru'=>$request->qnty,
                                                'keterangan_lama'=>$isi->keterangan,
                                                'keterangan_baru'=>$request->keterangan,
                                                'user_id'=>CRUDBooster::myId(),
                                                ]);
        $isi->update(['jumlah'=>(int)$request->qnty,'keterangan'=>$request->keterangan,'total'=>$total]);
        $isi->mr_subdept->update(['total'=>$tmr]);
        $isi->mr_subdept->project->update(['saldo'=>$saldo]);
        $totaltampil = 'Rp '.number_format($tmr);
        return response()->json(['request'=>$request,'total'=>$totaltampil]);
      }

      public function historyDelBarang(){
        $request = request();
        Isi_mr_subdept::where('id',$request->id)->delete();
        return response()->json($data);
      }

      public function requestBarang(){
        $request = request();
        // dd(Request::file('gambar'));
        // dd($request);
        if($request->file('gambar')){
          $file = Request::file('gambar');
          if($file->getError()==1){
            // CRUDBooster::redirect( route('subdept.mr.home'),'Gagal upload gambar','warning');
            return response()->json(['berhasil'=>0]);
          }
          $ext  = $file->getClientOriginalExtension();
          $filename = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
          $uploadMaxSize = 2097152; //2mb ; diambil dari https://www.gbmb.org/mb-to-bytes
          $filesize = $file->getClientSize();
          if($filesize > $uploadMaxSize) {
            CRUDBooster::redirect( route('subdept.mr.home'),'ukuran gambar terlalu besar','warning');
          }
          $file_path = 'uploads/'.CRUDBooster::myId().'/'.date('Y-m');          
          // dd($file);
          Storage::makeDirectory($file_path);
          $lokasi = Storage::disk('local')->putFile($file_path,$file);

          Request_barang::insert([
                  'user_id'=>CRUDBooster::myId(),
                  'nama'=>$request->nama,
                  'keterangan'=>$request->keterangan,
                  'created_at' => date('Y-m-d H:i:s'),
                  'gambar' => $lokasi,
                  ]);
          // CRUDBooster::redirect( route('subdept.mr.home'),'Usulan barang berhasil dibuat','success');       
          return response()->json(['berhasil'=>1]);
        } 
        else {
          // CRUDBooster::redirect( route('subdept.mr.home'),'Pilih Gambar terlebih dahulu','warning');
          return response()->json(['berhasil'=>0]);
        }
      }

      public function getSubdept($id){
        $request = request();
        if(strtolower($request->dept) != 'opd' || isset($request->nogetgis)){
          $kategoris = DB::table('wellhos_subdepts')->whereNull('deleted_at')
                          ->where('wellhos_departments_id','=',$id)
                          ->select(
                            'id as id',
                            'nama as text'
                            )
                          ->get();
        }
        else{
          $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/asm.php');
          $data = json_decode($json,true);
          $kategoris = [];
          foreach ($data as $key => $value) {
          $kategoris[] = ([
            'id' => $value['id_asm'],
            'text' => $value['nama_asm'].' | '.$value['area'],
          ]);
        }
        }
      return json_encode($kategoris);
      }

      public function ajaxGetProject(){
        $request = request();
        $projects = [];
        if(strtolower(CRUDBooster::myPrivilegeName())=='mr-0'){//ada data di assign user x project dan privileges = 0;
          $periodeMrBelumApprove = DB::table('mr_subdepts')
                                // ->where('user_id',CRUDBooster::myId())
                                ->where('approval','belum')
                                ->where('periode_mrs_id',$request->periode_id)
                                ->pluck('projects_id');
          $dataz = DB::table("users_x_projects")
                      ->join('projects','projects.id','=','users_x_projects.projects_id')
                      ->where('users_x_projects.user_id','=',CRUDBooster::myId())
                      ->whereNotIn('users_x_projects.projects_id',$periodeMrBelumApprove)
                      ->select('projects.id','projects.nama','projects.area')
                      ->get();
          foreach ($dataz as $key => $value) {
              $projects[] = ([
                'id' => $value->id,
                'text' => $value->nama.' | '.$value->area,
              ]); 
            }
        }
        else
        {
          if($request->dept=='opd') {
            $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/list_project_asm.php?asm='.$request->id);
            $dataz = json_decode($json,true);
            // dd($dataz);
            $periodeMrBelumApprove = DB::table('mr_subdepts')
                                      ->join('projects','projects.id','=','mr_subdepts.projects_id')
                                      // ->where('mr_subdepts.user_id',CRUDBooster::myId())
                                      ->where('mr_subdepts.approval','belum')
                                      ->where('periode_mrs_id',$request->periode_id)
                                      ->pluck('projects.nama')
                                      ->toArray();
            foreach ($dataz as $key => $value) {
              if(in_array($value['rekening'], $periodeMrBelumApprove)) continue;
              $projects[] = ([
                'id' => $value['id'],
                'text' => $value['rekening'].' | '.$value['area_name'],
              ]);
            }
          }
          else {
            //check apakah exist di table pivot
            $periodeMrBelumApprove = DB::table('mr_subdepts')
                                      // ->where('user_id',CRUDBooster::myId())
                                      ->where('approval','belum')
                                      ->where('periode_mrs_id',$request->periode_id)
                                      ->pluck('projects_id');
            $dataz = DB::table('projects')->leftJoin('master_projects','master_projects.id','=','projects.master_projects_id')
                                          ->whereNull('projects.deleted_at');

            /*$check_exist_table_pivot_user_project = DB::table("users_x_projects")->where('user_id','=',CRUDBooster::myId())->pluck('projects_id');
            // dd($check_exist_table_pivot_user_project);
            if(count($check_exist_table_pivot_user_project)>0){
              $dataz = $dataz->whereIn('projects.id',$check_exist_table_pivot_user_project);
            }*/

            $dataz = $dataz->whereNotIn('projects.id',$periodeMrBelumApprove)
                              ->select('projects.id','projects.nama','projects.area' ,'master_projects.alamat')
                              ->get()
                              ;
            // $json = file_get_contents('http://api.gis.gemilang.co.id/accounting/periode_project.php');
            // dd($periodeMrBelumApprove);
            // dd($dataz);
            foreach ($dataz as $key => $value) {
              $projects[] = ([
                'id' => $value->id,
                'text' => $value->nama.' | '.$value->area,
              ]); 
            }
          }
        }
        return response()->json($projects);
      }

      public function dt_history(){
        $dataz = Mr_subdept::whereNull('deleted_at')
                      ->where('mr_subdepts.user_id','=',CRUDBooster::myId())
                      ->get()
                      ;
      $dd = $dataz[0]->project->nama;
        dd($dd);
      }

      public function updateStatus(){
        $request = request();
        $data = DB::table('pengiriman_barangs')->where('jenis','=','mr')
                      ->where('global_id','=',$request->mr_id)
                      ->latest()
                      ->update(['status'=>$request->status])
                      ;
        return response()->json($request->status);
      }

      public function laporan(){
        $data = array();
        $this->cbView('customView.laporanmr',compact('data'));        

      }
      public function laporanperiodemr(){
        $data = array();
        $data['periodermr'] = DB::table('periode_mrs')->leftJoin('mr_subdepts','mr_subdepts.periode_mrs_id','=','periode_mrs.id')
        ->select('Periode_mrs.judul',DB::raw('sum(mr_subdepts.total) as total'))->groupBy('periode_mrs.judul')->whereNull('periode_mrs.deleted_at')->get();
        foreach ($data['periodermr'] as $d) {
          $data['judulperiode'][] = $d->judul;
          $data['nominalperiode'][] = $d->total;
        }
        
        $this->cbView('customView.laporanmrperiode',compact('data'));        

      }

      public function postApprove(){
        $request = request();
        DB::table('mr_subdepts')->where('id',$request->mr_id)->update(['approval'=>'oke','updated_at'=>now()]);
        return response()->json(['berhasil'=>1,'request'=>$request->mr_id]);
      }

      public function postSelesai(){
        $request = request();
        DB::table('mr_subdepts')->where('id',$request->mr_id)->update(['approval'=>'Selesai','updated_at'=>now(),'selesai'=>1]);
        return response()->json(['berhasil'=>1,'request'=>$request->mr_id]);
      }  

      public function deleteMR(){
        $request = request();
        DB::table('mr_subdepts')->where('id',$request->mr_id)->update(['deleted_at'=>now()]);
        return response()->json(['berhasil'=>1]);
      }

      public function ajax_getMRs(){
        $barangs=[];
        $request = request();
        // dd($request);
        if(isset($request->periode_mrs_id)) {
          $dataz = Mr_subdept::whereNull('deleted_at')
                      // ->leftJoin('cms_users','cms_users.id','mr_subdepts.user_id')
                      ->where('periode_mrs_id','=',$request->periode_mrs_id)
                      // ->get()
                      ;
                      // dd($dataz);
          if(isset($request->subdepts)){
            // $ifke = $request->subdepts;
            if($request->subdepts==0){
              if($request->lvl>1){
                $subdepsku = DB::table('wellhos_subdepts')
                              ->leftJoin('wellhos_departments','wellhos_departments.id','=','wellhos_subdepts.wellhos_departments_id')
                              ->leftJoin('assign_users','wellhos_departments.id','=','assign_users.table_id')
                              ->where('assign_users.user_id',CRUDBooster::myId())
                              ->pluck('wellhos_subdepts.id')
                              // ->get()
                              ->toArray()
                              ;
                              // dd($subdepsku);
                $dataz = $dataz->whereIn('mr_subdepts.wellhos_subdepts_id',$subdepsku);
                $barangs = $this->dt_barangs($request->periode_mrs_id,$subdepsku,0);
              }
            }
            else
            {
              $dataz = $dataz->where('mr_subdepts.wellhos_subdepts_id','=',$request->subdepts)
                            // ->get()
                            ;
                            // dd($dataz);
            }
            if($request->lvl=="1" || $request->lvl =="0"){
              $barangs = $this->dt_barangs($request->periode_mrs_id,$request->subdepts,CRUDBooster::myid());
              if($request->lvl =="0"){
                $dataz = $dataz->where('mr_subdepts.user_id','=',CRUDBooster::myId());
              }
              else{
                $tableidku = DB::table('assign_users')->whereNull('deleted_at')->where('user_id',CRUDBooster::myId())->first()->table_id;
                // dd($tableidku);
                $idusers = DB::table('cms_users')
                              ->leftJoin('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
                              ->leftJoin('assign_users','assign_users.user_id','=','cms_users.id')
                              ->where('assign_users.table_name','wellhos_subdepts')
                              ->where('assign_users.table_id',$tableidku)
                              ->whereNull('assign_users.deleted_at')
                              ->pluck('cms_users.id');
                $idusers->push(CRUDBooster::myId());
                $dataz = $dataz->whereIn('mr_subdepts.user_id',$idusers);
              }
            }
            else {
              $barangs = $this->dt_barangs($request->periode_mrs_id,$request->subdepts);

            }
          }
          else{
            $ifke = 'this?';
            if($request->lvl=="1" || $request->lvl =="0"){
              $barangs = $this->dt_barangs($request->periode_mrs_id,0,CRUDBooster::myId());
              $dataz = $dataz->where('mr_subdepts.user_id','=',CRUDBooster::myId());
              // $ifke = 'gege';
            }
            else{
               $barangs = $this->dt_barangs($request->periode_mrs_id);
               // $ifke = 'ini?';
            }
          }
          $dataz = $dataz->select('mr_subdepts.*',
                                  // 'cms_users.*',
                                  'mr_subdepts.id as id');
          $dataz = $dataz->get();
          // dd($dataz);
          $i=1;
          $data = []; 
          // $jmlhku = 0;

          foreach($dataz as $d){
            $action2='';
            $action3='';
            $action = '<button type="button" class="btn btn-success btn-xs mr_clickable" mr_id="'.$d->id.'" data-togle="tooltip" title="List Barang"><i class="fa fa-bars"></i></button>';

            if($d->periode_mrs->selesai==0){
              if(strtolower($d->approval)=='belum'){
                if($request->lvl!='0'){
                  if($request->lvl=='1' && strtolower($d->user->privilege->name)=='mr-0'){//check pakah mr yg diajukan mr dibwh level 0 dan user sekarang lv 1
                    // dd('oke lv1');
                    $action2= ' <button type="button" class="btn btn-info btn-xs btnApprove" mr_id="'.$d->id.'" data-togle="tooltip" title="Approve MR"><i class="fa fa-check"></i></button>';
                    $jmlhku++;
                  }
                  if($request->lvl!='1'){
                    $action2= ' <button type="button" class="btn btn-info btn-xs btnApprove" mr_id="'.$d->id.'" data-togle="tooltip" title="Approve MR"><i class="fa fa-check"></i></button>';
                  }
                }
                $action2.= ' <button type="button" class="btn btn-danger btn-xs mr_delete" mr_id="'.$d->id.'" data-togle="tooltip" title="Hapus MR"><i class="fa fa-times"></i></button>';
              }
              else if($request->lvl=='4' || CRUDBOOSTER::isSuperadmin()==true){
                $action2.= ' <button type="button" class="btn btn-danger btn-xs mr_delete" mr_id="'.$d->id.'" data-togle="tooltip" title="Hapus MR"><i class="fa fa-times"></i></button>'; 
              }
            }
            // dd($d->pengiriman_barang);
            if(count($d->pengiriman_barang)>0){
              $jmlhkirim = 0;
              $persen = 0;
              $jmlhmr = 0;
              $dataku = [];
              $gg = [];
              // $action .= ' <button type="button" class="btn btn-info btn-xs mr_edit_kirim" mr_id="'.$d->id.'"><i class="fa fa-pencil"></i></button>';
              foreach ($d->isi_mr as $k2 => $isi) { //$d == MR
                // dd($isi->detail_pengiriman_barang);
                foreach ($isi->detail_pengiriman_barang->where('barang_id','=',$isi->barangs_id) as $k3 => $vv) {
                  // dd($vv);
                  $jmlhkirim += $vv->jumlah;
                }
                $dataku[] = ['jmlhkirim'=>$vv->jumlah,'jmlhmr'=>$isi->jumlah,'barang'=>$isi->barang->nama];
                $jmlhmr += $isi->jumlah; 
                // dd($jmlhmr);
              }
              $persen = $jmlhkirim / $jmlhmr *100;
              // dd($d->isi_mr);
              $kirim = number_format($persen,2).' %';
              // else{
              //   $kirim = $d->pengiriman_barang->last()->status;
              // }
              $action .= '<button type="button" class="btn btn-warning btn-xs mr_lihat_kirim" data-togle="tooltip" title="List Pengiriman" style="margin-left:5px;"><i class="fa fa-truck"></i></button>';
              if($persen == 100){
                //tombol tandai selesai
                if((CRUDBooster::myId() == $d->user_id || CRUDBooster::isSuperadmin()) && strtolower($d->approval)!='selesai'){
                  $action3 = '<button type="button" class="btn btn-danger btn-xs mr_selesai" data-togle="tooltip" title="Tandai MR Selesai"><i class="fa fa-check"></i></button>';
                }
              }
            }
            else {
              $kirim = 'belum kirim';
            }
            $data[] = [
                'uruts'=>$i,
                'nomr'=>$d->nomr,
                'tanggal'=>$d->tanggal,
                'user'=>$d->user->name,
                'projects'=>$d->project->nama.' | '.$d->project->area,
                'approval'=>$d->approval,
                'total'=>'Rp '.number_format($d->total),
                'action1'=>$action,
                'action2'=>$action2,
                'action3'=>$action3,
                'status_kirim' =>$kirim,
                'mr_id'=>$d->id,
                'saldo'=>number_format($d->project->total /6)
                ];
            $i++;
          }
          // dd($jmlhku);
          return response()->json(['mr'=>$data,'barangs'=>$barangs,'if_ke'=>$ifke]);
        }
        else {
          $hasil = DB::table('isi_mr_subdepts')->join('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
                      ->join('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
                      ->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
                      ->join('periode_mrs','periode_mrs.id','=','mr_subdepts.periode_mrs_id')
                      ->whereNull('isi_mr_subdepts.deleted_at')
                      ->where('isi_mr_subdepts.mr_subdepts_id','=',$request->mr_subdepts_id)
                      ->select('isi_mr_subdepts.id','barangs.nama','isi_mr_subdepts.jumlah','isi_mr_subdepts.dikirim','mr_subdepts.approval','mr_subdepts.approval','periode_mrs.selesai as periode_selesai','isi_mr_subdepts.keterangan','projects.saldo')
                      ->get()
                      ;
          $data = [];
          $revisi = [];
          $ids=[];
          $privilegeku = explode('-',CRUDBooster::myPrivilegeName());
          // dd((int)$privilegeku[1]);
          // dd($request->unedit);
          foreach ($hasil as $key => $d) {
            $action ='';
            if($d->periode_selesai != 1 && $d->approval =='belum' && ((int)$privilegeku[1]>=2 ) || CRUDBooster::isSuperadmin() && $request->unedit =='0'){
              $action .= '<button type="button" class="btn btn-xs btn-warning btneditjumlahbarang" data-togle="tooltip" title="Revisi Barang"><i class="fa fa-pencil"></i></button>';
            }
            $data[] = ['no'=>'',
                      'barang'=>$d->nama,
                      'jumlah'=>$d->jumlah,
                      'dikirim'=>$d->dikirim,
                      'keterangan'=>$d->keterangan,
                      'action'=>$action,
                      'saldo'=>number_format($d->saldo),
                      'id'=>$d->id];
            $ids[] = $d->id;
          }
          $hasil2 = Revisi_mr_subdept::whereIn('isi_mr_id',$ids)->get();
          foreach ($hasil2 as $key => $d) {
            $revisi[] = ['no'=>'',
                      'barang'=>$d->isi_mr_subdept->barang->nama,
                      'tanggal'=>date('d-M-Y',strtotime($d->created_at)),
                      'jumlah_lama'=>$d->jumlah_lama,
                      'jumlah_baru'=>$d->jumlah_baru,
                      'keterangan_lama'=>$d->keterangan_lama,
                      'keterangan_baru'=>$d->keterangan_baru,
                      'user_id'=>$d->user->name,
                      'id'=>$d->id];
          }
        }
        return response()->json(['data'=>$data,'revisi'=>$revisi]);
      }

      public function dt_barangs($periode_id,$subdepts=0,$user_id=0){
        $hasil = DB::table('mr_subdepts')
              ->Join('isi_mr_subdepts','isi_mr_subdepts.mr_subdepts_id','=','mr_subdepts.id')
              ->Join('barangs','isi_mr_subdepts.barangs_id','=','barangs.id')
              ->Join('satuans','barangs.satuans_id','=','satuans.id')
              ->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
              ->select('barangs.nama','barangs.kode','isi_mr_subdepts.jumlah','mr_subdepts.nomr','isi_mr_subdepts.total','projects.area as area')
              ->where('mr_subdepts.periode_mrs_id',$periode_id)
              // ->whereIn('mr_subdepts.wellhos_subdepts_id',$subdepts)->get();dd($hasil)
              ;
              // dd($subdepts);
        if(is_array($subdepts)) {
          
          $hasil = $hasil->whereIn('mr_subdepts.wellhos_subdepts_id',$subdepts);
        }
        else if($subdepts>0){
          $hasil = $hasil->where('mr_subdepts.wellhos_subdepts_id',$subdepts);
        }
        if($user_id>0){
         $hasil = $hasil->where('mr_subdepts.user_id',$user_id); 
        }
        $hasil = $hasil->whereNull('mr_subdepts.deleted_at')->orderby('mr_subdepts.created_at','desc')->get();
        // dd($hasil);
        $data = [];
        foreach($hasil as $d){
          $data[] = [
                 'nama'=>$d->nama,
                 'kode'=>$d->kode,
                 'nomr'=>$d->nomr.' |'.$d->area,
                 'jumlah'=>$d->jumlah,
                 'total'=>number_format($d->total),
                 ];
         }
        //$data[0]=[1,'a'];
        //$data[1]=[2,'b'];
              // $results = [
              // "draw" => 1,
              //     "recordsTotal" => count($data),
              //     "recordsFiltered" => count($data),
              //     "data" => $data
              //     ];
        return $data;
      }

      public function getapprove($id){
        // dd($id);
        DB::table('mr_subdepts')->where('id',$id)->update(['approval'=>'oke','updated_at'=>now()]);
        CRUDBooster::redirect( CRUDBooster::adminPath('mr_subdepts36'),'Data berhasil diapprove','success');
      }

      public function getlistkirim($step=0){
        $request = request();
        $data = [];
        if($step == 1){ //list pengiriman barang
          $data = DB::table('pengiriman_barangs')->where('jenis','mr_baru')->where('global_id',$request->id)->get();
          foreach ($data as $key => $d) {
            $d->action = '<button type="button" class="btn btn-info btn-xs btn_detailkirim" data-togle="tooltip" title="List Barang Terkirim"><i class="fa fa-bars"></i>
              </button>';
            if($d->status == null){
              $d->action .= '<button type="button" class="btn btn-success btn-xs btn_approvekirim" data-togle="tooltip" title="tandai selesai kirim" style="margin-left:5px;"><i class="fa fa-check"></i></button>';
            }
          }
          return response()->json($data);
        }
        else if($step==2){ //detail pengiriman barang
          $data = Detail_Pengiriman_Barang::where('pengiriman_barang_id',$request->id)->get();
          $results = [];
          foreach ($data as $key => $d) {
            $results[] = [$d->barang->nama,$d->jumlah];
          }
          return response()->json($results); 
        }
     }

     public function kirimselesai(){
      $request = request();
      DB::table('pengiriman_barangs')->where('id',$request->id)->update(['status'=>'selesai']);
      return response()->json('selesai');
     }

     public function getProgress($id){
      $hasil = DB::table('isi_mr_subdepts')    
                  ->leftJoin('mr_subdepts','mr_subdepts.id','=','isi_mr_subdepts.mr_subdepts_id')
                  ->whereNull('isi_mr_subdepts.deleted_at')
                  ->whereNull('mr_subdepts.deleted_at')
                  ->where('mr_subdepts.id',$id)
                  ->get()
                  ;
      $dikirim = $hasil->sum('dikirim');
      $jumlah = $hasil->sum('jumlah');
      $persen = $dikirim / $jumlah * 100;
      return number_format($persen,2);
     }
} 