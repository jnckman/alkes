<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Retur_mr;
	use App\Models\Isi_retur_mr;

	class AdminReturMrsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "retur_mrs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Mr Subdepts","name"=>"mr_subdepts_id","join"=>"mr_subdepts,nomr"];
			$this->col[] = ["label"=>"User Id","name"=>"user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Nomor","name"=>"nomor"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Mr Subdepts Id','name'=>'mr_subdepts_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'mr_subdepts,id'];
			$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'user,id'];
			$this->form[] = ['label'=>'Nomor','name'=>'nomor','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Mr Subdepts Id","name"=>"mr_subdepts_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"mr_subdepts,id"];
			//$this->form[] = ["label"=>"User Id","name"=>"user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"user,id"];
			//$this->form[] = ["label"=>"Nomor","name"=>"nomor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	           // dd($query->tosql());
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function getIndex(){
			$data = [];
			$data['page_title'] = 'Retur MR';
			$this->cbView('customView.retur_mrs_index',$data);
	    }

	    public function listmr(){
	    	$data = DB::table('mr_subdepts')
	    			->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
	    			->leftJoin('periode_mrs','periode_mrs.id','=','mr_subdepts.periode_mrs_id')
	    			->leftJoin('wellhos_subdepts','wellhos_subdepts.id','=','mr_subdepts.wellhos_subdepts_id')
	    			->leftJoin('cms_users','cms_users.id','=','mr_subdepts.user_id')
	    			->whereNull('mr_subdepts.deleted_at')
	    			->where('mr_subdepts.selesai','=',1)
	    			->where('mr_subdepts.approval','!=','selesai')
	    			->select(
	    				'mr_subdepts.nomr as nomr'
	    				,'projects.nama as project'
	    				,'projects.area as area'
	    				,'periode_mrs.judul as periode'
	    				,'wellhos_subdepts.nama as subdept'
	    				,'mr_subdepts.tanggal as tanggal'
	    				,'mr_subdepts.total as total'
	    				,'cms_users.name as creator'
	    				,'mr_subdepts.approval'
	    				,'mr_subdepts.id as id'
	    			)
	    			->get()
	    			->toArray();
	    			;

	    	foreach ($data as $key => $q) {
	    		$q->action = '<button type="button" class="btn btn-xs btn_isi_mr btn-success" data-togle="tooltip" title="isi MR"><i class="fa fa-bars"></i></button>';
	    	}

	    	$results = [
              	"draw" => 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
                ];
	    	return json_encode($results);
	    }

	    public function listretur(){
	    	$data = DB::table('retur_mrs')
	    				->leftJoin('mr_subdepts','mr_subdepts.id','=','retur_mrs.mr_subdepts_id')
	    				->leftJoin('projects','projects.id','=','mr_subdepts.projects_id')
						->leftJoin('periode_mrs','periode_mrs.id','=','mr_subdepts.periode_mrs_id')
						->leftJoin('cms_users','cms_users.id','=','retur_mrs.user_id')
	    				->whereNull('retur_mrs.deleted_at')
	    				->select(
	    					'retur_mrs.nomor as noretur'
	    					,'mr_subdepts.nomr'
	    					,'projects.area'
	    					,'periode_mrs.judul as periode'
	    					,DB::raw("DATE_FORMAT(retur_mrs.created_at, '%y-%m-%d') as tanggal")
	    					,'cms_users.name as creator'
	    					,'retur_mrs.status'
	    					,'retur_mrs.id as id'
	    					,'retur_mrs.keterangan'
	    				)
	    				->get()
	    				->toArray()
	    				;

	    	foreach ($data as $key => $q) {
	    		$q->action = '<button type="button" class="btn btn-xs btn_isi_retur btn-success" data-togle="tooltip" title="isi MR"><i class="fa fa-bars"></i></button>';
	    	}

	    	$results = [
              	"draw" => 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
                ];
                
	    	return json_encode($results);
	    }

	    public function listisimr($id){
	    	$data = DB::table('isi_mr_subdepts')
	    				->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
	    				->where('isi_mr_subdepts.mr_subdepts_id',$id)
	    				->whereNull('isi_mr_subdepts.deleted_at')
	    				->select(
	    					'barangs.nama as barang'
	    					,'isi_mr_subdepts.jumlah as jumlah'
	    					,'isi_mr_subdepts.keterangan as keterangan'
	    					,'isi_mr_subdepts.id'
	    					)
	    				->get()
	    				->toArray()
	    				;
	    	$results = [
              	"draw" => 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
                ];
	    	return json_encode($results);
	    }

	    public function listisiretur($id){
	    	$data = DB::table('isi_retur_mrs')
	    				->leftJoin('isi_mr_subdepts','isi_mr_subdepts.id','=','isi_retur_mrs.isi_mr_subdepts_id')
	    				->leftJoin('barangs','barangs.id','=','isi_mr_subdepts.barangs_id')
	    				->where('isi_retur_mrs.retur_mrs_id',$id)
	    				->whereNull('isi_retur_mrs.deleted_at')
	    				->select(
	    					'barangs.nama as barang'
	    					,'isi_retur_mrs.jumlah as jumlah'
	    					)
	    				->get()
	    				->toArray()
	    				;
	    	$results = [
              	"draw" => 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
                ];
	    	return json_encode($results);
	    }

	    public function insert_retur(){
	    	$request = request();
	    	$input= [];

	    	$retur = DB::table('retur_mrs')->insertGetId([
					    		'created_at'=>date('Y-m-d H:i:s'),
	    						'mr_subdepts_id'=>$request->mr_subdepts_id,
	    						'user_id'=>CRUDBooster::myId(),
	    						'status'=> 'new',
	    						'keterangan'=>$request->ketRetur,
	    					]);
	    	$nomor = 'RMR-'.date('ym').'-'.$retur;
	    	DB::table('retur_mrs')->where('id','=',$retur)->update(['nomor'=>$nomor,]);
	    	foreach ($request->input as $key => $v) {
	    		if($v>0){
		    		$array_insert_isi_retur_mr[] = [
		    								'created_at'=>date('Y-m-d H:i:s'),
	    									'retur_mrs_id' => $retur,
	    									'isi_mr_subdepts_id'=>$key,
	    									'jumlah'=>$v,
	    									];
	    		}
	    	}

	    	$isi_returmr = DB::table('isi_retur_mrs')->insert($array_insert_isi_retur_mr);
	    	return response()->json($request);
	    }
}