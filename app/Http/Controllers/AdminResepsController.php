<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Schema;
	use App\Models\Resep;
	use App\Models\Barang_gudang;
	use App\Models\Listbahan;
	use App\Models\Barang;
	use App\Models\Groupby;

	class AdminResepsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "reseps";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"Barangs","name"=>"barangs_id","join"=>"barangs,nama"];
			$this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];
			$this->col[] = ["label"=>"Biaya Proses","name"=>"biaya_proses"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Barang','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Biaya Proses','name'=>'biaya_proses','type'=>'number','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			//$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Biaya_proses','name'=>'biaya_proses','type'=>'number','validation'=>'required','width'=>'col-sm-9'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();

	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here
	    	// dd($postdata);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)
	    public function getKategoris(){
			$kategoris = DB::table('kategoris')->whereNull('deleted_at')->get();

			foreach ($kategoris as $k => $tt) {
				$dat = [
						'id' => $tt->id,
						'text' => $tt->nama,
						];
				$dat_treatments[] = $dat;
			}
			$dat_treatments = json_encode($dat_treatments);
			return $dat_treatments;
		}

		public function getSelect2($table,$format = 'nama') {
			$kategoris = DB::table($table)->whereNull('deleted_at')
											->select('id',$format.' as text')
											->get();
			return json_encode($kategoris);
		}

		public function getGudang_barang($id){
			$kategoris = DB::table('gudangs')->join('barang_gudangs','barang_gudangs.gudangs_id','=','gudangs.id')
													->whereNull('gudangs.deleted_at')
													->whereNull('barang_gudangs.deleted_at')
													->where('barang_gudangs.barangs_id','=',$id)
													->where('barang_gudangs.stok','>',0)
													->select(
														'barang_gudangs.id as id',
														DB::raw('CONCAT(gudangs.nama," | ",barang_gudangs.kode) as text'),
														'barang_gudangs.stok as title'
														)
													->get();
			return json_encode($kategoris);
		}

	    public function getAdd(){
	    	if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
	    	$this->getAksi(1);
	    }

	    public function postAksi(){
	    	if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
	    	$aksi = Request::get('lanjut');
	    	$request = Request::except(['_token','lanjut']);
	    	// dd($request);
	    	switch ($aksi) {
	    		case "k2":
	    			Session::forget('mix_1');
	    			Session::push('mix_1',$request);
	    			CRUDBooster::redirect(route('resep.mix',2),'');
	    			break;
	    		case "k3":
	    			//check stok digudang
		    		// dd($request);

	    			$barang_gudangs = request()->barang_gudangs;
	    			if(session()->has('mix_2')) {
	    				$mix2 = session()->get('mix_2');
		    			foreach ($mix2 as $key => $value) {
		    				if($value['barang_gudangs']==$barang_gudangs)
		    					CRUDBooster::redirect(route('resep.mix',2),'Barang Sudah ada');
		    			}
	    			}
	    			$stok = DB::table('barang_gudangs')->find($request['barang_gudangs']);
	    			if($request['jumlah'] > $stok->stok) CRUDBooster::redirect(route('resep.mix',2),'Stok barang di gudang tidak cukup');
	    			Session::push('mix_2',$request);
	    			CRUDBooster::redirect(route('resep.mix',2),'');
	    			break;
	    		case "end":
	    			//INSERT
	    			$mix_1 = session()->get('mix_1')[0];
	    			$mix_2 = session()->get('mix_2');
	    			// dd($mix_2);
	    			$resep = new Resep;
	    			$resep->nama = $mix_1['nama'];
	    			$resep->keterangan = $mix_1['keterangan'];
	    			$resep->biaya_proses = $mix_1['biaya_proses'];

	    			$barang = new Barang;
	    			$barang->nama = $mix_1['nama'];
	    			$barang->jenis = $mix_1['jenis'];
	    			$barang->tipe = "finished";
	    			$barang->kategoris_id = $mix_1['kategoris'];
	    			$barang->kode = $mix_1['kode'];
	    			$barang->avgprice = 0;
	    			$barang->ukuran = '0x0';
	    			$barang->min = 0;
	    			$barang->max = 0;
	    			$barang->akumstok = 0;
	    			$barang->satuans_id= $mix_1['satuans'];
	    			$barang->ppn = 0;
	    			$barang->status = 'tidak';
	    			$barang->harga_beli_akhir= 0;
	    			$barang->harga_jual= 0;

	    			$groupbys = new Groupby;
	    			$groupbys->barang_id = $barangs_id;
	    			$groupbys->group_id = $mix_1['groups'];
	    			$barang->save();
	    			$barang_gudang = new Barang_gudang;
	    			$barang_gudang->kode = $mix_1['kode'];
	    			$barang_gudang->barangs_id = $barang->id;
	    			$barang_gudang->gudangs_id = $request['gudangs'];
	    			$barang_gudang->stok =0;
	    			$barang_gudang->min=0;
	    			$barang_gudang->max=0;
	    			$barang_gudang->save();
	    			$resep->barangs_id = $barang->id;
	    			$resep->keterangan = '';
	    			$resep->biaya_proses = 0;
	    			$resep->save();

	    			foreach ($mix_2 as $key => $value) {
	    				$bahan = new Listbahan;
	    				// dd($value);
	    				$bg = DB::table('barang_gudangs')->find($value['barang_gudangs']);
						$display = DB::table('barangs')->find($value['barangs']);
	    				$bahan->barang_gudangs_id = $bg->id;
	    				$bahan->jumlah = $value['jumlah'];
	    				$bahan->reseps_id = $resep->id;
	    				$bahan->display = $display->nama;
	    				$bahan->save();
	    				$stokbaru = $bg->stok - $value['jumlah'];
	    				DB::table('barang_gudangs')->where('id','=',$value['barang_gudangs'])->update(['stok'=>$stokbaru]);
	    			}
	    			session()->forget('mix_1');
	    			session()->forget('mix_2');
	    			CRUDBooster::redirect(CRUDBooster::adminPath('reseps'),'Data berhasil ditambahkan<br/>Jangan lupa menambahkan detail Barang','success');
	    			//save bersama
	    			break;
	    		default:
	    			CRUDBooster::redirect(CRUDBooster::adminPath('reseps'),'Terjadi Kesalahan postAksi');
	    	}
	    }

	    public function getAksi($id){
	    	$data = [];
			$data['page_title'] = 'Mix Item';
			$data['satuans'] = $this->getSelect2('satuans');
	    	switch ($id) {
	    		case 1:
	    			$data['kategoris'] = $this->getSelect2('kategoris');
					$data['groups'] = $this->getSelect2('groups');
					// dd($data['groups']);
			    	$this->cbView('customView.mix_item',$data);
	    			break;
	    		case 2:
	    			if(Session::has('mix_1')) {
	    				$data['barangs'] = $this->getSelect2('barangs');
		    			$data['mix_2'] = $this->getMixTable(2);

		    			$this->cbView('customView.mix_item_2',$data);
	    			}
	    			else {
	    				CRUDBooster::redirect(route('resep.mix',1),'Mix Item bermula dari sini');
	    			}
	    			break;
	    		case 3:
	    			if(Session::has('mix_1')) {
	    				$data['mix_1'] = $this->getMixTable(1);
	    			}
	    			else {
	    				CRUDBooster::redirect(route('resep.mix',1),'Mix Item bermula dari sini');
	    			}
	    			if(Session::has('mix_2')) {
	    				$data['mix_2'] = $this->getMixTable(2);
	    				$data['gudangs'] = $this->getSelect2('gudangs');
		    			$this->cbView('customView.mix_item_3',$data);
	    			}
	    			else {
	    				CRUDBooster::redirect(route('resep.mix',2),'Belum ada Bahan untuk resep');
	    			}
	    			break;
	    		default:
	    			CRUDBooster::redirect(route('resep.mix',1),'Mix Item bermula dari sini');
	    			break;
	    	}
	    }

	    public function getMixBatal(){
	    	if(Session::has('mix_1')) {
	    		Session::forget('mix_1');
	    	}
	    	if(Session::has('mix_2')) {
	    		Session::forget('mix_2');
	    	}
	    	CRUDBooster::redirect(CRUDBooster::adminPath('reseps'),'');
	    }

	    public function getMixTable($id){
	    	switch ($id) {
	    		case 1:
		    		if(Session::has('mix_1')) {
						$mix = Session::get('mix_1')[0];
						// dd($mix);
						$gg = [];
						$dat = [];
						foreach ($mix as $k => $value) {
							if(Schema::hasTable($k)) { //check apakah ada table , (use schema)
								$dat[$k] = DB::table($k)->find($value)->nama;
	    					}
	    					else {
	    						$dat[$k] = $value;
	    					}
						}
						$gg = $dat;
						return $gg;
					}
					else return null;
	    		case 2:
		    		if(Session::has('mix_2')) {
						$mix = Session::get('mix_2');
						$gg = [];
						foreach ($mix as $key => $m2) {
							$dat = [];
							foreach ($m2 as $k => $value) {

								if(Schema::hasTable($k)) { //check apakah ada table , (use schema)
									if($k=='barang_gudangs'){
									$dat[] = DB::table($k)->find($value)->kode;
									}
									else
									$dat[] = DB::table($k)->find($value)->nama;
		    					}
		    					else {
		    						$dat[] = $value;
		    					}
							}
							$dat['id'] = $key;
							// dd($dat);
							$gg[] = $dat;
						}
						return $gg;
					}
					else return null;
	    		default:
	    			return null;
	    	}

	    }

	    public function delBarang($id){
	    	Session::forget('mix_2.'.$id);
	    	$this->getAksi(2);
	    }

	}
