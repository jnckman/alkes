<?php namespace App\Http\Controllers;

  use Session;
  use Request;
  use DB;
  use CRUDBooster;
  use App\Models\Pricelist;
  use App\Models\Log_purchaserequest;
  use App\Models\Lock_rekap;
  use App\Models\Purchaseorder;

  class AdminPeriodeMrsController extends \crocodicstudio\crudbooster\controllers\CBController {
      public $selesaimr = 0;
      public $status = 0;
      public $activeid = 0;
      public function cbInit() {
      
      //$status = Purchaseorder::find()

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "judul";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "periode_mrs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Judul","name"=>"judul"];
			$this->col[] = ["label"=>"Tglmulai","name"=>"tglmulai"];
			$this->col[] = ["label"=>"Tglselesai","name"=>"tglselesai"];
			$this->col[] = ["label"=>"Catatan","name"=>"catatan"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Selesai","name"=>"selesai"];
			$this->col[] = ["label"=>"Tahun","name"=>"tahun"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Judul','name'=>'judul','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Tglmulai','name'=>'tglmulai','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Tglselesai','name'=>'tglselesai','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Catatan','name'=>'catatan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Tahun','name'=>'tahun','type'=>'text','validation'=>'required|integer','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Judul','name'=>'judul','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Tglmulai','name'=>'tglmulai','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tglselesai','name'=>'tglselesai','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Catatan','name'=>'catatan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tahun','name'=>'tahun','type'=>'date','validation'=>'required','width'=>'col-sm-9'];
			# OLD END FORM

    			$this->sub_module = array();
          $this->sub_module[] = ['label'=>'List PO Terbuat','path'=>'purchaseorders','parent_columns'=>'judul','foreign_key'=>'periode_mrs_id','button_color'=>'success','button_icon'=>'fa fa-bars'];

          $this->addaction = array();
          if(CRUDBooster::isSuperadmin()==true){
            $this->addaction[] = ['label'=>'Aktifkan', 'url'=>CRUDBooster::mainpath('aktif/[id]'),'icon'=>'fa fa-check','color'=>'primary','showIf'=>'[status] != "true"','confirmation' => true];
          }
          $this->addaction[] = ['label'=>'List MR','url'=>'listdepartments/[id]','icon'=>'fa fa-bars','color'=>'success','showIf'=>true];
          $this->addaction[] = ['label'=>'print tt','url'=>'printtandaterima/[id]','icon'=>'fa fa-bars','color'=>'success','showIf'=>true];

          $this->button_selected = array();
          $this->alert        = array();
          $this->index_button = array();
          $this->table_row_color = array();
          $this->index_statistic = array();
          $this->script_js = null;
          $this->pre_index_html = null;
          $this->post_index_html = null;
          $this->load_js = array();
          $this->style_css = NULL;
          $this->load_css = array();
      }

     public function actionButtonSelected($id_selected,$button_name) {}
     public function hook_query_index(&$query) {
      
      
     }
     public function hook_row_index($column_index,&$column_value) {
      
      if($column_index==0){
        preg_match_all('!\d+!', $column_value, $matches);
        $this->activeid = $matches[0][0];
        //$this->activeid = substr($column_value,65);
        //$this->activeid = substr($this->activeid, -69,1);
        
      }
      if($column_index==5){

        if($column_value == 'true'){

          $this->status =0;
          $column_value = 'MR AKTIF';
          
        }
        elseif($column_value == 'false'){
          $this->status =1;
          $column_value = 'MR NON AKTIF';
          
        }
        
      }
      // if($column_index==6){
      //   if($column_value == 0){
      //     $this->selesaimr =0;
      //     $column_value = 'on progress';
          
      //   }
      //   else{
      //     $this->selesaimr =1;
      //     $column_value = 'finished';
          
      //   }

      // }

      // selesaimr == 0 > belum selesai mr
      // selesaimr == 1 > sudah selesai mr
      // status == 1 = aktif
      // status == 0 = non aktif
      // if($column_index==8){
          
          

      //       if($this->status == 0)
      //       {
      //         $column_value = '<div class="button_action pull-left" style="text-align:right;"><a class="btn btn-xs btn-primary" title="Aktifkan" 
      //                           onclick="swal({   
      //                             title: `Confirmation`,
      //                             text: `Are you sure want to do this action?`,
      //                             type: `warning`,
      //                             showCancelButton: true,
      //                             confirmButtonColor: `#DD6B55`,
      //                             confirmButtonText: `Yes!`,
      //                             cancelButtonText: `No`,
      //                             closeOnConfirm: true, 
      //                           }, 
      //                             function(){
      //                               location.href=`../admin/periode_mrs/aktif/'.$this->activeid.'`
      //                             }
      //                           );" href="javascript:;"><i class="fa fa-check"></i> Aktifkan</a>';

                                
      //       }
      //       else
      //       {

      //         $column_value = '<div class="button_action pull-left" style="text-align:right;">
      //                           <a class="btn btn-xs btn-primary" disabled title="Aktifkan" onclick="
      //                           swal({   
      //                             title: `Confirmation`,
      //                             text: `Apakah anda akan mengaktfikan MR ini`,
      //                             type: `warning`,
      //                             showCancelButton: true,
      //                             confirmButtonColor: `#DD6B55`,
      //                             confirmButtonText: `Yes!`,
      //                             cancelButtonText: `No`,
      //                             closeOnConfirm: true, 
      //                           }, 
      //                             function(){
      //                               location.href=`../admin/periode_mrs/aktif/'.$this->activeid.'`}
      //                           );" href="#">
      //                           <i class="fa fa-check"></i> Aktifkan</a>';
      //       }


      //       $column_value = $column_value.'          
      //        <a class="btn btn-xs btn-success" title="List MR" href="listdepartments/'.$this->activeid.'">
      //         <i class="fa fa-bars"></i> List MR
      //       </a>          
      //       <a class="btn btn-xs btn-success" title="List PO Terbuat" onclick="" href="http://localhost:8080/wellhosproject/public/admin/purchaseorders?parent_table=periode_mrs&parent_columns=judul&parent_columns_alias=&parent_id='.$this->activeid.'&return_url=public/admin/periode_mrs&foreign_key=periode_mrs_id&label=List+PO+Terbuat">
      //         <i class="fa fa-bars"></i> List PO Terbuat
      //       </a>
      //       <a class="btn btn-xs btn-primary btn-detail" title="Detail Data" href="http://localhost:8080/wellhosproject/public/admin/periode_mrs/detail/'.$this->activeid.'?return_url=public/admin/periode_mrs"><i class="fa fa-eye"></i>
      //       </a>
      //       <a class="btn btn-xs btn-success btn-edit" title="Edit Data" href="public/admin/periode_mrs/edit/'.$this->activeid.'?return_url=http%3A%2F%2Flocalhost%3A8080%2Fwellhosproject%2Fpublic%2Fadmin%2Fperiode_mrs&amp;parent_id=&amp;parent_field="><i class="fa fa-pencil"></i>
      //       </a>
      //       <a class="btn btn-xs btn-warning btn-delete" title="Delete" href="javascript:;" onclick="
      //       swal({
      //         ttitle: `Are you sure ?`,
      //         text: `You will not be able to recover this record data!`,
      //         type: `warning`,   
      //         showCancelButton: true,   
      //         confirmButtonColor: `#ff0000`,   
      //         confirmButtonText: `Yes!`,  
      //         cancelButtonText: `No`,  
      //         closeOnConfirm: false 
      //       }, 
      //         function(){
      //           location.href="http://localhost:8080/wellhosproject/public/admin/periode_mrs/delete/'.$this->activeid.'"
      //         }
      //       );
      //         ">
      //         <i class="fa fa-trash"></i>
      //       </a>
      //     </div>';
        
        
          

          
              
      // }
     }
     public function hook_before_add(&$postdata) {
      $postdata['selesai']=0;
      
        if( date("Y",strtotime($postdata['tglmulai'])) == date("Y") ) //tahun sama
          if( date("m",strtotime($postdata['tglmulai'])) < date("m") ){ //bulan lalu
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Input TANGGAL Periode tidak boleh BULAN lampau","danger");
          }
        if( date("Y",strtotime($postdata['tglmulai'])) < date("Y") ){ //tahun lalu
          CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Input TANGGAL Periode tidak boleh TAHUN lampau","danger");
        }
        if( $postdata['tglselesai'] < $postdata['tglmulai'] ){ //tgl selesai sebelum tanggal mulai
          CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Input tanggal tidak valid","danger");
        } 
     }
     public function hook_after_add($id) {}
     public function hook_before_edit(&$postdata,$id) {}
     public function hook_after_edit($id) {}
     public function hook_before_delete($id) {}
     public function hook_after_delete($id) {}


    public function add_pricelist()
    {
      // dd("OK");
      $data = Request::all();
      // dd($data);
      $tgl = date("Ymd");
      $nama_pricelist = $data["harga_p"]."-".$data["nama_supplier"]."-".$data["id_barang"]."-".$tgl;
      // dd($nama_pricelist);
      $p = new Pricelist;
      $p->nama = $nama_pricelist;
      $p->harga = $data["harga_p"];
      $p->barangs_id =$data["id_barang"];
      $p->suppliers_id = $data["supplier"];
      $p->save();
      session(['status_addprice' => true]);
      $hasil = 'ok';
      return back();
    }

    public function add_pricelistajax()
    {
      // dd("OK");
      $data = Request::all();
      // dd($data);
      $tgl = date("Ymd");
      $nama_pricelist = $data["harga_p"]."-".$data["nama_supplier"]."-".$data["id_barang"]."-".$tgl;
      // dd($nama_pricelist);
      $p = new Pricelist;
      $p->nama = $nama_pricelist;
      $p->harga = $data["harga_p"];
      $p->barangs_id =$data["id_barang"];
      $p->suppliers_id = $data["supplier"];
      $p->save();
      session(['status_addprice' => true]);
      $hasil = 'ok';
      return json_encode($hasil);
    }

    public function log_pricelist()
    {
      $data = Request::all();
      // dd($data);
      $log = new Log_purchaserequest;
      $log->lock_rekaps_id = $data['rekap_id'];
      $log->act = $data['act'];
      $log->save();
      session(['change_pricelist' => true]);
      return back();
    }

    public function getAktif($id){
      DB::table('periode_mrs')->where('id','=',$id)->update(['status'=>'true']);
      DB::table('periode_mrs')->where('id','<>',$id)->update(['status'=>'false']);
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"The status product has been updated !","info");
    }

    public function approve_pr() {

      
      $data = Request::except('_token');
      $id = $data['id'];
      unset($data['id']);
      // dd($data);
      $array_keys = array_keys($data);
      // dd($array_keys);

      for ($i=0; $i < count($data); $i++) {
        if($data[$array_keys[$i]] == 0){

        }
        else{    
        $d = DB::table('lock_rekaps')
            ->where('id', $array_keys[$i])
            ->update(['approve' => $data[$array_keys[$i]]]);
        }
      }
      // session()->flash('approve', true);
      return redirect()->back()->with(['message'=>'PR di Approve','message_type'=>'success']);
    }

    public function unapprove_pr() {

    
      $data = Request::except('_token');
      $id = $data['id'];
      unset($data['id']);
      // dd($data);
      $array_keys = array_keys($data);
      
      // dd($array_keys);

      for ($i=0; $i < count($data); $i++) {
        
        $d = DB::table('lock_rekaps')
            ->where('id', $array_keys[$i])
            ->update(['approve' => null]);

      }
      // session()->flash('approve', true);
      return redirect()->back()->with(['message'=>'Barang Berhasil di buat','message_type'=>'success']);
    }
  }