<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminSuratJalansController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "surat_jalans";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Kode Pengiriman","name"=>"pengiriman_kode"];
			$this->col[] = ["label"=>"User","name"=>"user","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Tgl Cetak","name"=>"tgl_cetak"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM
	      $this->sub_module = array();
	      $this->addaction = array();
	      $this->button_selected = array();
	      $this->alert        = array();
	      $this->index_button = array();
	      $this->table_row_color = array();
	      $this->index_statistic = array();
	      $this->script_js = NULL;
	      $this->pre_index_html = null;
	      $this->post_index_html = null;
	      $this->load_js = array();
	      $this->style_css = NULL;
	      $this->load_css = array();
	   }

	   public function actionButtonSelected($id_selected,$button_name) {}
	   public function hook_query_index(&$query) {}
	   public function hook_row_index($column_index,&$column_value) {}
	   public function hook_before_add(&$postdata) {}
		public function hook_after_add($id) {}
	   public function hook_before_edit(&$postdata,$id) {}
	   public function hook_after_edit($id) {}
		public function hook_before_delete($id) {}
	   public function hook_after_delete($id) {}
	}
