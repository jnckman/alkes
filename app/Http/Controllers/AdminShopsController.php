<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Barang;
	use App\Models\Gudang;
	use App\Models\Barang_gudang;
	use App\Models\Kategori;
	use App\Models\Sales_order;
	use App\Models\Isi_sales_order;
	use App\Models\Customer;
	use Hash;

	class AdminShopsController extends \crocodicstudio\crudbooster\controllers\CBController {

		private $gudangId = 1;
		private $bisaRetur = 'approved';

		public function getHome($kategori=0){
			$data=[];
			$data['privilege'] = json_encode(explode('-',CRUDBooster::myPrivilegeName()));
			if(strtolower(CRUDBooster::myPrivilegeName())=='customer'){
				if(!session()->has('customerShop')){
					$data['customer'] = DB::table('customers')->where('user_id',CRUDBooster::myId())->first();
					if(!isset($data['customer']->jenis_harga_jual)){
						return '<h1>DATA anda belum lengkap; Harap hubungi admin</h1><a href="'.route('getLogout').'">LOG OUT</a>';
					}
					session()->put('customerShop',$data['customer']);
				}

			}
			// dd($data['privilege']);
			$barangBaru = [];
	   		$barangs = Gudang::find($this->gudangId)->barang();

	   		//se ini tk ganti jadi barnag gudang langsung
	   		// $barangs = Barang_gudang::where('gudangs_id',1);
	   		//maaf sam; DENIED !
	   		if(isset($barangs)){
	   			$barangs = $barangs->orderBy('created_at','desc');
		   		$barangBaru = $barangs->take(4)->get();
		   		$barangs = $barangs->paginate(20);
	   		}
	   		else $barangs = [];
	   		$data['kategori'] = [];
	   		$data['page_title'] = 'Online Shop';
			$kategoris = Kategori::has('barang')->get();
			$datacustomer = DB::table('customers')->whereNull('deleted_at')->whereNotNull('jenis_harga_jual')->select('id','nama as text')->get();
			$datacustomer = json_encode($datacustomer);
			if(session()->has('customerShop')){
				$data['customer'] = session()->get('customerShop');
				// dd($data['customer']);
				if(strtolower(CRUDBooster::myPrivilegeName())=='customer'){
					$gg[] = ['text'=>$data['customer']->nama,
							'id'=>$data['customer']->id];
					$datacustomer = json_encode($gg);
				}
			}	

	   		return view('customView.shop_home',[
					   							'data'=>$data,
					   							'kategoris'=>$kategoris,
					   							'barangBaru'=>$barangBaru,
					   							'barangs' => $barangs,
					   							'idKat' => $kategori,
					   							'datacustomer' => $datacustomer,
				   							]);
		}

		public function addCart(){
	    	$request = request();
	    	$cartClient = session()->has('cartClient') ? session()->get('cartClient') : null ;
    		$id = $request->id;
	    	if($cartClient && $request->update==0 && isset($cartClient->items[$id])){
	    		if(array_key_exists($id, $cartClient->items)) {//barang sudah, tambahkan jumlahnya
	    			$jmlh = $cartClient->items[$id]['jmlh']+(int)$request->qnty;
	    			if((int)$request->hargaBaru!='-1'){
	    				$total = $jmlh * (int)$request->hargaBaru;
	    			}else {
	    				$total = $jmlh * (int)$request->harga_jual;
	    				// $total = $jmlh * $cartClient->items[$id]['item']->harga_jual;
	    			}
	    			$cartClient->items[$id]['jmlh'] = $jmlh;
	    			$cartClient->items[$id]['total'] = $total;
	    			return response()->json(['berhasil'=>1,'jmlh'=>$request->qnty,'angkaCart'=>count($cartClient->items)]);
	    		}
	    	}
	    	//belum ada & update
	    	// return response()->json(['berhasil'=>1,'request'=>$request->hargaBaru]);
	    	$barang = DB::table('barangs')->find($id);
	    	if((int)$request->hargaBaru!='-1'){
	    		$total = (int)$request->qnty * (int)$request->hargaBaru;
	    	}
	    	else{
	    		$total = (int)$request->qnty * (int)$request->harga_jual;
	    		// $total = (int)$request->qnty * $barang->harga_jual;
	    		// return response()->json($total);
	    	}
	    	$cartClient->items[$id] = ['item'=>$barang,'jmlh'=>(int)$request->qnty,'total'=>$total];
	    	session()->put('cartClient',$cartClient);
	    	if($request->update==1) $berhasil=2;
	    	else $berhasil =1;
	    	return response()->json(['berhasil'=>$berhasil,'jmlh'=>$cartClient->items[$id]['jmlh'],'angkaCart'=>count($cartClient->items),'total'=>$total]);
	    }

		public function getCart($id=0){
	    	if(session()->has('cartClient')) $cartClient = session()->get('cartClient');
	    	else return redirect(route('getShop'))->with(['message'=>'Cart anda kosong','alert-class'=>'alert-info']);
	    	if(CRUDBooster::myId()==null) return redirect(route('getLogin'));
	    	if(session()->has('customerShop')) {
	    		$alamats = DB::table('alamats')->where('customers_id',session()->get('customerShop')->id)->whereNull('deleted_at')->select('id','alamat as text')->get();
	    	}
	    	else $alamats = [];
	    	$alamats = json_encode($alamats);
	    	// dd($alamats);
	    	$data = [];
	    	$data['page_title'] = 'Shop Cart';
	    	$data['privilege'] = json_encode(explode('-',CRUDBooster::myPrivilegeName()));
	    	$barangs_id = [];
	    	$barangs = $cartClient->items;
	    	// dd($barangs);
	    	return view('customView.shop_cart',[
	    										'barangs'=>$barangs,
	    										'data'=>$data,
	    										'alamats'=>$alamats,
	    										]);
	    }

		public function getProfil(){
			$me = CRUDBooster::me();
			$data['privilege'] = json_encode(explode('-',CRUDBooster::myPrivilegeName()));
			$data['page_title'] = 'Shop_Profil';
			return view('customView.shop_profile',[
													'dataUser'=>$me,
													'sales_orders'=>$so,
													'data'=>$data,
													]);
		}

		public function cariBarang(){
			$request = request();
			if(session()->has('customerShop')){
				$data['customer'] = session()->get('customerShop');
			}
			else{
				return redirect(route('getShop'))->with(['message'=>'Pilih customer terlebih dahulu','alert-class'=>'alert-info']);
			}
			$data['privilege'] = json_encode(explode('-',CRUDBooster::myPrivilegeName()));
			$kunci = $request->kunci;
			if(isset($kunci)){
		    	$hasil = Gudang::find(6)->cariBarang($kunci)
	    								->paginate(25)
	    								// ->get()
	    								;
				// $hasil->appends(['kunci' => $kunci]);
			}
	    	else $hasil= [];
	    	// dd($hasil);
	    	$data['page_title'] = 'Cari Barang';
	    	return view('customView.shop_cari',[
	    					'hasils'=>$hasil,
	    					'data'=>$data,
	    					]);
	    	// return response()->json(['hasil'=>$hasil,'count'=>$count]);
		}

		public function delCart(){
	    	$request = request();
	    	$cart = session()->has('cartClient') ? session()->get('cartClient') : null ;
	    	if($cart){
				$id = $request->id;
				unset($cart->items[$id]);
    			session()->put('cartClient',$cart);
    			if(count($cart->items)==0) session()->forget('cartClient');
    			return response()->json(['berhasil'=>1]);
	    	}
	    	return response()->json(['berhasil'=>0]);
	    }

	    public function checkout(){
	    	$request = request();
			// dd($request);
	    	$user_id = CRUDBooster::myId();
	    	$cart = session()->get('cartClient');
	    	if(!isset($cart)){
	    		return redirect(route('getShop'))->with(['message'=>'Cart anda kosong','alert-class'=>'alert-info']);
	    	}
	    	$total = 0;
	    	foreach ($cart->items as $key => $value) {
	    		$total += $value['total'];
	    	}
	    	$customer =session()->get('customerShop');
	    	if(isset($customer->detailrek)){
	    		$norek = $customer->detailrek['id'];
	    	}
	    	else{
	    		$norek = null;
	    	}
	    	if($customer->jenis_harga_jual=='dua_berlian') $jenis_harga_jual = 'DB';
	    	if($customer->jenis_harga_jual=='gemilang') $jenis_harga_jual = 'GEMILANG';
	    	if($customer->jenis_harga_jual=='online_shop') $jenis_harga_jual = 'ONLINESHOP';
	    	$so = new Sales_order;
	    	$so->customers_id = $customer->id;
	    	$so->user_id = $user_id;
	    	$so->metode_bayar = $request->bayar;
	    	$so->banks_id = $norek;
	    	if($request->alamat>0) $so->alamats_id = $request->alamat;
	    	if(isset($request->diskonTotal)){
	    		$so->diskon = $request->diskonTotal;
	    		$so->total = $total-$request->diskonTotal;
	    	}
	    	else{
	    		$so->total = $total;
	    	}
	    	$so->status = 'new';
	    	$so->save();
	    	$nomor = 'SO-'.date('ym').'-'.$so->id.'-'.$jenis_harga_jual;
	    	$so->update(['nomor'=>$nomor]);
	    	$array = [];
	    	foreach ($cart->items as $key => $value) {
	    		$isi = new Isi_sales_order;
	    		$isi->barangs_id = $key;
	    		$isi->jumlah = $value['jmlh'];
	    		$isi->sales_orders_id = $so->id;
	    		if(isset($request->diskon)){
	    			$isi->diskon = $request->diskon[$key];
	    			$isi->total = $value['total']-$request->diskon[$key];
	    		}
	    		else{
	    			$isi->total = $value['total'];
	    		}
	    		$isi->created_at = date('Y-m-d H:i:s');
	    		$array[] = $isi->toArray();
	    	}
	    	Isi_sales_order::insert($array);
	    	session()->forget('cartClient');
	    	session()->forget('customerShop');
	    	return redirect( route('getShop'))->with(['message'=>'Sales Order berhasil dibuat',
	    														'alert-class'=>'alert-success']);
			// return view('customView.shop_keterangan')->with(['message'=>'Sales Order berhasil dibuat','alert-class'=>'alert-success']);
	    }


	    public function dtsales_order(){
	    	$hasil = Sales_order::leftJoin('customers','customers.id','=','sales_orders.customers_id')
	    						->leftJoin('isi_sales_orders','isi_sales_orders.sales_orders_id','=','sales_orders.id')
	    						->leftJoin('barangs','barangs.id','=','isi_sales_orders.barangs_id')
	    						->leftJoin('pengiriman_barangs',function($join){
	    							// $join->on('pengiriman_barangs.jenis','=','so_baru');
	    							$join->on('pengiriman_barangs.global_id','=','sales_orders.id');
	    							$join->where('pengiriman_barangs.jenis','=','so_baru');
	    						})
	    						->leftJoin('detail_pengiriman_barangs','detail_pengiriman_barangs.pengiriman_barang_id','=','pengiriman_barangs.id')
	    						->select('isi_sales_orders.jumlah','sales_orders.nomor','sales_orders.total','sales_orders.metode_bayar','sales_orders.jatuh_tempo','sales_orders.status','barangs.nama as barang','sales_orders.id as SOid','customers.nama as customer_nama'
	    							,DB::raw('
	    								SUM(CASE When detail_pengiriman_barangs.barang_id = isi_sales_orders.barangs_id Then detail_pengiriman_barangs.jumlah Else 0 End) as jumlahKirim')
	    							,DB::raw('
	    								SUM(CASE When detail_pengiriman_barangs.pengiriman_barang_id = pengiriman_barangs.id Then detail_pengiriman_barangs.jumlah Else 0 End) as totalkirim
	    								')
	    							)
	    						->where('customers.user_id',CRUDBooster::myId())
	    						->orWhere('sales_orders.user_id',CRUDBooster::myId())
	    						->whereNull('isi_sales_orders.deleted_at')
	    						->whereNull('sales_orders.deleted_at')
	    						->groupBy('nomor','barang','jumlah','total','metode_bayar','jatuh_tempo','status','SOid','sales_orders.created_at','customer_nama')
	    						->orderBy('sales_orders.created_at','desc')
	    						->get()
	    						;
	    						// dd($hasil);
			$i=1;
			$data = [];
			foreach($hasil as $d){
				$returSO = '<button type="button" class="btn btn-info btn-xs btnKirim" data-togle="tooltip" title="List pengiriman" style="margin-left:5px"><i class ="fa fa-truck"></i></button>';
				/*if(strtolower($d->status) == $this->bisaRetur) { //status untuk sudah diterima user
	     			$returSO .= '<a href="'.route('shop.returso',$d->SOid).'" class="btn btn-xs btn-danger btnretur" style="margin-left:5px">RETUR</a>';
	     		}*/
	     		$data[] = [
	     				$i, //0
	     				$d->nomor, //1
		     			$d->barang, //2
		     			$d->jumlah, //3
		     			$d->jumlahKirim, //4
	     				$d->total, //5
	     				$d->metode_bayar, //6
	     				$d->jatuh_tempo, //7
	     				' ','', //8,9
	     				$d->status, //10
	     				$returSO, //11
	     				$d->SOid, //12
	     				$d->customer_nama, //13
	     		 		];
	     		$i++;
	    	 }
	    	// dd($data);
	    	$results = [
	    				"draw" => 0,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	return response()->json($results);
	    }

	    public function dt_returso(){
	    	$hasil = DB::table('returso')
	    						->join('isi_returso','isi_returso.returso_id','=','returso.id')
						    	->join('sales_orders','sales_orders.id','=','returso.sales_order_id')
						    	->join('customers','customers.id','sales_orders.customers_id')
	    						->join('isi_sales_orders','isi_sales_orders.id','=','isi_returso.isi_sales_order_id')
	    						->leftJoin('barangs','barangs.id','=','isi_sales_orders.barangs_id')
	    						->where('customers.user_id',CRUDBooster::myId())
	    						->orWhere('sales_orders.user_id',CRUDBooster::myId())
	    						->select('returso.*','returso.keterangan as keteranganRetur','returso.status as statusRetur','isi_returso.*','barangs.nama as barang')
	    						->whereNull('isi_returso.deleted_at')
	    						->whereNull('returso.deleted_at')
	    						->orderBy('returso.created_at','desc')
	    						->get()
	    						;
			$i=1;
			$data = [];
			foreach($hasil as $d){
	     		$data[] = [
	     				$i,
	     				$d->nomor,
		     			$d->barang,
		     			$d->jumlah,
	     				$d->status,
	     				$d->keterangan,
	     				$d->statusRetur,
	     				$d->keteranganRetur,
	     		 		];
	     		$i++;
	    	 }
	    	// dd($data);
	    	$results = [
	    				"draw" => 0,
	    	        	"recordsTotal" => count($data),
	    	        	"recordsFiltered" => count($data),
	    	        	"data" => $data
	    	        	];
	    	return response()->json($results);
	    }

	    public function profilSunting($param=0){
			$request = request();
			if(isset($request->update)){
				DB::table('cms_users')->where('id',CRUDBooster::myId())->update([$param=>$request->update]);
				if($pama='nama')
				return redirect(route('shop.getProfil'))->with(['message'=>$param.' berhasil diedit','alert-class'=>'alert-success']);
			}
			else{ //password, check apakah password 1 2 sama
				if($request->password1 == $request->password2)
				DB::table('cms_users')->where('id',CRUDBooster::myId())->update([$param=>$request->update]);
			}
	    }

	    public function returSO($id){
	    	$so = Sales_order::find($id);
	    	if(strtolower($so->status)!=$this->bisaRetur 
	    		// || strtolower($so->user_id)!=CRUDBooster::myId()
	    	){
	    		return redirect(route('shop.getProfil'))->with(['message'=>'Tidak bisa melakukan Retur','alert-class'=>'alert-danger']);
	    	}

	    	return view('customView.shop_retur',[
	    										'SO'=> $so,
	    										]);
	    }

	    public function postReturSO(){
	    	$request = request();
	    	// dd($request);
	    	$array_insert_isi_returso = [];
	    	$returso = DB::table('returso')->insertGetId([
	    						'sales_order_id'=>$request->sales_order_id,
	    						'status'=> 'new',
	    						'created_at'=>date('Y-m-d H:i:s'),
	    						'keterangan'=>$request->keterangan,
	    					]);
	    	$nomor = 'RSO-'.date('ym').'-'.$returso;
	    	DB::table('returso')->where('id','=',$returso)->update(['nomor'=>$nomor,]);
	    	foreach ($request->jumlah as $key => $jumlah) {
	    		if($jumlah>0){
		    		$array_insert_isi_returso[] = [
	    									'returso_id' => $returso,
	    									'created_at'=>date('Y-m-d H:i:s'),
	    									'isi_sales_order_id'=>$request->isi_sales_order_id[$key],
	    									'jumlah'=>(int)$jumlah,
	    									'status'=>'new',
	    									];
	    		}
	    	}

	    	$isi_returso = DB::table('isi_returso')->insert($array_insert_isi_returso);
	    	return redirect(route('shop.getProfil'))->with(['message'=>'Berhasil membuat request retur','alert-class'=>'alert-success']);
	    }

	    public function getDetailBarang(){
	    	$request= request();
	    	$barang = DB::table('barangs')->where('barangs.id','=',$request->id)->join('satuans','satuans.id','=','barangs.satuans_id')
	    												->join('kategoris','kategoris.id','=','barangs.kategoris_id')
	    												->select('barangs.*','satuans.nama as satuan','kategoris.nama as kategori')
	    												->whereNull('barangs.deleted_at')
	    												->first();
	    												;

	    	return response()->json($barang);
	    }

	    public function getPengiriman(){
	    	$request=request();

	    	$dataz = DB::table('pengiriman_barangs')->where('jenis','=','so_baru')
	    											->where('global_id','=',$request->id)
	    											->whereNull('deleted_at')
	    											->orderBy('created_at','desc')
	    											->get()
	    											;
	    	foreach ($dataz as $key => $d) {
	    		$dd[] = [
	    			'kode'=>$d->kode,
	    			'tanggal'=>$d->tgl_kirim,
	    			'status'=>$d->status,
	    			'action' => '<button type="button" class="btn btn-success btn-xs btn_approvekirim" data-togle="tooltip" title="tandai selesai kirim" style="margin-left:5px;"><i class="fa fa-check"></i></button>',
	    			'id'=>$d->id,
	    		];
	    	}
	    	return response()->json($dd);
	    }

	    public function tandaiselesai(){
	    	$request = request();
	    	$update = DB::table('sales_orders')
			    		->where('user_id',CRUDBooster::myId())
			    		->where('id',$request->id)
			    		->update(['status'=>'Selesai','selesai'=>1,'updated_at'=>now()]);

	    	return response()->json('berhasil');
	    }

	    public function setcustomer(){
	    	$request = request();
			if(session()->has('cartClient')) session()->forget('cartClient');

			$customer = Customer::where('id',$request->customer)->first();
			$rekening = $customer->rekening()->latest()->first();
			if(isset($rekening)){
				$rekening = $rekening->toArray();
			}
			$customer->detailrek = $rekening;
			// dd($customer);
			session()->put('customerShop',$customer);
			return redirect(route('getShop'))->with(['message'=>'Berhasil Memilih Customer','alert-class'=>'alert-success']);
	    }

	    public function getcustomer(){
	    	$customers = [];
	    	$customers = DB::table('customers')->whereNull('deleted_at')->select('id','nama as text')->get();
	    	return response()->json($customers);
	    }

	    public function rahasia_update_ppn(){
	    	$user = DB::table('cms_users')->where('id',CRUDBooster::myId())->first();
			$request = request();
			if (Hash::check($request->pass, $user->password)) {
		    	DB::table('sales_orders')
		    		// ->whereNull('deleted_at')
		    		->whereNull('ppn')
		    		->update(['ppn'=>10]);
	    		dd('done');
	    	}	
	    }
}