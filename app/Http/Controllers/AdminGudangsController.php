<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Barang_gudang;
	use App\Models\Transferlog;
	use App\Models\Barang;
	use App\Models\Gudang;
	use DataTables;
	// use Illuminate\Http\Request as HttpReq;

	class AdminGudangsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "gudangs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-8','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Telp','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email','width'=>'col-sm-8'];
			$this->form[] = ['label'=>'Picture','name'=>'pic','type'=>'upload','validation'=>'min:1|max:255','width'=>'col-sm-8'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-8','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Telp','name'=>'notelp','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:gudangs','width'=>'col-sm-8'];
			//$this->form[] = ['label'=>'Picture','name'=>'pic','type'=>'upload','validation'=>'min:1|max:255','width'=>'col-sm-8','placeholder'=>'Please enter a valid email address'];
			# OLD END FORM

			$this->sub_module = array();
	        $this->addaction = array();

	        $this->addaction[] = ['label'=>'price list','url'=>CRUDBooster::mainpath('set-status/active/[id]'),'icon'=>'','color'=>'success','showIf'=>true];
	        $this->addaction[] = ['label'=>'Set Pending','url'=>CRUDBooster::mainpath('set-status/pending/[id]'),'icon'=>'fa fa-ban','color'=>'warning','showIf'=>"[status] == 'active'", 'confirmation' => true];

	        $this->button_selected = array();
	        $this->alert        = array();
	        $this->index_button = array();
	        $this->table_row_color = array();
	        $this->index_statistic = array();
	        $this->script_js = NULL;
	        $this->pre_index_html = null;
	        $this->post_index_html = null;
	        $this->load_js = array();
	        $this->style_css = NULL;
	        $this->load_css = array();
	    }

	    public function getSetStatus($status,$id) {
	       DB::table('products')->where('id',$id)->update(['status'=>$status]);

	       //This will redirect back and gives a message
	       CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"The status product has been updated !","info");
	    }
	    public function actionButtonSelected($id_selected,$button_name) {
	    }
	    public function hook_query_index(&$query) {
	    }
	    public function hook_row_index($column_index,&$column_value) {
	    }

	   public function hook_before_add(&$postdata) {
	   }

	   public function hook_after_add($id) {
			$kode = "g-".$id;
			DB::table('gudangs')->where('id', $id)->update(['kode' => $kode]);
		}

	    public function hook_before_edit(&$postdata,$id) {
	    }
	    public function hook_after_edit($id) {
	    }
	    public function hook_before_delete($id) {
	    }
	    public function hook_after_delete($id) {
	    }

		public function getIndex() {
 	   	if(!CRUDBooster::isView()) CRUDBooster::denyAccess();

			$data = [];
			$data['page_title'] = 'Gudang';
			$data['result'] = DB::table('gudangs')
									->whereNull('deleted_at')
									->get();
			// dd($data);
 			$this->cbView('customView.gudang',$data);
 		}

		public function dt_master() {
			$data = Gudang::whereNull('deleted_at')->get();
			// dd($data);
			return DataTables::of($data)
				->addColumn('action', function ($data) { //untuk ambil jam saja
					if(CRUDBooster::isUpdate()){
						return '
							<a class="btn btn-success btn-xs" title="Edit" href="'.CRUDBooster::mainpath('edit/'.$data->id).'"><i class="fa fa-pencil"></i></a>
							<button type="button" class="btn btn-info btn-xs" title="Price List" onClick="lihatBarang('.$data->id.')"><i class="fa fa-bars"></i></button>';
					}
				})
				->make(true);
		}

		public function gudang_barang() {
			$id = Request::get('id');
			$data = DB::table('barang_gudangs')
					->leftJoin('barangs', 'barang_gudangs.barangs_id', '=', 'barangs.id')
					->where('barang_gudangs.gudangs_id', $id)
					->where('stok', '!=' , 0)
					->whereNull('barang_gudangs.deleted_at')
					->orderBy('barang_gudangs.barangs_id', 'asc')
					->select('barang_gudangs.*',
								'barangs.nama')
					->get();
			// dd($data);
			return $data;
		}

		public function transferBarang() {
			// $id_d = Request::get('id_gudang_d');
			// $id_k = Request::get('id_gudang_k');
			$data = Request::all();
			$id_d = $data["id_gudang_d"];
			$id_k = $data["id_gudang_k"];
			$catatan = $data["catatan"];
			// dd($data);
			unset($data[_token], $data["id_gudang_d"], $data["id_gudang_k"], $data["catatan"]);
			arsort($data);
			// dd($data);
			$array_keys = array_keys($data);
			// dd(count($data));
			// dd($array_keys);
			$user_id = Session::get('admin_id');
			$tgl = date("Y-m-d");
			$jml_transfer_today = Transferlog::where('user', $user_id)
										->whereDate('created_at', $tgl)
										->distinct('logcode')
										->count('logcode');
			$logcode = "T-".$user_id."-".date("dmY")."-".($jml_transfer_today+1);
			$count = 0;
			$count_success = 0;
			for ($i=0; $i < count($data); $i++) {
				//NOTE karena data null juga diambil, klu ketemu null lgsg berhenti
				if(is_null( $data[$array_keys[$i]] )) {
					break;
				}
				else {
					$log = new Transferlog;

					$data_gudang_d = Barang_gudang::whereNull('deleted_at')
										->where('gudangs_id', $id_d)
										->where('barangs_id', $array_keys[$i])
										->get();
					// dd($data_gudang_d[0]->stok);
					$data_gudang_d[0]->stok -= (int) $data[$array_keys[$i]];
					$data_gudang_d[0]->save();

					$data_gudang_k = Barang_gudang::whereNull('deleted_at')
										->where('gudangs_id', $id_k)
										->where('barangs_id', $array_keys[$i])
										->get();
					// dd($data_gudang_k[0]->stok);
					if($data_gudang_k[0]){ //digudang ada barang
						$data_gudang_k[0]->stok += (int) $data[$array_keys[$i]];
						// dd($data_gudang_k[0]->stok);
						$log->stok_stlh_transfer_k = $data_gudang_k[0]->stok;
						//dd($log->stok_stlh_transfer_k);
						$log->stok_sblm_transfer_k = $data_gudang_k[0]->stok - (int) $data[$array_keys[$i]];

						$data_gudang_k[0]->save();
					}
					else{
						$new = new Barang_gudang;
						$new->gudangs_id = $id_k;
						$new->barangs_id = $array_keys[$i];
						$new->kode = Barang::where('id', $array_keys[$i])->pluck('kode')->first();
						$new->stok = (int) $data[$array_keys[$i]];

						$log->stok_sblm_transfer_k = 0;
						$log->stok_stlh_transfer_k = (int) $data[$array_keys[$i]];
						// dd($new->stok);
						// dd($log->stok_stlh_transfer_k);
						$new->save();
					}

					$log->user = $user_id;
					$log->tgl = $tgl;
					$log->logcode = $logcode;
					$log->barang_id = $array_keys[$i];
					$log->gudangs_id_dari = $id_d;
					$log->gudangs_id_ke = $id_k;
					$log->stok_sblm_transfer_d = $data_gudang_d[0]->stok + (int) $data[$array_keys[$i]];
					$log->stok_stlh_transfer_d = $data_gudang_d[0]->stok;
					$log->jml_transfer = (int) $data[$array_keys[$i]];
					$log->note = $catatan;
					$saved = $log->save();
					if ($saved) $count_success++;
				}
				$count++;
			}

			//dd($count_success);
			if ($count_success == $count){
				$config['content'] = "Ada ".$count_success." transfer barang";
				$config['to'] = CRUDBooster::adminPath('transferlogs');
				$config['id_cms_users'] = [1];
				CRUDBooster::sendNotification($config);
				CRUDBooster::redirect(CRUDBooster::adminPath('gudangs'), 'Transfer Berhasil !!!', 'success');
			}
			else CRUDBooster::redirect(CRUDBooster::adminPath('gudangs'), 'Transfer Gagal !!!', 'danger');;
		}
		//NOTE jgn lupa bobo
	}