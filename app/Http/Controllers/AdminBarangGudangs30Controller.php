<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Models\Barang_gudang;
	use App\Models\Split_log;
	use App\Models\Barang;
	use App\Models\Transferlog;

	class AdminBarangGudangs30Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "barang_gudangs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Barang","name"=>"barangs_id","join"=>"barangs,nama"];
			$this->col[] = ["label"=>"Kode","name"=>"kode"];
			$this->col[] = ["label"=>"Gudang","name"=>"gudangs_id","join"=>"gudangs,nama"];
			$this->col[] = ["label"=>"Stok","name"=>"stok"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			$this->form[] = ['label'=>'Gudangs Id','name'=>'gudangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'gudangs,nama'];
			$this->form[] = ['label'=>'Stok','name'=>'stok','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Min Stok','name'=>'min','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Max Stok','name'=>'max','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Refrencecode','name'=>'refrencecode','type'=>'text','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Barangs Id','name'=>'barangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'barangs,nama'];
			//$this->form[] = ['label'=>'Gudangs Id','name'=>'gudangs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'gudangs,nama'];
			//$this->form[] = ['label'=>'Stok','name'=>'stok','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Min Stok','name'=>'min','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Max Stok','name'=>'max','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
			# OLD END FORM

			$this->sub_module = array();

	        $this->addaction = array();
	        //caranya jadi rute dan ambil id
	        if(CRUDBooster::isSuperadmin()==true){
	        	$this->addaction[] = ['label'=>'Split','url'=>'barang_gudangs/split/[id]','icon'=>'fa fa-check','color'=>'danger'];
				$this->addaction[] = ['label'=>'Transfer','url'=>'barang_gudangs/transfer/[id]','icon'=>'fa fa-exchange','color'=>'info'];
	        }
	        $this->addaction[] = ['url'=>'barang_gudangs/detail_barang/[id]','icon'=>'fa fa-eye','color'=>'primary'];

	        $this->button_selected = array();
	        $this->alert        = array();
	        $this->index_button = array();
	        $this->table_row_color = array();
	        $this->index_statistic = array();
	        $this->script_js = NULL;
	        $this->pre_index_html = null;
	        $this->post_index_html = null;
	        $this->load_js = array();
	        $this->style_css = NULL;
	        $this->load_css = array();
	    }
	    public function actionButtonSelected($id_selected,$button_name) {}
	    public function hook_query_index(&$query) {}
	    public function hook_row_index($column_index,&$column_value) {}
	    public function hook_before_add(&$postdata) {}
	    public function hook_after_add($id) {}
	    public function hook_before_edit(&$postdata,$id) {}
	    public function hook_after_edit($id) {}
	    public function hook_before_delete($id) {}
	    public function hook_after_delete($id) {
	        //Your code here
	    	DB::table('split_logs')->where('barang_gudangs_id_2','=',$id)->delete();
	    }

	    public function getSplitItem($id){
	    	if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$barang = DB::table('barang_gudangs')->find($id);
			// dd($barang);
	    	$data = [];
	    	$data['page_title']='Split Item';
	    	$data['barang_gudangs'] = $barang;
	    	$gudangku = DB::table('gudangs')->find($barang->gudangs_id);
	    	$barang = DB::table('barangs')->find($barang->barangs_id);
	    	$data['barangku'] = $barang;
	    	$data['gudangku'] = $gudangku;
	    	// dd($barangku);
	    	$kategoris = DB::table('gudangs')->whereNull('deleted_at')
											->select('id','nama as text')
											->get();
			$data['gudangs'] = json_encode($kategoris);

	    	$this->cbView('customView.split_item',$data);
	    }

	    public function postSplitItem(){
	    	if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
	    	$request = request();
	    	// dd($request);
	    	$barang_gudang_lama = Barang_gudang::find($request->barang_gudangs_1);
	    	$barang_lama = Barang::find($barang_gudang_lama->barangs_id);
	    	// dd($barang_lama);
	    	// if($barang_gudang_lama->stok % $request->pembagi != 0) CRUDBooster::redirect(route('barang_gudangs.getsplit',$request->barang_gudangs_1),trans("Stok tidak habis dibagi ".$request->pembagi));

			// dd($barang_lama);
	    	// $jmlh_split1 = $barang_gudang_lama->stok / $request->pembagi;
	    	// $jmlh_split = $barang_gudang_lama->stok / $jmlh_split1;
	    	// $array_barang = [];
	    	// $array_brang_gudang = [];
	    	// $array_split = [];
	    	// dd($id_barang_gudang);
	    	// for ($i=1; $i <= $jmlh_split; $i++) {
	    		// $kode = $request->kode."_".sprintf("%04d",$i);
	    		$kode = $request->kode;
	    		$barang_baru = new Barang;
	    		// $barang_baru->id = $id_barang;
	    		// $barang_baru->nama = $request->nama."_".sprintf("%04d",$i);
	    		$barang_baru->nama = $request->nama;
	    		$barang_baru->kode = $kode;
	    		$barang_baru->min = $request->min;
	    		$barang_baru->max = $request->max;
	    		$barang_baru->jenis = $barang_lama->jenis;
	    		$barang_baru->kategoris_id = $barang_lama->kategoris_id;
	    		$barang_baru->avgprice = 0;
	    		$barang_baru->ukuran = '0x0';//$barang_lama->ukuran;
	    		// $barang_baru->akumstok = 0;
	    		$barang_baru->akumstok = $request->pembagi;
	    		$barang_baru->satuans_id = $barang_lama->satuans_id;
	    		$barang_baru->ppn = 0;
	    		$barang_baru->status = 'split';
	    		$barang_baru->harga_beli_akhir = 0;
	    		$barang_baru->harga_jual  = 0;
	    		// $barang_baru->created_at = date('Y-m-d H:i:s');
	    		$barang_baru->save();
	    		// dd($barang_baru->id);
	    		$barang_gudangs_baru = new Barang_gudang;
		    	$barang_gudangs_baru->kode = $kode;
		    	$barang_gudangs_baru->barangs_id = $barang_baru->id;
		    	$barang_gudangs_baru->gudangs_id = $request->gudangs;
		    	// $barang_gudangs_baru->stok = $jmlh_split1; //$request->stok; //menunggu revisi
		    	$barang_gudangs_baru->stok = $request->pembagi;
		    	$barang_gudangs_baru->min = $request->min;
		    	$barang_gudangs_baru->max = $request->max;
		    	// $barang_gudangs_baru->created_at = date('Y-m-d H:i:s');
		    	$barang_gudangs_baru->save();

		    	$split = new Split_log;
		    	$split->barangs_id = $barang_gudang_lama->barangs_id;
		    	$split->barang_gudangs_id_1 = $request->barang_gudangs_1;
		    	$split->barang_gudangs_id_2 = $barang_gudangs_baru->id;
		    	$split->gudangs_id = $request->gudangs;
		    	$split->jumlah = $jmlh_split1;//$request->stok;
		    	$split->created_at = date('Y-m-d H:i:s');
		    	$split->save();
		    	// $array_barang[] = $barang_baru->toArray();
		    	// $array_brang_gudang[] = $barang_gudangs_baru->toArray();
		    	// $array_split[] = $split->toArray();
	    	// }
	    	// Barang::insert($array_barang);
	    	// Barang_gudang::insert($array_brang_gudang);
	    	// Split_log::insert($array_split);
	    	// $barang_gudang_lama->update([
		    // 	'stok' => 0,
		    // ]);
	    	$barang_gudang_lama->update([
		    	'stok' => $barang_gudang_lama->stok - $request->pembagi,
		    ]);
		    $barang_lama->update([
		    	'akumstok' => $barang_lama->akumstok - $request->pembagi,
		    ]);
	    	CRUDBooster::redirect(CRUDBooster::adminPath('barang_gudangs30'),'Data berhasil ditambahkan<br/>Jangan Lupa mengisi detail barang','success');
	    }

		public function transferView($id) {
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			//barang di gudang
			$barang_digudang = DB::table('barang_gudangs')->find($id);
			// dd($barang);
	    	$data = [];
	    	$data['page_title']='Transfer Barang';
	    	$data['barang_gudangs'] = $barang_digudang;
	    	$gudang = DB::table('gudangs')->find($barang_digudang->gudangs_id);
	    	$barang = DB::table('barangs')->find($barang_digudang->barangs_id);
	    	$data['barangku'] = $barang;
	    	$data['gudangku'] = $gudang;

			$data['semua_gudang'] = DB::table('gudangs')
											->whereNull('deleted_at')
											->where('id', '!=', $barang_digudang->gudangs_id)
											->get();

			// dd($data['semua_gudang']);

	    	$this->cbView('customView.transfer_view',$data);
		}

		public function transferBarang() {
			$data = Request::all();
			// dd($all);
			$id_d = $data["id_gudang_d"];
			$id_k = $data["id_gudang_k"];
			$id_barang = $data["id_barang"];
			$jumlah = $data["jumlah"];
			$catatan = $data["catatan"];

			$user_id = Session::get('admin_id');
			$tgl = date("Y-m-d");
			$jml_transfer_today = Transferlog::where('user', $user_id)
										->whereDate('created_at', $tgl)
										->distinct('logcode')
										->count('logcode');

			$logcode = "T-".$user_id."-".date("dmY")."-".($jml_transfer_today+1);

			$log = new Transferlog;

			$data_gudang_d = Barang_gudang::whereNull('deleted_at')
								->where('gudangs_id', $id_d)
								->where('barangs_id', $id_barang)
								->get();

			$data_gudang_d[0]->stok -= (int) $jumlah;
			$data_gudang_d[0]->save();

			$data_gudang_k = Barang_gudang::whereNull('deleted_at')
								->where('gudangs_id', $id_k)
								->where('barangs_id', $id_barang)
								->get();

			if($data_gudang_k[0]){ //digudang ada barang
				$data_gudang_k[0]->stok += (int) $jumlah;

				$log->stok_stlh_transfer_k = $data_gudang_k[0]->stok;

				$log->stok_sblm_transfer_k = $data_gudang_k[0]->stok - (int) $jumlah;

				$data_gudang_k[0]->save();
			}
			else{
				$new = new Barang_gudang;
				$new->gudangs_id = $id_k;
				$new->barangs_id = $id_barang;
				$new->kode = Barang::where('id', $id_barang)->pluck('kode')->first();
				$new->stok = (int) $jumlah;

				$log->stok_sblm_transfer_k = 0;
				$log->stok_stlh_transfer_k = (int) $jumlah;
				$new->save();
			}
			$log->user = $user_id;
			$log->tgl = $tgl;
			$log->logcode = $logcode;
			$log->barang_id = $id_barang;
			$log->gudangs_id_dari = $id_d;
			$log->gudangs_id_ke = $id_k;
			$log->stok_sblm_transfer_d = $data_gudang_d[0]->stok + (int) $jumlah;
			$log->stok_stlh_transfer_d = $data_gudang_d[0]->stok;
			$log->jml_transfer = (int) $jumlah;
			$log->note = $catatan;
			$saved = $log->save();

			$config['content'] = "Ada transfer barang";
			$config['to'] = CRUDBooster::adminPath('transferlogs');
			$config['id_cms_users'] = [1];
			CRUDBooster::sendNotification($config);

			return redirect(CRUDBooster::adminPath('barang_gudangs30'));
			CRUDBooster::redirect(CRUDBooster::adminPath('barang_gudangs30'));
		}

		public function getDetail($id) {
			// dd(CRUDBooster::isCreate());
			// if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
	  //   		CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
	  // 		}
			// dd($id);
			$d = DB::table('barang_gudangs')
								->where('id', $id)
								->select('barangs_id')
								->first();

			// dd($d->barangs_id);

			$data = [];
	  		$data['page_title'] = 'Detail Barang';
	  		$data['row'] = DB::table('barangs')->where('barangs.id', $d->barangs_id)
						->leftJoin('satuans', 'barangs.satuans_id', '=', 'satuans.id')
						->leftJoin('kategoris', 'barangs.kategoris_id', '=', 'kategoris.id')
						->select('barangs.kode',
									'barangs.nama',
									'barangs.jenis',
									'barangs.tipe',
									'barangs.ukuran',
									'barangs.min',
									'barangs.max',
									'barangs.akumstok',
									'barangs.status',
									'barangs.ppn',
									'barangs.harga_beli_akhir',
									'barangs.harga_jual',
									'barangs.gambar',
									'barangs.avgprice',
									'barangs.spesifikasi',
									'barangs.cara_pakai',
									'barangs.video',
									'satuans.nama as satuan',
									'kategoris.nama as kategori')
						->first();
			// dd($data);
	  		$this->cbView('customView.barang_detail',$data);
		}

}